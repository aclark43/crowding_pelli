function errorBar( xData, yData )
%ERRORBAR Summary of this function goes here
%   xData = curvature, velocity, etc in a vector
%   yData = strokewidth vector the same length

%         allThresh = xData;
        averageThresh = mean(xData);
        corThresh = mean(yData);
        sigma = std(xData);
        sigma2 = std(yData);

        hold on
        errorbar(averageThresh,corThresh,sigma,...
            'horizontal', '-ok','MarkerSize',6,...
            'MarkerEdgeColor','black',...
            'MarkerFaceColor','black',...
            'CapSize',4,...
            'LineWidth',2)
end

