%%% Function to calculate number of cones that share stimulation from a
%%% given target plus a flanker.

%%% x = the eye trace in one dimension
%%% stimSize = the size of the stimulus in the same dimension

%%%NOTE:- this function only looks at one dimension of space
%%%     - this function will only report shared cones if they were 
%%%         stimulated for a minimum of 5 samples.

%%% Created by Ashley M. Clark, 2023

function prob = coneAndFlankerProbability(x,stimSize)


%%% if running alone instead of function
% Xe = [];Ye = [];
% for i = 1:30
%     [XeT,YeT] = EM_Brownian2(40, 1000, 500, 1); %(area of about 4.5 vs 80 = )
%     Xe = [Xe XeT];
%     Ye = [Ye YeT];
% 
% end
% area = polyarea(Xe,Ye)
% x = abs(Xe(1,:));
% stimSize = 3.7;
% x = [33.6328   33.6328   33.7500   33.7500   33.6328   33.5156   33.6328   33.7500   33.5156   33.6328   33.6328   33.7500   33.6328   33.5156  33.6328]
% stimSize = 2;
% x = xALLTrial{1, 1}
% y = yALLTrial{1, 1}
% stimSize = round(stimSize);%;2;%(arcmin) 
spacingCones = 0.5;% (arcmin)
stimSizeInCones = stimSize / spacingCones;
% figure;plot(x-x(1),y-y(1)); axis([-5 5 -5 5])

targetActivation = abs(round(x) + round( (x-round(x))/0.5) * 0.5);
targetActRight = targetActivation + (stimSizeInCones/2);
targetActLeft = targetActivation - (stimSizeInCones/2);

xFlanker = targetActivation + (stimSize * (1.4/.5));
flankerActivation = abs(round(xFlanker) + ...
    round( (xFlanker-round(xFlanker))/0.5) * 0.5);
flankerActRight= flankerActivation + (stimSizeInCones/2);
flankerActLeft = flankerActivation - (stimSizeInCones/2);

% % figure;plot(abs(targetActivation*2),'Color','r');hold on;
% % leg(1) = plot(abs(targetActRight*2),'Color','r'); hold on
% % plot(abs(targetActLeft*2),'Color','r'); hold on

% % leg(2) = plot(abs(flankerActivation*2),'Color','b');hold on;
% % plot(abs(flankerActRight*2),'Color','b'); hold on
% % plot(abs(flankerActLeft*2),'Color','b');
% % legend(leg,{'Target','Flanker'});
% % ylabel('Cones Stimulated');

[targetAll,~, numTimesT] = unique([targetActivation targetActRight targetActLeft]);
[flankerAll,~, numTimesF] = unique([flankerActivation flankerActRight flankerActLeft]);

a_countsT = accumarray(numTimesT,1);
% value_countsT = [targetAll', a_countsT];
targetAllThresh = targetAll(find(a_countsT'>4));

a_countsF = accumarray(numTimesF,1);
% value_countsF = [flankerAll', a_countsF];
flankerAllThresh = flankerAll(find(a_countsF'>4)); %cone must be stimulated at least X times

sameCones = intersect(targetAllThresh, flankerAllThresh);
prob = length(sameCones)/length(targetAllThresh);











