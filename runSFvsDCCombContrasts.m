clc
clear all

dataAll{5} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A031\data.mat');
dataAll{6} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A039\data.mat');

dataAll{1} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A008_DDPI\data.mat');
dataAll{2} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A085_DDPI\data.mat');
dataAll{3} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A084\data.mat');
dataAll{4} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A040\data.mat');
% dataAll{5} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A047_DDPI\data.mat');

samplingRate = [341 341 1000 1000 1000 1000];

counter = 1;
for subIdx = 1:(length(dataAll))
    fixationTrials = find(dataAll{subIdx}.data.TimeFixationON > 0);
    temp = [dataAll{subIdx}.fix.trialid];
    fixPos = [];
    for i = 1:length(temp)
        if find(temp(i) == fixationTrials)
            if length(dataAll{subIdx}.fix(i).span) < 150
                fixPos.x{counter} = dataAll{subIdx}.fix(i).x;
                fixPos.y{counter} = dataAll{subIdx}.fix(i).y;
                counter = counter + 1;
            end
        end
    end
  
    forDSQCalcShort.x = fixPos.x;
    forDSQCalcShort.y = fixPos.y;
    
    [~,Bias,dCoefDsq(subIdx), ~,Dsq, ...
        SingleSegmentDsq,~,~] = ...
        CalculateDiffusionCoef(samplingRate(subIdx), struct('x',forDSQCalcShort.x, 'y', forDSQCalcShort.y));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % %%% Determine Best Contrasts
% contrasts20{4} = [31.6200, 32.36];
% contrasts20{5} =  [31.62] 17.78]; 
% contrasts20{3} = [37.49 43.3];
% contrasts6{4} = [1.5800 1.78];
% contrasts6{5} =  [1.20 1.0]; 
% contrasts6{3} = [1.58 1.19];
% contrasts20{1} = [35.68 42.43];
% contrasts20{2} = [63.5 71.35];
% contrasts6{1} = [0.94 1.88];
% contrasts6{2} =  [7.94 7.5]; 
% contrasts6{6} = [1.54 1.41];
% contrasts20{6} = [17.78 31.62];
% % % % 
% idx = find(dataAll{subIdx}.data.spatialFreq == 20);
% temp = (dataAll{subIdx}.data.contrast(idx))
% unique(temp)
% numTrials20(subIdx) = sum(dataAll{subIdx}.data.Correct(find...
%     (dataAll{subIdx}.data.spatialFreq == 20 & ...
%     dataAll{subIdx}.data.contrast == contrasts20{subIdx}&...
%     dataAll{subIdx}.data.Correct ~= 3)))
% perf = mean(dataAll{subIdx}.data.Correct(find...
%     (dataAll{subIdx}.data.spatialFreq == 20 & ...
%     dataAll{subIdx}.data.contrast == contrasts20{subIdx} &...
%     dataAll{subIdx}.data.Correct ~= 3)))
% 
% idx = find(dataAll{subIdx}.data.spatialFreq == 6);
% temp = (dataAll{subIdx}.data.contrast(idx))
% unique(temp)
% numTrials6(subIdx) = sum((dataAll{subIdx}.data.Correct(find...
%     (dataAll{subIdx}.data.spatialFreq == 6 & ...
%      dataAll{subIdx}.data.contrast == contrasts6{subIdx}&...
%     dataAll{subIdx}.data.Correct ~= 3))))
% 
% perf = mean((dataAll{subIdx}.data.Correct(find...
%     (dataAll{subIdx}.data.spatialFreq == 6 & ...
%      dataAll{subIdx}.data.contrast == contrasts6{subIdx}&...
%     dataAll{subIdx}.data.Correct ~= 3))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% contrasts20 = [31.6200 17.78 34.89];
% contrasts6 = [1.5800 1.54 1.58];
% contrasts20 = [31.6200 21.34 37.49];
% contrasts6 = [1.5800 1.20 1.58];
contrasts20{5} = [31.6200, 32.36];
contrasts20{4} =  [21.34 17.78]; 
contrasts20{3} = [37.49 43.3];
contrasts6{5} = [1.5800 1.78];
contrasts6{4} =  [1.20 1.0]; 
contrasts6{3} = [1.58 1.19];
contrasts20{1} = [35.68 42.43];
contrasts20{2} = [63.5 71.35];
contrasts6{1} = [0.94 1.88];
contrasts6{2} =  [7.94 7.5]; 
contrasts6{6} = [1.54 1.41];
contrasts20{6} = [17.78 31.62];

% trials20{subIdx}.idx = ((find...
%     (dataAll{subIdx}.data.spatialFreq == 20 & ...
%     (dataAll{subIdx}.data.contrast == contrasts20{subIdx}(1)|...
%     dataAll{subIdx}.data.contrast == contrasts20{subIdx}(2))&...
%     dataAll{subIdx}.data.Correct ~= 3)));

trials20{subIdx}.idxContrasts{1} = ((find...
    (dataAll{subIdx}.data.spatialFreq == 20 & ...
     (dataAll{subIdx}.data.contrast == contrasts20{subIdx}(1))&...
    dataAll{subIdx}.data.Correct ~= 3)));
trials20{subIdx}.idxContrasts{2} = ((find...
    (dataAll{subIdx}.data.spatialFreq == 20 & ...
     (dataAll{subIdx}.data.contrast == contrasts20{subIdx}(2))&...
    dataAll{subIdx}.data.Correct ~= 3)));

trials20{subIdx}.idx = [trials20{subIdx}.idxContrasts{1} trials20{subIdx}.idxContrasts{2}];


trials20{subIdx}.performance = dataAll{subIdx}.data.Correct(trials20{subIdx}.idx);

ids = [dataAll{subIdx}.fix.trialid];
counter = 1;
for c = 1%:length(trials20{subIdx}.idxContrasts)
%     counter = 1;
    for i = 1:length(ids)
        if find(ids(i) == trials20{subIdx}.idx)
             if (dataAll{subIdx}.fix(i).span) < 100
                trials20{subIdx}.x{counter} = dataAll{subIdx}.fix(i).x;
                trials20{subIdx}.y{counter} = dataAll{subIdx}.fix(i).y;
                [~,~,trials20{subIdx}.dcTrial{c,counter}] =...
                    CalculateDiffusionCoef(samplingRate(subIdx), struct('x',trials20{subIdx}.x{counter},...
                    'y', trials20{subIdx}.y{counter}));
                trials20{subIdx}.perfSelect{c,counter} = dataAll{subIdx}.data.Correct(...
                    dataAll{subIdx}.fix(i).trialid);
                counter = counter + 1;
            end
        end
    end
end
forDSQCalc20.x = trials20{subIdx}.x;
forDSQCalc20.y = trials20{subIdx}.y;

[~,Bias,trials20{subIdx}.dCoefDsq, ~,Dsq, ...
    SingleSegmentDsq,~,~] = ...
    CalculateDiffusionCoef(1000, struct('x',forDSQCalc20.x, 'y', forDSQCalc20.y));


trials6{subIdx}.idxContrasts{1} = ((find...
    (dataAll{subIdx}.data.spatialFreq == 6 & ...
     (dataAll{subIdx}.data.contrast == contrasts6{subIdx}(1))&...
    dataAll{subIdx}.data.Correct ~= 3)));
trials6{subIdx}.idxContrasts{2} = ((find...
    (dataAll{subIdx}.data.spatialFreq == 6 & ...
     (dataAll{subIdx}.data.contrast == contrasts6{subIdx}(2))&...
    dataAll{subIdx}.data.Correct ~= 3)));

% trials6{subIdx}.idx = ((find...
%     (dataAll{subIdx}.data.spatialFreq == 6 & ...
%      (dataAll{subIdx}.data.contrast == contrasts6{subIdx}(1)|...
%     dataAll{subIdx}.data.contrast == contrasts6{subIdx}(2))&...
%     dataAll{subIdx}.data.Correct ~= 3)));
trials6{subIdx}.idx = [trials6{subIdx}.idxContrasts{1} trials6{subIdx}.idxContrasts{2}];


trials6{subIdx}.performance = dataAll{subIdx}.data.Correct(trials6{subIdx}.idx);

ids = [dataAll{subIdx}.fix.trialid];


for c = 1%:length(trials6{subIdx}.idxContrasts)
    counter = 1;
    test = 1;
    for i = 1:length(ids)
        if find(ids(i) == trials6{subIdx}.idx)
           if (dataAll{subIdx}.fix(i).span) < 100 
                trials6{subIdx}.x{counter} = dataAll{subIdx}.fix(i).x;
                trials6{subIdx}.y{counter} = dataAll{subIdx}.fix(i).y;
                [~,~,trials6{subIdx}.dcTrial{c,counter}] =...
                    CalculateDiffusionCoef(samplingRate(subIdx), struct('x',trials6{subIdx}.x{counter},...
                    'y', trials6{subIdx}.y{counter}));
                trials6{subIdx}.perfSelect{c,counter} = dataAll{subIdx}.data.Correct(...
                    dataAll{subIdx}.fix(i).trialid);
                counter = counter + 1;
           else
               test = 1 + test;
            end
        end
    end
end

forDSQCalc6.x = trials6{subIdx}.x;
forDSQCalc6.y = trials6{subIdx}.y;

[~,Bias,trials6{subIdx}.dCoefDsq] = ...
    CalculateDiffusionCoef(samplingRate(subIdx), struct('x',forDSQCalc6.x, 'y', forDSQCalc6.y));
end

for i = 1:length(dataAll)
    allDC6(i) = trials6{i}.dCoefDsq;
    allDC20(i) = trials20{i}.dCoefDsq;
end
figure;
critFreq20 = powerAnalysis_JustCriticalFrequency(allDC20, 0);
hold on
critFreq6 = powerAnalysis_JustCriticalFrequency(allDC6, 0);
hold on
critFreq6 = powerAnalysis_JustCriticalFrequency(dCoefDsq(i), 0);
%1, 3, 5
dcThresh20S = [8 10 10 12 8 5.5];
dcThresh20L = [8 10 11 13 17 5.5];
dcThresh6S = [8 10 12 5 7 6];
dcThresh6L = [8 10 15 15 7 6];

colr = winter(6);
for c = 1
    for i = [1:6]
        idxSmall = [];
        idxSmall = find(cell2mat(trials20{i}.dcTrial(c,:)) < dcThresh20S(i));
        sf20.meanDCSmall(i) = mean(cell2mat(trials20{i}.dcTrial(c,idxSmall)));
        sf20.meanPerfSmall(i) = mean(cell2mat(trials20{i}.perfSelect(c,idxSmall)));
        sf20.numPerfSmall(i) = length(cell2mat(trials20{i}.perfSelect(c,idxSmall)));
        sf20.numPerfVecS{i} = ((trials20{i}.perfSelect(c,idxSmall)));
        sf20.numdcVecS{i} = ((trials20{i}.dcTrial(c,idxSmall)));
        
        idxLarge = [];
        idxLarge = find(cell2mat(trials20{i}.dcTrial(c,:)) > dcThresh20L(i) & ...
            cell2mat(trials20{i}.dcTrial(c,:)) < 60);
        sf20.meanDCLarge(i) = mean(cell2mat(trials20{i}.dcTrial(c,idxLarge)));
        sf20.meanPerfLarge(i) = mean(cell2mat(trials20{i}.perfSelect(c,idxLarge)));
        sf20.numPerfLarge(i) = length(cell2mat(trials20{i}.perfSelect(c,idxLarge)));
        sf20.numdcVecL{i} = ((trials20{i}.dcTrial(c,idxLarge)));
        sf20.numPerfVecL{i} = ((trials20{i}.perfSelect(c,idxLarge)));

        
        idxSmall = [];
        idxSmall = find(cell2mat(trials6{i}.dcTrial(c,:)) < dcThresh6S(i));
        sf6.meanDCSmall(i) = mean(cell2mat(trials6{i}.dcTrial(c,idxSmall)));
        sf6.meanPerfSmall(i) = mean(cell2mat(trials6{i}.perfSelect(c,idxSmall)));
        sf6.numPerfSmall(i) = length(cell2mat(trials6{i}.perfSelect(c,idxSmall)));
        sf6.numPerfVecS{i} = ((trials6{i}.perfSelect(c,idxSmall)));
        sf6.numdcVecS{i} = ((trials6{i}.dcTrial(c,idxSmall)));

        
        idxLarge = [];
        idxLarge = find(cell2mat(trials6{i}.dcTrial(c,:)) > dcThresh6L(i) & ...
            cell2mat(trials6{i}.dcTrial(c,:)) < 60);
        sf6.meanDCLarge(i) = mean(cell2mat(trials6{i}.dcTrial(c,idxLarge)));
        sf6.meanPerfLarge(i) = mean(cell2mat(trials6{i}.perfSelect(c,idxLarge)));
        sf6.numPerfLarge(i) = length(cell2mat(trials6{i}.perfSelect(c,idxLarge)));
        sf6.numdcVecL{i} = ((trials6{i}.dcTrial(c,idxLarge)));
        sf6.numPerfVecL{i} = ((trials6{i}.perfSelect(c,idxLarge)));
       
    end
end
figure;
subplot(2,2,[1 2])
for i = [3 4 5 6]
%     figure;
    l(1) = plot([sf20.meanDCSmall(i) sf20.meanDCLarge(i)],...
        [sf20.meanPerfSmall(i) sf20.meanPerfLarge(i)],...
        '-o','Color',colr(i,:),'MarkerSize',20);
    text([sf20.meanDCSmall(i) sf20.meanDCLarge(i)],...
        [sf20.meanPerfSmall(i) sf20.meanPerfLarge(i)],...
        {string(sf20.numPerfSmall(i)), string(sf20.numPerfLarge(i))}); 
   
    hold on

    l(2) = plot([sf6.meanDCSmall(i) sf6.meanDCLarge(i)],...
        [sf6.meanPerfSmall(i) sf6.meanPerfLarge(i)],...
        '-d','Color',colr(i,:),'MarkerSize',20);
    text([sf6.meanDCSmall(i) sf6.meanDCLarge(i)],...
        [sf6.meanPerfSmall(i) sf6.meanPerfLarge(i)],...
        {string(sf6.numPerfSmall(i)), string(sf6.numPerfLarge(i))});
   
    hold on
    title('Performance by DC')
end
legend(l,{'20 cpd','6cpd','Fix'})
ylabel('Prop Correct')
xlabel('DC')
%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
subplot(1,2,1)
for i = [3 4 5 6]
    plot([1.1 2.1],...
        [sf20.meanPerfSmall(i) sf20.meanPerfLarge(i)],...
        '-o','Color',colr(i,:),'MarkerFaceColor',colr(i,:),...
         'LineWidth',3);
%      text([1 2],...
%         [sf20.meanPerfSmall(i) sf20.meanPerfLarge(i)],...
%         {string(sf20.numPerfSmall(i)), string(sf20.numPerfLarge(i))});
    forTest120(i) = sf20.meanPerfSmall(i);
    forTest220(i) = sf20.meanPerfLarge(i);
    hold on
end

[E1, E2, t, p, hh, E] = T_Test(sf20.meanPerfSmall(3:6)', sf20.meanPerfLarge(3:6)');
[h,p,ci] = ttest(sf20.meanPerfSmall(3:6)', sf20.meanPerfLarge(3:6)');

% notBoxPlot([sf20.meanPerfSmall(3:6)' sf20.meanPerfLarge(3:6)'])
errorbar([1 2],[mean(sf20.meanPerfSmall(3:6)) mean(sf20.meanPerfLarge(3:6))],...
    [E/2 E/2],...
    '-o','Color','k','MarkerFaceColor',...
    'k','LineWidth',5,'MarkerSize',10);
% temp1 = [sf20.meanPerfSmall(3:6)' sf20.meanPerfLarge(3:6)'];
% q = quantile(temp1,0.95)
% boxplot(temp1,'PlotStyle','compact')

[h,p1] = ttest(forTest120([3 4 5 6]), forTest220([3 4 5 6]))
xticks([1 2])
xlim([0.5 2.5])
ylim([.5 1.15]);
xticklabels({'Small DC','Large DC'})
title('20cpd') 
yticks([.5:.1:1]);
text(1.2,1.1,sprintf('p = %.3f',p1),'FontSize',14);
line([1 2],[1.05 1.05],'Color','k');
ylabel('Proportion Correct')
set(gca,'FontSize',14);

subplot(1,2,2)
for i = [3 4 5 6]
    plot([1.1 2.1],...
        [sf6.meanPerfSmall(i) sf6.meanPerfLarge(i)],...
        '-o','Color',colr(i,:),'MarkerFaceColor',colr(i,:),...
        'LineWidth',3);
%     text([1 2],...
%         [sf6.meanPerfSmall(i) sf6.meanPerfLarge(i)],...
%         {string(sf6.numPerfSmall(i)), string(sf6.numPerfLarge(i))});
    forTest16(i) = sf6.meanPerfSmall(i);
    forTest26(i) = sf6.meanPerfLarge(i);
    hold on
end
% x1 = abs(ci95(sf6.meanPerfSmall(3:6))- mean(sf6.meanPerfSmall(3:6)));
% x2 = abs(ci95(sf6.meanPerfLarge(3:6))- mean(sf6.meanPerfLarge(3:6)));
[E1, E2, t, p, hh, E] = T_Test(sf6.meanPerfSmall(3:6)', sf6.meanPerfLarge(3:6)');
[h,p,ci] = ttest(sf6.meanPerfSmall(3:6)', sf6.meanPerfLarge(3:6)');
% figure;
temp = [sf6.meanPerfSmall(3:6)' sf6.meanPerfLarge(3:6)'];
% boxplot(temp,'PlotStyle','compact');


errorbar([1 2],[mean(sf6.meanPerfSmall(3:6)) mean(sf6.meanPerfLarge(3:6))],...
    [E/2 E/2],...
    '-o','Color','k', 'MarkerFaceColor','k','LineWidth',5,...
    'MarkerSize',10);
[h,p2] = ttest(forTest16([3:6]), forTest26([3:6]));
% [h,p2] = ttest([0.662337662337662,0.562500000000000,0.794117647058824,0.766666666666667],...
% %     [0.684210526315790,0.642857142857143,0.860000000000000,0.851851851851852]);
xticks([1 2])
xlim([.5 2.5])
ylim([.5 1.15]);
xticklabels({'Small DC','Large DC'})
yticks([.5:.1:1]);
title('6cpd') 
text(1.2,1.1,sprintf('p = %.3f',p2),'FontSize',14);
line([1 2],[1.05 1.05],'Color','k');
set(gca,'FontSize',14);
saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\SummaryDocuments\ManuscriptDocs\Manuscript.VisualAcuity.Clark2020\OverleafDoc\figures\CPDDifferences.epsc');
saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\SummaryDocuments\ManuscriptDocs\Manuscript.VisualAcuity.Clark2020\OverleafDoc\figures\CPDDifferences.png');

%% %bootstrapping performance
% x = tempDataStruct.subjectThreshUnc(ii).em.allTraces.x;
%     y = tempDataStruct.subjectThreshUnc(ii).em.allTraces.y;
%     
%     st500 = struct('sampling', params.sampling(ii), ...
%         'x', x, ...
%         'y', y);
% 
%     fprintf('Starting DC Boots Analysis for %s', subjectsAll{ii});
%     capable = @(z)(bootCalculateDiffusionCoef(z));  % Process capability
%     tic
%     [stats500{ii}.ci, stats500{ii}.info] = bootci(100,capable,st500);
%     toc
%     
% %     stats500 = bootstrp(2,@bootCalculateDiffusionCoef,st500);

for i = 3:6
    capable = @(z)(mean(z));
    
    y = cell2mat(sf6.numPerfVecS{i});
    sf6.bootSmall{i} = (bootstrp(1000,@mean,y));
    sf6.bootSmallCI{i} = bootci(1000,capable,y)';
    
    y = cell2mat(sf6.numPerfVecL{i});
    sf6.bootLarge{i} = (bootstrp(1000,@mean,y));
    sf6.bootLargeCI{i} = bootci(1000,capable,y)';
    
    y = cell2mat(sf20.numPerfVecS{i});
    sf20.bootSmall{i} = (bootstrp(1000,@mean,y));
    sf20.bootSmallCI{i} = bootci(1000,capable,y);
    
    y = cell2mat(sf20.numPerfVecL{i});
    sf20.bootLarge{i} = (bootstrp(1000,@mean,y));
    sf20.bootLargeCI{i} = bootci(1000,capable,y)';
end


figure;
subplot(1,2,1)

for i = [3 4 5 6]
    yneg1 = mean(sf6.bootSmall{i})-sf6.bootSmallCI{i}(1);
    ypos1 = sf6.bootSmallCI{i}(2)-mean(sf6.bootSmall{i});
    
    yneg2 = mean(sf6.bootLarge{i})-sf6.bootLargeCI{i}(1);
    ypos2 = sf6.bootLargeCI{i}(2)-mean(sf6.bootLarge{i});
    
    errorbar([1+0.3*i 4.1+0.3*i],...
        [mean(sf6.bootSmall{i}') ...
            mean(sf6.bootLarge{i}')],...
             [yneg1 yneg2],[ypos1 ypos2],...
        '-o','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
%     vals(i,:) = [sf6.meanPerfSmall(i) sf6.meanPerfLarge(i)];
    hold on
end
%     errorbar([2.5 5.5],...
%         [mean(vals([3:6],1)'), mean(vals([3:6],2)')],...
%         [sem(vals([3:6],1)'), sem(vals([3:6],2)')],...
%         '-o','Color','k','MarkerFaceColor','k','MarkerSize',10);
%     hold on

ylim([.25 1])
ylabel('Bootstrapped Prop Correct');
xticks([2.5 5.5])
xlim([1.5 6.5])
xticklabels({'Small DC','Large DC'});
title ('6cpd');

subplot(1,2,2)
for i = [3 4 5 6]
    yneg1 = mean(sf20.bootSmall{i})-sf20.bootSmallCI{i}(1);
    ypos1 = sf20.bootSmallCI{i}(2)-mean(sf20.bootSmall{i});
    
    yneg2 = mean(sf20.bootLarge{i})-sf20.bootLargeCI{i}(1);
    ypos2 = sf20.bootLargeCI{i}(2)-mean(sf20.bootLarge{i});
    
    errorbar([1+0.3*i 4.1+0.3*i],...
        [mean(sf20.bootSmall{i}') ...
            mean(sf20.bootLarge{i}')],...
             [yneg1 yneg2],[ypos1 ypos2],...
        '-o','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
    hold on
end
%     errorbar([2.5 5.5],...
%         [mean(vals([3:6],1)'), mean(vals([3:6],2)')],...
%         [sem(vals([3:6],1)'), sem(vals([3:6],2)')],...
%         '-o','Color','k','MarkerFaceColor','k','MarkerSize',10);
%     hold on
% [h,p2] = ttest(sf20.bootSmall{i}', sf20.bootLarge{i}');
ylim([.25 1])
ylabel('Bootstrapped Prop Correct');
xticks([2.5 5.5])
xlim([1.5 6.5])
xticklabels({'Small DC','Large DC'});
title('20 cpd')


