function swIdx = determineSameSWCandU(ii)
switch ii
    case 1
        swIdx = 1.5;
    case 2
        swIdx = 1.5;
    case 3
        swIdx = 2;
    case 4
        swIdx = 2;
    case 5
        swIdx = 1.5;
    case 6
        swIdx = 1.8;
    case 7
        swIdx = 1.8;
    case 8
        swIdx = 1.8;
    case 9
        swIdx = 1.9;
    case 10
        swIdx = 1.9;
    case 11
        swIdx = 2.2;
    case 12
        swIdx = 1.9;
    case 13
        swIdx = 3;
end