%%% Script for Analyzing Huxlin Data and Creating Manuscript Figures
clear all
temp = load('dataTableStroke.mat');

tableSubjectInfo = struct2table(temp.dataTable); %% Also Table 1
%% Figure 2A - Heatmaps for Fixation
figure;
counter = 1;
for ii = (find(tableSubjectInfo.Stroke == 1))' %Patients
    subplot(6,3,counter)
    heatMapIm = generateHeatMapSimple( ...
        tableSubjectInfo.fixX{ii}, ...
        tableSubjectInfo.fixY{ii}, ...
        'Bins', 30,...
        'StimulusSize', 0,...
        'AxisValue', 40,...
        'Uncrowded', 4,...
        'Borders', 0);
    hold on
    line([0 0],[-40 40],'LineStyle','--','Color','k','LineWidth',3);
    line([-40 40],[0 0],'LineStyle','--','Color','k','LineWidth',3);
    axis square
    counter = counter + 1;
end

figure;
counter = 1;
for ii = (find(tableSubjectInfo.Stroke == 0))' %Controls
    subplot(6,3,counter)
    heatMapIm = generateHeatMapSimple( ...
        tableSubjectInfo.fixX{ii}, ...
        tableSubjectInfo.fixY{ii}, ...
        'Bins', 30,...
        'StimulusSize', 0,...
        'AxisValue', 40,...
        'Uncrowded', 4,...
        'Borders', 0);
    hold on
    line([0 0],[-40 40],'LineStyle','--','Color','k','LineWidth',3);
    line([-40 40],[0 0],'LineStyle','--','Color','k','LineWidth',3);
    axis square
    counter = counter + 1;
end
%% Figure 2B - BCEA for Fixation

figure;
boxplot(tableSubjectInfo.bceaFix,...
    tableSubjectInfo.Stroke','PlotStyle','compact');

set(gca, 'YScale', 'log')
ylim([.05 10])
yticks([.3 1 2 10])
xticks([1 2])
xticklabels({'Control','Patient'});

[h,p] = ttest2(tableSubjectInfo.bceaFix(tableSubjectInfo.Stroke == 1),...
    tableSubjectInfo.bceaFix(tableSubjectInfo.Stroke == 0));



%% Figure 3
counter = 1;
figure;
for ii = 1:height(tableSubjectInfo)
    if tableSubjectInfo.Stroke(ii) == 0
        continue;
    end
    rotateAngle = tableSubjectInfo.AngleOfLoss{ii};
    if ii == 1
        rotateAngle = 315;
    elseif ii == 2
        rotateAngle = 50;
    elseif ii == 3
        rotateAngle = 145;
    elseif ii == 4
        rotateAngle = 90;
    elseif ii == 10
        rotateAngle = 90;
    elseif ii == 22
        rotateAngle = 315;
    elseif ii == 23
        rotateAngle = 90;
    end
    
    
    subplot(4,4,counter)
    temp = tableSubjectInfo.vfMap{ii};
    temp(isnan(temp)) = -20;
    
    
    J = imrotate((temp),rotateAngle);
    J(find(J == -20)) = max(max(J));
    
    imagesc(J);
    axis square;
    set(gca,'XTick',[],'YTick',[])
    
    colormap((gray))
    if ii == 1
        ylabel('Degrees')
        xlabel('Degrees')
        yticks([1 420])
        xticks([1 540])
        xticklabels({'-30','30'})
        yticklabels({'-30','30'})
    else
        yticks([1 420])
        xticks([1 540])
        xticklabels({'',''})
        yticklabels({'',''})
    end
    
    title(sprintf('P%i',ii));
    
    counter = counter + 1;
    
end

counter = 1;
figure;
for ii = 1:height(tableSubjectInfo)
    if tableSubjectInfo.Stroke(ii) == 0
        continue;
    end
    rotateAngle = tableSubjectInfo.AngleOfLoss{ii};
    if ii == 1
        rotateAngle = 315;
    elseif ii == 2
        rotateAngle = 50;
    elseif ii == 3
        rotateAngle = 145;
    elseif ii == 4
        rotateAngle = 90;
    elseif ii == 10
        rotateAngle = 90;
    elseif ii == 22
        rotateAngle = 315;
    elseif ii == 23
        rotateAngle = 90;
    end
    
    axisVal = 30;
    
    [h, xT] = unique(tableSubjectInfo.fixX{ii});
    v = [tableSubjectInfo.fixX{ii}(xT);tableSubjectInfo.fixY{ii}(xT)]';
    theta = -rotateAngle;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    vRTemp = (v*R);
    idx = find(vRTemp(:,1) < axisVal & vRTemp(:,1) > -axisVal...
        & vRTemp(:,2) < axisVal & vRTemp(:,2) > -axisVal);
    vR = vRTemp(idx,:);
    
    subplot(4,4,counter)
    generateHeatMapSimple( ...
        vR(:,1)', ...
        vR(:,2)', ...
        'Bins', 30,...
        'StimulusSize', 16,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    
    hold on
    plot([-axisVal axisVal], [0 0], '--k', 'LineWidth',2)
    plot([0 0], [-axisVal axisVal], '--k', 'LineWidth',2)
    
    
    if ii == 1
        ylabel('Degrees')
        xlabel('Degrees')
        
    else
        yticks([1 420])
        xticks([1 540])
        xticklabels({'',''})
        yticklabels({'',''})
    end
    
    title(sprintf('P%i',ii));
    axis square
    counter = counter + 1;
    
end


%% Figure 4A
figure;
subplot(1,2,1)
patientsAllX = [];
patientsAllY = [];

for ii = find(tableSubjectInfo.Stroke == 1)'
    rotateAngle = tableSubjectInfo.AngleOfLoss{ii};
    if ii == 1
        rotateAngle = 315;
    elseif ii == 2
        rotateAngle = 50;
    elseif ii == 3
        rotateAngle = 145;
    elseif ii == 4
        rotateAngle = 90;
    elseif ii == 10
        rotateAngle = 90;
    elseif ii == 22
        rotateAngle = 315;
    elseif ii == 23
        rotateAngle = 90;
    end
%     rotateAngle = 0;
    
    v = [tableSubjectInfo.fixationGroupX{ii};tableSubjectInfo.fixationGroupY{ii}]';
    theta = -rotateAngle;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    vR = v*R;
    
    patientsAllY = [patientsAllY vR(:,2)'];
    patientsAllX = [patientsAllX vR(:,1)'];
    axisVal = 20;
    hold on
    ellipseXY(vR(:,1)', vR(:,2)', 68, [150 150 150]/255,0)
    axis([-axisVal axisVal -axisVal axisVal])
    
    hold on
    plot(0,0,'o','MarkerSize',1,'LineWidth',3,'MarkerEdgeColor','k');
    line([0 0],[-axisVal axisVal],'Color','k','LineWidth',1,'LineStyle',':')
    line([-axisVal axisVal],[0 0],'Color','k','LineWidth',1,'LineStyle',':')
    
    axis square;
    
    set(gca,'XTick',[],'YTick',[],'xlabel',[],'ylabel',[])
    %         title(sprintf('%s',subjectsAll{ii}));
    
end
title('Patients')
ellipseXY(patientsAllX, patientsAllY, 65, 'm' ,0)
plot(mean(patientsAllX),mean(patientsAllY),'d','Color','m')

subplot(1,2,2)
patientsAllX = [];
patientsAllY = [];

for ii = find(tableSubjectInfo.Stroke == 0)'
   
    rotateAngle = 0;
    
    v = [tableSubjectInfo.fixationGroupX{ii};tableSubjectInfo.fixationGroupY{ii}]';
    theta = -rotateAngle;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    vR = v*R;
    
    patientsAllY = [patientsAllY vR(:,2)'];
    patientsAllX = [patientsAllX vR(:,1)'];
    axisVal = 20;
    hold on
    ellipseXY(vR(:,1)', vR(:,2)', 68, [150 150 150]/255,0)
    axis([-axisVal axisVal -axisVal axisVal])
    
    hold on
    plot(0,0,'o','MarkerSize',1,'LineWidth',3,'MarkerEdgeColor','k');
    line([0 0],[-axisVal axisVal],'Color','k','LineWidth',1,'LineStyle',':')
    line([-axisVal axisVal],[0 0],'Color','k','LineWidth',1,'LineStyle',':')
    
    axis square;
    
    set(gca,'XTick',[],'YTick',[],'xlabel',[],'ylabel',[])
    %         title(sprintf('%s',subjectsAll{ii}));
    
end
title('Controls')
ellipseXY(patientsAllX, patientsAllY, 65, 'm' ,0)
plot(mean(patientsAllX),mean(patientsAllY),'d','Color','m')

%% Figure3B and C

[pY_FullFixation, meanValsCL, meanValsPT, meansValsIndY] = ...
    histogramNormalizedForRotation(1:26, ~tableSubjectInfo.Stroke, ...
    tableSubjectInfo.allRotated(find(tableSubjectInfo.Stroke)),...
    tableSubjectInfo.allRotated(find(~tableSubjectInfo.Stroke)), 2, 0); %x = 1 or y = 2

[pY_500Fixation, meanValsCL500Y, meanValsPT500Y, meansValsInd500Y] = ...
    histogramNormalizedForRotation(1:26, ~tableSubjectInfo.Stroke, ...
    tableSubjectInfo.allRotated500(find(tableSubjectInfo.Stroke)),...
    tableSubjectInfo.allRotated500(find(~tableSubjectInfo.Stroke)), 2, 0); %x = 1 or y = 2


figure;
subplot(3,2,1)
    errorbar([0 1],[meanValsCL.mean meanValsPT.mean],[meanValsCL.sem meanValsPT.sem],...
        '-o');
    xlim([-.2 1.2])
    ylim([-5 5])
    title('Y')
    ylabel('Full Fixation')
subplot(3,2,3)
    errorbar([0 1],[meanValsCL500Y.mean meanValsPT500Y.mean],[meanValsCL500Y.sem meanValsPT500Y.sem],...
        '-o');
    xlim([-.2 1.2])
    ylim([-5 5])
    title('Y');
    ylabel('500ms Fixation')


%% Figure 4 MS Fixation
load('msAllAnalysis.mat')    
load('rotatedAligned.mat');
    
values = [];
msThreshold = 5;
rotateExtra = 0;
majorAxis = 0:15:330;

ii = 1;
for t = (find(msAllAnalysis.PT == 0))
    theta_deg = []; 
    theta_radians = [];
    theta_deg = mod(msAllAnalysis.fixation{t}.msAngleCorrected(...
        msAllAnalysis.fixation{t}.msAmpCorrected > msThreshold),360);
    theta_radians = deg2rad(theta_deg);
    for i = 1:length(majorAxis)
        if majorAxis(i) == 0
            vecVals{ii,i} = (theta_deg < 15 & theta_deg >= 0) | ...
                theta_deg > 345;
        else
            vecVals{ii,i} = (theta_deg < majorAxis(i) + 15 &...
                theta_deg > majorAxis(i)-15);
        end
        values(ii,i) = length(find(vecVals{ii,i}));
        SEMVal(ii,i) = sem(normalizeVector(theta_deg(theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15)));
    end
    ii = 1+ii;
end
rho = normalizeVector(mean(values));% (values(1))]); % change this vector (it is a vector of 8, and then add the first value to the end  to connect the polar plot )

msNum.Controls = values;
msNum.Angles = majorAxis;
ii = 1;
for t = (find(tableSubjectInfo.Stroke))'
    theta_deg = []; 
    theta_radians = [];
    theta_deg = mod(rotatedAligned.msAngleCorrectedFix{t}(...
        msAllAnalysis.fixation{t}.msAmpCorrected > msThreshold),360)'+rotateExtra;
    if rotateExtra ~= 0
        idx = (find(theta_deg>360)); theta_deg(idx) = theta_deg(idx) - 360;
    end
       theta_degNonRot = mod(msAllAnalysis.fixation{t}.msAngleCorrected(...
        msAllAnalysis.fixation{t}.msAmpCorrected > msThreshold),360)';
    
    theta_radians = deg2rad(theta_deg);
    for i = 1:length(majorAxis)
         if majorAxis(i) == 0
            vecVals2{ii,i} = ((theta_deg < 15 & theta_deg >= 0) | ...
                theta_deg > 345);
        else
            vecVals2{ii,i} = (theta_deg < majorAxis(i) + 15 &...
                theta_deg > majorAxis(i)-15);
        end

        values2(ii,i) = length(find(vecVals2{ii,i}));       
        vecVals3{ii,i} = (theta_degNonRot < majorAxis(i) + 15 &...
            theta_degNonRot > majorAxis(i)-15);
        values3(ii,i) = length(find(vecVals3{ii,i}));
        SEMVal(ii,i) = sem((theta_deg(theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15)));
    end
    ii = 1+ii;
end
hold on
rho2 = normalizeVector(mean(values2));
msNum.Patients = values2;
msNum.notRotatedPatients = values3;

% P = msNum.notRotatedPatients./max(msNum.notRotatedPatients')';
P = msNum.Patients./max(msNum.Patients')';
C = msNum.Controls./max(msNum.Controls')';

towardsBF = nanmean([P(:,6) P(:,7) P(:,8)]);
awayBF = nanmean([P(:,18) P(:,19) P(:,20)]);
[h,p,~,stats] = ttest([P(:,6)' P(:,7)' P(:,8)'], [P(:,18)' P(:,19)' P(:,20)']);
figure;
errorbar([0 1],[mean([P(:,6)' P(:,7)' P(:,8)']) mean([P(:,18)' P(:,19)' P(:,20)'])],...
    [sem([P(:,6)' P(:,7)' P(:,8)']) sem([P(:,8)' P(:,19)' P(:,20)'])],'-o');
ylabel('Normalized Probability of MS Direction');
xticks([0 1])
xlim([-.25 1.25])
xticklabels({'Towards BF','Away from BF'});
title('p = ',round(p,3))
set(gca,'FontSize',14);

mean([P(:,6)' P(:,7)' P(:,8)'])
std([P(:,6)' P(:,7)' P(:,8)'])
mean(([P(:,18)' P(:,19)' P(:,20)']))
std(([P(:,18)' P(:,19)' P(:,20)']))


figure;
temp = nanmean(msNum.notRotatedPatients./max(msNum.notRotatedPatients')');

polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [temp temp(1)],...
    'b','MarkerSize', 30);
hold on
tempS = (sem(msNum.notRotatedPatients./max(msNum.notRotatedPatients')')/2);
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [tempS+temp tempS(1)+temp(1)],...
    '--b','MarkerSize', 30);
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [temp-tempS temp(1)-tempS(1)],...
    '--b','MarkerSize', 30);
hold on
temp2 = mean(msNum.Controls./max(msNum.Controls')');
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),[temp2 temp2(1)],...
    'r','MarkerSize', 30);
tempS2 = (sem(msNum.Controls./max(msNum.Controls')')/2);
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [tempS2+temp2 tempS2(1)+temp2(1)],...
    '--r','MarkerSize', 30);
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [temp2-tempS2 temp2(1)-tempS2(1)],...
    '--r','MarkerSize', 30);

[h,p] = ttest2(msNum.notRotatedPatients,msNum.Controls);
[~,~,stats] = anovan(mean([msNum.notRotatedPatients',...
       msNum.Controls']),{~tableSubjectInfo.Stroke});
multcompare(stats)

[h,p] = ttest2(P,C);
polarplot(deg2rad(msNum.Angles(find(h))),ones(1,length(find(h))),...
    '*','Color','k');

[~,~,stats] = anovan([mean([msNum.notRotatedPatients]),...
       mean([msNum.Controls])],{[ones(1,23),ones(1,23)*2]});
multcompare(stats)
    
%% Figure 4 Drift Fixation
load('huxlinDriftFixation.mat')
subNum = height(tableSubjectInfo);

majorAxis = 0:15:330;

P = dNum.Patients./max(dNum.Patients')';
C = dNum.Controls./max(dNum.Controls')';

figure;
temp = [];
temp = flip(mean(dNum.Patients));

figure;
pie(mean(dNum.Patients),(string(majorAxis)))
ax = gca;
ax.View = [110 90];

towardsBF = nanmean([P(:,3) P(:,4) P(:,5)]);
awayBF = nanmean([P(:,18) P(:,19) P(:,20)]);


[h,p] = ttest([P(:,3)',P(:,4)',P(:,5)'], ...
   [P(:,18)' P(:,19)' P(:,20)']);

figure;
errorbar([0 1],[nanmean([P(:,6)',P(:,7)',P(:,8)']) ...
    nanmean([P(:,18)' P(:,19)' P(:,20)'])],...
    [sem([P(:,3)',P(:,4)',P(:,5)']) ...
    sem([P(:,18)' P(:,19)' P(:,20)'])],'-o',...
    'Color','k','MarkerFaceColor','k','MarkerSize',10);

ylabel('Normalized Probability of Drift Direction');
xticks([0 1])
xlim([-.25 1.25])
xticklabels({'Towards BF','Away from BF'});
title('p = ',round(p,3))
set(gca,'FontSize',14);

%% Figure 5A
figure;
counter = 1;
for ii = (find(tableSubjectInfo.Stroke == 1))' %Patients
    if ii == 3 %% this subject didnt do the task
        continue;
    end
    subplot(6,3,counter)
    heatMapIm = generateHeatMapSimple( ...
        tableSubjectInfo.taskX{ii}, ...
        tableSubjectInfo.taskY{ii}, ...
        'Bins', 10,...
        'StimulusSize', 0,...
        'AxisValue', 20,...
        'Uncrowded', 4,...
        'Borders', 0);
    hold on
    line([0 0],[-40 40],'LineStyle','--','Color','k','LineWidth',3);
    line([-40 40],[0 0],'LineStyle','--','Color','k','LineWidth',3);
    axis square
    counter = counter + 1;
end

figure;
counter = 1;
for ii = (find(tableSubjectInfo.Stroke == 0))' %Controls
    subplot(6,3,counter)
    heatMapIm = generateHeatMapSimple( ...
        tableSubjectInfo.taskX{ii}, ...
        tableSubjectInfo.taskY{ii}, ...
        'Bins', 10,...
        'StimulusSize', 0,...
        'AxisValue', 20,...
        'Uncrowded', 4,...
        'Borders', 0);
    hold on
    line([0 0],[-40 40],'LineStyle','--','Color','k','LineWidth',3);
    line([-40 40],[0 0],'LineStyle','--','Color','k','LineWidth',3);
    axis square
    counter = counter + 1;
end
%% Figure 5B - BCEA for Task

idx = find(tableSubjectInfo.Acuity < 60);

figure;
boxplot(tableSubjectInfo.bceaTask,...
    tableSubjectInfo.Stroke','PlotStyle','compact');

set(gca, 'YScale', 'log')
ylim([.02 1])
yticks([.03 .1 .3 1])
xticks([1 2])
xticklabels({'Control','Patient'});
idx2 = (tableSubjectInfo.Acuity < 60);

[h,p] = ttest2(tableSubjectInfo.bceaTask(tableSubjectInfo.Stroke == 1& idx2),...
    tableSubjectInfo.bceaTask(tableSubjectInfo.Stroke == 0& idx2));


%% Figure 5B - Acuity for Task

idx = find(tableSubjectInfo.Acuity < 60);

figure;
boxplot(tableSubjectInfo.Acuity(idx),...
    tableSubjectInfo.Stroke(idx)','PlotStyle','compact');

set(gca, 'YScale', 'log')
% ylim([.05 10])
% yticks([.3 1 2 10])
xticks([1 2])
xticklabels({'Control','Patient'});

idx2 = (tableSubjectInfo.Acuity < 60);

[h,p] = ttest2(tableSubjectInfo.Acuity(tableSubjectInfo.Stroke == 1 & idx2),...
    tableSubjectInfo.Acuity(tableSubjectInfo.Stroke == 0 & idx2));

%% Figure 6A&B

[pY_Task, meanValsCLT, meanValsPTT, temp] = ...
    histogramNormalizedForRotation(1:26, ~tableSubjectInfo.Stroke, ...
    tableSubjectInfo.allRotatedTask(find(tableSubjectInfo.Stroke)), ...
    tableSubjectInfo.allRotatedTask(find(~tableSubjectInfo.Stroke)), 2, 1); %x = 1 or y = 2

% [pX_Task, meanValsCLTX, meanValsPTTX] = histogramNormalizedForRotation(subjectsAll, control, ...
%     allRotatedTask, allControlTask, 1, 1); %x = 1 or y = 2
 
    
%% functions

function [p, meanValsCL, meanValsPT, meanVals] = ...
    histogramNormalizedForRotation(subjectsAll, control, ...
    rotatedValsPT, rotatedValsControl, coordVal, task)

figure;
    hold on
    for ii = 1:length(subjectsAll)
        if ii == 3 && task == 1
            meanVals(ii) = NaN;
            continue;
        end
        if find(control(ii))
            allHistOut{ii} = histogram(rotatedValsControl{ii}(:,coordVal)','BinLimits',[-60 60],...
                'BinEdges', [-60:0.5:60]);
%             meanVals(ii) = allControl{ii};
        else
            allHistOut{ii} = histogram(rotatedValsPT{ii}(:,coordVal)','BinLimits',[-60 60],...
                'BinEdges', [-60:0.5:60]);
%             meanVals(ii) = allRotated{ii}(:,2)';
        end
        meanVals(ii) = mean(allHistOut{ii}.Data);
        temp = find(allHistOut{ii}.Values == max(allHistOut{ii}.Values));
        if length(temp) > 1
            maxBin(ii) = temp(1);
        else
            maxBin(ii) = temp;
        end
        valBin(ii) = allHistOut{ii}.BinEdges(maxBin(ii));
    end
    
    [h,p] = ttest2(meanVals(control == 1), meanVals(control == 0));
    
    %%plot meaned new histogram
    for ii = 1:length(subjectsAll)
        if strcmp(subjectsAll{ii},'HUX5') && task == 1
            meanBinNorm(ii,:) = NaN;
             continue;
         end
        for i = 1:length(allHistOut{1}.BinEdges)-1
            meanBin(ii,i) = (allHistOut{ii}.Values(i));
        end
        meanBinNorm(ii,:) = normalizeVector(meanBin(ii,:));
    end
%     close figure 20
    
    figure; %normalizes
    if task == 1
        figsHist(1) = plot(allHistOut{1}.BinEdges(1:end-1), ...
            normalizeVector(mean(meanBinNorm(control == 1,:))),...
            'r');
        hold on
        figsHist(2) =plot(allHistOut{1}.BinEdges(1:end-1), ...
            normalizeVector(nanmean(meanBinNorm(control == 0,:))),...
            'b');
    else
        figsHist(1) = plot(allHistOut{1}.BinEdges(1:end-1), ...
            mean(meanBinNorm(control == 1,:)),...
            'r');
        hold on
        figsHist(2) =plot(allHistOut{1}.BinEdges(1:end-1), ...
            mean(meanBinNorm(control == 0,:)),...
            'b');
    end
    line([mean(meanVals(control == 1)) mean(meanVals(control == 1))],...
        [0 1],'Color','r');
    line([nanmean(meanVals(control == 0)) nanmean(meanVals(control == 0))],...
        [0 1],'Color','b');
    legend(figsHist,{'Control','Patient'})
    ylabel('Normalized Bin Means');
    if coordVal == 1
        xlabel('X Gaze Position Rotated');
    else
        xlabel('Y Gaze Position Rotated');
    end
    
%     xlim([-45 45])
    title(sprintf(' p = %.3f',p));
    
    meanValsCL.mean = nanmean(meanVals(control == 1));
    meanValsCL.std = nanstd(meanVals(control == 1));
    meanValsCL.sem = sem(meanVals(control == 1));
    meanValsPT.mean = nanmean(meanVals(control == 0));
    meanValsPT.std = nanstd(meanVals(control == 0));
    meanValsPT.sem = sem(meanVals(control == 0));
end 

function [result,limit] = generateHeatMapSimple( xValues, yValues, varargin )
%Creates 2D Distribution Map of Traces
n_bins = 40;
axisValue = 40;
stimuliSize = 1;
offset = 0;
doubleTarget = 0;
condition = 0;
borders = 1;
rotateIm = 0;

k = 1;
Properties = varargin;
while k <= length(Properties) && ischar(Properties{k})
    switch (Properties{k})
        case 'Bins'
            n_bins =  Properties{k + 1};
            Properties(k:k+1) = [];
        case 'StimulusSize'
            stimuliSize = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'AxisValue'
            axisValue = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Offset'
            offset = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'DoubleTargets'
            doubleTarget = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Uncrowded' %(should be 1 for U, 2 for Crowded, 4 = Fixation)
            condition = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Borders'
            borders = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'Rotate'
            rotateIm = Properties{k + 1};
            Properties(k:k+1) = [];
        otherwise
            k = k + 1;
    end
end

idx = find(xValues > -axisValue & ...
    xValues < axisValue &...
    yValues > -axisValue & ...
    yValues < axisValue);

xValues = xValues(idx);
yValues = yValues(idx);

limit.xmin = floor(min(xValues));
limit.xmax = ceil(max(xValues));
limit.ymin = floor(min(yValues));
limit.ymax = ceil(max(yValues));

result = MyHistogram2(xValues, yValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
result = result./(max(max(result)));

if rotateIm ~= 0
    result = imrotate((result),rotateIm);
end

load('./MyColormaps.mat');
mycmap(1,:) = [1 1 1];
set(gcf, 'Colormap', mycmap)

hold on
temp = pcolor(linspace(limit.xmin, limit.xmax, size(result, 1)),...
    linspace(limit.ymin, limit.ymax, size(result, 1)),...
    result');

%width in arcminutes
if stimuliSize == 0
    if condition == 0
    else
        centerX = 8;
        centerY = -8;
        width = 16;
        height = 16;
        rectangle('Position',[-centerX+stimuliSize, centerY, width, height],'LineWidth',3,'LineStyle','-')
    end
else
    stimuliSize = stimuliSize/2;
    width = 2 * stimuliSize;
    height = width * 5;
    centerX = (-stimuliSize+offset);
    centerY = (-stimuliSize*5);
%     rectangle('Position',[centerX, centerY, width, height],'LineWidth',2)
    
    if doubleTarget
        % if uEcc(numEcc)> 0
        rectangle('Position',[-centerX+stimuliSize, centerY, width, height],'LineWidth',2,'LineStyle','--')
        % end
    end
    
    %     spacing = 1.4;
    %     arcminEcc = uEcc * params.pixelAngle;
    if condition == 2
        
        rectangle('Position',[-(width + (width * 1.4)) + offset, centerY, width, height],'LineWidth',1) %Right
        
        rectangle('Position',[(width * 1.4) + offset, centerY, width, height], 'LineWidth',1) %Left
        
        rectangle('Position',[centerX, -(height + (height * 1.4)), width, height], 'LineWidth',1) % Bottom
        
        rectangle('Position',[centerX, (height * 1.4), width, height], 'LineWidth',1) %Top
        
    end
    if condition == 1 || condition == 2
        rectangle('Position',[centerX, centerY, width, height],'LineWidth',3)
    end
    if condition == 4
        p = plot(0,0,'--ks','MarkerSize',stimuliSize);
        p(1).LineWidth = 3;
    end
end
% p(1).LineWidth = 3;

set(gca, 'FontSize', 12)

if borders == 0
    set(gca,'xtick',[], 'ytick', []);
end

caxis([.1 ceil(max(max(result)))])
shading interp; %interp/flat

axis([-axisValue axisValue -axisValue axisValue]);


end


