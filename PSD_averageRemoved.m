function [critFreq, power, spatial, powerDist] = PSD_averageRemoved(dc, pixelAnglel, widthAngle)
%% power spectra from drift (Brownian motion model) - removing the average for the stimulus
% dc = 20; % diffusion constant (arcmin^2/s)

%% Plot's individual power based on DC
f = linspace(1, 80, 80); % temporal frequencies (Hz)
k = logspace(-1, 2, 100); % spatial frequencies (cpd)

ps = Qfunction(f, k, dc / 60^2); % assumes isotropic

powerDist = [];

subplot(2, 2, 1);
pcolor(k, f, pow2db(ps));
colormap 'hot'; shading interp;
set(gca, 'XScale', 'log', 'YScale', 'log');
xlabel('spatial frqeuency (cpd)');
ylabel('temporal frequency (Hz)');
set(gca, 'XTick', [1, 10, 30, 50], 'YTick', [1, 10, 30, 80]);
title('PSD of BM drift');
axis square;
%

figure; clf;
subplot(2, 2, 2);
plot(k, pow2db(nansum(ps(f >=2 & f <= 40, :), 1)), 'k-', 'linewidth', 2);
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
ylabel('PSD (db)');
title('temporal power in RGC range');
xlabel('spatial frequency (cpd)');
axis square;

power.All = pow2db(nansum(ps(f >=2 & f <= 40, :), 1));
spatial.All = k;

[~,I] = max(pow2db(nansum(ps(f >=2 & f <= 40, :), 1)));
critFreq = k(I);

%% stimuli (drawing sanity check)
digits = [3, 5, 6, 9];
vertical = 1;

%% power spectra of uncrowded stimuli
% works best when pixelAngle and widthAngle of the digit result in an even
% number of pixels for the width of the digit

% pixelAngle = .25; % arcmin
% widthAngle = 1; % arcmin (angular width of digit) FIXED SW 1 FOR EVERYONE
% 
% widthPixel = widthAngle / pixelAngle;
% pixelAngle = round(pixelAngle,2);

pixelAngle = round(pixelAnglel/5,2)*5;
widthPixel = round2(widthAngle / pixelAngle, 2); % pixel - even number please!

bufferSizeAngle = 2 * 60; % arcmin
bufferSizePixel = bufferSizeAngle / pixelAngle;

sz = 2 * bufferSizePixel;
k = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);

% allSpec = cell(size(digits));

ca = [-40, 20];


%% power spectra from drift (Brownian motion model)
kdc = k;
psD = Qfunction(f, kdc, dc / 60^2); % assumes isotropic
Dps1 = mean(psD);

% for patternVS1 = 2
    for di = 1:length(digits)
        im = drawDigit(digits(di), widthPixel);
%         if patternVS1 == 1
%             temp = padarray(im-mean(im(:,size(im,2)/2)),[4,4],0,'both');
%             im_buffered = repmat(temp,bufferSizePixel,bufferSizePixel);
%         else
            if vertical == 1
                % buffer image and make it square
                if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
%                     im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel],0, 'both');
                    im_buffered = padarray(im-mean(im(:,size(im,2)/2)), [bufferSizePixel, bufferSizePixel],0, 'both');
                else
                    im_buffered = padarray(im-mean(im(:,(size(im,2)/2)+1)), [bufferSizePixel, bufferSizePixel],0, 'both');
                end
            else
                im_buffered = padarray(im-mean(im(round(size(im,1)/2)-1,:)), [bufferSizePixel, bufferSizePixel],0, 'both');
            end
%         end
        [m, n] = size(im_buffered);
        df = floor((m - sz)/2);
        im_buffered = im_buffered(df:(df + sz - 1), :);
        df = floor((n - sz)/2);
        im_buffered = im_buffered(:, df:(df + sz - 1));
        
        
        if vertical == 1
            if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
                spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)+1)));
                %[freq,Am,Ph]=ft_spect(im_buffered(:,(length(im_buffered)/2)+1),(pixelAngle/60))
            else
                spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)+2)));
                %[freq,Am,Ph]=ft_spect(im_buffered(:,(length(im_buffered)/2)+2),(pixelAngle/60))
            end
        else
            spec = fftshift(fft(im_buffered((length(im_buffered)/2)-1,:)));
        end
        
        
        DpsIm = spec.*Dps1;
        
        a = ((abs(spec)).^2);
        b = (mean(psD).^2);
        
        power.forPowerAmplification.k{di} = k;
        power.forPowerAmplification.digitPower{di,patternVS1} = pow2db((abs(spec)).^2);
        power.forPowerAmplification.driftPower = pow2db(mean(psD).^2);
        power.forPowerAmplification.amplifPower{di,patternVS1} = pow2db(a'.*b);
        power.forPowerAmplification.a{di,patternVS1} = (abs(spec)).^2;
        power.forPowerAmplification.b{di,patternVS1} =(mean(psD).^2);
        
        
        figure;
hold on
        semilogx(k,pow2db((abs(spec)).^2),'-');
%         ylim([-60 50])
    end
    
    
    %% add my Martina for phase
    % phase
    X2=spec;%store the FFT results in another array
    %detect noise (very small numbers (eps)) and ignore them
    threshold = (abs(X2))/10000; %tolerance threshold
    X2(abs(spec)<threshold) = 0; %maskout values that are below the threshold
    %*180/pi; %phase information multiply by 180/pi to convert to deg
    Phase_spec = (angle(X2));
    allSpec(di,:) = pow2db(abs(spec).^2);
    allPhase(di,:) = Phase_spec;
    plot_k = [2, 10, 30, 50];
    plot_k_index = interp1(k, 1:length(k), plot_k, 'n-mean(mean(im_buffered))earest');
    figure(10);
    semilogx(k(length(Phase_spec)/2:end), ((unwrap(Phase_spec(length(Phase_spec)/2:end)))), '-');
    hold on
    title('Phase of digits');
    xlim([0.5 60])
    xlabel('horizontal spatial frequency (cpd)');
    ylabel('phase deg');
    legend({'3','5','6', '9'}, 'Location', 'Best')
    figure(5)
    semilogx(k,  pow2db((abs(spec)).^2));
    hold on
    %semilogx(k,pow2db(mean(psD).^2),'r')
    a = ((abs(spec)).^2);
    b = (mean(psD).^2);
    %semilogx(k,pow2db(a'.*b),'g')
    title('average vertical');
    xlabel('spatial frequency (cpd)');
    %ylim([-10, 50]);
    xlim([0.5 60])
    legend({'3','5','6', '9'}, 'Location', 'Best')
    
% end
D1_P = circ_dist2(allPhase(1,:),allPhase(2,:));
D2_P = circ_dist2(allPhase(1,:)-allPhase(3,:));
D3_P = circ_dist2(allPhase(1,:)-allPhase(4,:));
D4_P = circ_dist2(allPhase(2,:)-allPhase(3,:));
D5_P = circ_dist2(allPhase(2,:)-allPhase(4,:));
D6_P = circ_dist2(allPhase(3,:)-allPhase(4,:)); %6 and 9
D1_A = allSpec(1,:)-allSpec(2,:);
D2_A = allSpec(1,:)-allSpec(3,:);
D3_A = allSpec(1,:)-allSpec(4,:);
D4_A = allSpec(2,:)-allSpec(3,:);
D5_A = allSpec(2,:)-allSpec(4,:);
D6_A = allSpec(3,:)-allSpec(4,:); %6 and 9
DP_var = var([D1_P;D2_P;D3_P;D4_P;D5_P;D6_P]);
DA_var = var([D1_A;D2_A;D3_A;D4_A;D5_A;D6_A]);
six_nineDP = var([D6_P]);
% six_nineDA = var([D6_A]);
allSpec(allSpec<0)= 0;
M_A = var(allSpec)-mean(allSpec);
M_P = circ_mean(allPhase);
% figure(4)
power.forPowerAmplification.phaseVarianceDiff = (DP_var*10)-mean(DP_var*10);

figure(4);
power.forPowerAmplification.phaseVarianceDiff = (DP_var*10)-mean(DP_var*10);

semilogx(k,(six_nineDP*10)-mean(six_nineDP*10), 'r', 'LineWidth', 2)
hold on
%semilogx(k,(DA_var/100), 'c', 'LineWidth', 2)
%semilogx(k,M_A/10, 'k', 'LineWidth', 2)
xlim([0.5 60])
title('Phase variance difference')
ylim([-10 20])
%% Power of horizontal and vertical Stimulus
close all;
figure;
for di = 1:length(digits)
    im = drawDigit(digits(di), widthPixel);
    
    % buffer image and make it square
    im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel], 1, 'both');
    [m, n] = size(im_buffered);
    df = floor((m - sz)/2);
    im_buffered = im_buffered(df:(df + sz - 1), :);
    df = floor((n - sz)/2);
    im_buffered = im_buffered(:, df:(df + sz - 1));
    
    
    %%%%%%%%% contrast reverse the image because fft pads with zeros
    % This may affect phase but does not have a major impact on power
    % except at (0, 0)
    im_buffered = 1 - im_buffered;
    
    spec = fftshift(fft2(im_buffered));
    
    %     allSpec{di} = struct('PS', spec, 'k', k);
    
    plot_k = [2, 10, 30, 50];
    plot_k_index = interp1(k, 1:length(k), plot_k, 'nearest');
    
    figure; %Heatmap of Number
    subplot(2, 2, di);
    imagesc(k, k, pow2db(abs(spec).^2));
    title(sprintf('PSD of %i', digits(di)));
    axis image;
    xlabel('horizontal spatial frequency (cpd)');
    ylabel('vertical spatial frequency (cpd)');
    colormap hot; colorbar;
    caxis(ca);
    %
    powerDist.k {di}= k;
    powerDist.pow2db{di} = pow2db(abs(spec).^2);
    powerDist.vertAverage{di} = pow2db(nanmean(abs(spec), 2).^2);
    powerDist.horizAverage{di} = pow2db(nanmean(abs(spec), 1).^2);
    
    %     figure; %Vertical vs Horizontal average power
    %     subplot(1, 2, 1);
    %     plot(k,pow2db(nanmean(abs(spec), 2).^2))
    %     subplot(1, 2, 2);
    %     plot(k,pow2db(nanmean(abs(spec), 1).^2))
    
end

    function digit = drawDigit(whichDigit, pixelwidth, patterned)
        % pixelwidth is ideally an even number
        digit = ones(pixelwidth*5, pixelwidth);
        strokewidth = pixelwidth/2;
        
        switch whichDigit
            case 3
                for ii = [1, 5.5 10]
                    si = (ii - 1)*strokewidth + 1;
                    ei = ii * strokewidth;
                    digit(si:ei, :) = 0;
                end
                
                digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
            case 5
                for ii = [1, 5.5 10]
                    si = (ii - 1)*strokewidth + 1;
                    ei = ii * strokewidth;
                    digit(si:ei, :) = 0;
                end
                digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
                digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
            case 6
                for ii = [1, 6:10]
                    si = (ii - 1)*strokewidth + 1;
                    ei = ii * strokewidth;
                    digit(si:ei, :) = 0;
                end
                
                digit(:, 1:strokewidth) = 0;
            case 9
                for ii = [1:5, 10]
                    si = (ii - 1)*strokewidth + 1;
                    ei = ii * strokewidth;
                    digit(si:ei, :) = 0;
                end
                digit(:, (strokewidth+1):(2*strokewidth)) = 0;
                % end
        end
        
        %     if exist('patterned','var')
        %         digit = [digit digit digit digit digit;
        %             digit digit digit digit digit];
        %     end
    end
end