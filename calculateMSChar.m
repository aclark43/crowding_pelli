function dataStruct = calculateMSChar(valueGroup,params,pptrials,em)

idx = valueGroup.idx;
strokeWidth = valueGroup.strokeWidth;
% strokeWidth = valueGroup.strokeWidth;
ecc = valueGroup.ecc;
stimulusSize = valueGroup.stimulusSize;

counter = 1;

for idxValue = 1:length(idx)
    i = idx(idxValue);
    if isempty(pptrials{i}.microsaccades.amplitude)
        continue;
    end
    
    dataStruct.position(counter).x = em.x(i);
    dataStruct.position(counter).y = em.y(i);
    
    dataStruct.msStartTime(counter) = {pptrials{i}.microsaccades.start};
    dataStruct.msEndTime(counter) = ...
        {pptrials{i}.microsaccades.duration + pptrials{i}.microsaccades.start - 1};
    
    xx = em.x{i};
    yy = em.y{i};
    
    microSaccadeStart = dataStruct.msStartTime{counter};
    microSaccadeEnd = dataStruct.msEndTime{counter};
    for s = 1:length(microSaccadeStart)
        if floor(microSaccadeStart(s)*(330/1000)) > length(xx) || ...
                floor(microSaccadeEnd(s)*(330/1000)) > length(xx)
            dataStruct.MicroSaccStartLoc(counter).x(s) = NaN;
            dataStruct.MicroSaccStartLoc(counter).y(s) = NaN;
            dataStruct.MicroSaccEndLoc(counter).x(s) = NaN;
            dataStruct.MicroSaccEndLoc(counter).y(s) = NaN;
            dataStruct.msAmplitude(counter) = NaN;%{pptrials{i}.microsaccades.amplitude};
            dataStruct.msAngle(counter) = NaN;%{pptrials{i}.microsaccades.angle};
        else
            startx = (xx(floor(max([microSaccadeStart(s)*(330/1000) 1]))));
            starty = (yy(floor(max([microSaccadeStart(s)*(330/1000) 1]))));
            endx = xx(floor(max([microSaccadeEnd(s)*(330/1000) 1])));
            endy = yy(floor(max([microSaccadeEnd(s)*(330/1000) 1])));
            
            dataStruct.MicroSaccStartLoc(counter).x(s) = startx;
            dataStruct.MicroSaccStartLoc(counter).y(s) = starty;
            dataStruct.MicroSaccEndLoc(counter).x(s) = endx;
            dataStruct.MicroSaccEndLoc(counter).y(s) = endy;
            
            dataStruct.msAmplitude(counter) = hypot(endx-startx, endy-starty); %{pptrials{i}.microsaccades.amplitude};
            x10 = startx;
            y10 = starty;
            x20 = endx;
            y20 = endy;
            dataStruct.msAngle(counter) = rad2deg(atan2(abs(x10*y20-x20*y10),x10*y10+x20*y20));%{pptrials{i}.microsaccades.angle};
        end
    end
    
    
    %     else
%         dataStruct.MicroSaccStartLoc.x = NaN;
%         dataStruct.MicroSaccStartLoc.y = NaN;
%         dataStruct.MicroSaccEndLoc.x = NaN;
%         dataStruct.MicroSaccEndLoc.y = NaN;
%     end

    counter = counter + 1;
end
[~, msRates] = buildRates(pptrials(valueGroup.idx));

dataStruct.microsaccadeRate = msRates;

% dataStruct.pattern = analyzeTask(idx, 0, 80, params, pptrials);

% em.msAnalysis.(strokeWidth) = dataStruct;
% msAnalysis.(strokeWidth).
end