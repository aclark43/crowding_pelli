function  dsqSanityChecks( fixation, drift, idx, params )
%Looks at the driftsegment vector and all the single segments 

% figure;
c = params.c;
subjectsAll = params.subjectsAll;

for ii = 1:10
    figure;

    if params.machine(ii) == 1
        sampRate = 1;
    else
        sampRate = (1000/330);
    end
    subplot(3,3,1)
    plot(drift.First200.dsqTime{1,ii}(1:200/sampRate),...
        drift.First200.dsqVec{1, ii}(1:200/sampRate),'o',...
        'Color','k')
    hold on
        x = drift.First200.dsqTime{1,ii}(1:200/sampRate); % Defines the domain as [-15,25] with a refinement of 0.25
        m  = drift.First200.dsqFit{ii};  % Specify your slope
        x1 = drift.First200.dsqTime{1,ii}(1); % Specify your starting x
        y1 = drift.First200.dsqVec{1, ii}(1);  % Specify your starting y
        y = m*(x - x1) + y1;    
    plot(x,y,'Color','r');
    ylabel('dsqVec')
    
    subplot(3,3,2)
    plot(drift.First200.dsqTime{1,ii},drift.First200.dsqSingleSegs{1, ii})
    hold on
    ylabel('Single Segments')
    subplot(3,3,3)
    plot(ii,drift.First200.dsq(1,ii),'o')
    hold on
    ylabel('Single Segments')
    %%%%%%%%%%%%%%%%%%%%%
    hold on
    subplot(3,3,4)
    plot(fixation.First200.dsqTime{1,ii},fixation.First200.dsqVec{1, ii},'o')
     hold on
        x = fixation.First200.dsqTime{1,ii}; % Defines the domain as [-15,25] with a refinement of 0.25
        m  = fixation.First200.dsqFit{ii};  % Specify your slope
        x1 = fixation.First200.dsqTime{1,ii}(1); % Specify your starting x
        y1 = fixation.First200.dsqVec{1, ii}(1);  % Specify your starting y
        y = m*(x - x1) + y1;    
    plot(x,y,'Color','r');
    ylabel('dsqVec')
    subplot(3,3,5)
    plot(fixation.First200.dsqTime{1,ii},fixation.First200.dsqSingleSegs{1, ii})
    hold on
    ylabel('Single Segments')
    subplot(3,3,6)
    plot(ii,fixation.First200.dsq(1,ii),'o')
    hold on

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    subplot(3,3,7)
    plot(drift.First500.dsqTime{1,ii},drift.First500.dsqVec{1, ii},'o','Color','r')
    hold on
        x = drift.First500.dsqTime{1,ii}; % Defines the domain as [-15,25] with a refinement of 0.25
        m  = drift.First500.dsqFit{ii};  % Specify your slope
        x1 = drift.First500.dsqTime{1,ii}(1); % Specify your starting x
        y1 = drift.First500.dsqVec{1, ii}(1);  % Specify your starting y
        y = m*(x - x1) + y1;
    
    hPlot = plot(x,y,'Color','k');
    ylabel('dsqVec')
    
    subplot(3,3,8)
    plot(drift.First500.dsqTime{1,ii},drift.First500.dsqSingleSegs{1, ii})
%     plot(1:500/sampRate,drift.First500.dsqSingleSegs{1, ii})
    hold on
    ylabel('Single Segments')
    subplot(3,3,9)
    plot(ii,drift.First500.dsq(1,ii),'o')
    ylabel('DC')
    hold on
%     title('500')
    suptitle(sprintf('%s', subjectsAll{ii}));
    
%     saveas(gcf,...
%     sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%s_dsqDetails.png', params.subjectsAll{ii}));
%     saveas(gcf,...
%     sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%s_dsqDetails.epsc', params.subjectsAll{ii}));
end

end

