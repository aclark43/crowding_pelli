function [ mSpeedSubj ] = speedAnalysis( subjectCondition, subjectThreshCondition, subjectsAll, condition , c, fig)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

figure
for ii = 1:length(subjectsAll)
    strokeValue =(subjectThreshCondition(ii).thresh);
    [~,thresholdIdx] = min(abs(strokeValue - subjectCondition(ii).stimulusSize));
    strokeWidth = sprintf('strokeWidth_%i',subjectCondition(ii).SW(thresholdIdx));
    %     idx = ~isnan(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).mn_speed{:})
    for idx = 1:length(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).mn_speed)
        speedSubj(idx) = (subjectThreshCondition(ii).em.ecc_0.(strokeWidth).mn_speed{idx});
    end
    mSpeedSubj(ii) = nanmean(speedSubj);
    %     scatter(curvSubj,strokeValue,100,'filled')
    
    %     plotSW = zeros(length(curvSubj), 1);
    %     plotSW(:) = strokeValue;
    %     plotSW = plotSW';
    
    errorBar(mSpeedSubj(ii),strokeValue)
    hold on
    points(ii) = scatter(mSpeedSubj(ii),strokeValue,100,c(ii,:),'filled');
end
xlabel('Speed(Mean)')
ylabel('Threshold Strokewidth')
title(sprintf('Speed,All Subjects,%s',condition{1}))
legend([points],subjectsAll)
hold on

saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjSpeed%s.png', condition{1}));


thresholds = [subjectThreshCondition.thresh];

figure
[~,p,~,r] = LinRegression(mSpeedSubj,thresholds,0,NaN,1,0);
for ii = 1:length(mSpeedSubj)
scatter(mSpeedSubj(ii), thresholds(ii),200,[c(ii,1) c(ii,2), c(ii,3)],'filled')
end
rValue = sprintf('r^2 = %.3f', r);
pValue = sprintf('p = %.3f', p);
text(40, 1, pValue,'Fontsize',10);
text(40,0.75,rValue,'Fontsize',10);
xlabel('Mean Speed at Threshold')
ylabel('Strokewidth Threshold')
graphTitle1 = 'Mean Speed by Threshold Strokewidth';
graphTitle2 = (sprintf('%s', cell2mat(condition)));
title({graphTitle1,graphTitle2})
xlim([min(mSpeedSubj) max(mSpeedSubj)])
ylim([0 4])
saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjSpeedbyStrokewidth%s.png', condition{1}));

end



