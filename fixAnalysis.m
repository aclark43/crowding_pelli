function [ fixationResult, nXValues, nYValues, t, fixationPPtrialsIdx, fullTime, fix ] = ...
    fixAnalysis( pptrials, uEcc, em, params, valid, trialChar, title_str )

eccentricity = sprintf('ecc_%i', round(uEcc));
strokeWidth = 'strokeWidth_ALL';
fixCounter = 1;
xAll = [];
yAll = [];

for ii = 1:length(pptrials)
    if pptrials{ii}.Correct == 3
        timeOn = ceil(pptrials{ii}.TimeFixationON/(params.samplingRate));
        timeOff = floor(pptrials{ii}.TimeFixationOFF/(params.samplingRate));
        if timeOff == 0
            continue
        elseif timeOff > length(pptrials{ii}.x.position)
            timeOff = length(pptrials{ii}.x.position);
        end
        
        %         em.(eccentricity).(strokeWidth).positionDrift(counter).x = ...
        x = pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
        %             em.(eccentricity).(strokeWidth).positionDrift(counter).y = ...
        y = pptrials{ii}.y.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
        
        em.(eccentricity).(strokeWidth).positionDrift(fixCounter).x = x;
        em.(eccentricity).(strokeWidth).positionDrift(fixCounter).y = y;
        
        xAll = [xAll x];
        yAll = [yAll y];
        
        xCell{fixCounter} = x;
        yCell{fixCounter} = y;
        fixCounter = fixCounter + 1;
    end
end
        [nXValues, nYValues, ~, ~] = posValueLimit(xCell, yCell);
        [ nXValues, nYValues] = checkXYArt(nXValues, nYValues, min(round(length(xAll)/5)), 61);
        [ nXValues, nYValues] = checkXYBound(nXValues, nYValues, 30); %Checks boundries
        
        limit.xmin = floor(min(nXValues));
        limit.xmax = ceil(max(nXValues));
        limit.ymin = floor(min(nYValues));
        limit.ymax = ceil(max(nYValues));
        n_bins = 100;
        
        fixationResult = MyHistogram2(nXValues, nYValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
        fixationResult = fixationResult./(max(max(fixationResult)));
        em.fixation_heatmap = fixationResult;
       
        
        t = round(length(x) * (params.samplingRate));
%         
%     end
% end
fixationPPtrialsIdx = 0;
fullTime = 0;
fix = 0;

% fixCounter = 1;
% for ii = 1:length(pptrials)
%     if pptrials{ii}.Correct == 3
%         fixIdx(ii) = 1;
%         fixPP(fixCounter) = ii;
%         fixCounter = fixCounter +1;
%     else
%         fixIdx(ii) = 0;
%     end
% end
% fixationalIdx = find(fixIdx == 1);
% 
% % allIdx = intersect(find(valid.dms == 1), fixationalIdx);
% xValues = [];
% yValues = [];
% 
% % for swIdx = 1:length(allSW)
% counter = 1;
% for ii = 1:length(pptrials)
%     eccPrint = round(uEcc);
%     eccentricity = sprintf('ecc_%i', eccPrint);
%     strokeWidth = 'strokeWidth_ALL';
%     if pptrials{ii}.Correct == 3
%         if pptrials{ii}.TimeFixationON == 0 %%%No Time Captured
%             continue;
%         else
%             timeOn = ceil(pptrials{ii}.TimeFixationON/(params.samplingRate));
%             timeOff = floor(pptrials{ii}.TimeFixationOFF/(params.samplingRate));
%         end
%         t = round(timeOff - timeOn);
%         if timeOff > length(pptrials{ii}.x.position)
%             continue;
%             %             timeOff = length(pptrials{ii}.x.position);
%         end
%         
%         msNum = length(pptrials{ii}.microsaccades.start);
%         if msNum == 0
%             em.(eccentricity).(strokeWidth).positionDrift(counter).x = ...
%                 pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
%             em.(eccentricity).(strokeWidth).positionDrift(counter).y = ...
%                 pptrials{ii}.y.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
%         else
%             for  msIdx = 1:msNum
%                 if msIdx == 1
%                     timeOnN = 1;
%                 else
%                     timeOnN = pptrials{ii}.microsaccades.start(msIdx-1)+1;
%                 end
%                 timeOffN = pptrials{ii}.microsaccades.start(msIdx)+ pptrials{ii}.microsaccades.duration(msIdx);
%                 if timeOffN > length(pptrials{ii}.x.position)
%                     timeOffN = length(pptrials{ii}.x.position);
%                 end
%                 em.(eccentricity).(strokeWidth).positionDrift(counter).x = ...
%                     pptrials{ii}.x.position(timeOnN:timeOffN) + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
%                 em.(eccentricity).(strokeWidth).positionDrift(counter).y = ...
%                     pptrials{ii}.y.position(timeOnN:timeOffN) + pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
%             end
%         end
%         
%         if t > 0
%             time(counter) = t;
%             eccPrint = round(uEcc);
%             eccentricity = sprintf('ecc_%i', eccPrint);
%             strokeWidth = 'strokeWidth_ALL';
%             
%             if pptrials{ii}.TimeFixationOFF == 0
%                 continue;
%             end
%             
%             em.(eccentricity).(strokeWidth).position(counter).x = ...
%                 pptrials{ii}.x.position(timeOn:timeOff)...
%                 + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
%             
%             %%Remove Offset for Test
%             
%             xValues{ii} = em.(eccentricity).(strokeWidth).position(counter).x;
%             
%             em.(eccentricity).(strokeWidth).position(counter).y = ...
%                 pptrials{ii}.y.position(timeOn:timeOff)...
%                 + pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
%             
%             
%             yValues{ii} = em.(eccentricity).(strokeWidth).position(counter).y;
%             fixationPPtrialsIdx(counter) = (ii);
%             
%             counter = counter+1;
%             %         end
%         end
%     end
% end
% for posIdx = 1:length(em.(eccentricity).(strokeWidth).position)
%     positionDrift = em.(eccentricity).(strokeWidth).positionDrift(:,posIdx);
%     
%     %     [~, instSpX, instSpY, ~, ~, ~, ~, ~] = ...
%     %         getDriftChar(positionDrift.x, positionDrift.y, 41, 1, 180);
%     
%     %     [~, fix.velocity(ii), velX{ii}, velY{ii}] = ...
%     %         CalculateDriftVelocity(positionDrift, 1);
%     
%     %     nInstX = instSpX(~isnan(instSpX));
%     %     nInstY = instSpY(~isnan(instSpY));
%     
%     %     nInstX = instSpX(1:1650);
%     %     nInstY = instSpY(1:1650);
%     %     em.(eccentricity).(strokeWidth).instSpX{posIdx} = instSpX;
%     %     em.(eccentricity).(strokeWidth).instSpY{posIdx} = instSpY;
%     
%     %     em.(eccentricity).(strokeWidth).instSpX{posIdx} = nInstX(~isnan(nInstX));
%     %     em.(eccentricity).(strokeWidth).instSpY{posIdx} = nInstY(~isnan(nInstX));
%     
% end
% fix.velocity = [];
% 
% % generateDriftVelocityMap(...
% %     em.(eccentricity).(strokeWidth).instSpX, ...
% %     em.(eccentricity).(strokeWidth).instSpY, ...
% %     strokeWidth, params, title_str, trialChar)
% 
% %   1-330 = 0-500
% %   165-330 = 500-1000
% 
% convFactor = params.samplingRate; %conversion factor for the dDPI
% switch params.timeBin
%     case 1
%         startFullTime = 1;
%         fullTime = 500/convFactor;
%         %         fullTime = 2000/convFactor;
%     case 2
%         startFullTime = 500/convFactor;
%         fullTime = 1500/convFactor;
%     case 3
%         startFullTime = 1500/convFactor;
%         fullTime = 3000/convFactor;
%     case 4
%         startFullTime = 3000/convFactor;
%         fullTime = 5000/convFactor;
%     otherwise
%         startFullTime = 1;
%         fullTime = 5000/convFactor;
% end
% 
% % fullTime = 1650;%5seconds of data
% % fullTime = 660;%2seconds of data
% % fullTime = 165;%0.5 seconds of data = same length as task
% 
% % time = length(xValues);
% for compFixIdx = 1:length(xValues)
%     currentXVel = xValues{compFixIdx};
%     currentYVel = yValues{compFixIdx};
%     if ~isempty(currentXVel)
%         testxValues{compFixIdx} = currentXVel(1:fullTime); %Full 5 seconds
%         testyValues{compFixIdx} = currentYVel(1:fullTime);
%         t = fullTime;
%     end
% end
% 
% % [ testxValues, testyValues] = checkXYArt(testxValues, testyValues, min(round(length(testxValues)/100)));
% [nXValues, nYValues, ~, ~] = posValueLimit(testxValues, testyValues);
% [ nXValues, nYValues] = checkXYArt(nXValues, nYValues, min(round(length(testxValues)/5)), 61);
% [ nXValues, nYValues] = checkXYBound(nXValues, nYValues, 60); %Checks boundries
% 
% limit.xmin = floor(min(nXValues));
% limit.xmax = ceil(max(nXValues));
% limit.ymin = floor(min(nYValues));
% limit.ymax = ceil(max(nYValues));
% n_bins = 100;
% 
% fixationResult = MyHistogram2(nXValues, nYValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
% fixationResult = fixationResult./(max(max(fixationResult)));
% em.fixation_heatmap = fixationResult;
% 
% 
% 
% 
% if strcmp('D',params.machine)
%     t = round(t * (1000/330));
% else
%     t = round(t);
% end
% 
% if ~isempty(valid.ms(fixationPPtrialsIdx))
%     if isfield(em,'ms')
%         if isfield(em.ms,eccentricity)
%             if isfield(em.ms.(eccentricity),strokeWidth)
%                 fixAllSW = em.ms.(eccentricity).(strokeWidth);
%             end
%         end
%     end
%     if exist('fixAllSW','var')
%         fixationMsLocation(fixAllSW, params, 1, trialChar, title_str, em)
%     end
% end
% 
% t = round(mean(t));
end

