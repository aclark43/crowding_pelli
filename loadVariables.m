function[ subjectCond, subjectThreshCond, subjectMSCond, perform ] = ...
    loadVariables(subjectsAll, condition, params, huxlinProject, ecc)
% Loads all variables that were created in runMupltipleSubj. Stored in
% MATFiles Folder inside scripts folder.
em = params.em;

for ii = 1:length(subjectsAll)
%     if strcmp('AshleyDDPI',subjectsAll(ii)) || ...
%             strcmp('Z002DDPI',subjectsAll(ii)) ||...
%             strcmp('Z084DDPI',subjectsAll(ii)) ||...
%             strcmp('Z046DDPI',subjectsAll(ii)) 
% %         ecc = '0eccEcc';
% 
%     else
%         ecc = '0ecc';
%     end
clear fileName4;
clear fileName5;
clear fileName6;


    conditionT = condition{1};
%     fileName1 = sprintf('%s_%s_SpanAmpEval.mat', subjectsAll{ii},condition{1});
    fileName1 = sprintf('%s_%s_Unstabilized_ecc_0_SpanAmpEval.mat', subjectsAll{ii},condition{1});
    if strcmp('Drift',em)
        fileName2 = sprintf('%s_%s_Unstabilized_Drift_%s_Threshold.mat', subjectsAll{ii},condition{1}, ecc);
%         fileName2 = sprintf('%s_%s_Unstabilized_Drift_%s_ThresholdTestingAfterEmAnalysisChange.mat', subjectsAll{ii},condition{1}, ecc);
        if ~params.fig.FOLDED
            fileName4 = sprintf('%s_%s_Unstabilized_Drift_%s_SmallSpanPerform.mat', ...
                subjectsAll{ii},condition{1}, ecc);
            fileName5 = sprintf('%s_%s_Unstabilized_Drift_%s_LargeSpanPerform.mat', ...
                subjectsAll{ii},condition{1}, ecc);
            fileName6 = sprintf('%s_%s_Unstabilized_Drift_%s_BootstrapThreshold.mat', ...
                subjectsAll{ii},condition{1}, ecc);
        end
    elseif strcmp('Drift_MS',em)
        fileName2 = sprintf('%s_%s_Unstabilized_Drift_MS_%s_Threshold.mat', subjectsAll{ii},condition{1}, ecc);
    end
    if params.fig.FOLDED
        if strcmp('Z091',subjectsAll{ii})
            fileName3 = sprintf('%s_%s_Unstabilized_MS_%s_Threshold.mat', subjectsAll{ii},condition{1}, '0eccEcc');
        else
            fileName3 = sprintf('%s_%s_Unstabilized_MS_%s_Threshold.mat', subjectsAll{ii},condition{1}, '0ecc');
        end
    else
        fileName3 = sprintf('%s_%s_Unstabilized_MS_%s_Threshold.mat', subjectsAll{ii},condition{1}, ecc);
    end
    % fileName = 'Z023_Uncrowded_SpanAmpEval.mat';
    folderName = ...
        'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Scripts\crowding_pelli\MATFiles\';
    file1 = fullfile(folderName, fileName1);
    file2 = fullfile(folderName, fileName2);
    file3 = fullfile(folderName, fileName3);
    if exist(file2, 'file') == 0
        continue;
    end
%     if ~ii == 11
        
    if exist('fileName4')
        file4 = fullfile(folderName, fileName4);
        load(file4);
        performStruct.SmallSpans = threshStruct;
    else
        performStruct.SmallSpans = NaN;
    end
    if exist('fileName5')
        file5 = fullfile(folderName, fileName5);
        load(file5);
        performStruct.LargeSpans = threshStruct;
    else
        performStruct.LargeSpans = NaN;
    end
    if exist('fileName6')
        file6 = fullfile(folderName, fileName6);
        load(file6);
        performStruct.BootThresh = threshInfo.thresh;
        performStruct.BootThreshAll = threshInfo.threshB;
    else
        performStruct.BootThresh = NaN;
        performStruct.BootThreshAll = NaN;
    end

    load(file1);
    load(file2);
    threshInfoDrift = threshInfo;
    if isnan(threshInfoDrift.thresh)
        continue;
    end
%     load(file3);
    threshInfoMs.thresh = NaN;
    if huxlinProject == 1
        subjectCond(ii) = spnAmpEvaluate;
        perform(ii).thresh = performStruct.BootThresh;
        perform(ii).bootStrapAll = performStruct.BootThreshAll;
        subjectThreshCond(ii) = threshInfoDrift;
        if ~isnan(threshInfoMs.thresh)
            subjectMSCond(ii) = threshInfoMs;
        end
    else
        subjectCond(ii) = spnAmpEvaluate;
        perform(ii).thresh = performStruct.BootThresh;
        perform(ii).bootStrapAll = performStruct.BootThreshAll;
        subjectThreshCond(ii) = threshInfoDrift;
        if ~isnan(threshInfoMs.thresh)
            subjectMSCond(ii) = threshInfoMs;
        end
    end
end

if ~exist('subjectCond','var')
    subjectCond(ii) = NaN;
    subjectThreshCond(ii) = NaN;
    subjectMSCond(ii) = NaN;
end
if ~exist('subjectMSCond','var')
    subjectMSCond = [];
end

end
% end

