function Coordinates = SetCoordinates(uEcc, sw)

% stimuliSize = sw * params.pixelAngle;
stimuliSize = sw;

figure
centerTarget = (rectangle('Position',[(-2*stimuliSize*1.4)+uEcc ...
    (-stimuliSize*5*1.4) ...
    (2*stimuliSize*1.4*2) ...
    (10*stimuliSize)*1.4]));
pointsCenterTarget = bbox2points(centerTarget.Position);


leftFlanker = rectangle...
    ('Position',[-(4*1.4*stimuliSize+(2*stimuliSize*1.4))+uEcc,...
    -stimuliSize*5*1.4, ...
    2*stimuliSize*1.4*2, ...
    10*stimuliSize*1.4],'LineWidth',1);
pointsLeftFlanker = bbox2points(leftFlanker.Position);



rightFlanker = rectangle...
    ('Position',[(2*stimuliSize*1.4)+uEcc,...
     -stimuliSize*5*1.4, ...
    (2*stimuliSize*1.4*2), ...
    10*stimuliSize*1.4], 'LineWidth',1);
pointsRightFlanker = bbox2points(rightFlanker.Position);



bottomFlanker = rectangle...
    ('Position',[(-2*stimuliSize*1.4)+uEcc, ...
    -(7*stimuliSize+(10*stimuliSize*1.4)),...
    (2*stimuliSize*1.4*2), ...
    10*stimuliSize*1.4], 'LineWidth',1);
pointsBottomFlanker = bbox2points(bottomFlanker.Position);


topFlanker    = rectangle...
    ('Position',[(-2*stimuliSize*1.4)+uEcc,...
    ((10*stimuliSize*1.4)-7*stimuliSize),...
    (2*stimuliSize*1.4*2), ...
    10*stimuliSize*1.4], 'LineWidth',1);
pointsTopFlanker = bbox2points(topFlanker.Position);

multiplier = 2;

Coordinates.centerTarget_x1 = multiplier* pointsCenterTarget(1,1);
Coordinates.centerTarget_x2 = multiplier* pointsCenterTarget(2,1);
Coordinates.centerTarget_y1 = multiplier* pointsCenterTarget(1,2);
Coordinates.centerTarget_y2 = multiplier* pointsCenterTarget(3,2);

Coordinates.leftFlanker_x1 = multiplier*pointsLeftFlanker(1,1);
Coordinates.leftFlanker_x2 = multiplier*pointsLeftFlanker(2,1);
Coordinates.leftFlanker_y1 = multiplier*pointsLeftFlanker(1,2);
Coordinates.leftFlanker_y2 = multiplier*pointsLeftFlanker(3,2);

Coordinates.rightFlanker_x1 =multiplier* pointsRightFlanker(1,1);
Coordinates.rightFlanker_x2 =multiplier* pointsRightFlanker(2,1);
Coordinates.rightFlanker_y1 = multiplier*pointsRightFlanker(1,2);
Coordinates.rightFlanker_y2 =multiplier* pointsRightFlanker(3,2);

Coordinates.topFlanker_x1 = multiplier*pointsTopFlanker(1,1);
Coordinates.topFlanker_x2 = multiplier*pointsTopFlanker(2,1);
Coordinates.topFlanker_y1 = multiplier*pointsTopFlanker(1,2);
Coordinates.topFlanker_y2 = multiplier*pointsTopFlanker(3,2);

Coordinates.bottomFlanker_x1 = multiplier*pointsBottomFlanker(1,1);
Coordinates.bottomFlanker_x2 = multiplier*pointsBottomFlanker(2,1);
Coordinates.bottomFlanker_y1 = multiplier*pointsBottomFlanker(1,2);
Coordinates.bottomFlanker_y2 = multiplier*pointsBottomFlanker(3,2);

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%     Coordinates.centerTarget = rectangle...
%         ('Position',[(-stimuliSize)+uEcc ...
%         (-stimuliSize*5) ...
%         (2*stimuliSize) ...
%         (10*stimuliSize)],'LineWidth',1);
    
%     Coordinates.centerTarget_x1 = (-stimuliSize)+uEcc;
%     Coordinates.centerTarget_x2 = ((stimuliSize)+uEcc);
%     Coordinates.centerTarget_y1 = -(stimuliSize*5)*(stimuliSize);
%     Coordinates.centerTarget_y2 = (stimuliSize*5)*(stimuliSize);

%     Coordinates.leftFlanker_x1 = -(2*stimuliSize+(2*stimuliSize*1.4))+uEcc;
%     Coordinates.leftFlanker_x2 = 2*stimuliSize*-(2*stimuliSize+(2*stimuliSize*1.4))+uEcc;
%     Coordinates.leftFlanker_y1 = stimuliSize*5;
%     Coordinates.leftFlanker_y2 = (2*stimuliSize)*-stimuliSize*5;
    
%     = rectangle...
%         ('Position',[-(2*stimuliSize+(2*stimuliSize*1.4))+uEcc,...
%         -stimuliSize*5, ...
%         2*stimuliSize, ...
%         10*stimuliSize],'LineWidth',1);


%     Coordinates.rightFlanker_x1 = (2*stimuliSize*1.4)+uEcc;
%     Coordinates.rightFlanker_x2 = 2*stimuliSize*(2*stimuliSize*1.4)+uEcc;
%     Coordinates.rightFlanker_y1 = stimuliSize*5;
%     Coordinates.rightFlanker_y2 = -stimuliSize*5*(2*stimuliSize);
    
%     = rectangle...
%         ('Position',[(2*stimuliSize*1.4)+uEcc,...
%         -stimuliSize*5, ...
%         -stimuliSize*5, ...
%         10*stimuliSize], 'LineWidth',1);

    
%     Coordinates.bottomFlanker_x1 = (-stimuliSize+uEcc);
%     Coordinates.bottomFlanker_x2 = (2*stimuliSize)*(stimuliSize+uEcc);
%     Coordinates.bottomFlanker_y1 = -(3*stimuliSize+(10*stimuliSize*1.4));
%     Coordinates.bottomFlanker_y2 = -(3*stimuliSize+(10*stimuliSize*1.4))*2*stimuliSize;

%     Coordinates.topFlanker_x1 = (-stimuliSize+uEcc);
%     Coordinates.topFlanker_x2 = (stimuliSize+uEcc)*(2*stimuliSize);
%     Coordinates.topFlanker_y1 = ((10*stimuliSize*1.4)-7*stimuliSize);
%     Coordinates.topFlanker_y2 = ((10*stimuliSize*1.4)-7*stimuliSize)*(2*stimuliSize);













%     Coordinates.LeftEye_x1 = -40;
%     Coordinates.LeftEye_x2 = 0;
%     Coordinates.LeftEye_y1 = 30;
%     Coordinates.LeftEye_y2 = 5;
%     
%     Coordinates.RightEye_x1 = 0;
%     Coordinates.RightEye_x2 = 40;
%     Coordinates.RightEye_y1 = 30;
%     Coordinates.RightEye_y2 = 5;
%     
%     Coordinates.Nose_x1 = -16;
%     Coordinates.Nose_x2 =  16;
%     Coordinates.Nose_y1 = 4;
%     Coordinates.Nose_y2 = -15;
%     
%     Coordinates.Mouth_x1 = -26;
%     Coordinates.Mouth_x2 = 26;
%     Coordinates.Mouth_y1 = -16;
%     Coordinates.Mouth_y2 = -55;
%   
%  if ImageSize==120 
%     AreaLE = (Coordinates.LeftEye_x2-Coordinates.LeftEye_x1)*(Coordinates.LeftEye_y1-Coordinates.LeftEye_y2);
%     AreaRE = (Coordinates.RightEye_x2-Coordinates.RightEye_x1)*(Coordinates.RightEye_y1-Coordinates.RightEye_y2);
%     AreaN = (Coordinates.Nose_x2-Coordinates.Nose_x1)*(Coordinates.Nose_y1-Coordinates.Nose_y2);
%     AreaM = (Coordinates.Mouth_x2-Coordinates.Mouth_x1)*(abs(Coordinates.Mouth_y2)-abs(Coordinates.Mouth_y1));
%     AreaB = (120^2)-(AreaLE + AreaRE + AreaN + AreaM);
%     
%  end
% 
%  if ImageSize==176;
%         
%     Diff = 1.46;
%     Coordinates.LeftEye_x1 = Coordinates.LeftEye_x1*Diff;
%     Coordinates.LeftEye_x2 = Coordinates.LeftEye_x2*Diff;
%     Coordinates.LeftEye_y1 = Coordinates.LeftEye_y1*Diff + 13; % Comes from vt.Y_orig
%     Coordinates.LeftEye_y2 = Coordinates.LeftEye_y2*Diff + 13;
%     
%     Coordinates.RightEye_x1 = Coordinates.RightEye_x1*Diff;
%     Coordinates.RightEye_x2 = Coordinates.RightEye_x2*Diff;
%     Coordinates.RightEye_y1 = Coordinates.RightEye_y1*Diff + 13;
%     Coordinates.RightEye_y2 = Coordinates.RightEye_y2*Diff + 13;
%     
%     Coordinates.Nose_x1 = Coordinates.Nose_x1*Diff;
%     Coordinates.Nose_x2 = Coordinates.Nose_x2*Diff;
%     Coordinates.Nose_y1 = Coordinates.Nose_y1*Diff + 13;
%     Coordinates.Nose_y2 = Coordinates.Nose_y2*Diff + 13;
%     
%     Coordinates.Mouth_x1 = Coordinates.Mouth_x1*Diff;
%     Coordinates.Mouth_x2 = Coordinates.Mouth_x2*Diff;
%     Coordinates.Mouth_y1 = Coordinates.Mouth_y1*Diff + 13;
%     Coordinates.Mouth_y2 = Coordinates.Mouth_y2*Diff + 13;
%     
%  end
%     
%     if ImageSize==176 
%     AreaLE = (Coordinates.LeftEye_x2-Coordinates.LeftEye_x1)*(Coordinates.LeftEye_y1-Coordinates.LeftEye_y2);
%     AreaRE = (Coordinates.RightEye_x2-Coordinates.RightEye_x1)*(Coordinates.RightEye_y1-Coordinates.RightEye_y2);
%     AreaN = (Coordinates.Nose_x2-Coordinates.Nose_x1)*(Coordinates.Nose_y1-Coordinates.Nose_y2);
%     AreaM = (Coordinates.Mouth_x2-Coordinates.Mouth_x1)*(abs(Coordinates.Mouth_y2)-abs(Coordinates.Mouth_y1));
%     AreaB = (176^2)-(AreaLE + AreaRE + AreaN + AreaM);
%     
%     end
%     
%     
% if ImageSize==500;
%         
%     Diff = 4;
%     Coordinates.LeftEye_x1 = Coordinates.LeftEye_x1*Diff;
%     Coordinates.LeftEye_x2 = Coordinates.LeftEye_x2*Diff;
%     Coordinates.LeftEye_y1 = Coordinates.LeftEye_y1*Diff;
%     Coordinates.LeftEye_y2 = Coordinates.LeftEye_y2*Diff;
%     
%     Coordinates.RightEye_x1 = Coordinates.RightEye_x1*Diff;
%     Coordinates.RightEye_x2 = Coordinates.RightEye_x2*Diff;
%     Coordinates.RightEye_y1 = Coordinates.RightEye_y1*Diff;
%     Coordinates.RightEye_y2 = Coordinates.RightEye_y2*Diff;
%     
%     Coordinates.Nose_x1 = Coordinates.Nose_x1*Diff;
%     Coordinates.Nose_x2 = Coordinates.Nose_x2*Diff;
%     Coordinates.Nose_y1 = Coordinates.Nose_y1*Diff;
%     Coordinates.Nose_y2 = Coordinates.Nose_y2*Diff;
%     
%     Coordinates.Mouth_x1 = Coordinates.Mouth_x1*Diff;
%     Coordinates.Mouth_x2 = Coordinates.Mouth_x2*Diff;
%     Coordinates.Mouth_y1 = Coordinates.Mouth_y1*Diff;
%     Coordinates.Mouth_y2 = Coordinates.Mouth_y2*Diff;
%     
% %     Coordinates.LeftEye_x1 = -(250- 100);
% %     Coordinates.LeftEye_x2 = -(250- 230);
% %     Coordinates.LeftEye_y1 = (250- 156);
% %     Coordinates.LeftEye_y2 = (250- 226);
% %     
% %     Coordinates.RightEye_x1 = (273 -250);
% %     Coordinates.RightEye_x2 = (400 -250);
% %     Coordinates.RightEye_y1 = (250- 156);
% %     Coordinates.RightEye_y2 = (250- 226);
% %     
% %     Coordinates.Nose_x1 = -(250- 182);
% %     Coordinates.Nose_x2 = (317 -250);
% %     Coordinates.Nose_y1 =(250- 230);
% %     Coordinates.Nose_y2 = -(346 -250);
% %     
% %     Coordinates.Mouth_x1 = -(250- 155);
% %     Coordinates.Mouth_x2 = (338 -250);
% %     Coordinates.Mouth_y1 = -(360 -250);
% %     Coordinates.Mouth_y2 = -(450 -250);
% end
