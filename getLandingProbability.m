function [ probStart, probEnd ] = ...
    getLandingProbability( pattern, trialChar )
%% initially, 0 = landed on background. These should ALL be converted to 
%   proabilites

landMSAll = extractfield(pattern,'startMS');
landMSIdx = landMSAll >= 0;
landingMS = landMSAll(landMSIdx);

probStart.NumOfMS = numel(landingMS);
probStart.Target = (numel(find(landingMS == 1))/numel(landingMS));
probStart.RFlanker = (numel(find(landingMS == 2))/numel(landingMS));
probStart.LFlanker = (numel(find(landingMS == 3))/numel(landingMS));
probStart.TFlanker = (numel(find(landingMS == 4))/numel(landingMS));
probStart.BFlanker = (numel(find(landingMS == 5))/numel(landingMS));
probStart.Background = (numel(find(landingMS == 0))/numel(landingMS));

endMSAll = extractfield(pattern,'endMS');
endMSIdx = endMSAll >= 0;
endingMS = endMSAll(endMSIdx);
probEnd.NumOfMS = numel(endingMS);
probEnd.Target = (numel(find(endingMS == 1))/numel(endingMS));
probEnd.RFlanker = (numel(find(endingMS == 2))/numel(endingMS));
probEnd.LFlanker = (numel(find(endingMS == 3))/numel(endingMS));
probEnd.TFlanker = (numel(find(endingMS == 4))/numel(endingMS));
probEnd.BFlanker = (numel(find(endingMS == 5))/numel(endingMS));
probEnd.Background = (numel(find(endingMS == 0))/numel(endingMS));

end
