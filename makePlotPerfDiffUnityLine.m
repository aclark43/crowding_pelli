function makePlotPerfDiffUnityLine(smallPerf,largePerf, pU, pC);

figure;
plot(smallPerf(2,:),largePerf(2,:),'o');
line([.4 1],[.4 1])
subplot(1,2,1)
plot(smallPerf(1,:),largePerf(1,:),'o');
axis([0 1 0 1])
xlabel('Small Area');
ylabel('Large Area');
line([0 1],[0 1])
hold on
errorbar(mean(smallPerf(1,:)),...
mean(largePerf(1,:)),sem(smallPerf(1,:)),...
sem(largePerf(1,:)),sem(smallPerf(1,:)),...
sem(largePerf(1,:)),'o','MarkerSize',10);
title(sprintf('Uncrowded, p = %.3f', pU))
axis square

subplot(1,2,2)
plot(smallPerf(2,:),largePerf(2,:),'o');
axis([0 1 0 1])
xlabel('Small Area');
ylabel('Large Area');
line([0 1],[0 1])
hold on
errorbar(mean(smallPerf(2,:)),...
mean(largePerf(2,:)),sem(smallPerf(2,:)),sem(smallPerf(2,:)),...
sem(largePerf(2,:)),sem(largePerf(2,:)),'o','MarkerSize',10)
title(sprintf('Crowded, p = %.3f',pC))
axis square