%runFigsForManuscript_FovealCrowding_Clark2022
clear all
clc

% subjectsAll = {'Z013','Z014','Z181','Z002','Z084'};

subjectsAll = {'Z023','Ashley','Z005','Z002','Z013',...
   'Z024','Z064','Z046','Z084','Z014',...
   'Z091','Z138','Z181'};%,...

conditions = {'Uncrowded','Crowded'};
eccNames = {'0ecc'};
numEcc = 1;

colorMap = brewermap(length(subjectsAll),'Set3');
colorMapVs = brewermap(length(subjectsAll),'Paired');
colorMapNew = brewermap(length(subjectsAll),'YlGnBu');

% colorMapNew = cool(length(subjectsAll));
colorMapNew = ashleysColorMap;
%% load in variables
for numSub = 1:length(subjectsAll)
    for numCrow = 1:2
        %         for numStab = 1:length(stabilization)
        if strcmp(subjectsAll{numSub}, 'Zoe') || strcmp(subjectsAll{numSub}, 'Z091')
            
            subjectCond.(conditions{numCrow}).Unstabilized{numSub} = ...
                load(sprintf('MATFiles/%s_%s_%s_Drift_%s_Threshold.mat', ...
                subjectsAll{numSub}, (conditions{numCrow}),...
                'Unstabilized', '0ecc'));
        else
            subjectCond.(conditions{numCrow}).Unstabilized{numSub} = ...
                load(sprintf('MATFiles/%s_%s_%s_Drift_%s_Threshold.mat', ...
                subjectsAll{numSub}, (conditions{numCrow}),...
                'Unstabilized', (eccNames{numEcc})));
        end
        %         end
    end
end

figure;
tempNum = 9;
plot(1:length(subjectCond.Uncrowded.Unstabilized{1, 1}.threshInfo.em.x{1, tempNum}),...
    subjectCond.Uncrowded.Unstabilized{1, 1}.threshInfo.em.x{1, tempNum},'-');
hold on
plot(1:length(subjectCond.Uncrowded.Unstabilized{1, 1}.threshInfo.em.y{1, tempNum}),...
    subjectCond.Uncrowded.Unstabilized{1, 1}.threshInfo.em.y{1, tempNum},'-');
ylim([-6 6])
% stabilized.Z181.Uncrowded = load('MATFiles/Z002DDPI_Uncrowded_Stabilized_Drift_0ecc_Threshold.mat');
stabilized.Z091.Uncrowded = load('MATFiles/Z091_Uncrowded_Stabilized_Drift_0eccEcc_Threshold.mat');
stabilized.Ashley.Uncrowded = load('MATFiles/Ashley_Uncrowded_Stabilized_Drift_0ecc_Threshold.mat');
%stabilized.Z023.Uncrowded = load('MATFiles/Z023_Uncrowded_Stabilized_Drift_0ecc_DriftChar.mat');

% stabilized.Zoe.Crowded = load('MATFiles/Zoe_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
stabilized.Z091.Crowded = load('MATFiles/Z091_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
stabilized.Ashley.Crowded = load('MATFiles/AshleyDDPI_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
%stabilized.Z023.Crowded = load('MATFiles/Z023_Crowded_Stabilized_Drift_0ecc_DriftChar.mat');

figure;
subplot(1,2,2)
% plot([1 2],[stabilized.Zoe.Uncrowded.threshInfo.thresh ...
%     stabilized.Zoe.Crowded.threshInfo.thresh],'-o');
hold on
plot([1 2],[stabilized.Z091.Uncrowded.threshInfo.thresh ...
    stabilized.Z091.Crowded.threshInfo.thresh],'-o');
plot([1 2],[stabilized.Ashley.Uncrowded.threshInfo.thresh ...
    stabilized.Ashley.Crowded.threshInfo.thresh],'-o');
ylim([1 3])
title('Stabilized')
subplot(1,2,1)
% plot([1 2],[subjectCond.Uncrowded.Unstabilized{14}.threshInfo.thresh   ...
%     subjectCond.Crowded.Unstabilized{14}.threshInfo.thresh  ],'-o');
hold on
plot([1 2],[subjectCond.Uncrowded.Unstabilized{11}.threshInfo.thresh,...
    subjectCond.Crowded.Unstabilized{11}.threshInfo.thresh],'-o');
plot([1 2],[subjectCond.Uncrowded.Unstabilized{2}.threshInfo.thresh   ...
    subjectCond.Crowded.Unstabilized{2}.threshInfo.thresh  ],'-o');
ylim([1 3])
title('Unstabilized')

figure;
 [~,p,b,r] = LinRegression(...
            [1 2 1 2],...%x([1 2 4:13])
            [subjectCond.Uncrowded.Unstabilized{11}.threshInfo.thresh,...
            subjectCond.Crowded.Unstabilized{11}.threshInfo.thresh,... %y
            subjectCond.Uncrowded.Unstabilized{2}.threshInfo.thresh,...
            subjectCond.Crowded.Unstabilized{2}.threshInfo.thresh],...
            0,NaN,1,0);
        
        figure;
 [~,p,b,r] = LinRegression(...
            [1 2 1 2],...%x([1 2 4:13])
            [stabilized.Z091.Uncrowded.threshInfo.thresh ...
    stabilized.Z091.Crowded.threshInfo.thresh,... %y
            stabilized.Ashley.Uncrowded.threshInfo.thresh ...
    stabilized.Ashley.Crowded.threshInfo.thresh],...
            0,NaN,1,0);

%% build table
for ii = 1:length(subjectsAll)
    for c = 1:2
        toPath = subjectCond.(conditions{c}).Unstabilized;
        tbl.(conditions{c})(ii).thresh = toPath{ii}.threshInfo.thresh;
        tbl.(conditions{c})(ii).dc = toPath{ii}.threshInfo.em.allTraces.dCoefDsq;
        tbl.(conditions{c})(ii).prl = ...
            mean(toPath{ii}.threshInfo.em.allTraces.prlDistance);
        tbl.(conditions{c})(ii).span = mean(toPath{ii}.threshInfo.em.allTraces.span);
        tbl.(conditions{c})(ii).bcea = mean(cell2mat(...
            toPath{ii}.threshInfo.em.allTraces.bcea));
        tbl.(conditions{c})(ii).curvature = ...
            mean(cell2mat(toPath{ii}.threshInfo.em.allTraces.curvature));
        
        for i = 1:length((toPath{ii}.threshInfo.em.allTraces.mn_speed))
            tempSpan(i) = toPath{ii}.threshInfo.em.allTraces.mn_speed{i};
        end
        tbl.(conditions{c})(ii).span = ...
            nanmean(tempSpan);
                
        matchingThresholdRound = round(toPath{ii}.threshInfo.thresh,1);
        matchingThresholdRound = [1.2 1.5];
        [tbl.(conditions{c})(ii).thresholdSW, ~, tbl.(conditions{c})(ii).actualStimSize] = ...
            findMatchingStrokewidthValue(matchingThresholdRound(1), toPath{ii}.threshInfo.em.ecc_0);
        
        if c == 1
            matchingThresholdRound = [1.2 1.5];
        else
            matchingThresholdRound = [1.7 2];
        end
        [stringVal{1}, perfVal(1)] = ...
            findMatchingStrokewidthValue(matchingThresholdRound(1), toPath{ii}.threshInfo.em.ecc_0);
        
        [stringVal{2}, perfVal(2)] = ...
            findMatchingStrokewidthValue(matchingThresholdRound(2), toPath{ii}.threshInfo.em.ecc_0);
        tbl.(conditions{c})(ii).stringVals{1} = stringVal(1);
        tbl.(conditions{c})(ii).stringVals{2} = stringVal(2);
        
        tbl.(conditions{c})(ii).performance{1} = perfVal(1);
        tbl.(conditions{c})(ii).performance{2} = perfVal(2);

        tbl.(conditions{c})(ii).prlDistances{1} = mean(toPath{ii}.threshInfo.em.ecc_0.(stringVal{1}).prlDistance);
        tbl.(conditions{c})(ii).prlDistances{2} = mean(toPath{ii}.threshInfo.em.ecc_0.(stringVal{2}).prlDistance);
        tbl.(conditions{c})(ii).name = subjectsAll{ii};
    end
end

[SubjectOrderDC,SubjectOrderDCIdx] = sort([tbl.Uncrowded(:).thresh]);
figure
for ii = 1:length(subjectsAll)
    plot(ii,tbl.Uncrowded(ii).thresh,'o','Color',colorMapNew(SubjectOrderDCIdx == ii,:),...
        'MarkerFaceColor',colorMapNew(SubjectOrderDCIdx == ii,:));
hold on
end

%%
for ii = 1:length(subjectsAll)
    for c = 1:length(conditions)
        currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em.ecc_0;
        allSWOptions = fieldnames(currentPath);
        for i = 1:length(allSWOptions)
            saveStimInfo{c,ii}(1,i) = currentPath.(allSWOptions{i}).stimulusSize;
            saveStimInfo{c,ii}(2,i) = string(cell2mat(...
                regexp(allSWOptions{i},'\d*','Match')));
            saveStimInfo{c,ii}(3,i) = length(currentPath.(allSWOptions{i}).id);
        end
    end
end


stimSizeWanted = [1.5 2.2];
for ii = 1:length(subjectsAll)
    for c = 1:length(conditions)
        currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em.ecc_0;
        [val,idx] = min(abs(saveStimInfo{c, ii}(1,:)-stimSizeWanted(c)));
        temp = (idx);
        nameTemp = sprintf('strokeWidth_%i', ...
            saveStimInfo{c, ii}(2,temp));
        actualPerf(c,ii) = currentPath.(nameTemp).performanceAtSize;
        actualPRLatSize(c,ii) = mean(currentPath.(nameTemp).prlDistance);
        actualSize(c,ii) = currentPath.(nameTemp).stimulusSize;
        fixedStim.actualCorrect{c,ii} =  currentPath.(nameTemp).correct;
        actualPRLatSizeInd{c,ii} = (currentPath.(nameTemp).prlDistance);
        stimSizeDesired = stimSizeWanted(c);
        farAway = stimSizeDesired*1.4;

        counter = 1;
        for i = length(currentPath.(nameTemp).position)
            temp = currentPath.(nameTemp).position(i).x < (stimSizeDesired/2) & ...
                currentPath.(nameTemp).position(i).x > -(stimSizeDesired/2) &...
                currentPath.(nameTemp).position(i).y < (5*stimSizeDesired/2) & ...
                currentPath.(nameTemp).position(i).y > -(5*stimSizeDesired/2);
            
            
            percentsOnStimFixSize(counter) = length(find(temp==1))/length(temp);
            temp2 = (currentPath.(nameTemp).position(i).x < (stimSizeDesired/2)+farAway & ...
                currentPath.(nameTemp).position(i).x > -(stimSizeDesired/2)+farAway) |...
                (currentPath.(nameTemp).position(i).x < (stimSizeDesired/2)-farAway & ...
                currentPath.(nameTemp).position(i).x > -(stimSizeDesired/2)-farAway);
            percentOnFlankerFixSize(counter) = length(find(temp2==1))/length(temp2);           
            meanDistFixSize(counter) = mean(currentPath.(nameTemp).position(i).x);
            counter = counter + 1;
        end
        fixedStim.timeSpentOnTarget{c,ii} = (percentsOnStimFixSize);
        fixedStim.timeOnFlankers{c,ii} = (percentOnFlankerFixSize); %just horizontal flankers
        fixedStim.meanDistX{c,ii} = meanDistFixSize;
    end
end

for ii = 1:length(subjectsAll)
    for c = 1:length(conditions)
                stimSizeDesired = find(saveStimInfo{c,ii}(3,:) ==...
                    max(saveStimInfo{c,ii}(3,:)));
%         if ii <= 5
%             stimSizeDesired = 2.0;
%         else
%             stimSizeDesired = 2.2;
%         end
%         stimSizeDesired = 2.2;
        stimSizeMost = saveStimInfo{c,ii}(1,stimSizeDesired);       
        currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em;
        temp = unique(currentPath.size);
        columnIdx = stimSizeDesired;

        idx = currentPath.ecc_0.(sprintf('strokeWidth_%i',saveStimInfo{c,ii}(2,columnIdx))).id;

        counter = 1; clear percentsOnStim; clear percentOnFlanker; clear meanDist;
        farAway = stimSizeDesired*1.4;
        stimSizeDesired = stimSizeDesired*1.5; %(make larger)
        for i = idx
            clear temp, clear temp2
            temp = currentPath.x{i} < (stimSizeDesired/2) & ...
                currentPath.x{i} > -(stimSizeDesired/2) &...
                currentPath.y{i} < 5*(stimSizeDesired/2) & ...
                currentPath.y{i} > -5*(stimSizeDesired/2);
%             temp = currentPath.x{i} < (stimSizeDesired/2) & ... %%just x
%                 currentPath.x{i} > -(stimSizeDesired/2);
            
            percentsOnStim(counter) = length(find(temp==1))/length(temp);
                     
            temp2 = (currentPath.x{i} < (stimSizeDesired/2)+farAway & ...
                currentPath.x{i} > -(stimSizeDesired/2)+farAway) |...
                (currentPath.x{i} < (stimSizeDesired/2)-farAway & ...
                currentPath.x{i} > -(stimSizeDesired/2)-farAway);
%                 temp2 = (currentPath.x{i} < (stimSizeDesired/2)+farAway & ... %%just x
%                 currentPath.x{i} > -(stimSizeDesired/2)+farAway);

            percentOnFlanker(counter) = length(find(temp2==1))/length(temp2);
            temp3 = temp2(find(temp2 == 1))* -1;
            percentWeightOnStim(1,counter) = (sum(temp)+sum(temp3));
            percentWeightOnStim(2,counter) = length(temp);
            
            meanDist(counter) = mean(currentPath.x{i});
            xALLTrial{counter} = currentPath.x{i};
            yALLTrial{counter} = currentPath.y{i};
            xPRL(counter) = mean(currentPath.x{i});
            counter = counter + 1;
        end
        timeSpentOnTarget{c,ii} = (percentsOnStim);
        weightSpentOnTarget{c,ii} = percentWeightOnStim;
        timeOnFlankers{c,ii} = (percentOnFlanker); %just horizontal flankers
        performance{c,ii} = currentPath.performance(idx);
        meanPerf(c,ii) = nanmean(currentPath.performance(idx));
        size{c,ii} = currentPath.size(idx);
        prl{c,ii} = currentPath.prl(idx);
        prlX{c,ii} = xPRL(find(xPRL ~= 0.0));
        x{c,ii} = currentPath.x{idx};
        meanDistX{c,ii} = meanDist;
        numTrials(c,ii) = max(saveStimInfo{c,ii}(3,:));
        xALLTrials{c,ii} = xALLTrial;
        yALLTrials{c,ii} = yALLTrial;
    end
end
close all

smallPerf = [];
largePerf = [];
prlAll = [];
for ii = 1:length(subjectsAll)
    
    for c = 1:length(conditions)
        [chooseTrialsS, chooseTrialsL] = determineThreshSize(ii,c);
        findMeanSmall(c,ii) = mean(prl{c, ii}) - (sem(prl{c, ii})*chooseTrialsS);
        findMeanLarge(c,ii) = mean(prl{c, ii}) + (sem(prl{c, ii})*chooseTrialsL);
        smallPerf(c,ii) = mean(performance{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
        lengthSP(c,ii) = length(performance{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
        largePerf(c,ii) = mean(performance{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));
        lengthLP(c,ii) = length(performance{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));
        
        meanpercentOnTS(c,ii) = mean(timeSpentOnTarget{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
        meanpercentOnTL(c,ii) = mean(performance{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));
        figure;
    histogram(prl{c, ii},20)
    
    smallPRLX{c,ii} = cell2mat(xALLTrials{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
    smallPRLY{c,ii} = cell2mat(yALLTrials{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
    largePRLX{c,ii} = cell2mat(xALLTrials{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));
    largePRLY{c,ii} = cell2mat(yALLTrials{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));

    xlabel('PRL')
    hold on
%     line([findMeanSmall findMeanSmall], [0 5], 'Color','r');
%     line([findMeanLarge findMeanLarge], [0 5],'Color','r');
    title(sprintf('%i, %i',ii,c));
    end
end
figure;
[h,pU] = ttest(smallPerf(1,:), largePerf(1,:))
[h,pC] = ttest(smallPerf(2,:), largePerf(2,:))
[h,Thp] = ttest([tbl.Uncrowded(:).thresh], [tbl.Crowded(:).thresh])
close all;

% y = [{eucDist.all(chronic == 0)} {eucDist.all(chronic == 1)} {eucDist.all(chronic == 2)}];
%     temp = struct2table(cell2struct(y, {'Subacute','Chronic','Control'},2)); 
    
[idx1,idx2] = sort([tbl.Crowded.thresh])    
% [~,acutalIdx] = sort(idx2)
temp = struct2table(cell2struct(prlX(2,idx2),{subjectsAll{idx2}},2));
% temp = struct2table(cell2struct(prl(2,:),string([tbl.Crowded.thresh]),2));
figure;
violinplot(temp)
temp = [tbl.Crowded.thresh];
% axis top
hold on
s(1) = plot(1:length(subjectsAll),(temp(idx2)),'*','Color','k');
hold on
s(1) = plot(1:length(subjectsAll),-(temp(idx2)),'*','Color','k');
for iii = 1:length(subjectsAll)
hold on
text(iii,25,...
    sprintf('%.2f - %.2f',findMeanSmall(2,iii), findMeanLarge(2,iii)));
hold on
end
% s(1) = plot(1:length(subjectsAll),findMeanLarge(2,:),'*','Color','b');




xlabel('Subject')
ylabel('Individual Trial Offset X (arcmin)');
title('Crowded Condition')
legend(s,{'Threshold Stimulus Width'});


%%Heatmaps for small vs large
[SubjectOrderDC,SubjectOrderDCIdx] = sort([tbl.Uncrowded.thresh]);

for c = 1:length(conditions)
    
    for i = 1:length(subjectsAll)
        figure;
        ii = (SubjectOrderDCIdx(i));
        stimSizeDesired = find(saveStimInfo{c,ii}(3,:) ==...
            max(saveStimInfo{c,ii}(3,:)));
        stimSizeMost = saveStimInfo{c,ii}(1,stimSizeDesired);
        currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em;
        subplot(1,2,1)
        generateHeatMapSimple( ...
            smallPRLX{c,ii}, ...
            smallPRLY{c,ii}, ...
            'Bins', 15,...
            'StimulusSize', stimSizeMost,...
            'AxisValue', 20,...
            'Uncrowded', c,...
            'Borders', 0);
        subplot(1,2,2)
        generateHeatMapSimple( ...
            largePRLX{c,ii}, ...
            largePRLY{c,ii}, ...
            'Bins', 15,...
            'StimulusSize', stimSizeMost,...
            'AxisValue', 20,...
            'Uncrowded', c,...
            'Borders', 0);
        title(conditions{c});
    end
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/HeatMaps%s.png', conditions{c}));
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/HeatMaps%s.epsc',  conditions{c}));
end

%% figure difference in offset for conditions
figure;
y = [mean(findMeanSmall(1,:)) mean(findMeanSmall(2,:)); ...
    mean(findMeanLarge(1,:)) mean(findMeanLarge(2,:))];
b = bar(y,'FaceColor','flat');
hold on
errorbar([0.85 1.15],[mean(findMeanSmall(1,:)) mean(findMeanSmall(2,:))],...
    [sem(findMeanSmall(1,:)), sem(findMeanSmall(2,:))],'o','Color','k');
hold on
errorbar([1.85 2.15],[mean(findMeanLarge(1,:)) mean(findMeanLarge(2,:))],...
    [sem(findMeanLarge(1,:)), sem(findMeanLarge(2,:))],'o','Color','k');
ylabel('Average Gaze Offset');
xticks([1 2])
xticklabels({'Small Gaze Offset','Large Gaze Offset'});
legend({'Uncrowded','Crowded'},'Location','northwest');
ylabel('Mean Gaze Offset');
ylim([3 8])
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/OffsetAverages.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/OffsetAverages.epsc');


%% HeatMaps
[SubjectOrderDC,SubjectOrderDCIdx] = sort([tbl.Uncrowded.thresh]);

for c = 1:length(conditions)
    figure;
    for i = 1:length(subjectsAll)
        ii = (SubjectOrderDCIdx(i));
%           ii = (SubjectOrderDCIdx(i));

        stimSizeDesired = find(saveStimInfo{c,ii}(3,:) ==...
            max(saveStimInfo{c,ii}(3,:)));
        stimSizeMost = saveStimInfo{c,ii}(1,stimSizeDesired);
        currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em;
        subplot(2,7,i)
        generateHeatMapSimple( ...
            currentPath.allTraces.xALL, ...
            currentPath.allTraces.yALL, ...
            'Bins', 15,...
            'StimulusSize', stimSizeMost,...
            'AxisValue', 20,...
            'Uncrowded', c,...
            'Borders', 0);
        hold on
        text(-15,-17,sprintf('BCEA = %.2f',[tbl.(conditions{c})(ii).bcea]/60));
        text(-15,-14,sprintf('Th = %.2f',tbl.(conditions{c})(ii).thresh));
    end
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/HeatMaps%s.png', conditions{c}));
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/HeatMaps%s.epsc',  conditions{c}));
end

figure;
for i = 1:length(subjectsAll)
    hold on
    plot([0 1], [[tbl.Uncrowded(i).bcea]/60 [tbl.Crowded(i).bcea]/60],'-o'...
        ); 
    hold on
end

errorbar([0.1 1.1],[mean([tbl.Uncrowded(:).bcea]/60) mean([tbl.Crowded(:).bcea]/60)],...
    [sem([tbl.Uncrowded(:).bcea]/60) sem([tbl.Crowded(:).bcea]/60)],'-o',...
    'Color','k','MarkerFaceColor','k');
ylabel('BCEA (deg^2)');
xticks([0 1]);
xticklabels({'Uncrowded','Crowded'});
xlim([-.5 1.5])
set(gca, 'YScale', 'log')
% axis square
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/BCEA.png', conditions{c}));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/BCEA.epsc',  conditions{c}));

[h,p] = ttest([tbl.Uncrowded(:).bcea]/60, [tbl.Crowded(:).bcea]/60);

%% FIGURE DC and Acuity - not Crowding Effect
figure('Position',[2000 100 1000 800]);
subplot(2,2,1)
x = [tbl.Uncrowded(:).dc];
y = [tbl.Uncrowded(:).thresh];
[~,p,b,r] = LinRegression(...
    x,...%x
    y,... %y
    0,NaN,1,0);
axis([0 60 0 4])
axis square
text(30,1,sprintf('p = %.2f \nr = %.2f', p, r))
xlabel({'Uncrowded D', '(arcmin^2/sec)'});
ylabel({'Uncrowded', 'Threshold (arcmin)'})
title('')

subplot(2,2,2)
x = [tbl.Crowded(:).dc];
y = [tbl.Crowded(:).thresh];
[~,p,b,r] = LinRegression(...
    x,...%x
    y,... %y
    0,NaN,1,0);
axis([0 60 0 4])
axis square
title('')
text(30,1,sprintf('p = %.2f \nr = %.2f', p, r))
xlabel({'Crowded D', '(arcmin^2/sec)'});
ylabel({'Crowded', 'Threshold (arcmin)'})

subplot(2,2,3)
for ii = 1:length(subjectsAll)
    leg(ii) = plot([0 1],...
        [tbl.Uncrowded(ii).dc tbl.Crowded(ii).dc],'-o',...
        'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
        'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:));
    hold on
end
[h,pttest] = ttest([tbl.Uncrowded(:).dc], [tbl.Crowded(:).dc]);
xticks([0 1])
xticklabels({'Uncrowded','Crowded'});
xlim([-.5 1.5]);
text(0,50,sprintf('p = %.2f',pttest));
ylabel('D (arcmin^2/sec')
% legend(leg,subjectsAll)
axis square

subplot(2,2,4)
[~,p,b,r] = LinRegression(...
    [tbl.Crowded(:).dc] - [tbl.Uncrowded(:).dc],...%x
    [tbl.Crowded(:).thresh] - [tbl.Uncrowded(:).thresh],... %y
    0,NaN,1,0);
hold on
axis([-15 40 0 2])
title('');
ylabel('Crowding Effect (arcmin)')
xlabel('\Delta D (arcmin^2/sec)')
axis square
text(-10,1.6,sprintf('p = %.2f \nr = %.2f', p, r))

% legend(leg,subjectsAll)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaEffectDc.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaEffectPosition.epsc');

%% FIGURE crowding effects and PRL

figure('Position',[2000 100 1000 800]);

subplot(1,2,1)
for ii = 1:length(subjectsAll)
    plot([0 1],[tbl.Uncrowded(ii).thresh...
        tbl.Crowded(ii).thresh*1.4],'-o',...
        'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:),'Color',...
        colorMapNew(find(SubjectOrderDCIdx == ii),:),...
        'MarkerSize',10,'LineWidth',2);
    hold on
end
xlim([-.5 1.5])
set(gca,'xtick',[0 1],'xticklabel',conditions, 'FontSize',14) %,'YScale', 'log'
ylabel('Threshold');
axis square

h1 = figure;
% subplot(1,2,2)

[~,p,b,r] = LinRegression(...
    [tbl.Crowded(:).prl]-[tbl.Uncrowded(:).prl],...%x
    [tbl.Crowded(:).thresh]-[tbl.Uncrowded(:).thresh],... %y
    0,NaN,1,0);
axis([-2.5 2.5 0 1.65])
axis square
xlabel('\Delta Gaze Position')
ylabel('Crowding Effect')
hold on
% colorMapNew2 = cool(length(subjectsAll));
colorMapNew2 = colorMapNew;
[vals,idx] = sort(([tbl.Crowded(:).prl]))
for ii = 1:length(subjectsAll)
    leg(ii) = plot([tbl.Crowded(ii).prl]-[tbl.Uncrowded(ii).prl],...
        [tbl.Crowded(ii).thresh]-[tbl.Uncrowded(ii).thresh],'o',...
        'Color',colorMapNew2(find(idx == ii),:),...
        'MarkerFaceColor',colorMapNew2(find(idx == ii),:),...
        'MarkerSize',10)
end
% legend(leg,subjectsAll,'Location','northwest')
colormap(colorMapNew)
c = colorbar('Ticks',[0 0.5 1],...
         'TickLabels',{vals(1),mean([vals(1) vals(13)]),vals(13)})
c.Label.String = 'Mean Crowded Offset(arcmin)';
% xlabel('Diffusion Constant (arcmin^{2}/second)')
% ylabel('Visual Acuity Threshold')
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaEffectPosition.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaEffectPosition.epsc');
%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% get the same stim size to look at for all subjects

figure('Position',[2000 200 1000 700]); %%currently removing last subject because they're an outlier and have a diff size stim
    subplot(2,2,1)
        temp = [tbl.Crowded(:).prl]-[tbl.Uncrowded(:).prl];
        [~,p,b,r] = LinRegression(...
           temp(1:end-1),...
                actualPerf(2,1:end-1) - actualPerf(1,1:end-1),... %y
            0,NaN,1,0);
        xlabel('\Delta Gaze Position All Trials')
        ylabel('\Delta Performance at Fixed Stim Size');
        axis square
        ylim([-2 2])
    subplot(2,2,2)
        [~,p,b,r] = LinRegression(...
            actualPRLatSize(2,1:end-1) - actualPRLatSize(1,1:end-1),...
                actualPerf(2,1:end-1) - actualPerf(1,1:end-1),... %y
            0,NaN,1,0);
        xlabel('\Delta Gaze Position Fixed Stim Size')
        ylabel('\Delta Performance at Fixed Stim Size');
        ylim([-2 2])
        axis square
    subplot(2,2,3)
        [~,p,b,r] = LinRegression(...
           actualPRLatSize(1,1:end-1),...
                actualPerf(1,1:end-1),... %y
            0,NaN,1,0);
        xlabel('Gaze Position Uncr Fixed Stim')
        ylabel('Unc Performance at Fixed Stim Size');
        ylim([0 1])
        axis square
    subplot(2,2,4); figure;
        [~,p,b,r] = LinRegression(...
           actualPRLatSize(2,1:end)./[tbl.Crowded.actualStimSize],...
                [tbl.Crowded.thresh]-[tbl.Uncrowded.thresh],... %y
            0,NaN,1,0);
        xlabel('Gaze Position Cro Fixed Stim')
        ylabel('Cro Performance at Fixed Stim Size');
        ylim([0 1])
        axis square
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaGazePerformance.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaGazePerformance.epsc');
%% Plot Performance drop for crowded condition same size
figure;
subplot(1,2,1)
    for ii = 1:length(subjectsAll)
        swIdx = determineSameSWCandU(ii);
        swNameU = find(saveStimInfo{1,ii}(1,:) == swIdx);
        uPerformance(ii) = subjectCond.Uncrowded.Unstabilized...
            {ii}.threshInfo.em.ecc_0.(sprintf('strokeWidth_%i',...
            saveStimInfo{1,ii}(2,swNameU))).performanceAtSize;
        swNameC = find(saveStimInfo{2,ii}(1,:) == swIdx);
        cPerformance(ii) = subjectCond.Crowded.Unstabilized...
            {ii}.threshInfo.em.ecc_0.(sprintf('strokeWidth_%i',...
            saveStimInfo{1,ii}(2,swNameC))).performanceAtSize;
        plot([0 1],[uPerformance(ii) cPerformance(ii)],'o',...
            'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
            'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
            'MarkerSize',10);
        hold on
    end
    errorbar([0.2 1.2],[mean(uPerformance) mean(cPerformance)],...
        [std(uPerformance) std(cPerformance)],...
        '-o','MarkerFaceColor','k','Color','k','MarkerSize',15);
    xlim([-.5 1.5])
    xticks([0 1])
    xticklabels({'Uncrowded','Crowded'});
    ylabel('Performance')
    [~,pP] = ttest(uPerformance, cPerformance);
    title(sprintf('Fixed Size (STD), p<%.3f',pP));
subplot(1,2,2)
    for ii = 1:length(subjectsAll)
        plot([0 1],[tbl.Uncrowded(ii).thresh tbl.Crowded(ii).thresh],'o',...
            'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
            'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
            'MarkerSize',10);
        hold on
    end
     errorbar([0.2 1.2],[mean([tbl.Uncrowded(:).thresh]) mean([tbl.Crowded(:).thresh])],...
        [std([tbl.Uncrowded(:).thresh]) std([tbl.Crowded(:).thresh])],...
        '-o','MarkerFaceColor','k','Color','k','MarkerSize',15);
    xlim([-.5 1.5])
    ylim([1 3.5])

    xticks([0 1])
    xticklabels({'Uncrowded','Crowded'});
    ylabel('Threshold Strokewidth (arcmin)')
    [~,pT] = ttest([tbl.Uncrowded(:).thresh], [tbl.Crowded(:).thresh]);
    title(sprintf('Threshold (STD), p<%.3f',pT));
    hold on
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaPerformanceFixedSize.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaPerformanceFixedSize.epsc');


%% Seperate by Individual Trial

allXC = []; allYC = []; allXU = []; allYU = [];
for ii = 1:length(subjectsAll)
    allXU = [allXU actualPRLatSizeInd{1, ii}];
    allYU = [allYU fixedStim.actualCorrect{1, ii}];
    allXC = [allXC actualPRLatSizeInd{2, ii}];
    allYC = [allYC fixedStim.actualCorrect{2, ii}];
end
figure;
idxPrlLarge = find(allXU > 10);

[~,p,b,r] = LinRegression(...
        allXU(idxPrlLarge),...%x
        allYU(idxPrlLarge),... %y
        0,NaN,1,0);
hold on
[~,p,b,r] = LinRegression(...
        allXC,...%x
        allYC,... %y
        0,NaN,1,0);

%% All 0 and 1 MAIN FIG Combine all seperated Trials
catTimeOnFlanker = [];catTimeOnFlankerP = [];
figure;
for ii = 1:length(subjectsAll)
    for c = 2%1:length(conditions)
        threshTimeTarg = .45;    
        threshTimeFlank = .55;
%         threshTime = .60;  
        performanceOnTarget(ii) = mean(...
            performance{c,ii}(find(timeSpentOnTarget{c,ii} >= threshTimeTarg)));
        
        perfNotOnFlank(ii) = mean(...
            performance{c,ii}(find(timeOnFlankers{c,ii} < threshTimeFlank)));
        
        performanceOnFlanker(ii) = mean(...
            performance{c,ii}(find(timeOnFlankers{c,ii} >= threshTimeFlank)));
        plot( performance{c,ii}, timeOnFlankers{c,ii},'o');
        
        if c == 2
%             diffTimeOnTarget(ii) = mean(...
%             timeSpentOnTarget{2,ii}(find(timeSpentOnTarget{2,ii})))- mean(...
%             timeSpentOnTarget{1,ii}(find(timeSpentOnTarget{1,ii})));
%             diffTimeOnFlankers(ii) = mean(...
%             timeOnFlankers{2,ii}(find(timeOnFlankers{2,ii})))- mean(...
%             timeOnFlankers{1,ii}(find(timeOnFlankers{1,ii})));
         diffTimeOnTarget(ii) = mean(...
            timeSpentOnTarget{2,ii})- mean(...
            timeSpentOnTarget{1,ii});
         diffTimeOnFlankers(ii) = mean(...
            timeOnFlankers{2,ii})- mean(...
            timeOnFlankers{1,ii});
        if mean(weightSpentOnTarget{2,ii}(1,:)) <= 0
            weightedOnTarget(ii) = -(abs(mean(weightSpentOnTarget{2,ii}(1,:)))/...
                mean(weightSpentOnTarget{2,ii}(2,:)))*100;
        else
            weightedOnTarget(ii) = (abs(mean(weightSpentOnTarget{2,ii}(1,:)))/...
                mean(weightSpentOnTarget{2,ii}(2,:)))*100;
        end
       
        
        end
        hold on
        catTimeOnFlanker = [catTimeOnFlanker timeSpentOnTarget{c,ii}]
        catTimeOnFlankerP= [catTimeOnFlankerP performance{c,ii}]

    end
end
plot([0 1],...
    [mean(catTimeOnFlanker(catTimeOnFlankerP == 0)) mean(catTimeOnFlanker(catTimeOnFlankerP == 1))],...
    'o','LineWidth',10','Color','k')


%% Gaze Dist
figure;
subplot(1,2,1)
plot(smallPerf(2,:), largePerf(2,:),'o');
line([0 1],[0 1])
axis([.3 1 .3 1])
subplot(1,2,2)
plot(smallPerf(1,:), largePerf(1,:),'o');
line([0 1],[0 1])
axis([.3 1 .3 1])

figure('Position',[2000 100 1000 800]);
    subplot(2,2,1)
        for ii = 1:length(subjectsAll)
            plot(smallPerf(1,ii), largePerf(1,ii),'o',...
                'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
                'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:));
            hold on
        end
        xlabel({'Small Gaze Distance', 'Performance'});
        ylabel({'Large Gaze Distance', 'Performance'});
        hold on
        line([0 1],[0 1])
        axis([.3 1 .3 1])
        errorbar(mean(smallPerf(1,:)),mean(largePerf(1,:)),...
            std(smallPerf(1,:))/2,std(smallPerf(1,:))/2,std(largePerf(1,:))/2,std(largePerf(1,:))/2,...
            'o','Color','k','MarkerFaceColor','k','MarkerSize',8);
        axis square
%         title('Uncrowded');
        text(.35,.9,{'\it better performance','\it with large distances'})
        text(.65,.4,{'\it better performance','\it with small distances'})
        xticks([.4 .6 .8 1])
        yticks([.4 .6 .8 1])
        text(.70,.95,{'\bf Uncrowded'})


    subplot(2,2,2)
        for ii = 1:length(subjectsAll)
            plot(smallPerf(2,ii), largePerf(2,ii),'o',...
                'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
                'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:));
            hold on
        end
        xlabel({'Small Gaze Distance', 'Performance'});
        ylabel({'Large Gaze Distance', 'Performance'});
        hold on
        line([0 1],[0 1])
        axis([.3 1 .3 1])
        errorbar(mean(smallPerf(2,:)),mean(largePerf(2,:)),...
            std(smallPerf(2,:))/2,std(smallPerf(2,:))/2,std(largePerf(2,:))/2,std(largePerf(2,:))/2,'o',...
                'Color','k','MarkerFaceColor','k','MarkerSize',8);
        axis square
%         title('Crowded');
        text(.35,.9,{'\it better performance','\it with large distances'})
        text(.65,.4,{'\it better performance','\it with small distances'})
        xticks([.4 .6 .8 1])
        yticks([.4 .6 .8 1])
        text(.70,.95,{'\bf Crowded'})

% figure;
     subplot(2,2,3)
        templeg(1) = errorbar([0 1],[mean(smallPerf(1,:)),mean(largePerf(1,:))],...
            [sem(smallPerf(1,:)),sem(largePerf(1,:))],'-o',...
            'LineWidth',2);
        axis square
        axis([-.5 1.5 .25 1])
        xticks([0 1])
        xticklabels({'Small Gaze','Large Gaze'})
        ylabel('Performance');
        [h,pC] = ttest(smallPerf(2,:), largePerf(2,:));
        [h,pU] = ttest(smallPerf(1,:), largePerf(1,:));

        %         title('Uncrowded(SEM)')
        hold on
        templeg(2) = errorbar([0 1],[mean(smallPerf(2,:)),mean(largePerf(2,:))],...
            [sem(smallPerf(2,:)),sem(largePerf(2,:))],'-o',...
            'LineWidth',2);
        legend(templeg,{sprintf('Uncrowded, p = %.2f', pU),...
            sprintf('Crowded, p = %.2f', pC)});
        ylim([.55 .8])
        yticks([.6 .7 .8])
        
      subplot(2,2,4)
%         figure;
%         temp1 = [tbl.Crowded(:).prl]-[tbl.Uncrowded(:).prl];
        temp1 = [tbl.Crowded(:).prl]./[tbl.Crowded(:).thresh];
        crowdingEffect = [tbl.Crowded(:).thresh]-[tbl.Uncrowded(:).thresh];
        [~,p,b,r] = LinRegression(...
            [tbl.Crowded.prl]-[tbl.Uncrowded.prl],...%x([1 2 4:13])
            crowdingEffect,... %y
            0,NaN,1,0);
        axis([-2.5 2.5 0 1.65])
        axis square
        xlabel('\Delta Gaze Distance')
        ylabel('Crowding Effect')
        hold on
        for ii = 1:length(subjectsAll)
            leg(ii) = plot([tbl.Crowded(ii).prl]-[tbl.Uncrowded(ii).prl],...
                [tbl.Crowded(ii).thresh]-[tbl.Uncrowded(ii).thresh],'o',...
                'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
                'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
                'MarkerSize',10)
        end
        yticks([0 .4 .8 1.2 1.6])
        title('')
        text(-2,0.8,{sprintf('p = %.2f',p),sprintf('r = %.2f',r)});
 saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/PLFLargevsSmall.png');
 saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/PLFLargevsSmall.epsc');

 %% Diff in Time on Target
 figure;
 subplot(2,2,1)
 [~,p,b,r] = LinRegression(...
     (weightedOnTarget),...%x([1 2 4:13])
     crowdingEffect,... %y
     0,NaN,1,0);
xlabel('Weighted Target');
ylabel('Crowding Effect');
ylim([0 1.5])
%  figure;
subplot(2,2,2)
 for ii = 1:length(subjectsAll)
      threshTimeTarg = .45;  
      idx1 = find(timeSpentOnTarget{1,ii} >= threshTimeTarg);
      idx2 = find(timeSpentOnTarget{2,ii} >= threshTimeTarg);
      
     plot([mean(timeSpentOnTarget{2,ii}(idx2))],...
         [crowdingEffect(ii)],'d');
     hold on
     plot([mean(timeSpentOnTarget{1,ii}(idx1))],...
         [crowdingEffect(ii)],'s');
     if [mean(timeSpentOnTarget{1,ii}(idx1))] > [mean(timeSpentOnTarget{2,ii}(idx2))]
         line([mean(timeSpentOnTarget{2,ii}(idx2)) mean(timeSpentOnTarget{1,ii}(idx1))],...
             [crowdingEffect(ii) crowdingEffect(ii)],'Color','b')
     else %%% more time on target in the crowded condition
         line([mean(timeSpentOnTarget{2,ii}(idx2)) mean(timeSpentOnTarget{1,ii}(idx1))],...
             [crowdingEffect(ii) crowdingEffect(ii)],'Color','r')
     end
     hold on
 end
 xlabel('Time Spent on Target')
 ylabel('Crowding Effect')
 title('Red = more time on target in Crowded');
 
%  figure;
subplot(1,2,1)
 [~,p,b,r] = LinRegression(...
            diffTimeOnTarget,...%x([1 2 4:13])
            [tbl.Crowded(:).thresh],... %y
            0,NaN,1,0);
 xlabel('\Delta Time on Target X')
 ylabel('Crowding Threshold')
  ylim([0 5])
subplot(1,2,2)
[~,p,b,r] = LinRegression(...
            diffTimeOnFlankers,...%x([1 2 4:13])
            [tbl.Crowded(:).thresh],... %y
            0,NaN,1,0);
xlabel('\Delta Time on Flankers in X')
 ylabel('Crowding Threshold')
 ylim([0 5])
%% Same figures with different colors
fig = figure;
% right_color = [255 51 239]./256;
% left_color = [72 179 175]./256;
% set(fig,'defaultAxesColorOrder',[left_color; right_color]);
subplot(1,3,1)
     legt(1) = errorbar([0 1],[mean(smallPerf(1,:)),mean(largePerf(1,:))],...
            [sem(smallPerf(1,:)),sem(largePerf(1,:))],'-d',...
            'MarkerSize',15,'MarkerFaceColor',[72 179 175]./256,...
            'Color',[72 179 175]./256,'LineWidth',3);
       
    hold on
%     yyaxis left
        legt(2) = errorbar([0 1],[mean(smallPerf(2,:)),mean(largePerf(2,:))],...
            [sem(smallPerf(2,:)),sem(largePerf(2,:))],'-d',...
            'MarkerSize',15,'MarkerFaceColor',[255 51 239]./256,...
            'Color',[255 51 239]./256,'LineWidth',3);
%         hold on
       
%         yyaxis left
%         axis square
        axis([-.5 1.5 .5 .8])
        xticks([0 1])
         xlabel('PLF (arcmin)')
        xticklabels({'Small','Large'})
        ylabel('Proportion Correct');
        [h,p] = ttest(smallPerf, largePerf);
        yticks([.5 .6 .7 .8])
        legend(legt,{'Uncrowded, n.s.',sprintf('Crowded, p = %.3f', pC)},'Location','south');
%         title('Crowded (SEM)')
 set(gca,'FontSize',14)
% saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/ttestPLFLargevsSmall.png');
%  saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/ttestPLFLargevsSmall.epsc');
  
%  figure;
subplot(1,3,2)
%  legp(1) = errorbar([0 1],[mean(meanpercentOnTS(1,:)),mean(meanpercentOnTL(1,:))],...
%      [sem(meanpercentOnTS(1,:)),sem(meanpercentOnTL(1,:))],'-s',...
%      'MarkerSize',15,'MarkerFaceColor',[72 179 175]./256,...
%      'Color',[72 179 175]./256,'LineWidth',3);
%  %  yyaxis right
% hold on
% text(-.10,[mean(meanpercentOnTS(1,:))],'S');
% text(.9,[mean(meanpercentOnTL(1,:))],'L');
% 
%  legp(2) = errorbar([2 3],[mean(meanpercentOnTS(2,:)),mean(meanpercentOnTL(2,:))],...
%      [sem(meanpercentOnTS(2,:)),sem(meanpercentOnTL(2,:))],'-s',...
%      'MarkerSize',15,'MarkerFaceColor',[255 51 239]./256,...
%      'Color',[255 51 239]./256,'LineWidth',3);
%  text([1.9],[mean(meanpercentOnTS(2,:))],'S');
%  text([2.9],[mean(meanpercentOnTL(2,:))],'L');
%  axis([-.5 3.5 .5 .8])
%  yticks([.5 .6 .7 .8])
 
%  figure;
 legp(1) = errorbar(0,mean(meanpercentOnTS(1,:)) - mean(meanpercentOnTL(1,:)),...
     [sem(meanpercentOnTS(1,:))-sem(meanpercentOnTL(1,:))],'-s',...
     'MarkerSize',15,'MarkerFaceColor',[72 179 175]./256,...
     'Color',[72 179 175]./256,'LineWidth',3);
 hold on
 legp(2) = errorbar(1,mean(meanpercentOnTS(2,:))-mean(meanpercentOnTL(2,:)),...
     [sem(meanpercentOnTS(2,:))-sem(meanpercentOnTL(2,:))],'-s',...
     'MarkerSize',15,'MarkerFaceColor',[255 51 239]./256,...
     'Color',[255 51 239]./256,'LineWidth',3);
 axis([-.5 1.5 .05 .15])
 ylabel({'Percent of Time Difference','On Target (Small-Large)'});
%  xticks([0.5 2.5])
xticks([0 1])
 xticklabels({'Uncrowded','Crowded'})
 xlabel('Condition')
%  [h,pU] = ttest(meanpercentOnTS(1,:),meanpercentOnTL(1,:));
%  [h,pC2] = ttest(meanpercentOnTS(2,:),meanpercentOnTL(2,:));
%  legend(legp,{sprintf('Uncrowded, p = %.3f', pU),sprintf('Crowded, p = %.3f', pC2)},'Location','south');
 [h, pDiff] = ttest(meanpercentOnTS(1,:)- meanpercentOnTL(1,:),...
     meanpercentOnTS(2,:) -meanpercentOnTL(2,:));
set(gca,'FontSize',14)
% saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/ttestOnTarg.png');
%  saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/ttestOnTarg.epsc');
 
figure;
% yyaxis left
subplot(2,1,1)
legnd(1) = errorbar([0 1],[mean([tbl.Uncrowded(:).thresh]) mean([tbl.Crowded(:).thresh])],...
        [sem([tbl.Uncrowded(:).thresh]) sem([tbl.Crowded(:).thresh])],...
        '-o','MarkerFaceColor','k','Color','k','MarkerSize',15,'LineWidth',3);
    ylabel('Acuity Threshold (arcmin)')
    ylim([1.2 2.8])
    yticks([1.2 1.6 2.0 2.4 2.8])
    axis square
    xticks([0 1])
    xticklabels({'Isolated','Crowded'});
    xlim([-.5 1.5])
% hold on
% yyaxis right
subplot(2,1,2)
  legt(1) = errorbar([0 1],[mean(smallPerf(1,:)),mean(largePerf(1,:))],...
            [sem(smallPerf(1,:)),sem(largePerf(1,:))],'-o',...
            'MarkerSize',12,'MarkerFaceColor',[84 93 232]./256,...
            'Color',[84 93 232]./256,'LineWidth',3);

    hold on
        legt(2) = errorbar([0 1],[mean(smallPerf(2,:)),mean(largePerf(2,:))],...
            [sem(smallPerf(2,:)),sem(largePerf(2,:))],'-o',...
            'MarkerSize',12,'MarkerFaceColor',[232 84 116]./256,...
            'Color',[232 84 116]./256,'LineWidth',3)
        axis square
        axis([-.5 1.5 .5 .8])
        xticks([0 1])
        xticklabels({'Small Gaze Offset','Large Gaze Offset'})
        ylabel('Performance');
        [h,p] = ttest(smallPerf, largePerf);
        yticks([.5 .6 .7 .8])
        legend(legt,{'Isolated','Crowded'});
%  saveas(gcf,'C:\Users\Ruccilab\Box\APLab-Projects\Grants\ClarkF31_Resubmission/ttestPLFLargevsSmall.png');
 saveas(gcf,'C:/Users/Ruccilab/Box/APLab-Projects/Grants/ClarkF31_Resubmission/ttestPLFLargevsSmall.epsc');

%% Percent of Time on Flanker vs Target

smallPerf = [];
largePerf = [];
divSTDSmall(1,:) = [.5 .05 .5 .5 .5 .5 .5 .5 .5 .5 .5 .5 1];
divSTDSmall(2,:) = [1 1.6 1 1.2 .75 .5 .01 1.2 0.5 .5 .05 .25 .25];

divSTDL(1,:) = [.5 .5 .5 .5 .5 .5 .5 .5 .5 .5 .5 .5 .01];
divSTDL(2,:) = [1.3 .8 2 1 .5 .95  1.25 2.5 2 .02 .2 .5 0.25];    
% divSTDL(2,:) = [1.3 .8 1.4 1 .5 .95  1.25 1.2...%2.5 - 8,2.0-9,2-3
%     1.5 .02 .2 .5 0.25];  
for ii = 1:length(subjectsAll)
    for c = 1:length(conditions)
        findMeanSmall(c,ii) =  mean(timeSpentOnTarget{c, ii} - ...
            (std(timeSpentOnTarget{c, ii}) * divSTDSmall(c,ii)));
        findMeanLarge(c,ii) =  mean(timeSpentOnTarget{c, ii} + ...
            (std(timeSpentOnTarget{c, ii}) * divSTDL(c,ii)));
        
        
        
        smallPerf(c,ii) = mean(performance{c,ii}(find(timeSpentOnTarget{c,ii} <...
            findMeanSmall(c,ii))));
        lengthSP(c,ii) = length(performance{c,ii}(find(timeSpentOnTarget{c,ii} < ...
            findMeanSmall(c,ii))));
        largePerf(c,ii) = mean(performance{c,ii}(find(timeSpentOnTarget{c,ii} > ...
            findMeanLarge(c,ii))));
        lengthLP(c,ii) = length(performance{c,ii}(find(timeSpentOnTarget{c,ii} > ...
            findMeanLarge(c,ii))));
        
        avTimeSpentBin.small(c,ii) = mean(timeSpentOnTarget{c,ii}(find(timeSpentOnTarget{c,ii} <...
            findMeanSmall(c,ii))));
        avTimeSpentBin.large(c,ii) = mean(timeSpentOnTarget{c,ii}(find(timeSpentOnTarget{c,ii} >...
            findMeanLarge(c,ii))));
        
    end
end
% idx = find()
idx = find((lengthSP(2,:)) > 20 & lengthLP(2,:) >= 15)

tempC = [tbl.Crowded(:).thresh];
figure;
[~,p,b,r] = LinRegression(...
        [avTimeSpentBin.large(2,idx) - avTimeSpentBin.small(2,idx)],...%x
        tempC(idx),... %y
        0,NaN,1,0);

figure;
for ii = idx%1:length(largePerf)
%     plot([0 1], double([(smallPerf(1,ii)) (largePerf(1,ii))]),'-o','Color','k');
    hold on
    plot([0 1], double([(smallPerf(2,ii)) (largePerf(2,ii))]),'-o','Color','r');
    hold on
end


figure;
subplot(1,3,3)
% boxplot([(smallPerf(1,:)),(largePerf(1,:))],[1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2])
legt(1) = errorbar([0 1],[mean(smallPerf(1,idx)),mean(largePerf(1,idx))],...
    [sem(smallPerf(1,idx)),sem(largePerf(1,idx))],'-o',...
    'MarkerSize',12,'MarkerFaceColor',[72 179 175]./256,...
    'Color',[72 179 175]./256,'LineWidth',3);
hold on
% boxplot([(smallPerf(2,:)),(largePerf(2,:))],2+[1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2])

hold on
legt(2) = errorbar([0 1],[mean(smallPerf(2,idx)),mean(largePerf(2,idx))],...
    [sem(smallPerf(2,idx)),sem(largePerf(2,idx))],'-o',...
    'MarkerSize',12,'MarkerFaceColor',[255 51 239]./256,...
    'Color',[255 51 239]./256,'LineWidth',3)
% axis square
axis([-.5 1.5 .5 .8])
xticks([0 1])
xticklabels({'Min','Max'})
xlabel('Time on Target')
ylabel('Proportion Correct');
[h,p1] = ttest(smallPerf(1,idx), largePerf(1,idx));
[h,p2] = ttest(smallPerf(2,idx), largePerf(2,idx));
yticks([.5 .6 .7 .8])
%  plot([0 1],[mean(meanpercentOnTS(2,:)),mean(meanpercentOnTL(2,:))],'--s',...
%             'MarkerSize',8,'MarkerFaceColor',[255 51 239]./256,...
%             'Color',[255 51 239]./256,'LineWidth',1);
%  plot([0 1],[mean(meanpercentOnTS(1,:)),mean(meanpercentOnTL(1,:))],'--s',...
%             'MarkerSize',8,'MarkerFaceColor',[72 179 175]./256,...
%             'Color',[72 179 175]./256,'LineWidth',1);

legend(legt,{'Uncrowded p = n.s.',sprintf('Crowded p = %.3f', p2)},...
    'Location','south');
 set(gca,'FontSize',14)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/timeOnTargetLargevsSmall.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/timeOnTargetLargevsSmall.epsc');
  






crowdingEffect(1,:) = [tbl.Crowded(:).thresh]-[tbl.Uncrowded(:).thresh];
crowdingEffect(2,:) = performanceThresh(2,:) - performanceThresh(1,:)
idx = find(averageTime(2,:) > 0);
figure;
subplot(2,3,1)
 [~,p,b,r] = LinRegression(...
        [averageTime(2,idx)],...%x
        crowdingEffect(idx),... %y
        0,NaN,1,0);
xlabel('% on C Flanker')
ylabel('Crowding Effect')
axis([0 0.6 -2 2])

subplot(2,3,2)
 [~,p,b,r] = LinRegression(...
        [averageTimeTarget(2,idx)],...%x
         crowdingEffect(idx),... %y
        0,NaN,1,0);
xlabel('% on C Target')
ylabel('Difference in Performance')
axis([0 0.6 -2 2])

subplot(2,3,3)
 [~,p,b,r] = LinRegression(...
        [averageTime(1,idx)],...%x
        performanceThresh(1,idx) - performanceThresh(2,idx) ,... %y
        0,NaN,1,0);
xlabel('% on C Flanker')
ylabel('Crowding Effect')
axis([0 .6 -.4 .4])

subplot(2,3,4)
 [~,p,b,r] = LinRegression(...
        [averageTimeTarget(1,idx)],...%x
         performanceThresh(1,idx) - performanceThresh(2,idx),... %y
        0,NaN,1,0);
xlabel('% on C Target')
ylabel('Crowding Effect')
axis([0 .6 -.4 .4])

subplot(2,3,5)
 [~,p,b,r] = LinRegression(...
        [averageTime(1,idx)],...%x
        performanceThresh(1,idx) - performanceThresh(2,idx) ,... %y
        0,NaN,1,0);
xlabel('% on U Flanker')
ylabel('Crowding Effect')
axis([0 0.6 -.4 .4])

subplot(2,3,6)
 [~,p,b,r] = LinRegression(...
        [averageTimeTarget(1,idx)],...%x
         performanceThresh(1,idx) - performanceThresh(2,idx),... %y
        0,NaN,1,0);
xlabel('% on U Target')
ylabel('Difference in Performance')
axis([0 .6 -.4 .4])
    
%%
    for ii = 1:length(performanceMeans)
        performanceMeans(1,ii) = mean([performance{1,ii}]);
        performanceMeans(2,ii) = mean([performance{2,ii}]);
    end

figure;
    subplot(1,2,1)
    [~,p,b,r] = LinRegression(...
        [timeSpentOnTarget(2,exIdx)-timeSpentOnTarget(1,exIdx)],...%x
        performanceMeans(1,exIdx)-performanceMeans(2,exIdx),... %y
        0,NaN,1,0);
    subplot(1,2,2)
    [~,p,b,r] = LinRegression(...
        [timeOnFlankers(2,exIdx)-timeOnFlankers(1,exIdx)],...%x
        performanceMeans(1,exIdx)-performanceMeans(2,exIdx),... %y
        0,NaN,1,0);














