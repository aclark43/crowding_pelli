function [em] = emAnalysisUpdated11102020(em, pptrials, valid, trialChar, title_str, params, uEcc, filepath, figures)
%%Updated! 08/08/2019
% uSW = unique(trialChar.TargetStrokewidth);
uST = unique(round(trialChar.TargetSize,1));
valueGroup.ecc = sprintf('ecc_%i', uEcc);
%% Categorizes (whether drift or ms) for further analysis
if ~figures.FIXATION_ANALYSIS
    counter = 1;
    em.allTraces.xALL = [];
    em.allTraces.yALL = [];
    em.allTraces.shortAnalysis.xALL = [];
    em.allTraces.shortAnalysis.yALL = [];
    em.allTraces.xALLRecentered = [];
    em.allTraces.yALLRecentered = [];
    for ii = 1:length(uST)
        
        trialIdx = find(round(trialChar.TargetSize,1) == uST(ii));
        partMSIdx = intersect(find(valid.partialMS == 1), trialIdx);
        sIdx = intersect(find(valid.s == 1), trialIdx);
        fixIdx = find(trialChar.TimeFixationON > 0);
        
        
        if params.D
            idx = intersect(find(valid.d == 1 ), trialIdx);
        elseif params.MS
            idx = intersect(find(valid.ms == 1), trialIdx);
        elseif params.DMS
            idx = intersect(find(valid.dms == 1), trialIdx);
        end
        
        valueGroup.idx = idx;
        valueGroup.strokeWidth = sprintf('strokeWidth_%i', (ii));
        
        valueGroup.stimulusSize = uST(ii);
        
        numberTrials(ii) = numel(idx);
        if numberTrials(ii) < 1
            spnAmpEvaluate.ccDist(ii) = 0;
            spnAmpEvaluate.stimulusSize(ii) = 0;%em.ecc_0.(strokeWidth).stimulusSize;
            spnAmpEvaluate.SW(ii) = 0;
            continue;
        end
        
        %%Calculates all drift paramters inside POI
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        em = calculateDriftChar(valueGroup,params,pptrials,em);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %         addSpace = length(em.(valueGroup.ecc).(valueGroup.strokeWidth).position);
        
        for numTraces = 1:length(em.(valueGroup.ecc).(valueGroup.strokeWidth).position)
            
            em.allTraces.x{counter} = ...
                em.(valueGroup.ecc).(valueGroup.strokeWidth).position(numTraces).x;
            em.allTraces.y{counter} = ...
                em.(valueGroup.ecc).(valueGroup.strokeWidth).position(numTraces).y;
            
            if strcmp(params.stabilization ,'Stabilized')
                tempPathStab = em.(valueGroup.ecc).(valueGroup.strokeWidth).stabInfo;
                em.allTraces.stabInfo(counter).stabX = tempPathStab(numTraces).stabX;
                em.allTraces.stabInfo(counter).stabY = tempPathStab(numTraces).stabY;
                em.allTraces.stabInfo(counter).stabXDiff = em.allTraces.x{counter} - em.allTraces.stabInfo(counter).stabX;
                em.allTraces.stabInfo(counter).stabYDiff = em.allTraces.y{counter} - em.allTraces.stabInfo(counter).stabY;
            end
            
            em.allTraces.xRecentered{counter} = ...
                em.(valueGroup.ecc).(valueGroup.strokeWidth).recenteredposition(numTraces).x;
            em.allTraces.yRecentered{counter} = ...
                em.(valueGroup.ecc).(valueGroup.strokeWidth).recenteredposition(numTraces).y;
            
            em.allTraces.xALLRecentered = [em.allTraces.xALLRecentered em.allTraces.xRecentered{counter}];
            em.allTraces.yALLRecentered = [em.allTraces.yALLRecentered em.allTraces.yRecentered{counter}];
            
            em.allTraces.xALL = [em.allTraces.xALL em.allTraces.x{counter}];
            em.allTraces.yALL = [em.allTraces.yALL em.allTraces.y{counter}];
            
            %%%%%%%%%%%%
            em.allTraces.shortAnalysis.x{counter} = ...
                em.(valueGroup.ecc).(valueGroup.strokeWidth).position(numTraces).x(1:round(175/(1000/params.sampling)));
            em.allTraces.shortAnalysis.y{counter} = ...
                em.(valueGroup.ecc).(valueGroup.strokeWidth).position(numTraces).y(1:round(175/(1000/params.sampling)));
            
            em.allTraces.shortAnalysis.xRecentered{counter} = ...
                em.(valueGroup.ecc).(valueGroup.strokeWidth).recenteredposition(numTraces).x;
            em.allTraces.shortAnalysis.yRecentered{counter} = ...
                em.(valueGroup.ecc).(valueGroup.strokeWidth).recenteredposition(numTraces).y;
            
            %             em.allTraces.shortAnalysis.xALLRecentered = [em.allTraces.shortAnalysis.xALLRecentered em.allTraces.shortAnalysis.xRecentered{counter}];
            %             em.allTraces.shortAnalysis.yALLRecentered = [em.allTraces.shortAnalysis.yALLRecentered em.allTraces.shortAnalysis.yRecentered{counter}];
            
            em.allTraces.shortAnalysis.xALL = [em.allTraces.shortAnalysis.xALL em.allTraces.shortAnalysis.x{counter}];
            em.allTraces.shortAnalysis.yALL = [em.allTraces.shortAnalysis.yALL em.allTraces.shortAnalysis.y{counter}];
            
            counter = counter + 1;
        end
        %         em.allY = em.(valueGroup.ecc).(valueGroup.strokeWidth).position.y;
        
        figure;
        generateHeatMapSimple( ...
            em.(valueGroup.ecc).(valueGroup.strokeWidth).xAll, ...
            em.(valueGroup.ecc).(valueGroup.strokeWidth).yAll, ...
            'Bins', 30,...
            'StimulusSize', uST(ii),...
            'AxisValue', 30,...
            'Uncrowded', params.crowded+1,...
            'Borders', 1);
        title(params.subject);
        
        if params.crowded
            temp = 'Crowded';
            saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/HeatMaps/HeatMap%s%sCrowded=%.2f.png',...
                params.subject, params.em, uST(ii)));
        elseif ~params.crowded
            temp = 'Uncrowded';
            saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/HeatMaps/HeatMap%s%s=%.2f.png',...
                params.subject, params.em, uST(ii)));
            saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/HeatMaps/HeatMap%s%sUncrowded=%.2f.png',...
                params.subject, params.em, uST(ii)));
        end
        if params.DMS %&& sum(valid.ms) > 1%params.MS || params.DMS
            MSvalueGroup.idx = intersect(find(valid.dms == 1), trialIdx);
            MSvalueGroup.strokeWidth = sprintf('strokeWidth_%i', (ii));
            MSvalueGroup.ecc = sprintf('ecc_%i', uEcc);
            MSvalueGroup.stimulusSize = uST(ii);
            
            em.msAnalysis.(MSvalueGroup.strokeWidth) = ...
                calculateMSChar(MSvalueGroup,params,pptrials,em);
        else
%             em.msAnalysis.(MSvalueGroup.strokeWidth).msRate = 0;
        end
        
        spnAmpEvaluate.ccDist(ii) = em.(valueGroup.ecc).(valueGroup.strokeWidth).ccDistance;
        spnAmpEvaluate.stimulusSize(ii) = uST(ii);%em.ecc_0.(strokeWidth).stimulusSize;
        spnAmpEvaluate.SW(ii) = (ii);
        %         tempName = ;
        save(sprintf('MATFiles/%s_%s_SpanAmpEval.mat',params.subject,temp),'spnAmpEvaluate');
    end
    if any(numberTrials > 2)
        %% All Trials Analysis
        for analysisType = 1:2
            if analysisType == 1
                em.allTraces = ...
                    recalculateDriftParamsAllTrials(em.allTraces, params, analysisType);
                figure;
                if strcmp(params.subject,'HUX23')
                    binNum = 300;
                else
                    binNum = 30;
                end
                generateHeatMapSimple( ...
                    em.allTraces.xALL, ...
                    em.allTraces.yALL, ...
                    'Bins', binNum,...
                    'StimulusSize', uST(round(length(uST)/2)),...
                    'AxisValue', 30,...
                    'Uncrowded', 4,...
                    'Borders', 0);
%                 x = em.allTraces.xALL;
%                 y = em.allTraces.yALL;
%                 x2 = 2.291; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
%                 bcea =  2*x2*pi * std(x) * std(y) * (1-corrcoef(x,y).^2).^0.5; %smaller BCEA correlates to more stable fixation

                if params.crowded
                    saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/HeatMaps/HeatMap%s%sCrowded=ALLCOMBINED.png',...
                        params.subject, params.em));
                elseif ~params.crowded
                    saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/HeatMaps/HeatMap%s%s=ALLCOMBINED.png',...
                        params.subject, params.em));
                    saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/HeatMaps/HeatMap%s%sUncrowded=ALLCOMBINED.png',...
                        params.subject, params.em));
                end
                if figures.HUX_RUN
                    
                    saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/task/AllTracesHeatMap%s%s.png',...
                        params.subject, params.em));
                end
            elseif analysisType == 2 %%Short Analysis
                em.allTraces.shortAnalysis = ...
                    recalculateDriftParamsAllTrials(em.allTraces.shortAnalysis, params, analysisType);
            end
        end
    end
    if params.crowded
        stringC = 'Crowded';
    else
        stringC = 'Uncrowded';
    end
    filename = sprintf('MATFiles/%s_%s_%s_%s_SpanAmpEval.mat', ...
        trialChar.Subject, stringC, params.stabilization, (valueGroup.ecc));
    save(filename,'spnAmpEvaluate')
end


%% Create MS Landing Map for All MS
% if params.MS
%     if isfield(em, 'ms')
%         allMSStruct = getfield(em.ms,(eccentricity));
%         listSW = (fieldnames(allMSStruct));
%         strokeWidth = 'strokeWidth_All';
%         em.ms.(eccentricity).strokeWidth_All = [];
%         if numel(listSW) > 1
%             for msSWCounter = 1:length(listSW)
%                 em.ms.(eccentricity).strokeWidth_All = ...
%                     [em.ms.(eccentricity).strokeWidth_All...
%                     em.ms.(eccentricity).(char(listSW(msSWCounter)))];
%                 %          numList = [char(listSW(msSWCounter));
%             end
%         else
%             em.ms.(eccentricity).strokeWidth_All = ...
%                 em.ms.(eccentricity).(char(listSW));
%         end
%         msLocation(em.ms, params, 0, trialChar, strokeWidth, ...
%             uEcc, title_str, mean(msRate), figures.FIXATION_ANALYSIS)
%     end
% end

% Generates Fixation Heat Map%%
if figures.FIXATION_ANALYSIS
    for timeBinIdx = 5
        params.timeString = sprintf('TimeBin=%i', timeBinIdx);
        params.timeBin = timeBinIdx;
        
        fixIdx = find(trialChar.TimeFixationON > 0);
        
        xValues = [];
        yValues = [];
        xOffset = [];
        yOffset = [];
        counter = 1;
        for i = fixIdx
            timeOn = round(pptrials{i}.TimeFixationON/params.samplingRate)+1;
            timeOff = floor(min(pptrials{i}.TimeFixationOFF/(1000/params.sampling), length(pptrials{i}.x.position)));
            t = (timeOff-timeOn)*(1000/params.sampling);
            x = pptrials{i}.x.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.xoffset * params.pixelAngle(i);
            y = pptrials{i}.y.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.yoffset * params.pixelAngle(i);
            
            counter = 1;
            for ndrift = 1:length(pptrials{i}.drifts.start)
                if pptrials{i}.drifts.duration(ndrift) > 50
                    startTime = pptrials{i}.drifts.start(ndrift)/(1000/params.sampling);
                    endTime = pptrials{i}.drifts.start(ndrift)/(1000/params.sampling) + ...
                        pptrials{i}.drifts.duration(ndrift)/(1000/params.sampling);
                    xDrift{counter} = pptrials{i}.x.position(ceil(startTime):floor(endTime)) + ...
                        pptrials{i}.xoffset * params.pixelAngle(i);
                    yDrift{counter} = pptrials{i}.y.position(ceil(startTime):floor(endTime)) + ...
                        pptrials{i}.yoffset * params.pixelAngle(i);
                    counter = counter + 1;
                end
            end
            xDriftAll = []; yDriftAll = [];
            if length(pptrials{i}.drifts.start) > 0
                for nLongDrift = 1:length(xDrift)
                    [~, instSpX{nLongDrift,1},...
                        instSpY{nLongDrift,1},...
                        mn_speed{nLongDrift},...
                        driftAngle{nLongDrift,1},...
                        curvature{nLongDrift},...
                        varx{nLongDrift},...
                        vary{nLongDrift},...
                        bcea{nLongDrift},...
                        span{nLongDrift}, ...
                        amplitude(nLongDrift), ...
                        prlDistance(nLongDrift),...
                        prlDistanceX(nLongDrift), ...
                        prlDistanceY(nLongDrift)] = ...
                        getDriftChar(xDrift{nLongDrift}, yDrift{nLongDrift}, 11, 1, Inf); %41
                    xDriftAll = [xDrift{nLongDrift} xDriftAll];
                    yDriftAll = [yDrift{nLongDrift} yDriftAll];
                end
                fixation.instSpX = instSpX;
                fixation.instSpY = instSpY;
                fixation.mn_speed = ([mn_speed{:}]);
                fixation.driftAngle = driftAngle;
                fixation.curvature = ([curvature{:}]);
                fixation.varx = [varx{:}];
                fixation.vary = [vary{:}];
                fixation.bcea = [bcea{:}];
                fixation.span = [span{:}];
                fixation.amplitude = amplitude(:);
                fixation.prlDistance = prlDistance;
                fixation.prlDistanceX = prlDistanceX;
                fixation.prlDistanceY = prlDistanceY;
            end
            
            %             figure;
            driftIdx = (yDriftAll < 60 & yDriftAll > -60 & xDriftAll < 60 & xDriftAll > -60);
            %             generateHeatMapSimple( ...
            %                 xDriftAll(driftIdx), ...
            %                 yDriftAll(driftIdx), ...
            %                 'Bins', 60,...
            %                 'StimulusSize', 0,...
            %                 'AxisValue', 60,...
            %                 'Uncrowded', 4,...
            %                 'Borders', 1);
            
            
            xValues = [xValues, x];
            yValues = [yValues, y];
            
            xOffset = [xOffset, pptrials{i}.xoffset * params.pixelAngle(i)];
            yOffset = [yOffset, pptrials{i}.yoffset * params.pixelAngle(i)];
            
            fixationInfo.position(counter).x = x;
            fixationInfo.position(counter).y = y;
            
            counterms = 1;
            if ~isempty(pptrials{i}.microsaccades.start) && any(pptrials{i}.microsaccades.start) > 0
                msIdx(counterms) = i;
            else
                msIdx(counterms) = 0;
            end
            counterms = counterms + 1;
        end
        clear msInfo
        tempIdx = (find(msIdx > 0));
        [~, msInfo.msRates] = buildRates(pptrials(msIdx(tempIdx)));
        counterms = 1;
        for t = tempIdx
            msInfo.startTime{counterms} = pptrials{msIdx(t)}.microsaccades.start(...
                pptrials{msIdx(t)}.microsaccades.start  > 0);
            msInfo.startPos{counterms}(1,:) = pptrials{msIdx(t)}.x.position( msInfo.startTime{counterms});
            msInfo.startPos{counterms}(2,:) = pptrials{msIdx(t)}.y.position( msInfo.startTime{counterms});
            
            msInfo.endTime{counterms} = msInfo.startTime{counterms} + ...
                pptrials{msIdx(t)}.microsaccades.duration( pptrials{msIdx(t)}.microsaccades.start  > 0);
            if length(pptrials{msIdx(t)}.x.position) < msInfo.endTime{counterms}(end)
                msInfo.endPos{counterms}(1,:) = pptrials{msIdx(t)}.x.position( msInfo.endTime{counterms}(1:end-1));
                msInfo.endPos{counterms}(2,:) = pptrials{msIdx(t)}.y.position( msInfo.endTime{counterms}(1:end-1));
            else
                msInfo.endPos{counterms}(1,:) = pptrials{msIdx(t)}.x.position( msInfo.endTime{counterms});
                msInfo.endPos{counterms}(2,:) = pptrials{msIdx(t)}.y.position( msInfo.endTime{counterms});
            end
            counterms = counterms + 1;
        end
        
        if strcmp('ALL',params.ses)
            sValsX = [];
            sValsY = [];
            eValsX = [];
            eValsY = [];
            for c = 1:length(msInfo.startPos)
                if length( msInfo.startPos{c}(1,:)) ~=  length(msInfo.endPos{c}(1,:))
                    continue;
                else
                    sValsX = [sValsX msInfo.startPos{c}(1,:)];
                    sValsY = [sValsY msInfo.startPos{c}(2,:)];
                    eValsX = [eValsX msInfo.endPos{c}(1,:)];
                    eValsY = [eValsY msInfo.endPos{c}(2,:)];
                end
            end
            
            
            [thetaE,rhoE] = cart2pol(eValsX-sValsX,eValsY-sValsY);
            
            figure;
            subplot(1,2,1)
            polarplot(thetaE,rhoE,'o')
            rlim([0 60])
            title('Aligned to Start')
            subplot(1,2,2)
            templeg(1) = plot(sValsX,sValsY,'o','Color','r');
            hold on
            templeg(2) = plot(eValsX,eValsY,'o','Color','b');
            axis([-60,60 -60,60])
            rectangle('Position',[-5 -5 10 10])
            axis square
            legend(templeg,{'Start','End'});
            title(sprintf('%i MS',length(sValsX)));
            suptitle(sprintf('%s',params.subject));
            if figures.HUX_RUN
                saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/FixationMaps/BySession/%s/%s_%i_MSFixation%s.png',...
                    params.ses,params.em,round(t,-2),params.subject));
                saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%s/%s_%i_MSFixation%s.png',...
                    params.ses,params.em,round(t,-2),params.subject));
                saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%s/%s_%i_MSFixation%s.fig',...
                    params.ses,params.em,round(t,-2),params.subject));
            end
            msInfo.theta = thetaE;
            msInfo.rho = rhoE;
        end
        if strcmp('ses1',params.session{1})
            filenameSes1 = sprintf('MATFiles/%s_Ses1FixationMSInfo.mat', ...
                trialChar.Subject);
            save(filenameSes1,'msInfo')
        end
        em.ecc_0.fixation = fixationInfo;
        [ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/100)),31); %Checks strange artifacts
        [ xValues, yValues] = checkXYBound(xValues, yValues, 60); %Checks boundries
        
        figure;
        generateHeatMapSimple( ...
            xValues, ...
            yValues, ...
            'Bins', 60,...
            'StimulusSize', 0,...
            'AxisValue', 60,...
            'Uncrowded', 4,...
            'Borders', 1);
        title(params.subject);
        if figures.HUX_RUN
            saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/FixationMaps/BySession/%s/%s_%i_HeatMapFixation%s.png',...
                params.ses,params.em,round(t,-2),params.subject));
            saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%s/%s_%i_HeatMapFixation%s.png',...
                params.ses,params.em,round(t,-2),params.subject));
            saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%s/%s_%i_HeatMapFixation%s.fig',...
                params.ses,params.em,round(t,-2),params.subject));
        else
            saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/HeatMaps/HeatMapFixation%s.png',...
                params.subject));
        end
        figure;
        subplot(1,2,1)
        generateHeatMapSimple( ...
            xValues, ...
            yValues, ...
            'Bins', 30,...
            'StimulusSize', 0,...
            'AxisValue', 30,...
            'Uncrowded', 4,...
            'Borders', 1);
        title(params.subject);
        
        subplot(1,2,2)
        leg(1) = histogram(xOffset);
        hold on
        leg(2) = histogram(yOffset);
        legend(leg,{'xoff','yoff'},'Location','northwest');
        axis square
        line([0 0] ,[0 60],'LineWidth',3,'Color','k');
        
        if figures.HUX_RUN
            saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/FixationMaps/BySession/%s/%s_%i_HeatMapWithOffsetFixation%s.png',...
                params.ses,params.em,round(t,-2),params.subject));
            saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%s/%s_%i_HeatMapWithOffsetFixation%s.png',...
                params.ses,params.em,round(t,-2),params.subject));
        else
            saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/HeatMaps/HeatMapFixation_WithOffset%s.png',...
                params.subject));
        end
        
        [fixation.MeanDistanceFrom0, fixation.dAbs] = generateDirectionMap(xValues, yValues, title_str );
        
        counterFix = 1;
        for ii = fixIdx
            timeOn = ceil(pptrials{ii}.TimeFixationON/params.sampling);
            timeOff = round(pptrials{ii}.TimeFixationOFF/params.sampling);
            if timeOff > length(pptrials{ii}.x.position)
                timeOff = length(pptrials{ii}.x.position);
            end
            fixation.tracesX{counterFix} = pptrials{ii}.x.position(ceil(timeOn):floor(timeOff)) + (pptrials{ii}.pixelAngle * pptrials{ii}.xoffset);
            fixation.tracesY{counterFix} = pptrials{ii}.y.position(ceil(timeOn):floor(timeOff)) + (pptrials{ii}.pixelAngle * pptrials{ii}.yoffset);
            fixation.velocityX{counterFix} = pptrials{ii}.x.velocity;
            fixation.velocityY{counterFix} = pptrials{ii}.y.velocity;
            counterFix = counterFix + 1;
            %                 end
        end
        %             generateDriftVelocityMap(fixation.velocityX, fixation.velocityY,...
        %                 0, params, title_str, trialChar)
        if strcmp('ALL',params.session{1})
            filename = sprintf('MATFiles/%s_FixationResults.mat', trialChar.Subject);
            save(filename,'fixation')
        end
        %         end
    end
end

%% Graphs Drift Characteristics%%
% if isfield(em, 'ecc_0')
%     if ~ isfield((em.ecc_0), 'position')
%         if exist('validSW','var')
%             if params.D
%
%                 generateDriftAmpAndSpanGraphs...
%                     ( meanDriftAmplitude,  meanDriftSpan, stdDriftAmplitude, stdDriftSpan, title_str);
%
%             end
%         end
%     end
% end
function path = recalculateDriftParamsAllTrials(path, params, type)
[~,path.Bias, path.dCoefDsq, ~,path.Dsq, ...
    path.SingleSegmentDsq,...
    path.TimeDsq,~,...
    path.RegLineDsq] = ...
    CalculateDiffusionCoef(params.sampling, struct('x',path.x, 'y', path.y));

% r_ellipse can be used for plotting ; original_mu offset
if type == 1
    %     [path.VXRecenteredALL,...
    %         path.original_muRecenteredALL,...
    %         path.r_ellipseRecenteredALL] = tgdt_fit_2d_gaussian(path.xALLRecentered,...
    %         path.yALLRecentered,0.99);
end

for numTrials = 1:length(path.x)
    [~, path.instSpX{numTrials},...
        path.instSpY{numTrials},...
        path.mn_speed{numTrials},...
        path.driftAngle{numTrials},...
        path.curvature{numTrials},...
        path.varx{numTrials},...
        path.vary{numTrials}, ...
        path.bcea{numTrials}, ...
        path.span(numTrials), ...
        path.amplitude(numTrials), ...
        path.prlDistance(numTrials),...
        path.prlDistanceX(numTrials), ...
        path.prlDistanceY(numTrials)] = ...
        getDriftChar(cell2mat(path.x(numTrials)), ...
        cell2mat(path.y(numTrials)), ...
        41, 1, 250);
    %
    
    %     [path.VX{numTrials},...
    %         path.original_mu{numTrials},...
    %         path.r_ellipse{numTrials}] = ...
    %         tgdt_fit_2d_gaussian(cell2mat(path.x(numTrials)), ...
    %         cell2mat(path.y(numTrials)), 0.99);
    
    %     [path.VXRecentered{numTrials},...
    %         path.original_muRecentered{numTrials},...
    %         path.r_ellipseRecentered{numTrials}] = ...
    %         tgdt_fit_2d_gaussian(cell2mat(path.xRecentered(numTrials)), ...
    %         cell2mat(path.yRecentered(numTrials)), 0.99);
    
    limit.xmin = floor(min(path.xALL));
    limit.xmax = ceil(max(path.xALL));
    limit.ymin = floor(min(path.yALL));
    limit.ymax = ceil(max(path.yALL));
    
    path.areaCovered = CalculateTheArea(path.xALL,path.yALL,...
        0.68, max(abs(cell2mat(struct2cell(limit)))), 20);
    
    x = path.x{numTrials};
    y = path.y{numTrials};
    dist_from_start = cumsum( [0, sqrt((x(2:end)-x(1:end-1)).^2 + ...
        (y(2:end)-y(1:end-1)).^2)] );
    
    path.old_curvatureAshley  = dist_from_start(length(dist_from_start))/...
        mean(sqrt(x.^2 + y.^2));
    
    [path.old_curvature2(numTrials), ~, path.old_curvature3(numTrials)] = ...
        CalculateLengthAndCurvature(x, y);
end


