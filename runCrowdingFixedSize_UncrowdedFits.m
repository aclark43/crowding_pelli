% data.Krish = load('X:\Ashley\Crowding\Krish\Crowded_Stabilized_0ecc_sw2.mat');
% data.Z055 = load('X:\Ashley\Crowding\Z055\crowded_stabilized_0ecc.mat');
% data.Z114 = load('X:\Ashley\Crowding\Z114\stabilized_2arcminSW.mat');
clear all
clc

% data.Stabilized.Z091 = load(...
%     'X:\Ashley\Crowding\Z091\Uncrowded_Stabilized_PEST.mat');
%  data.Unstabilized.Z091 = load(...
%     'X:\Ashley\Crowding\Z091\Crowded_Unstabilized_27.mat');

% data.Stabilized.Krish = load(...
%     'X:\Ashley\Crowding\Krish\Crowded_Stabilized_0ecc_37.mat');
% data.Unstabilized.Krish = load(...
%     'X:\Ashley\Crowding\Krish\Crowded_Unstabilized_0ecc_30.mat');

data.Stabilized.Z160 = load(...
    'X:\Ashley\Crowding\Z160\Uncrowded_Stabilized_0ecc_PEST.mat');
data.Unstabilized.Z160 = load(...
    'X:\Ashley\Crowding\Z160\Uncrowded_Unstabilized_0ecc_PEST.mat');

data.Stabilized.A146 = load(...
    'X:\Ashley\Crowding\A146\Uncrowded_Stabilized_PEST.mat');
data.Unstabilized.A146 = load(...
    'X:\Ashley\Crowding\A146\Uncrowded_Unstabilized_PEST.mat');

data.Stabilized.SM = load(...
    'X:\Ashley\Crowding\SM\Uncrowded_Stabilized_PEST.mat');
data.Unstabilized.SM = load(...
    'X:\Ashley\Crowding\SM\Uncrowded_Unstabilized_PEST.mat');

% data.Stabilized.Ben = load(...
%     'X:\Ashley\Crowding\Ben\Crowded_Stabilized_46.mat');
% data.Unstabilized.Ben = load(...
%     'X:\Ashley\Crowding\Ben\Crowded_Unstabilized_40.mat');

data.Stabilized.Z055 = load(...
    'X:\Ashley\Crowding\Z055\Uncrowded_Stabilized_PEST.mat');
data.Unstabilized.Z055 = load(...
    'X:\Ashley\Crowding\Z055\Uncrowded_Unstabilized_PEST.mat');

% data.Stabilized.A144 = load(...
%     'X:\Ashley\Crowding\A144\Crowded_Stabilized_28.mat');
% data.Unstabilized.A144 = load(...
%     'X:\Ashley\Crowding\A144\Crowded_Unstabilized_26.mat');

%--------------------------------------------------------------

subjectsAll = fieldnames(data.Unstabilized);
conditionsS = {'Stabilized','Unstabilized'};

reprocess = 1;

%--------------------------------------------------------------
if reprocess
    counterStruct = 1;
    for e = 1:length(subjectsAll)
        for s = 1:2
            temp = data.(conditionsS{s}).(subjectsAll{e});
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            
            temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
            processedPPTrials = newEyerisEISRead(temppptrials);
            
            counter = 1;
            for ii = 1:length(processedPPTrials)
                
                dataAll{e}.sw(counter) = (double(processedPPTrials{ii}.TargetStrokewidth)/5)*2;
                dataAll{e}.response(ii) = processedPPTrials{ii}.Response;
                dataAll{e}.correct(ii) = processedPPTrials{ii}.Correct;
                dataAll{e}.snellenAcuity(counter) = (dataAll{e}.sw(counter))/4;
                dataAll{e}.logMar(counter) = dataAll{e}.snellenAcuity(counter)*20;
                dataAll{e}.x{counter} = processedPPTrials{ii}.x;
                dataAll{e}.y{counter} = processedPPTrials{ii}.y;
                counter = counter + 1;
                
                tableData(counterStruct).subject = e;
                tableData(counterStruct).stabilized = s;
                tableData(counterStruct).sw = (double(processedPPTrials{ii}.TargetStrokewidth)/5)*2;
                tableData(counterStruct).response = processedPPTrials{ii}.Response;
                tableData(counterStruct).correct = processedPPTrials{ii}.Correct;
                tableData(counterStruct).snellenAcuity = (dataAll{e}.sw(counter-1))/2;
                tableData(counterStruct).logMar = dataAll{e}.snellenAcuity(counter-1)*20;
                tableData(counterStruct).x = processedPPTrials{ii}.x;
                tableData(counterStruct).y = processedPPTrials{ii}.y;
                counterStruct = counterStruct + 1;
            end
            %     close all
            figure;
            idxTrials = find(dataAll{e}.correct < 3);
            dataAll{e}.idxTrials = idxTrials;
            acrossSubStats.numTrials(e,s) = length(idxTrials);
            
            [thresh{e}, par{e}, threshB{e}, xi{e}, yi{e}, chiSq{e}] = ...
                psyfitCrowding(dataAll{e}.sw(idxTrials), ...
                dataAll{e}.correct(idxTrials), 'DistType', ...
                'Normal','Chance', .25, 'Extra');
            
            xlabel('Uncrowded Stim Width');
            ylabel('Performance');
            xticks([0.5:.5:2.5])
            xticklabels({'0.5','1','1.5','2','2.5'});
            acrossSubStats.thresh(e,s) = thresh{e};
            title(sprintf('%s, %s',subjectsAll{e},conditionsS{s}))
            saveas(gcf,sprintf('CrowdingFits/PsychFitSize%s%s.png',subjectsAll{e},conditionsS{s}));
        end
    end
    save('uncrowdedPESTSizeData');
else
    load('uncrowdedPESTSizeData.mat');
end


figure;
for ii = 1:length(subjectsAll)
    forLegs(ii) = plot([1.1 2.2],[acrossSubStats.thresh(ii,1),acrossSubStats.thresh(ii,2)],'-*');
    hold on
end
errorbar([1 2],[mean(acrossSubStats.thresh(:,1)) mean(acrossSubStats.thresh(:,2))],...
    [sem(acrossSubStats.thresh(:,1)) sem(acrossSubStats.thresh(:,2))],'-o',...
    'MarkerFaceColor','k','Color','k');
xticks([1 2])
xlim([0 3])
xticklabels({'Stabilized','Unstabilized'});
ylabel('Threshold Width')
[h,p] = ttest(acrossSubStats.thresh(:,1),acrossSubStats.thresh(:,2))
title(sprintf('p = %.2f',p))
legend(forLegs,subjectsAll)