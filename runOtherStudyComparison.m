% 'C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftCompare\Data\Snellen\combinedData.mat'));
% directorySnellen = 'C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftCompare\Data\Snellen';
clc
clear all

directorySnellen = 'C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftCompare\Data_Processed\dsq_mindur_200_2020-02-11_noBoot';

% load(fullfile(directorySnellen,'\driftData.mat'));
filesTemp =dir(fullfile(directorySnellen,'*.mat'));
files = filesTemp(startsWith({filesTemp(:).name},"Fixation2_",'IgnoreCase',true));
% files = filesTemp(startsWith({filesTemp(:).name},"Snellen_",'IgnoreCase',true));
% files = filesTemp(startsWith({filesTemp(:).name},"Reading",'IgnoreCase',true));

for ii = 1:length(files)
    fprintf('loading %s files \n',files(ii).name);
    subjectFiles{ii} = load(fullfile(directorySnellen,files(ii).name));
%     fprintf('processing %s files \n',files(ii).name);
end
for ii = 1:length(subjectFiles)
    counter = 1;
    xALLRecentered = []; yALLRecentered = [];
    for i = 1:length(subjectFiles{ii}.fix)
        path = subjectFiles{ii}.fix;
        lengthTrace = 200;
       
        if length(path(i).x) >= lengthTrace
            x = path(i).x(1:lengthTrace);
            y = path(i).y(1:lengthTrace);
            
            xALLRecentered = [xALLRecentered x-x(1)];
            yALLRecentered = [yALLRecentered y-y(1)];
            
            snellenData(ii).x{counter} = x;
            snellenData(ii).y{counter} = y;
            
            [snellenData(ii).old_curvature2(counter), ~, snellenData(ii).old_curvature3(counter)] = ...
                CalculateLengthAndCurvature(x, y);
            
            [dataStruct.VX{counter},...
                dataStruct.original_mu{counter},...
                dataStruct.r_ellipse{counter}] = tgdt_fit_2d_gaussian(x,y,0.99);
            counter = counter + 1;
        end
    end
    
    snellenData(ii).xALLRecentered = xALLRecentered;
    snellenData(ii).yALLRecentered = yALLRecentered;
    
    [~,snellenData(ii).Bias,snellenData(ii).dCoefDsq, ~,snellenData(ii).Dsq, ...
        snellenData(ii).SingleSegmentDsq,...
        snellenData(ii).TimeDsq,~,...
        snellenData(ii).RegLineDsq] = ...
        CalculateDiffusionCoef(1000, struct('x',snellenData(ii).x, 'y', snellenData(ii).y));
    
    [snellenData(ii).VXRecenteredALL,...
        snellenData(ii).original_muRecenteredALL,...
        snellenData(ii).r_ellipseRecenteredALL] = ...
        tgdt_fit_2d_gaussian(snellenData(ii).xALLRecentered,...
        snellenData(ii).yALLRecentered,0.99);

end
save('MATFiles/FixationTask.mat','snellenData');

% save('MATFiles/SnellenTask.mat','snellenData');
% save('MATFiles/ReadingTask.mat','snellenData');


%     path = subjectFiles{ii}.emdata;
%     fprintf('total files %i \n ',length(path.isdrift));
    

%     %% Check to see if field "x" exists
%     namesFolder = fieldnames(path.eye_movements);
%     for n = 1:length(namesFolder)
%         if strcmp(namesFolder{n},'x')
%             countN = 1;
%         else
%             countN = 0;
%         end
%     end
%     if sum(countN) == 0
%         fprintf('skipping %s files \n',files(ii).name);
%         continue;
%     end
%     
%     for i = 1:length(path.isdrift)
%         driftX = [];
%         driftY = [];
%         
%         idxDrift = find(path.isdrift{i});
%         counter = 2;
%         chunk(1,2) = path.isdrift{i}(1);
%         chunk(1,1) = 1;
%         %         fprintf('finding chunks \n');
%         %         tic;
%         for t = 1:length(path.isdrift{i})
%             currentNum = path.isdrift{i}(t);
%             if t ~= length(path.isdrift{i})
%                 if path.isdrift{i}(t+1) ~= currentNum
%                     chunk(1,counter) = t;
%                     chunk(2,counter) = currentNum;
%                     counter = counter + 1;
%                 end
%             end
%         end
%         
%         
%         
%         %% Getting Drift Only X Traces
%         driftX = path.eye_movements.x{i,1}(path.isdrift{i});
%         driftY = path.eye_movements.y{i,1}(path.isdrift{i});
%         
%         counter = 1;
%         for j = 1:length(chunk)-1
%             %          tempX = path.eye_movements.x{i,1};
%             %          tempY = path.eye_movements.y{i,1};
%             if j == 1 && chunk(2,1) == 1
%                 drift(i).x_Ind{counter,:} = path.eye_movements.x{i,1}(chunk(1,j):chunk(1,j+1));
%                 drift(i).y_Ind{counter,:} = path.eye_movements.y{i,1}(chunk(1,j):chunk(1,j+1));
%                 counter = counter + 1;
%             elseif j == 1 && chunk(2,1) ~= 0
%                 continue;
%             else
%                 if chunk(2,j) == 0
%                     if chunk(1,j+1) > length(path.eye_movements.x{i,1})
%                         continue;
%                     else
%                         drift(i).x_Ind{counter,:} = path.eye_movements.x{i,1}(chunk(1,j)+1:chunk(1,j+1));
%                         drift(i).y_Ind{counter,:} = path.eye_movements.y{i,1}(chunk(1,j)+1:chunk(1,j+1));
%                         counter = counter + 1;
%                     end
%                 else
%                     continue;
%                 end
%             end
%         end
%         
%     end
% end
% C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Scripts\crowding_pelli