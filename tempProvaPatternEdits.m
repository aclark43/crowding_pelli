function tempProvaPatternEdits
%% Creates the power analysis for figure 5 in the Acuity paper (Pelli Font)
%   Plot the power spectrum and phase for each spatial frequency. 

colorDigits = brewermap(4,'Set1');
%% Figuring showing how the offset in the frequencies peaks is due to the "smoothing" effects of the lobes.
%   There were two methods for this - one where you did a fourier transform on a single
%   digit with a small window, cretaing "lobes" across spatial frequency.
%   There is a second method were instead of having a window, you repeat
%   the digit multuple times. This is the second, method, removing
%   artifacts from the lobes. 

fs = 2000;                   % Sampling frequency (samples per second)
dt = 1/fs;                   % seconds per sample
StopTime = 3;                % seconds
t = (0:dt:StopTime-dt)';     % seconds
F = 20;                      % Sine wave frequency (hertz)
data = sin(2*pi*F*t);
F1 = 22;
data1 = sin(2*pi*F1*t);
F1 = 25;
data2 = sin(2*pi*F1*t);
F1 = 1;
data3 = sin(2*pi*F1*t);
datac = data1+data+data2+data3;
k =((-length(data2)/2 + 1):(length(data2)/2)) / length(data2) / dt;


figure
plot(k,fftshift(abs(fft(datac))))
hold on
datac(1:2600) = 0;
datac(end-2600:end) = 0;
plot(k,fftshift(abs(fft(datac))))
xlim([0 30])
xlabel('Frequency')
legend('Full scale stimulus', 'Stimulus through aperture')

%% Phase & Individual Spatial Frequency
%   We can also look that phase differences between digits, specifically
%   the 3,5,6,and 9. Six and nine have the same spatial frequency
%   information, but different phase - something that is important in
%   distinguishing the two numbers. The figures below plot the phase
%   differences bewteen all 4 numbers, as well as the differences between 6
%   and 9.

% % Larger Version
% stimSize = 2;                       % width of stim in arcmin
% widthPixel = 16*2;                     % how many pixels in stim? size should be multiple of 2

stimSize = 1.5;                       % width of stim in arcmin
widthPixel = 24;                     % how many pixels in stim? size should be multiple of 2
pixelAngle = stimSize/widthPixel;   % pixel ~ arcmin conversion

% stimSize = 1;                       % width of stim in arcmin
% widthPixel = 16;                     % how many pixels in stim? size should be multiple of 2
% pixelAngle = stimSize/widthPixel;   % pixel ~ arcmin conversion


numberToAnalyze = [3 5 6 9];
numberOfRepititions = 8;
figure('Position',[2200 180 800 800])
for di = 1:length(numberToAnalyze)
     digit = drawDigit(numberToAnalyze(di), widthPixel);
%        digit = lowpass(digit,10,1000); %%%if you want to lowpass the
%        filter at 10cpd, and 1000hz
    for rep = numberOfRepititions %number of repeats of the digit
        repN = rep;
        if numberToAnalyze(di) == 6 %%number 6
            digitSlice = digit(:,size(digit,2)/2+1);
        else
            digitSlice = digit(:,size(digit,2)/2);
        end
%         figure; imagesc(digitSlice)
        sizpad = 1;%size(digit,1)/4;%length(find(digitSlice==1))/4;
        stim = [ones(1,sizpad)'*0.5; digitSlice; ones(1,sizpad)'*0.5];
        for ii = 1:repN
            stim = [stim; stim];
        end
        stim = stim-mean(stim);
        stimSaved{di} = stim;
        k = ((-size(stim,1)/2 ):(size(stim,1)/2)-1) / size(stim,1) / (pixelAngle/60);
        spect = (fftshift(fft(stim)));
        X2=spect;
        threshold = max(abs(spect))/1000; % tolerance threshold
        X2(abs(spect)<threshold) = 0;     % maskout values that are below the threshold
        %*180/pi;                         % phase information multiply by 180/pi to convert to deg
        Phase_spec = (angle(X2));
        allSpec(di,:) = pow2db(abs(stim).^2)';
        allPhase(di,:) = Phase_spec';
        plot_k = [2, 10, 30, 50];
        
        
        subplot(4,1,di)
        k2 = ((-size(stim,1)/2 ):(size(stim,1)/2)-1) / size(stim,1) / (pixelAngle/60);  
        powerForDigitSaved(di,:) = abs(fftshift(fft(stim)));
        largeTemp = (abs(fftshift(fft(stim))));
        idx = (round(largeTemp)>0);
        leg2{di} = stem(k2(idx),largeTemp(idx),'fill','--');
        
%         plot(k2,temp,'-','Color',colorDigits(di,:),...
%             'LineWidth',4);
        tempPIdx = round(largeTemp) > 0 | round(largeTemp) < 0;
        stem(k2(tempPIdx),largeTemp(tempPIdx),'fill','--')
        ylabel('Power (dB)')
        xlabel('horizontal spatial frequency (cpd)');
        ylim([0 10000])
        xlim([0.5 100])
        set(gca, 'XScale', 'log', 'XTick', [2 10 30 50 75])
        title(numberToAnalyze(di));
    end
end
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/SingleDigitPhaseAndPower.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/SingleDigitPhaseAndPower.epsc');


% h = fspecial('disk',10);
% filteredRGB = imfilter(digit, h, 'replicate');
% figure;
% subplot(1,2,1); imagesc(filteredRGB)
% subplot(1,2,2); imagesc(digit)

%% Individual Subject DC * Fixed Stimulus Size
clear temp; clear tempIdx;
f = linspace(1, 80, 80); % temporal frequencies (Hz)
kdc = k;
stimPower = abs(fftshift(fft(stimSaved{1})))';
stimPower5 = abs(fftshift(fft(stimSaved{2})))';
stimPower6 = abs(fftshift(fft(stimSaved{3})))';
stimPower9 = abs(fftshift(fft(stimSaved{4})))';

performance15 = [0.736000000000000,0.818181818181818,0.606250000000000,0.606299212598425,0.761904761904762,0.416666666666667,0.606299212598425,0.588888888888889,0.525714285714286,0.515151515151515];
semPerformance15 = [0.0395849433741604,0.0323667724961960,0.0387469566668583,0.0435252597159502,0.0417646675860490,0.0505814148889070,0.0435252597159502,0.0521556406110755,0.0378547416904336,0.0504844319900026];
dc = [11.4434213638306,4.86753463745117,19.6812686920166,14.3462362289429,8.66789627075195,17.9170112609863,8.90083122253418,18.7623386383057,13.3849983215332,17.5736274719238];

for ii = 1:length(dc)
    psDAll{ii} = Qfunction(f, kdc, dc(ii) / 60^2);
end
for ii = 1:length(dc)
    driftPowerAll{ii} = (pow2db(mean(psDAll{ii}).^2));
end

psDLarge = Qfunction(f, kdc, max(dc) / 60^2); % assumes isotropic
psDSmall = Qfunction(f, kdc, min(dc) / 60^2); % assumes isotropic
pdDStab = Qfunction(f, kdc, 0 / 60^2);

driftPowerLarge = (pow2db(mean(psDLarge).^2));
driftPowerSmall = (pow2db(mean(psDSmall).^2));
driftPowerStab = (pow2db(mean(pdDStab).^2));


windowK = find(k >= 0 & k<= 100);
tempSmall = (pow2db(mean(Qfunction(f, kdc, min(dc) / 60^2)).^2));
tempLarge = (pow2db(mean(Qfunction(f, kdc, max(dc) / 60^2)).^2));
% figure;
% plot(tempSmall)
figure;
leg(1) = plot(kdc(windowK),-(normalizeVector(tempLarge(windowK)))+2,...
    'Color',[0.5 0.5 0.5], 'LineWidth',7); %sanity check
hold on
leg(2) = plot(kdc(windowK),-(normalizeVector(tempSmall(windowK)))+2,...
    'Color','k', 'LineWidth',7); %sanity check

% leg(1) = plot(kdc(windowK),((tempLarge(windowK))),...
%     'Color',[0.5 0.5 0.5], 'LineWidth',7); %sanity check
% hold on
% leg(2) = plot(kdc(windowK),((tempSmall(windowK))),...
%     'Color','k', 'LineWidth',7); %sanity check

hold on
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlim([.1 100])
ylim([.2 1.1])
axis square
set(gca, 'XScale', 'log','FontSize',16)
[M,IL] = max(-(normalizeVector(tempLarge))+2)
critFreq= abs(kdc(IL));
line([critFreq critFreq],[0 2],...
    'Color',[0.5 0.5 0.5],'LineStyle','--','LineWidth',2);

[M,IS] = max(-(normalizeVector(tempSmall))+2)
critFreq= abs(kdc(IS));
line([critFreq critFreq],[0 2],...
    'Color','k','LineStyle','--','LineWidth',2);
xlabel('Spatial Frequency (cpd')
ylabel({'Normalized','Temporal Power (dB)'})
set(gca,'box','off') 
legend(leg,{'Large DC','Small DC'},'Location','northwest');

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/MinMaxCritFreq.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/MinMaxCritFreq.epsc');



% MAKE PLOTS %%%%%%%%%%%%%%%%%%%%%
figure;
subplot(2,1,1)

hold on
if stimSize == 1
    largeTemp = driftPowerLarge.*stimPower;
%     largeIdx = round(largeTemp) > 0;
    smallTemp = driftPowerSmall.*stimPower;
%     smallIdx = round(largeTemp) > 0;
else
    largeTemp = driftPowerLarge.*stimPower;
%     largeIdx = round(largeTemp) > 0;
    smallTemp = driftPowerSmall.*stimPower;
%     smallIdx = round(largeTemp) > 0;
end
% legRetina(1) = stem(k(largeIdx),largeTemp(largeIdx),'fill','--');
% hold on
% legRetina(2) = stem(k(smallIdx),smallTemp(smallIdx),'fill','--');

legRetina(1) = plot(k,largeTemp,'--');
hold on
legRetina(2) = plot(k,smallTemp,'--');


% set(gca, 'XTick',-100:10:100)
xlim([0.5 100])
legend(legRetina,{'Large DC','Small DC'},'Location','northwest')
title(sprintf('Number 3 (%i) * DC', stimSize))
set(gca, 'XScale', 'log', 'XTick', [2 10 30 50 75])
hold on

subplot(2,1,2)
tempY = (diff([driftPowerLarge.*stimPower;driftPowerSmall.*stimPower]));
largeIdx = round(tempY) > 0 | round(tempY) < 0;
stem(k(largeIdx),tempY(largeIdx),'fill','--')
xlim([0.5 100])
% ylim([1.75 -1.75])
set(gca, 'XScale', 'log', 'XTick', [2 10 30 50 75])

saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerxDC%i.png', stimSize));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerxDC%i.epsc', stimSize));

saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerxDC%s.png', 'ComapLargeSmall'));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerxDC%s.epsc', 'ComapLargeSmall'));

%(Normalized Figures)
if stimSize == 1
    cLines = 'b';
    markerShape = 'o';
else
    cLines = 'g';
    markerShape = 's';
end
figure; 
largeTemp = diff([driftPowerLarge.*stimPower;driftPowerSmall.*stimPower]);
tempNorm = largeTemp/max(abs(largeTemp(k>0&k<75)));

hold on
tempY = (diff([driftPowerLarge.*stimPower;driftPowerSmall.*stimPower]));
largeIdx = round(tempY) > 0 | round(tempY) < 0;
stem(k(largeIdx),tempNorm(largeIdx),'fill','--');

axis([0.5 100 -1.25 1.25])
set(gca, 'XScale', 'log', 'XTick', [2 10 30 50 75])

saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerxDCNormalized%i.png', stimSize));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerxDCNormalized%i.epsc', stimSize));
%%
% INSTEAD OF DIFFERENCE, NOW LOOK AT THE SUM OF SMALL VS LARGE
% windowK = find(k >= 0 & k<= 60);
windowK = find(k >= 8 & k<= 60);


ks = k(windowK);
largeID = (driftPowerLarge(windowK).*stimPower(windowK));
largeID5 = (driftPowerLarge(windowK).*stimPower5(windowK));
largeID6 = (driftPowerLarge(windowK).*stimPower6(windowK));
largeID9 = (driftPowerLarge(windowK).*stimPower9(windowK));

% largeID = (driftPowerLarge.*stimPower);

forPlotting.largeID = largeID(round(largeID,5)<0);
forPlotting.largeID5 = largeID5(round(largeID5,5)<0);
forPlotting.largeID6 = largeID6(round(largeID6,5)<0);
forPlotting.largeID9 = largeID9(round(largeID9,5)<0);
forPlotting.sf = ks(round(largeID,5)<0);
forPlotting.sf5 = ks(round(largeID5,5)<0);
forPlotting.sf6 = ks(round(largeID6,5)<0);
forPlotting.sf9 = ks(round(largeID9,5)<0);

figure;
subplot(2,2,1)
plot(ks ,largeID);
xlabel('spatial freq')
ylabel({'drift power *', 'stim power'})
title('3')
subplot(2,2,2)
plot(ks ,largeID5);
xlabel('spatial freq')
ylabel({'drift power *', 'stim power'})
title('5')
subplot(2,2,3)
plot(ks ,largeID6);
xlabel('spatial freq')
ylabel({'drift power *', 'stim power'})
title('6')
subplot(2,2,4)
plot(ks ,largeID9);
xlabel('spatial freq')
ylabel({'drift power *', 'stim power'})
title('9')
suptitle('large stim * drift (12-60cpd)')

figure;
subplot(2,2,1)
plot(forPlotting.sf ,forPlotting.largeID);
subplot(2,2,2)
plot(forPlotting.sf5 ,forPlotting.largeID5);
subplot(2,2,3)
plot(forPlotting.sf6 ,forPlotting.largeID6);
subplot(2,2,4)
plot(forPlotting.sf9 ,forPlotting.largeID9);
suptitle('large extrap peaks stim * drift');

sumsPower.large = sum(largeID(~isinf(largeID)));
sumsPower.large5 = sum(largeID5(~isinf(largeID5)));
sumsPower.large6 = sum(largeID6(~isinf(largeID6)));
sumsPower.large9 = sum(largeID9(~isinf(largeID9)));

smallID = (driftPowerSmall(windowK).*stimPower(windowK));
smallID5 = (driftPowerSmall(windowK).*stimPower5(windowK));
smallID6 = (driftPowerSmall(windowK).*stimPower6(windowK));
smallID9 = (driftPowerSmall(windowK).*stimPower9(windowK));

% smallID = (driftPowerSmall.*stimPower);

forPlotting.smallID = smallID(round(smallID,5)<0);
forPlotting.smallID5 = smallID5(round(smallID5,5)<0);
forPlotting.smallID6 = smallID6(round(smallID6,5)<0);
forPlotting.smallID9 = smallID9(round(smallID9,5)<0);
forPlotting.sfS = ks(round(smallID,5)<0);
forPlotting.sfS5 = ks(round(smallID5,5)<0);
forPlotting.sfS6 = ks(round(smallID6,5)<0);
forPlotting.sfS9 = ks(round(smallID9,5)<0);

figure;
subplot(2,2,1)
plot(ks ,smallID);
subplot(2,2,2)
plot(ks ,smallID5);
subplot(2,2,3)
plot(ks ,smallID6);
subplot(2,2,4)
plot(ks ,smallID9);
suptitle('small stim * drift')

figure;
subplot(2,2,1)
plot(forPlotting.sfS ,forPlotting.smallID);
subplot(2,2,2)
plot(forPlotting.sfS5 ,forPlotting.smallID5);
subplot(2,2,3)
plot(forPlotting.sfS6 ,forPlotting.smallID6);
subplot(2,2,4)
plot(forPlotting.sfS9 ,forPlotting.smallID9);
suptitle('small extrap peaks stim * drift');

figure;
subplot(2,2,1)
plot(forPlotting.sf ,forPlotting.largeID);
hold on;
plot(forPlotting.sfS ,forPlotting.smallID);
title('3')

subplot(2,2,2)
plot(forPlotting.sf5 ,forPlotting.largeID5);
hold on
plot(forPlotting.sfS5 ,forPlotting.smallID5);
title('5')

subplot(2,2,3)
plot(forPlotting.sf6 ,forPlotting.largeID6);
hold on
plot(forPlotting.sfS6 ,forPlotting.smallID6);
title('6')

clear leg
subplot(2,2,4)
leg(1) = plot(forPlotting.sf9 ,forPlotting.largeID9);
hold on
leg(2) = plot(forPlotting.sfS9 ,forPlotting.smallID9);
title('9')

suptitle('large vs small extrap peaks stim * drift');
legend(leg,{'large drift','small drift'},'Location','southeast');


sumsPower.small = sum(smallID(~isinf(smallID)));
sumsPower.small5 = sum(smallID5(~isinf(smallID5)));
sumsPower.small6 = sum(smallID6(~isinf(smallID6)));
sumsPower.small9 = sum(smallID9(~isinf(smallID9)));


% forPlotting.freqRangeLargeIdx = (round(largeID,5)<0);
% forPlotting.freqRangeLargeIdx5 = (round(largeID5,5)<0);
% forPlotting.freqRangeLargeIdx6 = (round(largeID6,5)<0);
% forPlotting.freqRangeLargeIdx9 = (round(largeID9,5)<0);
% 
% forPlotting.freqRangeSmallIdx = (round(smallID,5)<0);
% forPlotting.freqRangeSmallIdx5 = (round(smallID5,5)<0);
% forPlotting.freqRangeSmallIdx6 = (round(smallID6,5)<0);
% forPlotting.freqRangeSmallIdx9 = (round(smallID9,5)<0);

figure;
% subplot(2,2,[1 2])
% leg(1) = stem(k(forPlotting.freqRangeLargeIdx),forPlotting.largeID,'filled','--');
% ylabel('Power (DC*Stim)')
% hold on
% leg(2) = stem(k(forPlotting.freqRangeSmallIdx),forPlotting.smallID,'filled','--');
% hold on 
% legend(leg,{'LargeDC','SmallDC'})
% set(gca,'YDir','reverse','XLim',[0 100])
% 
% 
% subplot(2,2,3);
% plot(k,largeID-smallID);
% ylabel('\Delta (Large - Small)')
% set(gca,'XLim',[0 100])
clear leg
% subplot(2,2,4);
leg(1) = plot([1,2],[sumsPower.small sumsPower.large],'-o','LineWidth',5);
hold on
leg(2) =plot([1,2],[sumsPower.small5 sumsPower.large5],'-o','LineWidth',5);
hold on
leg(3) = plot([1,2],[sumsPower.small6 sumsPower.large6],'-o','LineWidth',5);
hold on
leg(3) =plot([1,2],[sumsPower.small9 sumsPower.large9],'-o','LineWidth',5);

set(gca,'xtick',[1 2],'xticklabel', ...
    {'Small DC','Large DC'})
xlim([.5 2.5])
ylabel('Power (DC*Stim)')
suptitle('Stimulus Size 1.5arcmin')
legend(leg,{'3','5','6/9'});
tempIDXSmall = ~isinf((forPlotting.smallID));
tempIDXLarge = ~isinf((forPlotting.largeID));


[h,p] = ttest([forPlotting.smallID(tempIDXSmall) ...
    forPlotting.smallID5(tempIDXSmall)...
    forPlotting.smallID6(tempIDXSmall)...
    forPlotting.smallID9(tempIDXSmall)],...
    [forPlotting.largeID(tempIDXLarge)...
    forPlotting.largeID5(tempIDXLarge)...
    forPlotting.largeID6(tempIDXLarge)...
    forPlotting.largeID9(tempIDXLarge)]);


[h,pSum] = ttest([sumsPower.small ...
    sumsPower.small5...
    sumsPower.small6...
    sumsPower.small9],...
    [sumsPower.large...
    sumsPower.large5...
    sumsPower.large6...
    sumsPower.large9]);
% [p] = ranksum(forPlotting.smallID(tempIDXSmall),forPlotting.largeID(tempIDXSmall));

% [tbl,chi2stat,pval] = crosstab(forPlotting.smallID(tempIDXSmall),...
%     forPlotting.largeID(tempIDXSmall));

%%%Do this analysis for each digit
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerxDCSummed%i.png', stimSize));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerxDCSummed%i.epsc', stimSize));
%% Do the same thing but lin regres for all dc
windowK = find(k >= 8 & k<= 60);

for ii = 1:length(dc)
    idAll{ii} = (driftPowerAll{ii}(windowK).*stimPower(windowK));
    idAll5{ii} = (driftPowerAll{ii}(windowK).*stimPower5(windowK));
    idAll6{ii} = (driftPowerAll{ii}(windowK).*stimPower6(windowK));
    idAll9{ii} = (driftPowerAll{ii}(windowK).*stimPower9(windowK));

    sumsPower.All(ii) = sum(idAll{ii}(~isinf(idAll{ii})));
    sumsPower.All5(ii) = sum(idAll5{ii}(~isinf(idAll5{ii})));
    sumsPower.All6(ii) = sum(idAll6{ii}(~isinf(idAll6{ii})));
    sumsPower.All9(ii) = sum(idAll9{ii}(~isinf(idAll9{ii})));
end

figure;
subplot(2,2,1)
[~,p,b,r] = LinRegression(sumsPower.All,performance15,0,NaN,1,0);
hold on
plot(sumsPower.All,performance15,'o','Color','r',...
    'MarkerFaceColor','r');
xlim([min(sumsPower.All) max(sumsPower.All)])
ylim([.25 1])
ylabel('3');
xlabel('Summed Power')
ylabel('Performance')

subplot(2,2,2)
[~,p,b,r] = LinRegression(sumsPower.All5,performance15,0,NaN,1,0);
hold on
plot(sumsPower.All5,performance15,'o','Color','b',...
    'MarkerFaceColor','b');
xlim([min(sumsPower.All5) max(sumsPower.All5)])
ylim([.25 1])
ylabel('5');
xlabel('Summed Power')
ylabel('Performance')

subplot(2,2,3)
[~,p,b,r] = LinRegression(sumsPower.All6,performance15,0,NaN,1,0);
hold on
plot(sumsPower.All6,performance15,'o','Color','g',...
    'MarkerFaceColor','b');
xlim([min(sumsPower.All6) max(sumsPower.All6)])
ylim([.25 1])
ylabel('6');
xlabel('Summed Power')
ylabel('Performance')

subplot(2,2,4)
[~,p,b,r] = LinRegression(sumsPower.All9,performance15,0,NaN,1,0);
hold on
plot(sumsPower.All9,performance15,'o','Color','k',...
    'MarkerFaceColor','k');
xlim([min(sumsPower.All9) max(sumsPower.All9)])
ylim([.25 1])
ylabel('9');
xlabel('Summed Power')
ylabel('Performance')

saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/EachDigitLin%i.png', stimSize));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/EachDigitLin%i.epsc', stimSize));

%%%normalize the figures

[SubjectOrderDC,SubjectOrderDCIdx] = sort(dc);
c = flip(brewermap(10,'YlGn'));
normSumsPower.All3 = normalizeVector((sumsPower.All)- min((sumsPower.All)));
normSumsPower.All5 = normalizeVector((sumsPower.All5)- min(sumsPower.All5));
normSumsPower.All6 = normalizeVector((sumsPower.All6)- min(sumsPower.All6));
normSumsPower.All9 = normalizeVector((sumsPower.All9)- min(sumsPower.All9));

normSumPower.Average = mean([normSumsPower.All3 ;normSumsPower.All5 ;normSumsPower.All6 ;normSumsPower.All9]);
[SubjectOrderDC,SubjectOrderDCIdx] = sort(dc);
perfErrorbars = load('MATFiles/performance_errorbars.mat');

figure;
[~,p,b,r] = LinRegression(normSumPower.Average,performance15,0,NaN,1,0);
hold on
errorbar(normSumPower.Average,performance15,semPerformance15/100,'vertical','o','Color','k');
hold on
for ii = 1:10
    plot(normSumPower.Average(ii),performance15(ii),'o','Color',c(SubjectOrderDCIdx == ii,:),...
        'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),'MarkerSize',10);
    errorbar(normSumPower.Average(ii),performance15(ii),...%sem([sumsPower.All(ii);sumsPower.All5(ii);sumsPower.All6(ii);sumsPower.All9(ii)]),'horizontal',...
        perfErrorbars.corUSEM(ii), ...
        'vertical','o','Color',c(SubjectOrderDCIdx == ii,:),...
        'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:));
    hold on
end

% axis([-0.1 1.1 0 1])
% line([-0.1,1.1],[0.25,0.25],'Color','red','LineStyle','--')
axis([-1.1 1.1 0.25 1])
xlabel({'Normalized Power','(SF 8-60)'});
ylabel('Proportion Correct');
% text(.5,.3,'Stimulus Size = 1.5arcmin');
axis square
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/NormalizedAverageLinRegPower.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/NormalizedAverageLinRegPower.epsc');

%%
figure;
[~,p,b,r] = LinRegression(mean([sumsPower.All;sumsPower.All5;sumsPower.All6;sumsPower.All9]),...
    performance15,0,NaN,1,0);
c = flip(brewermap(10,'YlGn'));
[SubjectOrderDC,SubjectOrderDCIdx] = sort(dc);
perfErrorbars = load('MATFiles/performance_errorbars.mat');
% justThreshSW = 0;
% [distAllTabUnc] = ...
%     buildDCStruct(justThreshSW, params, tempDataStruct.subjectUnc, tempDataStruct.subjectThreshUnc, tempDataStruct.subjectCro, tempDataStruct.subjectThreshCro, tempDataStruct.thresholdSWVals);

ylim([0.25 1])
xlim([min(mean([sumsPower.All;sumsPower.All5;sumsPower.All6;sumsPower.All9])) max(mean([sumsPower.All;sumsPower.All5;sumsPower.All6;sumsPower.All9]))])
% hold on
for ii = 1:10
%     corUSEM(ii) = sem(distAllTabUnc(i).correct);
hold on
    errorbar(mean([sumsPower.All(ii);sumsPower.All5(ii);sumsPower.All6(ii);sumsPower.All9(ii)]),performance15(ii),...%sem([sumsPower.All(ii);sumsPower.All5(ii);sumsPower.All6(ii);sumsPower.All9(ii)]),'horizontal',...
        perfErrorbars.corUSEM(ii), ...
         'both','o','Color',c(SubjectOrderDCIdx == ii,:),...
        'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:));
%         perfErrorbars.corUSEM(ii), ...
%         sem(normalizeVector([sumsPower.All(ii);sumsPower.All5(ii);sumsPower.All6(ii);sumsPower.All9(ii)])),...
%         sem(normalizeVector([sumsPower.All(ii);sumsPower.All5(ii);sumsPower.All6(ii);sumsPower.All9(ii)])),...
       
hold on
end

ylabel('Performance')
xlabel({'Summed Power (DC * Stim Size)';'12-60 cpd'});
axis square
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/LinRegresPowerxPerf%i.png', stimSize));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/LinRegresPowerxPerf%i.epsc', stimSize));


figure;
plot([1,2],[sumsPower.small sumsPower.large],'-o','LineWidth',5);
set(gca,'xtick',[1 2],'xticklabel', ...
    {'Small DC','Large DC'})
xlim([.5 2.5])
ylabel('Power (DC*Stim)');
suptitle('Stimulus Size 1.5arcmin')
tempIDXSmall = ~isinf((forPlotting.smallID));
tempIDXLarge = ~isinf((forPlotting.largeID));


[h,p] = ttest(forPlotting.smallID(tempIDXSmall),forPlotting.largeID(tempIDXSmall));
[p] = ranksum(forPlotting.smallID(tempIDXSmall),forPlotting.largeID(tempIDXSmall));
%% Compare the phases for each combination
figure;
plot(k,allPhase,'-');
xlim([0 80])
combos = combntns(1:4,2); %All combinations for 4 pairs of digits
for i = 1:length(combos)
    varName = sprintf('D%i_P',i);
    D.(varName) = circ_dist(...
        allPhase(combos(i,1),:),allPhase(combos(i,2),:));
    varName2 = sprintf('D%i_A',i);
    D.(varName2) = allSpec(combos(i,1),:)-allSpec(combos(i,2),:);
end

figure('Position',[2000 100 1000 800]);
subplot(2,1,1)
for i = 1:6 %number of combinations
    varName = sprintf('D%i_P',i);
    fieldNamesVar{i} = sprintf('%iVS%i',...
        numberToAnalyze(combos(i,1)),...
        numberToAnalyze(combos(i,2)));
    CP_currentVar(i,:) = ([D.(varName)]);
%     legPhase(i) = semilogx(k,(CP_currentVar*10)-mean(CP_currentVar*10), 'LineWidth', 2);
    legPhase(i) = plot(k,(CP_currentVar(i,:)*10)-mean(CP_currentVar(i,:)*10),...
        '-', 'LineWidth', 2);
% 
    hold on
end
legend(legPhase,fieldNamesVar)
% ylim([-.036 0.35])
xlim([0.5 100])
% set(gca, 'XScale', 'log', 'XTick', [2 10 30 50 75])

subplot(2,1,2)
DP_var_new = circ_var([D.D1_P; D.D2_P; D.D3_P; D.D4_P; D.D5_P; D.D6_P]);
DP_var = var([D.D1_P; D.D2_P; D.D3_P; D.D4_P; D.D5_P; D.D6_P]);
% DP_var = var(CP_currentVar);

% allSpec(allSpec<0)= 0;
hold on
leg{1} = semilogx(k,DP_var, 'r', 'LineWidth', 2);
title('Phase variance difference')
xlabel('K')
xlim([0.5 100])
% set(gca, 'XScale', 'log', 'XTick', [2 10 30 50 75])
% saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PhaseComparison.png');
% saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PhaseComparison.epsc');

%% Looking at Phase and Power Together
figure('Position',[2000 100 800 400]);
% % % subplot(2,1,1)
% % % allPowerVar = var(powerForDigitSaved);
% % % normYPower = allPowerVar/max(allPowerVar);
% % % % stem(k2(round(abs(allPowerVar))>0),allPowerVar(round(abs(allPowerVar))>0),'fill','--');
% % % stem(k2(round(abs(allPowerVar))>0),normYPower(round(abs(allPowerVar))>0),'fill','--');
% % % xlim([0.5 100])
% % % ylim([0 1.1])
% % % set(gca, 'XScale', 'log', 'XTick', [2 10 30 50 75])
% % % ylabel('Power Variance')

% % % subplot(2,1,2)
% % % allPhaseVar = DP_var; 
% % % normYPhase = allPhaseVar/max(allPhaseVar);
% % % stem(k(round(abs(allPhaseVar),10) > 0.0000005),...
% % %     normYPhase(round(abs(allPhaseVar),9) > 0.0000005),'fill','--');
% % % hold on

allPhaseVar = DP_var_new; 
normYPhase = allPhaseVar/max(allPhaseVar);
stem(k(round(abs(allPhaseVar),10) > 0.0000005),...
    normYPhase(round(abs(allPhaseVar),9) > 0.0000005),'fill','--');
hold on

varName = 'D6_P';
% fieldNamesVar{i} = sprintf('%iVS%i',...
%     numberToAnalyze(combos(i,1)),...
%     numberToAnalyze(combos(i,2)));
CP_currentVar = ([D.(varName)]);
% legPhase(i) = semilogx(k,(CP_currentVar*10)-mean(CP_currentVar*10), 'LineWidth', 2);
largeTemp = CP_currentVar;
normTemp = normalizeVector(abs(largeTemp));
stem(k(round(abs(largeTemp),4) > 0.0005),...
    normTemp(round(abs(largeTemp),4) > 0.0005),'fill','--');
ylim([0 1.1]) 
xlim([0.5 100])
set(gca, 'XScale', 'log', 'XTick', [2 10 30 50 75])
ylabel('Phase Variance')

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerandPhaseVarianceAll.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerandPhaseVarianceAll.epsc');

function digit = drawDigit(whichDigit, pixelwidth)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth + 1;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
end
