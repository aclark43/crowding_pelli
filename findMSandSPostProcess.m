function [microsaccades, saccades] = findMSandSPostProcess(data,currentTrialId)
MinSaccSpeed = 60;
MinSaccAmp = 30;
MinMSaccSpeed = 1;
MinMSaccAmp = 2;
MaxMSaccAmp = 60;
MinVelocity = 10;

% pptrialsTemp1 = findSaccades(pptrials{currentTrialId}, ...
%     'minvel',MinSaccSpeed, ...
%     'minsa',MinSaccAmp);
% 
% Trial = pptrials{currentTrialId};
% Events = findEvents(Trial.velocity, ...
%     'minvel', MinVelocity, ...
%     'minterval', 25, ...
%     'mduration', 25 );                %%% YB@2018.10.27.
% 
% % Filter all movements within a blink event
% Events = filterIntersected(Events, Trial.blinks);
% 
% % Filter all movements within the track trace
% Events = filterIntersected(Events, Trial.notracks);
% 
% % Filter all movements within the invalid list
% Events = filterIntersected(Events, Trial.invalid);
% 
% % Filter all movements that do not fall within
% % the stimulus timeframe
% Events = filterNotIncluded(Events, Trial.analysis);

% Calculate the amplitudes/angles of the movements
counter = 1;
startTemp = [];
counterB = 1;
blinks = [];

x = data.tracesX{currentTrialId};
y = data.tracesY{currentTrialId};
%             if isempty(Events.start)
for i = 1:length(x)-15
    if diff([x(i+15), x]) > 6 && ...
            diff([x(i+15), x(i)]) < 200
        startTemp(counter) = abs(i-8);%Trial.x.position(i);
        counter = counter + 1;
    elseif diff([x(i+5), x(i)]) > 75
        blinks(counterB) = abs(i-8);
        counterB = counterB + 1;
        
    end
end
%             end

for l = 1:length(startTemp)-1
    if diff([startTemp(l), startTemp(l+1)]) < 30
        startTemp(l+1) = startTemp(l);
    end
end

for l = 1:length(blinks)-1
    if diff([blinks(l), blinks(l+1)]) < 30
        blinks(l+1) = blinks(l);
    end
end
b = unique(floor(blinks));
idxB = [];
temp = [];
for i = 1:length(b)
    temp = blinks(find(b(i) == blinks));
    idxB(i,1) = temp(1);
    idxB(i,2) = length(temp)+30;
end
b = [];

idxB(idxB == 0) = 1;
if isempty(idxB)
    pptrials{currentTrialId}.blinks.start = [];
    pptrials{currentTrialId}.blinks.duration = [];
else
    pptrials{currentTrialId}.blinks.start = round(idxB(:,1)');
    pptrials{currentTrialId}.blinks.duration = idxB(:,2)';
end
x = Trial.x.position;
idxN = [];
idxN = find(diff(x) == 0);
for l = 1:length(idxN)-1
    if diff([idxN(l), idxN(l+1)]) < 30
        idxN(l+1) = idxN(l);
    end
end
n = unique(floor(idxN));
n(n == 0) = 1;
n = unique(floor(n));
for i = 1:length(n)
    temp = idxN(find(n(i) == idxN));
    idxNo(i,1) = temp(1);
    idxNo(i,2) = length(temp)+15;
end
%             pptrials{currentTrialId}.notracks.start = round(idxNo(:,1)');
%             pptrials{currentTrialId}.notracks.duration = idxNo(:,2)';
%
t = unique(floor(startTemp));
t(t == 0) = 1;
Events.start = t;
Events.duration = ones(1,length(Events.start))*25;
Events = updateAmplitudeAngle(Trial, Events);

% Filter all movements with an amplitude according to
% minimum and maximum amplitude
Events = filterBy('amplitude', Events, ...
    MinMSaccAmp, MaxMSaccAmp);

% Save the filtered events into the trial
% if ~isempty(Events)
%     pptrialsTemp2.microsaccades = Events;
% end

% pptrials{currentTrialId}.saccades = pptrialsTemp1.saccades;
% pptrials{currentTrialId}.microsaccades = pptrialsTemp2.microsaccades;
