function plotPRLDistTabs(distTabUnc, distTabCro,  justThreshSW, params)
em = params.em;
c = params.c;

if justThreshSW == 0 %%All Gaze Pos Distances
    %% PRL by Performance
%     figure;
    
%     for subjIdx = 1:params.subNum
%         counter = 1;
%         counterC = 1;
%         for ii = 1:length(distTabUnc)
%             if distTabUnc(ii).subject == subjIdx
%                 prlDistAll(counter) = [distTabUnc(ii).meanPRLDistance];
%                 prlDistAllX(counter) = [distTabUnc(ii).meanPRLDistanceX];
%                 prlDistAllY(counter) = [distTabUnc(ii).meanPRLDistanceY];
%                 threshU = [distTabUnc(ii).threshold];
%                 counter = counter + 1;
%             end
%         end
%         for ii = 1:length(distTabCro)
%             if distTabCro(ii).subject == subjIdx
%                 prlDistAllC(counterC) = [distTabUnc(ii).meanPRLDistance];
%                 prlDistAllCX(counterC) = [distTabUnc(ii).meanPRLDistanceX];
%                 prlDistAllCY(counterC) = [distTabUnc(ii).meanPRLDistanceY];
%                 threshC = [distTabCro(ii).threshold];
%                 counterC = counterC + 1;
%             end
%         end
%         meanPRLUnc(subjIdx) = mean(prlDistAll);
%                 meanPRLUncX(subjIdx) = mean(prlDistAllX);
%                 meanPRLUncY(subjIdx) = mean(prlDistAllY);
% 
%         meanPRLCro(subjIdx) = mean(prlDistAllC);
%         meanPRLCroX(subjIdx) = mean(prlDistAllCX);
%         meanPRLCroY(subjIdx) = mean(prlDistAllCY);
%         threshUnc(subjIdx) = mean(threshU);
%         threshCro(subjIdx) = mean(threshC);
%     end
%     figure;
%     subplot(2,2,1);
%      [~,p1,~,~] = LinRegression(meanPRLUnc,...
%         threshUnc,0,NaN,1,0);
%     xlabel('Mean Gaze Position Distance across SW - Uncrowded')
%     ylabel('SW Threshold');
%     
%     subplot(2,2,2);
%      [~,p2,~,~] = LinRegression(meanPRLCro,...
%         threshCro,0,NaN,1,0);
%     xlabel('Mean Gaze Position Distance across SW - Crowded')
%     ylabel('SW Threshold');
%     
%     
% %     figure;
% %     [~,p,~,~] = LinRegression([distTabUnc.meanPRLDistance],...
% %         [distTabUnc.sizePerform],0,NaN,1,0);
%     
%     idx = ~isnan([distTabUnc.sizePerform]);
%     x = [distTabUnc.meanPRLDistance];
%     y =  [distTabUnc.sizePerform];
%     
%     subplot(2,2,3);
%     [~,p,~,~] = LinRegression(x(idx),y(idx),0,NaN,1,0);
%      xlabel('Gaze Position Distance');
%     ylabel('Performance');
% %     scatter(x,,y,[1 1 0]);
%     ylim([0 1])
%     title('Uncrowded')
%     text(1,.9,sprintf('p = %.3f',p))
% %     saveas(gcf,'../../Data/AllSubjTogether/UncPRLbyPerformance.png');
%     
%     idx = ~isnan([distTabCro.sizePerform]);
%     x = [distTabCro.meanPRLDistance];
%     y =  [distTabCro.sizePerform];
%     
%     subplot(2,2,4);
%     [~,p,~,~] = LinRegression(x(idx),y(idx),0,NaN,1,0);
% %     scatter([distTabCro.meanPRLDistance],[distTabCro.sizePerform],[],[0 1 0]);
%     xlabel('Gaze Position Distance');
%     ylabel('Performance');
%     ylim([0 1])
%     text(1,.9,sprintf('p = %.3f',p))
%     title('Crowded')
%     saveas(gcf,'../../Data/AllSubjTogether/GazePositionbySWandPerformance.png');  
%     saveas(gcf,'../../Data/AllSubjTogether/GazePositionbySWandPerformance.epsc');  
%     
    %% PRL With MS by Threshold
%     if strcmp('Drift_MS',em)
%         figure;
%         [~,p,~,~] = LinRegression([distTabUnc.meanPRLDistanceWholeTrace],...
%             [distTabUnc.threshold],0,NaN,1,0);
%         scatter([distTabUnc.meanPRLDistanceWholeTrace],[distTabUnc.threshold],[],[1 1 0]);
%         xlabel('PRL Distance with MS');
%         ylabel('Strokewidth Threshold');
%         ylim([0 1])
%         title('Uncrowded')
%         text(5,.9,sprintf('p = %.3f',p))
%         saveas(gcf,'../../Data/AllSubjTogether/UncPRLMSbyThresh.png');
%         
%         
%         figure
%         [~,p,~,~] = LinRegression([distTabCro.meanPRLDistanceWholeTrace],...
%             [distTabCro.threshold],0,NaN,1,0);
%         scatter([distTabCro.meanPRLDistanceWholeTrace],[distTabCro.threshold],[],[0 1 0]);
%         xlabel('PRL Distance with MS');
%         ylabel('Strokewidth Threshold');
%         ylim([0 1])
%         text(5,.9,sprintf('p = %.3f',p))
%         title('Crowded')
%         saveas(gcf,'../../Data/AllSubjTogether/CroPRLMSbyThresh.png');
%     end
else
     %% PRL by Threshold
    figure;
    subplot(1,2,1);
    [~,p,~,r] = LinRegression([distTabUnc.meanPRLDistance],...
        [distTabUnc.threshold],0,NaN,1,0);
    for ii = 1:length([distTabUnc.meanPRLDistance])
        scatter(distTabUnc(ii).meanPRLDistance,distTabUnc(ii).threshold,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
%     scatter([distTabUnc.meanPRLDistance],[distTabUnc.threshold],[],[1 1 0]);
    xlabel('Gaze Pos. Distance');
    ylabel('Strokewidth Threshold');
    %  ylim([0 1])
    title('Uncrowded')
    axis([3 14 0 4]);
    axis square
    text(5,.9,sprintf('p = %.3f',p))
%     saveas(gcf,'../../Data/AllSubjTogether/UncPRLbyThresh.png');
    
    
    subplot(1,2,2);
    [~,p,~,r] = LinRegression([distTabCro.meanPRLDistance],...
        [distTabCro.threshold],0,NaN,1,0);
    for ii = 1:length([distTabUnc.meanPRLDistance])
        scatter(distTabCro(ii).meanPRLDistance,distTabCro(ii).threshold,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
%     scatter([distTabCro.meanPRLDistance],[distTabCro.threshold],[],[0 1 0]);
    xlabel('Gaze Pos. Distance');
    ylabel('Strokewidth Threshold');
    %  ylim([0 1])
    axis([3 14 0 4]);
        axis square

    text(5,.9,sprintf('p = %.3f',p))
    title('Crowded')
    saveas(gcf,'../../Data/AllSubjTogether/PRLbyThresh.png');
    saveas(gcf,'../../Data/AllSubjTogether/PRLbyThresh.epsc');
     %% X PRL by Threshold
    figure;
    subplot(1,2,1);
    [~,p,~,r] = LinRegression([distTabUnc.meanXPRLDistance],...
        [distTabUnc.threshold],0,NaN,1,0);
    for ii = 1:length([distTabUnc.meanXPRLDistance])
        scatter(distTabUnc(ii).meanXPRLDistance,distTabUnc(ii).threshold,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
%     scatter([distTabUnc.meanPRLDistance],[distTabUnc.threshold],[],[1 1 0]);
    xlabel('X Gaze Pos. Distance');
    ylabel('Strokewidth Threshold');
    %  ylim([0 1])
    title('Uncrowded')
        axis square

    axis([0 8 0 4]);
    text(5,.9,sprintf('p = %.3f',p))
%     saveas(gcf,'../../Data/AllSubjTogether/UncPRLbyThresh.png');
    
    
    subplot(1,2,2);
    [~,p,~,r] = LinRegression([distTabCro.meanXPRLDistance],...
        [distTabCro.threshold],0,NaN,1,0);
    for ii = 1:length([distTabUnc.meanXPRLDistance])
        scatter(distTabCro(ii).meanXPRLDistance,distTabCro(ii).threshold,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
%     scatter([distTabCro.meanPRLDistance],[distTabCro.threshold],[],[0 1 0]);
    xlabel('X Gaze Pos. Distance');
    ylabel('Strokewidth Threshold');
    %  ylim([0 1])
    axis([0 8 0 4]);
        axis square

    text(5,.9,sprintf('p = %.3f',p))
    title('Crowded')
    saveas(gcf,'../../Data/AllSubjTogether/XPRLbyThresh.png');
    saveas(gcf,'../../Data/AllSubjTogether/XPRLbyThresh.epsc');
 %% Y PRL by Threshold
    figure;
    subplot(1,2,1);
    [~,p,~,r] = LinRegression([distTabUnc.meanYPRLDistance],...
        [distTabUnc.threshold],0,NaN,1,0);
    for ii = 1:length([distTabUnc.meanYPRLDistance])
        scatter(distTabUnc(ii).meanYPRLDistance,distTabUnc(ii).threshold,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
%     scatter([distTabUnc.meanPRLDistance],[distTabUnc.threshold],[],[1 1 0]);
    xlabel('Y Gaze Pos. Distance');
    ylabel('Strokewidth Threshold');
    %  ylim([0 1])
    title('Uncrowded')
    axis([0 10 0 4]);
        axis square

    text(5,.9,sprintf('p = %.3f',p))
%     saveas(gcf,'../../Data/AllSubjTogether/UncPRLbyThresh.png');
    
    
    subplot(1,2,2);
    [~,p,~,r] = LinRegression([distTabCro.meanYPRLDistance],...
        [distTabCro.threshold],0,NaN,1,0);
    for ii = 1:length([distTabUnc.meanYPRLDistance])
        scatter(distTabCro(ii).meanYPRLDistance,distTabCro(ii).threshold,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
%     scatter([distTabCro.meanPRLDistance],[distTabCro.threshold],[],[0 1 0]);
    xlabel('Y Gaze Pos. Distance');
    ylabel('Strokewidth Threshold');
    %  ylim([0 1])
    axis([0 10 0 4]);
        axis square

    text(5,.9,sprintf('p = %.3f',p))
    title('Crowded')
    saveas(gcf,'../../Data/AllSubjTogether/YPRLbyThresh.png');
    saveas(gcf,'../../Data/AllSubjTogether/YPRLbyThresh.epsc');
    %% PRL From Edge of Target by Threshold
    figure;
    [~,p,~,r] = LinRegression([distTabUnc.PRLDistanceFromEdge],...
        [distTabUnc.threshold],0,NaN,1,0);
    for ii = 1:length([distTabUnc.PRLDistanceFromEdge])
        scatter(distTabUnc(ii).PRLDistanceFromEdge,distTabUnc(ii).threshold,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
%     scatter([distTabUnc.meanPRLDistance],[distTabUnc.threshold],[],[1 1 0]);
    xlabel('PLF Distance from Edge of Target');
    ylabel('Strokewidth Threshold');
    %  ylim([0 1])
    title('Uncrowded')
%     axis([3 14 0 4]);
    text(5,.9,sprintf('p = %.3f',p))
    saveas(gcf,'../../Data/AllSubjTogether/UncPRLEdgebyThresh.png');
    
    
    figure;
    [~,p,~,r] = LinRegression([distTabCro.PRLDistanceFromEdge],...
        [distTabCro.threshold],0,NaN,1,0);
    for ii = 1:length([distTabUnc.PRLDistanceFromEdge])
        scatter(distTabCro(ii).PRLDistanceFromEdge,distTabCro(ii).threshold,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
%     scatter([distTabCro.meanPRLDistance],[distTabCro.threshold],[],[0 1 0]);
    xlabel('PLF Distance from Edge of Target');
    ylabel('Strokewidth Threshold');
    %  ylim([0 1])
%     axis([3 14 0 4]);
    text(5,.9,sprintf('p = %.3f',p))
    title('Crowded')
    saveas(gcf,'../../Data/AllSubjTogether/CroPRLEdgebyThresh.png');
    %% PRL by SPan
    figure;
    [~,p,~,~] = LinRegression([distTabUnc.meanPRLDistance],...
        [distTabUnc.span],0,NaN,1,0);
    for ii = 1:length([distTabUnc.meanPRLDistance])
        scatter([distTabUnc(ii).meanPRLDistance],[distTabUnc(ii).span],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
    xlabel('PLF Distance');
    ylabel('Span');
    %  ylim([0 1])
    axis([3 11 0 7]);
    title('Uncrowded')
    text(5,.9,sprintf('p = %.3f',p))
    saveas(gcf,'../../Data/AllSubjTogether/UncPRLbySpan.png');
    
    figure;
    [~,p,~,~] = LinRegression([distTabCro.meanPRLDistance],...
        [distTabCro.span],0,NaN,1,0);
    for ii = 1:length([distTabCro.meanPRLDistance])
        scatter([distTabCro(ii).meanPRLDistance],[distTabCro(ii).span],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
    xlabel('PLF Distance');
    ylabel('Span');
    axis([3 11 0 7]);
    %  ylim([0 1])
    text(5,.9,sprintf('p = %.3f',p))
    title('Crowded')
    
    saveas(gcf,'../../Data/AllSubjTogether/CroPRLbySpan.png');
    %% PRL by DC
     figure;
     subplot(1,2,1);
    [~,p,~,~] = LinRegression([distTabUnc.meanPRLDistance],...
        [distTabUnc.dc],0,NaN,1,0);
    for ii = 1:length([distTabUnc.meanPRLDistance])
        scatter([distTabUnc(ii).meanPRLDistance],[distTabUnc(ii).dc],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
    xlabel('PLF Distance');
    ylabel('DC');
    %  ylim([0 1])
    axis([3 15 0 50]);
axis square
    title('Uncrowded')
    text(5,.9,sprintf('p = %.3f',p))
    saveas(gcf,'../../Data/AllSubjTogether/UncPRLbySpan.png');
    
    subplot(1,2,2)
    [~,p,~,~] = LinRegression([distTabCro.meanPRLDistance],...
        [distTabCro.dc],0,NaN,1,0);
    for ii = 1:length([distTabCro.meanPRLDistance])
        scatter([distTabCro(ii).meanPRLDistance],[distTabCro(ii).dc],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
    xlabel('PLF Distance');
    ylabel('DC');
    axis([3 15 0 50]);
axis square
    %  ylim([0 1])
    text(5,.9,sprintf('p = %.3f',p))
    title('Crowded')
    
    saveas(gcf,'../../Data/AllSubjTogether/PRLbyDC.png');
        saveas(gcf,'../../Data/AllSubjTogether/PRLbyDC.epsc');

    %% Span by DC
     figure;
     subplot(1,2,1)
    [~,p,~,~] = LinRegression([distTabUnc.span],...
        [distTabUnc.dc],0,NaN,1,0);
    for ii = 1:length([distTabUnc.span])
        scatter([distTabUnc(ii).span],[distTabUnc(ii).dc],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
    xlabel('span');
    ylabel('DC');
    %  ylim([0 1])
%     axis([3 11 0 7]);
axis square
    title('Uncrowded')
    text(5,.9,sprintf('p = %.3f',p))
    saveas(gcf,'../../Data/AllSubjTogether/UncDCbySpan.png');
    
subplot(1,2,2)
[~,p,~,~] = LinRegression([distTabCro.span],...
        [distTabCro.dc],0,NaN,1,0);
    for ii = 1:length([distTabCro.span])
        scatter([distTabCro(ii).span],[distTabCro(ii).dc],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    end
    xlabel('span');
    ylabel('DC');
%     axis([3 11 0 7]);
axis square
    %  ylim([0 1])
    text(5,.9,sprintf('p = %.3f',p))
    title('Crowded')
    
    saveas(gcf,'../../Data/AllSubjTogether/CroSpanbyDC.png');
    saveas(gcf,'../../Data/AllSubjTogether/CroSpanbyDC.epsc');
    
end
end