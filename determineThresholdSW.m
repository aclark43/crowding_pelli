function thresholdVals = determineThresholdSW(subjectUnc,subjectThreshUnc,...
    subjectThreshCro, subjectCro, subNum, subjectsAll)
%Determine the threshold strokewidth value for both the crowded and
%uncrowded condition

for ii = 1:subNum
    thresholdSizeSWRound = round(subjectUnc(ii).stimulusSize,1);
    matchingThresholdRound = round(subjectThreshUnc(ii).thresh,1);
    [~,thresholdIdxUnc] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    thresholdVals(ii).thresholdSWUncrowded = ...
        sprintf('strokeWidth_%i', subjectUnc(ii).SW(thresholdIdxUnc));
end

for ii = 1:subNum
    thresholdSizeSWRound = round(subjectCro(ii).stimulusSize,1);
    matchingThresholdRound = round(subjectThreshCro(ii).thresh,1);
    [~,thresholdIdxCro] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    thresholdVals(ii).thresholdSWCrowded = ...
        sprintf('strokeWidth_%i', subjectCro(ii).SW(thresholdIdxCro));
end

end

