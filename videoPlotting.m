function videoPlotting(xx, yy, i, nametitle)

currPos = plot(xx(1), yy(1), 'g*');
hold on
pathPos = plot(xx(1), yy(1), 'm-');

frame_count = 0;

temporal_bins = length(1:i:length(xx));
movie_trial(temporal_bins, 1) = struct('cdata', [], 'colormap', []);

hold on
xlabel('X position');
ylabel('Y position');
axis([-30 30 -30 30])

axis square

% allTs = [1:i:min(length(xx), length(yy)), min(length(xx), length(yy))];
figure;
for t = [1:i:min(length(xx), length(yy))-i]
    clf
    frame_count = frame_count + 1;
    k = text(-25,25,sprintf('sample=%i',t));
    %     set(currPos, 'XData', xx(t), 'YData', yy(t));
    %     set(pathPos, 'XData', xx(1:t), 'YData', yy(1:t));
    generateHeatMapSimple( ...
        reshape(xx(:,t:t+(i-1)),1,[]),...
        reshape(yy(:,t:t+(i-1)),1,[]), ...
        'Bins', i/2,...
        'StimulusSize', 40,...
        'AxisValue', 30,...
        'Uncrowded', 4,...
        'Borders', 1);
    movie_trial(frame_count) = getframe(gcf);
    pause(.000009)
    title(nametitle);
%     delete(k);
end
%         input ''
%         clf
% videoName = sprintf('../../Videos/Graph%s',i);
% v_trial = VideoWriter(videoName, 'Motion JPEG AVI');
% v_trial.FrameRate = 10;
% open(v_trial);
% for ii = 1:temporal_bins
%     writeVideo(v_trial, movie_trial(ii));
% end
% close(v_trial);

% input ''
% clf


end

