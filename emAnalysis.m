function [em] = emAnalysis(em, pptrials, valid, trialChar, title_str, params, uEcc, filepath, figures)
%%Updated! 08/08/2019
% uSW = unique(trialChar.TargetStrokewidth);
uST = unique(round(trialChar.TargetSize,1));

dCounter = 1;
msCounter = 1;
%% Categorizes (whether drift or ms) for further analysis
% for ii = uSW %loops through all unique SW
for ii = 1:length(uST)
    trialIdx = find(trialChar.TargetStrokewidth == ii);
    driftIdx = intersect(find(valid.d == 1 ), trialIdx);
    msIdx = intersect(find(valid.ms == 1), trialIdx);
    partMSIdx = intersect(find(valid.partialMS == 1), trialIdx);
    sIdx = intersect(find(valid.s == 1), trialIdx);
    bothIdx = intersect(find(valid.dms == 1), trialIdx);
    
    n = numel(driftIdx);
    b = numel(msIdx);
    s = numel(sIdx);
    p = numel(partMSIdx);
    
    strokeWidth = sprintf('strokeWidth_%i', (ii));
    stimulusSize = uST(ii);
    
    if params.D && ~figures.FIXATION_ANALYSIS
        %% If DRIFT ONLY
        if n > 10
            [em,~,~] = driftAnalysis(pptrials, em, params,...
                driftIdx, ii, uEcc, title_str, trialChar, filepath, figures, stimulusSize);
            allSW{dCounter} = strokeWidth;
            dCounter = dCounter +1;
        end
        
    elseif params.MS && ~figures.FIXATION_ANALYSIS
        %% If MS ONLY
        if b > 1 %|| s > 10
            [em,~,~] = msAnalysis(em, pptrials, uEcc,...
                valid, trialChar, msIdx, sIdx, params, title_str, ii,...
                ii, figures, (ii), 1);
%             if s > 2
%                  [em,~,~] = msAnalysis(em, pptrials, uEcc,...
%                 valid, trialChar, msIdx, sIdx, params, title_str, ii,...
%                 ii, figures.FIXATION_ANALYSIS, (ii), 2);
%             end
        
            eccentricity = sprintf('ecc_%i', abs(uEcc));
            msRate(ii)= mean(em.(eccentricity).(strokeWidth).microsaccadeRate);
            msLocation(em.ms, params, ii, trialChar, strokeWidth, ...
                uEcc, title_str, ...
                msRate(ii), ...
                figures.FIXATION_ANALYSIS)
%             close all;
            swAll(msCounter) = ii;
            msCounter = msCounter + 1;
        end
    elseif params.DMS && ~figures.FIXATION_ANALYSIS
        %% If DRIFT & MS
        if n > 10 || b > 10
            [em,~,~] = driftAnalysis(pptrials, em, params, bothIdx, ii, ...
                uEcc, title_str, trialChar, filepath, figures);
            allSW{dCounter} = strokeWidth;
            dCounter = dCounter +1;
        end
    end
    if isfield(em, 'ecc_0')
        if isfield (em.ecc_0, sprintf('strokeWidth_%i',(ii)))
            if ~params.MS && ~figures.FIXATION_ANALYSIS
                %                 [ spnAmpEvaluate ] = buildSpanAmpEvaluate( em, params, ii, strokeWidth );
                spnAmpEvaluate.meanDriftAmplitude(ii) = em.ecc_0.(strokeWidth).meanAmplitude;
                spnAmpEvaluate.meanDriftSpan(ii) = em.ecc_0.(strokeWidth).meanSpan;
                spnAmpEvaluate.stdDriftAmplitude(ii) = em.ecc_0.(strokeWidth).stdAmplitude;
                spnAmpEvaluate.stdDriftSpan(ii) = em.ecc_0.(strokeWidth).stdSpan;
                %centerDistance = (round(2*trialChar.TargetStrokewidth(driftIdx)*1.4))*params.pixelAngle;
                spnAmpEvaluate.ccDist(ii) = em.ecc_0.(strokeWidth).ccDistance;
                spnAmpEvaluate.stimulusSize(ii) = uST(ii);%em.ecc_0.(strokeWidth).stimulusSize;
                spnAmpEvaluate.SW(ii) = (ii);
                spnAmpEvaluate.pAcrossSWForSpan = [];
            end
        end
    end
end

filepathN = ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/');

if exist('spnAmpEvaluate','var')
    if params.D || params.DMS
        p = spanAmpEvaluation(params,spnAmpEvaluate,title_str,trialChar, em);
        spnAmpEvaluate.pAcrossSWForSpan = p;
        trialChar.spnAmpEvaluate = spnAmpEvaluate;
        
        if strcmp('D',params.machine)
            if params.crowded == 1
                filename = sprintf('%sScripts/crowding_pelli/MATFiles/%s_Crowded_SpanAmpEval.mat', filepathN, trialChar.Subject);
            else
                filename = sprintf('%sScripts/crowding_pelli/MATFiles/%s_Uncrowded_SpanAmpEval.mat', filepathN, trialChar.Subject);
            end
        else
            if params.crowded == 1
                filename = sprintf('%sScripts/crowding_pelli/MATFiles/%s_Crowded_SpanAmpEval.mat', filepathN, trialChar.Subject);
            else
                filename = sprintf('%sScripts/crowding_pelli/MATFiles/%s_Uncrowded_SpanAmpEval.mat', filepathN, trialChar.Subject);
            end
        end
        
        save(filename,'spnAmpEvaluate')
    end
end
%% Create MS Landing Map for All MS
if params.MS
    if isfield(em, 'ms')
        allMSStruct = getfield(em.ms,(eccentricity));
        listSW = (fieldnames(allMSStruct));
        strokeWidth = 'strokeWidth_All';
        em.ms.(eccentricity).strokeWidth_All = [];
        if numel(listSW) > 1
            for msSWCounter = 1:length(listSW)
                em.ms.(eccentricity).strokeWidth_All = ...
                    [em.ms.(eccentricity).strokeWidth_All...
                    em.ms.(eccentricity).(char(listSW(msSWCounter)))];
                %          numList = [char(listSW(msSWCounter));
            end
        else
            em.ms.(eccentricity).strokeWidth_All = ...
                em.ms.(eccentricity).(char(listSW));
        end
        msLocation(em.ms, params, 0, trialChar, strokeWidth, ...
            uEcc, title_str, mean(msRate), figures.FIXATION_ANALYSIS)
    end
end

%% Generates Fixation Heat Map%%
if figures.FIXATION_ANALYSIS
    for timeBinIdx = 5
        params.timeString = sprintf('TimeBin=%i', timeBinIdx);
        params.timeBin = timeBinIdx;
        
        fixIdx = find(trialChar.TimeFixationON > 0);
        
        [em,~,~] = driftAnalysis(pptrials, em, params,...
            fixIdx, 1, uEcc, title_str, trialChar, filepath, figures);
        
        [em,~,~] = msAnalysis(em, pptrials, uEcc,...
            valid, trialChar, fixIdx, fixIdx, params, title_str, 1,...
            'ALL', figures.FIXATION_ANALYSIS, uST, 1, 16);
        
        if timeBinIdx == 4
            msLocation(em.ms, params, ii, trialChar, 'strokeWidth_ALL', ...
                'ecc_0', title_str, em.ecc_0.strokeWidth_ALL.microsaccadeRate, figures.FIXATION_ANALYSIS)
        end
        
        [ fixationResult, xValues, yValues, t, idx, ~, fixation] = ...
            fixAnalysis( pptrials, uEcc, em, params,...
            valid, trialChar, title_str ); 
%         close all;
        
        generateHeatMap( 60, uST, fixationResult, xValues, yValues, NaN, ...
            title_str,trialChar, params, idx, t, figures, pptrials)
        
        if t > 4000
            [fixation.MeanDistanceFrom0, fixation.dAbs] = generateDirectionMap(xValues, yValues, title_str );
            
            numMatLength = length(idx);
            timeBin = (t/(1000/330));
            numMatAll = round(repmat(1:1:timeBin, numMatLength, 1))*(1000/330);
            
            numMatSingle = numMatAll(1,:);
%             generateHeatMapMovie(xValues,yValues,numMatAll, params, fixationResult, t, trialChar, lengthVector);
            counterFix = 1;
            for ii = fixIdx
                timeOn = ceil(pptrials{ii}.TimeFixationON/(1000/330));
                timeOff = round(pptrials{ii}.TimeFixationOFF/(1000/330));
                if timeOff > length(pptrials{ii}.x.position)
                    timeOff = length(pptrials{ii}.x.position);
                end
                    fixation.tracesX{counterFix} = pptrials{ii}.x.position(ceil(timeOn):floor(timeOff)) + (pptrials{ii}.pixelAngle * pptrials{ii}.xoffset);
                    fixation.tracesY{counterFix} = pptrials{ii}.y.position(ceil(timeOn):floor(timeOff)) + (pptrials{ii}.pixelAngle * pptrials{ii}.yoffset);
                    fixation.velocityX{counterFix} = pptrials{ii}.x.velocity;
                    fixation.velocityY{counterFix} = pptrials{ii}.y.velocity;
                    counterFix = counterFix + 1;
%                 end
            end
            generateDriftVelocityMap(fixation.velocityX, fixation.velocityY,...
                0, params, title_str, trialChar)
            
            filename = sprintf('MATFiles/%s_FixationResults.mat', trialChar.Subject);
            save(filename,'fixation')
        end
    end
end

%% Graphs Drift Characteristics%%
if isfield(em, 'ecc_0')
    if ~ isfield((em.ecc_0), 'position')
        if exist('validSW','var')
            if params.D
                
                generateDriftAmpAndSpanGraphs...
                    ( meanDriftAmplitude,  meanDriftSpan, stdDriftAmplitude, stdDriftSpan, title_str);
                
            end
        end
    end
end

end


