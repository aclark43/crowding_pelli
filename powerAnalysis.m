function powerAnalysis( params, subjectThreshUnc, subjectThreshCro, threshDSQUncr, fixation )

subNum = params.subNum;
subjectsAll = params.subjectsAll;
c = params.c;
idx = params.idx;
%% DC Thresh Power Analysis for Ind Subjects (zoom and large)
% allSpec = cell(size(1:4));
%%% Uncrowded Task
for ii = 1:subNum
    dc = threshDSQUncr(ii);
    dcSmall = cell2mat(idxBins.small.recalcDC(ii));
    dcLarge = cell2mat(idxBins.large.recalcDC(ii));
    pixelAngle(ii) = round((unique(subjectThreshUnc(ii).em.ecc_0.strokeWidth_4.stimulusSize/8))/5,2)*5;
%     pixelAngle(ii) = subjectThreshUnc(ii).em.ecc_0.strokeWidth_4.stimulusSize/8;
    threshWidth(ii) = subjectThreshUnc(ii).thresh;
%         [critFreq(1,ii), power.(subjectsAll{ii}), spatial.(subjectsAll{ii}), powerHeat.(subjectsAll{ii})] = ...
%             PSD_crowding(dc, pixelAngle, threshWidth(ii));
    [critFreq(1,ii), power.(subjectsAll{ii}), spatial.(subjectsAll{ii}), powerHeat.(subjectsAll{ii})] = ...
        PSD_averageRemoved(dc, pixelAngle(ii), threshWidth(ii));
    
    [critFreqSmall(1,ii), powerSmall.(subjectsAll{ii}), spatialSmall.(subjectsAll{ii}), powerHeatSmall.(subjectsAll{ii})] = ...
        PSD_averageRemoved(dcSmall, pixelAngle(ii), threshWidth(ii));
    
    [critFreqLarge(1,ii), powerLarge.(subjectsAll{ii}), spatialLarge.(subjectsAll{ii}), powerHeatLarge.(subjectsAll{ii})] = ...
        PSD_averageRemoved(dcLarge, pixelAngle(ii), threshWidth(ii));
    
    suptitle('Task')
%     saveas(gcf,...
%         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%sNumberPowerSpectrums.png', ...
%         params.subjectsAll{ii}));
%     saveas(gcf,...
%         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%sNumberPowerSpectrums.epsc', ...
%         params.subjectsAll{ii}));
    close all
    %%%%%%%%%%%%%%
    dcF = fixation.First200.dsq(ii);
    if ~isnan(dcF)
%                 [critFreq(3,ii), powerF.(subjectsAll{ii}), spatialF.(subjectsAll{ii}), ~] = ...
%                     PSD_crowding(dcF, pixelAngle, threshWidth(ii));
        [critFreq(3,ii), powerF.(subjectsAll{ii}), spatialF.(subjectsAll{ii}), ~] = ...
            PSD_averageRemoved(dcF, pixelAngle(ii), threshWidth(ii));
        suptitle('Fixation')
    else
        critFreq(3,ii) = NaN;
    end
    close all;
end

figure;
%%% Crowded
for ii = 1:subNum
    subplot(2,2,1)
    plot(spatial.(subjectsAll{ii}).All, power.(subjectsAll{ii}).All, 'Color', c(ii,:),'Linewidth',2)
    x = [critFreq(1,ii) critFreq(1,ii)];
    y = [-50 0+ii];
    line(x,y,'Color',c(ii,:),'LineStyle','--')
    xS = find(round(power.(subjectsAll{ii}).All) == -7);
    x21 = [spatial.(subjectsAll{ii}).All(xS(1)) spatial.(subjectsAll{ii}).All((xS(length(xS))))];
    y21 = [0+ii 0+ii];
    line(x21,y21,'Color',c(ii,:));
    hold on
    hold on
    
    subplot(2,2,2)
    if idx(ii)
        plot(spatialF.(subjectsAll{ii}).All, powerF.(subjectsAll{ii}).All, 'Color', c(ii,:),'Linewidth',2)
        x = [critFreq(3,ii) critFreq(3,ii)];
        y = [-50 0+ii];
        line(x,y,'Color',c(ii,:),'LineStyle','--')
        
        xS = find(round(powerF.(subjectsAll{ii}).All) == -7);
        if ~isempty(xS)
            x22 = [spatialF.(subjectsAll{ii}).All(xS(1)) spatialF.(subjectsAll{ii}).All((xS(length(xS))))];
            y22 = [0+ii 0+ii];
            line(x22,y22,'Color',c(ii,:));
            hold on
        end
        %         f.newK(ii,:) = [powerHeatF.(subjectsAll{ii}).k];
        %         f.newF(ii,:) = [powerHeatF.(subjectsAll{ii}).f];
    end
    subplot(2,2,3)
    plot(critFreq(1,ii), critFreq(3,ii),'o', 'Color', c(ii,:));
    axis([10 35 10 35])
    %     errorbar(critFreq(1,ii), critFreq(3,ii), x21(1), x21(2), x22(1), x22(2),'o',...
    %         'Color', c(ii,:));
    hold on
    %     t(1).newK{ii,:} = [powerHeat.(subjectsAll{ii}).k{1}];
    %     t(2).newK{ii,:} = [powerHeat.(subjectsAll{ii}).k{2}];
    %     t(3).newK{ii,:} = [powerHeat.(subjectsAll{ii}).k{3}];
    %     t(4).newK{ii,:} = [powerHeat.(subjectsAll{ii}).k{4}];
    %
    %     h(1).newK{ii,:} = [powerHeat.(subjectsAll{ii}).pow2db{1}];
    %     h(2).newK{ii,:} = [powerHeat.(subjectsAll{ii}).pow2db{2}];
    %     h(3).newK{ii,:} = [powerHeat.(subjectsAll{ii}).pow2db{3}];
    %     h(4).newK{ii,:} = [powerHeat.(subjectsAll{ii}).pow2db{4}];
end
line([10 35],[10,35],'Color','k');

[~,p] = ttest(critFreq(1,idx),critFreq(3,idx))
% subplot(1,2,1)
% set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
% xlabel('spatial frequency (cpd)');
% title('Task')
% ylabel('PSD (db)');
% axis square;
%
% subplot(1,2,2)
% set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
% xlabel('spatial frequency (cpd)');
% title('Fixation')
% ylabel('PSD (db)');
% axis square;
%
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SubjectIndPowerSpectrums.png');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SubjectIndPowerSpectrums.epsc');
% close all;

subplot(2,2,1)
ylim([-15 12]);
xlim([2.5 100])
set(gca, 'XScale', 'log','XTick', [1, 10, 30, 50]);
xlabel('spatial frequency (cpd)');
title('Task')
ylabel('PSD (db)');
axis square;

subplot(2,2,2)
ylim([-15 12]);
xlim([2.5 100])
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlabel('spatial frequency (cpd)');
title('Fixation')
ylabel('PSD (db)');
axis square;

subplot(2,2,3)
xlim([5 100])
ylim([5 100])
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50],...
    'YScale', 'log', 'YTick', [1, 10, 30, 50]);
xlabel('Task');
% title('Fixation')
ylabel('Fixation');
axis square;
%% Average Removed from Stimulus (Martina Updates)

%Plot all details for each subject
for i = 1:subNum
    
%     figure;
%     subplot(1,2,1)
%     for di = 1:4
%         x1 = power.(subjectsAll{i}).forPowerAmplification.digitPower{di}';
%         norm_data = x1/max(x1);
%         semilogx(power.(subjectsAll{i}).forPowerAmplification.k{di},...
%             (norm_data));
%         
%         hold on
%     end
% % % % % %     
% % %     hold on
% % %     
% % %     x2 = power.(subjectsAll{i}).forPowerAmplification.driftPower;
% % %     normData = (-x2/max(x2)+2);
% % %     semilogx(power.(subjectsAll{i}).forPowerAmplification.k{1},...
% % %         (normData),'Color','k') %plots power for drift
    
% % %     hold on
% % %     x3 =  power.(subjectsAll{i}).forPowerAmplification.phaseVarianceDiff;
% % %     normData3 = (x3/max(x3));
% % %     
% % %     semilogx(power.(subjectsAll{i}).forPowerAmplification.k{1},... %%all k are the same
% % %         normData3, 'r', 'LineWidth', 2)
    
% % %     %     semilogx(power.(subjectsAll{i}).forPowerAmplification.k{1},... %%all k are the same
% % %     %        x3, 'r', 'LineWidth', 2)
% % %     
% % %     
% % %     %     xlim([0.5 60])
% % %     %     xlabel(sprintf('DC = %.3f', threshDSQUncr(i)));
% % %     %     title('Drift Power')
% % %     ylim([-0.5 1.5])
% % %     set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50, 75]);
% % %     xlim([.5 80])
% % %     %     legend({'3','5','6', '9', 'Drift'}, 'Location', 'Best')
% % %     
% % %     title(sprintf('%s, %i',subjectsAll{i}, i));
% % %     ylabel('Normalized Power (dB)')
% % %     xlabel('spatial frequency (cpd)');
% % %     axis square
% % %     
% % %     
%     pixelAngle = round(pixelAnglel/5,2)*5;
    swCalculated = round2(threshWidth(i) / pixelAngle(i), 2) * pixelAngle(i);
    
    %     subplot(2,2,3)
        figure;
%     subplot(1,2,2)
    for di = 1%1:4
        %                 semilogx(power.(subjectsAll{i}).forPowerAmplification.k{di},...
        %                     power.(subjectsAll{i}).forPowerAmplification.amplifPower{di}) %combines power to show amplification
        
        %         a = power.(subjectsAll{2}).forPowerAmplification.a{di}; %%Digit - choosing smallest SW (Ashley)
        a = power.(subjectsAll{i}).forPowerAmplification.a{di,2}; %%2 is single digit
        b = powerLarge.(subjectsAll{i}).forPowerAmplification.b{di,2}; %%Drift
        y = pow2db(a'.*b);
        %          semilogx(power.(subjectsAll{2}).forPowerAmplification.k{di},...
        %                     y)
        leg(1) = semilogx(power.(subjectsAll{i}).forPowerAmplification.k{di},...
            y, 'Color' ,'k');
        hold on
    end
    for di = 1%1:4
        a = power.(subjectsAll{i}).forPowerAmplification.a{di,1};
        b = powerLarge.(subjectsAll{i}).forPowerAmplification.b{di,1}; %%Drift
        y = pow2db(a'.*b);
       
        leg(2) = semilogx(power.(subjectsAll{i}).forPowerAmplification.k{di},...
            y, 'Color' ,'g');
        hold on
    end
    
     hold on
     normData3 =  power.(subjectsAll{i}).forPowerAmplification.phaseVarianceDiff;
%      normData3 = (x3/max(x3));
    
    leg(3) = semilogx(power.(subjectsAll{i}).forPowerAmplification.k{1},... %%all k are the same
        normData3, 'r', 'LineWidth', 2);
    
%     title('average vertical');
    xlabel('spatial frequency (cpd)');
%     title('Combined Power - 1SW')
%     title(sprintf('Combined Power - %.2fSW, Large Drift',swCalculated));
    title(sprintf('%s Combined Power - %.2fSW, #3',subjectsAll{i}, swCalculated));
    ylim([-60, 20]);
    xlim([2 60])
    %  ylim([-1.5 1.5])
    set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
    %     xlim([0 100])
    %
    %     subplot(2,2,4)
    % subplot(2,1,2)
    %     for di = 1:4
    %         semilogx(power.(subjectsAll{i}).forPowerAmplification.k{di},...
    %             (power.(subjectsAll{i}).forPowerAmplification.amplifPower{di}-...
    %             power.(subjectsAll{i}).forPowerAmplification.digitPower{di}')); %combines power to show amplification
    %         hold on
    %     end
    %     title('Diff: Amp and Digit Power')
    %     xlabel('spatial frequency (cpd)');
    %     ylim([-80, -40]);
    %     xlim([0.5 60])
    
    %     suptitle(sprintf('%s',subjectsAll{i}));
    %     set(gcf, 'Units', 'Normalized', 'OuterPosition', [1.2, -0.2, .5, 2]);
    axis square
    
    %%%%Temporary
%     subplot(1,2,1)
     hold on
     for di = 1%:4
        a = power.(subjectsAll{i}).forPowerAmplification.a{di};
        b = powerSmall.(subjectsAll{i}).forPowerAmplification.b{di}; %%Drift
        y = pow2db(a'.*b);
       
        leg(3) = semilogx(power.(subjectsAll{i}).forPowerAmplification.k{di},...
            y, 'Color', 'b')
        hold on
    end
%     title('average vertical');
%     xlabel('spatial frequency (cpd)');
% %     title('Combined Power - 1SW')
%     title(sprintf('Combined Power - %.2fSW, Small Drift',swCalculated));
%     ylim([-60, -10]);
%     xlim([2 60])
%     set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
%     axis square
    largeDriftCorrect = (sum(idxBins.large.correct{i})/length(idxBins.large.correct{i}))...
        * 100;
     smallDriftCorrect = (sum(idxBins.small.correct{i})/length(idxBins.small.correct{i}))...
        * 100;

    largeDriftStr = sprintf('Large Drift (%.3f), %.1f%%',cell2mat(idxBins.large.recalcDC(i)), largeDriftCorrect);
    smallDriftStr = sprintf('Small Drift (%.3f), %.1f%%',cell2mat(idxBins.small.recalcDC(i)), smallDriftCorrect);
    legend(leg,{largeDriftStr,'\Delta Phase', smallDriftStr},'Location','northwest');
    saveas(gcf,...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%sStimPowerSpectrumsIndSWSmallvsLarge.png', subjectsAll{i}));
    saveas(gcf,...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%sStimPowerSpectrumsIndSWSmallvsLarge.epsc', subjectsAll{i}));
end

%Plot combined data
% figure;
% for di = 1:4
%     subplot(2,2,di)
%     for i = 1:subNum
%         semilogx(power.(subjectsAll{i}).forPowerAmplification.k{di},...
%             (power.(subjectsAll{i}).forPowerAmplification.amplifPower{di}-...
%             power.(subjectsAll{i}).forPowerAmplification.digitPower{di}')); %combines power to show amplification
%         hold on
%     end
%     title(di)
%     xlabel('spatial frequency (cpd)');
%     ylabel('Amplification')
%     ylim([-80, -40]);
%     xlim([0.5 60])
% end
% saveas(gcf,...
%     ('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AmplifStimPowerSpectrums.png'));
% saveas(gcf,...
%     ('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AmplifStimPowerSpectrums.epsc'));
%%
figure;
for ii = 1:subNum
    clear newP
    for num = 1:length(h(1).newK{ii})
        newP(num,:) = mean([(h(1).newK{ii}(num,:)); (h(2).newK{ii}(num,:)); (h(3).newK{ii}(num,:)); (h(4).newK{ii}(num,:))]);
    end
    figure;
    imagesc(mean([t(1).newK{ii}; t(2).newK{ii}; t(3).newK{ii}; t(4).newK{ii}]), ...
        mean([t(1).newK{ii}; t(2).newK{ii}; t(3).newK{ii}; t(4).newK{ii}]),...
        newP...
        );
end
xlim([5 100])
ylim([5 100])
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50],...
    'YScale', 'log', 'YTick', [1, 10, 30, 50]);
xlabel('Task');
% title('Fixation')
ylabel('Fixation');
axis square;
%
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SubjectIndPowerSpectrumsZoom.png');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SubjectIndPowerSpectrumsZoom.epsc');
close all;

%% plot vertical power by horizontal power
digits = [3 5 6 9];
for ii = find(idx)
    figure;
        for i = 1:4
            subplot(2,2,i)
           plot(powerHeat.(subjectsAll{ii}).pow2db{i}(:,1));
            ylabel('V power')
            axis square
            title(sprintf('%i',digits(i)));
        end
    for i = 1:4
        subplot(4,1,i)
        %         amount = length(powerHeat.(subjectsAll{ii}).pow2db{i}(:,2));
        plot(powerHeat.(subjectsAll{ii}).horizAverage{i}(1,:));
        xlabel('H power')
        title(sprintf('%i',digits(i)));
        ylim([-10, 30]);
        
    end
    suptitle(sprintf('%s',subjectsAll{ii}))
    ylim([-10, 30]);
    saveas(gcf,...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%s_SubjectHPower.png', subjectsAll{ii}));
    saveas(gcf,...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%s_SubjectHPower.epsc', subjectsAll{ii}));
end
for ii = find(idx)
    figure;
    for i = 1:4
        subplot(4,1,i)
        %         amount = length(powerHeat.(subjectsAll{ii}).pow2db{i}(1,:));
        plot(powerHeat.(subjectsAll{ii}).vertAverage{i}(:,1));
        ylabel('V power')
        title(sprintf('%i',digits(i)));
        ylim([-10, 30]);
        
    end
    suptitle(sprintf('%s',subjectsAll{ii}))
    saveas(gcf,...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%s_SubjectVPower.png', subjectsAll{ii}));
    saveas(gcf,...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%s_SubjectVPower.epsc', subjectsAll{ii}));
end
close all

%% Varaiance plots
cols = lines(4);
for ii = 1:subNum
    
    clear variance
    clear varianceH
    figure;
    subplot(2,3,1)
    for nums = 1:4
        plot(spatial.(subjectsAll{ii}).H(nums,:), power.(subjectsAll{ii}).H(nums,:), 'b-',...
            'linewidth', 2, 'Color', cols(nums, :));
        hold on
    end
    xlabel('vertical spatial frequency (cpd)');
    ylim([-10, 30]);
    axis square;
    ylabel('power (db)');
    xlim([0 100])
    set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
    xlim([5, length(spatial.(subjectsAll{ii}).H)]);
    
    subplot(2,3,4)
    for nums = 1:4
        plot(spatial.(subjectsAll{ii}).V(nums,:), power.(subjectsAll{ii}).V(nums,:), 'b-',...
            'linewidth', 2, 'Color', cols(nums, :));
        hold on
    end
    xlabel('vertical spatial frequency (cpd)');
    ylim([-10, 30]);
    axis square;
    ylabel('power (db)');
    xlim([0 100])
    set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
    xlim([5, length(spatial.(subjectsAll{ii}).V)]);
    
    for sIdx = 1:length(spatial.(subjectsAll{ii}).V)
        variance(1,sIdx) = abs(diff([power.(subjectsAll{ii}).V(1,sIdx), power.(subjectsAll{ii}).V(2,sIdx)]));
        variance(2,sIdx) = abs(diff([power.(subjectsAll{ii}).V(1,sIdx), power.(subjectsAll{ii}).V(3,sIdx)]));
        variance(3,sIdx) = abs(diff([power.(subjectsAll{ii}).V(1,sIdx), power.(subjectsAll{ii}).V(4,sIdx)]));
        variance(4,sIdx) = abs(diff([power.(subjectsAll{ii}).V(2,sIdx), power.(subjectsAll{ii}).V(3,sIdx)]));
        variance(5,sIdx) = abs(diff([power.(subjectsAll{ii}).V(2,sIdx), power.(subjectsAll{ii}).V(4,sIdx)]));
        variance(6,sIdx) = abs(diff([power.(subjectsAll{ii}).V(3,sIdx), power.(subjectsAll{ii}).V(4,sIdx)]));
        
        varianceH(1,sIdx) = abs(diff([power.(subjectsAll{ii}).H(1,sIdx), power.(subjectsAll{ii}).H(2,sIdx)]));
        varianceH(2,sIdx) = abs(diff([power.(subjectsAll{ii}).H(1,sIdx), power.(subjectsAll{ii}).H(3,sIdx)]));
        varianceH(3,sIdx) = abs(diff([power.(subjectsAll{ii}).H(1,sIdx), power.(subjectsAll{ii}).H(4,sIdx)]));
        varianceH(4,sIdx) = abs(diff([power.(subjectsAll{ii}).H(2,sIdx), power.(subjectsAll{ii}).H(3,sIdx)]));
        varianceH(5,sIdx) = abs(diff([power.(subjectsAll{ii}).H(2,sIdx), power.(subjectsAll{ii}).H(4,sIdx)]));
        varianceH(6,sIdx) = abs(diff([power.(subjectsAll{ii}).H(3,sIdx), power.(subjectsAll{ii}).H(4,sIdx)]));
    end
    subplot(2,3,2)
    counter = 1;
    for comboIdx = [1 2 4]
        plottingCombH(counter) = plot(spatial.(subjectsAll{ii}).H(1,:), varianceH(comboIdx,:));
        hold on
        counter = counter + 1;
    end
    axis square
    xlim([0 100])
    set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
    %     legend(plottingCombH,{'3:5','3:6','3:9','5:6','5:9','6:9'},'Location','northwest')
    legend(plottingCombH,{'3:5','3:6/9','5:6/9'},'Location','northwest')
    
    xlabel('Spatial Frequency (cpd)')
    ylabel('Absolute \Delta Power (dB)')
    title('Horizontal Differences in Optotypes')
    
    subplot(2,3,5)
    counter = 1;
    for comboIdx = [1 2 4]%1:6
        plottingCombV(counter) = plot(spatial.(subjectsAll{ii}).V(1,:), variance(comboIdx,:));
        hold on
        counter = counter + 1;
    end
    axis square
    xlim([0 100])
    ylim([0 60])
    set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
    xlabel('Spatial Frequency (cpd)')
    ylabel('Absolute \Delta Power (dB)')
    %     legend(plottingCombV,{'3:5','3:6','3:9','5:6','5:9','6:9'},'Location','northwest')
    legend(plottingCombV,{'3:5','3:6/9','5:6/9'},'Location','northwest')
    title('Vertical Differences in Optotypes')
    
    %%%%%%%%%%%%%%%%%
    subplot(2,3,3)
    %     plot(spatial.(subjectsAll{ii}).H(1,:), var(varianceH),'Color','k');
    plot(spatial.(subjectsAll{ii}).H(1,:), var([...
        varianceH(1,:);...
        varianceH(2,:);...
        varianceH(4,:)]),'Color','k');
    
    axis square
    xlim([0 100])
    set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
    xlabel('Spatial Frequency (cpd)')
    ylabel('Variance of \Delta Power (dB)')
    %     legend(plottingCombV,{'3:5','3:6','3:9','5:6','5:9','6:9'},'Location','northwest')
    title('Horizontal Differences in Optotypes')
    
    subplot(2,3,6)
    plot(spatial.(subjectsAll{ii}).V(1,:), var([...
        variance(1,:);...
        variance(2,:);...
        variance(4,:)]),'Color','k');
    axis square
    xlim([0 100])
    ylim([0 60])
    set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
    xlabel('Spatial Frequency (cpd)')
    ylabel('Variance of \Delta Power (dB)')
    %     legend(plottingCombV,{'3:5','3:6','3:9','5:6','5:9','6:9'},'Location','northwest')
    title('Vertical Differences in Optotypes')
    
    suptitle(sprintf('%s Task', subjectsAll{ii}));
    
    set(gcf, 'Position',  [2100, 20, 1000, 800])
    
    saveas(gcf,...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%s_SubjectIndPowerSpectrums.png',...
        subjectsAll{ii}));
    saveas(gcf,...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%s_SubjectIndPowerSpectrums.epsc',...
        subjectsAll{ii}));
    
    temp = var([...
        variance(1,:);...
        variance(2,:);...
        variance(4,:)]);
    
    temp2 = std([...
        variance(1,:);...
        variance(2,:);...
        variance(4,:)]);
    
    allVarsV(ii,:) = temp(1:480);
    allVarsVError(ii,:) = temp(1:480);
    
    clear temp;
    
    temp = var([...
        varianceH(1,:);...
        varianceH(2,:);...
        varianceH(4,:)]);
    
    temp2 = std([...
        varianceH(1,:);...
        varianceH(2,:);...
        varianceH(4,:)]);
    
    allVarsH(ii,:) = temp(1:480);
    allVarsHError(ii,:) = temp2(1:480);
    
    clear temp;
end

figure;
subplot(1,2,1)
plot(0.25:.25:120, mean(allVarsH),'Color','k');
hold on
plot(0.25:.25:120, std(allVarsH)+mean(allVarsH),'Color','r');
plot(0.25:.25:120, mean(allVarsH)- std(allVarsH),'Color','r');
% x = 0.25:.25:120;
% curve1 = std(allVarsH)+mean(allVarsH);
% curve2 = mean(allVarsH)-std(allVarsH);
% x2 = [x, fliplr(x)];
% inBetween = [curve1, fliplr(curve2)];
% fill(x2, inBetween,'r');%, 'FaceAlpha',0.5);
plot(0.25:.25:120, mean(allVarsH),'Color','k');
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlabel('spatial frequency (cpd)');
title('Horizontal')
ylabel('Average PSD Differences (db)');
axis square;
xlim([0 100]);

% ylim([0 100]);

subplot(1,2,2)
x = 0.25:.25:120;
curve1 = std(allVarsV)+mean(allVarsV);
curve2 = mean(allVarsV)-std(allVarsV);
x2 = [x, fliplr(x)];
inBetween2 = [curve1, fliplr(curve2)];
fill(x2, inBetween2,'r');
hold on
plot(0.25:.25:120, mean(allVarsV),'Color','k');
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
ylim([0 40]);
xlim([0 100]);
xlabel('spatial frequency (cpd)');
title('Vertical')
ylabel('Average PSD Differences (db)');
axis square;
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllSubjectPowerDifferences.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllSubjectPowerDifferences.epsc');
%% CritFreq by Thresh

figure
a=[threshDSQUncr];
b=[critFreq(1,:)];
[AX,H1,H2] =plotyy([1:10],a, [1:10],b, 'bar', 'bar');
set(H1,'FaceColor','r') % a
set(H2,'FaceColor','b') % b

figure;
bar(sort(critFreq(1,:)));

figure;
subplot(1,3,1)
[t,p,b1,r1] = LinRegression(critFreq(1,:),...
    [subjectThreshUnc.thresh],0,NaN,1,0);
for ii = 1:params.subNum
    scatter(critFreq(1,ii),...
        subjectThreshUnc(ii).thresh,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on;
end
axis square
text(10,2.1,sprintf('p = %.3f',p))
text(10,2.3,sprintf('b = %.3f',b1))
text(10,2.5,sprintf('r = %.3f',r1))
% ylim([0.5 3.5])
axis([0 35 0.5 3.5])

% yyaxis left

xlabel('Critical Frequency')
ylabel('Threshold SW')
title('Uncrowded')

subplot(1,3,2)
[t,p,b1,r1] = LinRegression(critFreq(2,:),...
    [subjectThreshCro.thresh],0,NaN,1,0);
for ii = 1:params.subNum
    scatter(critFreq(2,ii),...
        subjectThreshCro(ii).thresh,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on;
end
axis square
text(10,1,sprintf('p = %.3f',p))
text(10,1.2,sprintf('b = %.3f',b1))
text(10,1.4,sprintf('r = %.3f',r1))
ylim([0.5 3.5])
xlabel('Critical Frequency')
ylabel('Threshold SW')
title('Crowded')

subplot(1,3,3)
idx = ~isnan(critFreq(3,:));
[t,p,b1,r1] = LinRegression(critFreq(3,idx),...
    [subjectThreshUnc(idx).thresh],0,NaN,1,0);
for ii = 1:params.subNum
    scatter(critFreq(3,ii),...
        subjectThreshUnc(ii).thresh,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on;
end
axis square
text(10,1,sprintf('p = %.3f',p))
text(10,1.2,sprintf('b = %.3f',b1))
text(10,1.4,sprintf('r = %.3f',r1))
% ylim([0.5 3.5])
axis([0 35 0.5 3.5])
xlabel('Critical Frequency Fixation DC')
ylabel('Threshold SW Uncrowded')
title('Fixation')
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCPowervsSW.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCPowervsSW.epsc');
%% DC Critical by Ideal Freq
figure;
subplot(1,3,1)
for ii = 1:params.subNum
    idealFreq(ii) = 60/(2*subjectThreshUnc(ii).thresh);
end

[t,p,b1,r1] = LinRegression(critFreq(1,:),...
    idealFreq,0,NaN,1,0);

for ii = 1:params.subNum
    scatter(critFreq(1,ii),...
        idealFreq(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on;
end
text(25,12,sprintf('p = %.3f',p))
text(25,13,sprintf('b = %.3f',b1))
text(25,14,sprintf('r = %.3f',r1))
axis([5 35 5 35])
x = [35 0];
y = [35 0];
line(x,y,'Color','k','LineStyle','--')
axis square
xlabel('Critical Frequency')
ylabel('Strokewidth Frequency')
title('Uncrowded')

subplot(1,3,2)
for ii = 1:params.subNum
    idealFreq(ii) = 60/(2*subjectThreshCro(ii).thresh);
end

[t,p,b1,r1] = LinRegression(critFreq(2,:),...
    idealFreq,0,NaN,1,0);
for ii = 1:params.subNum
    scatter(critFreq(2,ii),...
        idealFreq(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on;
end

text(25,12,sprintf('p = %.3f',p))
text(25,13,sprintf('b = %.3f',b1))
text(25,14,sprintf('r = %.3f',r1))
axis([5 35 5 35])
axis square
x = [35 0];
y = [35 0];
line(x,y,'Color','k','LineStyle','--')
axis square
xlabel('Critical Frequency')
ylabel('Strokewidth Frequency')
title('Crowded')

subplot(1,3,3)
%%%FIXATION
for ii = 1:params.subNum
    if idx(ii)
        idealFreq(ii) = 60/(2*subjectThreshUnc(ii).thresh);
    else
        idealFreq(ii) = NaN;
    end
end

[t,p,b1,r1] = LinRegression(critFreq(3,idx),...
    idealFreq(idx),0,NaN,1,0);
for ii = 1:params.subNum
    scatter(critFreq(3,ii),...
        idealFreq(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on;
end

text(25,12,sprintf('p = %.3f',p))
text(25,13,sprintf('b = %.3f',b1))
text(25,14,sprintf('r = %.3f',r1))
axis([5 35 5 35])
x = [35 0];
y = [35 0];
line(x,y,'Color','k','LineStyle','--')
axis square
xlabel('Critical Frequency Fixation')
ylabel('Strokewidth Frequency Uncrowded')
title('Fixation')
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCPowervsIdeal.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCPowervsIdeal.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/DCPowervsIdeal.png');

%% 1:1 Ratio of Task and Fixation Crit Freq
figure;
for ii = 1:params.subNum
    plot(critFreq(1,ii), critFreq(3,ii),'o','Color',params.c(ii,:), ...
        'MarkerFaceColor', params.c(ii,:), 'MarkerSize', 15);
    hold on
end
x = [50 0];
y = [50 0];
line(x,y,'Color','k','LineStyle','--')
xlim([10 50])
ylim([10 50])
set(gca, 'XScale', 'log', 'XTick', [10, 30, 50],...
    'YScale', 'log', 'YTick', [10, 30, 50]);
axis square
xlabel('Task Critical Frequency');
ylabel('Fixation Critical Frequency')
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/RatioTaskFixCritFreq.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/RatioTaskFixCritFreq.epsc');
end

