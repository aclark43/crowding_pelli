% x = threshDSQUncr';
% y = [subjectThreshUnc.thresh]';
% 
% X = y;
% %% %%%https://www.mathworks.com/help/stats/cvpartition.html
% X = [1 2 3 4 5 6 7 8 9 -10]';
% c = cvpartition(10,'Leaveout') 
% values = crossval(@(Xtrain,Xtest)mean(Xtrain),X,'Partition',c);
% % values = crossval(@(Xtrain,Xtest)mean(Xtrain), x,'leaveout',1);
% 
% figure; boxplot(values)
% 
% [~,repetitionIdx] = min(values);
% observationIdx = test(c,repetitionIdx);
% influentialObservation = X(observationIdx);
% 
%% https://www.mathworks.com/matlabcentral/answers/323449-how-do-i-create-a-cross-validated-linear-regression-model-with-fitlm
% 
% % prediction function given training and testing instances
fcn = @(Xtr, Ytr, Xte) predict( fitlm(Xtr,Ytr), Xte);
% 
% % perform cross-validation, and return average MSE across folds
mse = crossval('mse', x, y, 'Predfun', fcn, 'leaveout', 1);
% 
% % compute root mean squared error
% avrg_rmse = sqrt(mse)

%%
% cvmodel = fitrtree(x,y,'leaveout','on')
% L = kfoldLoss(cvmodel)
%creates a cross-validated model when name is one of 'CrossVal', 'KFold', 'Holdout', 'Leaveout', or 'CVPartition'. For syntax details, see the fitrtree function reference page
%% https://stackoverflow.com/questions/38566548/how-to-use-crossval-in-matlab-for-a-leave-one-out-validation-method
X = x;
Y = y;
testval=@(XTRAIN,ytrain,XTEST)Linear_regression_indices( XTRAIN,ytrain,XTEST);

cvMse = crossval('mse',X,y,'predfun',testval,'leaveout',1);

c = cvpartition(y,'LeaveOut'); 
cvMse2=crossval('mse',X,y,'predfun',testval,'partition',c);
RMSE=sqrt(mse);
RMSE2=sqrt(cvMse2);


% function [ Linear_regression_indices ] = Linear_regression_indices(XTRAIN,ytrain,XTEST )
%          
% Linear_regression_indices=(polyval(polyfit(XTRAIN,ytrain,1),XTEST));
% 
% end




