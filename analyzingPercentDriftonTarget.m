function analyzingPercentDriftonTarget(percentOnTarget, condition, subNum)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

for conditionIdx = 1:length(condition)
    savedAll = [];
    currentCond = char(condition(conditionIdx));
    allNames = fieldnames(percentOnTarget.(currentCond).sizeStim);
    
    for ii = 1:length(allNames)
        sw = sprintf('%s',allNames{ii});
        for subIdx = 1:subNum
            stimSizes{ii,subIdx} = percentOnTarget.(currentCond).sizeStim(subIdx).(sw);
            performance{ii,subIdx} = percentOnTarget.(currentCond).perform(subIdx).(sw);
            percentTimeonTarget{ii,subIdx} = percentOnTarget.(currentCond).percent(subIdx).(sw);
        end
    end
    
    saveVals = [];
    for subIdx = 1:length(allNames)
        [vals] = unique(cell2mat(stimSizes(subIdx,:)));
        valsRound = round(vals,1);
        saveVals = [saveVals valsRound];
    end
    
    orderSaveVals = sort(unique(saveVals));
    
    savedSizes = [];
    savedPerformance = [];
    savedPercent = [];
    for ii = 1:length(orderSaveVals)
        currentSize = orderSaveVals(ii);
%          width = length(stimSizes);
        for rows = 1:length(allNames)
            for col = 1:subNum
                size = round(cell2mat(stimSizes(rows,col)),1);
                if ~isempty(size)
                    if size <= currentSize + 0.25 && size >= currentSize - 0.25
                        savedSizesSingle = stimSizes(rows,col);
                        savedPerformanceSingle = performance(rows,col);
                        savedPercentSingle = percentTimeonTarget(rows,col);
                        
                        savedSizes = [savedSizes savedSizesSingle];
                        savedPerformance = [savedPerformance savedPerformanceSingle];
                        savedPercent = [savedPercent savedPercentSingle];
                    end
                end
            end
        end
    end
    
    savedAll(1,:) = cell2mat(savedSizes);%Stimulus Sizes
    savedAll(2,:) = cell2mat(savedPerformance); %Performance
    savedAll(3,:) = str2double(savedPercent); %Percent
    [temp, order] = sort(savedAll(1,:));
    
%     sortedSaved = round(savedAll(:,order),1); %round to the 10th place

    newSavedAll = floor(savedAll/.5)*.5;
    sortedSaved = round(newSavedAll(:,order),1); %round to the nearest .5 place
    
    testedSW = unique(sortedSaved(1,:));
    
    figure;
    colormap(jet)
    colorGraph1 = jet(length(testedSW));
    for ii = 1:length(testedSW)
        idx = find(sortedSaved(1,:) == round(testedSW(ii),1));
        leg = scatter(...
            savedAll(3,idx),...
            savedAll(2,idx),...
            100,...
            [colorGraph1(ii,1),colorGraph1(ii,2),colorGraph1(ii,3)],...
            'filled');
        hold on;
    end
    xlabel('Percent of Time on Target')
    ylabel('Performance')
    title('Percent of time on target by Strokewidth')
    cb = colorbar;
    set(cb, 'Ticks', [0 1], 'TickLabels', {'0.5 arcmin', '4.5 arcmin'})
    saveas(gcf,sprintf(...
        '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PercentTimeSpentonTargetPerformStimSize%s.png',...
        condition{conditionIdx}));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     conditionIdx = 2
    figure;
    colormap(jet)
    colorGraph1 = jet(length(testedSW));
    for ii = 1:length(testedSW)
        charSW{ii} = sprintf('%.1f arcmin',round(testedSW(ii),1));
        idx = find(sortedSaved(1,:) == round(testedSW(ii),1));
        x = savedAll(3,idx);
        largestX = max(x);
        normalizedX = x/largestX;
        leg = scatter(...
            normalizedX,...
            savedAll(2,idx),...
            100,...
            [colorGraph1(ii,1),colorGraph1(ii,2),colorGraph1(ii,3)],...
            'filled');
        line(normalizedX,max(savedAll(2,idx)))
        hold on;
    end
    xlabel('Normalized Percent of Time on Target')
    ylabel('Performance')
    title(sprintf('Percent of time on target by Strokewidth - %s',condition{conditionIdx}));
    cb = colorbar;
    set(cb, 'Ticks', [0:1/(length(testedSW)-1):1], 'TickLabels', charSW)
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/NormalizedPercentTimeSpentonTargetPerformStimSize%s.png',...
        condition{conditionIdx}));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    allStim = [];
    allSpan = [];
    figure;
    for i = 1:subNum
        stimSize = cell2mat(struct2cell(percentOnTarget.(currentCond).sizeStim(i)))';
        span = cell2mat(struct2cell(percentOnTarget.(currentCond).span(i)))';
        subplot(2,5,i)
        [~,p,~,r] = LinRegression(stimSize,span,0,NaN,1,0);
        xlabel('Stimulus Size')
        ylabel('Mean Span')
        allSpan = [allSpan span];
        allStim = [allStim stimSize];
    end
%     set(
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%sEachSubjectSpanBySize.png',...
        condition{conditionIdx}));
    
    figure;
    [~,p,~,r] = LinRegression(allStim,allSpan,0,NaN,1,0);
    text(4,3,sprintf('p = %.3f',p));
    xlabel('Stimulus Size')
    ylabel('Mean Span')
    title('Across All Subjects')
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%sAllSubjSpanBySize.png',...
       condition{conditionIdx}));
end
for condLength = 1:2
    [ legends, info ] = plot3VarsOnGraph( percentOnTarget, condition{condLength}, subNum );
    hold on
    yyaxis left
    line(sort(info.meanPerc),sort(smoothdata(info.meanPerf)))
    ylabel('Performance')
    text(10,.9,sprintf('p = %.3f',info.pPercPerf))
    
    hold on
    yyaxis right
    line(sort(info.meanPerc),sort(smoothdata(info.meanSize)))
    ylabel('Size of Stimulus')
    text(60,1.5,sprintf('p = %.3f',info.pPercSize))
    ylim([0 5.5])
    
    hold on
    title(sprintf('%s',condition{condLength}))
    xlabel('Percent of Time on Target')
    saveas(gcf,sprintf...
        ('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PercentOnTargPerfStim%s.png',...
        condition{condLength}));
end

end

