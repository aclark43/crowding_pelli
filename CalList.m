function list = CalList()

% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

list = eis_readData([], 'x');
list = eis_readData(list, 'y');
%list = eis_readData(list, 'trigger', 'frame'); %% just added for test
list = eis_readData(list, 'trigger', 'blink');
list = eis_readData(list, 'trigger', 'notrack');
list = eis_readData(list, 'stream', 0, 'double');
list = eis_readData(list, 'stream', 1, 'double');
list = eis_readData(list, 'stream', 2, 'double');
list = eis_readData(list, 'stream', 3, 'double');
%list = eis_readData(list, 'joypad', 'r1');

% % user variables
list = eis_readData(list, 'uservar', 'xoffset');
list = eis_readData(list, 'uservar', 'yoffset');

list = eis_readData(list, 'uservar', 'ResponseTime');
list = eis_readData(list, 'uservar', 'TimeCueON');
list = eis_readData(list, 'uservar', 'TimeCueOFF');
list = eis_readData(list, 'uservar', 'TimeFixationON');
list = eis_readData(list, 'uservar', 'TimeFixationOFF');
list = eis_readData(list, 'uservar', 'TimeTargetON');
list = eis_readData(list, 'uservar', 'TimeTargetOFF');
list = eis_readData(list, 'uservar', 'Subject');

list = eis_readData(list, 'uservar', 'Correct');
list = eis_readData(list, 'uservar', 'Response');
list = eis_readData(list, 'uservar', 'TargetEccentricityIndex');
list = eis_readData(list, 'uservar', 'TargetOrientation');
list = eis_readData(list, 'uservar', 'TargetEccentricity');
list = eis_readData(list, 'uservar', 'TargetStrokewidth');

list = eis_readData(list, 'uservar', 'Unstab');
list = eis_readData(list, 'uservar', 'Uncrowded');
list = eis_readData(list, 'uservar', 'FlankerType');
list = eis_readData(list, 'uservar', 'FlankerDist');
list = eis_readData(list, 'uservar', 'FlankerOrientations');

% list = eis_readData(list, 'uservar', 'XResolution');
% list = eis_readData(list, 'uservar', 'YResolution');
%  list = eis_readData(list, 'uservar', 'ScreenDistance');
% list = eis_readData(list, 'uservar', 'ScreenWidth');
% list = eis_readData(list, 'uservar', 'pixelAngle');

list = eis_readData(list, 'uservar', 'InitPestLevel');
list = eis_readData(list, 'uservar', 'PestLevel');
list = eis_readData(list, 'uservar', 'TargetGray');


list = eis_readData(list, 'uservar', 'TestCalibration');




