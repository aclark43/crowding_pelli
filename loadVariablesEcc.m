function[ subjectCond, subjectThreshCond, subjectMSCond, perform ] = ...
    loadVariablesEcc(subjectsAll, condition, params, huxlinProject, ecc, singleEcc)
% Loads all variables that were created in runMupltipleSubj. Stored in
% MATFiles Folder inside scripts folder.
% subjectCond = [];
% subjectThreshCond = [];
% subjectMSCond = [];
% perform = [];
em = params.em;

for ii = 1:length(subjectsAll)
    conditionT = condition{1};
    fileName1 = sprintf('%s_%s_%s_%s_SpanAmpEval.mat', ...
        subjectsAll{ii},condition{1}, params.stabil, ecc);
    if strcmp('Drift',em)
        fileName2 = sprintf('%s_%s_%s_Drift_%s_Threshold.mat', ...
            subjectsAll{ii}, condition{1}, params.stabil, singleEcc);
        if ~params.fig.FOLDED
            fileName4 = sprintf('%s_%s_%s_Drift_%s_SmallSpanPerform.mat', ...
                subjectsAll{ii},condition{1}, params.stabil{stabilIdx},singleEcc);
            fileName5 = sprintf('%s_%s_%s_Drift_%s_LargeSpanPerform.mat', ...
                subjectsAll{ii},condition{1},params.stabil{stabilIdx}, singleEcc);
            fileName6 = sprintf('%s_%s_%s_Drift_%s_BootstrapThreshold.mat', ...
                subjectsAll{ii},condition{1}, params.stabil, singleEcc);
        end
    elseif strcmp('Drift_MS',em)
        fileName2 = sprintf('%s_%s_%s_Drift_MS_%s_Threshold.mat', ...
            subjectsAll{ii},condition{1}, params.stabil, singleEcc);
    end
    if params.fig.FOLDED
        if strcmp('Z091',subjectsAll{ii})
            fileName3 = sprintf('%s_%s_%s_MS_%s_Threshold.mat', ...
                subjectsAll{ii},condition{1},params.stabil, '0eccEcc');
        else
            fileName3 = sprintf('%s_%s_%s_MS_%s_Threshold.mat', ...
                subjectsAll{ii},condition{1},params.stabil, '0ecc');
        end
    else
        fileName3 = sprintf('%s_%s_%s_MS_%s_Threshold.mat', ...
            subjectsAll{ii},condition{1}, params.stabil, singleEcc);
    end
    % fileName = 'Z023_Uncrowded_SpanAmpEval.mat';
    folderName = ...
        'MATFiles\';
    file1 = fullfile(folderName, fileName1);
    file2 = fullfile(folderName, fileName2);
    file3 = fullfile(folderName, fileName3);
    if exist(file2, 'file') == 0
        continue;
    end
    if exist('fileName4')
        file4 = fullfile(folderName, fileName4);
        load(file4);
        performStruct.SmallSpans = threshStruct;
    else
        performStruct.SmallSpans = NaN;
    end
    if exist('fileName5')
        file5 = fullfile(folderName, fileName5);
        load(file5);
        performStruct.LargeSpans = threshStruct;
    else
        performStruct.LargeSpans = NaN;
    end
    if exist('fileName6')
        file6 = fullfile(folderName, fileName6);
        load(file6);
        performStruct.BootThresh = threshInfo.thresh;
        performStruct.BootThreshAll = threshInfo.threshB;
    else
        performStruct.BootThresh = NaN;
        performStruct.BootThreshAll = NaN;
    end
    load(file1);
    load(file2);
    threshInfoDrift = threshInfo;
    if isnan(threshInfoDrift.thresh)
        continue;
    end
   
        subjectCond(ii) = spnAmpEvaluate;
        perform(ii).thresh = performStruct.BootThresh;
        perform(ii).bootStrapAll = performStruct.BootThreshAll;
        subjectThreshCond(ii) = threshInfoDrift;
        
     
    if huxlinProject == 1
        subjectCond(ii) = spnAmpEvaluate;
        perform(ii).thresh = performStruct.BootThresh;
        perform(ii).bootStrapAll = performStruct.BootThreshAll;
        subjectThreshCond(ii) = threshInfoDrift;
       
    else
        subjectCond(ii) = spnAmpEvaluate;
        perform(ii).thresh = performStruct.BootThresh;
        perform(ii).bootStrapAll = performStruct.BootThreshAll;
        subjectThreshCond(ii) = threshInfoDrift;
    
    end
    if ~exist('subjectCond','var')
        subjectCond(ii) = NaN;
        subjectThreshCond(ii) = NaN;
    end
    if ~exist('subjectMSCond','var')
    end
     subjectMSCond = [];
end
% end

