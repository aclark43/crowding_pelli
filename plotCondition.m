function p2 = plotCondition(var1, var2, params)
c = params.c;
        for ii = 1:length(var2)
            s1 = scatter(0,var2(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
            s1.MarkerFaceAlpha = .4;
            hold on
        end
        scatter(0,mean(var2),200,'k','filled')
        
        for ii = 1:length(var1)
            s1 = scatter(1,var1(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
            s1.MarkerFaceAlpha = .4;
            hold on
        end
        scatter(1,mean(var1),200,'k','filled')
        
        xS = [0 1];
        for ii = 1:params.subNum
            y1 = [var2(1,ii) var1(1,ii)];
            points = line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
            points.LineStyle = '--';
        end
        yMean = [mean(var2) mean(var1)];
        ySTD = [std(var2) std(var1)];
        points = line(xS, yMean, 'Color','k','LineWidth',2);
        
        ylim([0 10])
        xlim([-0.5 1.5])
        [~,p2,ci] = ttest(var2,var1);
%         ttestP = sprintf('(paired) p = %.3f', p2);
%         text(0.19, 9, ttestP,'Fontsize',20);
        
        ySEM = ySTD/(sqrt(params.subNum)); %Standard Error Of The Mean At Each Value Of �x�
        
        CI95 = tinv([0.025 0.975], params.subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
        yCI95 = bsxfun(@times, ySEM, CI95(:)); % Calculate 95% Confidence Intervals Of All Experiments At Each Value Of �x�
        
        errorbar(0,yMean(1),yCI95(1),yCI95(1),...
            'vertical', '-ok','MarkerSize',15,...
            'MarkerEdgeColor','black',...
            'MarkerFaceColor','black',...
            'CapSize',10,...
            'LineWidth',2)
        errorbar(1,yMean(2),yCI95(2),yCI95(2),...
            'vertical', '-ok','MarkerSize',15,...
            'MarkerEdgeColor','black',...
            'MarkerFaceColor','black',...
            'CapSize',10,...
            'LineWidth',2)
    end