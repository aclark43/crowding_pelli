function [xa, ya] = p2a(xp, yp, dmm, hmm, wmm, hnp, wnp)
if ~exist('dmm', 'var')
    % default to led screen settings
    dmm = 4100; % distance (mm) to monitor
    hmm = 302; % height (mm) of monitor
    wmm = 538; % width (mm) of monitor
    hnp = 1080; % height in number of pixels
    wnp = 1920; % width in number of pixels
end

xa = rad2deg(60 * atan(xp * wmm / dmm / wnp));
ya = rad2deg(60 * atan(yp * hmm / dmm / hnp));
end