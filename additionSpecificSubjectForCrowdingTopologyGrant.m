figure;
subplot(1,2,1)
    for ii = [2 6]%length(subjectsAll)
    plot(eccVals, forBouma{2}(ii,:)* (1.4),'-o','LineWidth',3,...
        'Color',c1(ii,:),'MarkerFace',c1(ii,:)) %plotting in terms of critical spacing
    hold on
    end
    coefficients1 = polyfit(eccVals,mean([(forBouma{2}(2,:)); ...
        (forBouma{2}(6,:))])*(1.4), 1);
    xFit = linspace(min(eccVals), max(eccVals), 1000);
    % xFitTemp - 
    yFit = polyval(coefficients1 , xFit);
    % m = (yFit(end)-yFit(1))/(xFit(end)-xFit(1)) ; %same as coefficents(1)
    temp = plot(xFit, yFit,'--','Color','b',...
        'MarkerSize',12,'MarkerFaceColor','b','LineWidth',3);
    hold on
    xticks([0 10 15 25])
    % plot(eccVals,mean(forBouma{2})*(1.4),'o','MarkerSize',12,'MarkerFaceColor','k');
    % [~,p,bCB,r] = LinRegression(eccVals, mean(forBouma{2})*(1.4),...
    %     0,NaN,1,0);
    legend(temp,sprintf('b = %.2f',coefficients1(1)),'Location','northwest');
    axis square
    xlim([-5 30])
    ylim([2 5])
    xlabel('Eccentricity (arcmin)')
    ylabel('Critical Spacing (arcmin)')
    sigma = eccVals;
    w = mean(forBouma{2});
    BoumaLaw = (0.5 * sigma);
    hold on
    % temp(2) = plot(sigma, BoumaLaw,'--','Color','m',...
    %     'MarkerSize',12,'MarkerFaceColor','m','LineWidth',3);
    % hold on
    % temp(3) = plot(sigma, BoumaLaw+w,'--','Color','b',...
    %     'MarkerSize',12,'MarkerFaceColor','b','LineWidth',3);
    % legend(temp,{sprintf('Best Fit Line, b = %.2f',coefficients(1)),...
    %     'Bouma Law','Bouma + w'},'Location','northwest')



colorForPeriph = [1 0 0;c1(5,:);c1(2,:);c1(6,:)];
subplot(1,2,2)
% t = tiledlayout(2,1);
% ax1 = axes(t);

    for ii = [3 4]%size(peripheralData{1})
    plot(peripheralData{3}(5:8), peripheralData{2}(ii,5:8)*(1.4),'-o','LineWidth',3,...
        'Color',colorForPeriph(ii,:),'MarkerFace',colorForPeriph(ii,:)) %plotting in terms of critical spacing
    hold on
    end
    coefficients = polyfit(peripheralData{3}(5:8),...
        mean([peripheralData{2}(3,5:8);peripheralData{2}(4,5:8)])*(1.4), 1);
    xFit2 = linspace(min(peripheralData{3}(5:8)), max(peripheralData{3}(5:8)), 1000);
    yFit2 = polyval(coefficients , xFit2);
%     m = (yFit(end)-yFit(1))/(xFit(end)-xFit(1)) ; %same as coefficents(1)
    temp2 = plot(xFit2, yFit2,'--','Color','k',...
        'MarkerSize',12,'MarkerFaceColor','k','LineWidth',3);
    hold on
    plot(xFit2, polyval(coefficients1, xFit2),'--','Color','b',...
        'MarkerSize',12,'MarkerFaceColor','k','LineWidth',3);
%     hold on
%     plot(peripheralData{3}(5:8),mean(peripheralData{2}(:,5:8))*(1.4),...
%         'o','MarkerSize',12,'MarkerFaceColor','k');
    axis square
    xlim([40 380])
    ylim([4 80])
    xlabel('Eccentricity (arcmin)')
    ylabel('Critical Spacing (arcmin)')
    xticks([60 120 240 360])
    sigma = peripheralData{3}(5:8);
    w = mean(peripheralData{2}(:,5:8));
    BoumaLaw = (0.5 * sigma);
    hold on
%     temp2(2) = plot(sigma, BoumaLaw,'--','Color','m',...
%         'MarkerSize',12,'MarkerFaceColor','m','LineWidth',3);

    legend(temp2,{sprintf('b = %.2f',coefficients(1)),...
        'Boumas Law'},'Location','northwest')

    percentChange = (abs(0.15 - 0.05)/0.15)*100

% ax2 = axes(t);
% plot(ax2,peripheralData{3}(5:8)/60,mean(peripheralData{2}(:,5:8))*(1.4),...
%     'o','MarkerSize',12,'MarkerFaceColor','k')
% ax2.XAxisLocation = 'top';
% ax2.YAxisLocation = 'right';
% ax2.Color = 'none';
% ax1.Box = 'off';
% ax2.Box = 'off';
saveas(gcf, 'C:/Users/Ruccilab/Box/APLab-Projects/Grants/ClarkF31_Resubmission/FoveavsPeriph.epsc');

figure;
plot(data(11).eccentricities, [data(4).thresholds - data(3).thresholds],'-o');
hold on
% errorbar(data(3).eccentricities, mean([...
%     data(4).thresholds - data(3).thresholds;...
%     data(12).thresholds - data(11).thresholds]),...
%     sem([...
%     data(4).thresholds - data(3).thresholds;...
%     data(12).thresholds - data(11).thresholds]),...
%     '-o','MarkerSize',10,'Color','k',...
%     'MarkerEdgeColor','k','MarkerFaceColor','k');
xticks([0 10 15 25])
xlim([-1 26])
ylabel('\Delta Threshold')
axis square

