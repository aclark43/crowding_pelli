function [ xValues, yValues ] = checkXYArt( xValues, yValues, artifactCount, limit )
%Checks to see if there are strange artifacts for the heatmaps and x/y
%values analysis.
%   xValues = vector of X Values
%   yValues = vector of Y Values
%   artifactCount = number of times that a number can repeat itself before
%   considered an artifact


xUnqiue = (unique(xValues));
xUnqiue(isnan(xUnqiue)) = limit; %%Set to value larger than the size of the matrix you want
xCheck = xUnqiue(artifactCount<histc(xValues,sort(xUnqiue)));
for xx = 1:length(xCheck)
    if ~isempty(xCheck)
        xCheckIdx = find(xValues == xCheck(xx));
        xValues(xCheckIdx) = NaN;
%         yValues(xCheckIdx) = NaN;
        if ~isempty(xCheckIdx)
            xValues(xCheckIdx(1)) = xCheck(xx);
        end
    end
end

yUnqiue = unique(yValues);
yUnqiue(isnan(yUnqiue)) = limit; %%Set to value larger than the size of the matrix you want
yCheck = yUnqiue(artifactCount<histcounts(yValues,sort(yUnqiue)));
for yy = 1:length(yCheck)
    if ~isempty(yCheck)
        yCheckIdx = find(yValues == yCheck(yy));
%         xValues(yCheckIdx) = NaN;
        yValues(yCheckIdx) = NaN;
        if ~isempty(yCheckIdx)
            yValues(yCheckIdx(1)) = yCheck(yy);
        end
    end
end

end

