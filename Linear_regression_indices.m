function [ Linear_regression_indices ] = Linear_regression_indices(XTRAIN,ytrain,XTEST )
         
Linear_regression_indices=(polyval(polyfit(XTRAIN,ytrain,1),XTEST));

end