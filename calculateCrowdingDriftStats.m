function [span, amplitude, meanSpan, stdSpan, ...
    meanAmplitude, stdAmplitude, prlDistance,...
    prlDistanceX, prlDistanceY] = ...
    calculateCrowdingDriftStats(x, y)

span = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95); %Caclulates Span

amplitude = sqrt((x(end) - x(1))^2 + ...
    (y(end) - y(1))^2);

meanSpan = mean(span);
stdSpan= ...
    std(span)/...
    sqrt(length(span));

meanAmplitude = mean(amplitude(counter));
stdAmplitude = ...
    std(amplitude)/sqrt(length(amplitude));

prlDistance = mean(sqrt(x.^2 + y.^2));
prlDistanceX = mean(abs(x));
prlDistanceY = mean(abs(y));




end