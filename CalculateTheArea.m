% INPUT - POSX and POSY:  are calculated from GetTheFreq and are the X and Y
%           positions of all the EM traces selected (note: per each trace its
%           mean has been subtracted so all the EM are centered)
%         thres: is the proportion of the probability distribution of the position
%           on which the area is calculated.
%         rangeXY: range of position on the X and Y axis from which the
%           probability distribution is calculated (if rangeXY= 60, the
%           distribution is centered at 0,0 and goes from -60 to +60 on
%           both axis).
%         nImagePixel: number of bins used by matlab to bin the
%           probability  distribution 
% OUTPUT - area: area of the position distribution in 2d at the threshold
%           choosen.

function [area] =CalculateTheArea(PosX,PosY,thres, rangeXY, nImagePixel);

    
    conversionFactor = ((2*rangeXY)^2/nImagePixel^2);
    N = GetTheFreq(PosX+i*PosY, rangeXY,nImagePixel);
    p = N/sum(sum(N));
    v = [];
    for th = [0:0.0001:1]
        v = [v sum(sum(p(p>th)))];
    end

    ii = find(v>thres);
    th = [0:0.0001:1];
    area= sum(sum(p>th(length(ii)+1)))*conversionFactor;