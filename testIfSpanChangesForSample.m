function testIfSpanChangesForSample(subjectUnc,subjectThreshUnc,subNum, subjectsAll)

figure;
for ii = 1:subNum
    thresholdSizeSWRound = round(subjectUnc(ii).stimulusSize,1);
    matchingThresholdRound = round(subjectThreshUnc(ii).thresh,1);
    [~,thresholdIdx] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    swUnc = sprintf('strokeWidth_%i', subjectUnc(ii).SW(thresholdIdx));
    thresholdUncrowdedSpan{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;%subjectUnc(ii).meanDriftSpan(thresholdIdx);
    fprintf('Current Mean Span for %s = %.3f\n', subjectsAll{ii}, thresholdUncrowdedSpan{ii})
    checkSpan = [];
    for i = 1:length(subjectThreshUnc(ii).em.ecc_0.(swUnc).position)
        x = subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).x;
        y = subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).y;
        checkSpan(i) = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95);
    end
    fprintf('Checking Mean Span for same Length %s = %.3f\n', subjectsAll{ii}, mean(checkSpan));
    newSpan = [];
    clear i
    for i = 1:length(subjectThreshUnc(ii).em.ecc_0.(swUnc).position)
        if ii == 7
            nx = subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).x;
            ny = subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).y;
        else
            nx = subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).x...
                (1:3:length(subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).x));
            ny = subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).y...
                (1:3:length(subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).y));
        end
        newSpan(i) = quantile(sqrt((nx-mean(nx)).^2 + (ny-mean(ny)).^2), .95);  
    end
    fprintf('New Span for Shorter %s = %.3f\n\n', subjectsAll{ii}, mean(newSpan));
    scatter(mean(checkSpan),mean(newSpan));
    hold on
end
xlabel('DPI Span')
ylabel('SubSampl Span')





end