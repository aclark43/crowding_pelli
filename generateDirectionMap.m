function [meanEucDistance, dAbs] = generateDirectionMap(xValues, yValues, title_str )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
D = zeros(1,length(xValues));
for ii = 1:length(xValues)
    x = xValues(ii);
    y = yValues(ii);
    D(ii) = sqrt((x-0)^2 + (y-0)^2);
    thetaAll(ii) = round(2*atan(y/(x+sqrt(x^2+y^2))),2);
end


figure;
histogram(D,100);

meanEucDistance = mean(~isnan(D));
xlabel('Euclidean Distance in arcmin from Center')
ylabel('# of Samples')
title(sprintf('E.Distance_Fixation_%s',title_str));
saveas(gcf,sprintf('../../SummaryDocuments/HuxlinPTFigures/HUX_Fixation_Direction%s.png', title_str));

figure;
for ii = 1:length(xValues)
    dAbs(1,ii) = xValues(ii);
    dAbs(2,ii) = yValues(ii);
end
figure;
hax = axes;
histogram(dAbs(1,:),100);
hold on
histogram(dAbs(2,:),100);
line([0 0],get(hax,'YLim'),'Color',[0 0 0],'LineWidth',1);
legend('X','Y')
xlabel('Distance from Center (arcmin)');
ylabel('# of Positions');
title1 = 'Fixation Distribution from Center';
title2 = title_str;
title({title1;title2});
saveas(gcf,sprintf('../../SummaryDocuments/HuxlinPTFigures/HUX_AbsFixation_Direction%s.png', title_str));

figure;
[C,~,ic] = unique(thetaAll(~isnan(thetaAll)));
a_counts = accumarray(ic,1);
valueCounts = [C', a_counts];
figure;
polarplot(valueCounts(:,1),valueCounts(:,2));
title1 = 'Drift Direction During Fixation';
title2 = title_str;
title({title1;title2});
saveas(gcf,sprintf('../../SummaryDocuments/HuxlinPTFigures/HUX_PolarDriftDirection%s.png', title_str));

close all;

end

