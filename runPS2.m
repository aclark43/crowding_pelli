function runPS2()
% dc = 15; %diffusion constant
f = linspace(1, 80, 80); % temporal frequencies (Hz)
k = logspace(-1, 2, 100); % spatial frequencies (cpd)

digits = [3, 5, 6, 9];
vertical = 1;

pixelAngle = 0.25;
widthPixel = 6;

bufferSizeAngle = 20 * 60; % arcmin
bufferSizePixel = bufferSizeAngle / pixelAngle;

padsizepixel = 500;

sz = bufferSizePixel;
k = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);


ca = [-40, 20];

kdc = k;

figure; clf;
for patternVS1 = 1:2
    for di = 1%:length(digits)
        im = drawDigit(digits(di), widthPixel);
        if patternVS1 == 1
            temp = padarray(im-mean(im(:,size(im,2)/2)),[padsizepixel, padsizepixel],0,'both');
            im_buffered = repmat(temp,2*ceil(sz / size(temp, 2)) ,...
                2*ceil(sz / size(temp, 1)));
        else
            if vertical == 1
                if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
                    im_buffered = padarray(im-mean(im(:,size(im,2)/2)), [bufferSizePixel, bufferSizePixel],0, 'both');
                else
                    im_buffered = padarray(im-mean(im(:,(size(im,2)/2)+1)), [bufferSizePixel, bufferSizePixel],0, 'both');
                end
            else
                im_buffered = padarray(im-mean(im(round(size(im,1)/2)-1,:)), [bufferSizePixel, bufferSizePixel],0, 'both');
            end
        end
        
        
        [m, n] = size(im_buffered);
        df = floor((m - sz)/2);
        im_buffered = im_buffered(df:(df + sz - 1), :);
        df = floor((n - sz)/2);
        im_buffered = im_buffered(:, df:(df + sz - 1));
        
        subplot(2, 2, patternVS1);
        imagesc((1:sz) * pixelAngle / 60, (1:sz) * pixelAngle / 60, im_buffered); axis image;
        xlabel( 'deg'); ylabel('deg');
        if patternVS1 == 2
            xlim(sz/2 * pixelAngle / 60  + [-0.5, 0.5]);
            ylim(sz/2 * pixelAngle / 60  + [-0.5, 0.5]);
            title('single');
        else
            title('repeated');
        end
        
        if vertical == 1
           spec = fftshift( mean(fft(im_buffered, [], 1), 2) );

        else
            spec = fftshift(fft(im_buffered((length(im_buffered)/2)-1,:)));
        end
        
        spec = spec / max(spec);
    
        subplot(2, 1, 2); hold on;
        loglog(k(sz/2:end),...
            pow2db((abs(spec(sz/2:end))).^2),'-');
    end
end

subplot(2, 1, 2);
legend({'repeated', 'single'}, 'Location', 'southwest');
ylim([-100, 30]);
xlabel('spatial frequency (cpd)');
ylabel('vertical power (db)');
end

function digit = drawDigit(whichDigit, pixelwidth, patterned)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;

switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
        % end
end
end

%     if exist('patterned','var')
%         digit = [digit digit digit digit digit;
%             digit digit digit digit digit];
%     end
