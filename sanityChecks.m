% check trial component timing and other sanity checks

%% check the pest procedure (assuming only one eccentricity)
flds = {'PestLevel', 'Correct', 'TargetStrokewidth'};
outs = cell(size(flds));
for fi = 1:length(flds)
    for i = 1:length(pptrials)
        if pptrials{i}.Correct == 3
            continue; % fixation trials
        end
        outs{fi} = [outs{fi}; pptrials{i}.(flds{fi})];
    end
end

cidx = find(outs{2});
widx = find(outs{2} == 0);

figure(1); clf; hold on; set(gcf, 'Color', 'w');
plot(outs{1}, 'k-');
plot(cidx, outs{1}(cidx), 'g.', 'markersize', 15);
plot(widx, outs{1}(widx), 'r.', 'markersize', 15);
plot(outs{3}, 'b-');
xlabel('trial #');
ylabel('pest Level (stroke width)');
set(gca, 'FontSize', 14, 'FontWeight', 'bold');
ylim([0, 12]);
title('Pest Test');

%% check that the stimulus presentation time is correct
flds = {'TimeTargetOFF', 'TimeTargetON'};
outs = cell(size(flds));
for fi = 1:length(flds)
    for i = 1:length(pptrials)
        if pptrials{i}.Correct == 3
            continue; % fixation trials
        end
        outs{fi} = [outs{fi}; pptrials{i}.(flds{fi})];
    end
end

figure(2); clf; hold on; set(gcf, 'Color', 'w');
hist(outs{1} - outs{2});
xlabel('stimulus duration');
ylabel('count');
set(gca, 'FontSize', 14, 'FontWeight', 'bold');
ylim([0, 12]);
title('Stimulus Duration');