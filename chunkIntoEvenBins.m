function newVec = chunkIntoEvenBins(data,numBins)

meanData = mean(data);

y=quantile(data,numBins);

y = [0 y];
for i = 1:length(y)-1
    newVec{i} = find(data > y(i) & data < y(i+1));
%     lengthEach = length(idxInOrder);
end

% [o1 o2 o3] = histcounts(data,numBins)
end