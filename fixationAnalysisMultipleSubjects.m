function fixStruct = fixationAnalysisMultipleSubjects(fixationInfo, subjectsAll, subNum, params)
%Fixation analysis across several subjects
%   looks at the fixation behavior across several subjects. Includes
%   heatmaps of different time bins, distance from 0,0 across time bins,
%   etc.
fixStruct = [];
c = params.c;
%% Looking at specific TimeBins during Fixation
% This is serperating fixation into 10 different time bins, then
% performing a series of analysis on each of those time bins to see how
% fixation changes over time.
for ii = 1:subNum
    for numTrials = 1:length(fixationInfo{ii}.tracesX)
        counter = 1;
        for numSamp = 1:10
            bySampleX(ii).first10Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).first10Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 11:50
            bySampleX(ii).second40Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).second40Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 51:100
            bySampleX(ii).third50Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).third50Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 101:200
            bySampleX(ii).fourth100Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).fourth100Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 201:300
            bySampleX(ii).fifth100Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).fifth100Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 301:500
            bySampleX(ii).sixth200Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).sixth200Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 501:700
            bySampleX(ii).seventh200Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).seventh200Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 701:1000
            bySampleX(ii).eighth300Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).eighth300Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 1001:1300
            bySampleX(ii).ninth300Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).ninth300Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 1301:1600
            if length(fixationInfo{ii}.tracesX{numTrials}) < 1600
                continue;
            end
            bySampleX(ii).tenth300Samp(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).tenth300Samp(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 1:1600 %%whole sample
            if length(fixationInfo{ii}.tracesX{numTrials}) < 1600
                continue;
            end
            bySampleX(ii).allSamples(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).allSamples(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
        counter = 1;
        for numSamp = 1:165 %%first 500MS
            bySampleX(ii).first500ms(numTrials,counter) = fixationInfo{ii}.tracesX{numTrials}(numSamp);
            bySampleY(ii).first500ms(numTrials,counter) = fixationInfo{ii}.tracesY{numTrials}(numSamp);
            counter = counter + 1;
        end
    end
end

sizeSamples = size(bySampleX(ii));

%% Build COM and Heatmaps for Fixation
for ii = 1:subNum
    
    allNames = fieldnames(bySampleX);
    for numSampleBins = 1:length(allNames)
        nameSamp = allNames{numSampleBins};
        
        xValStruct = bySampleX(ii).(nameSamp);
        yValStruct = bySampleY(ii).(nameSamp);
        x = [];
        y = [];
        xCat = [];
        yCat = [];
        if numSampleBins == 11 %%all values
            for i = 1:size(xValStruct)
                x = xValStruct(i,:);
                y = yValStruct(i,:);
                
                [nXValues, nYValues] = cleanXYTrialsFromNans(x, y, 200);
                [height,width] = size(nXValues);
                if height>width
                    nXValues = nXValues';
                    nYValues = nYValues';
                end
                trialLevel.xValues{ii,i} = nXValues;
                trialLevel.yValues{ii,i} = nYValues;
                
                %                 [trialLevel.com(ii,i), ...
                %                     trialLevel.massX(ii,i), ...
                %                     trialLevel.massY(ii,i), ...
                %                     trialLevel.prl(ii,i)] ...
                %                     = centerOfMassCalculation(nXValues, nYValues);
                
                %                 trialLevel.angle(ii,i) = atan2d(nanmean(nYValues),nanmean(nXValues));
                
                xCat = [xCat nXValues];
                yCat = [yCat nYValues];
                
                
            end
            trialLevel.xValuesCAT{ii} = xCat;
            trialLevel.yValuesCAT{ii} = yCat;
            
                        massLevel.stdX(ii) = nanstd(xCat);
                        massLevel.stdY(ii) = nanstd(yCat);
            
                        [massLevel.com(ii), ...
                            massLevel.massX(ii), ...
                            massLevel.massY(ii), ...
                            massLevel.prl(ii)] ...
                            = centerOfMassCalculation(xCat, yCat);
            
            limit.xmin = floor(min(xCat));
            limit.xmax = ceil(max(xCat));
            limit.ymin = floor(min(yCat));
            limit.ymax = ceil(max(yCat));
            
            result1 = MyHistogram2(xValuesNew, yValuesNew, [limit.xmin,limit.xmax,30;limit.ymin,limit.ymax,30]);
            result = result1./(max(max(result1)));
            
            x_arcmin = linspace(limit.xmin, limit.xmax, size(result, 1)); % these are the pretend arcmin corresponding to the pdf
            y_arcmin = linspace(limit.ymin, limit.ymax, size(result, 1));
            
            [~, idx] = max(result(:)); % find the max
            
            [row, col] = ind2sub(size(result), idx); % convert to row and col
            
            x_pk = x_arcmin(row); % get row and col in arcmin
            y_pk = y_arcmin(col);
            
            massLevel.x_pk(ii) = x_pk;
            massLevel.y_pk(ii) = y_pk;
            massLevel.eucDist(ii) = sqrt(sum((x_pk - y_pk) .^ 2));
            
            
        end
        
        xVal = reshape(xValStruct, [1, size(xValStruct,1)*size(xValStruct,2)]);
        yVal = reshape(yValStruct, [1, size(yValStruct,1)*size(yValStruct,2)]);
        [nXValues, nYValues] = cleanXYTrialsFromNans(xVal, yVal, 200);
        
        [h,w] = size(nXValues);
        if h>w
            nXValues = nXValues';
            nYValues = nYValues';
        end
%         
        timeIntervalLevel.meanOfXY(ii,numSampleBins) = nanmean([nXValues nYValues]);
        timeIntervalLevel.stdOfXY(ii,numSampleBins) = nanstd([nXValues nYValues]);
        timeIntervalLevel.eucDist(ii,numSampleBins) =  massLevel.eucDist(ii);
%         timeIntervalLevel.stdOfXY(ii,numSampleBins) = nanstd([nXValues nYValues]);
%         
%         [timeIntervalLevel.com(ii,numSampleBins), ...
%             timeIntervalLevel.massX(ii,numSampleBins), ...
%             timeIntervalLevel.massY(ii,numSampleBins), ...
%             timeIntervalLevel.prl(ii,numSampleBins),...
%             timeIntervalLevel.eucDistance{ii,numSampleBins}] ...
%             = centerOfMassCalculation(nXValues, nYValues);
        
        %         fixStruct = [];
        
    end
end
close all

time = [10 10+40 10+40+50 10+40+50+100 10+40+50+100+100 ...
    10+40+50+100+100+200 10+40+50+100+100+200+200 ...
    10+40+50+100+100+200+200+300 10+40+50+100+100+200+200+300+300 ...
    10+40+50+100+100+200+200+300+300+300];

fixStruct.timeIntervals = timeIntervalLevel;
fixStruct.trialLevel = trialLevel;
% fixStruct.timeIntervals = time;
% fixStruct.meanofXY = timeIntervalLevel.meanOfXY(:,1:10);
% fixStruct.meanofXYCentered = ...
%     abs(timeIntervalLevel.meanOfXY(:,1:10)-timeIntervalLevel.meanOfXY(1:subNum,1));

%% Makes figure for mean across time (10 points)
timeBins = round((1000/330)*time);
figure;
plotAcrossTime(params,subNum,time,timeIntervalLevel.meanOfXY(:,1:10),c);
% xlim([0 11])
ylabel('Mean Distance')
xlabel('Time(ms)');
legend(subjectsAll,'Location','northwest')
title('Mean from Center across Time (NonCumalitive)')
set(gcf, 'Position',  [2000, 100, 1400, 800])
line([0 time(10)], [0 0],'Color','black','LineStyle','--')

saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/MeanAcrossTime.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MeanAcrossTime.png');


figure;

plotAcrossTime(params,subNum,time,...
    abs(timeIntervalLevel.meanOfXY(:,1:10)-timeIntervalLevel.meanOfXY(1:subNum,1)),...
    c);
ylabel('Absolute Mean Distance')
xlabel('Time(ms)');
legend(subjectsAll,'Location','northwest')
title('\Delta Mean from Center across Time (NonCumalitive)')
set(gcf, 'Position',  [2000, 100, 1400, 800])
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/AbsoluteMeanAcrossTime.png');
%     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HistogramsOfPRL.png');

% makes figure for mean of means for all times (1 point)
figure;
% for ii = 1:subNum
%     distVal(ii) = abs(mean(meanAXY(ii,:)));
%     distSTD(ii) = std(meanAXY(ii,:));
% end
leg = plotCombineTime(params,subNum,abs(timeIntervalLevel.meanOfXY(:,1:10)),(timeIntervalLevel.stdOfXY(:,1:10)),c, 1);
line([0,subNum+1],[0,0])
xlim([0 subNum+1]);
ylim([-5 80]);
xticks([1:subNum])
xticklabels(subjectsAll)%{'HUX1','HUX3','HUX4','HUX6','HUX7','HUX8','Z070','AshleyDDPI'})
legend(leg,xticklabels,'Location','northwest')
ylabel('Absolute Mean Distance from Center');
xlabel('Subject')
title('Distance from Center')
set(gcf, 'Position',  [2000, 300, 1000, 600])
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/AbsoluteMeanSinglePnt.png');


% Makes figure for COM across time (10 points)
fixStruct.massX = timeIntervalLevel.massX(:,1:10);
fixStruct.massY = timeIntervalLevel.massY(:,1:10);
figure;
subplot(2,1,1);
plotAcrossTime(params,subNum,time,timeIntervalLevel.massX(:,1:10),c);
ylabel('Mean Center of Mass X')
xlabel('Time(ms)')

subplot(2,1,2);
plotAcrossTime(params,subNum,time,timeIntervalLevel.massY(:,1:10),c);
ylabel('Mean Center of Mass Y')
xlabel('Time(ms)')
set(gcf, 'Position',  [2000, 300, 1000, 600])
suptitle('Center of Mass Across Time')

saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/CenterofMass.png');
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/CenterofMass.fig');

%
fixStruct.com = timeIntervalLevel.com(:,1:10);
figure;
plotAcrossTime(params,subNum,time,timeIntervalLevel.com(:,1:10),c);
ylabel('Hypot Center of Mass X and Y')
xlabel('Time(ms)')
legend(subjectsAll)
set(gcf, 'Position',  [2000, 300, 1000, 600])
title('Hypot. of Center of MassX and Y Across Time')

saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/CenterofMassHypo.png');
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/CenterofMassHypo.fig');

%% makes figure for mean of COMs for all times (1 point)
figure;
subplot(2,1,1);
plotCombineTime(params,subNum, mean(timeIntervalLevel.massX(1:subNum,1:10)'),std(timeIntervalLevel.massX(1:subNum,1:10)'),c, 0);
line([0,subNum+1],[0,0])
xticks([1:subNum]);
xticklabels(subjectsAll);
ylabel('Center of Mass Mean X');
xlabel('Subject')
title('Center of Mass X')

subplot(2,1,2);
plotCombineTime(params,subNum,mean(timeIntervalLevel.massY(1:subNum,1:10)'),std(timeIntervalLevel.massY(1:subNum,1:10)'),c,0);
line([0,subNum+1],[0,0])
xticks([1:subNum])
xticklabels(subjectsAll)
set(gcf, 'Position',  [2000, 300, 1000, 600])
ylabel('Center of Mass Mean Y');
xlabel('Subject')
title('Center of Mass Y')
suptitle('Center of Mass Means')
% subplot(2,1,2);
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/CenterofMassMeans.png');


%% Mean of X and Y scatterplot trial Level
figure;
trialLevel.massX(trialLevel.massX == 0) = NaN;
trialLevel.massy(trialLevel.massY == 0) = NaN;
trialLevel.com(trialLevel.com == 0) = NaN;
for ii = 1:subNum
    x = nanmean(trialLevel.massX(ii,:));
    y = nanmean(trialLevel.massY(ii,:));
    std1 = nanstd(trialLevel.massX(ii,:))/4;
    std2 = nanstd(trialLevel.massY(ii,:))/4;
    
    if strcmp('HUX4',subjectsAll{ii}) || ...
            strcmp('AshleyDDPI',subjectsAll{ii}) || ...
            strcmp('HUX12',subjectsAll{ii}) || ...
            strcmp('HUX18',subjectsAll{ii}) || ...
            strcmp('HUX17',subjectsAll{ii}) || ...
            strcmp('HUX10',subjectsAll{ii})
        leg(ii) =  errorbar(x, y, ...
            std1, std1, ...
            std2, std2, ...
            '*', 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',10);
    else
        leg(ii) =  errorbar(x, y, ...
            std1, std1, ...
            std2, std2, ...
            'o', 'MarkerFaceColor',c(ii,:), 'Color', c(ii,:),'MarkerSize',10);
        hold on
    end
end
axisSize = 15;
axis([-axisSize axisSize -axisSize axisSize])
axis square
legend(leg,subjectsAll);
% line([-15 15], [-15 15])
line([0 0],[-axisSize axisSize],'Color','k')
line([-axisSize axisSize],[0 0],'Color','k')

ylabel('Center of Mass Y')
xlabel('Center of Mass X')
set(gcf, 'Position',  [2000, 300, 800, 600])
title('Center of Mass Means Trial Level & 2SD');
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/CenterofMassMeansTRIAL2D.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/CenterofMassMeansTRIAL2D.png');

%% ^^ Mass Level
% [massLevel(ii).com(i), ...
%     massLevel(ii).massY(i), ...
%     massLevel(ii).massY(i), ...
%     massLevel(ii).prl(i)] ...
%     = centerOfMassCalculation(xCat, yCat);
massLevel.massX(massLevel.massX == 0) = NaN;
massLevel.massY(massLevel.massY == 0) = NaN;
massLevel.com(massLevel.com == 0) = NaN;

figure;
for ii = 1:subNum
    x = nanmean(massLevel.massX(ii));
    y = nanmean(massLevel.massY(ii));
    std1 = (massLevel.stdX(ii))/4;
    std2 = (massLevel.stdY(ii))/4;
    
    fixStruct.COMCatLevel(ii,1) = x;
    fixStruct.COMCatLevel(ii,2) = y;
    if params.control(ii) == 1
        leg(ii) =  errorbar(x, y, ...
            std1, std1, ...
            std2, std2, ...
            '*', 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',10);
    else
        leg(ii) =  errorbar(x, y, ...
            std1, std1, ...
            std2, std2, ...
            'o', 'MarkerFaceColor',c(ii,:), 'Color', c(ii,:),'MarkerSize',10);
        hold on
    end
end
axis([-axisSize axisSize -axisSize axisSize])
legend(leg,subjectsAll);
% line([-15 15], [-15 15])
line([0 0],[-axisSize axisSize],'Color','k')
line([-axisSize axisSize],[0 0],'Color','k')

ylabel('Center of Mass Y')
xlabel('Center of Mass X')
set(gcf, 'Position',  [2000, 300, 800, 600])
title('Center of Mass Means CAT Level & 2SD Position');
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/CenterofMassMeansCAT2D.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/CenterofMassMeansCAT2D.png');


%% mean of mean x and y
figure;
% for ii = 1:subNum
%     meanCOM(ii) = abs(mean([meanCOMX(ii) meanCOMY(ii)]));
%     stdCOM(ii) = std([meanCOMX(ii) meanCOMY(ii)]);
% end
leg = plotCombineTime(params,subNum,...
    nanmean([trialLevel.massX(:,1:10) trialLevel.massY(:,1:10)]'),...
    nanstd([trialLevel.massX(:,1:10) trialLevel.massY(:,1:10)]'),c,0);
line([0,subNum+1],[0,0])
% legend(leg,subjectsAll);
xticks([1:subNum])
xticklabels(subjectsAll)
set(gcf, 'Position',  [2000, 500, 1700, 400])
ylabel('Center of Mass Mean');
xlabel('Subject')
title('Absolute Mean(XY) Center of Mass')
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/AbsCenterofMassMeans.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/AbsCenterofMassMeans.png');

fixStruct.massLevel = massLevel;
% close all;


%% plot on top of heat map for sanity check
% figure
% axes('pos',[.1 .6 .5 .3])openfig(___)
for ii = 1:length(subjectsAll)
    clear leg
    heatmap = openfig(sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/ALL/Drift_5000_HeatMapFixation%s.fig',...
        subjectsAll{ii}));
    axis([-30 30 -30 30])
    colormap(flipud(bone))
    hold on
    %     leg(1) = plot(nanmean(trialLevel.massX(19,1:10)),nanmean(trialLevel.massY(19,1:10)),...
    %         '*','Color','r','MarkerSize',15);
    %
    x = nanmean(massLevel.massX(ii));
    y = nanmean(massLevel.massY(ii));
    %     fixStruct.COMCatLevel(ii,1) = x;
    %     fixStruct.COMCatLevel(ii,2) = y;
    %     hold on
    
    leg(1) = plot(x,y,...
        '*','Color','g','MarkerSize',15,'LineWidth',2);
    
    clear x; clear y;
    x = nanmean(trialLevel.massX(ii,:));
    y = nanmean(trialLevel.massY(ii,:));
    hold on
    leg(2) = plot(x,y,...
        '*','Color','y','MarkerSize',15,'LineWidth',2);
    
    legend(leg,{'COM Means CAT Level',...
        'COM Means Trial Level'});
    
    %     saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/HeatMapsWithMeasures.png');
    saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sHeatMapsWithMeasures.png', subjectsAll{ii}));
    
end


   