function individualHeatMap(params, pptrials, useTrials, trialChar, title_str)
%Plot the heat map overtime for an individual trial
%   Detailed explanation goes here

trialsNoOS = osRemoverInEM(pptrials);
temptrials = trialsNoOS(useTrials);
for i = 1:length(temptrials)
    
    xx= temptrials{i}.x.position(round(temptrials{i}.TimeTargetON):round(temptrials{i}.TimeTargetOFF)) + params.pixelAngle*temptrials{i}.xoffset;
    yy= temptrials{i}.y.position(round(temptrials{i}.TimeTargetON):round(temptrials{i}.TimeTargetOFF)) + params.pixelAngle*temptrials{i}.yoffset;
    
    xxs = temptrials{i}.x.position(double(round(temptrials{i}.TimeTargetON)))+ params.pixelAngle*temptrials{i}.xoffset;
    yys = temptrials{i}.y.position(double(round(temptrials{i}.TimeTargetON)))+ params.pixelAngle*temptrials{i}.yoffset;
    
    hold on
    stimuliSize = temptrials{i}.TargetStrokewidth * params.pixelAngle;
    
    
    figure
    load('./MyColormaps.mat');
    set(gcf, 'Colormap', mycmap)
    
    
    hold on
    pcolor(linspace(-limit, limit, size(temptrials, 1)), linspace(-limit, limit,size(temptrials, 1)), temptrials');
    % imgaussfilt(f)
    plot([-limit limit], [0 0], '--k')
    plot([0 0], [-limit limit], '--k')
    set(gca, 'FontSize', 12)
    xlabel('X [arcmin]')
    ylabel('Y [arcmin]')
    axis tight
    axis square
    caxis([floor(min(min(result))) ceil(max(max(result)))])
    % colorbar
    shading interp;
    colorbar
    stimuliSize = sw * params.pixelAngle;
    
    if params.crowded == true
        rectangle('Position',[-(2*stimuliSize+(2*stimuliSize*1.4)) -stimuliSize*5 2*stimuliSize 10*stimuliSize])
        rectangle('Position',[(2*stimuliSize*1.4) -stimuliSize*5 2*stimuliSize 10*stimuliSize])
        rectangle('Position',[(-stimuliSize) -(5*stimuliSize+(10*stimuliSize*1.4)) (2*stimuliSize) (10*stimuliSize)])
        rectangle('Position',[(-stimuliSize) ((10*stimuliSize*1.4)-5*stimuliSize) (2*stimuliSize) (10*stimuliSize)])
    end
    rectangle('Position',[(-stimuliSize) (-stimuliSize*5) (2*stimuliSize) (10*stimuliSize)])
    
    
    graphTitle = sprintf('Drift_Heat_Map_for_Strokewidth_%i,_Condition_%s',(sw), (title_str));
    %graphTitle = ({['Drift Heat Map for',(sw), 'Condition', (title_str)]});
    title(graphTitle,'Interpreter','none');
    
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.epsc', trialChar.Subject, graphTitle));
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.jpeg', trialChar.Subject, graphTitle));
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.fig', trialChar.Subject, graphTitle));
    
    
    clear figure
    
    
end
end


