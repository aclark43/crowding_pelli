function [ h, N, edges ] = errorbar_linehistogram( var, nBins, color, start )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% [N, edges, bin] = histcounts(var,nBins,'Normalization','probability');
% standDevs = [];
% for binIdx = 1:nBins
%     standDevs(binIdx) = std(var(bin == binIdx)/length(var)); %find where each VAR value is located, then normalize all the values
% %     sems(binIdx) = standDevs/sqrt(length(x));
% end
if exist('start')
    x = start;
else
    start = 0;
    x = (start:start+nBins);
end

% x = (start:start+nBins);
curve1 = mean(var)+ std(var)/sqrt(length(var));
curve2 = mean(var) - std(var)/sqrt(length(var));
x2 = [x, fliplr(x)];
inBetween = [curve1, fliplr(curve2)];
h(1) = fill(x2, inBetween, color);
hold on
plot(x, mean(var),'Color', 'k');


end

