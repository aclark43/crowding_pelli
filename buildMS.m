function MS = buildMS(pptrials,ii,eccentricity, strokeWidth, counter, params, jj)
%Builds the structure MS used for graph MS characteristics
%%% ii = idx of ms trials

    timeOn = round(pptrials{ii}.TimeTargetON);
    timeOff = round(min(pptrials{ii}.TimeTargetOFF, pptrials{ii}.ResponseTime));
    MS.(eccentricity).(strokeWidth)(jj).id = ii;
    
    microSaccadeStart = pptrials{ii}.microsaccades.start;
    microSaccadeEnd = pptrials{ii}.microsaccades.duration + microSaccadeStart - 1;
    xMS = pptrials{ii}.x.position(microSaccadeStart:microSaccadeEnd) + pptrials{ii}.xoffset * params.pixelAngle; % in arcmin
    yMS = pptrials{ii}.y.position(microSaccadeStart:microSaccadeEnd) + pptrials{ii}.yoffset * params.pixelAngle;
    
    angleMS = pptrials{ii}.microsaccades.angle;
    MS.(eccentricity).(strokeWidth)(jj).MSangle = angleMS;
    
    amplitudeMS = pptrials{ii}.microsaccades.amplitude;
    MS.(eccentricity).(strokeWidth)(jj).MSamplitude = amplitudeMS;
       
%%Use pptrials, figure out which ones occured during the correct time interval 
%Get isolated RATE(# between time on-OFF: each type to each SW), AMPLITUDE
%(assign to each MS that occurd: vector of amplitudes, rates. Then take
%averages) DO AVERAGES (of each sw) IN HERE. Also do STANDARD DEVIATION in
%here

%     empty_elems = arrayfun(@(s) all(structfun(@isempty,s)), tmpDrift.(strokeWidth));
%     tmpDrift.(strokeWidth)(empty_elems) = [];

%TODO: UNCOMMENT AND WORK ON
%     [~,~,MS.(eccentricity).(strokeWidth).msCoefDsq,~, MS.(eccentricity).(strokeWidth).dsq, MS.(eccentricity).(strokeWidth).singleSegmentDsq,~,~] = ...
%      CalculateDiffusionCoef(MS.(strokeWidth));

%%%START AND LANDING POSITION
MS.(eccentricity).(strokeWidth)(jj).startLocationMSx = xMS(1);
MS.(eccentricity).(strokeWidth)(jj).startLocationMSy = yMS(1);
MS.(eccentricity).(strokeWidth)(jj).endLocationMSx = xMS(end);
MS.(eccentricity).(strokeWidth)(jj).endLocationMSy = yMS(end);
end

