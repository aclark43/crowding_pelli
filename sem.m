function [ SEM ] = sem( var )
%Calculates the sem of the vector WILL REMOVE NANs
idx = ~isnan(var);
temp = size(var(idx));

if temp(1) <= 1
    SEM = std(var(idx))/sqrt(length(var(idx)));
else
    numSize = size(var) 
    for i = 1:numSize(2)
        SEM(i) = std(var(idx(:,i),i))/sqrt(length(var(idx(:,i),i)));
    end
end
end

