function [startReqdMS, endReqdMS] = getRequiredMicroSaccades(data, whichMicroSaccade)

% whichMicroSaccade = which MS you want to looked at (ie 1st, 2nd, 3rd,
% etc... within A TRIAL
%
%data = pptrials

[startMicroSacc,endMicroSacc] = deal([]);


for num = 1:size(data, 2)
        if ~isempty(data(num).startMS)
            if whichMicroSaccade > 1 && length(data(num).startMS) > 1 && length(data(num).endMS) > 1
                startMicroSacc(num) = data(num).startMS(whichMicroSaccade);
                endMicroSacc(num) = data(num).endMS(whichMicroSaccade);
            elseif whichMicroSaccade == 1
                startMicroSacc(num) = data(num).startMS(whichMicroSaccade);
                endMicroSacc(num) = data(num).endMS(whichMicroSaccade);
            else
                startMicroSacc(num) = 9;
                endMicroSacc(num) = 9;   
            end      
        else
            startMicroSacc(num) = 9;
            endMicroSacc(num) = 9;
        end        
end
startReqdMS = transpose(startMicroSacc);
endReqdMS = transpose(endMicroSacc);

end



