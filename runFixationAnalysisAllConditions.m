clear all
close all
clc

figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial figure with traces
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',1,... %Plots psychometric curves
    'QUICK_ANALYSIS',1,... %Only plots psychometric curves
    'CHECK_TRACES',0,... %will plot traces
    'HUX_RUN',0,... %will discard trials differently
    'COMPARE_SPANS',0,...%1 = small spans, 2 = large spans
    'MS_ANALYSIS', 0); %1 = small spans, 2 = large spans

%{'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};

% machine = {'A'}; %(D = DDPI, A = DPI)
% subjectsAll = {'Z024','Z064','Z046','Z084','Z014'};%Z070
% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z063'};

% subjectsAll = {'HUX1','HUX3','HUX5','HUX6','HUX7','HUX8',...
%     'HUX9','HUX11','HUX13','HUX14','HUX16'}; %patients
% subjectsAll = {'HUX10','HUX12','HUX18','HUX4','HUX17','HUX21','Martina'}; %cntrols

% subjectsAll = {'HUX1','HUX3','HUX5','HUX6','HUX7','HUX8',...
%     'HUX9','HUX11','HUX13','HUX14','HUX16',...%patients
%     'HUX10','HUX12','HUX18','HUX4','HUX17','HUX21','Martina'};

% subjectsAll = {'Z023'};

% trialLength = 175;
trialLength = 500;

conditionsAll = {'Uncrowded','Crowded'};
% conditionsAll = {'Uncrowded'};
eccAll = {'0ecc'};
emInclude = {'Drift'};
justGetXYTracesAll = 1;

session = {'ALL'}; %%ALL
for machineIdx = 1:2
    if machineIdx == 1
        machine = {'A'};
        subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z063'};
    elseif machineIdx == 2
        machine = {'D'};
        subjectsAll = {'Z024','Z064','Z046','Z084','Z014','Z091','Zoe'};
        %         subjectsAll = subjectsAll;
        %                 subjectsAll = {'Z014'};
    end
    for ii = 1:length(subjectsAll)
        for jj = 1:length(conditionsAll)
            for kk = 1:length(eccAll)
                
                subject = subjectsAll(ii);%'Z002','Z023','Z013','Z005','Z002','Z063'};%Z063
                condition = conditionsAll(jj); %Crowded or Uncrowded
                ecc = eccAll(kk);
                
                em = {'Drift_MS'}; %Drift,MS,Drift_MS
                stabParam = {'Unstabilized'};
                
                params = buildParams(machine, session, figures.COMPARE_SPANS, subject, condition);
                
                for emIdx = 1:length(em)
                    currentEm = em{emIdx};
                    for condIdx = 1:length(condition)
                        currentCond = condition{condIdx};
                        for subIdx = 1:length(subject)
                            currentSubj = subject{subIdx};
                            for stabIdx = 1:length(stabParam)
                                currentStab = stabParam{stabIdx};
                                for eccIdx = 1:length(ecc)
                                    currentEcc = ecc{eccIdx};
                                    
                                    params = determineParams(params, currentEm, currentCond);
                                    params.ecc = currentEcc;
                                    
                                    filepath = sprintf...
                                        ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/%s_%s_%s', ...
                                        currentSubj, currentCond, currentStab, currentEcc);
                                    
                                    title_str = sprintf('%s_%s_%s_%s_%s',currentSubj, currentCond, ...
                                        currentStab, currentEm, currentEcc);
                                    
                                    %                                 [traces, threshInfo, trialChar, em] = ...
                                    %                                     oneSubjectPelli(params, filepath, figures, stabParam, condition, title_str, currentSubj);
                                    
                                    if strcmp('D',params.machine)
                                        [ pptrials ] = reBuildPPtrials (filepath, params);
                                    else
                                        load(fullfile(filepath, 'pptrials.mat'), 'pptrials');
                                    end
                                    
                                    for xx = 1:length(pptrials)
                                        if isfield(pptrials{xx}, 'pixelAngle')
                                            params.pixelAngle = pptrials{1,xx}.pixelAngle;
                                            pptrials{1,xx}.pxAngle = pptrials{1,xx}.pixelAngle;
                                        else
                                            params.pixelAngle = pptrials{1,xx}.pxAngle;
                                        end
                                    end
                                    params.fixation = 1;
                                    pptrialsUnc = [];
                                    pptrialsCro = [];
                                    allTrials{ii,jj} = pptrials;
                                    clear pptrials;
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    if justGetXYTracesAll
        for ii = 1:length(subjectsAll)
            counter = 1;
            for c = 1:length(allTrials(ii,:))
                for i = 1:length(allTrials{ii,c})
                    if allTrials{ii,c}{i}.TimeFixationON > 0
                        fix.x.(subjectsAll{ii}){counter} = allTrials{ii,c}{i}.x.position;
                        counter = counter + 1;
                    end
                end
            end
        end
    end
    if strcmp('Drift',emInclude) && trialLength == 175
        load(fullfile('MATFiles', sprintf('KeepFixationTrials2_%s.mat',...
            string(trialLength))));
    end
    for i = 1:length(subjectsAll)
        counter = 1;
        subj = (subjectsAll{i});
        for ii = 1:length(allTrials{i,1}) %%Uncrowded
            pptrials = allTrials{i,1};
            if ceil(pptrials{ii}.TimeFixationON) > 0
                fixationOnlyTrialsIdx.(subj)(1,counter) = ii;
                counter = counter + 1;
            end
        end
        
        counter = 1;
        for ii = 1:length(allTrials{i,2}) %%Crowded
            pptrials = allTrials{i,2};
            if ceil(pptrials{ii}.TimeFixationON) > 0
                fixationOnlyTrialsIdx.(subj)(2,counter) = ii;
                counter = counter + 1;
            end
        end
    end
    %% MS Analysis
    %     if figures.MS_ANALYSIS
    %         for ii = 1:length(subjectsAll)
    %             subject = subjectsAll{ii};
    %             %     allTrialsBothCond = [allTrials{ii,1} allTrials{ii,2}];
    %             if strcmp('D',params.machine)
    %                 samplingRate = 330;
    %             else
    %                 samplingRate = 1000;
    %             end
    %             numTrials1 = numel(find(fixationOnlyTrialsIdx.(subject)(1,:) > 0));
    %             numTrials2 = numel(find(fixationOnlyTrialsIdx.(subject)(2,:) > 0));
    %
    %             pptrials1 = allTrials{ii,1};
    %             pptrials2 = allTrials{ii,2};
    %             %Updates the Amplitudes and Angles for microsaccades and saccades
    %             pptrials1 = updatePPTAmplitudeAngle(pptrials1);
    %             pptrials2 = updatePPTAmplitudeAngle(pptrials2);
    %
    %             % Removes the overshoots for microsaccades
    %             %     pptrials1 = osRemoverTrialDDPIFriendly(pptrials1, 'microsaccades', samplingRate);
    %             %     pptrials2 = osRemoverTrialDDPIFriendly(pptrials2, 'microsaccades', samplingRate);
    %
    %             %     alltrialsBothCondFixIdx = [fixationOnlyTrialsIdx.(subject)(1,1:numTrials1) fixationOnlyTrialsIdx.(subject)(2,1:numTrials1)];
    %             [sRates1, msRates1] = buildRates(allTrials{ii,1}(1,fixationOnlyTrialsIdx.(subject)(1,1:numTrials1)));
    %             [sRates2, msRates2] = buildRates(allTrials{ii,2}(1,fixationOnlyTrialsIdx.(subject)(2,1:numTrials2)));
    %             counter = 1;
    %             %     for i = 1:length(pptrials1)
    %             %         msAmps(ii,counter) ={pptrials1{i}.microsaccades.amplitude};
    %             %         msAngles(ii,counter) ={pptrials1{i}.microsaccades.angle};
    %             %         counter = counter + 1;
    %             %     end
    %             %     for i = 1:length(pptrials2)
    %             %         msAmps(ii,counter) ={pptrials2{i}.microsaccades.amplitude};
    %             %         msAngles(ii,counter) ={pptrials2{i}.microsaccades.angle};
    %             %         counter = counter + 1;
    %             %     end
    %
    %             sRates(ii) = mean([sRates1(sRates1 > 0) sRates2(sRates2 > 0)]);
    %             msRates(ii) = mean([msRates1(msRates1 > 0) msRates2(msRates2 > 0)]);
    %             allRates(ii) = mean([sRates1(sRates1 > 0) sRates2(sRates2 > 0) ...
    %                 msRates1(msRates1 > 0) msRates2(msRates2 > 0)]);
    %         end
    %     end
    
    %% Drift Analysis
    for i = 1:length(subjectsAll)
        subj = (subjectsAll{i});
        counter = 1;
        counterDrift = 1;
        for cond = 1:length(conditionsAll)
            if cond == 1
                condition = ('Uncrowded');
                row = 1;
            else
                condition = ('Crowded');
                row = 2;
            end
            for ii = fixationOnlyTrialsIdx.(subj)(row,:) %%Condition
                pptrials = allTrials{i,row};
                if ~ii == 0
                    timeOn = round(pptrials{ii}.TimeFixationON)+1;
                    if strcmp('D',params.machine)
                        conversionFactor = 1000/330;
                        timeOff = trialLength/conversionFactor;
                        pxAngle = pptrials{ii}.pixelAngle;
                    else
                        conversionFactor = 1;
                        timeOff = trialLength;
                        pxAngle = pptrials{ii}.pxAngle;
                    end
                    %                     timeOff = timeOff + timeOn;
                    if strcmp('Drift',emInclude)
                        if strcmp('Z091',subj)
                            if  ~strcmp('EIS2-20201004-093707-203.eis', pptrials{ii}.FileName)
                                traces.(subj).x{counter} = pptrials{ii}.x.position((ceil(timeOn/conversionFactor)):timeOff + timeOn) + pxAngle * pptrials{ii}.xoffset;
                                traces.(subj).y{counter} = pptrials{ii}.y.position((ceil(timeOn/conversionFactor)):timeOff + timeOn) + pxAngle * pptrials{ii}.yoffset;
                            end
                        else
                            if timeOn+timeOff < length(pptrials{ii}.x.position)
                                traces.(subj).x{counter} = pptrials{ii}.x.position((ceil(timeOn/conversionFactor)):timeOff + timeOn) + pxAngle * pptrials{ii}.xoffset;
                                traces.(subj).y{counter} = pptrials{ii}.y.position((ceil(timeOn/conversionFactor)):timeOff + timeOn) + pxAngle * pptrials{ii}.yoffset;
                            end
                        end
                    else
                        lengthofTraces.(subj) = length(pptrials{ii}.x.position(1:end));
                        traces.(subj).x{counter} = pptrials{ii}.x.position(ceil(timeOn/conversionFactor):trialLength/conversionFactor) + pxAngle * pptrials{ii}.xoffset;
                        traces.(subj).y{counter} = pptrials{ii}.y.position(ceil(timeOn/conversionFactor):trialLength/conversionFactor) + pxAngle * pptrials{ii}.yoffset;
                    end
                    if trialLength == 500
                        counter = counter + 1;
                        continue;
                    end
                    throwOut = 0;
                    start = struct(...
                        'ms', pptrials{ii}.microsaccades.start,...
                        's', pptrials{ii}.saccades.start,...
                        'blinks',pptrials{ii}.blinks.start,...
                        'notracks',pptrials{ii}.notracks.start);
                    
                    duration = struct(...
                        'ms', pptrials{ii}.microsaccades.duration,...
                        's', pptrials{ii}.saccades.duration,...
                        'blinks',pptrials{ii}.blinks.duration,...
                        'notracks',pptrials{ii}.notracks.duration);
                    
                    [start.drifts, s] = sort(pptrials{ii}.drifts.start);
                    duration.drifts = pptrials{ii}.drifts.duration(s);
                    if strcmp('Drift',emInclude)
                        if isempty(start.drifts)
                            continue;
                        else
                            prevDrift = 0;
                            for idx = 1:length(start.drifts)
                                if idx > 1 && start.drifts(idx) <= prevDrift || ...
                                        start.drifts(idx) == 0
                                    if duration.drifts(idx) == 0
                                        continue;
                                    end
                                    if sum(find(start.drifts(idx) == start.drifts)) > 1 %copies of same start time
                                        continue;
                                    end
                                    
                                    if sum(round(start.drifts(idx)/5)*5 == (round(start.drifts/5)*5)) > 1%copies of same end time
                                        continue;
                                    end
                                    start.drifts(idx) = 1;
                                end
                                prevDrift = start.drifts(idx);
                                segDuration = duration.drifts(idx);
                                if segDuration >= trialLength
                                    numSegs = floor(segDuration/trialLength);
                                else
                                    continue;
                                end
                                segmentNum = 0;
                                for segDriftIdx = 1:numSegs
                                    idxStart = ceil((start.drifts(idx)/conversionFactor))+segmentNum;%ceil((start.drifts(idx) + segmentNum));
                                    idxEnd =  floor(timeOff + (start.drifts(idx)/conversionFactor) + segmentNum);% floor((start.drifts(idx) + timeOff + segmentNum));
                                    if idxEnd < length(pptrials{ii}.x.position)
                                        
                                        traceX = pptrials{ii}.x.position(idxStart:idxEnd) + ...
                                            pxAngle * pptrials{ii}.xoffset;
                                        
                                        traceY = pptrials{ii}.y.position(idxStart:idxEnd) + ...
                                            pxAngle * pptrials{ii}.yoffset;
                                        
                                        if any(isnan(traceX)) || any(isnan(traceY))
                                            continue;
                                        end
                                        
                                        if mean(sqrt(traceX.^2 + traceY.^2)) > 50 %%%Gaze Off Center/Span too large (looking around)
                                            continue;
                                        end
                                        if sum(abs(traceX) > 30) > 0 || ...%Checks for LARGE OFFSET from recal
                                                sum(abs(traceY) > 30) > 0
                                            continue;
                                        end
                                        
                                        if strcmp('D',params.machine)
                                            sampling = 341;
                                        else
                                            sampling = 1000;
                                        end
                                        [~,~,~, ~, ~, ...
                                            singleSeg,~,~] = ...
                                            CalculateDiffusionCoef(sampling,struct...
                                            ('x',traceX, 'y', traceY));
%                                                                             if strcmp('Z064',subjectsAll{i})
%                                                                                 maxS = 120;
%                                                                                 maxL = 90;
%                                                                             elseif strcmp('Z014',subjectsAll{i})
%                                                                                 maxS = 30;
%                                                                                 maxL = 80;
%                                                                              elseif strcmp('Z084',subjectsAll{i})
%                                                                                 maxS = 20;
%                                                                                 maxL = 60;
%                                                                             elseif strcmp('Ashley',subjectsAll{i})
%                                                                                 maxS = 30;
%                                                                                 maxL = 45;
%                                                                             elseif strcmp('Z063',subjectsAll{i})
%                                                                                 maxS = 30;
%                                                                                 maxL = 80;
%                                                                             elseif strcmp('Z005',subjectsAll{i})
%                                                                                 maxS = 20;
%                                                                                 maxL = 45;
%                                                                             elseif strcmp('Z023',subjectsAll{i})
%                                                                                 maxS = 30;
%                                                                                 maxL = 80;
%                                                                             elseif strcmp('Z046',subjectsAll{i})
%                                                                                 maxS = 20;
%                                                                                 maxL = 50;
%                                                                             elseif strcmp('Z024',subjectsAll{i})
%                                                                                 maxS = 20;
%                                                                                 maxL = 80;
%                                                                             elseif strcmp('Z013',subjectsAll{i})
%                                                                                 maxS = 10;
%                                                                                 maxL = 55;
%                                                                             elseif strcmp('Z002',subjectsAll{i})
%                                                                                 maxS = 10;
%                                                                                 maxL = 45;
%                                                                             else
%                                                                                 maxS = 30;
%                                                                                 maxL = 90;
%                                                                             end
%                                         
%                                                                             if any(singleSeg(7/conversionFactor:20/conversionFactor) > maxS) ||...
%                                                                                     any(singleSeg(120/conversionFactor:200/conversionFactor) > maxL) ||...
%                                                                                     any(singleSeg > 250)
                                        %                                     figure;
                                        %                                     subplot(1,2,1)
                                        %                                     plot(singleSeg);
                                        %                                     ylim([0 100]);
                                        %                                     subplot(1,2,2)
                                        %                                     plot(traceX);
                                        %                                     hold on
                                        %                                     plot(traceY);
                                        %                                     ylim([-30 30]);
                                        %                                     suptitle(sprintf('%s',subjectsAll{i}))
                                        %
                                        %                                     keep = input('X to toss trial','s');
                                        %
                                        %                                     if ~isempty(keep)
                                        if ~isfield(saveIdx,subj)
                                            traces.(subj).xDrift{counterDrift} = traceX;
                                            traces.(subj).yDrift{counterDrift} = traceY;
                                            
                                            span.(subj).span(counterDrift) = quantile(sqrt((traceX-mean(traceX)).^2 + ...
                                                (traceY-mean(traceY)).^2), .95);
                                            
                                            counterDrift = counterDrift + 1;
                                            continue;
                                        end
                                        if saveIdx.(subj).(condition)(ii,segDriftIdx) == 0
                                            %                                         saveIdx.(subj).(condition)(ii,segDriftIdx) = 0;
                                            segmentNum = segmentNum + timeOff;
                                            %                                     close all;
                                            % %                                     figure;
                                            % %                                                         plot(1:length(traceX),traceX)
                                            % %                                                         ylim([-10 10])
                                            % %                                                         hold on
                                            % %                                                         plot(1:length(traceY),traceY)
                                            % %                                                         ylim([-10 10])
                                            % %
                                            % %                                     PLOT_DRIFTS = input('Keep Trace (y/n): ','s');
                                            % %                                     if PLOT_DRIFTS == 'n'
                                            %                                   if saveIdx.(subj).uncr(ii) == 0
                                            %                                          saveIdx.(subj).(condition)(ii,segDriftIdx) = 0;
                                            %                                          segmentNum = segmentNum + timeOff;
                                            %                                         continue;
                                            % %                                     elseif PLOT_DRIFTS == 'y'
                                            %                                   elseif saveIdx.(subj).uncr(ii) == 1
                                            %                                         saveIdx.(subj).(condition)(ii) = 0;
                                        else
                                            %                                         figure;
                                            %                                         plot(singleSeg);
                                            %                                         ylim([0 100]);
                                            %                                         title(sprintf('%s',subjectsAll{i}))
                                            
                                            %                                         saveIdx.(subj).(condition)(ii,segDriftIdx) = 1;
                                            if any(singleSeg(7/conversionFactor:50/conversionFactor) > 25) ||...
                                                    any(singleSeg > 200)
%                                             if any(singleSeg(7/conversionFactor:50/conversionFactor) > maxS) ||...
%                                                     any(singleSeg > maxL)
%                                                 segmentNum = segmentNum + timeOff;
                                            else
                                                traces.(subj).xDrift{counterDrift} = traceX;
                                                traces.(subj).yDrift{counterDrift} = traceY;
                                                
                                                span.(subj).span(counterDrift) = quantile(sqrt((traceX-mean(traceX)).^2 + ...
                                                    (traceY-mean(traceY)).^2), .95);
                                                
                                                counterDrift = counterDrift + 1;
                                                segmentNum = segmentNum + timeOff;
                                            end
                                        end
                                    end
                                end
                                
                            end
                        end
                    end
                end
                counter = counter + 1;
            end
        end
    end
    
    %     filename = sprintf('MATFiles/KeepFixationTrials2_%s.mat',string(trialLength));
    %     save(filename,'saveIdx')
    
    %% Plotting Heat Maps
    for i = 1:length(subjectsAll)
        subj = (subjectsAll{i});
        speedTemp = [];
        curveTemp = [];
        xValues = [];
        yValues = [];
        xDrift = [];
        yDrift = [];
        counter = 1;
        if isfield(traces.(subj), 'xDrift')
            for ii = 1:length(traces.(subj).xDrift) %%Drift
                x = traces.(subj).xDrift{ii};
                y = traces.(subj).yDrift{ii};
                
                spanTemp= quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95);
                if ~isnan(spanTemp)
                    spanT(counter) = spanTemp;
                    counter = counter + 1;
                end
                xDrift = [xDrift x];
                yDrift = [yDrift y];
            end
        end
        for ii = 1:length(traces.(subj).x) %%Drift & MS
            xTemp = traces.(subj).x{ii};
            yTemp = traces.(subj).y{ii};
            
            if ~isempty(xTemp)
                [~, instSpX, instSpY, speedTemp(ii), driftAngle, curveTemp(ii), varx, vary] = ...
                    getDriftChar(xTemp, yTemp, 41, 1, 180);
            else
                speedTemp(ii) = NaN;
                curveTemp(ii) = NaN;
            end
            xValues = [xValues xTemp];
            yValues = [yValues yTemp];
        end
        fix.mn_speed.(subj) = nanmean((speedTemp));
        fix.ind_speed.(subj) = speedTemp;
        fix.curvature.(subj) = nanmean((curveTemp));
        fix.ind_curvature.(subj) = curveTemp;
        
        [ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/1000)),31); %Checks strange artifacts
        [ xValues, yValues] = checkXYBound(xValues, yValues, 30); %Checks boundries
        
        if isfield(traces.(subj), 'xDrift')
            forDSQCalc.x = traces.(subj).xDrift;
            forDSQCalc.y = traces.(subj).yDrift;
            
            if strcmp('D',params.machine)
                sampling = 341;
            else
                sampling = 1000;
            end
            [~,~,dsq, ~, dsqVec, ...
                SingleSegmentDsq.(subj), dsqTime, Nfix, RegFit] = ...
                CalculateDiffusionCoef(sampling,struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
            
            
            [numTrials(i)] = length(traces.(subj).xDrift);
            
            fix.span.(subj) = mean((span.(subj).span));
            fix.ind_span.(subj) = (span.(subj).span);
            %             fix.amplitude.(subj) = sqrt(forDSQCalc.x^2 + forDSQCalc.y^2);
            fix.prlDrift.(subj) = mean(sqrt(xDrift.^2 + yDrift.^2));
            fix.numTrialsDrift.(subj) = numTrials(i);
            
            limit.xmin = floor(min(xDrift));
            limit.xmax = ceil(max(xDrift));
            limit.ymin = floor(min(yDrift));
            limit.ymax = ceil(max(yDrift));
            
            rangeXY = max(abs(cell2mat(struct2cell(limit))));
            areaDrift(i) = CalculateTheArea(xDrift, yDrift,0.68, rangeXY, 50);
            fix.areaDrift.(subj) = areaDrift(i);
            
            if numTrials(i) > 13%30
                %                 stats = bootstrp(10,@bootCalculateDiffusionCoef, struct('sampling', sampling, 'x',forDSQCalc.x, 'y', forDSQCalc.y));
                
                % Simulated process data
                % Process specifications
                %                 st = struct('sampling', sampling, 'x',forDSQCalc.x, 'y', forDSQCalc.y);
                %                 capable = @(z)(bootCalculateDiffusionCoef(z));  % Process capability
                %                 [stats.ci, stats.info] = bootci(10,capable,st);            % BCa confidence interval
                
                fix.dCoefDsq.(subj) = dsq;
                %                 fix.bootDSQ.(subj) = stats;
                fix.dCoefVec.(subj) = dsqVec;
                fix.dsqTime.(subj) = dsqTime;
                fix.Nfix.(subj) = Nfix;
                fix.regfit.(subj) = RegFit;
                fix.dsqSingleSegs.(subj) = SingleSegmentDsq.(subj);
            else
                fix.dCoefDsq.(subj) = NaN;
                fix.bootDSQ.(subj) = NaN;
                fix.dCoefVec.(subj) = NaN;
                fix.dsqTime.(subj) = NaN;
                fix.Nfix.(subj) = NaN;
                fix.regfit.(subj) = NaN;
                fix.dsqSingleSegs.(subj) = SingleSegmentDsq.(subj);
            end
        else
            limit.xmin = floor(min(xValues));
            limit.xmax = ceil(max(xValues));
            limit.ymin = floor(min(yValues));
            limit.ymax = ceil(max(yValues));
            
            rangeXY = max(abs(cell2mat(struct2cell(limit))));
            areaDrift(i) = CalculateTheArea(xValues, yValues,0.68, rangeXY, 50);
            fix.areaDrift.(subj) = areaDrift(i);
        end
        
        
        %         else
        %             fix.dCoefDsq.(subj) = NaN;
        %             fix.dCoefVec.(subj) = NaN;
        %             fix.dsqTime.(subj) = NaN;
        %             fix.dsqSingleSegs.(subj) = NaN;
        %         end
        %         figure;
        %         for s = 1:size(SingleSegmentDsq.(subj))
        %             plot(SingleSegmentDsq.(subj)(s,:))
        %             hold on
        %         end
        
        %         meanSpan(i) = mean(spanT);
        %%
        fix.x.(subj) = xValues;
        fix.y.(subj) = yValues;
        figure;
        generateHeatMapSimple( xValues, yValues, ...
            'Bins', 30,...
            'StimulusSize', 0,...
            'AxisValue', 30,...
            'Uncrowded', 4,...
            'Borders', 1);
        title(subjectsAll{i});
        % %         n_bins = 50;
        % %         axisValue = 30;
        % %
        % %         limit.xmin = floor(min(xValues));
        % %         limit.xmax = ceil(max(xValues));
        % %         limit.ymin = floor(min(yValues));
        % %         limit.ymax = ceil(max(yValues));
        % %
        %         rangeXY = max(abs(cell2mat(struct2cell(limit))));
        %         area(i) = CalculateTheArea(xValues, yValues,0.68, rangeXY, n_bins);
        %         fix.area.(subj) = area(i);
        % %
        % %         result = MyHistogram2(xValues, yValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
        % %         result = result./(max(max(result)));
        % %
        % %         load('./MyColormaps.mat');
        % %         set(gcf, 'Colormap', mycmap)
        % %         hold on
        % %         result(result==0)=NaN;
        % %         pcolor(linspace(limit.xmin, limit.xmax, size(result, 1)),...
        % %             linspace(limit.ymin, limit.ymax, size(result, 1)),...
        % %             result');
        % %         p = plot(0,0,'--ks','MarkerSize',16);
        % %         p(1).LineWidth = 3;
        % %
        % %         set(gca, 'FontSize', 12)
        % %         xlabel('X [arcmin]')
        % %         ylabel('Y [arcmin]')
        % %         set(gca,'XColor', 'none','YColor','none')
        % %         axis tight
        % %         axis square
        % %         caxis([0 ceil(max(max(result)))])
        % %         shading interp;
        % %         %         colorbar
        % %         axis([-axisValue axisValue -axisValue axisValue]);
        % %         hold on
        % %         plot([-limit.xmin limit.xmax], [0 0], '--k')
        % %         plot([0 0], [-limit.ymin limit.ymax], '--k')
        %         title(sprintf('%s Fixation',subj))
        
        %         text(-20, -20,sprintf('n = %i', numTrials));
        
        %         saveas(gcf, sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/HeatMaps/%s%iFixationMap.epsc', subj, trialLength));
        %         saveas(gcf, sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/HeatMaps/%s%iFixationMap.png', subj,trialLength));
        %         saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/HeatMaps/HeatMap%s%iFixationMap.png', subj,trialLength));
        
        
        
        %         saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/ALL/DriftOnlyFixationMarkers/%s_%i%sFixationMap.png', subj, trialLength, emInclude{1}));
        %         saveas(gcf, sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/HeatMaps/%s%iFixationMap.png', subj,trialLength));
        %         saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/HeatMaps/HeatMap%s%iFixationMap.png', subj,trialLength));
        % saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/%s.epsc', filepath, trialChar.Subject, sprintf('%s_Drift_Anaylsis',trialChar.Subject)));
        
    end
end
% filename = sprintf('MATFiles/%s_FixationResultsHuxlin.mat',string(trialLength)); %%running 8/11 9:29PM
% save(filename,'fix')
filename = sprintf('MATFiles/Fixation_9212021%s.mat',string(trialLength)); %%running 8/11 9:29PM
save(filename,'fix')


