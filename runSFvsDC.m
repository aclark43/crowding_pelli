clc
clear all

dataAll{1} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A031\data.mat')
%dataAll{2} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A039\data.mat')
dataAll{2} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A040\data.mat')
dataAll{3} = load('C:\Users\Ruccilab\Box\APLab-Projects\Drift\DriftControl_6-20\Data\A084\data.mat')

counter = 1;
for subIdx = 1:(length(dataAll))
    fixationTrials = find(dataAll{subIdx}.data.TimeFixationON > 0);
    temp = [dataAll{subIdx}.fix.trialid];
    fixPos = [];
    for i = 1:length(temp)
        if find(temp(i) == fixationTrials)
            if length(dataAll{subIdx}.fix(i).x) > 200
                fixPos.x{counter} = dataAll{subIdx}.fix(i).x;
                fixPos.y{counter} = dataAll{subIdx}.fix(i).y;
                counter = counter + 1;
            end
        end
    end
  
    forDSQCalcShort.x = fixPos.x;
    forDSQCalcShort.y = fixPos.y;
    
    [~,Bias,dCoefDsq(subIdx), ~,Dsq, ...
        SingleSegmentDsq,~,~] = ...
        CalculateDiffusionCoef(1000, struct('x',forDSQCalcShort.x, 'y', forDSQCalcShort.y));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % %%% Determine Best Contrasts
% % % contrasts20{1} = [31.6200, 32.36];
% % % contrasts20{2} =  [21.34 17.78]; 
% % % contrasts20{3} = [37.49 43.3];
% % % contrasts6{1} = [1.5800 1.78];
% % % contrasts6{2} =  [1.20 1.0]; 
% % % contrasts6{3} = [1.58 1.19];
% % % % contrasts6 = [1.5800; 1.20; 1.58];
% % % 
% % % idx = find(dataAll{subIdx}.data.spatialFreq == 20);
% % % temp = (dataAll{subIdx}.data.contrast(idx))
% % % unique(temp)
% % % numTrials20(subIdx) = sum(dataAll{subIdx}.data.Correct(find...
% % %     (dataAll{subIdx}.data.spatialFreq == 20 & ...
% % %     dataAll{subIdx}.data.contrast == contrasts20{subIdx}(1) |...
% % %     dataAll{subIdx}.data.contrast == contrasts20{subIdx}(2) &...
% % %     dataAll{subIdx}.data.Correct ~= 3)))
% % % perf = mean(dataAll{subIdx}.data.Correct(find...
% % %     (dataAll{subIdx}.data.spatialFreq == 20 & ...
% % %     dataAll{subIdx}.data.contrast == contrasts20{subIdx}(1) |...
% % %     dataAll{subIdx}.data.contrast == contrasts20{subIdx}(2) &...
% % %     dataAll{subIdx}.data.Correct ~= 3)))
% % % 
% % % idx = find(dataAll{subIdx}.data.spatialFreq == 6);
% % % temp = (dataAll{subIdx}.data.contrast(idx))
% % % unique(temp)
% % % numTrials6(subIdx) = sum([sum(dataAll{subIdx}.data.Correct(find...
% % %     (dataAll{subIdx}.data.spatialFreq == 6 & ...
% % %      dataAll{subIdx}.data.contrast == contrasts6{subIdx}(1)&...
% % %     dataAll{subIdx}.data.Correct ~= 3))) ...
% % %     sum(dataAll{subIdx}.data.Correct(find...
% % %     (dataAll{subIdx}.data.spatialFreq == 6 & ...
% % %      dataAll{subIdx}.data.contrast == contrasts6{subIdx}(2)&...
% % %     dataAll{subIdx}.data.Correct ~= 3)))])
% % % 
% % % perf = mean([mean(dataAll{subIdx}.data.Correct(find...
% % %     (dataAll{subIdx}.data.spatialFreq == 6 & ...
% % %      dataAll{subIdx}.data.contrast == contrasts6{subIdx}(1)&...
% % %     dataAll{subIdx}.data.Correct ~= 3))) ...
% % %     mean(dataAll{subIdx}.data.Correct(find...
% % %     (dataAll{subIdx}.data.spatialFreq == 6 & ...
% % %      dataAll{subIdx}.data.contrast == contrasts6{subIdx}(2)&...
% % %     dataAll{subIdx}.data.Correct ~= 3)))])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% contrasts20 = [31.6200 17.78 34.89];
% contrasts6 = [1.5800 1.54 1.58];
% contrasts20 = [31.6200 21.34 37.49];
% contrasts6 = [1.5800 1.20 1.58];
contrasts20{1} = [31.6200, 32.36];
contrasts20{2} =  [21.34 17.78]; 
contrasts20{3} = [37.49 43.3];
contrasts6{1} = [1.5800 1.78];
contrasts6{2} =  [1.20 1.0]; 
contrasts6{3} = [1.58 1.19];


trials20{subIdx}.idx = ((find...
    (dataAll{subIdx}.data.spatialFreq == 20 & ...
    (dataAll{subIdx}.data.contrast == contrasts20{subIdx}(1)|...
    dataAll{subIdx}.data.contrast == contrasts20{subIdx}(2))&...
    dataAll{subIdx}.data.Correct ~= 3)));

trials20{subIdx}.idxContrasts{1} = ((find...
    (dataAll{subIdx}.data.spatialFreq == 20 & ...
     (dataAll{subIdx}.data.contrast == contrasts20{subIdx}(1))&...
    dataAll{subIdx}.data.Correct ~= 3)));
trials20{subIdx}.idxContrasts{2} = ((find...
    (dataAll{subIdx}.data.spatialFreq == 20 & ...
     (dataAll{subIdx}.data.contrast == contrasts20{subIdx}(2))&...
    dataAll{subIdx}.data.Correct ~= 3)));

trials20{subIdx}.performance = dataAll{subIdx}.data.Correct(trials20{subIdx}.idx);

ids = [dataAll{subIdx}.fix.trialid];
% counter = 1;
for c = 1:length(trials20{subIdx}.idxContrasts)
    counter = 1;
    for i = 1:length(ids)
        if find(ids(i) == trials20{subIdx}.idxContrasts{c})
            if length(dataAll{subIdx}.fix(i).x) > 200
                trials20{subIdx}.x{counter} = dataAll{subIdx}.fix(i).x;
                trials20{subIdx}.y{counter} = dataAll{subIdx}.fix(i).y;
                [~,~,trials20{subIdx}.dcTrial{c,counter}] =...
                    CalculateDiffusionCoef(1000, struct('x',trials20{subIdx}.x{counter},...
                    'y', trials20{subIdx}.y{counter}));
                trials20{subIdx}.perfSelect{c,counter} = dataAll{subIdx}.data.Correct(...
                    dataAll{subIdx}.fix(i).trialid);
                counter = counter + 1;
            end
        end
    end
end
forDSQCalc20.x = trials20{subIdx}.x;
forDSQCalc20.y = trials20{subIdx}.y;

[~,Bias,trials20{subIdx}.dCoefDsq, ~,Dsq, ...
    SingleSegmentDsq,~,~] = ...
    CalculateDiffusionCoef(1000, struct('x',forDSQCalc20.x, 'y', forDSQCalc20.y));

trials6{subIdx}.idx = ((find...
    (dataAll{subIdx}.data.spatialFreq == 6 & ...
     (dataAll{subIdx}.data.contrast == contrasts6{subIdx}(1)|...
    dataAll{subIdx}.data.contrast == contrasts6{subIdx}(2))&...
    dataAll{subIdx}.data.Correct ~= 3)));

trials6{subIdx}.idxContrasts{1} = ((find...
    (dataAll{subIdx}.data.spatialFreq == 6 & ...
     (dataAll{subIdx}.data.contrast == contrasts6{subIdx}(1))&...
    dataAll{subIdx}.data.Correct ~= 3)));
trials6{subIdx}.idxContrasts{2} = ((find...
    (dataAll{subIdx}.data.spatialFreq == 6 & ...
     (dataAll{subIdx}.data.contrast == contrasts6{subIdx}(2))&...
    dataAll{subIdx}.data.Correct ~= 3)));

trials6{subIdx}.performance = dataAll{subIdx}.data.Correct(trials6{subIdx}.idx);

ids = [dataAll{subIdx}.fix.trialid];


for c = 1:length(trials6{subIdx}.idxContrasts)
    counter = 1;
    for i = 1:length(ids)
        if find(ids(i) == trials6{subIdx}.idxContrasts{c})
            if length(dataAll{subIdx}.fix(i).x) > 200
                trials6{subIdx}.x{counter} = dataAll{subIdx}.fix(i).x;
                trials6{subIdx}.y{counter} = dataAll{subIdx}.fix(i).y;
                [~,~,trials6{subIdx}.dcTrial{c,counter}] =...
                    CalculateDiffusionCoef(1000, struct('x',trials6{subIdx}.x{counter},...
                    'y', trials6{subIdx}.y{counter}));
                trials6{subIdx}.perfSelect{c,counter} = dataAll{subIdx}.data.Correct(...
                    dataAll{subIdx}.fix(i).trialid);
                counter = counter + 1;
            end
        end
    end
end

forDSQCalc6.x = trials6{subIdx}.x;
forDSQCalc6.y = trials6{subIdx}.y;

[~,Bias,trials6{subIdx}.dCoefDsq, ~,Dsq, ...
    SingleSegmentDsq,~,~] = ...
    CalculateDiffusionCoef(1000, struct('x',forDSQCalc6.x, 'y', forDSQCalc6.y));
end

for i = 1:3
    allDC6(i) = trials6{i}.dCoefDsq;
    allDC20(i) = trials20{i}.dCoefDsq;
end
figure;
critFreq20 = powerAnalysis_JustCriticalFrequency(allDC20, 0);
hold on
critFreq6 = powerAnalysis_JustCriticalFrequency(allDC6, 0);
hold on
critFreq6 = powerAnalysis_JustCriticalFrequency(dCoefDsq(i), 0);

dcThresh20S = [6 12 15];
dcThresh20L = [8 12 15];
dcThresh6S = [6 12 4];
dcThresh6L = [8 12 4];

colr = parula(3);
for c = 1
    for i = [1 3]
        idxSmall = [];
        idxSmall = find(cell2mat(trials20{i}.dcTrial(c,:)) < dcThresh20S(i));
        sf20.meanDCSmall(i) = mean(cell2mat(trials20{i}.dcTrial(c,idxSmall)));
        sf20.meanPerfSmall(i) = mean(cell2mat(trials20{i}.perfSelect(c,idxSmall)));
        sf20.numPerfSmall(i) = length(cell2mat(trials20{i}.perfSelect(c,idxSmall)));
        
        idxLarge = [];
        idxLarge = find(cell2mat(trials20{i}.dcTrial(c,:)) > dcThresh20L(i));
        sf20.meanDCLarge(i) = mean(cell2mat(trials20{i}.dcTrial(c,idxLarge)));
        sf20.meanPerfLarge(i) = mean(cell2mat(trials20{i}.perfSelect(c,idxLarge)));
        sf20.numPerfLarge(i) = length(cell2mat(trials20{i}.perfSelect(c,idxLarge)));
        
        idxSmall = [];
        idxSmall = find(cell2mat(trials6{i}.dcTrial(c,:)) < dcThresh6S(i));
        sf6.meanDCSmall(i) = mean(cell2mat(trials6{i}.dcTrial(c,idxSmall)));
        sf6.meanPerfSmall(i) = mean(cell2mat(trials6{i}.perfSelect(c,idxSmall)));
        sf6.numPerfSmall(i) = length(cell2mat(trials6{i}.perfSelect(c,idxSmall)));
        
        idxLarge = [];
        idxLarge = find(cell2mat(trials6{i}.dcTrial(c,:)) > dcThresh6L(i) & ...
            cell2mat(trials6{i}.dcTrial(c,:)) < 60);
        sf6.meanDCLarge(i) = mean(cell2mat(trials6{i}.dcTrial(c,idxLarge)));
        sf6.meanPerfLarge(i) = mean(cell2mat(trials6{i}.perfSelect(c,idxLarge)));
        sf6.numPerfLarge(i) = length(cell2mat(trials6{i}.perfSelect(c,idxLarge)));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        c = 2
        idxSmall = [];
        idxSmall = find(cell2mat(trials20{i}.dcTrial(c,:)) < dcThresh20S(i));
        sf20.meanDCSmallC(i) = mean(cell2mat(trials20{i}.dcTrial(c,idxSmall)));
        sf20.meanPerfSmallC(i) = mean(cell2mat(trials20{i}.perfSelect(c,idxSmall)));
        sf20.numPerfSmallC(i) = length(cell2mat(trials20{i}.perfSelect(c,idxSmall)));
        
        idxLarge = [];
        idxLarge = find(cell2mat(trials20{i}.dcTrial(c,:)) > dcThresh20L(i));
        sf20.meanDCLargeC(i) = mean(cell2mat(trials20{i}.dcTrial(c,idxLarge)));
        sf20.meanPerfLargeC(i) = mean(cell2mat(trials20{i}.perfSelect(c,idxLarge)));
        sf20.numPerfLargeC(i) = length(cell2mat(trials20{i}.perfSelect(c,idxLarge)));
        
        idxSmall = [];
        idxSmall = find(cell2mat(trials6{i}.dcTrial(c,:)) < dcThresh6S(i));
        sf6.meanDCSmallC(i) = mean(cell2mat(trials6{i}.dcTrial(c,idxSmall)));
        sf6.meanPerfSmallC(i) = mean(cell2mat(trials6{i}.perfSelect(c,idxSmall)));
        sf6.numPerfSmallC(i) = length(cell2mat(trials6{i}.perfSelect(c,idxSmall)));
        
        idxLarge = [];
        idxLarge = find(cell2mat(trials6{i}.dcTrial(c,:)) > dcThresh6L(i) & ...
            cell2mat(trials6{i}.dcTrial(c,:)) < 60);
        sf6.meanDCLargeC(i) = mean(cell2mat(trials6{i}.dcTrial(c,idxLarge)));
        sf6.meanPerfLargeC(i) = mean(cell2mat(trials6{i}.perfSelect(c,idxLarge)));
        sf6.numPerfLargeC(i) = length(cell2mat(trials6{i}.perfSelect(c,idxLarge)));
        
    end
end
figure;
subplot(2,2,[1 2])
for i = [1  3]
    l(1) = plot([sf20.meanDCSmall(i) sf20.meanDCLarge(i)],...
        [sf20.meanPerfSmall(i) sf20.meanPerfLarge(i)],...
        '-o','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
    text([sf20.meanDCSmall(i) sf20.meanDCLarge(i)],...
        [sf20.meanPerfSmall(i) sf20.meanPerfLarge(i)],...
        {string(sf20.numPerfSmall(i)), string(sf20.numPerfLarge(i))});
    
    l(1) = plot([sf20.meanDCSmallC(i) sf20.meanDCLargeC(i)],...
        [sf20.meanPerfSmallC(i) sf20.meanPerfLargeC(i)],...
        '-o','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
    text([sf20.meanDCSmallC(i) sf20.meanDCLargeC(i)],...
        [sf20.meanPerfSmallC(i) sf20.meanPerfLargeC(i)],...
        {string(sf20.numPerfSmallC(i)), string(sf20.numPerfLargeC(i))});
    
    hold on
%     l(1) = plot(sf20.meanDCLarge(i),sf20.meanPerfLarge(i),...
%         'o','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
%     hold on
    l(2) = plot([sf6.meanDCSmall(i) sf6.meanDCLarge(i)],...
        [sf6.meanPerfSmall(i) sf6.meanPerfLarge(i)],...
        '-d','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
    text([sf6.meanDCSmall(i) sf6.meanDCLarge(i)],...
        [sf6.meanPerfSmall(i) sf6.meanPerfLarge(i)],...
        {string(sf6.numPerfSmall(i)), string(sf6.numPerfLarge(i))});
    
    l(2) = plot([sf6.meanDCSmallC(i) sf6.meanDCLargeC(i)],...
        [sf6.meanPerfSmallC(i) sf6.meanPerfLargeC(i)],...
        '-d','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
    text([sf6.meanDCSmallC(i) sf6.meanDCLargeC(i)],...
        [sf6.meanPerfSmallC(i) sf6.meanPerfLargeC(i)],...
        {string(sf6.numPerfSmallC(i)), string(sf6.numPerfLargeC(i))});
    
    
    l(3) = plot(dCoefDsq(i),0.4,...
        's','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
%     hold on
%     l(2) = plot(sf6.meanDCLarge(i),sf6.meanPerfLarge(i),...
%         'd','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
    hold on
end
legend(l,{'20 cpd','6cpd','Fix'})
ylabel('Prop Correct')
xlabel('DC')

% figure;
subplot(2,2,3)
for i = [1 3]
    plot([1 2],...
        [sf20.meanPerfSmall(i) sf20.meanPerfLarge(i)],...
        '-o','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
    forTest1(i) = sf20.meanPerfSmall(i);
    forTest2(i) = sf20.meanPerfLarge(i);
    hold on
end
% [h,p] = ttest2(forTest1, forTest2)
xticks([1 2])
xticklabels({'Small DC','Large DC'})
title('20cpd')   
ylabel('Prop Correct')

subplot(2,2,4)
for i = [1 3]
    plot([1 2],...
        [sf6.meanPerfSmall(i) sf6.meanPerfLarge(i)],...
        '-o','Color',colr(i,:),'MarkerFaceColor',colr(i,:));
    forTest1(i) = sf6.meanPerfSmall(i);
    forTest2(i) = sf6.meanPerfLarge(i);
    hold on
end
% [h,p] = ttest2(forTest1, forTest2)
xticks([1 2])
xticklabels({'Small DC','Large DC'})
title('6cpd') 
ylabel('Prop Correct')










% trialIdx = 0;
% for ii = 1:length(dataAll{1, 1}.pptrials)
%     if dataAll{1, 1}.pptrials{ii}.Correct == 3
%         continue
%     end
% x = dataAll{1, 1}.pptrials{ii}.x.position;
% y = dataAll{1, 1}.pptrials{ii}.y.position;
% 
% x = dataAll{1, 1}.pptrials{ii}.x.position + pptrials{ii}.xoffset * params.pixelAngle(ii);
% y = dataAll{1, 1}.pptrials{ii}.y.position + pptrials{ii}.yoffset * params.pixelAngle(ii);
% 
% end
% mode(dataAll{1, 1}.data.contrast(find(dataAll{1, 1}.data.spatialFreq == 6)))

dcAll = [dataAll{1, 1}.data.diffConstant.dsq];


