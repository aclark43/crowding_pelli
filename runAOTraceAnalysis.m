clear all
close all
clc

subjectsAll = {'A144','A146','A197','Z055','Z151','Z190','Z192'};
for subNum = 1:length(subjectsAll)
    allVels = [];
    temp = dir('X:\Adaptive_Optics\psychophysicsData\MiniScotoma\eyetraceData');
    ind = contains(string({temp.name}),subjectsAll{subNum});
    
    load(sprintf('X:/Adaptive_Optics/psychophysicsData/MiniScotoma/eyetraceData/%s',temp(find(ind)).name))
    
    % trial = 1;
    % figure;
    % plot(eyetraceData(trial).t,eyetraceData(trial).x,'-o')
    % hold on
    % plot(eyetraceData(trial).t,eyetraceData(trial).y,'-o')
    %
    %
    % trial = 7;
    % figure;
    % plot(eyetraceData(trial).t,eyetraceData(trial).x,'-o')
    % hold on
    % plot(eyetraceData(trial).t,eyetraceData(trial).y,'-o')
    %
    % trial = 7;
    % figure;
    % plot(eyetraceData(trial).x, eyetraceData(trial).y,'*')
    % set(gca, 'YDir','reverse')
    % xlabel('Motion on the Retina')
    % axis square
    % text(10,0,'N');
    % text(0,15,'I');
    % hold on
    % forleg(1) = plot(0,0,'o','Color','k','MarkerFaceColor','k');
    % legend(forleg,{'PRL'});
    % axis([-15 15 -15 15]);
    
    for j = 1:length(eyetraceData)
        if strcmp(eyetraceData(j).type,'task')
            eyetraceData(j).taskIdx = 1;
        elseif strcmp(eyetraceData(j).type,'fixation')
            eyetraceData(j).taskIdx = 2;
        end
    end
    
    figure;
    % for t = 1:length(eyetraceData)
    counterFigure = 1;
    for s = 1:2
        for f =1:2
            velX = [];
            velY = [];
            trial = find([eyetraceData.scotomaCondition] == s-1 & ...
                [eyetraceData.taskIdx] == f);
            counter = 1;
            counterMS = 1;
            for t = trial
                allIs = 1:500:length(eyetraceData(t).x);
                for ii = 1:length(allIs)-1
                    x = eyetraceData(t).x(allIs(ii):allIs(ii+1));
                    y = eyetraceData(t).y(allIs(ii):allIs(ii+1));
                    
                    [newX,newY,...
                        ms,drift] = ...
                        cleanAODataForMS(x,y);
                    data{subNum,counterFigure}.x{counter} = newX;
                    data{subNum,counterFigure}.y{counter} = newY;
                    
                    
                    if ~isempty(drift.Start)
                        for d = 1:length(drift.Start)
                            if isnan(drift.Duration(d))
                                continue;
                            end
                            [~, data{subNum,counterFigure}.instSpX{counter},...
                                data{subNum,counterFigure}.instSpY{counter},...
                                data{subNum,counterFigure}.mn_speed{counter},...
                                data{subNum,counterFigure}.driftAngle{counter},...
                                data{subNum,counterFigure}.curvature(counter),...
                                data{subNum,counterFigure}.varx{counter},...
                                data{subNum,counterFigure}.vary{counter},...
                                data{subNum,counterFigure}.bcea(counter),...
                                data{subNum,counterFigure}.span(counter), ...
                                data{subNum,counterFigure}.amplitude(counter), ...
                                data{subNum,counterFigure}.prlDistance(counter),...
                                data{subNum,counterFigure}.prlDistanceX(counter), ...
                                data{subNum,counterFigure}.prlDistanceY(counter)] = ...
                                getDriftChar(newX(drift.Start(d):drift.Start(d)+drift.Duration(d))',...
                                newY(drift.Start(d):drift.Start(d)+drift.Duration(d))', 41, 1, 250,480);
                            velX = [velX data{subNum,counterFigure}.instSpX{counter}];
                            velY = [velY data{subNum,counterFigure}.instSpY{counter}];
                            forDsq(counter).x = x;
                            forDsq(counter).y = y;
                            counter = counter + 1;
                        end
                    end
                    if ~isempty(ms.Start)
                        for m = 1:length(ms.Start)-1
                            if isnan(ms.Duration(m)) || isnan(ms.Start(m)) ||...
                                    isnan(ms.Start(m+1))
                                continue;
                            end
                            xtemp = newX(ms.Start(m)) - newX(ms.Start(m+1));
                            ytemp = newY(ms.Start(m)) - newY(ms.Start(m+1));
                            dataMS{subNum,counterFigure}.msAmp(counterMS) = eucDist(xtemp,ytemp);
                            dataMS{subNum,counterFigure}.msAngle(counterMS) = ...
                                atan2d([newX(ms.Start(m)-1)-newX(ms.Start(m))],...
                                [newY(ms.Start(m)-1)-newY(ms.Start(m))]);
                            counterMS = counterMS + 1;
                        end
                    end
                end
            end
            %         figure;
            counterD = 1;
            for d = 1:length(forDsq)
                if any(isnan(forDsq(d).x))
                    isnanChunks = find(diff((isnan(forDsq(d).x))));
                    for dd = 1:length(isnanChunks)-1
                        if dd == 1
                            if any(isnan(forDsq(d).x(1:isnanChunks(dd))')) || ...
                                    any(isnan(forDsq(d).y(1:isnanChunks(dd))'))
                                continue;
                            end
                            forDsqDriftOnly(counterD).x = forDsq(d).x(1:isnanChunks(dd))';
                            forDsqDriftOnly(counterD).y = forDsq(d).y(1:isnanChunks(dd))';
                            
                        else
                            if length(isnanChunks(dd)+1:isnanChunks(dd+1)) < 80
                                continue
                            end
                            if isnan(forDsq(d).x(isnanChunks(dd)))
                                forDsqDriftOnly(counterD).x = forDsq(d).x(isnanChunks(dd)+1:...
                                    isnanChunks(dd+1))';
                                forDsqDriftOnly(counterD).y = forDsq(d).y(isnanChunks(dd)+1:...
                                    isnanChunks(dd+1))';
                            else
                                continue;
                            end
                        end
                        counterD = counterD + 1;
                    end
                end
            end
            [~,~,data{subNum,counterFigure}.dc,~,~,~,~,~] = ...
                CalculateDiffusionCoef(1000,(forDsqDriftOnly));
            
            hold on
            params = {data{subNum,counterFigure}.bcea,data{subNum,counterFigure}.span,...
                data{subNum,counterFigure}.amplitude,data{subNum,counterFigure}.curvature};
            paramsNames = {'bcea','span','amplitude','curvature'};
            for i = 1:length(params)
                varNow = params{i};
                subplot(2,2,i)
                hold on
                y = 0:max(varNow);
                mu = nanmean(varNow);
                sigma = nanstd(varNow);
                f = exp(-(y-mu).^2./(2*sigma^2))./(sigma*sqrt(2*pi));
                axLeg(counterFigure) = plot(y,f,'LineWidth',1.5);
                
                %             histogram((varNow),50)
                %             ylabel('Count')
                ylabel('Probibility');
                xlabel(sprintf('%s',paramsNames{i}))
            end
            %         suptitle(sprintf('%s,Scot = %i',...
            %             eyetraceData(trial(1)).type,eyetraceData(trial(1)).scotomaCondition))
            legendText{counterFigure} = sprintf('%s,Scot = %i',...
                eyetraceData(trial(1)).type,eyetraceData(trial(1)).scotomaCondition);
            counterFigure = counterFigure + 1;
            allVels{subNum,counterFigure}.x = velX;
            allVels{subNum,counterFigure}.y = velY;

        end
    end
    suptitle(sprintf('%s',subjectsAll{subNum}))
end
legend(axLeg,legendText)
% end
%%
counter = 1;
figure;
for s = 1:subNum
    for c = 1:4 %conditions
        plot(c,data{s,c}.dc,'o');
        
        dcAll(s,c) = data{s,c}.dc;
        
        hold on
    end
end

errorbar([1 2 3 4],mean(dcAll),sem(dcAll),'-o','Color','k');
xlim([0 5])
xticks([1:4])
xticklabels(legendText)
ylabel('DC')
%%

allVels = [];
counter = 1;
velX = [];
velY = [];
for s = 1:subNum
    for c = 1:4 %conditions
        velX = [];
        velY = [];
        for v = 1:length(data{s,c}.instSpX)
            velX = [velX data{s,c}.instSpX{v}];
            velY = [velY data{s,c}.instSpY{v}];
        end
        allVels{s,c}.x = velX;
        allVels{s,c}.y = velY;
    end
end

% figure;
% y = [0 0 0];
% x = [0 50 100];
% ndhist(x, y, 'bins', 2, 'radial','axis',[-100 100 -100 100],'nr','filt');
% figure;plot(x,y,'-')
%         angles = rad2deg(circ_mean(atan2(y,x)));

%% Start Here for Creating AO Presentation Figures

for s = 1:subNum
    figure;
    for i = 1:4
        subplot(2,2,i)
        idx = find(allVels{s, i}.x < 100 & allVels{s, i}.y < 100);
        heatmpaOutPut{s}{i} = ndhist(allVels{s, i}.x(idx), allVels{s, i}.y(idx), 'bins', 2, 'radial','axis',[-200 200 -200 200],'nr','filt');
        angles = atan2(allVels{s, i}.y(idx),allVels{s, i}.x(idx));
        avAngle(s,i) = rad2deg(circ_mean(angles'));
        avConcentration(s,i) = rad2deg(circ_var(angles'));
        avSkew(s,i) = rad2deg(circ_skewness(angles'));

        temp = cell2mat(data{s,i}.mn_speed);
        
        temp2 = temp(~isnan(temp) & temp > 1);
        avSpeed(s,i) = prctile(temp,68);
        mdSpeed(s,i) = median(temp2);
        axis([-60 60 -60 60])
        load('./MyColormaps.mat')
        set(gcf, 'Colormap', mycmap)
        shading interp
        
        title(legendText(i),avAngle(s,i))
        axis square
    end
    suptitle(subjectsAll{s})
    saveas(gcf,sprintf('AOVSSPresentation/ImgVel%i.fig',s));
    saveas(gcf,sprintf('AOVSSPresentation/ImgVel%i.png',s));
end

close all
%% load in PRL info from the AO machine
prlAngles = load('C:\Users\Ruccilab\Downloads\anglePCD_PRL_2024_03_15.mat');
for s = 1:subNum
    for ss = 1:length(prlAngles.anglePCD_PRL.subID)
        if strcmp(prlAngles.anglePCD_PRL.subID{ss},subjectsAll{s})
%             x1 = prlAngles.anglePCD_PRL.PCD_loc(1,1);
%             x2 = prlAngles.anglePCD_PRL.PRL_loc(1,1);
%             
%             y1 = prlAngles.anglePCD_PRL.PCD_loc(1,2);
%             y2 = prlAngles.anglePCD_PRL.PRL_loc(1,2);
            
            
            
%             anglesFix(s) = rad2deg(atan2([y1-y2],[x1-x2]));
            angleTask(s) = mod( prlAngles.anglePCD_PRL.angleTaskDeg(ss)+180 , 360);
            angleFix(s) = mod( prlAngles.anglePCD_PRL.angleDeg(ss)+180, 360);
            
%             angleTask(s) = prlAngles.anglePCD_PRL.angleTaskDeg(ss);
%             angleFix(s) = prlAngles.anglePCD_PRL.angleDeg(ss);
        end
    end
end

figure;
plot(prlAngles.anglePCD_PRL.PCD_loc(1,1),prlAngles.anglePCD_PRL.PCD_loc(1,2),'o');
hold on
plot(prlAngles.anglePCD_PRL.PRL_loc(1,1),prlAngles.anglePCD_PRL.PRL_loc(1,2),'o');
axis square

%% % % task vs fixation
figure;
for s = 1:subNum
    subplot(2,2,1)
    plot([1 2],[avSpeed(s,1) avSpeed(s,2)],'-o')
    hold on
    subplot(2,2,2)
    plot([1 2],[avConcentration(s,1) avConcentration(s,2)],'-o')
    hold on
    subplot(2,2,3)
    plot([1 2],[avAngle(s,1) avAngle(s,2)],'-o')
    hold on
    subplot(2,2,4)
      plot([1 2],[avSkew(s,1) avSkew(s,2)],'-o')
%     plot([1 2],rad2deg([angdiff(deg2rad(avAngle(s,1)), deg2rad(angleTask(s))) ...
%         angdiff(deg2rad(avAngle(s,2)), deg2rad(angleFix(s)))]),'-o')
    hold on
end
subplot(2,2,1)
errorbar([1 2],[mean(avSpeed(:,1)) mean(avSpeed(:,2))],...
    [sem(avSpeed(:,1)) sem(avSpeed(:,2))],'-o','Color','k')
[h,p1] = ttest(avSpeed(:,1),avSpeed(:,2));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Task','Fixation'});
ylabel('Peak Speed');
xlim([.75 2.25])

hold on
subplot(2,2,2)
errorbar([1 2],[mean(avConcentration(:,1)) mean(avConcentration(:,2))],...
    [sem(avConcentration(:,1)) sem(avConcentration(:,2))],'-o','Color','k')
[h,p1] = ttest(avConcentration(:,1),avConcentration(:,2));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Task','Fixation'});
ylabel('Average Var of Angle');
xlim([.75 2.25])

subplot(2,2,3)
errorbar([1 2],[mean(avAngle(:,1)) mean(avAngle(:,2))],...
    [sem(avAngle(:,1)) sem(avAngle(:,2))],'-o','Color','k')
ylim([-180 180]);
[h,p1] = ttest(avAngle(:,1),avAngle(:,2));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Task','Fixation'});
ylabel('Average Angle');
xlim([.75 2.25])

subplot(2,2,4)
errorbar([1 2],[mean(avSkew(:,1)) mean(avSkew(:,2))],...
    [sem(avSkew(:,1)) sem(avSkew(:,2))],'-o','Color','k')
ylim([-10 10]);
[h,p1] = ttest(avSkew(:,1),avSkew(:,2));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Task','Fixation'});
ylabel('Average Skewness');
xlim([.75 2.25])

 saveas(gcf,('AOVSSPresentation/AllStatsAllSubj.fig'));
 saveas(gcf,('AOVSSPresentation/AllStatsAllSubj.png'));

% ylim([-180 180]);
% xticks([1 2])
% xticklabels({'Task','Fixation'});
% ylabel('Angle Difference to PRL');
% ylim([-180 180])
% xlim([.75 2.25])
%% Summed Diff of angle

heatmpaOutPut{1}{1}
figure;
imshow(heatmpaOutPut{1}{1})
J = imrotate(heatmpaOutPut{1}{1},20);
figure;
imshow(J)





for s = 1:subNum
    New = [];
    J = [];
    
    New = heatmpaOutPut{1, s}{1};
%     figure;
%     subplot(1,2,1)
%     imagesc(New)
    J = imrotate(New,angleTask(s));
    if s == 3
        probToward(s) =.5;
         probAway(s) = .5;
         continue;
%         continue;
%         J = imrotate(New,101);
    end
%     subplot(1,2,2)
%     imagesc(J);
    [~,Width] = size(J);
%     line([
%      figure;
%     subplot(1,2,1)
%     imagesc(J(:,1:Width/2));
%     subplot(1,2,2)
%     imagesc(J(:,Width/2:end));
    
    sumAway(s) = sum(J(:,1:Width/2),'all');
    sumToward(s) = sum(J(:,Width/2:end),'all');
    
    probToward(s) = sumToward(s)/sum(J,'all');
    
    probAway(s) = sumAway(s)/sum(J,'all');


%     figure;
%     imshow(J(:,1:Width/2))
%     
%      figure;
%     imshow(J(:,Width/2:end))
    
end

figure;
for s = 1:subNum
    plot([1 2],([probToward(s), probAway(s)]),...
        '-o','Color',[.7 .7 .7])
    hold on
end

errorbar([1 2],[nanmean(probToward) nanmean(probAway)],...
    [sem(probToward) sem(probAway)],'-o','Color','k');

xticks([1 2])
xticklabels({'Toward PCD','Away PCD'});
xlim([.5 2.5])
ylabel('Probability');
[h,p] = ttest(probToward,probAway)
title(sprintf('p = %.3f',p));

 saveas(gcf,('AOVSSPresentation/TowardPCD.fig'));
 saveas(gcf,('AOVSSPresentation/TowardPCD.png'));


%% fixation vs task angles diff

figure;
for s = 1:subNum
    polarplot([deg2rad(avAngle(s,1)) ...%task
        deg2rad(prlAngles.anglePCD_PRL.angleTaskDeg(s))],...
        [1 2],'-o');
    hold on
end

figure;
for s = 1:subNum
    forlegs(s) = polarplot([deg2rad(avAngle(s,2)) ...%fixation
        deg2rad(prlAngles.anglePCD_PRL.angleDeg(s))],...
        [1 2],'-o');
    hold on
end
legend(forlegs,subjectsAll);


for s = 1:subNum %task
   az_rayT(s) = mod(avAngle(s,1),360);
   idxTask(s) = rad2deg(abs(angdiff(deg2rad(az_rayT(s)),deg2rad(angleTask(s)))));
end

for s = 1:subNum %fixtation
    az_rayF(s) = mod(avAngle(s,2),360);
    idxFix(s) = rad2deg(abs(angdiff(deg2rad(az_rayF(s)),deg2rad(angleFix(s)))));    
end

figure;
for s = 1:subNum %fixtation
    plot([1 2],[(idxTask(s)),(idxFix(s))],...
    '-o','Color',[.7 .7 .7]);
    hold on
end
% subplot(1,2,1)
errorbar([1 2],[mean(idxTask),mean(idxFix)],[sem(idxTask),sem(idxFix)],...
    '-o','Color','k');
xlabel('Condition')
xticks([1 2])
xticklabels({'E','Fixation'});
ylabel('Difference in angle of Gaze and PCD (degrees)');
xlim([.5 2.5])
[h,p] = ttest(idxTask,idxFix)
title(sprintf('p = %.2f',p));

 saveas(gcf,('AOVSSPresentation/AngleDiffTaskAndFix.fig'));
 saveas(gcf,('AOVSSPresentation/AngleDiffTaskAndFix.png'));

%% control vs scotoma E angle diff
for s = 1:subNum %socotma
    az_ray(s) = mod(avAngle(s,3),360);
    idxTaskScot(s) = rad2deg(abs(angdiff(deg2rad(az_ray(s)),deg2rad(pcdAngle(s)))));    
end


figure;
for s = 1:subNum %fixtation
    plot([1 2],[(idxTask(s)),(idxTaskScot(s))],...
    '-o','Color',[.7 .7 .7]);
hold on
end
% subplot(1,2,1)
errorbar([1 2],[mean(idxTask),mean(idxTaskScot)],[sem(idxTask),sem(idxTaskScot)],...
    '-o','Color','k');
xlabel('Condition')
xticks([1 2])
xticklabels({'E Control','E Scotoma'});
ylabel('Difference in angle of Gaze and PCD (degrees)');
xlim([.5 2.5])
[h,p] = ttest(idxTask,idxTaskScot)
title(sprintf('p = %.2f',p));

 saveas(gcf,('AOVSSPresentation/AngleDiffTaskAndScot.fig'));
 saveas(gcf,('AOVSSPresentation/AngleDiffTaskAndScot.png'));


%% % % all stats E control vs scotoma
figure;
for s = 1:subNum
    subplot(2,2,1)
    plot([1 2],[avSpeed(s,1) avSpeed(s,3)],'-o')
    hold on
    subplot(2,2,2)
    plot([1 2],[avConcentration(s,1) avConcentration(s,3)],'-o')
    hold on
    subplot(2,2,3)
    plot([1 2],[avAngle(s,1) avAngle(s,3)],'-o')
    hold on
    subplot(2,2,4)
      plot([1 2],[avSkew(s,1) avSkew(s,3)],'-o')
%     plot([1 2],rad2deg([angdiff(deg2rad(avAngle(s,1)), deg2rad(angleTask(s))) ...
%         angdiff(deg2rad(avAngle(s,2)), deg2rad(angleFix(s)))]),'-o')
    hold on
end
subplot(2,2,1)
errorbar([1 2],[mean(avSpeed(:,1)) mean(avSpeed(:,3))],...
    [sem(avSpeed(:,1)) sem(avSpeed(:,3))],'-o','Color','k')
[h,p1] = ttest(avSpeed(:,1),avSpeed(:,3));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Peak Speed');
xlim([.75 2.25])

hold on
subplot(2,2,2)
errorbar([1 2],[mean(avConcentration(:,1)) mean(avConcentration(:,3))],...
    [sem(avConcentration(:,1)) sem(avConcentration(:,3))],'-o','Color','k')
[h,p1] = ttest(avConcentration(:,1),avConcentration(:,3));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Average Var of Angle');
xlim([.75 2.25])

subplot(2,2,3)
errorbar([1 2],[mean(avAngle(:,1)) mean(avAngle(:,3))],...
    [sem(avAngle(:,1)) sem(avAngle(:,3))],'-o','Color','k')
ylim([-180 180]);
[h,p1] = ttest(avAngle(:,1),avAngle(:,3));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Average Angle');
xlim([.75 2.25])

subplot(2,2,4)
errorbar([1 2],[mean(avSkew(:,1)) mean(avSkew(:,3))],...
    [sem(avSkew(:,1)) sem(avSkew(:,3))],'-o','Color','k')
ylim([-10 10]);
[h,p1] = ttest(avSkew(:,1),avSkew(:,3));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Average Skewness');
xlim([.75 2.25])

 saveas(gcf,('AOVSSPresentation/AllStatsAllSubjScotoma.fig'));
 saveas(gcf,('AOVSSPresentation/AllStatsAllSubjScotoma.png'));


%%
% figure;
% for s = 1:5
%     forlegs(s) = polarplot([deg2rad(avAngle(s,1)) ...
%         deg2rad(prlAngles.anglePCD_PRL.angleDeg(s)+180)],...
%         [1 2],'-o');
%     hold on
% end
% for s = 6
%     forlegs(s) = polarplot([deg2rad(avAngle(6,1)) ...
%         deg2rad(prlAngles.anglePCD_PRL.angleDeg(7)+180)],...
%         [1 2],'-o');
%     hold on
% end
% for s = 7
%     forlegs(s) = polarplot([deg2rad(avAngle(7,1)) ...
%         deg2rad(prlAngles.anglePCD_PRL.angleDeg(6)+180)],...
%         [1 2],'-o');
%     hold on
% end
% 
% subtitle('Task');
% legend(forlegs,subjectsAll);



%% % task control vs task scotoma
figure;
for s = 1:subNum
    subplot(1,3,1)
    plot([1 2],[avSpeed(s,1) avSpeed(s,3)],'-o')
    hold on
    subplot(1,3,2)
    plot([1 2],[avConcentration(s,1) avConcentration(s,3)],'-o')
    hold on
    subplot(1,3,3)
    plot([1 2],[avAngle(s,1) avAngle(s,3)],'-o')
    hold on
end
subplot(1,3,1)
errorbar([1 2],[mean(avSpeed(:,1)) mean(avSpeed(:,3))],...
    [sem(avSpeed(:,1)) sem(avSpeed(:,3))],'-o','Color','k')
[h,p1] = ttest(avSpeed(:,1),avSpeed(:,3));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Peak Speed');

hold on
subplot(1,3,2)
errorbar([1 2],[mean(avConcentration(:,1)) mean(avConcentration(:,3))],...
    [sem(avConcentration(:,1)) sem(avConcentration(:,3))],'-o','Color','k')
[h,p1] = ttest(avConcentration(:,1),avConcentration(:,3));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Average Concentration');
subplot(1,3,3)
errorbar([1 2],[mean(avAngle(:,1)) mean(avAngle(:,3))],...
    [sem(avAngle(:,1)) sem(avAngle(:,3))],'-o','Color','k')
ylim([-180 180]);
[h,p1] = ttest(avAngle(:,1),avAngle(:,3));
title(sprintf('p = %.2f',p1));
xticks([1 2])
xticklabels({'Control','Scotoma'});
ylabel('Average Angle');

suptitle('Task');

%% ms direction prob distribution

figure;
for ii = 1:subNum
    polarhistogram(dataMS{ii}.msAngle,'DisplayStyle','stairs');
    hold on
end

figure;
t1 = []; t2 = []; t3 = [];
subplot(1,2,1)
for ii = 1:subNum
    idx = find(dataMS{ii,1}.msAmp > 5);
%     [t1,t2] = sort(round(dataMS{ii,1}.msAngle(idx)));
%     t3 = dataMS{ii,1}.msAmp(idx);
%     polarplot(deg2rad(t1),t3(t2),'-');
polarplot(deg2rad(dataMS{ii,1}.msAngle(idx)), dataMS{ii,1}.msAmp(idx),'o');
    hold on
end
title('Task MS')

t1 = []; t2 = []; t3 = [];
subplot(1,2,2)
for ii = 1:subNum
    idx = find(dataMS{ii,2}.msAmp > 5);
%     [t1,t2] = sort(round(dataMS{ii,2}.msAngle(idx)));
%     t3 = dataMS{ii,2}.msAmp(idx);
%     polarplot(t1,t3(t2),'-');
    forlegs(ii) = polarplot(deg2rad(dataMS{ii,2}.msAngle(idx)), dataMS{ii,2}.msAmp(idx),'o');

    hold on
end
title('Fixation MS')
legend(forlegs,subjectsAll);

%%
figure;
xtemp = ones(1,100);
ytemp = [1:100]*-1;
subplot(1,2,1)
 ndhist(xtemp, ytemp, 'bins', 2, 'radial','axis',[-200 200 -200 200],'nr','filt');
subplot (1,2,2)
cp = parula(100);
for t = 1:length(xtemp)
    plot(xtemp(t),ytemp(t),'o','Color',cp(t,:));
    hold on
end
axis square
%%

figure;
% for ii = 1:length(params)
subplot(2,2,1)
for i = 1:subNum
    curvature(i,1) = nanmean(data{i,1}.curvature);
    curvature(i,2) = nanmean(data{i,2}.curvature);
    curvature(i,3) = nanmean(data{i,3}.curvature);
    curvature(i,4) = nanmean(data{i,4}.curvature);
    
end
errorbar([1 2 3 4],[mean(curvature(:,1)) mean(curvature(:,2)) ...
    mean(curvature(:,3)) mean(curvature(:,4))],[sem(curvature(:,1)) sem(curvature(:,2)) ...
    sem(curvature(:,3)) sem(curvature(:,4))],'o');
xticks([1:4])
xlim([0 5])
ylim([9 15])
xticklabels({'Task, Cont','Fix, Cont','Task, Scot','Fix, Scot'});
ylabel('Curvature');

[h,p1] = ttest(curvature(:,1),curvature(:,2))
[h,p2] = ttest(curvature(:,1),curvature(:,3))
[h,p3] = ttest(curvature(:,3),curvature(:,4))
[h,p4] = ttest(curvature(:,2),curvature(:,4))

groups = {[1 2],[1 3],[3 4],[2 4]};
H=sigstar(groups,[p1 p2 p3 p4]);
%%%%
subplot(2,2,2)
for i = 1:subNum
    bcea(i,1) = nanmean(data{i,1}.bcea);
    bcea(i,2) = nanmean(data{i,2}.bcea);
    bcea(i,3) = nanmean(data{i,3}.bcea);
    bcea(i,4) = nanmean(data{i,4}.bcea);
    
end
errorbar([1 2 3 4],[mean(bcea(:,1)) mean(bcea(:,2)) ...
    nanmean(bcea(:,3)) nanmean(bcea(:,4))],[sem(bcea(:,1)) sem(bcea(:,2)) ...
    sem(bcea(:,3)) sem(bcea(:,4))],'o');
xticks([1:4])
xlim([0 5])
xticklabels({'Task, Cont','Fix, Cont','Task, Scot','Fix, Scot'});
ylabel('bcea');

[h,p1] = ttest(bcea(:,1),bcea(:,2))
[h,p2] = ttest(bcea(:,1),bcea(:,3))
[h,p3] = ttest(bcea(:,3),bcea(:,4))
[h,p4] = ttest(bcea(:,2),bcea(:,4))

groups = {[1 2],[1 3],[3 4],[2 4]};
H=sigstar(groups,[p1 p2 p3 p4]);

%%%%
subplot(2,2,3)
for i = 1:subNum
    span(i,1) = nanmean(data{i,1}.span);
    span(i,2) = nanmean(data{i,2}.span);
    span(i,3) = nanmean(data{i,3}.span);
    span(i,4) = nanmean(data{i,4}.span);
    
end
errorbar([1 2 3 4],[mean(span(:,1)) mean(span(:,2)) ...
    nanmean(span(:,3)) nanmean(span(:,4))],[sem(span(:,1)) sem(span(:,2)) ...
    sem(span(:,3)) sem(span(:,4))],'o');
xticks([1:4])
xlim([0 5])

xticklabels({'Task, Cont','Fix, Cont','Task, Scot','Fix, Scot'});
ylabel('span');

[h,p1] = ttest(span(:,1),span(:,2))
[h,p2] = ttest(span(:,1),span(:,3))
[h,p3] = ttest(span(:,3),span(:,4))
[h,p4] = ttest(span(:,2),span(:,4))

groups = {[1 2],[1 3],[3 4],[2 4]};
H=sigstar(groups,[p1 p2 p3 p4]);
% end
%%%%
subplot(2,2,4)
for i = 1:subNum
    amplitude(i,1) = nanmean(data{i,1}.amplitude);
    amplitude(i,2) = nanmean(data{i,2}.amplitude);
    amplitude(i,3) = nanmean(data{i,3}.amplitude);
    amplitude(i,4) = nanmean(data{i,4}.amplitude);
    
end
errorbar([1 2 3 4],[mean(amplitude(:,1)) mean(amplitude(:,2)) ...
    nanmean(amplitude(:,3)) nanmean(amplitude(:,4))],[sem(amplitude(:,1)) sem(amplitude(:,2)) ...
    sem(amplitude(:,3)) sem(amplitude(:,4))],'o');
xticks([1:4])
xlim([0 5])

xticklabels({'Task, Cont','Fix, Cont','Task, Scot','Fix, Scot'});
ylabel('amplitude');

[h,p1] = ttest(amplitude(:,1),amplitude(:,2))
[h,p2] = ttest(amplitude(:,1),amplitude(:,3))
[h,p3] = ttest(amplitude(:,3),amplitude(:,4))
[h,p4] = ttest(amplitude(:,2),amplitude(:,4))

groups = {[1 2],[1 3],[3 4],[2 4]};
H=sigstar(groups,[p1 p2 p3 p4]);

%%
figure;

for i = 1:length(subjectsAll)
    subplot(2,3,i)
    allAnglesAll = [];
    allSpeed = [];
%     counter = 1;
    for a = 1:length(data{i}.driftAngle)
        allAngles = rad2deg(data{i}.driftAngle{a});
        allAnglesAll = [allAnglesAll allAngles];
        allSpeed = [allSpeed data{i}.instSpX{a}];
    end
%     find(allAnglesAll > 0 &
    
    polarplot(allAnglesAll,allSpeed,'o');
end

%% Open questions
% 1. It would be nice to supplment our so far data with drift behavior (ie
% difference between fix and snellen CONTROL are there differences in
% directionality of drift? IE toward there cdc/pcd? Any differences...
% 2. Include miniscotoma data... "we investigated differences in visual
% task and we find a difference - now what is driving this change in
% beavior? if we immobilize the part of the retina they are using for the
% high acuity task... how does behavior change?"
%   this section is additional - supplement other material

% Ashley TO DO:
% * Direction of Drift (polar plot) --> directionality in drift?
% * Standard battery (amplitude, speed, duration, D, amplitude);
% * Create notion page "Eye trace prcoessing notes": https://www.notion.so/Eyetrace-processing-notes-159afac9a9564c308e9af8c44fb1592e
% * report all 4 conditions (scotoma/control,fixation/task)




