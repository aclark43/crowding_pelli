function crowding_PSD()
%% power spectra from drift (Brownian motion model)
dc = 20; % diffusion constant (arcmin^2/s)

f = linspace(1, 80, 80); % temporal frequencies (Hz)
k = logspace(-1, 2, 100); % spatial frequencies (cpd)

ps = Qfunction(f, k, dc / 60^2); % assumes isotropic

figure(1); clf;
subplot(1, 2, 1);
pcolor(k, f, pow2db(ps));
colormap 'hot'; shading interp;
set(gca, 'XScale', 'log', 'YScale', 'log');
xlabel('spatial frqeuency (cpd)');
ylabel('temporal frequency (Hz)');
set(gca, 'XTick', [1, 10, 30, 50], 'YTick', [1, 10, 30, 80]);
title('PSD of BM drift');
axis square;

subplot(1, 2, 2);
plot(k, pow2db(nansum(ps(f >=2 & f <= 40, :), 1)), 'k-', 'linewidth', 2);
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
ylabel('PSD (db)');
title('temporal power in RGC range');
xlabel('spatial frequency (cpd)');
axis square;

%% stimuli (drawing sanity check)
digits = [3, 5, 6, 9];
figure(2); clf;
for di = 1:length(digits)
    im = drawDigit(digits(di), 4);
    subplot(2, 2, di);
    imagesc(im);
    axis image; colormap gray;
end

%% power spectra of uncrowded stimuli
% works best when pixelAngle and widthAngle of the digit result in an even
% number of pixels for the width of the digit
pixelAngle = .25; % arcmin
widthAngle = 2; % arcmin (angular width of digit)
widthPixel = widthAngle / pixelAngle; % pixel - even number please!

bufferSizeAngle = 2 * 60; % arcmin
bufferSizePixel = bufferSizeAngle / pixelAngle;

sz = 2 * bufferSizePixel;
k = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);

allSpec = cell(size(digits));

ca = [-40, 20];

for di = 1:length(digits)
    im = drawDigit(digits(di), widthPixel);
    
    % buffer image and make it square
    im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel], 1, 'both');
    [m, n] = size(im_buffered);
    df = floor((m - sz)/2);
    im_buffered = im_buffered(df:(df + sz - 1), :);
    df = floor((n - sz)/2);
    im_buffered = im_buffered(:, df:(df + sz - 1));
    
    
    %%%%%%%%% contrast reverse the image because fft pads with zeros
    % This may affect phase but does not have a major impact on power
    % except at (0, 0)
    im_buffered = 1 - im_buffered;
    
    spec = fftshift(fft2(im_buffered));
    
    allSpec{di} = struct('PS', spec, 'k', k);
    
    plot_k = [2, 10, 30, 50];
    plot_k_index = interp1(k, 1:length(k), plot_k, 'nearest');
    
    figure(10+di); clf; % plots for just this digit
    
    subplot(2, 2, 1); % space-time PSD
    imagesc(k, k, pow2db(abs(spec).^2));
    title(sprintf('PSD of %i', digits(di)));
    axis image; 
    xlabel('horizontal spatial frequency (cpd)');
    ylabel('vertical spatial frequency (cpd)');
    colormap hot; colorbar;
    caxis(ca);
    
    subplot(2, 2, 2); % vertical slices across horizontal k
    plot(k, pow2db(abs(spec(:, plot_k_index)).^2));
    xlabel('vertical spatial frequency (cpd)');
    grid on;
    ylim(ca);
    
    subplot(2, 2, 3); % vertical slices across horizontal k
    plot(k, pow2db(abs(spec(plot_k_index, :)).^2));
    xlabel('horizontal spatial frequency (cpd)');
    grid on;
    ylim(ca);
    
    subplot(2, 2, 4); hold on;
    plot(k, pow2db(nanmean(abs(spec), 1).^2), 'b-', 'linewidth', 2);
    plot(k, pow2db(nanmean(abs(spec), 2).^2), 'r-', 'linewidth', 2);
    legend('average horizontal', 'average vertical');
    xlabel('spatial frequency (cpd)');
    ylim([-10, 30]);
end

%% compare across digits
cols = lines(length(digits));
figure(3); clf; % plot across digits
figure(4); clf; % plot horizontal and vertical slices
for di = 1:length(digits)
    spec = allSpec{di}.PS;
    k = allSpec{di}.k;
    
    figure(3);
    subplot(2, 2, di);
    imagesc(k, k, pow2db(abs(spec).^2));
    title(sprintf('PSD of %i', digits(di)));
    axis image;
    xlabel('horizontal spatial frequency (cpd)');
    ylabel('vertical spatial frequency (cpd)');
    colormap hot; colorbar;
    caxis(ca);
    
    figure(4);
    subplot(1, 2, 1); hold on;
    plot(k(k > 0), pow2db(nanmean(abs(spec(:, k>0)), 1).^2), 'b-',...
        'linewidth', 2, 'Color', cols(di, :));
    
    subplot(1, 2, 2); hold on;
    plot(k(k > 0), pow2db(nanmean(abs(spec(k>0, :)), 2).^2), 'r-',...
        'linewidth', 2, 'Color', cols(di, :));
    
end

figure(3); hold off;

figure(4);
subplot(1, 2, 1);
xlabel('horizontal spatial frequency (cpd)');
ylim([-10, 30]);
axis square;
ylabel('power (db)');
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlim([5, k(end)]);
hold off;
title('uncrowded target');

subplot(1, 2, 2);
xlabel('vertical spatial frequency (cpd)');
ylim([-10, 30]);
legend(cellfun(@num2str, num2cell(digits), 'UniformOutput', false));
axis square;
ylabel('power (db)');
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlim([5, k(end)]);
title('uncrowded target');
hold off;

%% power spectrum of crowded stimulus
% works best when pixelAngle and widthAngle of the digit result in an even
% number of pixels for the width of the digit
target = 3;
flankers = [5, 6, 9, 5];

c2c = 1.4; % center to center spacing scalar

sz = 2 * bufferSizePixel;
k = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);

im_target = drawDigit(target, widthPixel);
[m, n] = size(im_target);

im_buffered = ones(sz, sz);

im_buffered(bufferSizePixel - round(m/2) + (1:m),...
    bufferSizePixel - round(n/2) + (1:n)) = im_target;

im_buffered_target = 1 - im_buffered;
xx_img = ((1:sz) - (sz + 1) / 2) * pixelAngle;

for fi = 1:length(flankers)
    im_flanker = drawDigit(flankers(fi), widthPixel);
    [m, n] = size(im_flanker);
    
    switch fi
        case 1 %right
            cv = 0;
            ch = round(c2c * widthPixel);
        case 2 % up
            cv = round(c2c * widthPixel * 5);
            ch = 0;
        case 3 % left
            cv = 0;
            ch = -round(c2c * widthPixel);
        case 4 % down
            cv = -round(c2c * widthPixel * 5);
            ch = 0;
    end
    
    im_buffered(bufferSizePixel + cv - round(m/2) + (1:m),...
        bufferSizePixel + ch - round(n/2) + (1:n)) = im_flanker;

end
im_buffered = 1 - im_buffered;

spec_uncrowded = fftshift(fft2(im_buffered_target));
spec_crowded = fftshift(fft2(im_buffered));

figure(5); clf;
imagesc(xx_img, xx_img, 1-im_buffered);
axis image;
xlim([-30, 30]);
ylim([-30, 30]);
colormap gray;


ca = [-40, 40];

figure(6); clf;
subplot(1, 2, 1);
pcolor(k, k, pow2db(abs(spec_uncrowded).^2));
colormap hot;
xlabel('horizontal frequency (cpd)');
xlabel('vertical frequency (cpd)');
title(sprintf('uncrowded %i', target));
shading interp
caxis(ca);
axis image;

subplot(1, 2, 2);
pcolor(k, k, pow2db(abs(spec_crowded).^2));
colormap hot;
xlabel('horizontal frequency (cpd)');
xlabel('vertical frequency (cpd)');
title(sprintf('crowded %i', target));
shading interp;
caxis(ca);
axis image;

figure(7); clf; 
subplot(2, 2, 1); % space-time PSD
imagesc(k, k, pow2db(abs(spec_crowded).^2));
title(sprintf('PSD of crowded %i', digits(di)));
axis image;
xlabel('horizontal spatial frequency (cpd)');
ylabel('vertical spatial frequency (cpd)');
colormap hot; colorbar;
caxis(ca);

subplot(2, 2, 2); % vertical slices across horizontal k
plot(k, pow2db(abs(spec_crowded(:, plot_k_index)).^2));
xlabel('vertical spatial frequency (cpd)');
grid on;
ylim([-10, 50]);
ylabel('power (db)');

subplot(2, 2, 3); % vertical slices across horizontal k
plot(k, pow2db(abs(spec_crowded(plot_k_index, :)).^2));
xlabel('horizontal spatial frequency (cpd)');
grid on;
ylim([-10, 50]);
ylabel('power (db)');

subplot(2, 2, 4); hold on;
plot(k, pow2db(nanmean(abs(spec_crowded), 1).^2), 'b-', 'linewidth', 2);
plot(k, pow2db(nanmean(abs(spec_crowded), 2).^2), 'r-', 'linewidth', 2);
plot(k, pow2db(nanmean(abs(spec_uncrowded), 1).^2), 'g--', 'linewidth', 2);
plot(k, pow2db(nanmean(abs(spec_uncrowded), 2).^2), 'm--', 'linewidth', 2);
legend('avg horizontal crowded', 'avg vertical crowded',...
    'avg horizontal uncrowded', 'avg vertical uncrowded');
xlabel('spatial frequency (cpd)');
ylim([0, 40]);
ylabel('power (db)');

figure(8); clf;
subplot(1, 2, 1); hold on;
plot(k(k>0), pow2db(nanmean(abs(spec_crowded(:, k>0)), 1).^2), 'b-', 'linewidth', 2);
plot(k(k>0), pow2db(nanmean(abs(spec_uncrowded(:, k>0)), 1).^2), 'g--', 'linewidth', 2);
xlabel('horizontal spatial frequency (cpd)');
ylim([0, 40]);
legend('crowded', 'uncrowded');
axis square;
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlim([5, k(end)]);
ylabel('power (db)');

subplot(1, 2, 2); hold on;
plot(k(k>0), pow2db(nanmean(abs(spec_crowded(k>0, :)), 2).^2), 'r-', 'linewidth', 2);
plot(k(k>0), pow2db(nanmean(abs(spec_uncrowded(k>0, :)), 2).^2), 'm--', 'linewidth', 2);
xlabel('vertical spatial frequency (cpd)');
ylim([0, 40]);
legend('crowded', 'uncrowded');
axis square;
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlim([5, k(end)]);
ylabel('power (db)');
keyboard;

end

function digit = drawDigit(whichDigit, pixelwidth)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
end

end