% This M-file computes the temporal response of an P-type retinal ganglion
% cell as reported by Benardete and Kaplan (1999)
% JOurnal of Physiology
%
% USAGE:
%     P_temporal = MacaqueRetinaP_Temporal_BK
% OUTPUT:
%    P_temporal = impulse response consisting of 300 samples
%                 and with a temporal interval between each
%                 sample of 1ms
%
function [P_Temporal, K, w] = filters_MacaqueRetinaP_Temporal_BK(NSteps, Hs)

% number of steps computed ... it MUST be an EVEN number
if nargin==0
    NSteps = 300;
end

% time step (IN S!)
TStep = .001;
MaxFreq = .5*(1/TStep);

% parameters reported in the paper by Benardete and Kaplan (use 1997)
c = .4;
A = 67.59; % JI 12.63;% 
% factor D (in ms. In the original paper it was in s)
D = 3.5/1000;% 2.2/1000; %
% if Hs = 1 then the integral of the temporal impulse response is 0
% in the original paper Benardete and Kaplan reported Hs = .98
% in my simulations we put Hs = 1
% Hs = 0.98;
if ~exist('Hs', 'var')
    Hs = 0.62; % JI - use 0.62 for extrafoveal sensitivity
end

% factor tau_S and tau_L (in seconds. In the original paper it was in ms)
tau_S = 29.36 / 1000;%25.88/1000; % JI
N_L = 38;%46.15; % JI 
tau_L = (56.35 / N_L) / 1000; %JI


% here we compute the impulse response in the Fourier domain
w = 2*pi*linspace(0, MaxFreq, floor(NSteps/2) + 1);
K = A * exp(-1i*w*D) .* (1 - Hs./(1 + 1i*w*tau_S)) .* ...
	((1./(1+1i*w*tau_L)).^N_L);
RemSteps = NSteps - length(K);
FinK = [K, conj(fliplr(K(end-RemSteps:end-1)))];

% ... and here where perform the inverse Fourier Transform
P_Temporal = real(ifft(FinK));

% ... normalize so that the maximum of the response is 1
%P_Temporal = P_Temporal/max(abs(P_Temporal));
end