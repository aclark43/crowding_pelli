function thresholdVals = determineThresholdSWEcc(subjectCond,subjectThreshCond,...
    conditions, subNum, ecc)
%Determine the threshold strokewidth value for both the crowded and
%uncrowded condition

% for condIdx = 1:2
for ii = 1:subNum
    thresholdSizeSWRound = round(subjectCond(ii).stimulusSize,1);
    matchingThresholdRound = round(subjectThreshCond(ii).thresh,1);
    [~,thresholdIdx] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    thresholdVals(ii).name = subjectThreshCond(ii).Condition;
    thresholdVals(ii).vals = ...
        sprintf('strokeWidth_%i', subjectCond(ii).SW(thresholdIdx));
    %         thresholdVals(ii).name = subjectThreshCond(ii).Condition;
    if isempty(subjectThreshCond(ii).em)
        continue;
    end
    if strcmp(ecc,'ecc_0')
        eccName = 'ecc_0';
    else
        eccName = ecc;
    end
    match = [];
    A = fieldnames(subjectThreshCond(ii).em);
    match = lookThroughFields(ecc,A);
    if sum(match) ==  0 %%if this threshold exists
        warning('There is no ecc data here')
        thresholdVals(ii).vals = [];
        continue;
    end
    
    match = [];
    A = fieldnames(subjectThreshCond(ii).em.(eccName));
    match = lookThroughFields(thresholdVals(ii).vals,A);
    
    if sum(match) > 0 %%if this threshold exists
        continue
    else
        thresholdVals(ii).vals = ...
            sprintf('strokeWidth_%i', subjectCond(ii).SW(thresholdIdx)+1);
    end
    match = [];
    A = fieldnames(subjectThreshCond(ii).em.(ecc));
    match = lookThroughFields(thresholdVals(ii).vals,A);
    if sum(match) > 0 %%if this threshold exists
        continue
    else
        thresholdVals(ii).vals = ...
            sprintf('strokeWidth_%i', subjectCond(ii).SW(thresholdIdx)-1);
    end
    match = [];
    match = lookThroughFields(thresholdVals(ii).vals,A);
    if sum(match) > 0 %%if this threshold exists
        continue
    else
        thresholdVals(ii).vals = ...
            sprintf('strokeWidth_%i', subjectCond(ii).SW(thresholdIdx)+2);
    end
    
    match = [];
    match = lookThroughFields(thresholdVals(ii).vals,A);
    if sum(match) == 0
        error('Cant find SW for this condition');
    end
    
end
% end

% for ii = 1:subNum
%     thresholdSizeSWRound = round(subjectCro(ii).stimulusSize,1);
%     matchingThresholdRound = round(subjectThreshCro(ii).thresh,1);
%     [~,thresholdIdxCro] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
%     thresholdVals(ii).thresholdSWCrowded = ...
%         sprintf('strokeWidth_%i', subjectCro(ii).SW(thresholdIdxCro));
% end

    function match = lookThroughFields(B,A)
        for numSW = 1:length(A)
            if strcmp(A{numSW},(B))
                match(numSW) = 1;
            else
                match(numSW) = 0;
            end
        end
    end
end

