function data = fixAlternatingNoTrackAndBlinkIssue(data)

%% A function that takes in raw data from DDPI and modifies no tracks to overcome aternating notrack/blink issue 
  % finds the idx of blink occurences,
 % Sets No tracks to 1 for these idx's where blink occured (oVERCOMES
 % ALTERNATING BLINK & NO TRACK ISSUES)
 % Also calculates differential of x-traces to identify notracks and gets
 % those respective indices
 % Sets no tracks to 1 where there was a no track during the x traces.


dataManipulated = data;
for id = 1:length(data.triggers)
    
    diffX = diff(data.x{id});
    idxdiffX = find(diffX == 0);
    idxB = find(data.triggers{id}.blink == 1);
    
    dataManipulated.triggers{id}.notrack(idxB) = 1;
    dataManipulated.triggers{id}.notrack(idxdiffX) = 1;
    dataManipulated.triggers{id}.blink(idxB) = 0;
        
end
data = dataManipulated;
end