% function ft = calc_ms_peakvelocity(vt,ft,params)
function speed = calc_ms_peakvelocity(vt,ft,params)
% vt = structure of trial information
% 


for trialType = {'cong','incong'}
    
    microsaccade.start = vt{
    microsaccade.end
    
    %gaze position during microsaccade in samples %offset already acounted for
    x_pos_samples = ft.(trialType{1}).xpos_microsaccade;
    y_pos_samples = ft.(trialType{1}).ypos_microsaccade;
    
    ms_traces = input('\n Would you like to look at position and velocity plots? (y/n): \n','s');
    
    if ms_traces == 'y'
        
        
        for i = 1:length(x_pos_samples)
            trialnum = ft.(trialType{1}).id(i);
            xpos = cell2mat(x_pos_samples(i));
            ypos = cell2mat(y_pos_samples(i));
            
            vel_x = diff(xpos);
            vel_y = diff(ypos);
            
%             ft.speed.(trialType{1})(i) = max(sqrt(xpos.^2 + ypos.^2))*(params.Fs);
            speed(i) = max(sqrt(xpos.^2 + ypos.^2));%*(params.Fs);
            
            speed_seconds = sqrt(xpos.^2 + ypos.^2); %.*params.Fs
            
            subplot(2,2,1)
            tm = (0:(1/330):(length(xpos)-1)*1/330);
            plot(tm,xpos,'color',[.90 .70 .70]); 
            hold on; % in arcmin
            plot(tm,ypos,'color',[0 0 0]); 
            hold on;% in arcmin
            legend({'x trace', 'y trace'});
            xlabel('time (ms)');
            ylabel('Displacement of gaze location wrt fixation (arcmin)');
            subtitle(sprintf('Gaze position during microsaccade Trial %d',trialnum ));
            
            subplot(2,2,2)
            tm = (0:(1/330):(length(vel_x)-1)*1/330);
            plot(tm,vel_x,'color',[.90 .70 .70]); hold on; % in arcmin
            plot(tm,vel_y,'color',[0 0 0]); hold on;% in arcmin
            xlabel('time (secs)')
            legend({'x trace', 'y trace'});
            ylabel('Velocity (arcmin/s)');
            subtitle(sprintf("Velocity of microsaccade Trial %d", trialnum ));
            
            subplot(2,2,3)
            tm = (0:(1/330):(length(speed_seconds)-1)*1/330);
            plot(tm, speed_seconds,'color',[.20 .40 .40]); hold on;
            
            subplot(2,2,4)
            MakeFigure(vt,trialnum,1, params)
            
        end
    else
         for i = 1:length(x_pos_samples)
            trialnum = ft.(trialType{1}).id(i);
            xpos = cell2mat(x_pos_samples(i));
            ypos = cell2mat(y_pos_samples(i));
            
            vel_x = diff(xpos);
            vel_y = diff(ypos);
%             ft.speed.(trialType{1})(i) = max(sqrt(xpos.^2 + ypos.^2));%*(params.Fs);
            speed(i) = max(sqrt(xpos.^2 + ypos.^2));%*(params.Fs);
            speed_seconds = sqrt(xpos.^2 + ypos.^2); %.*params.Fs
        end
        
        
    end
    
    
    
    
    
end

end