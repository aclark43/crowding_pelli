function ppt_out = updatePPTAmplitudeAngle(ppt_in)
%ppt_out = updatePPTAmplitudeAngle(ppt_in)
%   ppt_in : pptrials cell array with updated start and duration
%       information in saccades, microsaccades, and drifts fields
%   ppt_out : pptrials cell array with updated amplitude and angle
%       information based on new start times

ppt_out = ppt_in;


% fields to update
flds = {'saccades', 'microsaccades', 'drifts'};

for ii = 1:length(ppt_in) % loop through trials
     trial = ppt_out{ii};
     
     for fi = 1:length(flds) % loop through EM types
         fld = flds{fi};
         
         st = trial.(fld); % original struct containing start, duration, amp, ang
         
         st_updated = updateAmplitudeAngle(trial, st); % update amps and angs
         
         trial.(fld) = st_updated; % save to trial structure
         
     end % loop through EM types
     
     ppt_out{ii} = trial; % save new trial to ppt
     
end % loop through trials

end

