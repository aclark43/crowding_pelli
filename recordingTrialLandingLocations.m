function [MicroSaccStartTime, microSaccadeEndTime, MicroSaccStartLoc, MicroSaccEndLoc, SaccStartLoc, SaccEndLoc] = ...
    recordingTrialLandingLocations(trial, Coordinates, params, ~)

%ReadParams;

% Recording the number of microsaccades per
% trial
microSaccadeStart = trial.microsaccades.start;
microSaccadeEnd = trial.microsaccades.duration + microSaccadeStart - 1;
MicroSaccStartTime = microSaccadeStart;
microSaccadeEndTime = microSaccadeEnd; %trial.microsaccades.start + trial.microsaccades.duration;

% Record the number of saccades per trial
saccadeStart = trial.saccades.start;
saccadeEnd = trial.saccades.duration + saccadeStart - 1;
sIDx = find(saccadeEnd<trial.TimeTargetOFF);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%              XX/YY Positions            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp('D',params.machine)
    xx = (trial.x.position(floor(trial.TimeTargetON/(1000/330):trial.TimeTargetOFF/(1000/330)))/trial.pixelAngle)+trial.xoffset; % in pixels
    yy = (trial.y.position(floor(trial.TimeTargetON/(1000/330):trial.TimeTargetOFF/(1000/330)))/trial.pixelAngle)+trial.yoffset;
else
    xx = (trial.x.position(floor(trial.trial.TimeTargetON/(1000/330):trial.TimeTargetOFF))/trial.pixelAngle)+trial.xoffset; % in pixels
    yy = (trial.y.position(floor(trial.trial.TimeTargetON/(1000/330):trial.TimeTargetOFF))/trial.pixelAngle)+trial.yoffset;
end

if trial.TimeTargetOFF < 500
    tmp_end = trial.TimeTargetOFF;
else
    tmp_end = 500;
end

if ~isempty(microSaccadeStart)
    idx = floor(microSaccadeStart*(330/1000))<length(xx);
    MicroSaccStartLoc.x = xx(floor(microSaccadeStart*(330/1000))&idx);
    MicroSaccStartLoc.y = yy(floor(microSaccadeStart*(330/1000))&idx);
    MicroSaccEndLoc.x = xx(floor(microSaccadeEnd*(330/1000))&idx);
    MicroSaccEndLoc.y = yy(floor(microSaccadeEnd*(330/1000))&idx);
else
     MicroSaccStartLoc.x = NaN;
    MicroSaccStartLoc.y = NaN;
    MicroSaccEndLoc.x = NaN;
    MicroSaccEndLoc.y = NaN;
end

% if ~isempty(microSaccadeStart)
    SaccStartLoc = NaN;
%     SaccStartLoc
    SaccEndLoc = NaN;
%     SaccEndLoc
% end
% % [Out,~,~,~,~,~,~] = ClassifyEM(xx,yy,Coordinates);

% classification for ms landing positions
% % MicroSaccStartLoc = [];
% % MicroSaccEndLoc = [];
% % SaccStartLoc = [];
% % SaccEndLoc = [];

% % % 
% % % for ii = 1:length(trial.microsaccades.start)
% % %     if isnan(trial.microsaccades.start(ii))
% % %         continue;
% % %     end
% % %     if (round(trial.microsaccades.start(ii))) > length(Out)
% % %         MicroSaccStartLoc(ii) = NaN;
% % %         %     elseif(round(trial.microsaccades.start(ii))) == 0
% % %         %         MicroSaccStartLoc(ii) = NaN; %%microsaccade not in trial
% % %     elseif (trial.microsaccades.start(ii)) == 0
% % %         continue;
% % %     else
% % %         MicroSaccStartLoc(ii) = Out(round(trial.microsaccades.start(ii)));
% % %     end
% % %     
% % %     if round(trial.microsaccades.duration(ii) + (trial.microsaccades.start(ii)) - 1) > length(Out)
% % %         MicroSaccEndLoc(ii) = NaN;
% % %         %     elseif round(trial.microsaccades.duration(ii) + (trial.microsaccades.start(ii)) - 1) == -1
% % %         %          MicroSaccEndLoc(ii) = NaN;
% % %     elseif trial.microsaccades.duration(ii) < 0
% % %         MicroSaccEndLoc(ii) = NaN;
% % %     else
% % %         MicroSaccEndLoc(ii) = Out(round(trial.microsaccades.duration(ii) + ...
% % %             (trial.microsaccades.start(ii)) - 1));
% % %     end
% % %     endTimeMs(ii) = round(trial.microsaccades.duration(ii) + ...
% % %         trial.microsaccades.start(ii) -1);
% % % end
% % % %% classification for s landing positions
% % % if isempty(trial.saccades.start) == 0
% % %     if ~(round(trial.saccades.start(sIDx))) > length (Out)
% % %         SaccStartLoc = Out(round(trial.saccades.start(sIDx)));
% % %         SaccEndLoc = Out(round(trial.saccades.duration(sIDx) + ...
% % %             trial.saccades.start(sIDx) - 1));
% % %         endTimeS = round(trial.saccades.duration(sIDx) + ...
% % %             trial.saccades.start(sIDx) -1);
% % %     else
% % %         SaccStartLoc = NaN;
% % %         SaccEndLoc = NaN;
% % %         endTimeS = NaN;
% % %     end
% % % else
% % %     for sIDx = 1:length(trial.saccades.start)
% % %         if isnan(trial.saccades.start(sIDx))
% % %             continue;
% % %         end
% % %         if (round(trial.saccades.start(sIDx))) > length(Out)
% % %             SaccStartLoc(ii) = NaN;
% % %             SaccEndLoc = NaN;
% % %             endTimeS = NaN;
% % %         else
% % %             SaccStartLoc = Out(round(trial.saccades.start(sIDx)));
% % %             if (round(trial.saccades.duration(sIDx) + ...
% % %                     trial.saccades.start(sIDx) - 1)) > length(Out)
% % %                 SaccEndLoc = NaN;
% % %                 endTimeS = NaN;
% % %             else
% % %                 SaccEndLoc = Out(round(trial.saccades.duration(sIDx) + ...
% % %                     trial.saccades.start(sIDx) - 1));
% % %                 endTimeS = round(trial.saccades.duration(sIDx) + ...
% % %                     trial.saccades.start(sIDx) -1);
% % %             end
% % %         end
% % %     end
% % % end
end