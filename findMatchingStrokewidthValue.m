function [stringVal, performance, actualSize] = findMatchingStrokewidthValue (matchingThresholdRound, toPath)


allSWSaved = (fieldnames(toPath));
for i = 1:length(allSWSaved)
    allSizes(i) = toPath.(allSWSaved{i}).stimulusSize;
end
[~,thresholdIdx] = find(min(abs(matchingThresholdRound - allSizes))...
    == (abs(matchingThresholdRound - allSizes)));
stringVal = ...
    sprintf('%s', allSWSaved{thresholdIdx(1)});
performance = ...
    toPath.(sprintf...
    ('%s', allSWSaved{thresholdIdx(1)})).performanceAtSize;

actualSize = allSizes(thresholdIdx(1));