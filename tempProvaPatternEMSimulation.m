% clear all
% close all
clc
counterP = 1;
% useRealTraces = 1;
%%% Create the image
% stimSize = 1.5;     % width of stim in arcmin
% pixelwidth = 24;    % how many pixels in stim? size should be multiple of 2
% pixel_angle = 1;   % pixel ~ arcmin conversion, pixel angle = xx arcmin (of images)

% stimSize = 1.5;     % width of stim in arcmin
% pixelwidth = 12;    % how many pixels in stim? size should be multiple of 2
% pixel_angle = .5;   % pixel ~ arcmin conversion, pixel angle = xx arcmin (of images)

stimSize = 1.5;        % width of stim in arcmin
pixelwidth = 4;
pixel_angle = .35;   % pixel ~ arcmin conversion, pixel angle = xx arcmin (of images)

bufferSizeAngle = 2*10;% arcmin
% bufferSizePixel = bufferSizeAngle / pixel_angle;
bufferSizePixel = round(255/2);
% sz = 2 * bufferSizePixel;

digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
dc = [4.86753463745117 19.6812686920166]; % diffusion constant arcmin^2/s

for useRealTraces = [0]
    for d = 2%length(dc)
        D = dc(d);
        for whichDigit = [3]
            digit = drawDigit(whichDigit, pixelwidth);
            %     digit(:, (strokewidth+1):(2*strokewidth)) = 0;
            %     digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
            
            im = digit;
            %         figure;imagesc(im)
            
            if whichDigit == 3 || whichDigit == 9
                %im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel],0, 'both');
                im_buffered = padarray(im-mean(im(:,size(im,2))), [bufferSizePixel, bufferSizePixel], 1, 'both');
            elseif whichDigit == 5
                im_buffered = padarray(im-mean(im(1:3,size(im,2))), [bufferSizePixel, bufferSizePixel], 1, 'both');
            else
                im_buffered = padarray(im-mean(im(:,(size(im,2)/2))), [bufferSizePixel, bufferSizePixel], 1, 'both');
            end
            
            
            [height, width] = size(im_buffered);
            trimImage = im_buffered([round((height-width)/2):height-round((height-width)/2)],:);
            img = trimImage;
            size_img = size(img, 1);
            
            size_retina = 151; % pixels, keep it odd
            half_size_retina = floor(size_retina/2);
            
            Fs = 1000; % sampling rate (Hz)
            nSamples = 300; % number of samples 512
            nTraces = 100; % number of traces
            
            mv = nan(size_retina, size_retina, nSamples);
            
            
            x_img_arcmin = ((1:size_img) - (size_img + 1) / 2) * pixel_angle; % angular position of each pixel in image
            %%  The Traces
            clear Xe; clear Ye;
            if useRealTraces
                %%% Actual Subject Traces
                
                
                %             nTraces = 10; %same number of trials for each
                if d == 1
                    load('MATFiles/dataEMPower.mat')
                    Traces = dataEMPower.position{1};
                    endValue = 300;
                    
                    for i = 1:nTraces
                        Xe(i,:) = Traces(i).x(1:endValue);
                        Ye(i,:) = Traces(i).y(1:endValue);
                        %                         Xe(i,:) = Traces(i).x(1:3:endValue); %for 1khz, undersample 500
                        %                         Ye(i,:) = Traces(i).y(1:3:endValue);
                    end
                elseif d == 2
                    load('MATFiles/dataEMPowerDPIZ005.mat')
                    Traces = temp;
                    endValue = 300;
                    
                    %                     endValue = 500/(1000/330);
                    %                 nTraces = length(Traces);
                    %                 randomPickTrace = randi([1 175],1,nTraces);
                    %                 for i = 1:nTraces
                    for i = 1:nTraces
                        Xe(i,:) = Traces(i).x(1:endValue);
                        Ye(i,:) = Traces(i).y(1:endValue);
                    end
                end
                [~,nSamples] = size(Xe);
            else
                %%%% Create some Example Drifts
                [XeU, YeU] = EM_Brownian2(D, Fs, nSamples, nTraces/2,[90, 20, 0]);
                [XeD, YeD] = EM_Brownian2(D, Fs, nSamples, nTraces/2,[270, 20, 0]);
                Xe = [XeU; XeD];
                Ye = [YeU; YeD];
            end
            Xe = Xe - Xe(:,1);
            Ye = Ye - Ye(:,1);
            
            %% Run traces over the image
            clear iteration2
            counter = 1;
            numRepeatForMoreTrials = 1;
            for numRep = 1:numRepeatForMoreTrials
                for tracei = 1:nTraces
                    for ti = 1:nSamples
                        
                        x_eye_arcmin = Xe(tracei,ti); % current eye position
                        y_eye_arcmin = Ye(tracei,ti);
                        
                        
                        % get eye position in units of image pixels
                        x_eye_pixel = interp1(x_img_arcmin, 1:size_img, x_eye_arcmin, 'nearest');
                        y_eye_pixel = interp1(x_img_arcmin, 1:size_img, y_eye_arcmin, 'nearest');
                        
                        mv(:, :, counter) = img(...
                            y_eye_pixel + (-half_size_retina:half_size_retina),...
                            x_eye_pixel + (-half_size_retina:half_size_retina));
                        counter = counter + 1;
                    end
                end
            end
            %%
            %         static_power = mean(iteration2{1,1}.mv, 3);
            %         dynamic_power = var(iteration2{1,1}.mv, [], 3);
            
            static_power = mean(mv, 3);
            figure
            %             subplot(1,2,1)
            %             imagesc(static_power)
            %             xlim([60 100])
            %             ylim([60 100])
            % %             colormap gray
            %             caxis([-1 1])
            M = movmean(mv,100,3);
            %             axis square
            %             title('Static');
            
            for kk = 1:size(mv,3)
                dynamic_power(:,:,kk) = mv(:,:,kk) - M(:,:,kk); %static_power;%(M(:,:,kk)- mean(M(:,:,kk))); %var(mv, [], 3);
                
            end
            
            %             save(sprintf('./DP_%.0f.mat', D), 'dynamic_power')
            forMovieSim{d} = dynamic_power;
            %         dynamic_power = forMovieSim{2};
            dynamic_power = var(dynamic_power, [], 3);
            allNums{counterP}.dynamicPower = dynamic_power;
            counterP = counterP + 1;
            %             subplot(1,2,2)
            imagesc(x_img_arcmin, x_img_arcmin, dynamic_power);
            %             colormap(flipud(pink));
            %             colormap(flipud(gray))
            %             colormap((autumn))
            %             colormap((hsv))
            %             colormap(flipud(jet))
            %             colormap((jet))
            %             colormap((parula))
            %             J = customcolormap([0 0.5 1], {'#000000','#ffffff','#808080'});#ecd500, #ff0000
            %             J = customcolormap([0 0.80 1], {'#ffffff','#808080','#808080'});
            J = customcolormap([0 0.3 1], {'#ff0000','#ffffff','#808080'});
            
            colormap(J);
%             figure;
%             for i = 1:8
%                                 figure;
% %                 subplot(2,4,i)
%                 imagesc(x_img_arcmin, x_img_arcmin, allNums{i}.dynamicPower);
%                                 colorbar
%                 axis image;
%                 axis off;
%                 %                 title(sprintf('dynamic power: ~%.1farcmin/sec2',D));
%                 xlabel('horizontal (arcmin)');
%                 ylabel('vertical (arcmin)');
%                 J = customcolormap([0 0.3 1], {'#ff0000','#ffffff','#808080'});
%                 colormap(J);
%                 axis([-15 15 -15 15])
%                 if useRealTraces && nTraces > 50
%                     caxis([0 0.17])
%                 elseif useRealTraces && nTraces <= 50
%                     caxis([0 0.14])
%                 else
%                     caxis([0 0.14])
%                 end 
%                     saveas(gcf,sprintf(...
%                         '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/%iColoredImageOfRetinalMotionStimTraces%i.png',i, nTraces));
%             end
            
            
            colorbar
            axis image;
            axis off;
            title(sprintf('dynamic power: ~%.1farcmin/sec2',D));
            xlabel('horizontal (arcmin)');
            ylabel('vertical (arcmin)');
            %             colormap gray
            axis([-15 15 -15 15])
            if useRealTraces && nTraces > 50
                caxis([0 0.17])
            elseif useRealTraces && nTraces <= 50
                caxis([0 0.14])
            else
                caxis([0 0.14])
            end
            % % %         static_power = mean(mv, 3);
            % % %         for kk = 1:size(mv,3)
            % % %             dynamic_power(:,:,kk) = mv(:,:,kk) - static_power; %var(mv, [], 3);
            % % %          end
            % % %         dynamic_power = var(dynamic_power, [], 3);
            % % % %         averageStaticPower = mean(mean(static_power));
            %%%%double check with Janis each pixel subjtract the average...
            %         image_over_time = mean(img(Xe,Ye));
            
            %         figure();
            %         subplot(1,2,1)
            %         %         imagesc(x_img_arcmin, x_img_arcmin, static_power-dynamic_power);
            %         imagesc(x_img_arcmin, x_img_arcmin, static_power);
            %
            %         % %         subplot(1, 3, 1);
            %         %         imagesc(x_img_arcmin, x_img_arcmin, static_power);
            %         %         % colormap gray;
            %         colormap(flipud(pink));
            %         axis image;
            %         axis off;
            %         title('static power (mean); f = 0Hz');
            %         xlabel('horizontal (arcmin)');
            %         ylabel('vertical (arcmin)');
            %         axis([-15 15 -15 15])
            %
            %
            %         %         figure;
            %         subplot(1,2,2)
            %         imagesc(x_img_arcmin, x_img_arcmin, dynamic_power);
            %         colormap(flipud(pink));
            %         axis image;
            %         axis off;
            %         title(('dynamic power (var)'));
            %         xlabel('horizontal (arcmin)');
            %         ylabel('vertical (arcmin)');
            %         axis([-15 15 -15 15])
            %
            %
            %         subplot(2,2,3)
            % %         imagesc(x_img_arcmin, x_img_arcmin, dynamic_power - static_power);
            %         imagesc(x_img_arcmin, x_img_arcmin, static_power - dynamic_power);
            %
            %         colormap(flipud(pink));
            %         axis image;
            %         axis off;
            %         title(sprintf('static - dynamic'));
            %         xlabel('horizontal (arcmin)');
            %         ylabel('vertical (arcmin)');
            %         axis([-15 15 -15 15])
            %
            
            %         suptitle(sprintf('~%.1farcmin/sec2, 1.5arc Stim',D));
            
            
            % % %         Nsteps = 300;
            % % %         Mcell_temporal = MacaqueRetinaM_Temporal_BK(Nsteps);
            % % %         mv_Mcell = filter(Mcell_temporal, 1, mv, [], 3);
            % % %
            % % %         playRetinaInputAndResponseMovies(mv, mv_Mcell, x_img_arcmin, Xe, Ye, Fs);
            
            %         figure;
            %         subplot(1, 3, 3);
            %         imshow(img);
            %         title('input image');
            
            %
            %         figure();
            %         subplot(1, 2, 1);
            %         imagesc(x_img_arcmin, x_img_arcmin, static_power);
            %         colormap((pink));
            %         axis image;
            %         axis off;
            %         title('static power; f = 0Hz');
            %         xlabel('horizontal (arcmin)');
            %         ylabel('vertical (arcmin)');
            %         axis([-20 20 -20 20])
            %
            %
            %         subplot(1, 2, 2);
            % %         figure('Position',[200 200 500 400]);
            %         imagesc(x_img_arcmin, x_img_arcmin, dynamic_power);
            %         %         colormap(flipud(gray));
            %         %         colormap(hot);
            %         colormap(flipud(pink));
            %
            %         axis image;
            %                 axis off;
            %         title(sprintf('dynamic power: ~%.1farcmin/sec2',D));
            %         xlabel('horizontal (arcmin)');
            %         ylabel('vertical (arcmin)');
            %         axis([-20 20 -20 20])
            
            %             sgtitle(sprintf('RealTraces, %i Trials', nTraces))
            %         suptitle('Stimulus Size = 1.5arcmin');
%             if useRealTraces
%                 suptitle(sprintf('RealTraces, %i Trials', nTraces))
%                 saveas(gcf,sprintf(...
%                     '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/%i_%iImageOfRetinalMotionRealTraces%i.png',round(D), whichDigit, nTraces));
%                 %             saveas(gcf,sprintf(...
%                 %                 '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/%i_%iImageOfRetinalMotionCorrectSizeRealTracesDownSamp.epsc',round(D), whichDigit));
%             else
% %                 suptitle(sprintf('SimTraces, %i Trials', nTraces))
% %                 saveas(gcf,sprintf(...
% %                     '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/%i_%iImageOfRetinalMotionStimTraces%i.png',round(D), whichDigit, nTraces));
%                 %             saveas(gcf,sprintf(...
%                 %                 '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/%.1f%iImageOfRetinalMotionPinkZoom.epsc',D, whichDigit));
%             end
            clear iteration
        end
    end
end
% clc
% close all
% clear all

dynamic_power_5 = forMovieSim{1};
%
dynamic_power_20 = forMovieSim{2};

v = VideoWriter('movies/PowerSimReal_Final_bw.avi','Motion JPEG AVI');
figure
open(v)
for kk = 1:200 %size(dynamic_power,3)
    subplot(1,2,1)
    imagesc(dynamic_power_5(:,:,kk))
    xlim([50 100])
    ylim([50 100])
    axis 'square'
    axis off
    %     text(60,58,'5 DC')
    title('5 arcmin^2/sec');
    J = customcolormap([0 .45 1], {'#ffffff','#000000','#000000'});  
%     J = customcolormap([0 0.3 1], {'#ff0000','#ffffff','#808080'});       

%     colormap(J);
     caxis([-.5 .5])
    colorbar
%     colormap gray
%         colormap((bone));
    subplot(1,2,2)
    imagesc(dynamic_power_20(:,:,kk))
    %caxis([-.6 .6])
    xlim([50 100])
    ylim([50 100])
    %     text(60,58,'20 DC')
    title('20 arcmin^2/sec');
    axis 'square'
    axis off
    colormap(J);
     caxis([-.5 .5])
     colorbar
%     colormap gray
%         colormap((bone));
    drawnow
    frame = getframe(gcf);
    writeVideo(v,frame) 
end
close(v)


v = VideoWriter('movies/PowerSimReal_Final_bwr.avi','Motion JPEG AVI');
figure
open(v)
for kk = 1:200 %size(dynamic_power,3)
    subplot(1,2,1)
    imagesc(dynamic_power_5(:,:,kk))
    xlim([50 100])
    ylim([50 100])
    axis 'square'
    axis off
    %     text(60,58,'5 DC')
    title('5 arcmin^2/sec');
    J = customcolormap([0 .1 1], {'#ffffff','#808080','#000000'});
%       J = customcolormap([0 .15 1], {'#ff0000','#ffffff','#000000'}); %*** 
%     J = customcolormap([0 .75 1], {'#000000','#ffffff','#ff0000'});  
%     J = customcolormap([0 .75 1], {'#ffffff','#000000','#ff0000'});  
%     J = customcolormap([0 0.3 1], {'#ff0000','#ffffff','#808080'});       

%     colormap(J);
     caxis([0 .5])
    colorbar
%     colormap gray
%         colormap((bone));
    subplot(1,2,2)
    imagesc(dynamic_power_20(:,:,kk))
    %caxis([-.6 .6])
    xlim([50 100])
    ylim([50 100])
    %     text(60,58,'20 DC')
    title('20 arcmin^2/sec');
    axis 'square'
    axis off
    colormap(J);
     caxis([0 .5])
     colorbar
%     colormap gray
%         colormap((bone));
    drawnow
    frame = getframe(gcf);
    writeVideo(v,frame) 
end
close(v)




function digit = drawDigit(whichDigit, pixelwidth)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth + 1;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth + 1;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
end
end