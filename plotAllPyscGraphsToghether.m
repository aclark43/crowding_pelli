function thresholds = plotAllPyscGraphsToghether(subjects, condition, stabilization, em, ecc, c)

% for stabIdx = 1:length(stabilization)
for subCount = 1:length(subjects)
    counter = 0;
    numConditions = 1;
    for ii = 1:length(ecc)
        pathToData = sprintf('../../Data/%s/Graphs/FIG', subjects{subCount}); %File Location
        psychFileName = sprintf('%s/%s_%s_%s_%s_%s.fig',...
            pathToData,subjects{subCount}, condition{1}, stabilization{1}, em{1} ,ecc{ii});
        if exist(psychFileName, 'file') == 1
            continue;
        end
        thresholdFile = sprintf('MATFiles/%s_%s_%s_%s_%s_Threshold.mat',...
            subjects{subCount},condition{1},stabilization{1},em{1},ecc{ii});
        if exist(thresholdFile, 'file') == 2 && exist(psychFileName,'file') == 2
            titleFigs{numConditions} = sprintf('%s_%s_%s_%s_%s', ...
                subjects{subCount}, condition{1}, stabilization{1}, em{1}, ecc{ii});
            color = c(ii,:);
            fH{numConditions} = createGraphTogetherMultipleSubj...
                (titleFigs{numConditions},pathToData,color);
            counter = counter + 1;
            whichECC(counter) = ii;
        elseif strcmp('0eccEcc',ecc{ii})
                titleFigs{numConditions} = sprintf('%s_%s_%s_%s_0ecc', ...
                    subjects{subCount}, condition{1}, stabilization{1}, em{1});
                color = c(ii,:);
                fH{numConditions} = createGraphTogetherMultipleSubj...
                    (titleFigs{numConditions},pathToData,color);
                counter = counter + 1;
                whichECC(counter) = ii;
        else
            continue;
        end
        numConditions = numConditions + 1;
    end
    pathToData = ('../../Data/'); %File Location
    
    if counter == 1
        continue;
    end
    for ii = 1:counter
        ax = get(fH{ii}, 'Children');
        axP{ii} = get(ax(1),'Children');
        axSaved{ii} = ax;
    end
    
    for ii = 2:counter
        copyobj(axP{ii},axSaved{1})
    end
     
    %figures to keep
    figs2keep = [1];
    all_figs = findobj(0, 'type', 'figure');
    delete(setdiff(all_figs, figs2keep));
    
   
    title_str = sprintf('%s%s%s',subjects{subCount},stabilization{1}, condition{1});
    title(title_str);
    set(gca, 'FontSize', 11, 'FontWeight', 'bold');
    
    locText = .25:.05:1;
    for ii = 1:counter
        text(0.25,locText(ii),ecc{whichECC(ii)},...
            'Color',[c(ii,1) c(ii,2) c(ii,3)]);
    end
%     xlim([0 4]);
    saveas(gcf, sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/individualCurves/ECCAll%s%s%s.png',...
        subjects{subCount},stabilization{1}, condition{1}));
%     saveas(gcf, ...
%         sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/individualCurves/ECCAll%s%s%s.png',...
%         subjects{subCount},condition{1},stabilization{1}));
%     close all
   
    
%     saveas(gcf, sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/%sThresh%s.png',...
%         subjects{subCount},condition{1}));
    
    for ii = 1:counter
        currentS = (condition{1});
        fileName = sprintf('%s_Threshold.mat',titleFigs{ii});
        folderName = 'MATFiles/';
        file1 = fullfile(folderName, fileName);
        load(file1);
        %                                             subjectThresh(ii) = threshInfo;
        allThresh(ii) = threshInfo.thresh;
        
        thresholds(subCount).subject = subjects{subCount};
        thresholds(subCount).thresh(ii) = allThresh(ii);
        thresholds(subCount).numTrials(ii) = threshInfo.em.numValidTrials;
%         if strcmp('Stabilized',stabilization{1})
%             thresholds(subCount).stabilized(stabIdx) = 1;
%         else
%             thresholds(subCount).stabilized(stabIdx) = 0;
%         end
        k = strfind(titleFigs{ii},'Temp');
        if isempty(k)
            %Nasal = -
            thresholds(subCount).ecc(ii) = -(str2double(cell2mat(regexp(ecc{whichECC(ii)},'\d*','Match'))));
        elseif ~isempty(k)
            %Temp = +
            thresholds(subCount).ecc(ii) = (str2double(cell2mat(regexp(ecc{whichECC(ii)},'\d*','Match'))));
        end
    end
%     close all;
end
end
% end

