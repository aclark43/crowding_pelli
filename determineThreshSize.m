function [chooseTrialsS, chooseTrialsL] = determineThreshSize(ii,c)


% if c == 2
    if ii == 1
        chooseTrialsS = 8;
        chooseTrialsL = 6;
    elseif ii == 2
        chooseTrialsS = 7;
        chooseTrialsL = 5;
    elseif ii == 3
        chooseTrialsS = 2;
        chooseTrialsL = 2;
    elseif ii == 4
        chooseTrialsS = 6;
        chooseTrialsL = 1;
    elseif ii == 5
        chooseTrialsS = 6.5;
        chooseTrialsL = 1;
    elseif ii == 6
        chooseTrialsS = 7.5;
        chooseTrialsL = 4;
    elseif ii == 7
        chooseTrialsS = 4;
        chooseTrialsL = 1;
    elseif ii == 8
        chooseTrialsS = 7;
        chooseTrialsL = 1;
    elseif ii == 9
        chooseTrialsS = 8;
        chooseTrialsL = 1;
    elseif ii == 10
        chooseTrialsS = 4;
        chooseTrialsL = 1;
    elseif ii == 11
        chooseTrialsS = 3;
        chooseTrialsL = 1;
    elseif ii == 12
        chooseTrialsS = 3;
        chooseTrialsL = 1;
    elseif ii == 13
        chooseTrialsS = 3;
        chooseTrialsL = 1;
    else
        chooseTrialsS = 1;
        chooseTrialsL = 1;
    end
% elseif c == 1
%     chooseTrialsS = 1;
%     chooseTrialsL = 1;
% end
