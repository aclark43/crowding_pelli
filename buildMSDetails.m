function [allStartLocationMSx, allEndLocationMSx, allStartLocationMSy, allEndLocationMSy,...
   allAngle, allAmplitude, allID] = buildMSDetails(fixAllSW)

%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
allStartLocationMSx = [];
allEndLocationMSx = [];
allStartLocationMSy = [];
allEndLocationMSy = [];
allAngle = [];
allAmplitude = [];
allID = [];
for ii = 1:length(fixAllSW)
%     if ~isempty(fixAllSW{ii})
    allStartLocationMSx = [extractfield(fixAllSW(ii),'startLocationMSx')];
    allEndLocationMSx = [extractfield(fixAllSW(ii),'endLocationMSx')];
    allStartLocationMSy = [extractfield(fixAllSW(ii),'startLocationMSy')];
    allEndLocationMSy = [extractfield(fixAllSW(ii),'endLocationMSy')];
    allAngle = [extractfield(fixAllSW(ii),'MSangle')];
    allAmplitude = [extractfield(fixAllSW(ii),'MSamplitude')];
    allID = extractfield(fixAllSW(ii),'id');
%     end
end

 if length(allStartLocationMSx) > 10
    allStartLocationMSx = cell2mat(allStartLocationMSx(~cellfun('isempty',allStartLocationMSx)));
    allEndLocationMSx = cell2mat(allEndLocationMSx(~cellfun('isempty',allEndLocationMSx)));
    allStartLocationMSy = cell2mat(allStartLocationMSy(~cellfun('isempty',allStartLocationMSy)));
    allEndLocationMSy = cell2mat(allEndLocationMSy(~cellfun('isempty',allEndLocationMSy)));
%     allID = cell2mat(allID(~cellfun('isempty',allEndLocationMSx)));
 end
end

