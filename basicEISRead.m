%% load in some data
%%%%4 things commented that will influence DPI

clc
clear all

% condSession = {...
%     'ses1',...
%     };

% condSession = {...
%     'ses3',...
%     };

% condSession = {...
%   'ses3',...
%     'ses4',...
%     'ses5',...
%     'ses6',...
%     'Uncrowded_Unstabilized_15eccTemp\ses1',...
%     'Crowded_Unstabilized_15eccTemp\ses1',...
%     'Uncrowded_Unstabilized_25eccTemp\ses1',...
%     'Crowded_Unstabilized_25eccTemp\ses1',...
%     };

% condSession = {...
%     'Uncrowded_Unstabilized_10eccNasal\ses1',...
%     'Uncrowded_Unstabilized_15eccNasal\ses1',...
%     'Uncrowded_Unstabilized_25eccNasal\ses1',...
%     'Crowded_Unstabilized_10eccNasal\ses1',...
%     'Crowded_Unstabilized_15eccNasal\ses1',...
%     'Crowded_Unstabilized_25eccNasal\ses1',...
%    };

pt = 'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\Z005\Uncrowded_Stabilized_0ecc';
% pt = '../../Data/Z046';

for si = 1%:length(condSession)
    pathtodata = fullfile(pt);
    
    %%Prevents re-writing of pptrials if they've already been analyzed
    dir = sprintf('%s/pptrials.mat', pt);
%     if exist(dir, 'file') == 2
%         error('This pptrial already exists. Delete it first if you want to rewrite it.');
%     end
    
    fname1 = sprintf('pptrials.mat');
    
    
    fprintf('%s\n', fname1);
    
    % this is the raw read in of the data
    data = readdata(pathtodata, CalList());
    
    pptrials =  preprocessing(data);
    
    %%For Data that doesn't contain information on screen%%%
    for ii = 1:length(pptrials)
        pptrials{ii}.machine = {'DPI'};
        pptrials{ii}.pixelAngle = .2526;
        count.eccDist(ii) = double(pptrials{ii}.pixelAngle * pptrials{ii}.TargetEccentricity);
        
        if pptrials{ii}.Uncrowded == 1
            count.condition(ii) = {'Uncrowded'};
        elseif pptrials{ii}.Uncrowded == 0
            count.condition(ii) = {'Crowded'};
        end
        
        if isfield(pptrials{ii}, 'nasal')
            if pptrials{ii}.nasal == 1
                count.side(ii) = {'Nasal'};
            elseif pptrials{ii}.nasal == 0
                if count.eccDist(ii) == 0
                    count.side(ii) = {''};
                else
                    count.side(ii) = {'Temp'};
                end
            end
            eccName = 1;
        else
            count.side(ii) = {''};
            eccName = 0;
        end
        if pptrials{ii}.Unstab == 1
            count.stabil(ii) = {'Unstabilized'};
        elseif pptrials{ii}.Unstab == 0
            count.stabil(ii) = {'Stabilized'};
        end
        
    end
    [~, id1] = findgroups(count.condition);
        [~, id2] = findgroups(count.side);
        [~, id3] = findgroups(count.stabil);
        
        
        if length(id1) > 1
            error('There is more than one condition in here!')
        elseif length(id2) > 1
            error('There is more than one side in here!')
        elseif length(id3)>1
            error('There is more than one stabilization in here!')
        elseif length(unique(count.eccDist)) > 1
            error('There is more than one ecc in here!')
        end
        
%         if count.eccDist(ii) == 0
%             if eccName == 1
%                 newSavedFile = fullfile(sprintf('../../Data/%s/%s_%s_%seccEcc%s/%s', ...
%                     subject{1}, count.condition{ii}, count.stabil{ii}, ...
%                     string(count.eccDist(ii)), count.side{ii}, sesName{numSess}));
%             else
%                 newSavedFile = fullfile(sprintf('../../Data/%s/%s_%s_%secc%s/%s', ...
%                     subject{1}, count.condition{ii}, count.stabil{ii}, ...
%                     string(count.eccDist(ii)), count.side{ii}, sesName{numSess}));
%             end
%         else
%             newSavedFile = fullfile(sprintf('../../Data/%s/%s_%s_%secc%s/%s', ...
%                 subject{1}, count.condition{ii}, count.stabil{ii}, ...
%                 string(count.eccDist(ii)), count.side{ii}, sesName{numSess}));
%         end
%         
%         if ~strcmp(newSavedFile,pathtodata)
%             error('data name and type dont match')
%         end
%     eccDist = double(pptrials{1}.pixelAngle * pptrials{1, 1}.TargetEccentricity);
    
%     fprintf('\n In %s First trial is %s %s %s %i \n', ...
%         condSession{si}, count.condition{1}, count.side{1}, count.stabil{1}, count.eccDist(1));
    
    save(fullfile(pathtodata, fname1), 'pptrials', 'data');
    
    
    
end

