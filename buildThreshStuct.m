function  threshStruct = buildThreshStuct (currentSubj,threshInfo,currentCond, currentStab, currentEm, currentEcc)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

threshStruct.subject = currentSubj;
threshStruct.threshold = num2str(threshInfo.thresh);
threshStruct.threshSW4 = num2str(threshInfo.threshSW4);
threshStruct.sizesTestedX = {threshInfo.sizesTestedX};
threshStruct.performanceAtSizesY = {threshInfo.performanceAtSizesY};
threshStruct.crowding = currentCond;
threshStruct.stabilizing = currentStab;
threshStruct.eyemovements = currentEm;
threshStruct.eccentricity = currentEcc;
end

