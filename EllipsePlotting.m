figure; 
for ii = 1:length(subjectsAll)%find(control == 0)
    if control(ii)
        rotateAngle = 0;
    else
        rotateAngle = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
        if ii == 1
            rotateAngle = 315;
        elseif ii == 2
            rotateAngle = 50;
        elseif ii == 3
            rotateAngle = 145;
        elseif ii == 4
            rotateAngle = 90;
        elseif ii == 10
            rotateAngle = 90;
        elseif ii == 22
            rotateAngle = 315;
        elseif ii == 23
            rotateAngle = 90;
        end
        rotateAngle = 0;
    end
    vR = []; v = [];
    v = [fixStruct.X{ii,11};fixStruct.Y{ii,11}]';
    theta = -rotateAngle;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    vR = v*R;
    axisVal = 20;
    if control(ii)
        subplot(1,2,1)
        ellipseXY(vR(:,1)', vR(:,2)', 65, [173/255 173/255 173/255], 0)
        hold on
        axis([-axisVal axisVal -axisVal axisVal])
        
    else
        subplot(1,2,2)
%          heatMapIm = generateHeatMapSimple( ...
%             vR(:,1)', ...
%             vR(:,2)', ...
%             'Bins', 30,...
%             'StimulusSize', 1,...
%             'AxisValue', axisVal,...
%             'Uncrowded', 0,...
%             'Rotate', 0,...
%             'Borders', 1);
%         hold on
        ellipseXY(vR(:,1)', vR(:,2)', 65, [173/255 173/255 173/255], 0);% c(ii,:))
        axis([-axisVal axisVal -axisVal axisVal])
        hold on
    end

    plot(0,0,'o','MarkerSize',5,'LineWidth',3,'MarkerEdgeColor','k');
    %         line(h(1),[0 0],[-axisVal axisVal],'Color','k','LineWidth',1,'LineStyle',':')
    %         line(h(1),[-axisVal axisVal],[0 0],'Color','k','LineWidth',1,'LineStyle',':')
    line([0 0],[-axisVal axisVal],'Color','k','LineWidth',1,'LineStyle',':')
    line([-axisVal axisVal],[0 0],'Color','k','LineWidth',1,'LineStyle',':')
    
    axis square;
    
    set(gca,'XTick',[],'YTick',[],'xlabel',[],'ylabel',[])
    title(sprintf('%s',subjectsAll{ii}));
    %         axis off
    %         delete(h(2))
    vrAll{ii} = vR;
end

controlAllX = []; patientAllX = [];
controlAllY = []; patientAllY = [];

for ii = 1:length(subjectsAll)
    if control(ii)
        controlAllX = [controlAllX vrAll{ii}(:,1)'];
        controlAllY = [controlAllY vrAll{ii}(:,2)'];
    else
        patientAllX = [patientAllX vrAll{ii}(:,1)'];
        patientAllY = [patientAllY vrAll{ii}(:,2)'];
    end
end
subplot(1,2,1)
plot(mean(controlAllX),mean(controlAllY),'d','Color','m','MarkerFaceColor','m');
ellipseXY(controlAllX, controlAllY, 65, 'm', 3);% c(ii,:))
title('Controls')
subplot(1,2,2)
plot(mean(patientAllX),mean(patientAllY),'d','Color','m','MarkerFaceColor','m');
ellipseXY(patientAllX, patientAllY, 65, 'm', 3);% c(ii,:))

title('Patients');


 saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/EMandHisEllipse%s.png'...
        ,subjectsAll{ii}));
saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/EMandHisEllipse%s.epsc'...
        ,subjectsAll{ii}));
axisVal = 20;
    figure;
    subplot(1,2,1)
    heatMapIm = generateHeatMapSimple( ...
        controlAllX, ...
        controlAllY, ...
        'Bins', 60,...
        'StimulusSize', 1,...
        'AxisValue', axisVal,...
        'Uncrowded', 4,...
        'Rotate', 0,...
        'Borders', 1);
    hold on
        caxis([.1 1.15]);
    line([-40 40],[0 0],'Color','k','LineStyle','--');
    line([0 0],[-40 40],'Color','k','LineStyle','--');
    subplot(1,2,2)
    heatMapIm = generateHeatMapSimple( ...
        patientAllX, ...
        patientAllY, ...
        'Bins', 60,...
        'StimulusSize', 1,...
        'AxisValue', axisVal,...
        'Uncrowded', 4,...
        'Rotate', 0,...
        'Borders', 1);
    caxis([0.1 1.15]);
    line([-40 40],[0 0],'Color','k','LineStyle','--');
    line([0 0],[-40 40],'Color','k','LineStyle','--');
   
 saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/EMandCombMap%s.png'...
        ,subjectsAll{ii}));
saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/EMandCombMap%s.epsc'...
        ,subjectsAll{ii}));






