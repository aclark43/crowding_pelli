function [ Drifts, xValues, yValues ] = buildDriftStructFirst300(...
    sw, driftIdx, Drifts, params, pptrials, counter, strokeWidth, eccentricity, xValues, yValues, figures, title_str, trialChar, uEcc)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

for ii = driftIdx %for each drift trials in pptrials
    if strcmp('D',params.machine)
        timeOn = round(pptrials{ii}.TimeTargetON/(1000/330));
        %         timeOff = floor(min(pptrials{ii}.TimeTargetOFF/(1000/330), pptrials{ii}.ResponseTime/(1000/330)));
        timeOff = floor((min(pptrials{ii}.TimeTargetOFF/(1000/330), pptrials{ii}.ResponseTime/(1000/330)))/1.3);
    else
        timeOn = round(pptrials{ii}.TimeTargetON);
        %         timeOff = round(min(pptrials{ii}.TimeTargetOFF, pptrials{ii}.ResponseTime));
        timeOff = floor((min(pptrials{ii}.TimeTargetOFF, pptrials{ii}.ResponseTime))/1.3);
    end
    if length(pptrials{ii}.x.position) < timeOff
        timeOff = length(pptrials{ii}.x.position);
    end
    %% create (Drifts.(eccentricity).(strokeWidth).position(counter).x) and ''y
    if ~isempty(pptrials{ii}.microsaccades.start) %if there is a microsaccade in trace
        for msNum = 1:length(pptrials{ii}.microsaccades.start)
            if pptrials{ii}.microsaccades.start(msNum) > timeOn && ...
                    pptrials{ii}.microsaccades.start(msNum) < timeOff
                continue;
%                 msStartTime(msNum) = pptrials{ii}.microsaccades.start(msNum);
%                 msEndTime(msNum) = round(pptrials{ii}.microsaccades.start(msNum)+...
%                     pptrials{ii}.microsaccades.duration(msNum));
%                 
%                 Drifts.First300.(eccentricity).(strokeWidth).position(counter).x = ...
%                     [pptrials{ii}.x.position(timeOn:msStartTime(msNum)) + params.pixelAngle * pptrials{ii}.xoffset...
%                     pptrials{ii}.x.position(msEndTime(msNum):timeOff) + params.pixelAngle * pptrials{ii}.xoffset];
%                 xValues = [xValues, Drifts.First300.(eccentricity).(strokeWidth).position(counter).x];
%                 
%                 Drifts.First300.(eccentricity).(strokeWidth).position(counter).y = ...
%                     [pptrials{ii}.y.position(timeOn:msStartTime(msNum)) + params.pixelAngle * pptrials{ii}.xoffset...
%                     pptrials{ii}.y.position(msEndTime(msNum):timeOff) + params.pixelAngle * pptrials{ii}.xoffset];
%                 yValues = [yValues, Drifts.First300.(eccentricity).(strokeWidth).position(counter).y];
            else
                Drifts.First300.(eccentricity).(strokeWidth).position(counter).x = ...
                    [pptrials{ii}.x.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset];
                xValues = [xValues, Drifts.First300.(eccentricity).(strokeWidth).position(counter).x];
                
                Drifts.First300.(eccentricity).(strokeWidth).position(counter).y = ...
                    [pptrials{ii}.y.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset];
                yValues = [yValues, Drifts.First300.(eccentricity).(strokeWidth).position(counter).y];
            end
        end
    end
    
    Drifts.First300.(eccentricity).(strokeWidth).position(counter).x = pptrials{ii}.x.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset;
    xValues = [xValues, Drifts.First300.(eccentricity).(strokeWidth).position(counter).x];
    
    Drifts.First300.(eccentricity).(strokeWidth).position(counter).y = pptrials{ii}.y.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.yoffset;
    yValues = [yValues, Drifts.First300.(eccentricity).(strokeWidth).position(counter).y];
end
%%
%     driftStruct = Drifts.(eccentricity).(strokeWidth);
Drifts.First300.(eccentricity).(strokeWidth).id(counter) = ii;

Drifts.First300.(eccentricity).(strokeWidth).span(counter) = quantile(...
    sqrt((Drifts.First300.(eccentricity).(strokeWidth).position(counter).x-mean(Drifts.First300.(eccentricity).(strokeWidth).position(counter).x)).^2 + ...
    (Drifts.First300.(eccentricity).(strokeWidth).position(counter).y-mean(Drifts.First300.(eccentricity).(strokeWidth).position(counter).y)).^2), .95);%Drifts.span(ii);

Drifts.First300.(eccentricity).(strokeWidth).amplitude(counter) = sqrt((pptrials{ii}.x.position(timeOff) - pptrials{ii}.x.position(timeOn))^2 + ...
    (pptrials{ii}.y.position(timeOff) - pptrials{ii}.y.position(timeOn))^2);

Drifts.First300.(eccentricity).(strokeWidth).time(counter) = mean(timeOff-timeOn);
Drifts.First300.(eccentricity).(strokeWidth).timeTargetOnSamp = timeOn;
Drifts.First300.(eccentricity).(strokeWidth).timeTargetOffSamp = timeOff;
counter = counter +1;


% clear ii
for ii = 1:length(Drifts.First300.(eccentricity).(strokeWidth).position)
    position = Drifts.First300.(eccentricity).(strokeWidth).position(:,ii);
    %Single Segment
    if ~params.MS
        if strcmp('D',params.machine)
            posX = position.x(~isnan(position.x));
            posY = position.y(~isnan(position.y));
            [~,~,~,~,~, ...
                Drifts.First300.(eccentricity).(strokeWidth).singleSegmentDsq{ii},~,~] = ...
                CalculateDiffusionCoef(struct...
                ('x',posX, ...
                'y',posY,...
                'Fsampling', 341));
        else
            [~,~,~,~,~, ...
                Drifts.First300.(eccentricity).(strokeWidth).singleSegmentDsq{ii},~,~] = ...
                CalculateDiffusionCoef(struct('x',position.x, 'y', position.y, 'Fsampling', 1000));
        end
    end
    %%
    %Mean Amplitude
    Drifts.First300.(eccentricity).(strokeWidth).meanAmplitude(ii) = ...
        mean(Drifts.First300.(eccentricity).(strokeWidth).amplitude(ii));
    
    %STD Amplitude
    Drifts.First300.(eccentricity).(strokeWidth).stdAmplitude(ii) = ...
        std(Drifts.First300.(eccentricity).(strokeWidth).amplitude)/sqrt(length(Drifts.First300.(eccentricity).(strokeWidth).amplitude));
    
    %VELOCITY (& x & y) and SPEED
    [~, Drifts.First300.(eccentricity).(strokeWidth).velocity(ii), velX{ii}, velY{ii}] = ...
        CalculateDriftVelocity(position, 1);
    
    [~, instSpX, instSpY, mn_speed, driftAngle, curvature, varx, vary] = ...
        getDriftChar(position.x, position.y, 41, 1, 180);
    Drifts.First300.(eccentricity).(strokeWidth).curvature{ii} = curvature;
    Drifts.First300.(eccentricity).(strokeWidth).driftAngle{ii} = driftAngle;
    Drifts.First300.(eccentricity).(strokeWidth).mn_speed{ii} = mn_speed;
    Drifts.First300.(eccentricity).(strokeWidth).varX{ii}= varx;
    Drifts.First300.(eccentricity).(strokeWidth).varY{ii} = vary;
    if strcmp('D',params.machine)
        Drifts.First300.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:(250/(1000/330)));
        Drifts.First300.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:(250/(1000/330)));
    else
        Drifts.First300.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:250);
        Drifts.First300.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:250);
    end
    
    %Mean Span
    Drifts.First300.(eccentricity).(strokeWidth).meanSpan(ii) = (mean(Drifts.First300.(eccentricity).(strokeWidth).span));
    
    %STD of Span
    Drifts.First300.(eccentricity).(strokeWidth).stdSpan(ii) = ...
        std(Drifts.First300.(eccentricity).(strokeWidth).span)/...
        sqrt(length(Drifts.First300.(eccentricity).(strokeWidth).span));
end
Drifts.First300.(eccentricity).(strokeWidth).ccDistance = (round(2*sw*1.4))*params.pixelAngle;
Drifts.First300.(eccentricity).(strokeWidth).stimulusSize = double((2*sw)*params.pixelAngle);
%%

allPosX = [];
allPosY = [];
for ii = 1:length(Drifts.First300.(eccentricity).(strokeWidth).position)
    nonNanXPosIdx = ~isnan(Drifts.First300.(eccentricity).(strokeWidth).position(ii).x);
    nonNanXPosIdy = ~isnan(Drifts.First300.(eccentricity).(strokeWidth).position(ii).y);
    positionX = Drifts.First300.(eccentricity).(strokeWidth).position(ii).x(nonNanXPosIdx);
    positionY = Drifts.First300.(eccentricity).(strokeWidth).position(ii).y(nonNanXPosIdy);
    allPosX = [allPosX, positionX];
    allPosY = [allPosY, positionY];
end

if ~params.MS
    if strcmp('D',params.machine)
        [~,~,Drifts.First300.(eccentricity).(strokeWidth).dCoefDsq,~, ~, ...
            ~,~,~] = ...
            CalculateDiffusionCoef(struct(...
            'x',allPosX,...
            'y', allPosY),...
            'Fsampling', 341);
    else
        [~,~,Drifts.First300.(eccentricity).(strokeWidth).dCoefDsq,~, ~, ...
            ~,~,~] = ...
            CalculateDiffusionCoef(struct(...
            'x',allPosX,...
            'y', allPosY),...
            'Fsampling', 1000);
    end
end
%%
Drifts.First300.(eccentricity).(strokeWidth).meanAmplitude = mean(Drifts.First300.(eccentricity).(strokeWidth).amplitude);
Drifts.First300.(eccentricity).(strokeWidth).stdAmplitude = std(Drifts.First300.(eccentricity).(strokeWidth).amplitude)/sqrt(length(Drifts.First300.(eccentricity).(strokeWidth).amplitude));

Drifts.First300.(eccentricity).(strokeWidth).meanSpan = double(mean(Drifts.First300.(eccentricity).(strokeWidth).span));
Drifts.First300.(eccentricity).(strokeWidth).stdSpan = std(Drifts.First300.(eccentricity).(strokeWidth).span)/sqrt(length(Drifts.First300.(eccentricity).(strokeWidth).span));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp('D',params.machine)
    t = round(mean(Drifts.First300.(eccentricity).(strokeWidth).time*(1000/330)));
else
    t = round(mean(Drifts.First300.(eccentricity).(strokeWidth).time));
end

%% Checks for boundries and artifacts
if ~figures.FIXATION_ANALYSIS
    [ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/250)),31); %Checks strange artifacts
    [ xValues, yValues] = checkXYBound(xValues, yValues, 30); %Checks boundries
end
%% Create matrix for heatmap
limit.xmin = floor(min(xValues));
limit.xmax = ceil(max(xValues));
limit.ymin = floor(min(yValues));
limit.ymax = ceil(max(yValues));
n_bins = 100;

figure;
result = MyHistogram2(xValues, yValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
result = result./(max(max(result)));
%%
if ~params.MS
    if ~figures.FIXATION_ANALYSIS
        
        limit = generateHeatMap(30, sw, result, xValues, ...
            yValues, sw, title_str, trialChar, params, driftIdx, t, figures.FIXATION_ANALYSIS, uEcc, pptrials);
        
        figure;
        %Generates histogram of Spans
        generateSpanHist(Drifts.First300.(eccentricity).(strokeWidth).span, sw, title_str, trialChar);
        
        %Generates histogram of span amplitude
        generateDriftAmplitudeHist(Drifts.First300.(eccentricity).(strokeWidth).amplitude, sw, title_str, trialChar);
        
        %Generates heat map of velocity
        generateDriftVelocityMap(Drifts.First300.(eccentricity).(strokeWidth).instSpX, ...
            Drifts.First300.(eccentricity).(strokeWidth).instSpY, sw, params, title_str, trialChar)
        
        rangeXY = max(abs(cell2mat(struct2cell(limit))));
        %         F = GetTheFreq(result, rangeXY, n_bins);
        area = CalculateTheArea(xValues,yValues ,0.68, rangeXY, n_bins);
        Drifts.First300.(eccentricity).(strokeWidth).areaCovered = area;
        
        format longG
        areaRound = round(area,5);
        arcminSW = sw*2*params.pixelAngle;
        fprintf('Area for %s SW%i(actual arcmin %.3f) = %.3f\n', trialChar.Subject,sw,arcminSW,areaRound);
        
        saveas(gcf, sprintf('../../Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/%s_Drift_Anaylsis_First300.epsc', trialChar.Subject,trialChar.Subject));
        saveas(gcf, sprintf('../../Data/%s/Graphs/DriftAnalysis/JPEG/Span&Amplitude/%s_Drift_Anaylsis_First300.png', trialChar.Subject,trialChar.Subject));
        saveas(gcf, sprintf('../../Data/%s/Graphs/DriftAnalysis/FIG/Span&Amplitude/%s_Drift_Anaylsis_First300.fig', trialChar.Subject,trialChar.Subject));
    end
end

close all




end