%Classifies coordinates on the face into boxes/regions
% xx,yy are the coordinates

function [Classification,idTarget,idRightFlanker,idLeftFlanker,idTopFlanker,idBottomFlanker,idBackground]...
    = ClassifyEM(xx,yy,Coordinates)


Classification = zeros(1,length(xx));

idTarget = find((Coordinates.centerTarget_x1<xx) & (xx<Coordinates.centerTarget_x2) & ...
    (Coordinates.centerTarget_y2>yy) & (yy>Coordinates.centerTarget_y1));

idRightFlanker = find((Coordinates.rightFlanker_x1<xx) & (xx<Coordinates.rightFlanker_x2) & ...
    (Coordinates.rightFlanker_y2>yy) & (yy>Coordinates.rightFlanker_y1));

idLeftFlanker = find((Coordinates.leftFlanker_x1<xx) & (xx<Coordinates.leftFlanker_x2) & ...
    (Coordinates.leftFlanker_y2>yy) & (yy>Coordinates.leftFlanker_y1));

idTopFlanker = find((Coordinates.topFlanker_x1<xx) & (xx<Coordinates.topFlanker_x2) & ...
    (Coordinates.topFlanker_y2>yy) & (yy>Coordinates.topFlanker_y1));

idBottomFlanker = find((Coordinates.bottomFlanker_x1<xx) & (xx<Coordinates.bottomFlanker_x2) & ...
    (Coordinates.bottomFlanker_y2>yy) & (yy>Coordinates.bottomFlanker_y1));

if ~isempty(idTarget)
    Classification(idTarget) = 1;
end
if ~isempty(idRightFlanker)
    Classification(idRightFlanker) = 2;
end
if ~isempty(idLeftFlanker)
    Classification(idLeftFlanker) = 3;
end
if ~isempty(idTopFlanker)
    Classification(idTopFlanker) = 4;
end
if ~isempty(idBottomFlanker)
    Classification(idBottomFlanker) = 5;
end
idBackground=find(Classification==0);

