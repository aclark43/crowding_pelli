function [acrossSubStats,dataAll] = calculatePerformanceAnalysisAcuityPelli(data, idxName, validForAnalyis, manCheck)

for e = 1:length(idxName)   
    currentIdxName = idxName{e};
%     directy = data.(currentIdxName).eis_data.user_data.variables;
%     allTrialNames = fieldnames(directy);
%     idxDir = startsWith(allTrialNames,"trial");
%     allX = data.(currentIdxName).eis_data.eye_data.eye_1.calibrated_x;
%     allY = data.(currentIdxName).eis_data.eye_data.eye_1.calibrated_y;
    counter = 1;
    if manCheck == 1
        direc = data.(currentIdxName).pptrials;
    else
        direc = data.(currentIdxName);
    end
    for ii = 1:length(direc)
        
        if ~isfield(direc{ii},'x')
            dataAll{e}.correct(ii) = 4;
            dataAll{e}.response(ii) = 2000;
            continue;
        end
        dataAll{e}.sw(counter) = double(direc{ii}.TargetStrokewidth);
        if ~isfield(direc{ii},'pixelAngle')
            dataAll{e}.pixelAngle(counter) = 0.2163;
        else
            dataAll{e}.pixelAngle(counter) = direc{ii}.pixelAngle;
        end
        dataAll{e}.response(ii) = direc{ii}.Response;
        dataAll{e}.correct(ii) = direc{ii}.Correct;
        % SW 1 = 5 pixels wide.
        dataAll{e}.spaceE(counter) = double(dataAll{e}.sw(counter))*dataAll{e}.pixelAngle(counter);
        dataAll{e}.snellenAcuity(counter) = (2*dataAll{e}.spaceE(counter))/2;
        dataAll{e}.logMar(counter) = dataAll{e}.snellenAcuity(counter)*20;
%         idxOn = direc{ii}.FrameTargetON;
%         idxOff = direc{ii}.FrameTargetOFF; 
        dataAll{e}.x{counter} = direc{ii}.x;
        dataAll{e}.y{counter} = direc{ii}.y;
        counter = counter + 1;
    end
%     close all
    figure(e);
    idxTrials = find(dataAll{e}.correct < 3 & validForAnalyis{e});
    dataAll{e}.idxTrials = idxTrials;
    acrossSubStats.numTrials(e) = length(idxTrials);
    
    [thresh{e}, par{e}, threshB{e}, xi{e}, yi{e}, chiSq{e}] = ...
        psyfitCrowding(dataAll{e}.spaceE(idxTrials), ...
        dataAll{e}.correct(idxTrials), 'DistType', ...
        'Normal','Chance', .25, 'Extra');
    
    xlabel('Size of Gap');
    acrossSubStats.thresh(e) = thresh{e};
    acrossSubStats.snellAcuity(e) = thresh{e}*20; %thresh is gap size
    acrossSubStats.freqThresh(e) = (60/(thresh{e}*2));
    acrossSubStats.ecc(e) = str2double(cell2mat(regexp(currentIdxName,'\d*','Match')));
%     freqThresh(e) = 60/(20/snellAcuity(e));
    title(sprintf('%s, Threshold = 20/%.0f MAR',(currentIdxName),thresh{e}*20))
end




end