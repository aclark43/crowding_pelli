temp = Tiff('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Adaptive Optics\ConeCounting\GoldStandardFiles\FovealMosaicImages\P01_L_refImage.tif');

imageData = read(temp);
textFile = readtable('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Adaptive Optics\ConeCounting\GoldStandardFiles\Data_txt_files\P01_L_cone_locations.txt');



figure;
imshow(imageData);
hold on
plot(textFile.Var1,textFile.Var2,'.');

[65,89] %top left
[652, 91] % top right
[648,630]; %bottom right
[67,621]; %bottom left

