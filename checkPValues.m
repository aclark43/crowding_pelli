function checkPValues (subjectUnc, subjectCro, subjectsAll, threshDSQUncr, threshDSQCro,...
     threshCurvUncr, threshCurvCro, threshVelUncr, threshVelCro)
 
[ pValueTable ] = findSpanAcrossSWPValue( subjectUnc, subjectCro, subjectsAll );

Name = subjectsAll';
DSQ_mean_Uncrowded = threshDSQUncr';
DSQ_mean_Crowded = threshDSQCro';
dsqTable = table(Name,DSQ_mean_Uncrowded, DSQ_mean_Crowded);
[~,p] = ttest2(threshDSQUncr,threshDSQCro)

Curvature_mean_Uncrowded = threshCurvUncr';
Curvature_mean_Crowded = threshCurvCro';
curveTable = table(Name,Curvature_mean_Uncrowded, Curvature_mean_Crowded);
[~,p] = ttest2(threshCurvUncr,threshCurvCro)

Velocity_mean_Uncrowded = threshVelUncr';
Velocity_mean_Crowded = threshVelCro';
velTable = table(Name,Velocity_mean_Uncrowded, Velocity_mean_Crowded);
[~,p] = ttest2(threshVelUncr,threshVelCro)

end