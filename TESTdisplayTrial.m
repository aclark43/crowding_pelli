% This function allows editing all information relative to a single trial.
% 
% Syntax:
%   TrialList = displayTrial(TrialList, ...
%      'PropertyName', 'PropertyValue' ...)
%   Trial = displayTrial(Trial, 'PropertyName', 'PropertyValue' ...)
%
% TrialList : Structure containing a list of trials
% Trial     : Structure containing information about a trial
% Properties: Optional properties
%               'trial' : Trial number
%               'start' : First trial
%               'events': Display a particular list of events
%               'xcolor': Color for the X-trace (default: [0 0 200])
%               'ycolor': Color for the Y-trace (default: [0 180 0])
%               'vcolor': Color for the velocity trace (default: [255 0 0])
%               'ecolor': Color for the specified events
%                         (default: [80 81 80])
%               'acolor': Color for the analysis period
%                         (default: [240 240 240])
%               'scolor': Color for the saccades (default: [255 175 100])
%               'mcolor': Color for the microsaccades 
%                         (default: [222 125 0])
%               'dcolor': Color for the drifts (default: [210 237 216]
%               'bcolor': Color for the blinks (default: [250 214 214])
%               'ncolor': Color for the no-tracks (default: [183 183 219])
%               'icolor': Color for the invalid events (default: [255 0 0])
%
function displayTrial(Trials, varargin)

  if ~isaTrial(Trials) && ~isaTrialList(Trials)
    p_ematError(6, 'displayTrial');
  end

  % Set the dimension of the plots
  g_PlotSizeX = .8;
  g_PlotEMSizeY = .65;
  g_PlotVSizeY = .1;

  g_PlotOriginX = .1;
  g_PlotVOriginY = .05;
  g_PlotEMOriginY = g_PlotVOriginY + g_PlotVSizeY + .05;
  g_ButtonsOriginY = g_PlotEMOriginY + g_PlotEMSizeY + .025;

  % Definition of the default color
  g_XColor = [0 0 200];
  g_YColor = [0 180 0];
  g_VColor = [255 0 0];
  g_EColor = [80 81 80];
  g_AColor = [240 240 240];
  g_SColor = [255 175 100]; 
  g_MColor = [222 125 0];
  g_DColor = [210 237 216];
  g_BColor = [250 214 214];
  g_NColor = [183 183 219];
  g_IColor = [255 0 0];

  g_events = [];

  g_currentTrial = 1;
  if isaTrialList(Trials)
    g_multiTrial = 'on';
  else
    g_multiTrial = 'off';
  end

  % Interpret the user parameters
  k = 1;
  while k <= length(varargin) && ischar(varargin{k})
    switch (lower(varargin{k}))
      case 'trial'
        g_currentTrial = varargin{k + 1};
        k = k + 1;
        g_multiTrial = 'off';
      case 'start'
        g_currentTrial = varargin{k + 1};
        k = k + 1;
      case 'events'
        g_events = varargin{k + 1};
        k = k + 1;
      case 'xcolor'
        g_XColor = varargin{k + 1};
        k = k + 1;
      case 'ycolor'
        g_YColor = varargin{k + 1};
        k = k + 1;
      case 'acolor'
        g_AColor = varargin{k + 1};
        k = k + 1;
      case 'scolor'
        g_SColor = varargin{k + 1};
        k = k + 1;
      case 'mcolor'
        g_MColor = varargin{k + 1};
        k = k + 1;
      case 'dcolor'
        g_DColor = varargin{k + 1};
        k = k + 1;
      case 'bcolor'
        g_BColor = varargin{k + 1};
        k = k + 1;
      case 'ncolor'
        g_NColor = varargin{k + 1};
        k = k + 1;
      case 'icolor'
        g_IColor = varargin{k + 1};
        k = k + 1;
      otherwise
        p_ematError(3, 'editTrial');
    end
    k = k + 1;
  end

  % Initialize the selection array
  g_SelectedEvent = [];
  g_SelectedEventType = [];
  g_SelectedObjects = [];
  g_MultiSelection = false;

  % Initialize the handles to the graphic objects
  % associated with the events
  g_SObjects = [];
  g_MObjects = [];
  g_DObjects = [];
  g_BObjects = [];
  g_NObjects = [];
  g_IObjects = [];

  g_DisplayAll = true;
  g_DisplaySaccades = false;
  g_DisplayMSaccades = false;
  g_DisplayDrifts = false;
  g_DisplayInvalid = false;
  g_DisplayBlinkNoTrack = false;

  % Create the main window
  g_GUIBackgroundColor = [0.85 0.85 0.85];
  MainFigure = figure('Visible', 'on', ...
    'Name', 'Display Trial', ...
    'Position', [0 0 1 1], ...
    'NumberTitle', 'off', ...
    'Toolbar', 'figure', ...
    'Resize', 'on', 'Color', g_GUIBackgroundColor,...
    'Units', 'normalized');
  set(MainFigure, 'PaperPositionMode','auto');
  set(MainFigure, 'InvertHardcopy','off');
  set(MainFigure, 'WindowButtonDownFcn', {@p_eventMouse});
  set(MainFigure, 'KeyPressFcn', {@p_eventKeyboard});
  set(MainFigure, 'KeyReleaseFcn', {@p_eventKeyboard});

  % Velocity plot
  VPlot = axes('Parent', MainFigure, 'Position', ...
    [g_PlotOriginX g_PlotVOriginY g_PlotSizeX g_PlotVSizeY], ...
    'Box', 'On', 'Clipping', 'On', 'Units', 'normalized');

  % Show the movements
  EMPlot = axes('Parent', MainFigure, 'Position', ...
    [g_PlotOriginX g_PlotEMOriginY g_PlotSizeX g_PlotEMSizeY], ...
    'Box', 'On', 'Clipping', 'On', 'Units', 'normalized');

  % Create all check boxe components
  cbSaccades = uicontrol(MainFigure, 'Style', 'checkbox', ...
    'String', 'Saccades',...
    'Value', g_DisplaySaccades, ...
    'Units', 'normalized',...
    'Position', [g_PlotOriginX g_ButtonsOriginY + .06 .16 .02], ...
    'BackgroundColor', g_GUIBackgroundColor, ...
    'Callback', {@cbEvents_Callback});

  cbMSaccades = uicontrol(MainFigure, 'Style', 'checkbox', ...
    'String', 'Microsaccades',...
    'Value', g_DisplayMSaccades, ...
    'Units', 'normalized',...
    'Position', [g_PlotOriginX g_ButtonsOriginY + .03 .16 .02], ...
    'BackgroundColor', g_GUIBackgroundColor, ...
    'Callback', {@cbEvents_Callback});

  cbDrifts = uicontrol(MainFigure, 'Style', 'checkbox', ...
    'String', 'Drifts',...
    'Value', g_DisplayDrifts, ...
    'Units', 'normalized',...
    'Position', [g_PlotOriginX g_ButtonsOriginY .16 .02], ...
    'BackgroundColor', g_GUIBackgroundColor, ...
    'Callback', {@cbEvents_Callback});

  cbBlinkNoTrack = uicontrol(MainFigure, 'Style', 'checkbox', ...
    'String', 'Blink, No-Track',...
    'Value', g_DisplayBlinkNoTrack, ...
    'Units', 'normalized',...
    'Position', [g_PlotOriginX + .2 g_ButtonsOriginY + .06 .16 .02], ...
    'BackgroundColor', g_GUIBackgroundColor, ...
    'Callback', {@cbEvents_Callback});

  cbInvalid = uicontrol(MainFigure, 'Style', 'checkbox', ...
    'String', 'Invalid',...
    'Value', g_DisplayInvalid, ...
    'Units', 'normalized',...
    'Position', [g_PlotOriginX + .2 g_ButtonsOriginY + .03 .16 .02], ...
    'BackgroundColor', g_GUIBackgroundColor, ...
    'Callback', {@cbEvents_Callback});

  cbAll = uicontrol(MainFigure, 'Style', 'checkbox', ...
    'String', 'All movements',...
    'Value', g_DisplayAll, ...
    'Units', 'normalized',...
    'Position', [g_PlotOriginX + .2 g_ButtonsOriginY .16 .02], ...
    'BackgroundColor', g_GUIBackgroundColor, ...
    'Callback', {@cbEvents_Callback});

  pbPrevTrial = uicontrol(MainFigure, 'Style', 'pushbutton', ...
    'String', 'Prev Trial', ...
    'Enable', 'off', ...
    'BackgroundColor', g_GUIBackgroundColor, ...
    'Units', 'normalized',...
    'Position', [g_PlotOriginX + .6 g_ButtonsOriginY .1 .03], ...
    'Callback', {@pbCommands_Callback});

  pbNextTrial = uicontrol(MainFigure, 'Style', 'pushbutton', ...
    'String', 'Next Trial', ...
    'Enable', 'off', ...
    'BackgroundColor', g_GUIBackgroundColor, ...
    'Units', 'normalized',...
    'Position', [g_PlotOriginX + .6 g_ButtonsOriginY + .03 .1 .03], ...
    'Callback', {@pbCommands_Callback});

  % Create the edit text components of the current trial
  stTrialNumber = uicontrol(MainFigure, 'Style', 'edit', ...
    'String', sprintf('%d', g_currentTrial), ...
    'Enable', g_multiTrial, ...
    'HorizontalAlignment', 'Center', ...
    'Units', 'normalized',...
    'Position', ...
    [g_PlotOriginX + .7 g_ButtonsOriginY + .025 .05 .025],...
    'Callback', {@editEvent_Callback});

  % Create the description of the selected event
  stEventInfo = uicontrol(MainFigure, 'Style', 'text', ...
    'String', 'No event selected.', ...
    'HorizontalAlignment', 'Left', ...
    'Units', 'normalized',...
    'Position', ...
    [g_PlotOriginX g_ButtonsOriginY + .08 g_PlotSizeX .025]);

  if g_currentTrial > 1 && strcmp(g_multiTrial, 'on')
    set(pbPrevTrial, 'Enable', 'on');
  end
  if g_currentTrial < length(Trials) && strcmp(g_multiTrial, 'on')
    set(pbNextTrial, 'Enable', 'on');
  end

  % Initialize input and output
  g_ResultTrials = Trials;

  if isaTrialList(Trials)
    g_Trial = Trials{g_currentTrial};
  else
    g_Trial = Trials;
  end

  % Refresh the figure using all the global settings
  p_refreshFigure();

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Callback function of the push buttons
  %
  function pbCommands_Callback(hObject, eventdata) %#ok<INUSD>

    switch (hObject)
      case pbPrevTrial
        % Change trial
        g_ResultTrials{g_currentTrial} = g_Trial;
        g_currentTrial = g_currentTrial - 1;
        g_Trial = g_ResultTrials{g_currentTrial};

        % Check the limits
        if g_currentTrial == 1
          set(pbPrevTrial, 'Enable', 'off');
        end
        set(pbNextTrial, 'Enable', 'on');
        set(stTrialNumber, 'String', sprintf('%d', g_currentTrial));

      case pbNextTrial
        % Change trial
        g_ResultTrials{g_currentTrial} = g_Trial;
        g_currentTrial = g_currentTrial + 1;
        g_Trial = g_ResultTrials{g_currentTrial};

        % Check the limits
        if g_currentTrial == length(g_ResultTrials)
          set(pbNextTrial, 'Enable', 'off');
        end
        set(pbPrevTrial, 'Enable', 'on');
        set(stTrialNumber, 'String', sprintf('%d', g_currentTrial));
    end

    % Initialize the selection array
    g_SelectedEvent = [];
    g_SelectedEventType = [];
    g_SelectedObjects = [];

    p_refreshFigure();
  end

  function editEvent_Callback(hObject, eventdata) %#ok<INUSD>
      n = str2double(get(hObject, 'String'));
      
      % Check the limits
      if n < 1 || n > length(g_ResultTrials)
          set(hObject, 'String', sprintf('%d', g_currentTrial));
          p_refreshFigure();
          return;
      end
      
      % Change trial
      g_ResultTrials{g_currentTrial} = g_Trial;
      g_currentTrial = n;
      g_Trial = g_ResultTrials{g_currentTrial};
      
      if g_currentTrial == 1
          set(pbPrevTrial, 'Enable', 'off'); 
          set(pbNextTrial, 'Enable', 'on');
      elseif g_currentTrial == length(g_ResultTrials)
          set(pbPrevTrial, 'Enable', 'on'); 
          set(pbNextTrial, 'Enable', 'off');
      else
          set(pbPrevTrial, 'Enable', 'on'); 
          set(pbNextTrial, 'Enable', 'on');
      end
      
      % Initialize the selection array
      g_SelectedEvent = [];
      g_SelectedEventType = [];
      g_SelectedObjects = [];
      
      p_refreshFigure();
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Callback function of the check-box components
  %
  function cbEvents_Callback(hObject, eventdata) %#ok<INUSD>

    % Verify if the check box is checked
    Check = get(hObject, 'Value') == get(hObject, 'Max');

    % Initialize all check boxes
    if hObject ~= cbAll && Check
      g_DisplayAll = false;
      set(cbAll, 'Value', get(hObject, 'Min'));
    end

    switch(hObject)
      case cbAll
        g_DisplayAll = Check;

        g_DisplaySaccades = false;
        set(cbSaccades, 'Value', get(hObject, 'Min'));

        g_DisplayMSaccades = false;
        set(cbMSaccades, 'Value', get(hObject, 'Min'));

        g_DisplayDrifts = false;
        set(cbDrifts, 'Value', get(hObject, 'Min'));

        g_DisplayBlinkNoTrack = false;
        set(cbBlinkNoTrack, 'Value', get(hObject, 'Min'));

        g_DisplayInvalid = false;
        set(cbInvalid, 'Value', get(hObject, 'Min'));
      case cbSaccades
        g_DisplaySaccades = Check;
      case cbMSaccades
        g_DisplayMSaccades = Check;
      case cbDrifts
        g_DisplayDrifts = Check;
      case cbBlinkNoTrack
        g_DisplayBlinkNoTrack = Check;
      case cbInvalid
        g_DisplayInvalid = Check;
    end

    % Refresh the figure according the the selections
    p_refreshFigure();
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Callback function of the event keyboard
  %
  function p_eventKeyboard(src, eventdata) %#ok<INUSL>

    if length(eventdata.Modifier) == 1 && ...
       strcmp(eventdata.Modifier{1}, 'shift')
      g_MultiSelection = true;
    else
      g_MultiSelection = false;
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Callback function of the event mouse
  %
  function p_eventMouse(src, eventdata) %#ok<INUSD>

    % Calculate the position of the mouse with rispect to the e.m. plot
    MousePosition = get(MainFigure, 'CurrentPoint');
    if (MousePosition(1) > g_PlotOriginX && ...
        MousePosition(1) < g_PlotOriginX + g_PlotSizeX) && ...
       (MousePosition(2) > g_PlotEMOriginY && ...
        MousePosition(2) < g_PlotEMOriginY + g_PlotEMSizeY)
      ClickInside = true;
    else
      ClickInside = false;
    end

    if ClickInside
      Limits = get(EMPlot, 'XLim');
      t = Limits(1) + (MousePosition(1) - g_PlotOriginX) * ...
        ((Limits(2) - Limits(1)) / g_PlotSizeX);

      Type = ''; %#ok<NASGU>
      Object = -1; %#ok<NASGU>

      % Try to find the event in the drifts
      P = p_selectEvent(g_Trial.drifts, t);
      if P > 0
        [Start, End, Duration, Amplitude, Angle, MinV, MaxV] = ...
          getEventsInfo(g_Trial, g_Trial.drifts, P);
        set(stEventInfo, 'String', ...
          sprintf('%.0f ms: [%d - %d ms]   Drift #%d - Duration = %d ms, Amplitude = %.2f arcmin, Angle = %.2f deg, Min Vel = %.2f arcmin/s, Max Vel = %.2f arcmin/s', ...
            t, Start, End, P, Duration, Amplitude, ...
            rad2deg(Angle), MinV, MaxV));
        Type = 'd';
        Object = g_DObjects(P);
      else 

        % Try to find the event in the saccades
        P = p_selectEvent(g_Trial.saccades, t);
        if P > 0
          [Start, End, Duration, Amplitude, Angle, MinV, MaxV] = ...
            getEventsInfo(g_Trial, g_Trial.saccades, P);
          set(stEventInfo, 'String', ...
            sprintf('%.0f ms: [%d - %d ms]   Saccade #%d - Duration = %d ms, Amplitude = %.2f arcmin, Angle = %.2f deg, Min Vel = %.2f arcmin/s, Max Vel = %.2f arcmin/s', ...
              t, Start, End, P, Duration, Amplitude, ...
              rad2deg(Angle), MinV, MaxV));
          Type = 's';
          Object = g_SObjects(P);
        else

          % Try to find the event in the microsaccades
          P = p_selectEvent(g_Trial.microsaccades, t);
          if P > 0
            [Start, End, Duration, Amplitude, Angle, MinV, MaxV] = ...
              getEventsInfo(g_Trial, g_Trial.microsaccades, P);
            set(stEventInfo, 'String', ...
              sprintf('%.0f ms: [%d - %d ms]   Microsaccade #%d - Duration = %d ms, Amplitude = %.2f arcmin, Angle = %.2f deg, Min Vel = %.2f arcmin/s, Max Vel = %.2f arcmin/s', ...
                t, Start, End, P, Duration, Amplitude, ...
                rad2deg(Angle), MinV, MaxV));
            Type = 'm';
            Object = g_MObjects(P);
          else

            % Try to find the event in the blink
            P = p_selectEvent(g_Trial.blinks, t);
            if P > 0
              [Start, End, Duration] = getEventsInfo(g_Trial.blinks, P);
              set(stEventInfo, 'String', ...
                sprintf('%.0f ms: [%d - %d ms]   Blink #%d - Duration = %d ms', ...
                  t, Start, End, P, Duration));
              Type = 'b';
              Object = g_BObjects(P);
            else

              % Try to find the event in the notrack
              P = p_selectEvent(g_Trial.notracks, t);
              if P > 0
                [Start, End, Duration] = getEventsInfo(g_Trial.notracks, P);
                set(stEventInfo, 'String', ...
                  sprintf('%.0f ms: [%d - %d ms]   No-track #%d - Duration = %d ms', ...
                    t, Start, End, P, Duration));
                Type = 'n';
                Object = g_NObjects(P);
              else

                % Try to find the event in the invalid
                P = p_selectEvent(g_Trial.invalid, t);
                if P > 0
                  [Start, End, Duration] = ...
                    getEventsInfo(g_Trial.invalid, P);
                  set(stEventInfo, 'String', ...
                    sprintf('%.0f ms: [%d - %d ms]   Invalid #%d - Duration = %d ms', ...
                      t, Start, End, P, Duration));
                  Type = 'i';
                  Object = g_IObjects(P);
                else

                  set(stEventInfo, 'String', ...
                    sprintf('No event selected at time %.0fms', t));
                  return;
                end
              end
            end
          end
        end
      end

      % Before storing the event verify if
      % it is not on the list of selected events
      Present = (g_SelectedEvent == P) & (g_SelectedEventType == Type);
      EventPosition = find(Present, 1);
      Present = sum(Present) > 0;

      if Present 

        % Re-establish the original color
        p_changeColor(Object, Type);

        % Delete the event from the list of selected objects
        g_SelectedEvent(EventPosition) = [];
        g_SelectedEventType(EventPosition) = [];
        g_SelectedObjects(EventPosition) = [];
      else

        % Reset the selection
        if ~g_MultiSelection, p_resetSelection(); end

        % Store the event number and its type
        g_SelectedEvent = [g_SelectedEvent P];
        g_SelectedEventType = [g_SelectedEventType Type];
        g_SelectedObjects = [g_SelectedObjects Object];

        p_changeColor(Object, 'h');
      end
    else

      % Reset the selection
      p_resetSelection();
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Refresh the figure using all the global settings
  %
  function p_refreshFigure()

    hold on;

    t = 1:g_Trial.samples;

    % Select the velocity plot
    axes(VPlot);
    cla;

    plot(t, g_Trial.velocity, 'Color', g_VColor / 255, 'HitTest', 'off');
    xlabel('Time [ms]');
    ylabel('Velocity [arcmin/s]');

    % Select the eye movements plot
    axes(EMPlot);
    cla;

    % Draw the movements the first time to set the 
    % scale of the axis
    plot(t, g_Trial.x.position, 'Color', g_XColor / 255, 'HitTest', 'off');
    plot(t, g_Trial.y.position, 'Color', g_YColor / 255, 'HitTest', 'off');

    % Display the analysis area
    p_displayEvents(EMPlot, g_Trial.analysis, g_AColor / 255, 'none', 0);

    % Display all events

    % Display the saccades if requested
    if g_DisplayAll || g_DisplaySaccades
       g_SObjects = p_displayEvents(EMPlot, g_Trial.saccades, ...
          g_SColor / 255, 'none', false);
    end

    % Display the microsaccades if requested
    if g_DisplayAll || g_DisplayMSaccades
      g_MObjects = p_displayEvents(EMPlot, g_Trial.microsaccades, ...
        g_MColor / 255, 'none', false);
    end

    % Display the drifts if requested
    if g_DisplayAll || g_DisplayDrifts
      g_DObjects = p_displayEvents(EMPlot, g_Trial.drifts, ...
        g_DColor / 255, 'none', false);
    end

    % Display the blink and no-track events if requested
    if g_DisplayAll || g_DisplayBlinkNoTrack
      g_BObjects = p_displayEvents(EMPlot, g_Trial.blinks, ...
        g_BColor / 255, 'none', false);

      % Display losses of track
      g_NObjects = p_displayEvents(EMPlot, g_Trial.notracks, ...
        g_NColor / 255, 'none', false);
    end

    % Display the invalid events if requested
    if g_DisplayAll || g_DisplayInvalid

      g_IObjects = p_displayEvents(EMPlot, g_Trial.invalid, ...
        g_IColor / 255, 'none', false);
    end

    if ~isempty(g_events)
      p_displayEvents(EMPlot, g_events, g_EColor / 255, 'none', false);
    end

    % Overimpose the movements again
    plot(t, g_Trial.x.position, 'Color', g_XColor / 255, 'HitTest', 'off');
    plot(t, g_Trial.y.position, 'Color', g_YColor / 255, 'HitTest', 'off');

    box on;
    xlabel('Time [ms]');
    ylabel('Position [arcmin]');

    legend('X', 'Y');
    linkaxes([VPlot, EMPlot], 'x'); % link axes on time axis
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % This function resets the selection of the events
  %
  function p_resetSelection()

    % Reset the selection
    for e = 1:length(g_SelectedEventType)
      p_changeColor(g_SelectedObjects(e), g_SelectedEventType(e));
    end

    % Initialize the selection array
    g_SelectedEvent = [];
    g_SelectedEventType = [];
    g_SelectedObjects = [];
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % This function changes the face color of the object
  %
  % Object   : Handle to the object 
  % EventType: Type of the event 
  function p_changeColor(Object, EventType)

    switch (EventType)
      case 's'
        set(Object, 'facecolor', g_SColor / 255);
      case 'm'
        set(Object, 'facecolor', g_MColor / 255);
      case 'd'
        set(Object, 'facecolor', g_DColor / 255);
      case 'b'
        set(Object, 'facecolor', g_BColor / 255);
      case 'n'
        set(Object, 'facecolor', g_NColor / 255);
      case 'i'
        set(Object, 'facecolor', g_IColor / 255);
      case 'h'
        Color = get(Object, 'facecolor') - 0.5;
        Color(Color < 0) = 0;
        set(Object, 'facecolor', Color);
    end
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % This function returns the position of the selected event
  %
  % Events: Structure containing the list of events
  % t     : Instant of time where to search for the event
  function Position = p_selectEvent(Events, t)

    % Find t in the saccades
    Position = isIncludedIn(t, Events);
  end

end