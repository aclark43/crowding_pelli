
badal(1) = 4.43; %0D
% badal(2) = 4.72; % -3.6D
badal(2) = 4.84; %NA

upper(1) = 4.31;
% upper(2) = 5.39;
upper(2) = 4.72;

lower(1) = 5.39;
% lower(2) = 4.31;
lower(2) = 5.66;

percent_errorLower0D=100*abs(5.39-badal(1))./5.39;
percent_errorLowerNA=100*abs(5.66-badal(2))./5.66;
percent_errorUpper0D=100*abs(4.31-badal(1))./4.31;
percent_errorUpperNA=100*abs(4.72-badal(2))./4.72;


figure;
% subplot(1,2,1)

plot([0 1],badal,'o','MarkerFaceColor','k');
hold on
plot([0 1],upper,'*','Color','r');
plot([0 1],lower,'*','Color','r');

% plot([0 1 2],badal,'o','MarkerFaceColor','k');

xlim([-.5 1.5])
ylim([4 6])
% line([2.5 2.5],[4 5.5],'LineStyle','--');
line([-.5 1.5],[5 5],'LineStyle','-');

xticks([0:1])
xticklabels({'0D','No B'});

% text(0, 4.7, '-11%');
% text(1, 4.3, '-6%');
% text(2, 4.6, '-3%');
% text(2,5.01,'Target');
ylabel('Width of 20/20 E (arcmin)')

text(0.1,lower(1),num2str(percent_errorLower0D),'Color','r')
text(1.1,lower(2),num2str(percent_errorLowerNA),'Color','r')
text(0.1,upper(1),num2str(percent_errorUpper0D),'Color','r')
text(1.1,upper(2),num2str(percent_errorUpperNA),'Color','r')



%%%%%%%%%%%%%%%%%%%%%%%%

objectSizeInDegrees = rad2deg(2 * atan(302.6 / (2 * 5105)));

monitor(1) = 3.789; %0D
% monitor(2) = 4.016; %-3.6D
monitor(2) = 3.316;
% monitor(4) = objectSizeInDegrees;

figure;
% subplot(1,2,2)

plot([0 1],monitor,'o','MarkerFaceColor','k');
xlim([-.5 1.5])
ylim([2 5])
% line([2.5 2.5],[4 5.5],'LineStyle','--');
line([-.5 1.5],[objectSizeInDegrees objectSizeInDegrees],'LineStyle','-');

xticks([0:1])
xticklabels({'0D','No B'});

% text(0, 4.7, '-3%');
% text(1, 4.3, '-11%');
% text(2, 4.6, '-6%');
ylabel('Height of Monitor (deg)')
