function plotDeltaSWtoDeltaVar( subjectUnc, subjectCro, ...
    subjectThreshUnc, subjectThreshCro, subNum )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:length(subjectCro)
  swDelta{ii} = subjectThreshCro(ii).thresh - subjectThreshUnc(ii).thresh;
    
    swUnc = thresholdSWVals(ii).thresholdSWUncrowded;
    swCro = thresholdSWVals(ii).thresholdSWCrowded;

    thresholdUncrowdedSpan{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;
    thresholdCrowdedSpan{ii} = subjectThreshCro(ii).em.ecc_0.(swCro).meanSpan;
    
    thresholdSWUnc{ii} = subjectThreshUnc(ii).thresh;
    thresholdSWCro{ii} = subjectThreshCro(ii).thresh;
end

plotDeltaSW = cell2mat(swDelta);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function multiplSubjVelAnalysis
figure
deltaVel = threshVelCro-threshVelUncr;
[~,p] = LinRegression(deltaVel,plotDeltaSW,0,NaN,1,0);
hold on
% figure
scatter(deltaVel,plotDeltaSW,100,c,'filled')
xlabel('\Delta Mean Velocity')
ylabel('\Delta Strokewidth')
title('Difference of Velocity by Difference of SW')
format longG
pValue = sprintf('p = %.3f', p);
text(6, 0, pValue,'Fontsize',25);
axis([-5 14 -1 2])
saveas(gcf,'../../Data/AllSubjTogether/deltaVelocity.png');

figure
scatter(zeros(1,length(threshVelUncr)),threshVelUncr,200,c,'filled')%,'filled')
hold on
scatter(ones(1,length(threshVelCro)),threshVelCro,200,c,'filled')%,'filled')
axis([-0.5 1.5 30 80])
% scatter(x,thresholdSize1,sz,c,'filled')%,'filled')
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
    'FontWeight','bold')
ylabel('Mean Velocity at Threshold','FontSize',15,'FontWeight','bold')

xS = [0 1];
for ii = 1:subNum
    y1 = [threshVelUncr(1,ii) threshVelCro(1,ii)];
    line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
end
% ttest
[~,p2] = ttest2(threshVelCro,threshVelUncr);
ttestP = sprintf('unpaired ttestP= %.3f', p2);
text(0, 66, ttestP,'Fontsize',18);
title('Velocity; Uncrowded vs Crowded')



saveas(gcf,'../../Data/AllSubjTogether/CvsUVelocity.png');
end

function multiplSubjCurveAnalysis
figure
deltaCurv = threshCurvCro - threshCurvUncr;
[~,p] = LinRegression(deltaCurv,plotDeltaSW,0,NaN,1,0);
hold on
scatter(deltaCurv,plotDeltaSW,100,c,'filled')
xlabel('\Delta Mean Curvature')
ylabel('\Delta Strokewidth')
title('Difference of Curvature by Difference of SW')
format longG
pValue = sprintf('p = %.3f', p);
text(-3, 0, pValue,'Fontsize',25);
axis([-4 3 -1 2])
[~,p2] = ttest2(threshCurvCro,threshCurvUncr);
ttestP = sprintf('paired ttest p = %.3f', p2);
text(-3, -0.5, ttestP,'Fontsize',25);
saveas(gcf,'../../Data/AllSubjTogether/deltaCurvature.png');
end

function multiplSubjDSQAnalysis
figure
sz = 400;
deltaDSQ = threshDSQCro-threshDSQUncr;
% [~,p,~,r] = LinRegression(deltaDSQ,plotDeltaSW,0,NaN,1,0);
% hold on

figure;
for ii = 1:subNum
    x(ii) = plotDeltaSW(ii)/thresholdSWUnc{ii};
end

[~,p,~,r] = LinRegression(threshDSQCro,x,0,NaN,1,0);
xlabel('Crowded DSQ')
ylabel('\Delta Threshold SW/Uncrowded Threshold')

figure;
for ii = 1:subNum
    meanDSQ(ii) = mean([threshDSQCro(ii),threshDSQUncr(ii)]);
end



figure;
[~,p,~,r] = LinRegression(threshDSQCro, threshDSQUncr,0,NaN,1,0);
xlabel('Crowded DSQ')
ylabel('Uncrowded DSQ')

figure;
for ii = 1:subNum
    meanDSQ(ii) = mean([threshDSQCro(ii),threshDSQUncr(ii)]);
end

[~,p,~,r] = LinRegression(meanDSQ,plotDeltaSW, 0, NaN,1,0);
hold on
for ii = 1:subNum
 scatter(meanDSQ(ii),plotDeltaSW(ii), sz,[c(ii,1) c(ii,2), c(ii,3)],'filled','d')
end
ylim([10 30])
xlabel('Mean DSQ')
ylabel('\Delta Strokewidth')

figure;
[~,p,~,r] = LinRegression(threshDSQCro,plotDeltaSW, 0, NaN,1,0);
hold on
for ii = 1:subNum
 scatter(threshDSQCro(ii),plotDeltaSW(ii), sz,[c(ii,1) c(ii,2), c(ii,3)],'filled')
end
% ylim([10 30])
xlabel('Crowded DC')
ylabel('\Delta Strokewidth')
saveas(gcf,'../../Data/AllSubjTogether/CrowDCdeltaSW.png');
saveas(gcf,'../../Data/AllSubjTogether/CrowDCdeltaSW.epsc');

figure;
[~,p,~,r] = LinRegression(threshDSQUncr,plotDeltaSW, 0, NaN,1,0);
hold on
for ii = 1:subNum
 scatter(threshDSQUncr(ii),plotDeltaSW(ii), sz,[c(ii,1) c(ii,2), c(ii,3)],'filled','d')
end
% ylim([10 30])
xlabel('Uncrowded DSQ')
ylabel('\Delta Strokewidth')


figure;
[~,p,~,r] = LinRegression(deltaDSQ,plotDeltaSW, 0, NaN,1,0);
for ii = 1:subNum
scatter(deltaDSQ(ii),plotDeltaSW(ii),sz,[c(ii,1) c(ii,2), c(ii,3)],'filled','d')
end
xlabel('\Delta Mean Diffusion Coefficient')
ylabel('\Delta Strokewidth')
title('Difference of DC by Difference of SW')
format longG
pValue = sprintf('p = %.3f', p);
% text(5, 2, pValue,'Fontsize',25);
axis([-5 25 0 2])
rValue = sprintf('r^2 = %.3f', r);
pValue = sprintf('p = %.3f', p);
text(-0.5, 1.4, pValue,'Fontsize',10);
text(-0.5,1.6,rValue,'Fontsize',10);
% [~,p2] = ttest2(threshDSQCro,threshDSQUncr);
% ttestP = sprintf('paired ttest p = %.3f', p2);
% text(5, 2.25, ttestP,'Fontsize',25);
saveas(gcf,'../../Data/AllSubjTogether/deltaDSQ.png');
end

function spansToMats%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plotDeltaSpan = (cell2mat(thresholdCrowdedSpan)) - (cell2mat(thresholdUncrowdedSpan));
%thresholdSize = cell2mat(thresholdSW);
thresholdUncrowdedSpan = cell2mat(thresholdUncrowdedSpan);
thresholdCrowdedSpan = cell2mat(thresholdCrowdedSpan);

end

%%%%%OLD%%%%%
function multipleSubjUncrandCroAll
%linspace(1,50,length(subjectCro));
%scatter(x,y,sz,c,'filled')
sz = 200;
c = jet(subNum);

figure
scatter(thresholdUncrowdedSpan,plotDeltaSW,sz,c)
hold on
scatter(thresholdCrowdedSpan, plotDeltaSW,sz,c, 'filled')

for ii = 1:subNum
    x1 = [thresholdUncrowdedSpan(ii) thresholdCrowdedSpan(ii)];
    y1 = [plotDeltaSW(ii) plotDeltaSW(ii)];
    line(x1, y1);
end

legend('Uncrowded','Crowded')
xlabel('Span of Drift')
ylabel('Delta SW (Crowded-Uncrowded)')
title('Span by SW Difference per Subject')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function multipleSubjDeltaSpan%%%

sz = 300;

figure
[~,p,~,r] = LinRegression(plotDeltaSpan,plotDeltaSW,0,NaN,1,0);

for ii = 1:subNum
    leg(ii) = scatter(plotDeltaSpan(ii), plotDeltaSW(ii),sz,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end

% legend(leg, subjectsAll)
coeffs = polyfit(plotDeltaSpan, plotDeltaSW, 1);
% Get fitted values
fittedX = linspace(min(plotDeltaSpan), max(plotDeltaSpan), 200);
fittedY = polyval(coeffs, fittedX);
% Plot the fitted line
hold on;
plot(fittedX, fittedY, 'b--', 'LineWidth', 1);

meanSpan = mean(plotDeltaSpan);
stdSpan = std(plotDeltaSpan);
totalNumSpan = subNum;

meanSW = mean(plotDeltaSW);
stdSW = std(plotDeltaSW);
totalNumSpan = subNum;


%[h,p,ci,stats] = ttest(plotDeltaSpan,plotDeltaSW);

% mdl = fitlm(plotDeltaSpan,plotDeltaSW);
% p = mdl.Coefficients.pValue(2);

%[~,p] = ttest2(plotDeltaSpan,plotDeltaSW);


%text(0,2,(sprintf('r^2 = %i',mdl.Rsquared)));

xlabel('\Delta Span')
ylabel('\Delta Strokewidth')
title('Difference of Span by Difference of SW')

% format longG
% rSquare = round(mdl.Rsquared.Ordinary,3);

rValue = sprintf('r^2 = %.3f', r);
pValue = sprintf('p = %.3f', p);
text(-0.5, 1.4, pValue,'Fontsize',10);
text(-0.5,1.6,rValue,'Fontsize',10);

axis([-.5 2.5 0 2])
axis square
yticks([0 0.5 1 1.5 2])
yticklabels({'0','0.5','1','1.5','2'})

saveas(gcf,'../../Data/AllSubjTogether/deltaSpan.png');
saveas(gcf,'../../Data/AllSubjTogether/deltaSpan.epsc');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LargeSpanCr = [1.72 1.92 2.97 1.53 2.04 1.70];
SmallSpanCr = [1.77 1.33 3.90 1.49 2.21 1.50];


LargeSpanUncr = [1.18 1.54 2.03 1.27 1.43 0.97];
SmallSpanUncr = [1.19 1.19 1.55 1.27 1.35 0.89];

[~,p1] = ttest2(SmallSpanUncr,LargeSpanUncr);
[~,p2] = ttest2(SmallSpanCr,LargeSpanCr);

smallAll = [SmallSpanCr SmallSpanUncr];
largeAll = [LargeSpanCr LargeSpanUncr ];
[~,p] = ttest2(smallAll,largeAll);
end
end

