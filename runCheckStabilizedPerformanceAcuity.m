%%checkStabilizedConditions
clc
clear all
close all

subjectsAll = {'Ashley','Z023','Z013','Z005','Z091',}; %zoe
conditions = {'Stabilized','Unstabilized'};
ending = {'0ecc','0ecc','0ecc','0ecc','0eccEcc','0eccEcc'};
sampling = [1000 1000 1000 1000 330];

for ii = 1:length(subjectsAll)
    data.Stabilized{ii} = load(sprintf('MATfiles/%s_Uncrowded_Stabilized_Drift_%s_Threshold.mat', ...
        subjectsAll{ii}, ending{ii}));
    
    data.Unstabilized{ii} = load(sprintf('MATfiles/%s_Uncrowded_Unstabilized_Drift_%s_Threshold.mat', ...
        subjectsAll{ii}, ending{ii}));
end

for ii = 1:length(subjectsAll)
    for t = 1:2
        path = data.(conditions{t}){ii}.threshInfo;
        dataAll.thresholds(t,ii) = path.thresh;
        dataAll.dc(t,ii) = path.em.allTraces.dCoefDsq;
    end
end
%% Plot differences in threshold
figure;
for ii = 1:length(subjectsAll)
    plot([0,1],dataAll.thresholds(:,ii)','-o');
    hold on
end
xlim([-.5 1.5])
xticks([0 1])
xticklabels(conditions);
ylabel('Acuity Threshold');

%% Plot delta change in acuity to DC
figure; %negative values suggest better acuity with stabilization
[~,p,b,r] = LinRegression(dataAll.dc(2,:),...
    dataAll.thresholds(2,:) - dataAll.thresholds(1,:),...
    0,NaN,1,0);
xlim([4 20])
ylim([-0.5 0.5])
ylabel('Unstab - Stab Thresholds')
xlabel('Unstab DC');
    
%% Estimate overall amount of motion in stab traces
for ii = 1:length(subjectsAll)
    path = data.Stabilized{ii}.threshInfo.em.allTraces.stabInfo;
    for b = 1:length(path)
        temp.position(b).x = [path(b).stabXDiff];
        temp.position(b).y = [path(b).stabYDiff];
    end
    forDSQCalc.x = {temp.position.x};
    forDSQCalc.y = {temp.position.y};
[~,~, dCoefDsq(ii)] = ...
    CalculateDiffusionCoef(sampling(ii), struct('x',forDSQCalc.x, 'y', forDSQCalc.y));

end

for ii = 1:length(subjectsAll)
    figure;
    critFreqStab(ii) = powerAnalysis_JustCriticalFrequency(dCoefDsq(ii), 1);
    hold on
    critFreqUnstab(ii) = powerAnalysis_JustCriticalFrequency(dataAll.dc(2,ii), 1);
end
%% Unstabilized and Stabilized
figure;
leg(1) = scatter3(dCoefDsq,dataAll.thresholds(1,:),critFreqStab,'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0 .75 .75]); %stabilized motion
hold on
leg(2) = scatter3(dataAll.dc(2,:),dataAll.thresholds(2,:),critFreqUnstab,'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0 0 .75]); %unst motion
legend(leg,{'Stabilized','Unstabilized'});
xlabel('DC')
ylabel('Threshold')
zlabel('Critical Freq');

%%
markers = {'s' 'o' 'p' 'h' 'v'};% 'd' 'x' 'o' '+'}; 
colors = flip(brewermap(length(subjectsAll),'Set1'));
figure;
subplot(1,3,1)
    for ii = 1:length(subjectsAll)
        h(1) = plot(dataAll.dc(2,ii),dataAll.thresholds(2,ii),...
            markers{ii},'MarkerSize',10,'Color',colors(ii,:));
        hold on
    end
    for ii = 1:length(subjectsAll)
        h(2) = plot(dCoefDsq(ii),dataAll.thresholds(1,ii),...
            markers{ii},'MarkerSize',10,'Color',colors(ii,:),...
            'MarkerFaceColor',colors(ii,:));
        hold on
    end
axis square
ylabel('Thresholds')
xlabel('Retinal Motion(DC)')
axis square
legend(h,{'Unstabilized','Stabilized'})

subplot(1,3,2)
    for ii = 1:length(subjectsAll)
        h(ii) = plot(dataAll.dc(2,ii),critFreqUnstab(ii),...
            markers{ii},'MarkerSize',10, 'Color',colors(ii,:));
        hold on
    end
    for ii = 1:length(subjectsAll)
        plot(dCoefDsq(ii),critFreqStab(ii),...
            markers{ii},'MarkerSize',10, 'Color',colors(ii,:),...
            'MarkerFaceColor',colors(ii,:));
        hold on
    end
    set(gca, 'YScale', 'log')
    ylabel('Critical Frequency')
    xlabel('Retinal Motion(DC)')
    axis square
    line([0 20],[55 55])
    ylim([10 80])
subplot(1,3,3)
    for ii = 1:length(subjectsAll)
        h(ii) = plot(critFreqUnstab(ii),dataAll.thresholds(2,ii),...
            markers{ii},'MarkerSize',10,'Color',colors(ii,:));
        hold on
    end
    for ii = 1:length(subjectsAll)
        plot(critFreqStab(ii),dataAll.thresholds(1,ii),...
            markers{ii},'MarkerSize',10,'Color',colors(ii,:),...
            'MarkerFaceColor',colors(ii,:));
        hold on
    end
    set(gca, 'XScale', 'log')
    xlabel('Critical Frequency')
    ylabel('Thresholds')
    axis square
    line([55 55],[1 2])
    ylim([1 2])
    xlim([10 80])
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % % subplot(1,2,1)
% % % % [~,p,b,r] = LinRegression(dataAll.dc(2,:)- dCoefDsq,...
% % % %     dataAll.thresholds(2,:) - dataAll.thresholds(1,:),...
% % % %     0,NaN,1,0);
% % % % title('\Delta Thresh and \Delta DC')
% % % % 
% % % % subplot(1,2,2)
% % % % [~,p,b,r] = LinRegression(dCoefDsq,...
% % % %     dataAll.thresholds(1,:),...
% % % %     0,NaN,1,0);
% % % % % title('\Delta Thresh and \Delta DC')
% % % % 
% % % % [~,p,b,r] = LinRegression(critFreqUnstab- critFreqStab,...
% % % %     dataAll.thresholds(2,:) - dataAll.thresholds(1,:),...
% % % %     0,NaN,1,0);
% % % % 

%% Crowded vs Uncrowded
UncrowdedS.Ashley = load('MATfiles/Ashley_Uncrowded_Stabilized_Drift_0ecc_Threshold.mat');
CrowdedS.Ashley = load('MATfiles/AshleyDDPI_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
UncrowdedU.Ashley = load('MATfiles/Ashley_Uncrowded_Unstabilized_Drift_0ecc_Threshold.mat');
CrowdedU.Ashley = load('MATfiles/AshleyDDPI_Crowded_Unstabilized_Drift_0eccEcc_Threshold.mat');

UncrowdedS.Zoe = load('MATfiles/Zoe_Uncrowded_Stabilized_Drift_0eccEcc_Threshold.mat');
CrowdedS.Zoe = load('MATfiles/Zoe_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
UncrowdedU.Zoe = load('MATfiles/Zoe_Uncrowded_Unstabilized_Drift_0eccEcc_Threshold.mat');
CrowdedU.Zoe = load('MATfiles/Zoe_Crowded_Unstabilized_Drift_0eccEcc_Threshold.mat');

UncrowdedS.Z091 = load('MATfiles/Z091_Uncrowded_Stabilized_Drift_0eccEcc_Threshold.mat');
CrowdedS.Z091 = load('MATfiles/Z091_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
UncrowdedU.Z091 = load('MATfiles/Z091_Uncrowded_Unstabilized_Drift_0eccEcc_Threshold.mat');
CrowdedU.Z091 = load('MATfiles/Z091_Crowded_Unstabilized_Drift_0eccEcc_Threshold.mat');

subjectID = fieldnames(Crowded);
figure;
for ii = 1:length(subjectID)
    for_leg(1) = plot([0 1],...
        [UncrowdedS.(subjectID{ii}).threshInfo.thresh,...
        CrowdedS.(subjectID{ii}).threshInfo.thresh],'-o','Color','r');
    hold on
    for_leg(2)= plot([0 1],...
        [UncrowdedU.(subjectID{ii}).threshInfo.thresh,...
        CrowdedU.(subjectID{ii}).threshInfo.thresh],'-o','Color','b');
end

legend(for_leg,{'Stabilized','Unstabilized'})
ylim([1 3])
xticks([0 1])
xticklabels({'Uncrowded','Crowded'})
xlim([-.5 1.5])
ylabel('Threshold');





    