wDeg = 1;  %size of image (in degrees)
nPix = 200;  %resolution of image (pixels);
pix_Per_Deg = nPix/wDeg;

[x,y] = meshgrid(linspace(-wDeg/2,wDeg/2,nPix+1));
x = x(1:end-1,1:end-1);
y = y(1:end-1,1:end-1);

sfs = [20 20 25 30 40 60];
fpass = [10 20 30 40];
for c = fpass
    counter = 1;
    figure;
    for i = sfs
        orientation = 45;  %deg (counter-clockwise from vertical)
        sf = i; %spatial frequency (cycles/deg)
        ramp = cos(orientation*pi/180)*x - sin(orientation*pi/180)*y;
        grating = sin(2*pi*sf*ramp);
        
        subplot(3,2,counter)
        if counter == 1
            subplot(3,2,counter)
            imagesc(grating);
            title(sprintf('No Filter, SF = %i/deg',sf))
        else
            grating2 = lowpass(grating,c,pix_Per_Deg);
            imagesc(grating2);
            title(sprintf('Filter 10,200, SF = %i/deg',sf))
        end
        counter = counter + 1;
    end
    sgtitle(sprintf('For an FPass of %i',c));
end