function CheckDriftsOLD(drifts, strokeWidth, driftIdx)

%%%Check Individual Drifts
%%% Allows you to look at each individual drift within a strokewidth. The
%%% limit is whatever the largest 'y' value you have in the graph is. To
%%% delete the outlying drift...

plot((drifts.(strokeWidth).singleSegmentDsq'));
%title(fprintf('%s, Trial number %s',fd,ii));

PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    y_lim = input('What is the y limit: ');
    for d = 1:size(drifts.(strokeWidth).singleSegmentDsq,1)
        figure(2)
        hold on
        axis([0 260 0 y_lim])
        plot(drifts.(strokeWidth).singleSegmentDsq(d,:)')
                title(sprintf('D = %i, ii = %i', ...
                    d, driftIdx(d)));
        hold on
        cont = input('Stop (s) or Plot drift (p): ','s');
        if cont == 's'
            break
        end
    end
    close
end

end