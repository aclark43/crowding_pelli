function [chronic, trainNum, birthYear] = huxlinChronAcuteList (subjectsAll)
for ii = 1:length(subjectsAll)
    switch subjectsAll{ii}
        case 'HUX1'
            chronic(ii) = 1; %chronic
            trainNum(ii) = 2;
            birthYear(ii) = 1990;
        case 'HUX10'
            chronic(ii) = 2; %control
            trainNum(ii) = 1;
            birthYear(ii) = 1994;
        case 'HUX11'
            chronic(ii) = 1;
            trainNum(ii) = 1;
            birthYear(ii) = 1971;
        case 'HUX12'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = 1979;
        case 'HUX13'
            chronic(ii) = 1;
            trainNum(ii) = 2;
            birthYear(ii) = 1951;
        case 'HUX14'
            chronic(ii) = 1;
           trainNum(ii) = 1;
           birthYear(ii) = 1960;
        case 'HUX15'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = 1947;
        case 'HUX16'
            chronic(ii) = 0; %subacute
            trainNum(ii) = 1;
            birthYear(ii) = 1952;
        case 'HUX18'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = 1974;
        case 'HUX2'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = NA
        case 'HUX3'
            chronic(ii) = 0;
            trainNum(ii) = 1;
            birthYear(ii) = 1977;
        case 'HUX4'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = 1969;
        case 'HUX5'
            chronic(ii) = 1;
            trainNum(ii) = 3;
            birthYear(ii) = 1967;
        case 'HUX6'
            chronic(ii) = 0;
            trainNum(ii) = 1;
            birthYear(ii) = 1976;
        case 'HUX7'
            chronic(ii) = 1;
            trainNum(ii) = 3;
            birthYear(ii) = 1964;
        case 'HUX8'
            chronic(ii) = 0;
            trainNum(ii) =1;
            birthYear(ii) = 1958;
        case 'HUX17'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = 1980;
        case 'HUX21'
            chronic(ii) = 2;
            trainNum(ii) =1;
            birthYear(ii) = 1993;
        case 'HUX9'
            chronic(ii) = 1;
            trainNum(ii) = 2;
            birthYear(ii) = 1944;
        case 'HUX22'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = 1962;
        case 'HUX23'
            chronic(ii) = 2;
            trainNum(ii) =1;
            birthYear(ii) = 1962;
         case 'Martina'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = 1981;
         case 'HUX24'
            chronic(ii) = 2;
            trainNum(ii) = 1;
            birthYear(ii) = 1971;
         case 'HUX25'
            chronic(ii) = 0;
            trainNum(ii) = 1;
            birthYear(ii) = 1978;
    end
end