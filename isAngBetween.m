function idx = isAngBetween(theta,lb,ub)
%% Purpose:
%
%  This routine will check whether theta is contained between two angles
%  specified by lower and the upper bounds.  If theta is determined to be
%  within the bounds [lb,ub] a true value for idx will be returned. This
%  routine can handle all types of angle space such as 0 to 2*pi or 0 to
%  360 as long as they do not contain negative values.
%
%% Inputs:
%
%  theta                  [N x M x O x ...]             Angle in Radians
%
%
%  lb                     [N x M x O x ...]             Lower Bound Angle
%                                                       in Radians
%
%  ub                     [N x M x O x ...]             Upper Bound Angle
%                                                       in Radians
%
%% Outputs:
%
%  idx                    [N x M x O x ...]             Boolean Index
%                                                       indicating if theta
%                                                       is between the
%                                                       bounds specified by
%                                                       lb and ub
%
%% Revision History:
%  Darin C. Koblick                                         (c) 10/21/2020
%% ------------------------- Begin Code Sequence --------------------------
if nargin == 0
   
   %Traditional Case:
   theta = linspace(0,359,360)'.*pi/180;
      lb = 0*pi/180;
      ub = 45*pi/180;
     idx = isAngBetween(theta,lb,ub);
     
     figure('color',[1 1 1]);
     subplot(1,2,1);
     if lb < ub
        polarhistogram([lb ub],[lb ub],'FaceColor','w','EdgeColor','none'); hold on;
        polarhistogram([ub 2*pi],[ub 2*pi],'FaceColor','k','EdgeColor','none','FaceAlpha',1);
        polarhistogram([0 lb],[0 lb],'FaceColor','k','EdgeColor','none','FaceAlpha',1);
     else
        polarhistogram([lb,2*pi],[lb,2*pi],'FaceColor','w','EdgeColor','none'); hold on;
        polarhistogram([0 ub],[0 ub],'FaceColor','w','EdgeColor','none');
        polarhistogram([ub lb],[ub lb],'FaceColor','k','EdgeColor','none','FaceAlpha',1);
     end
     %Plot the angles within bounds:
     polarplot(theta(idx),theta(idx).*0+2,'.','color',[0 1 0 0.5],'markersize',10);
     polarplot(theta(~idx),theta(~idx).*0+2,'.','color',[1 0 0 0.5],'markersize',10);
     polarplot([lb,lb],[0 2],'-','color',[0 0 0 1],'linewidth',2);
     polarplot([ub,ub],[0 2],'-','color',[0 0 0 1],'linewidth',2);
     %Turn off the rho axis:
                Ax = gca;
     Ax.RTickLabel = []; 
          Ax.RGrid = 'off';
     title(['[ \theta_{min} = ',num2str(lb*180/pi), ...
            '^\circ ,',' \theta_{max} = ',num2str(ub*180/pi),'^\circ ]']);
     subplot(1,2,2);
     %Wrapped Case:
 [lb,ub] = deal(ub,lb);
     idx = isAngBetween(theta,lb,ub);
     if lb < ub
        polarhistogram([lb ub],[lb ub],'FaceColor','w','EdgeColor','none'); hold on;
        polarhistogram([ub 2*pi],[ub 2*pi],'FaceColor','k','EdgeColor','none');
        polarhistogram([0 lb],[0 lb],'FaceColor','k','EdgeColor','none','FaceAlpha',1);
     else
        polarhistogram([lb,2*pi],[lb,2*pi],'FaceColor','w','EdgeColor','none'); hold on;
        polarhistogram([0 ub],[0 ub],'FaceColor','w','EdgeColor','none');
        polarhistogram([ub lb],[ub lb],'FaceColor','k','EdgeColor','none','FaceAlpha',1);
     end
     %Plot the angles within bounds:
     polarplot(theta(idx),theta(idx).*0+2,'.','color',[0 1 0 0.5],'markersize',10);
     polarplot(theta(~idx),theta(~idx).*0+2,'.','color',[1 0 0 0.5],'markersize',10);
     polarplot([lb,lb],[0 2],'-','color',[0 0 0 1],'linewidth',2);
     polarplot([ub,ub],[0 2],'-','color',[0 0 0 1],'linewidth',2);
     %Turn off the rho axis:
                Ax = gca;
     Ax.RTickLabel = []; 
          Ax.RGrid = 'off';
     title(['[ \theta_{min} = ',num2str(lb*180/pi), ...
            '^\circ ,',' \theta_{max} = ',num2str(ub*180/pi),'^\circ ]']);
   idx = [];
   return;
end
idx=(lb<=theta & theta<=ub)|(((0<=theta & theta<=ub)|lb <= theta) & lb>ub);
end