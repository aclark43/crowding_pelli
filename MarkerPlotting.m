function plotting = MarkerPlotting(params, pptrials, useTrials, trialChar, title_str, video)
% figure

% trialsNoOS = osRemoverInEM(pptrials);
temptrials = (pptrials);%trialsNoOS(useTrials);
% temptrials = trialsNoOS(intersect(find(useTrials == 1), (find(valid.trialIdx))

for i = 1:length(temptrials)
    if ~(temptrials{i}.TimeTargetON == 0)% equivalent (<->) to trialsNoOS{i}.TimeTargetON ~= 0
        figure
        if strcmp('D',params.machine)
            xx= temptrials{i}.x.position(round(temptrials{i}.TimeTargetON/(1000/330)):round(temptrials{i}.TimeTargetOFF/(1000/330))) + params.pixelAngle*temptrials{i}.xoffset;
            yy= temptrials{i}.y.position(round(temptrials{i}.TimeTargetON/(1000/330)):round(temptrials{i}.TimeTargetOFF/(1000/330))) + params.pixelAngle*temptrials{i}.yoffset;
            
            xxs = temptrials{i}.x.position(double(round(temptrials{i}.TimeTargetON/(1000/330))))+ params.pixelAngle*temptrials{i}.xoffset;
            yys = temptrials{i}.y.position(double(round(temptrials{i}.TimeTargetON/(1000/330))))+ params.pixelAngle*temptrials{i}.yoffset;
        else
            xx= temptrials{i}.x.position(round(temptrials{i}.TimeTargetON):round(temptrials{i}.TimeTargetOFF)) + params.pixelAngle*temptrials{i}.xoffset;
            yy= temptrials{i}.y.position(round(temptrials{i}.TimeTargetON):round(temptrials{i}.TimeTargetOFF)) + params.pixelAngle*temptrials{i}.yoffset;
            
            xxs = temptrials{i}.x.position(double(round(temptrials{i}.TimeTargetON)))+ params.pixelAngle*temptrials{i}.xoffset;
            yys = temptrials{i}.y.position(double(round(temptrials{i}.TimeTargetON)))+ params.pixelAngle*temptrials{i}.yoffset;
        end
        
        hold on
        stimuliSize = temptrials{i}.TargetStrokewidth * params.pixelAngle;
        
        if params.crowded
            %             flankerDist = temptrials{i}.FlankerDist;
            rectangle('Position',[-(2*stimuliSize+(2*stimuliSize*1.4)) -stimuliSize*5 2*stimuliSize 10*stimuliSize])
            rectangle('Position',[(2*stimuliSize*1.4) -stimuliSize*5 2*stimuliSize 10*stimuliSize])
            rectangle('Position',[(-stimuliSize) -(3*stimuliSize+(10*stimuliSize*1.4)) (2*stimuliSize) (10*stimuliSize)])
            rectangle('Position',[(-stimuliSize) ((10*stimuliSize*1.4)-7*stimuliSize) (2*stimuliSize) (10*stimuliSize)])
            %             plot(stimuliSize + flankerDist, 0,'sk', 'MarkerFaceColor','k')%Right Flanke
            %             plot(0, stimuliSize + flankerDist, 'sk', 'MarkerFaceColor','k')%Top Flanker
            %             plot(0, -stimuliSize - flankerDist, 'sk', 'MarkerFaceColor','k')%Bottom Flanker
        end
        
         if temptrials{i}.TargetOrientation == 1
            target_number = imread('3_20x100.png');
        elseif temptrials{i}.TargetOrientation == 2
            target_number = imread('5_20x100.png');
        elseif temptrials{i}.TargetOrientation == 3
            target_number = imread('6_20x100.png');
        else
            target_number = imread('9_20x100.png');
        end
%         rectangle('Position',[(-stimuliSize) (-stimuliSize*5) (2*stimuliSize) (10*stimuliSize)])
        
        % show the image and change the location
        image('CData',target_number,'XData',[-stimuliSize stimuliSize],'YData',[-5*stimuliSize 5*stimuliSize])
        % change the color back to black and white
        colormap([0:255; 0:255; 0:255]' / 255)
        % reverse the axis so the image does not look upside down
        set(gca,'YDir','reverse');
        
        if video
            videoPlotting(xx, yy, i)
        else
%             plot(0,0,'ok','MarkerFaceColor', 'k')%Center Number
            
            plot(xxs,yys, '*r')%Fixation Point on Graph (red *)
            plot(xx,yy);
            hold off
            
            hold on
            xlabel('X position');
            ylabel('Y position');
            axis([-30 30 -30 30])
            
            axis square
            fprintf('This is %i graph',i);
            
            input ''
            clf
        end
        filepath = ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding');
        saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/DriftperTrial%s.epsc', filepath, trialChar.Subject, title_str));
        saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/DriftperTrial%s.jpeg', filepath, trialChar.Subject, title_str));
        saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/DriftperTrial%s.fig', filepath, trialChar.Subject, title_str));
    end    
end
end



