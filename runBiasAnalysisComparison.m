% runBiasAnalysis
%%
% 

close all
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.fig = struct('FIGURE_ON', 0,...
    'FOLDED',0);
fig = params.fig;
subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,...
%     'Z091','Zoe'}; %Drift Only Subjects
params.driftSize = 175;
params.em = {'Drift'};

%%%%%%%%%%%%%%%%%
em = params.em;
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
cOrg = brewermap(12,'Set3');
params.c = cOrg;
% c = cOrg;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Introduction
%   This is the bias summaries based on 3 different experiments (incluidomg
%   the Pelli Acuity paper.
%% Load and Name Data
%   pelliData = pelli Acuity Data
%   readingData = drift segments from reading task. These drift segments
%   are short, around 200ms
%   snellenData = drift only segments from snellen task (Intoy 2020). these
%   are cut to 500ms (same as pelliData)

[tempPelliData, params] = loadVariablesComplete(params);
for ii = 1:subNum
    path = tempPelliData.subjectThreshUnc(ii).em.allTraces;
    pelliData(ii).x = path.x;
    pelliData(ii).y = path.y;
    pelliData(ii).old_curvature3 = mean(path.old_curvature3);
    pelliData(ii).old_curvature2 = mean(path.old_curvature2);
    pelliData(ii).Bias = path.Bias;
    pelliData(ii).dCoefDsq = path.dCoefDsq;
    pelliData(ii).Dsq = path.Dsq;
    pelliData(ii).SingleSegmentDsq = path.SingleSegmentDsq;
    pelliData(ii).TimeDsq = path.TimeDsq;
    pelliData(ii).RegLineDsq = path.RegLineDsq;
    [pelliData(ii).biasMeasure, pelliData(ii).angleRad] = ...
        calculateBiasSingleValue(pelliData(ii).Bias);
    temp(:) = eig(path.VXRecenteredALL);
    pelliData(ii).ratio = temp(2)/temp(1);
    pelliData(ii).threshold = path.RegLineDsq;
end

snellen = load('MATFiles/SnellenTask.mat');
snellenData = snellen.snellenData;
for ii = 1:length(snellenData)
    [snellenData(ii).biasMeasure, snellenData(ii).angleRad] = ...
        calculateBiasSingleValue(snellenData(ii).Bias);
    
    temp(:) = eig(snellenData(ii).VXRecenteredALL);
    snellenData(ii).ratio = temp(2)/temp(1);
    
    snellenData(ii).old_curvature3 = mean(snellenData(ii).old_curvature3);
    snellenData(ii).old_curvature2 = mean(snellenData(ii).old_curvature2);
end

reading = load('MATFiles/ReadingTask.mat');
readingData = reading.snellenData;
for ii = 1:length(readingData)
    [readingData(ii).biasMeasure, readingData(ii).angleRad] = ...
        calculateBiasSingleValue(readingData(ii).Bias);
    temp(:) = eig(readingData(ii).VXRecenteredALL);
    readingData(ii).ratio = temp(2)/temp(1);
    
    readingData(ii).old_curvature3 = mean(readingData(ii).old_curvature3);
    readingData(ii).old_curvature2 = mean(readingData(ii).old_curvature2);
end

fixation = load('MATFiles/FixationTask.mat');
fixationData = fixation.snellenData;
for ii = 1:length(fixationData)
    [fixationData(ii).biasMeasure, fixationData(ii).angleRad] = ...
        calculateBiasSingleValue(fixationData(ii).Bias);
    temp(:) = eig(fixationData(ii).VXRecenteredALL);
    fixationData(ii).ratio = temp(2)/temp(1);
    
    fixationData(ii).old_curvature3 = mean(fixationData(ii).old_curvature3);
    fixationData(ii).old_curvature2 = mean(fixationData(ii).old_curvature2);
end

cAll = {cOrg, parula(length(snellenData)), jet(length(readingData)), ...
    spring(length(jet(length(fixationData))))};
typesData = {pelliData,snellenData,readingData, fixationData};
typesDataName = {'pelli','snellen','reading200ms','fix200ms'};

%% Compare Bias
figure('Position', [2000 10 2200 900]);

for i = 1:length(typesDataName)
    dataTemp = typesData{i};
    plotConditionFigureSubplots2(length(dataTemp), dataTemp, i,  cAll{i})
end
for i = 1:4
    subplot(2,4,i)
    xlabel('DC Bias')
    ylabel('Old Curvature')
    title(typesDataName{i})
%     axis square
    axis([0 10 0 1])
end

yLabelChar = {'DC','DC Bias','OldCurvature','EigenRatio'};
for i = 5:8
    subplot(2,4,i)
    xlim([0.75 4.25])
    ylabel(yLabelChar{i-4})
    xticks([1 2 3 4]); xticklabels(typesDataName);
%     axis square
end

suptitle('Old Curvature (2)')
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/OldCurvature2.png');

%% 
    figure('Position', [2000 10 900 600]);
for i = 1:length(typesDataName)
    path = (typesData{i});
    colorTemp = cAll{i};
    subplot(2,2,i)
    clear a; clear b; clear c; clear d;
    for ii = 1:length(path)
        a(ii) = path(ii).biasMeasure/max([path(:).biasMeasure]);
        b(ii) = path(ii).ratio/max([path(:).ratio]);
        c(ii) = path(ii).old_curvature2/max([path(:).old_curvature2]);
%         d(ii) = path(ii).old_curvature2/max([path(:).old_curvature2]);
        
        plot(0,a(ii),...
            'o','MarkerSize',10,'Color',colorTemp(ii,:),'MarkerFaceColor',colorTemp(ii,:));
        plot(1,b(ii),...
            'o','MarkerSize',10,'Color',colorTemp(ii,:),'MarkerFaceColor',colorTemp(ii,:));
        plot(2,1-c(ii),...
            'o','MarkerSize',10,'Color',colorTemp(ii,:),'MarkerFaceColor',colorTemp(ii,:));
%         plot(3,d(ii),...
%             'o','MarkerSize',10,'Color',colorTemp(ii,:),'MarkerFaceColor',colorTemp(ii,:));
        
        line([0 1],[a(ii) ...
            b(ii)],'Color',colorTemp(ii,:));
        line([1 2],[b(ii) ...
            1-c(ii)],'Color',colorTemp(ii,:));
%         line([2 3],[c(ii) ...
%             d(ii)],'Color',colorTemp(ii,:));
        hold on
    end
    names = ({'Bias DSQ Function','Eigen Value Ratio', 'Old Curvature'});
    set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',12,...
        'FontWeight','bold','ytick',[0 .2 .4 .6 .8 1])
    axis([-0.5 2.5 -0.1 1.1])
    ylabel('Normalized');
    title(typesDataName{i})
end
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/LinePlotsAllBiasMeasures.png');


for i = 1:length(typesDataName)
    path = (typesData{i});
    colorTemp = cAll{i};
    figure('Position', [2000 10 900 600]);
        clear a; clear b; clear c; clear d; clear dataBiasMeasures;
    for ii = 1:length(path)
        a(ii) = path(ii).biasMeasure;
        b(ii) = path(ii).ratio;
        c(ii) = path(ii).old_curvature3;
        d(ii) = path(ii).old_curvature2;
        
        dataBiasMeasures(ii,1) = path(ii).biasMeasure;
        dataBiasMeasures(ii,2) = path(ii).ratio;
        dataBiasMeasures(ii,3) = path(ii).old_curvature3;
        dataBiasMeasures(ii,4) = path(ii).old_curvature2;

        subplot(1,4,1)
        plot(0,a(ii),...
            'o','MarkerSize',10,'Color',colorTemp(ii,:),'MarkerFaceColor',colorTemp(ii,:));
        hold on
        
        title(names{1})
        subplot(1,4,2)
        plot(0,b(ii),...
            'o','MarkerSize',10,'Color',colorTemp(ii,:),'MarkerFaceColor',colorTemp(ii,:));
        title(names{2})
        hold on
        
        subplot(1,4,3)
        plot(0,c(ii),...
            'o','MarkerSize',10,'Color',colorTemp(ii,:),'MarkerFaceColor',colorTemp(ii,:));
        title(names{3})
        hold on
        
        subplot(1,4,4)
        plot(0,d(ii),...
            'o','MarkerSize',10,'Color',colorTemp(ii,:),'MarkerFaceColor',colorTemp(ii,:));
        title(names{4})
        hold on
    end
    
    suptitle(typesDataName{i})
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/SubPlotsValueBias%s.png', typesDataName{i}));

    figure;
    [p1,ax] = plotmatrix(dataBiasMeasures);
    axis square
    allVars = {'Bias DSQ Function','Eigen Value Ratio', 'Old Curvature3', 'Old Curvature2'};
    for ii = 1:length(allVars)
        ax(ii,1).YLabel.String = (allVars{ii});
        ax(4,ii).XLabel.String = (allVars{ii});
    end
    set(gcf, 'Position', [100, 100, 1000, 800]);
    suptitle(typesDataName{i})
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/PlotMatrixVars%s.png', typesDataName{i}));

end



%% Functions
function plotConditionFigureSubplots(subNum, data, num, c)
% plotConditionFigureSubplots(subNum, data, 1, c)

%%%y = curvautre
for ii = 1:subNum
    
    subplot(2,4,num)
    plot(data(ii).biasMeasure, (data(ii).old_curvature3)*1000,'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
    
    subplot(2,4,5)
    plot(num, data(ii).dCoefDsq,'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
    
    subplot(2,4,6)
    plot(num, data(ii).biasMeasure ,'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
    
    subplot(2,4,7)
    plot(num, (data(ii).old_curvature3)*1000,'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
    
end

end

function plotConditionFigureSubplots2(subNum, data, num, c)
% plotConditionFigureSubplots(subNum, data, 1, c)

%%%y = curvautre
for ii = 1:subNum
    
    subplot(2,4,num)
    plot(data(ii).biasMeasure, (data(ii).old_curvature2),'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
    
    subplot(2,4,5)
    plot(num, data(ii).dCoefDsq,'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
    
    subplot(2,4,6)
    plot(num, data(ii).biasMeasure ,'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
    
    subplot(2,4,7)
    plot(num, (data(ii).old_curvature2),'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
    
     subplot(2,4,8)
    plot(num, (data(ii).ratio),'o',...
        'MarkerSize',10,'Color','k','MarkerFaceColor',c(ii,:));
    hold on
end

end