function [ pValueTable ] = findSpanAcrossSWPValue( subjectUnc, subjectCro, subjectsAll )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:length(subjectUnc)
    val = sprintf('%.3f',subjectUnc(ii).pAcrossSWForSpan);
    pValueUnc(ii) = str2num(val);
    
end
for jj = 1:length(subjectCro)
    val2 = sprintf('%.3f',subjectCro(jj).pAcrossSWForSpan);
    pValueCro(jj) = str2num(val2);
end

Name = subjectsAll';
pValue_for_Uncrowded = pValueUnc';
pValue_for_Crowded = pValueCro';
pValueTable = table(Name,pValue_for_Uncrowded, pValue_for_Crowded);

end

