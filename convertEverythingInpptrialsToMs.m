function [pptrialsInMs, pptrials] = convertEverythingInpptrialsToMs(pptrials, conversionFactor)

    for k = 1:length(pptrials)
        pptrials{k}.notracksMS.start = pptrials{k}.notracks.start/conversionFactor;
        pptrials{k}.blinksMS.start = pptrials{k}.blinks.start/conversionFactor;
        pptrials{k}.microsaccadesMS.start = pptrials{k}.microsaccades.start/conversionFactor;
        pptrials{k}.driftsMS.start = pptrials{k}.drifts.start/conversionFactor;
        pptrials{k}.saccadesMS.start = pptrials{k}.saccades.start/conversionFactor;
        
        pptrials{k}.notracksMS.duration = pptrials{k}.notracks.duration/conversionFactor;
        pptrials{k}.blinksMS.duration = pptrials{k}.blinks.duration/conversionFactor;
        pptrials{k}.microsaccadesMS.duration = pptrials{k}.microsaccades.duration/conversionFactor;
        pptrials{k}.driftsMS.duration = pptrials{k}.drifts.duration/conversionFactor;
        pptrials{k}.saccadesMS.duration = pptrials{k}.saccades.duration/conversionFactor;
        
        pptrials{k}.notracksMS.amplitude = pptrials{k}.notracks.amplitude/conversionFactor;
        pptrials{k}.blinksMS.amplitude = pptrials{k}.blinks.amplitude/conversionFactor;
        pptrials{k}.microsaccadesMS.amplitude = pptrials{k}.microsaccades.amplitude/conversionFactor;
        pptrials{k}.driftsMS.amplitude = pptrials{k}.drifts.amplitude/conversionFactor;
        pptrials{k}.saccadesMS.amplitude = pptrials{k}.saccades.amplitude/conversionFactor;
        
        pptrials{k}.notracksMS.angle = pptrials{k}.notracks.angle/conversionFactor;
        pptrials{k}.blinksMS.angle = pptrials{k}.blinks.angle/conversionFactor;
        pptrials{k}.microsaccadesMS.angle = pptrials{k}.microsaccades.angle/conversionFactor;
        pptrials{k}.driftsMS.angle = pptrials{k}.drifts.angle/conversionFactor;
        pptrials{k}.saccadesMS.angle = pptrials{k}.saccades.angle/conversionFactor;
        
%         pptrials{k}.ResponseMS = pptrials{k}.Response/conversionFactor;
%         pptrials{k}.ResponseTimeMS = pptrials{k}.ResponseTime/conversionFactor;
%         pptrials{k}.TimeCueOFFMS = pptrials{k}.TimeCueOFF/conversionFactor;
%         pptrials{k}.TimeCueONMS = pptrials{k}.TimeCueON/conversionFactor;
%         pptrials{k}.TimeFixationONMS = pptrials{k}.TimeFixationON/conversionFactor;
%         pptrials{k}.TimeFixationOFFMS = pptrials{k}.TimeFixationOFF/conversionFactor;
%         pptrials{k}.TimeTargetONMS = pptrials{k}.TimeTargetON/conversionFactor;
%         pptrials{k}.TimeTargetOFFMS = pptrials{k}.TimeTargetOFF/conversionFactor;

        
    end
    
    pptrialsInMs = pptrials;   
    

end