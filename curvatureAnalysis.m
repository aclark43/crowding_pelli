function mCurveSubj = curvatureAnalysis( subjectCondition, subjectThreshCondition, subjectsAll, condition , c, fig)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

% if fig.FIGURE_ON
%     for ii = 1:length(subjectsAll)
%         figure
%         for subIdx = 1:length(subjectCondition(ii).SW)
%             strokeValue = (subjectCondition(ii).SW(subIdx));
%             if strokeValue == 0
%                 continue
%             end
%             strokeWidth = sprintf('strokeWidth_%i',strokeValue);
%             trialNum = length(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).curvature);
%             for curIdx = 1:trialNum
%                 curvSubj = cell2mat(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).curvature);
%                 plotSW = zeros(length(curvSubj), 1);
%                 plotSW(:) = strokeValue;
%                 plotSW = plotSW';
%                 scatter(curvSubj,plotSW,'filled');
%                 hold on
%             end
%             allThresh = curvSubj;
%             averageThresh = mean(allThresh);
%             corThresh = strokeValue;
%             sigma = std(allThresh);
%             sigma2 = std(corThresh);
%             
%             hold on
%             errorbar(averageThresh,corThresh,sigma,...
%                 'horizontal', '-ok','MarkerSize',8,...
%                 'MarkerEdgeColor','black',...
%                 'MarkerFaceColor','black',...
%                 'CapSize',5,...
%                 'LineWidth',3)
%         end
%         title(sprintf('Curvature %s, %s',subjectsAll{ii}, condition{1}))
%         xlabel('Curvature')
%         ylabel('Threshold Strokewidth')
%         saveas(gcf, sprintf('../Data/AllSubjTogether/%s_CurvePerSubj_%s.png', ...
%             subjectsAll{ii}, condition{1}));
%         
%     end
%     close all
%     
%     % CHECK DRIFT VELOCITY FOR THRESHOLD SW
%     %'Z023 Blue','Ashley Orange','Z005 Yellow','Z002 Pruple','Z013 Green','Z063 Light Blue'
%     clear ii
% end

figure
for ii = 1:length(subjectsAll)
    strokeValue =(subjectThreshCondition(ii).thresh);
    [~,thresholdIdx] = min(abs(strokeValue - subjectCondition(ii).stimulusSize));
    strokeWidth = sprintf('strokeWidth_%i',subjectCondition(ii).SW(thresholdIdx));
    curvSubj = (cell2mat(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).curvature));
    mCurveSubj(ii) = mean(curvSubj);
    %     scatter(curvSubj,strokeValue,100,'filled')
    
    %     plotSW = zeros(length(curvSubj), 1);
    %     plotSW(:) = strokeValue;
    %     plotSW = plotSW';
    
    errorBar(curvSubj,strokeValue)
    hold on
    points(ii) = scatter(mean(curvSubj),strokeValue,100,c(ii,:),'filled');
end
xlabel('Curvature(Mean)')
ylabel('Threshold Strokewidth')
title(sprintf('Curvature,All Subjects,%s',condition{1}))
legend([points],subjectsAll)
hold on

saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjCurve%s.png', condition{1}));


thresholds = [subjectThreshCondition.thresh];

figure
[~,p,~,r] = LinRegression(mCurveSubj,thresholds,0,NaN,1,0);
for ii = 1:length(mCurveSubj)
scatter(mCurveSubj(ii), thresholds(ii),200,[c(ii,1) c(ii,2), c(ii,3)],'filled')
end
rValue = sprintf('r^2 = %.3f', r);
pValue = sprintf('p = %.3f', p);
text(9, 1, pValue,'Fontsize',10);
text(9,0.75,rValue,'Fontsize',10);
xlabel('Mean Curvature at Threshold')
ylabel('Strokewidth Threshold')
graphTitle1 = 'Mean Curvature by Threshold Strokewidth';
graphTitle2 = (sprintf('%s', cell2mat(condition)));
title({graphTitle1,graphTitle2})
xlim([8 18])
ylim([0 4])
saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjCurvebyStrokewidth%s.png', condition{1}));

end

