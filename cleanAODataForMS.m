function [xNew,yNew,ms,drift] = cleanAODataForMS(x,y)
ms = struct('Start',[]);
drift = struct('Start',[]);

numnans = sum(isnan(x));
percentTrialIsNan = numnans/(length(x));

if percentTrialIsNan > .3
    warning('Tossing Trial for too many NaN');
    xNew = NaN(1,length(x));
    yNew = NaN(1,length(x));
    return;
else
    warning('Filling NaN with trace average');
    idxNans = isnan(x);
    replaceXValue = nanmean(x);
    xNew = x;
    xNew(isnan(x)) = replaceXValue;
    
    idxNans = isnan(y);
    replaceXValue = nanmean(y);
    yNew = y;
    yNew(isnan(y)) = replaceXValue;
end
                 
% figure;
o = beast(xNew,'season','none');
% printbeast(o)
% plotbeast(o)
AbruptChange = abs(o.trend.cpAbruptChange');
counter = 1;
for a = 1:length(AbruptChange)
    if AbruptChange(a) < 3
        continue;
    else
        ms.Start(counter) = o.trend.cp(a);
        ms.Duration(counter) = 15;
        ms.Amplitude(counter) = abs(o.trend.cpAbruptChange(a));
        counter = counter + 1;
    end
end

counter = 1;
if isempty(ms.Start)
    drift.Start(counter) = 1;
    drift.Duration(counter) = length(x)-1;
    counter = counter + 1;
else
    allMS = sort(ms.Start);
    for d = 1:length(allMS)
        if d == 1
            stra = 1;
            durD = allMS(1)-1;
            if durD < 50
                continue
            end
        else
            stra = allMS(d-1) + 16;
            durD = (allMS(d)-1)-stra;
            if durD < 50
                continue
            end
        end
        drift.Start(counter) = stra;
        drift.Duration(counter) = durD;
        counter = counter + 1;
    end
end

% o.trend.cpAbruptChange
% o.trend.cpPr