Last Edit 01/10/2019

PRETRIAL FIXATION POINTS
Currently, the "PreTrial" HeatMaps look at the fixation time before a trial 
task - but is not limited to how the trial performance was (if there was
a blink, no track, etc). It will not be included, however, if there was a 
no track or blink during the fixation period.


CHANGES IN CODE
1) 01/10/2019
For running basicEISRead to generate pptrials, some data has a store 
variable with "uncrowded" vs "Uncrowded". To still have data processed the 
same way, an if statement on line 73 in the function "p_eis_execRules" was 
added that states if uncrowded is lowercase, change to uppercase.