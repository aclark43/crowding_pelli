function [angleVF1, angleVF2] = anglesOfLossHuxlin(ii)

if ii == 2
    angleVF1 = -20;
    angleVF2 = 100;
elseif ii == 1
    angleVF1 = 91;
    angleVF2 = -170;
elseif ii == 3
    angleVF1 = 0;
    angleVF2 = -100;
elseif ii == 4
    angleVF1 = 90;
    angleVF2 = -90;
elseif ii == 5
    angleVF1 = 45;
    angleVF2 = -90;
elseif ii == 6
    angleVF1 = -10;
    angleVF2 = 89;
elseif ii == 7
    angleVF1 = 89;
    angleVF2 = -179;
elseif ii == 8
    angleVF1 = 100;
    angleVF2 = -90;
elseif ii == 9
    angleVF1 = 145;
    angleVF2 = -90;
elseif ii == 10
    angleVF1 = 45;
    angleVF2 = -45;
elseif ii == 11
    angleVF1 = 90;
    angleVF2 = -90;
end