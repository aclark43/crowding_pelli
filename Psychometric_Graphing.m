function [condition] = Psychometric_Graphing(params, uEcc, valid, data, ~,  title_str)

fprintf('%s: psychometric fitting (normal)\n', datestr(now));

uEcc = round(uEcc);
condition = buildConditionStruct(uEcc);
xl = [0.1, 11];
yl = [0, 1.05];
gamma = 0.25;
performanceX = [];
%         if any(data.Response == 3 | data.Response == 4) %%NOTE: Why do this check
%             gamma = 0.25; % 4-afc
%         end

if params.DMS
    emType = valid.dms;
elseif params.MS
    emType = valid.ms;
else
    emType = valid.d;
end

for ui = 1:length(uEcc) %9 eccentricity for crowded condition
    useTrials = emType & (data.TargetEccentricity == uEcc(ui));
    % Define parameters
%     logmar = log10(params.pixelAngle(ii) * double(data.TargetStrokewidth(useTrials)));
%     mar = 10.^logmar;
    corr = data.Correct(useTrials);
    
%     tarSW = data.TargetStrokewidth(useTrials);
%     corrSW4 = corr(tarSW == 4);
    
    if sum(useTrials) < 10
        warning('There are too few trials to graph!')
        fprintf('Not Enough Trials for Graph!')
        condition(ui).thresh = NaN;
        condition(ui).threshSW4 = NaN;
        condition(ui).sizesTestedX = NaN;
        condition(ui).performanceAtSizesY = NaN;
        condition(ui).par = NaN;
        condition(ui).threshB = NaN;
        condition(ui).bootMS = [NaN, NaN];
        condition(ui).logBootMS = [NaN,NaN];
        condition(ui).Condition = title_str;
        return;
    end
    if params.crowded == 1 %% crowded condition "true"
        flankerdis = 1.4;
%         ccdist = (round(2*data.TargetStrokewidth(useTrials)*flankerdis))*params.pixelAngle;
        ccdist = (data.TargetSize(useTrials)*flankerdis);
%         eedist = (ccdist) - data.TargetSize(useTrials);
        [~, ~, ~] = psyfitCrowding(ccdist, corr, 'DistType', 'Normal',...
            'PlotHandle', ui, 'Xlim', xl, 'Ylim', yl, 'Boots', params.nBoots,...
            'Chance', gamma, 'Extra');
%             'Log', 'Chance', gamma, 'Extra');
        graphTitle = title_str;
        title(graphTitle,'Interpreter','none');
        xlabel('Flanker Distance C-C (arcmin)'); % Flanker Distance instead of sw
        set(gcf, 'Color', 'w');
        set(gca, 'FontSize', 12, 'FontWeight', 'bold');
        
        filepath = ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/');
        
        saveas(gcf, sprintf('%s/Data/%s/Graphs/%sCENTERTOCENTER.epsc', filepath, data.Subject, graphTitle));
        saveas(gcf, sprintf('%s/Data/%s/Graphs/FIG/%sCENTERTOCENTER.fig', filepath, data.Subject, graphTitle));
        saveas(gcf, sprintf('%s/Data/%s/Graphs/JPEG/%sCENTERTOCENTER.png', filepath, data.Subject, graphTitle));
    end
    
    if sum(useTrials) > 1
%         sw =(2*double(data.TargetStrokewidth(useTrials)))*params.pixelAngle; %ROUNDS X value before taking pixels to arcmin
        sw = round(data.TargetSize(useTrials),1);
%         close all;
        [thresh, par, threshB, xValues, yValues] = psyfitCrowding(sw, corr, 'DistType', 'Normal',...
            'PlotHandle', 10+ui, 'Xlim', xl, 'Ylim', yl, 'Boots', 2,...
            'Chance', gamma, 'Extra');
%             'Log', 'Chance', gamma, 'Extra');

        %SW4 Performance Value
%         if strcmp('Z070',data.Subject) 
%             xSize = find(round(xValues,2) == 4.02, 1);
%             if isempty(xSize) || isnan(xSize)
%                 xSize = round(median(find(round(xValues) == 4.0,1)));
%                 if isempty(xSize) || isnan(xSize)
%                     xSize = round(median(find(round(xValues) == 4)));
%                 end
%             end
%             performanceX = yValues(xSize);
%             actualSize = 4.02;
%         elseif strcmp('Z024',data.Subject) 
%             xSize = find(round(xValues,2) == 2.54, 1);
%             if isempty(xSize) || isnan(xSize)
%                 xSize = round(median(find(round(xValues) == 2.5,1)));
%                 if isempty(xSize) || isnan(xSize)
%                     xSize = round(median(find(round(xValues) == 2)));
%                 end
%             end
%             performanceX = yValues(xSize);
%             actualSize = 2.5;
%         else
%             xSize = find(round(xValues,2) == 2.02, 1);
%             if isempty(xSize) || isnan(xSize)
%                 xSize = round(median(find(round(xValues) == 2.0,1)));
%                 if isempty(xSize) || isnan(xSize)
%                     xSize = round(median(find(round(xValues) == 2)));
%                 end
%             end
            performanceX = [];%yValues(xSize);
            actualSize = [];
%         end
        
        set(gcf,'units','normalized','outerposition',[0.25 0.25 .3 .5])
        x = round(double(unique(sw)),1);
%         set(gca, 'XTick', x, 'xticklabel', {x})
        
        xlimit = max(x)+1;
        xlim([0,xlimit])
        
        graphTitle = title_str;
        title(graphTitle,'Interpreter','none');
        
        xlabel('SW in arcmin');
        ylabel('Proportion Correct');
        set(gcf, 'Color', 'w');
        set(gca, 'FontSize', 16);
        
%         figure 
%         swSW4 =(2*double(data.TargetStrokewidth(tarSW == 4)))*params.pixelAngle;
%         [threshSW4, ~, ~] = psyfitCrowding(swSW4, corrSW4, 'DistType', 'Normal',...
%             'PlotHandle', 10+ui, 'Xlim', xl, 'Ylim', yl, 'Boots', params.nBoots,...
%             'Log', 'Chance', gamma, 'Extra');
%         close figure 
        
        condition(ui).thresh = thresh;
        condition(ui).threshSW4 = performanceX;
        condition(ui).actualSizeSW4 = actualSize;
        condition(ui).sizesTestedX = xValues;
        condition(ui).performanceAtSizesY = yValues;
        condition(ui).par = par;
        condition(ui).threshB = threshB;
        condition(ui).bootMS = [nanmean(threshB), nanstd(threshB)];
        condition(ui).logBootMS = [nanmean(log10(threshB)), nanstd(log10(threshB))];
        condition(ui).Condition = title_str;
    elseif sum(useTrials) <= 1
        fprintf('Not Enough Trials for Graph!')
        condition(ui).thresh = NaN;
        condition(ui).threshSW4 = NaN;
        condition(ui).sizesTestedX = NaN;
        condition(ui).performanceAtSizesY = NaN;
        condition(ui).par = NaN;
        condition(ui).threshB = NaN;
        condition(ui).bootMS = [NaN, NaN];
        condition(ui).logBootMS = [NaN,NaN];
        condition(ui).Condition = title_str;
        return;
    else
        graphTitle = title_str;
        title(graphTitle,'Interpreter','none');
    end
    
    filepath = ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding');
%     save(sprintf...
%         ('%s/Data/%s/Graphs/THRESHOLDS/%s_for_SWinARC_%s_summary.mat', filepath, data.Subject, thresh, title_str))
end

data.psyfitsNorm.uncrowded = condition;

if params.compSpans == 1
    saveas(gcf, sprintf('../../Data/AllSubjTogether/Span/AllSpan/SmallSpan%s.png',graphTitle));
     saveas(gcf, sprintf('../../Data/AllSubjTogether/Span/AllSpan/SmallSpan%s.fig',graphTitle));
elseif params.compSpans == 2
    saveas(gcf, sprintf('../../Data/AllSubjTogether/Span/AllSpan/LargeSpan%s.png',graphTitle));
    saveas(gcf, sprintf('../../Data/AllSubjTogether/Span/AllSpan/LargeSpan%s.fig',graphTitle));
elseif params.compSpans == 0
    saveas(gcf, sprintf('%s/Data/%s/Graphs/%s.epsc', filepath, data.Subject, graphTitle));
    saveas(gcf, sprintf('%s/Data/%s/Graphs/FIG/%s.fig',filepath, data.Subject, graphTitle));
    saveas(gcf, sprintf('%s/Data/%s/Graphs/JPEG/%s.png',filepath, data.Subject, graphTitle));
    
    saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/PsychometricFits/%s.png', graphTitle));
%     if ~params.crowded == 1
%        saveas(gcf, sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PsychometricFits/%s.png', graphTitle));
%     end
    %      saveas(gcf, sprintf('%s/Data/AllSubjTogether/NoToss%s.epsc', filepath,  graphTitle));
%     saveas(gcf, sprintf('%s/Data/AllSubjTogether/NoToss%s.fig',filepath,  graphTitle));
%     saveas(gcf, sprintf('%s/Data/AllSubjTogether/NoToss%s.png',filepath,  graphTitle));
end
end

