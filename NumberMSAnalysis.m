%% Number of total MS occuring during target presentation

subjectsAll = {'Z063','Ashley','Z013','Z002','Z023','Z005'};
%   subjectsAll = {'AshleyDDPI','Z023DDPI','Ashley','Z023'};

subNum = length(subjectsAll);

totalNumCro = [466 391 678 605 366 257];
crowdedSum = [41 145 259 118 52 364];

totalNumUncr = [400 490 673 471 299 233];
uncrowdedSum = [4 34 161 71 26 202];

for ii = 1:length(totalNumUncr)
    percentTrialsWithMSUncr(ii) = (uncrowdedSum(ii)/totalNumUncr(ii));
end

for ii = 1:length(totalNumCro)
    percentTrialsWithMSCro(ii) = (crowdedSum(ii)/totalNumCro(ii));
end

% [~,p1] = ttest2(totalNumUncr,uncrowdedSum)
% [~,p2] = ttest2(totalNumCro,crowdedSum)


figure;
c = jet(subNum);
scatter(zeros(1,length(percentTrialsWithMSUncr)),percentTrialsWithMSUncr,200,c,'filled')
hold on
scatter(ones(1,length(percentTrialsWithMSCro)),percentTrialsWithMSCro,200,c,'filled')
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
       'FontWeight','bold')
ylabel('Percent of Trials with Microsaccades')
title({'Number of Microsaccade Trials (per condition)', 'Uncrowded vs Crowded'})
xS = [0 1];
for ii = 1:length(subjectsAll)
    y1 = [percentTrialsWithMSUncr(1,ii) percentTrialsWithMSCro(1,ii)];
    line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
end
xlim([-0.5 1.5])
[~,p2] = ttest2(percentTrialsWithMSUncr,percentTrialsWithMSCro);
ttestP = sprintf('p = %.3f', p2);
text(0.20, .45, ttestP,'Fontsize',20);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%Number of MS Trials - NOT individual number of ms occurring
%Uncrowded
totalNumUncr = [233 400 471 490 299 673];
numMSTrialsUncr = [76 2 44 14 11 82];

for ii = 1:length(totalNumUncr)
    percentTrialsWithMSUncr(ii) = (numMSTrialsUncr(ii)/totalNumUncr(ii))*100;
end

% [~,p1] = ttest2(totalNumUncr,numMSTrialsUncr)
%Crowded

totalNumCro = [257 466 605 391 366 678];
numMSTrialsCro = [111 23 87 62 20 130];

[~,p] = ttest2(numMSTrialsUncr,numMSTrialsCro)

for ii = 1:length(totalNumCro)
    percentTrialsWithMSCro(ii) = (numMSTrialsCro(ii)/totalNumCro(ii))*100;
end
% [~,p2] = ttest2(totalNumCro,numMSTrialsCro)

%%%%%%%%%%%%%%%
totalNum = [totalNumUncr totalNumCro];
numMSTrials = [numMSTrialsUncr numMSTrialsCro];

 [~,p] = ttest2(numMSTrialsCro,numMSTrialsUncr);

% for ii = 1:subNum
%     swColumnU = sprintf('strokeWidth_%i', swThreshUncr(ii));
%     uncr{ii} = percentOnTargUncr(ii).(swColumnU);
%     
%     swColumnC = sprintf('strokeWidth_%i', swThreshCro(ii));
%     cro{ii} = percentOnTargCro(ii).(swColumnC);
% end
% 
% uncr = str2double(uncr);
% cro = str2double(cro);


% uncr = ([50.21 67.79 90.70 54.78 57.77 55.13] * 5);
% cro = ([60.00 79.79 83.07 69.86 82.55 67.85] * 5);
figure;
scatter(zeros(1,length(percentTrialsWithMSUncr)),percentTrialsWithMSUncr,200,c,'filled')
hold on
scatter(ones(1,length(percentTrialsWithMSCro)),percentTrialsWithMSCro,200,c,'filled')
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
       'FontWeight','bold')
ylabel('Percent of Trials with Microsaccades')
title({'Number of Microsaccade Trials (per condition)', 'Uncrowded vs Crowded'})
xS = [0 1];
for ii = 1:length(subjectsAll)
    y1 = [percentTrialsWithMSUncr(1,ii) percentTrialsWithMSCro(1,ii)];
    line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
end
xlim([-0.5 1.5])
[~,p2] = ttest2(percentTrialsWithMSUncr,percentTrialsWithMSCro);
ttestP = sprintf('p = %.3f', p2);
text(0.20, 20, ttestP,'Fontsize',20);

