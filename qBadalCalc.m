function [nvd] = qBadalCalc(d, g)
% March 15, 2019
% MAC & RW

% SETUP:
% Face the flat side of lens to the male connector of the 3 in  tube. Push 
% the lens to the end to the holding tube and lock with lock ring. Mount
% 3 in tube holder on the 2" Dovetail Rail Carriers with post and post
% holder. Place the female side of 3 in tube (with lens) on a flat
% surface. When installing tube holder (with Carriers and holder), move it
% such that the short side of th carriers sit on the flat surface.

% MEASURE: 
% Slide two lens on the rail. The male side of tube should face subjects.
% Alight the far side (reference to subjects) of the carriers of Badel lens 
% to the rail at 50 mm. Move auxiliary lens and read the number on the rail 
% that alight to the far side of the carrier of auxiliary lens. Subtract
% 50mm from this measument to get the input d; 

% CALCULATE:
% d   = measured distance between lens *centers* in (mm) -- can measure from the same point on each lens
% g   = measured distance from eye to monitor (mm)  
% nvd = the view distance you should enter into EyeRIS (mm)

f  = 150; % focal length of our lenses (Fb and Fa)
d = d - 36.01 + 2 * 3.8705; % adjust d to be distance from front pp of Badal and back pp of Auxiliary

p  = g - (f + d); % Distance from Auxiliary lens to monitor.
q  = 1./(1 / f - 1 ./ p);
p1 = d - q;
q1 = 1./(1 / f - 1 ./ p1);
vd = f - q1;
m  = q1 ./ p1 .* q ./ p;

nvd = abs(round(vd/m)); 

%oh = q1 ./ p1 .* q ./ p * h; 
%va = atan2(oh, vd) / pi * 180;