function [val] = ...
    reBuildSpanInfo( subjectCondition , subjectThresh, subNum)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%% Mean Drift Span Overall Trials
% for hh = 1%1:subNum
%     figure;
%     edges = [1 1.25 1.5 1.75 2 2.25 2.5 2.75 3 4 5 6];%[0:20];
%     histogram(subjectThreshUnc(hh).em.span,100,'BinEdges',edges)
%     title(subjectThreshUnc(hh).Condition)
% end

%% Mean Drift Spans in Small and Large Strokewidths
for i = 1:subNum
    for ii = 1:length(subjectCondition(i).stimulusSize)
        if subjectCondition(i).SW(ii) == 0
            SW(i,ii) = NaN;
            stimulusSize(i,ii) = NaN;
            meanDriftSpan(i,ii) = NaN;
            stdDriftSpan(i,ii) = NaN;
            area (i,ii) = NaN;
        else
            SW(i,ii) = subjectCondition(i).SW(ii);
            stimulusSize(i,ii) = subjectCondition(i).stimulusSize(ii);
            meanDriftSpan(i,ii) = subjectCondition(i).meanDriftSpan(ii);
            stdDriftSpan(i,ii) = subjectCondition(i).stdDriftSpan(ii);
            area(i,ii) = subjectThresh(i).em.ecc_0.(sprintf('strokeWidth_%i',SW(i,ii))).areaCovered;
        end
    end
    idxActualSWVal{i} = find(~isnan(stimulusSize(i,:)) & stimulusSize(i,:)>0);
    numberSWTested(i) = length(idxActualSWVal{i});
end
%%
for jj = 1:subNum
    nonNanDriftValues = find(~isnan(meanDriftSpan(jj,:))>0);
    firstMeanDrift = meanDriftSpan(jj,nonNanDriftValues(1));
    val.smallSpansAbs(jj,1) = firstMeanDrift;
    val.smallErrorsAbs(jj,1) = stdDriftSpan(jj,nonNanDriftValues(1));
    realArea = find(~isnan(area(jj,:)) & area(jj,:)>0);
    val.smallArea = area(jj,realArea);
    
    for large = idxActualSWVal{jj}(length(idxActualSWVal{jj}))
        val.largeSpansAbs(jj,1) = meanDriftSpan(jj,large);
        val.largeErrorsAbs(jj,1) = stdDriftSpan(jj, large);
        val.largeArea = area(jj,realArea);
    end
end


end

