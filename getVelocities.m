function [velocity allVels] = getVelocities(subjectMSCondition, subNum)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:subNum
    if isfield(subjectMSCondition(ii).em,'ecc_0')
        swAll = fieldnames(subjectMSCondition(ii).em.ecc_0);
        counter = 1;
        for i = 1:numel(fieldnames(subjectMSCondition(ii).em.ecc_0)) %loop through SW
            sw = char(swAll(i));
            for numA = 1:length(subjectMSCondition(ii).em.ecc_0.(sw).velocityMax)
                temp = subjectMSCondition(ii).em.ecc_0.(sw).velocityMax{numA};
                for indMS = 1:length(temp)
%                     temp = max(temp);
                    msVel(counter) = double(temp(indMS));
                    
                    counter = counter + 1;
                end
                
            end
        end
        velocity(ii) = nanmean(msVel);
        allVels{1,ii} = msVel; 
%         allAmps{2,ii} = msAngle;
    else
        velocity(ii) = NaN;
        allVels{1,ii} = NaN;
%         allAmps{1,ii} = [];
%         allAmps{2,ii} = [];
    end
end

end

