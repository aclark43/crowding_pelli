function meanFixationbyEachSW(subjectUnc, subjectCro)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

figure
scatter(subjectUnc(1).meanDriftSpan,subjectUnc(1).SW,'MarkerEdgeColor',[255/255 0 68/255],...
              'MarkerFaceColor',[255/255 0 68/255])
hold on
scatter(subjectUnc(2).meanDriftSpan,subjectUnc(2).SW,'MarkerEdgeColor',[162/255 0 1],...
              'MarkerFaceColor',[162/255 0 1])
hold on
scatter(subjectUnc(3).meanDriftSpan,subjectUnc(3).SW,'MarkerEdgeColor',[3/255 128/255 154/255],...
              'MarkerFaceColor',[3/255 128/255 154/255])
hold on
scatter(subjectUnc(4).meanDriftSpan,subjectUnc(4).SW,'MarkerEdgeColor',[64/255 154/255 37/255],...
              'MarkerFaceColor',[64/255 154/255 37/255])
hold on
scatter(subjectUnc(5).meanDriftSpan,subjectUnc(5).SW,'MarkerEdgeColor',[0 0 0],...
              'MarkerFaceColor',[0 0 0])
hold on
scatter(subjectUnc(6).meanDriftSpan,subjectUnc(6).SW,'MarkerEdgeColor',[250/255 174/255 60/255],...
              'MarkerFaceColor',[250/255 174/255 60/255])
hold on
legend('Z023, Uncrowded','Ashley, Uncrowded','Z005, Uncrowded','Z002, Uncrowded','Z013, Uncrowded','Z090, Uncrowded','Location','northwest')

scatter(subjectCro(1).meanDriftSpan,subjectCro(1).SW,'MarkerEdgeColor',[255/255 0 68/255])
hold on
scatter(subjectCro(2).meanDriftSpan,subjectCro(2).SW,'MarkerEdgeColor',[162/255 0 1])
hold on
scatter(subjectCro(3).meanDriftSpan,subjectCro(3).SW,'MarkerEdgeColor',[3/255 128/255 154/255])
hold on
scatter(subjectCro(4).meanDriftSpan,subjectCro(4).SW,'MarkerEdgeColor',[64/255 154/255 37/255])
hold on
scatter(subjectCro(5).meanDriftSpan,subjectCro(5).SW,'MarkerEdgeColor',[0 0 0])
hold on
scatter(subjectCro(6).meanDriftSpan,subjectCro(6).SW,'MarkerEdgeColor',[250/255 174/255 60/255])
hold on

legend('Z023, Uncrowded','Ashley, Uncrowded','Z005, Uncrowded','Z002, Uncrowded','Z013, Uncrowded','Z063, Uncrowded',...
    'Z023, Crowded','Ashley, Crowded','Z005, Crowded','Z002, Crowded','Z013, Crowded','Z063, Crowded','Location','southwest')

ylabel('Strokewidth')
xlabel ('Mean Span of Drift')

title('Mean Span for Each SW')

end

