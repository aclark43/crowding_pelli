function [ fixation, drift ] = fixationvsuncrowded( params, subjectUnc, subjectThreshUnc, subjectCro, subjectThreshCro, thresholdSWVals )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:length(subjectsAll)
    tempidx = [];
    pathway = subjectThreshUnc(ii).em.ecc_0.(thresholdSWVals(ii).thresholdSWUncrowded);
    task.full.span(ii) = mean(pathway.span);
    task.full.dsq(ii) = pathway.dCoefDsq;
    task.full.curve(ii) = mean(cell2mat(pathway.curvature));
    tempidx = ~cellfun(@isnan,pathway.mn_speed);
    task.full.speed(ii) = mean(cell2mat(pathway.mn_speed(tempidx)));
    task.full.area(ii) = pathway.areaCovered;
    
    task.f175.span(ii) = mean(pathway.shortAnalysis.span);
    task.f175.dsq(ii) = pathway.shortAnalysis.dCoefDsq;
    task.f175.curve(ii) = mean(cell2mat(pathway.shortAnalysis.curvature));
    tempidx = ~cellfun(@isnan,pathway.shortAnalysis.mn_speed);
    task.f175.speed(ii) = mean(cell2mat(pathway.shortAnalysis.mn_speed(tempidx)));
    task.f175.area(ii) = pathway.shortAnalysis.areaCovered;
    
%     if ii = 7
%         continue;
%     end
%     fixation.
end

%% 250First vs 250 Last
% figure;
% subplot(2,2,1)
% [ fig, stats ] = plotStyledLinearRegression( task.First250.dsq, drift.Last250.dsq, params.c);
% xlabel('First250')
% ylabel('Last250')
% title('DC')
% 
% subplot(2,2,2)
% [ fig, stats ] = plotStyledLinearRegression( drift.First250.span, drift.Last250.span, params.c);
% xlabel('First250')
% ylabel('Last250')
% title('Span')
% 
% subplot(2,2,3)
% [ fig, stats ] = plotStyledLinearRegression( drift.First250.speed, drift.Last250.speed, params.c);
% xlabel('First250')
% ylabel('Last250')
% title('Speed')
% 
% subplot(2,2,4)
% [ fig, stats ] = plotStyledLinearRegression( drift.First250.curve, drift.Last250.curve, params.c);
% xlabel('First250')
% ylabel('Last250')
% title('Curvature')
% suptitle('Task')
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/FirstVsLastDrift.png');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/FirstVsLastDrift.epsc');
% saveas(gcf,...
%     '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/FirstVsLastDrift.png');

% figure;
% plot(dCoefDsq300Thresh(1,:),threshDSQUncr,'o');
% axis([0 60 0 60])
% axis square
% hold on
% plot(dCoefDsq300Thresh(2,:),threshDSQCro,'o');
% plot(dCoefDsqThresh(2,:),threshDSQCro,'*');
% plot(dCoefDsqThresh(1,:),threshDSQUncr,'*');
% fixation300 = [22.004753112792970, 9.677511215209961, 16.351053237915040, 11.936565399169922, 6.074677467346191...
%     20.073999404907227, 9.326773643493652, 3.452931404113770, 10.710441589355469, 25.990598678588867];
%% 200ms Fixaion Analysys
% fixation300 = [14.908885955810547, 6.135558128356934, 17.745409011840820, 15.223131179809570, 9.685349464416504, 23.889896392822266, NaN, NaN, 11.204099655151367, 19.288902282714844];
% load('MATFiles/MinFixationTrials200.mat') % Didn't toss trials based on
% span/recalibration
% load('MATFiles/FixationTrialsTossing5_175.mat')
% load('MATFiles/FixationBoots2_175.mat');

% load('MATFiles/FixationTrialsTossing6_175.mat') %Minimum 30 trials and countingTirals
% load('MATFiles/FixationTrials300.mat')
[idx, fixation] =  rebuildFixationData(fix, params);

for ii = 1:params.subNum
%     swThreshC(ii) = subjectThreshCro(ii).thresh;
    swThreshU(ii) = subjectThreshUnc(ii).thresh;
end
% figure;
% subplot(2,3,2)
% [t,p,b1,r1] = LinRegression(fixation.First200.dsq(idx),swThreshC(idx),...
%     0,NaN,1,0);
% xlabel(sprintf('Fixation DC %i', params.driftSize))
% ylabel('Crowded Threshold')
% axis square
% subplot(2,3,1)

% for ii = find(idx)
%     tempDC(ii) = mean(fixation.First200.bootDSQ{ii});
%     tempTh(ii) = mean(subjectThreshUnc(ii).bootsAll);
%     
%    tempDCU(ii) = mean(drift.First200.bootDSQ{ii});
% end

% figure;
% [t,p,b1,r1] = LinRegression(tempDC,tempTh,...
%     0,NaN,1,0);
% hold on
% for ii = find(idx)
%     [upperX, lowerX] = confInter95(fixation.First200.bootDSQ{ii});
%     errorbar(tempDC(ii),tempTh(ii), upperX, upperY,'horizontal',...
%         'o','Color',params.c(ii,:), 'MarkerFaceColor',params.c(ii,:),'MarkerSize',1)
%     hold on
%     errorbar(tempDC(ii), tempTh(ii), confInter95(subjectThreshUnc(ii).bootsAll),'vertical',...
%         'o','Color',params.c(ii,:), 'MarkerFaceColor',params.c(ii,:),'MarkerSize',1)
% end

% figure;
% [t,p,b1,r1] = LinRegression(fixation.First200.dsq(idx),swThreshU(idx),...
%     0,NaN,1,0);
% hold on
% for ii = find(idx)
% %     [upperX, lowerX] = confInter95(fixation.First200.bootDSQ{ii});
%     errorbar(fixation.First200.dsq(ii),swThreshU(ii), fixation.First200.dsq(ii) - fixation.First200.bootDSQci{ii}(1) , ...
%         fixation.First200.bootDSQci{ii}(2) - fixation.First200.dsq(ii) ,'horizontal',...
%         'o','Color',params.c(ii,:), 'MarkerFaceColor',params.c(ii,:),'MarkerSize',1)
%     hold on
%     [upperY, lowerY] = confInter95(subjectThreshUnc(ii).bootsAll);
%     errorbar(fixation.First200.dsq(ii),swThreshU(ii),upperY, lowerY,'vertical',...
%         'o','Color',params.c(ii,:), 'MarkerFaceColor',params.c(ii,:),'MarkerSize',1)
% end
% ylim([1 2.2])
% xlim([0 40])
% rValue = sprintf('r = %.3f', r1);
% bValue = sprintf('b = %.3f', b1);
% pValue = sprintf('p = %.3f', p);
% axis([0 40 1 2.2]);
% text(25,1.4,pValue)
% text(25,1.3,bValue)
% text(25,1.2,rValue)


figure;
[t,p,b1,r1] = LinRegression(fixation.First200.dsq(idx),swThreshU(idx),...
    0,NaN,1,0);
hold on
for ii = find(idx)
    [upperX, lowerX] = confInter95(fixation.First200.bootDSQ{ii});
    errorbar(fixation.First200.dsq(ii),swThreshU(ii),upperX, lowerX,'horizontal',...
        'o','Color',params.c(ii,:), 'MarkerFaceColor',params.c(ii,:),'MarkerSize',1)
    hold on
    [upperY, lowerY] = confInter95(subjectThreshUnc(ii).bootsAll);
    errorbar(fixation.First200.dsq(ii),swThreshU(ii),upperY, lowerY,'vertical',...
        'o','Color',params.c(ii,:), 'MarkerFaceColor',params.c(ii,:),'MarkerSize',1)
end
ylim([1 2.2])
xlim([0 40])
rValue = sprintf('r = %.3f', r1);
bValue = sprintf('b = %.3f', b1);
pValue = sprintf('p = %.3f', p);
axis([0 40 1 2.2]);
text(25,1.4,pValue)
text(25,1.3,bValue)
text(25,1.2,rValue)
xlabel(sprintf('Fixation DC %i', params.driftSize))
ylabel('Uncrowded Threshold')
axis square
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCFixation175VSUnc_Error.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCFixation175VSUnc_Error.epsc');
saveas(gcf,...
    '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Fixation/DCFixation175VSUnc_Error.png');

% figure;
% subplot(2,3,2)
% [t,p,b1,r1] = LinRegression(tempDCU(idx),swThreshU(idx),...
%     0,NaN,1,0);
% ylim([1 2.2])
% xlim([0 30])
% 
% xlabel(sprintf('Task DC %i', params.driftSize))
% ylabel('uncrowded Threshold')
% axis square
% 
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCTask175VSUnc.png');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCTask175VSUnc.epsc');

% figure;
% % subplot(2,3,2)
% [t,p,b1,r1] = LinRegression(drift.First500.dsq(idx),swThreshU(idx),...
%     0,NaN,1,0);
% ylim([1 2.2])
% xlim([0 30])
% 
% xlabel(sprintf('Task DC %i', params.driftSize))
% ylabel('uncrowded Threshold')
% axis square
% 
% 
% figure;
% % [t,p,b1,r1] = LinRegression(drift.First200.dsqFit(idx),swThreshU(idx),...
% %     0,NaN,1,0);
% hold on
% for num = find(idx)
%     temp = cell2mat(drift.First200.dsqFit(num));
%     errorbar(temp, swThreshU(num),  drift.First200.bint{num}(1) - temp,'horizontal','o')
% end
% xlabel('Task DSQ Fit, 95CI');
% ylabel('Visual Acuity')
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCTask175DCFit.png');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCTask175DCFit.epsc');

%% Put area in same form as other char
fixation.First200.area = threshAreaUncr.fix175.all;
% drift.First500.area = threshAreaUncr.task500.all;
% drift.First200.area = threshAreaUncr.task175.all;
%% Span, Curv, Speed, DC Fix vs Unc 200ms
figure;
namesChar = {'dsq','curve','span','speed','area'};

for numChar = 1:length(namesChar)
    subplot(2,3,numChar)
    [ fig, stats ] = plotStyledLinearRegression( fixation.First200.(namesChar{numChar})(idx), ...
        task.f175.(namesChar{numChar})(idx), params.c(idx,:));
    xlabel('Fixation')
    ylabel('Uncrowded')
    title(namesChar{numChar})
end
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/FixationVsUncAll.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/FixationVsUncAll.epsc');
saveas(gcf,...
    '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/FixationVsUncAll.png');



%% TTEST Span, Curv, Speed, DC Fix vs Unc 200ms
namesChar = {'dsq','curve','span','speed','area'};
figure;
for numChar = 1:length(namesChar)
    subplot(2,3,numChar)
    for ii = find(idx)
        scatter(0, fixation.First200.(namesChar{numChar})(ii),100, params.c(ii,:), 'filled');
        hold on
        scatter(1, task.f175.(namesChar{numChar})(ii),100, params.c(ii,:), 'filled');
        scatter(2, task.full.(namesChar{numChar})(ii),100, params.c(ii,:), 'filled');
        
        line([0 1],[fixation.First200.(namesChar{numChar})(ii) task.f175.(namesChar{numChar})(ii)],'Color',params.c(ii,:));
        line([1 2],[task.f175.(namesChar{numChar})(ii) task.full.(namesChar{numChar})(ii)],'Color',params.c(ii,:));
    end
    names = ({'Fixation','Uncrowded 200', 'Uncrowded 500'});
    set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',15,...
        'FontWeight','bold')
    errorbar(0,mean(fixation.First200.(namesChar{numChar})(idx)), std(fixation.First200.(namesChar{numChar})(idx)),...
        'o','MarkerSize',10,'Color','k','MarkerFaceColor','k')
    errorbar(1,mean(task.f175.(namesChar{numChar})(idx)), std(task.f175.(namesChar{numChar})(idx)),...
        'o','MarkerSize',10,'Color','k','MarkerFaceColor','k')
    errorbar(2,mean(task.full.(namesChar{numChar})(idx)), std(task.full.(namesChar{numChar})(idx)),...
        'o','MarkerSize',10,'Color','k','MarkerFaceColor','k')
    
    names = ({'F175','T175', 'T500'});
    set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',12,...
        'FontWeight','bold')
    ylabel(namesChar{numChar})
    [~,p] = ttest(fixation.First200.(namesChar{numChar})(idx), task.f175.(namesChar{numChar})(idx));
%     [~,p3] = ttest(task.full.dsq(idx), threshDSQUncr(idx));
    
    [~,p2] = ttest(fixation.First200.(namesChar{numChar})(idx), task.full.(namesChar{numChar})(idx));
    title(sprintf('p = %.3f,p = %.3f,', p, p2));
    %     title(sprintf('p = %.3f', p));
    
    xlim([-0.5 2.5])
end
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/TTestFixationVsUncAll.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/TTestFixationVsUncAll.epsc');
saveas(gcf,...
    '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/TTestFixationVsUncAll.png');

%%
namesChar = {'dsq','curve','span','speed'};
cChar = brewermap(length(namesChar),'Dark2');
figure;
counter = 1;
for ii = find(idx)
    for numChar = 1:length(namesChar)
        temp1 = (fixation.First200.(namesChar{numChar}));
        temp2 = (drift.First200.(namesChar{numChar}));
        temp3 = (drift.First500.(namesChar{numChar}));
        maxAll = max([temp1 temp2 temp3]);

        normT1 = temp1/maxAll;
        normT2 = temp2/maxAll;
        normT3 = temp3/maxAll;
        
        subplot(5,2,counter)
        pltLG(1) = plot(numChar,normT1(ii),'o','Color',cChar(1,:),'MarkerFaceColor',cChar(1,:));
        hold on
        pltLG(2) = plot(numChar,normT2(ii),'o','Color',cChar(2,:),'MarkerFaceColor',cChar(2,:));
        hold on
        pltLG(3) = plot(numChar,normT3(ii),'o','Color',cChar(3,:),'MarkerFaceColor',cChar(3,:));
        hold on
        %         plot(numChar, fixation.First200.(namesChar{numChar})(ii));
    end
     set(gca,'xtick',[1:5],'xticklabel', namesChar,'FontSize',15,...
        'FontWeight','bold')
     xlim([0.5 5.5])
     ylabel('Normalized');
    counter = counter + 1;
end
hL = subplot(5,2,10);
poshL = get(hL,'position');     % Getting its position

lgd = legend(hL,pltLG,{'Fixation 175ms','Task 175ms','Task 500ms'});
set(lgd,'position',poshL);      % Adjusting legend's position
axis(hL,'off');       

set(gcf, 'Position',  [2000, 100, 1200, 900])
saveas(gcf,...
    '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/NormalizedInSubjectTTestFixationVsUncAll.png');

%% 
namesChar = {'dsq','curve','span','speed'};
cChar = brewermap(length(namesChar),'Dark2');
figure;
for test = 1:5
leg = plot(test,test,'o','Color',cChar(test,:),'MarkerFaceColor',cChar(test,:));
hold on
end
figure;
counter = 1;
for ii = find(idx)
    figure;
    for numChar = 1:length(namesChar)
        temp1 = ([fixation.First200.(namesChar{numChar})(ii)...
            drift.First200.(namesChar{numChar})(ii) ...
            drift.First500.(namesChar{numChar})(ii)]);
        
        subplot(3,2,numChar)
        pltLG(numChar) = plot(1,temp1(1),'o','Color',cChar(numChar,:),'MarkerFaceColor',cChar(numChar,:));
        hold on
        pltLG = plot(2,temp1(2),'o','Color',cChar(numChar,:),'MarkerFaceColor',cChar(numChar,:));
        hold on
        pltLG = plot(3,temp1(3),'o','Color',cChar(numChar,:),'MarkerFaceColor',cChar(numChar,:));
        hold on
        
        line([1 2],[temp1(1) temp1(2)], 'Color', cChar(numChar,:));
        line([2 3],[temp1(2) temp1(3)], 'Color', cChar(numChar,:));
        ylabel((namesChar{numChar}))
        set(gca,'xtick',[1:3],'xticklabel', {'Fixation 175ms','Task 175ms','Task 500ms'},'FontSize',8)
     xlim([0.5 3.5])
        %         plot(numChar, fixation.First200.(namesChar{numChar})(ii));
    end
%      set(gca,'xtick',[1:3],'xticklabel', {'Fixation 175ms','Task 175ms','Task 500ms'},'FontSize',15,...
%         'FontWeight','bold')
%      xlim([0.5 3.5])
% %      ylabel('Normalized');
%     ylabel((namesChar{numChar}))
%     counter = counter + 1;
suptitle(subjectsAll{ii})
saveas(gcf,...
    sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/TTestFixationVsUncAll%s.png', subjectsAll{ii}));
saveas(gcf,...
    sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/TTestFixationVsUncAll%s.epsc',subjectsAll{ii}));
saveas(gcf,...
    sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/TTestFixationVsUncAll%s.png',subjectsAll{ii}));

end
hL = subplot(5,2,10);
poshL = get(hL,'position');     % Getting its position

lgd = legend(hL,leg,namesChar);
set(lgd,'position',poshL);      % Adjusting legend's position
axis(hL,'off');       

% set(gcf, 'Position',  [2000, 100, 1200, 900])
end

    


