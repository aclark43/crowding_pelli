%% load in some data
%%%%4 things commented that will influence DPI

clc
clear all

subject = {'Sanjana'};
machine = {'DDPI'};
noEyeTraces = 0;
pathToDataOpus ='Z:\Ashley\Acuity\Data\Sanjana\EsandBars';
%%

pt = sprintf('../../Data/%s', subject{1});
temp = dir(pt);
counter = 1;
for numFold = 1:length(temp)
    if (strcmp('Graphs', temp(numFold).name)) || ...
            (strcmp('.', temp(numFold).name)) || ...
            (strcmp('..', temp(numFold).name)) || ...
            (strcmp('Testing', temp(numFold).name)) || ...
            (strcmp('Calibration', temp(numFold).name))
        continue;
    end
    condSession{counter,1} = (temp(numFold).name);
    counter = counter + 1;
end

if ~strcmp('DDPI',machine)
    error('This code is only for dDPI Data')
end

%% Looping through conditions
for si = 1:length(condSession)
    %%Folder containing sessions of data
    temppathtodata = fullfile(pt, condSession{si});
    
    S = dir(temppathtodata);
    numSessions = sum([S(~ismember({S.name},{'.','..'})).isdir]);
    
    clear sesName
    for ii = 1:numSessions
        sesName{ii} = sprintf('ses%i',ii);
    end
    
    for numSess = 1:length(sesName)
        pathtodata = fullfile(pt, condSession{si}, sesName{numSess});
        %%Prevents re-writing of pptrials if they've already been analyzed
%         if noEyeTraces == 0
            dirT = sprintf('%s/%s/%s/pptrials.mat', pt, (condSession{si}), sesName{numSess});
%         else
%             dirT = sprintf('%s/%s/%s/pptrials_noEyeTraces.mat', pt, (condSession{si}), sesName{numSess});
% %             
%         end
        if exist(dirT, 'file') == 2
            %             error('This pptrial already exists. Delete it first if you want to rewrite it.');
            warning('Condition %s %s already exists. Will not overwrite pptrial.', condSession{si},...
                sesName{numSess})
            continue;
            
        end
%         if noEyeTraces == 0
%             dirT = sprintf('%s/%s/%s/pptrials.mat', pt, (condSession{si}), sesName{numSess});
%         else
%             dirT = sprintf('%s/%s/%s/pptrials_noEyeTraces.mat', pt, (condSession{si}), sesName{numSess});
%             
%         end
        fname1 = sprintf('pptrials.mat');
        
        fprintf('%s\n', fname1);
        
        % this is the raw read in of the data
        pathToNewData = fullfile(pathToDataOpus,condSession{si});
        
        if noEyeTraces == 0
            data = readdata(pathToNewData, CalListDDPI());
%             data = readdata(pathToNewData, CalListDDPI_Huxlin());
        else
            data = readdata(pathToNewData, CalListDDPI_NoTraces());
        end
        
        
        
        %         data = readdata(pathtodata, CalListDDPI());
        if noEyeTraces == 0
            temp_pptrials =  preprocessingDDPI(data, 30, 330);
            pptrials = temp_pptrials;
%             pptrials = addDebug(pathToNewData, temp_pptrials);
        elseif noEyeTraces == 1
            pptrials = data.user';
        end
        %%For Data that doesn't contain information on screen%%%
        for ii = 1:length(pptrials)
%             pptrials{ii}.debugData = debugTrials{ii};
%             [pxAngle, ~, ~] = CalculatePxAngle(1920, pptrials{ii}.pixelAngle, 543.7);
%             pptrials{ii}.pixelAngle = pxAngle;
            pptrials{ii}.checkedManually = 0;
            pptrials{ii}.machine = machine;
            count.eccDist(ii) = double(pptrials{1}.pixelAngle * pptrials{1, 1}.TargetEccentricity);
            
            if pptrials{ii}.Uncrowded == 1
                count.condition(ii) = {'Uncrowded'};
            elseif pptrials{ii}.Uncrowded == 0
                count.condition(ii) = {'Crowded'};
            end
            
            if isfield(pptrials{ii}, 'nasal')
                if pptrials{ii}.nasal == 1
                    count.side(ii) = {'Nasal'};
                elseif pptrials{ii}.nasal == 0
                    if count.eccDist(ii) == 0
                        count.side(ii) = {''};
                    else
                        count.side(ii) = {'Temp'};
                    end
                end
                eccName = 1;
            else
                count.side(ii) = {''};
                eccName = 0;
            end
            if pptrials{ii}.Unstab == 1
                count.stabil(ii) = {'Unstabilized'};
            elseif pptrials{ii}.Unstab == 0
                count.stabil(ii) = {'Stabilized'};
            end
            
        end
        
        [~, id1] = findgroups(count.condition);
        [~, id2] = findgroups(count.side);
        [~, id3] = findgroups(count.stabil);
        
        
        if length(id1) > 1
            error('There is more than one condition in here!')
        elseif length(id2) > 1
            error('There is more than one side in here!')
        elseif length(id3)>1
            error('There is more than one stabilization in here!')
        elseif length(unique(count.eccDist)) > 1
            error('There is more than one ecc in here!')
        end
        
        if count.eccDist(ii) == 0
            if eccName == 1
                newSavedFile = fullfile(sprintf('../../Data/%s/%s_%s_%seccEcc%s/%s', ...
                    subject{1}, count.condition{ii}, count.stabil{ii}, ...
                    string(count.eccDist(ii)), count.side{ii}, sesName{numSess}));
            else
                newSavedFile = fullfile(sprintf('../../Data/%s/%s_%s_%secc%s/%s', ...
                    subject{1}, count.condition{ii}, count.stabil{ii}, ...
                    string(count.eccDist(ii)), count.side{ii}, sesName{numSess}));
            end
        else
            newSavedFile = fullfile(sprintf('../../Data/%s/%s_%s_%secc%s/%s', ...
                subject{1}, count.condition{ii}, count.stabil{ii}, ...
                string(count.eccDist(ii)), count.side{ii}, sesName{numSess}));
        end
        
%         if ~strcmp(newSavedFile,pathtodata)
%             error('data name and type dont match')
%         end
        
        if noEyeTraces == 0
%             save(fullfile(newSavedFile,'pptrials.mat'), 'pptrials');
            save(fullfile(pathtodata,'pptrials.mat'), 'pptrials');

        elseif noEyeTraces == 1
%             save(fullfile(newSavedFile,'pptrials_noEyeTraces.mat'), 'pptrials');
            save(fullfile(pathtodata,'pptrials_noEyeTraces.mat'), 'pptrials');

        end
        clear count
        clear pptrials
        clear pathtodata
    end
end


function data = addDebug(DATDir,data)
% if isfield(data,'info')
%     warning('info field exists, so can use filename, which would be better, so develop option here')
% end
% % FileList = dir([DATDir '*.dat']);
DATDir = [DATDir filesep];
FileList = dir(fullfile(DATDir, '*.dat'));
% if length(FileList) ~= length(data)
% %     error('filelists are not equal, need to develop some options here')
%     %     warning('The debug data doesnt match %s', DATDir);
%     %     data = [];
%     %     return;
% end

MATCH = true(size(FileList));
counter = 1;
for i = 1:length(FileList)
    %     if strcmp(FileList(i).name,data{i}.FileName)
    numsDebug = regexp(FileList(i).name,'\d*','Match');
    numsEis = regexp(data{i}.FileName,'\d*','Match');
    if sum(numsDebug{1} == numsEis{2}) == length(numsDebug{1}) && ...
            sum(numsDebug{2} == numsEis{3}) == length(numsDebug{2})
        if  ~sum(numsDebug{3} == numsEis{4}) == length(numsDebug{3})
            warning('there is a problem with the debug trial match')
        elseif sum(numsDebug{3} == numsEis{4}) == length(numsDebug{3})
            
            % read in single debug data file
            [debug, labels, ~] = ddpi_readDEBUG([DATDir FileList(i).name]);
            if size(debug,2) ~= length(labels)
                error('FIX ME!')
            end
            ln = length(data{i}.x.position);
            debug = debug(1:ln,:);
            
            x = debug(:,strcmp(labels,'EyeX'))';
            %     if ~all(size(x) == size(data{i}.x.position))
            %         x = x';
            %     end
            if ~all(x == data{i}.x.position)
                debug(:,~strcmp(labels,'Tag')) = nan;
                MATCH(i) = false;
                fprintf('Match not found - skipping! \n');
                counter = 1 + counter;
            end
            
            if counter > round(length(FileList)*.5)
                warning('Hey there are a lot of not-matches!')
            end
            data{i}.ddpi.debug_matches_eis = MATCH(i);
            data{i}.ddpi.filename = FileList(i).name;
            for f = 1:length(labels)
                data{i}.ddpi.(labels{f}) = debug(:,strcmp(labels,labels{f}))';
            end
        end
    end
end

end
