function [traces, condition, trialChar, em] = ...
    oneSubjectPelli(params, filepath, figures, ~, ~, title_str, subject)


traces = [];
condition = []; 
trialChar = [];
em = [];
%% Fold ECC Data By Type
pptrials = [];

if figures.FOLDED
    if strcmp('Zoe',subject) || ...
            strcmp('AshleyDDPI',subject) || ...
            strcmp('Z138',subject) || ...
            strcmp('Z181',subject) || ...
            strcmp('Z091',subject)
        sides = {'Temp'};
    else
        sides = {'Nasal','Temp'};
    end
    for si = 1:length(sides)
        if strcmp('D',params.machine)
            temp_filepath = sprintf('%s%s', filepath, sides{si});
            [ new_pptrials ] = reBuildPPtrials (temp_filepath, params);
        else
            load(fullfile(temp_filepath, 'pptrials.mat'), 'pptrials');
            pptrials = new_pptrials;
        end
        if strcmp('Z064DDPI',subject)
            warning('Z064 has no %s data for %s?', sides{si}, params.ecc);
            pptrials = [pptrials new_pptrials];
        elseif isempty(new_pptrials)
            error('There is no %s data for %s', sides{si}, params.ecc);
        else
            pptrials = [pptrials new_pptrials];
        end
    end
else
    %% Load PPTrials
    if strcmp('D',params.machine)
        [ pptrials ] = reBuildPPtrials (filepath, params);
    else
        load(fullfile(filepath, ...
            'pptrials.mat'), 'pptrials');
    end
end

% if ~isempty(pptrials)

fprintf('Just loaded data for %s \n', subject)
trialChar = buildTrialCharStruct(pptrials);

%% Check recalibration
% for ii = 1:length(pptrials)
%     offset.xOffsets(ii) = pptrials{ii}.xoffset;
%     offset.yOffsets(ii) = pptrials{ii}.yoffset;
%     
%     offset.xStart(ii) = pptrials{ii}.x.position(1);
%     offset.yStart(ii) = pptrials{ii}.y.position(1);
% end
% save(sprintf('Offset/offset%s.mat',subject),'offset');
% return;


%% Build pixelangle & nasal into params

paramsEcc = 0;
for ii = 1:length(pptrials)
    if isfield(pptrials{ii}, 'pixelAngle')
        params.pixelAngle(ii) = pptrials{1,ii}.pixelAngle;
        pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
    else
        params.pixelAngle(ii) = pptrials{1,ii}.pxAngle;
    end
    %%If Nasal, else if....
    if isfield(pptrials{ii}, 'nasal')
        if pptrials{ii}.nasal == 1
            pptrials{ii}.TargetEccentricity = -(pptrials{ii}.TargetEccentricity);
            paramsEcc = sprintf('-%s', string(regexp(params.ecc,'\d*','Match')));
        else
            paramsEcc = (sprintf('%s', string(regexp(params.ecc,'\d*','Match'))));
        end
    end
end

flds = fields(trialChar);
for fi = 1:length(flds)
    trialChar.(flds{fi}) = cellfun(@(z) z(:).(flds{fi}), pptrials);
end
for ii = 1:length(pptrials)
    trialChar.TargetSize(ii) = double(pptrials{ii}.TargetStrokewidth)*2*pptrials{ii}.pxAngle;
    trialChar.TargetEccentricity(ii) = round(double(pptrials{ii}.TargetEccentricity)*params.pixelAngle(ii)); %picks 1 rounded ecc measure

end

%% Converts pixel --> arcmin
eccPixelAngle = unique(round((trialChar.TargetEccentricity)/5)*5);%%round to nearest 5
eccPixelArcmin = unique(params.pixelAngle);

if figures.FIXATION_ANALYSIS
    trialChar.TargetEccentricity(:) = 0;
end

%have a copy of all the data collected
trialChar.Subject = char(subject);

%% %%%%%%% TRIAL PARAMETERS %%%%%%%%%%%%%%%%%%
%Gathering information from each trial from pptrials
%Double checking to make sure all the data matches the params

for ii = 1:length(pptrials)
    if length(pptrials{ii}.microsaccades.start) > 10
        fprintf('Check trial num %i \n', ii)
    end
  
    targetEcc = string(pptrials{ii}.TargetEccentricity * pptrials{ii}.pxAngle);
%     if targetEcc ~= string(paramsEcc)
% %         warning('Trial num %i is the wrong ecc', ii);
%         %         error('Trial %i is wrong ECC in this!',ii);
%     end
    if unique(trialChar.Uncrowded) > 1
        disp('WARNING, there is more than one crowding condition in this data set');
    end
    if strcmp('A',params.machine)
        pptrials{ii}.pixelAngle = pptrials{ii}.pxAngle;
    end
end

%% TRIAL COUNTS %%%%%%%%%%%
%sorting all inavlid trials from pptrials
%counting the number of each type of invlaid trial
em = [];
[traces, counter, valid, em] = countingTrials(pptrials, em, params, figures);

if strcmp('Drift',params.em)
    tempParams.machine = params.machine;
    tempParams.em = {'MS'};
    tempParams.D = 0;
    tempParams.MS = 1;
    tempParams.pixelAngle = params.pixelAngle;
    tempParams.spanMin = params.spanMin;
    tempParams.spanMax = params.spanMax;
    tempParams.ecc = params.ecc;
    tempParams.sampling = params.sampling;
    tempParams.OnlyTemp = 1;
%     tempParams.subNum = params.subNum;
    [tracesMS, counterMS, validMS, emMS] = countingTrials(pptrials, em, tempParams, figures);
end


if params.D
    em.valid = valid.d;
    em.msValid = validMS.ms;
elseif params.DMS
    em.valid = valid.dms;
elseif params.MS
    em.valid = valid.ms;
end

nameFile = 'pptrials.mat';
if figures.CHECK_TRACES
    if figures.FIXATION_ANALYSIS
        params.fixation = 1;
        fixTRIALS = find(valid.fixation > 0);
        %         [singleSegs,x,y] = getDriftSegmentInfo (fixTRIALS, pptrials, params);
        if ~isempty(fixTRIALS)
            pptrials = checkMSTraces_new(fixTRIALS, pptrials, params, filepath, [], nameFile);
        end
    elseif params.MS
        msTRIALS = find(valid.ms > 0 & trialChar.TimeFixationON == 0);
        [singleSegs,x,y] = getDriftSegmentInfo (msTRIALS, pptrials, params);
        pptrials = checkMSTraces_new(msTRIALS, pptrials, params, filepath, singleSegs, nameFile);
        %         save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    elseif params.D
        driftTRIALS = find(valid.d > 0 & trialChar.TimeFixationON == 0);
        [singleSegs,x,y] = getDriftSegmentInfo (driftTRIALS, pptrials, params);
        pptrials = checkMSTraces_new(driftTRIALS, pptrials, params, filepath, singleSegs, nameFile);
        %         save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    elseif params.DMS
        driftTRIALS = find(valid.dms > 0 & trialChar.TimeFixationON == 0);
        [singleSegs,x,y] = getDriftSegmentInfo (driftTRIALS, pptrials, params);
        pptrials = checkMSTraces_new(driftTRIALS, pptrials, params, filepath, singleSegs, nameFile);
        %         pptrials = checkMSTraces(pptrials);
        fprintf('Saving pptrials\n')
        %         save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    end
end

%% Get RT Time Info

counter = 1;
for ii = 1:length(pptrials)
    if valid.d(ii)
        info.RT(counter) = double(pptrials{ii}.ResponseTime - pptrials{ii}.TimeTargetON);
        info.crowded = params.crowded;
        counter = counter + 1;
    end
end

fprintf('\n RT Crowd = %i, %.1f',params.crowded,mean(info.RT));

save(sprintf('RT_Info_%s_%i.mat',params.subject,params.crowded),'info');

return;
%% HANDY FUNCTIONS
% determineWeirdDSQ(valid,trialChar,driftTrials,pptrials,params)
traces.swIdx = trialChar.TargetStrokewidth(valid.d);

% if strcmp('D',params.machine)
%     params.samplingRate = (1000/330);
% elseif strcmp('A',params.machine)
%     params.samplingRate = 1;
% end

if figures.FIXATION_ANALYSIS
    params.F = 1;
else
    params.F = 0;
end

%% Main EM Analysis %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
condition = [NaN];
if ~figures.QUICK_ANALYSIS
%     [em] = emAnalysis(em, pptrials, valid, trialChar, title_str, params, ...
%         str2double(cell2mat(regexp(params.ecc,'\d*','Match'))), filepath, figures);
    [em] = emAnalysisUpdated11102020(em, pptrials, valid, trialChar, title_str, params, ...
        str2double(cell2mat(regexp(params.ecc,'\d*','Match'))), filepath, figures);
end
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
%% Psychometric Graphing Function %%
if ~figures.FIXATION_ANALYSIS
    figure;
    condition = Psychometric_Graphing(params, str2double(cell2mat(regexp(params.ecc,'\d*','Match'))),...
        valid, trialChar, params.crowded, title_str);
    fprintf('Threshold SW(Size) for %s  = %.3f\n', trialChar.Subject,condition.thresh);
end

%% Graphs Each Trace of the Trials / Makes Movie of Trace
if figures.FIGURE_ON
    figure
    if params.DMS
        MarkerPlotting(params, pptrials, valid.dms, trialChar, title_str, figures.VIDEO)
        
    elseif params.MS
        MarkerPlotting(params, pptrials, valid.ms, trialChar, title_str, figures.VIDEO)
    else
        MarkerPlotting(params, pptrials, valid.d, trialChar, title_str, figures.VIDEO)
    end
end
% end
end

