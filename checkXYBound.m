function [ xValues, yValues] = checkXYBound(xValues, yValues, limNum)
%UNTITLED4 Summary of this function goes here
%   xValues = vector of X Values
%   yValues = vector of Y Values
%   limNum = the limit value for the graph (IE: 30 by 30 arcminutes would
%   be listed as 30)

    for jj = 1:length(xValues)
        if xValues(jj) > limNum || yValues(jj) > limNum
            xValues(jj) = NaN;
            yValues(jj) = NaN;
        elseif xValues(jj) < -limNum || yValues(jj) < -limNum 
            xValues(jj) = NaN;
            yValues(jj) = NaN;
        end
    end
    
%     for kk = 1:length(yValues)
%         if yValues(kk) > limNum
%             yValues(kk) = NaN;
%         elseif yValues < -limNum
%             yValues(kk) = NaN;
%         end
%     end
end

