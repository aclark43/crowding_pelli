function [latex, T] = buildThresholdTable(threshStruct,ii,title_str)


%%%pass in structure that you want created into a table as threshStruct
T = struct2table(threshStruct);

(struct2cell(threshStruct));

%%%this takes the row values from your structure
rowNames = fieldnames(threshStruct);

%%%this reads the actual values of for structure
Condition = struct2cell(threshStruct);

%%%if building large table, this allows for each column to be different
title_str = sprintf('Condition%i', ii);

%%%creates table that you feed into latexTable
T = table(Condition, 'RowNames', rowNames, 'VariableNames', {title_str});


input.tableRowLabels = rowNames;
input.data = T;
input.dataFormat = {'%i'};
% Column alignment ('l'=left-justified, 'c'=centered,'r'=right-justified):
input.tableColumnAlignment = 'c';
% Switch table borders on/off:
input.tableBorders = 1;
% Switch to generate a complete LaTex document or just a table:
input.makeCompleteLatexDocument = 1;
% LaTex table caption:
input.tableCaption = 'MyTableCaption';
% LaTex table label:
input.tableLabel = 'MyTableLabel';
% Switch to generate a complete LaTex document or just a table:
input.makeCompleteLatexDocument = 1;

latex = latexTable(input);



end