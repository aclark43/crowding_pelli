% runMultipleSubjects_NewEyeris
%% crowding analysis for new eyeris (ie saves Pelli font analysis under a
% mat file
clc
clear all

subjectsAll = {'Z160'};
manCheckDone = [0];
output = {'Stabilized'};
conditions = {'Crowded'};

for ss = 1:length(subjectsAll)
    fprintf('Loading %s Data \n',subjectsAll{ss});
    [data{ss}] = loadAcuityPelliData(subjectsAll{ss}, manCheckDone(ss));
    idxName = fieldnames(data{ss});
    %% EM Analysis
    DC = [];
    validForAnalyis = [];
    strs = idxName;
    fprintf('Starting EM Analysis \n');
%     for iter=1:length(strs)
%         num_strs = regexp(strs{iter},'\d*','Match');
%         output(iter) = str2double(num_strs{1});
%     end
    
    for i = 1:length(unique(output))%:4
        currentIdxName = idxName{i};
        
        if manCheckDone(ss) == 0
            dataAll{i}.pptrials = newEyerisEISRead(data{ss}.(currentIdxName));
            %                 if ~exist(sprintf('SavedPPTrials_CleanAOAcuityData/%s', sprintf(subjectsAll{ss})), 'dir')
            %                     mkdir('SavedPPTrials_CleanAOAcuityData', sprintf(subjectsAll{ss}));
            %
            %                     dataAll{i}.pptrials = cleaningTrialsVisually(dataAll{i}.pptrials,...
            %                         'NewFileName',sprintf('pptrials_%s',currentIdxName),...
            %                         'Folder',sprintf('SavedPPTrials_CleanAOAcuityData/%s', subjectsAll{ss}),...
            %                         'Conversion',round(1000/dataAll{i}.pptrials{1}.sRate));
            %                 end
        else
            dataAll{i}.pptrials = newEyerisEISRead(data{ss}.(currentIdxName).pptrials);
        end
        
        xAll = [];
        yAll = [];
        xAllFixation = [];
        yAllFixation = [];
        counter = 1;
        counter2 = 1;
        for ii = 1:length(dataAll{i}.pptrials)
            if ~isfield(dataAll{i}.pptrials{ii},'drifts')
                validForAnalyis{i}(ii) = 0;
                continue;
            end
            if dataAll{i}.pptrials{ii}.fixationTrial
                keepIdx = find(dataAll{i}.pptrials{ii}.x > -60 & ...
                    dataAll{i}.pptrials{ii}.x < 60 & ...
                    dataAll{i}.pptrials{ii}.y > -60 & ...
                    dataAll{i}.pptrials{ii}.y < 60);
                x = dataAll{i}.pptrials{ii}.x(keepIdx);
                y = dataAll{i}.pptrials{ii}.y(keepIdx);
                if length(x) >= 400
                    xAllFixation = [xAllFixation x(1:400) - mean(x(1:400))];
                    yAllFixation = [yAllFixation y(1:400) - mean(y(1:400))];
                end
                %                 bceaFix{i}(counter) = get_bcea(...
                %                         dataAll{i}.pptrials{ii}.x(keepIdx)/60,...
                %                         dataAll{i}.pptrials{ii}.y(keepIdx)/60);
            end
            
            %%%getting correct trials for performance
            valid = countingTrialsAOAcuity(dataAll{i}.pptrials(ii));
            tossstats(ii).saveValid = valid;
            if valid.d || valid.dms
                validForAnalyis{i}(ii) = 1;
            else
                validForAnalyis{i}(ii) = 0;
            end
            for d = 1:length(dataAll{i}.pptrials{ii}.drifts.start)
                driftStart = dataAll{i}.pptrials{ii}.drifts.start(d);
                driftEnd = dataAll{i}.pptrials{ii}.drifts.duration(d) + ...
                    driftStart;
                if driftStart >= length(dataAll{i}.pptrials{ii}.x)
                    continue;
                elseif driftEnd >=length(dataAll{i}.pptrials{ii}.x)
                    driftEnd = length(dataAll{i}.pptrials{ii}.x);
                end
                if dataAll{i}.pptrials{ii}.fixationTrial
                    
                else
                    bceaTask{i}(counter) = get_bcea(...
                        dataAll{i}.pptrials{ii}.x(driftStart:driftEnd)/60,...
                        dataAll{i}.pptrials{ii}.y(driftStart:driftEnd)/60);
                    
                    
                    xAll = [xAll dataAll{i}.pptrials{ii}.x(driftStart:driftEnd)];
                    yAll = [yAll dataAll{i}.pptrials{ii}.y(driftStart:driftEnd)];
                    dataAll{i}.position(counter).x = dataAll{i}.pptrials{ii}.x(driftStart:driftEnd);
                    dataAll{i}.position(counter).y = dataAll{i}.pptrials{ii}.y(driftStart:driftEnd);
                    if length(dataAll{i}.pptrials{ii}.x(driftStart:driftEnd)) >= 256
                        dataAll{i}.positionDC(counter2).x = ...
                            dataAll{i}.pptrials{ii}.x(driftStart:driftStart+255);
                        dataAll{i}.positionDC(counter2).y = ...
                            dataAll{i}.pptrials{ii}.y(driftStart:driftStart+255);
                        counter2 = counter2 + 1;
                    end
                    counter = counter + 1;
                end
            end
        end
        tempDX = {dataAll{i}.positionDC.x};
        tempDY = {dataAll{i}.positionDC.y};
        forDSQCalc.x = tempDX;
        forDSQCalc.y = tempDY;
        xAllSaved{i} = xAll;
        yAllSaved{i} = yAll;
        xAllSavedF{i} = xAllFixation;
        yAllSavedF{i} = yAllFixation;
        [~,~,~, ~, ~, ...
            tempSingleSegmentDsq,~,~] = ...
            CalculateDiffusionCoef(1000, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
        idx2 = [];
        for s = 4:size(tempSingleSegmentDsq)
            %         if isempty(find(tempSingleSegmentDsq(s,:) > 65 | tempSingleSegmentDsq(s,100) > 40)) %Janis
            if strcmp(subjectsAll{ss},'A188')
                if isempty(find(tempSingleSegmentDsq(s,:) > 120 | ...
                        tempSingleSegmentDsq(s,40) > 15)) %Sanjana
                    idx2(s) = 1;
                else
                    idx2(s) = 0;
                end
            elseif  strcmp(subjectsAll{ss},'Sanjana')
                if isempty(find(tempSingleSegmentDsq(s,:) > 120 | tempSingleSegmentDsq(s,50) > 20)) %Sanjana
                    idx2(s) = 1;
                else
                    idx2(s) = 0;
                end
            elseif  strcmp(subjectsAll{ss},'Z091')
                if isempty(find(tempSingleSegmentDsq(s,:) > 80 | ...
                        tempSingleSegmentDsq(s,50) > 10))
                    idx2(s) = 1;
                else
                    idx2(s) = 0;
                end
            else
                if isempty(find(isnan(tempSingleSegmentDsq(s,:)))) %Sanjana
                    idx2(s) = 1;
                    if isempty(find(tempSingleSegmentDsq(s,:) > 120))
                        idx2(s) = 1;
                    else
                        idx2(s) = 0;
                    end
                else
                    idx2(s) = 0;
                end
            end
        end
        forDSQCalc.x = tempDX(find(idx2));
        forDSQCalc.y = tempDY(find(idx2));
        if strcmp (subjectsAll{ss},'Z091')
            [~,dataAll{i}.Bias,dataAll{i}.dCoefDsq, ~, dataAll{i}.Dsq, ...
                dataAll{i}.SingleSegmentDsq,~,~] = ...
                CalculateDiffusionCoef(341, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
        else
            [~,dataAll{i}.Bias,dataAll{i}.dCoefDsq, ~, dataAll{i}.Dsq, ...
                dataAll{i}.SingleSegmentDsq,~,~] = ...
                CalculateDiffusionCoef(1000, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
        end
        DC = [DC dataAll{i}.dCoefDsq];
        allStats{i} = tossstats;
    end
    %% Performance Analysis
    fprintf('Calculating Performance Data \n');
    [acrossSubStats,dataTemp] = ...
        calculatePerformanceAnalysisAcuityPelli(data{ss}, idxName, validForAnalyis, manCheckDone(ss));
    figure;
    plot(acrossSubStats.ecc, acrossSubStats.snellAcuity,'-o')
    xlabel('Stabilized Condition (arcmin)')
    ylabel('Acuity 20/X');
    text(acrossSubStats.ecc,acrossSubStats.snellAcuity,string(acrossSubStats.numTrials));
    % AO = [12359.85958	10813.84851	10091.52408	9277.956081	8737.329778];
    % %Ashley
    % AO = [12684.61208	10159.54458	9646.510861	8013.680678	7277.125707]; %Janis
    [AO] = loadinAOData(subjectsAll{ss});
    aoFreq = [];
    for i = 1:length(idxName)
        D = AO.(idxName{i}).AO.PRL;
        %%% We can compute the sampling limit a... using the following formulas:
        %%% https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6660219/
        aoFreq(i) = (1/2)*(sqrt((2/sqrt(3)) * D)); %%from Rossi Rooda
    end
    % figure;
    clear s
    [~,p,b,r] = LinRegression(acrossSubStats.ecc,acrossSubStats.freqThresh,0,NaN,1,0);
    hold on
    s(1) = plot(acrossSubStats.ecc,acrossSubStats.freqThresh,'o','MarkerFaceColor','g');
    [~,p,b,r] = LinRegression(acrossSubStats.ecc,aoFreq,0,NaN,1,0);
    s(2) = plot(acrossSubStats.ecc,aoFreq,'o','MarkerFaceColor','m');
    ylim([10 65])
    xlim([-3 27])
    line([0 27],[acrossSubStats.freqThresh(1) acrossSubStats.freqThresh(1)],...
        'Color','k','LineStyle','--')
    xticks([0 10 15 25])
    yticks([10 20 30 40 50 60])
    % legend(s,{'Acuity Frequency','Nyquist Frequency (Nasal)'});
    title(subjectsAll{1});
    axis square
    axis square
    xlabel('Eccentricity (arcmin)');
    ylabel('Frequency (cycl/deg)');
    
    
    
    % DCSave = [5.62084627151489,7.25673580169678,9.23340225219727,7.83249378204346];
    figure(100)
    acrossSubStats.SFfromDC = powerAnalysis_JustCriticalFrequency (DC, 0);
    close figure 100
    hold on
    % yyaxis left
    s(3) = plot(acrossSubStats.ecc, acrossSubStats.SFfromDC,'-o');
    %     legend(s,{'Acuity Frequency','Nyquist Frequency (Nasal)',...
    %         'DC Frequency'});
    title(subjectsAll{ss});
    
    allSubjectStats{ss} = acrossSubStats;
    allSubjectStats{ss}.dc = DC;
    allSubjectStats{ss}.bceaTask = bceaTask;
    %     allSubjectStats{ss}.bceaFix = bceaFix;
    allSubjectStats{ss}.xFix = xAllSavedF;
    allSubjectStats{ss}.yFix = yAllSavedF;
    allSubjectStats{ss}.AOFreq = aoFreq;
    allSubjectStats{ss}.xAllSaved = xAllSaved;
    allSubjectStats{ss}.yAllSaved = yAllSaved;
end