function videoPlotting(xx, yy, i)

currPos = plot(xx(1), yy(1), 'g*');
pathPos = plot(xx(1), yy(1), 'm-');

frame_count = 0;

temporal_bins = length(1:5:length(xx));
movie_trial(temporal_bins, 1) = struct('cdata', [], 'colormap', []);

hold on
xlabel('X position');
ylabel('Y position');
axis([-30 30 -30 30])

axis square
fprintf('This is %i graph',i);


for t = [1:5:length(xx), length(xx)]
    frame_count = frame_count + 1;
    
    set(currPos, 'XData', xx(t), 'YData', yy(t));
    set(pathPos, 'XData', xx(1:t), 'YData', yy(1:t));
    
    movie_trial(frame_count) = getframe(gcf);
    pause(.000009)
end
%         input ''
%         clf
videoName = sprintf('../../Videos/Graph%s',i);
v_trial = VideoWriter(videoName, 'Motion JPEG AVI');
v_trial.FrameRate = 10;
open(v_trial);
for ii = 1:temporal_bins
    writeVideo(v_trial, movie_trial(ii));
end
close(v_trial);

% input ''
% clf


end

