function  driftLandingLocation(thresholdSWVals, subjectUnc,subjectThreshUnc,subjectCro, subjectThreshCro,...
    c,subNum, em)


for ii = 1:subNum
        swUnc = thresholdSWVals(ii).thresholdSWUncrowded;
        meanDistLandLoc(ii) = mean(subjectThreshUnc(ii).em.First300.ecc_0.(swUnc).euclidDistofDriftStart);
        meanSpan(ii) = subjectThreshUnc(ii).em.First300.ecc_0.(swUnc).meanSpan;
        titleText = ('Uncrowded first 300ms');
end
figure;
[~,p,~,r] = LinRegression(meanDistLandLoc,meanSpan,0,NaN,1,0);
scatter(meanDistLandLoc, meanSpan,100,c,'filled','d')
axis([0 50 -5 50])
title(titleText)
xlabel('Mean Distance from Center of Landing Loc.(arcmin)')
ylabel('Mean Span')

for ii = 1:subNum
%     thresholdSizeSWRound = round(subjectCro(ii).stimulusSize,1);
%     matchingThresholdRound = round(subjectThreshCro(ii).thresh,1);
%     thresholds(ii) = subjectThreshCro(ii).thresh;
%     [~,thresholdIdx] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    swCro = thresholdSWVals(ii).thresholdSWCrowded;
%     if strcmp('Drift',em)
%         meanDistLandLoc(ii) = mean(subjectThreshCro(ii).em.ecc_0.(swCro).euclidDistofDriftStart);
%         meanSpan(ii) = subjectThreshCro(ii).em.ecc_0.(swCro).meanSpan;
%         titleText = ('Crowded');
%     elseif strcmp('Drift_MS',em)
        meanDistLandLoc(ii) = mean(subjectThreshCro(ii).em.First300.ecc_0.(swCro).euclidDistofDriftStart);
        meanSpan(ii) = subjectThreshCro(ii).em.First300.ecc_0.(swCro).meanSpan;
        titleText = ('Crowded First 300');
%     end
end
figure;
[~,p,~,r] = LinRegression(meanDistLandLoc,meanSpan,0,NaN,1,0);
subjCol = scatter(meanDistLandLoc, meanSpan,100,c,'filled','d');
xlabel('Mean Distance from Center of Landing Loc.(arcmin)')
ylabel('Mean Span')
title(titleText)
axis([0 50 -5 50])
title('Crowded')

end