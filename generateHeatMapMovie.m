function generateHeatMapMovie(posXOG, posYOG,...
    timeStamps, params, result, time, trialChar, lengthVector)

% numSamples = length(timeStamps);
% X =  [0,0,21.21,30,21.21,0,-21.21,-30,-21.21].*0.5;
% Y = [0,30,21.21,0,-21.21,-30,-21.21,0,21.21].*0.5;
% get all data in a vector format
counter = 1;
for ii = 1:(length(posXOG)/lengthVector)
    xStruct(ii,:) = posXOG(counter:counter+(lengthVector-1));
    yStruct(ii,:) = posYOG(counter:counter+(lengthVector-1));
    counter = counter + lengthVector;
end

posX = reshape(xStruct, [1, size(xStruct,1)*size(xStruct,2)]);
posY = reshape(yStruct, [1, size(yStruct,1)*size(yStruct,2)]);
% numMat = round(repmat([1:1:t], numMatLength, 1))*(1000/330);
timeStamps = reshape(timeStamps, [1, size(timeStamps,1)*size(timeStamps,2)]);

load('./MyColormaps.mat')
if ~exist('movies', 'dir')
    mkdir('movies')
end
% initialize movie
v_trial = VideoWriter(sprintf('./movies/%s', trialChar.Subject), 'Motion JPEG AVI');
v_trial.FrameRate = 8;
open(v_trial);
% spatial bins for the heatmaps
NBins = 100;
% temporal bins
TBins = 50; %ms
TimeIntervals = linspace(1, time, time/TBins);
limit.xmin = floor(min(posX));
limit.xmax = ceil(max(posX));
limit.ymin = floor(min(posY));
limit.ymax = ceil(max(posY));
Alph = 0.25;
frame_count = 0;
figure()
for t = 1:length(TimeIntervals)-1
    idx = find(timeStamps<TimeIntervals(t+1) & timeStamps>=TimeIntervals(t));
    
     [ posX(idx), posY(idx)] = checkXYArt(posX(idx), posY(idx), min(round(length(posX(idx))/110)), 61);
%     [ posX(idx), posY(idx)] = checkXYBound(nXValues, nYValues, 60); %Checks boundries
     
     result = MyHistogram2(posX(idx), posY(idx), [limit.xmin,limit.xmax,NBins;limit.ymin,limit.ymax,NBins]);
%     result = histogram2(posX(idx), posY(idx),NBins);
%     result = result.Values;
    result = result./(max(max(result)));
%     result(result==0)=NaN;
    
    figure()
    pH = pcolor(linspace(limit.xmin, limit.xmax, size(result, 1)),...
        linspace(limit.ymin, limit.ymax, size(result, 1)),...
        result');
    set(gcf, 'Colormap', mycmap)
    hold on
    if strcmp('D',params.machine)
        plot(0,0,'-ks','MarkerSize',100*params.pixelAngle);
        graphTitleLine1 = ('Fixation_Trial_Drift_Heat_Map');
    else
        plot(0,0,'-ks','MarkerSize',16/params.pixelAngle);
        graphTitleLine1 = ('Fixation_Trial_Drift_Heat_Map');
    end
    hold on
    plot([-limit.xmin limit.xmax], [0 0], '--k')
    plot([0 0], [-limit.ymin limit.ymax], '--k')  
    set(gca, 'FontSize', 12)
    xlabel('X [arcmin]')
    ylabel('Y [arcmin]') 
    axisValue = 60;
    axis tight
    axis square
    caxis([floor(min(min(result))) ceil(max(max(result)))])
    shading interp;
    axis([-axisValue axisValue -axisValue axisValue]);  
    text(20,17,sprintf('time: %dms', round(TimeIntervals(t+1))), 'FontWeight','bold')
%      text(20,24,sprintf('Number of Trials: %dms', round(TimeIntervals(t+1))), 'FontWeight','bold')
    title('Fixation Trials')
    
    frame_count = frame_count + 1;
    movie_trial(frame_count) = getframe(gcf);
    writeVideo(v_trial, movie_trial(frame_count));
end


close(v_trial);
close all;
end


