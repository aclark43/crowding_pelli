function calculateProbLandingTable(subjectUnc, subjectMSUnc, subNum, subjectCro, subjectMSCro)

condition = {'Uncrowded'};
[sumLandNumTotalU, sumLandBackTotalU,...
    meanAllU, stdAllU] = probLandingTable( subjectUnc, subjectMSUnc, subNum, condition );

condition = {'Crowded'};
 [sumLandNumTotalC, sumLandBackTotalC,...
    meanAllC, stdAllC] = probLandingTable( subjectCro, subjectMSCro, subNum, condition );

sumLandOnNum = (mean(sumLandNumTotalU) + mean(sumLandNumTotalC))/2;
sumLandOnBack = (mean(sumLandBackTotalU) + mean(sumLandBackTotalC))/2;
meanAll = (meanAllU + meanAllC)/2; 
stdAll = (stdAllU + stdAllC)/2;

figure
for ii = 1:length(sumLandNumTotalU)
    scatter(0 ,mean([sumLandNumTotalU(ii) sumLandNumTotalC(ii)]));
    hold on
    scatter(1, mean([sumLandBackTotalU(ii) sumLandBackTotalC(ii)]));
    hold on
end
scatter(0 ,mean([sumLandNumTotalU sumLandNumTotalC]),100,'k','filled');
hold on
scatter(1, mean([sumLandBackTotalU sumLandBackTotalC]),100,'k','filled');
hold on
er = errorbar(0, mean([sumLandNumTotalU sumLandNumTotalC]), std([sumLandNumTotalU sumLandNumTotalC]));
er.Color = [0 0 0];                            
er.LineStyle = 'none';  
hold on
er = errorbar(1, mean([sumLandBackTotalU sumLandBackTotalC]), std([sumLandBackTotalU sumLandBackTotalC]));
er.Color = [0 0 0];                            
er.LineStyle = 'none';  


xlim([-.5 1.5])
[~,p] = ttest([sumLandNumTotalU sumLandNumTotalC], ...
    [sumLandBackTotalU sumLandBackTotalC]);

% bar(categorical({'Landing on Target or Flanker'}),sumLandOnNum,'FaceColor', [1, .3, 0]);
% hold on
% bar(categorical({'Landing on Background'}),sumLandOnBack,'FaceColor', [1, .8, 0]);
xlabel('MS Landing Location');
ylabel('Probability');
title('Probabilities of MS Landing Location');
names = {'Landing on Target or Flanker',...
   'Landing on Background'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',12,...
    'FontWeight','bold')
% hold on
% er = errorbar((categorical([{'Landing on Target or Flanker'} {'Landing on Background'}]))...
%     ,meanAll, stdAll);
% er.Color = [0 0 0];                            
% er.LineStyle = 'none';  
% [~,p] = ttest([sumLandNumTotalU sumLandNumTotalC], ...
%     [sumLandBackTotalU sumLandBackTotalC]);
hold off

text(1,.8,sprintf('p < %.3f',p));
 saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSLandingProbability.png');
 saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSLandingProbability.epsc');

end