% runFiguresForManuscript_Clark2021

close all
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.fig = struct('FIGURE_ON', 0,...
    'FOLDED',0);
fig = params.fig;
subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,...
params.driftSize = 175;
params.em = {'Drift'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
em = params.em;
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
% c = brewermap(12,'Set3');
c = flip(brewermap(subNum,'YlGn'));
% c = jet(length(subjectsAll));
params.c = c;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[tempDataStruct, params] = loadVariablesComplete(params);

if strcmp('Drift',params.em)
    load('MATFiles/drift200_Boots_CleanZ024.mat');
    load('MATFiles/FixationBoots3_175.mat');
%     load('MATFiles/Fixation_11302020175.mat');
    [idx, fixation] =  rebuildFixationData(fix, params);
    params.idx = idx;
    params.fixation = fixation;
end
temp = tempDataStruct.subjectThreshUnc
save('acuityDataEmm.mat','temp');
if strcmp('Drift',params.em)
    for ii = 1:subNum
        allTrialsData.subject{ii} = subjectsAll{ii};
        tempDataStruct.performU(ii).em = tempDataStruct.subjectThreshUnc(ii).em;
        tempDataStruct.performC(ii).em = tempDataStruct.subjectThreshCro(ii).em;
        tempDataStruct.subjectThreshUnc(ii).bootsAll = tempDataStruct.performU(ii).bootStrapAll;
        tempDataStruct.subjectThreshUnc(ii).dsqBoot = drift.First500.bootDSQ{ii};
        tempDataStruct.subjectThreshCro(ii).bootsAll = tempDataStruct.performC(ii).bootStrapAll;
        allTrialsData.dsq(ii) = tempDataStruct.subjectThreshUnc(ii).em.allTraces.dCoefDsq;
        allTrialsData.span(ii) = mean(tempDataStruct.subjectThreshUnc(ii).em.allTraces.span);
        allTrialsData.curve(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.curvature));
        idx = ~cellfun(@isnan,tempDataStruct.subjectThreshUnc(ii).em.allTraces.mn_speed);
        allTrialsData.speed(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.mn_speed(idx)));
        allTrialsData.speedSEM(ii)= sem(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.mn_speed(idx)));
        allTrialsData.curveSEM(ii) = sem(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.curvature));

        allTrialsData.area(ii)= tempDataStruct.subjectThreshUnc(ii).em.allTraces.areaCovered;
        allTrialsData.bcea(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.bcea))/60;%in degrees
        allTrialsData.rms(ii) = rms(tempDataStruct.subjectThreshUnc(ii).em.allTraces.prlDistance)/60; %in degrees
        allTrialsData.sdP(ii) = std(tempDataStruct.subjectThreshUnc(ii).em.allTraces.prlDistance)/60;%in degrees
        allTrialsData.plf(ii) = mean(tempDataStruct.subjectThreshUnc(ii).em.allTraces.prlDistance);
        allTrialsData.acuity(ii) = tempDataStruct.subjectThreshUnc(ii).thresh;
        allTrialsData.shortAnalysis.dsq(ii) = tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.dCoefDsq;
        allTrialsData.shortAnalysis.span(ii)= mean(tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.span);
        allTrialsData.shortAnalysis.curve(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.curvature));
        idx2 = ~cellfun(@isnan,tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.mn_speed);
        allTrialsData.shortAnalysis.speed(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.mn_speed(idx2)));
        allTrialsData.shortAnalysis.area(ii)= tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.areaCovered;
        allTrialsData.thresholds(ii) = tempDataStruct.subjectThreshUnc(ii).thresh;
        allTrialsData.traces{ii}.x = tempDataStruct.subjectThreshUnc(ii).em.allTraces.x;
        allTrialsData.traces{ii}.y = tempDataStruct.subjectThreshUnc(ii).em.allTraces.x;


        [biasMeasure(ii), angleRad(ii)] = ...
            calculateBiasSingleValue(tempDataStruct.subjectThreshUnc(ii).em.allTraces.Bias);
        
        [biasMeasureT(ii), angleRadT(ii)] = ...
            calculateBiasSingleValue(tempDataStruct.subjectThreshUnc(ii).em.ecc_0.(tempDataStruct.thresholdSWVals(ii).thresholdSWUncrowded).Bias);
        %     test(ii) = std(drift.First500.bootDSQ{ii});
    end
end
load('MATFiles/otherStudyComparisonFEM.mat');

[a,bIDX] = sort(allTrialsData.dsq);
c = flip(brewermap(subNum,'YlGn'));

%% Bar Graph for Acuity
figure;
subplot(1,2,1)
bar(sort([allTrialsData.dsq]));
xlabel('Subject')
ylabel('Diffusion Constant (arcmin^2/sec)')
subplot(1,2,2)
bar(sort([allTrialsData.thresholds]))
ylim([1 1.8])
xlabel('Subject')
ylabel('Visual Acuity Threshold')
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/BarThresholds.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/BarThresholds.png');
%% Create X Y Plot
trialNum = 148; %#4 = 36, 139 ; #1 = 13
width = 3;
figure;
subplot(1,3,[1 2])
plot(...
1:length(tempDataStruct.subjectThreshUnc(1).em.allTraces.x{trialNum}),...
tempDataStruct.subjectThreshUnc(1).em.allTraces.x{trialNum},...
'-','LineWidth',width);
hold on
plot(...
1:length(tempDataStruct.subjectThreshUnc(1).em.allTraces.y{trialNum}),...
tempDataStruct.subjectThreshUnc(1).em.allTraces.y{trialNum},...
'-','LineWidth',width);
axis([0 500 -5 5])

subplot(1,3,3)
plot(tempDataStruct.subjectThreshUnc(1).em.allTraces.x{trialNum},...
    tempDataStruct.subjectThreshUnc(1).em.allTraces.y{trialNum},...
    '-','LineWidth',width);
axis([-5 5 -5 5])
axis square

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/Trace.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/Trace.epsc');
%% Linear Mixed Model For Fixation Effects
fIdx = find(~isnan(fixation.First200.dsq));
% [~,p,b,r] = LinRegression(fixation.First200.dsq(fIdx),...
%     (allTrialsData.acuity(fIdx)),0,NaN,1,0);
% [~,p,b,r] = LinRegression(allTrialsData.dsq(fIdx),...
%     (allTrialsData.acuity(fIdx)),0,NaN,1,0);
% allTrialsData.dsq
X = [allTrialsData.dsq(fIdx)',fixation.First200.dsq(fIdx)'];
lme = fitlm(X,allTrialsData.acuity(fIdx))
figure;
plot(lme)

allDC = [fixation.First200.dsq(fIdx) allTrialsData.dsq(fIdx)];
allDC_c = allDC - mean(allDC);
fixationIdx = [1 1 1 1 1 1 1 1 1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
subject = [1:9 1:9];
allAcuity = [allTrialsData.acuity(fIdx) allTrialsData.acuity(fIdx)];
tbl = table(allDC',...
    allDC_c',...
    fixationIdx',...
    [allTrialsData.acuity(fIdx) allTrialsData.acuity(fIdx)]', subject',...
    'VariableNames',{'DC_All','DC_All_Centered',...
    'If_Fixation','Acuity_Pelli', 'Subject'});
% m = fitlm(tbl,'MPG~Weight+Acceleration')

%%Is the slope of the fix vs task data significantly different?
lme2 = fitlm(tbl,'Acuity_Pelli~If_Fixation*DC_All_Centered') 

lme3 = fitlme(tbl,'Acuity_Pelli~DC_All_Centered*If_Fixation+(DC_All_Centered*If_Fixation|Subject)')

% DC_All_Centered:If_Fixation - p = .38253, suggesting we cannot reject the
% null hypothesis that the underlying mechanism is the same. (ie: the slope
% in these 2 conditions is the same.
figure;subplot(1,2,1); plot(lme2)
subplot(1,2,2); [~,p,b,r] = LinRegression(allDC,...
    [allAcuity],0,NaN,1,0);
ylim([1 2])
% Next, we use all of the data we have and re-parameterize/recode the same
% LM we have already fit to read out the simple effects of 
% DiffusionConstant at each level of condition - use the " / " symbol
[~,p1,b,r] = LinRegression(allDC(find(fixationIdx)),...
    [allAcuity(find(fixationIdx))],0,NaN,1,0);

[~,p2,b,r] = LinRegression(allDC(find(~fixationIdx)),...
    [allAcuity(find(~fixationIdx))],0,NaN,1,0);%%   Same as in paper but 
% without subject who has fixation

% [~,p2,b,r] = LinRegression(allTrialsData.dsq(fIdx),...
%     allTrialsData.acuity(fIdx),0,NaN,1,0);
% 
[~,p3,b,r] = LinRegression(allTrialsData.dsq,...
    allTrialsData.acuity,0,NaN,1,0);


%%%Notes from flo if we want to try further trial level analysis
% Accuracy ~ 1 + size * DC + (1 + size * DC | subject)
% 
% Mixed Logistic Regression
% * Take into account the data is bounded
% * Takes Trial Level Data (size) will give you the main effect of DC on trial level accuracy, and how does the slope change based on size
% * More accurately accounting for any uncertainty
% 
% Advantage: small step to account for autocorrelations....

ylim([1 2]);
%% Percent of Tossed Trials for Gaze larger thatn 30arcmin
percents = [1.3 0.3 0.7 0.2 0.2 ...
    1.6 0.7 1.0 0.9 6.4];

mean(percents);
sem(percents);
%% Power Curves
[critFreq, ~, sumNorm] = powerAnalysis_JustCriticalFrequency ([5 20], 1);
ylim([.7 1.05])
xlim([5 100])
diff = sumNorm(2) - sumNorm(1); % x2 and x1 are your input variables. x1 is reference and x2 the value to compare
relDiff = diff / sumNorm(1);

diff = 1.8 - 1.2; % x2 and x1 are your input variables. x1 is reference and x2 the value to compare
relDiff = diff / 1.2;


saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/MinMaxCritFreq.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/MinMaxCritFreq.epsc');
%% Span vs Acuity
figure;
[~,p,b,r] = LinRegression(allTrialsData.span,allTrialsData.acuity,0,NaN,1,0);
hold on
for ii = 1:subNum
    plot(allTrialsData.span(ii),allTrialsData.acuity(ii),...
        'o','MarkerFaceColor',c(bIDX == ii,:),'Color','k','MarkerSize',14);
end
ylim([1 2])
xlim([1.5 5])
ylabel('Visual Acuity (arcmin)');
xlabel('Span (arcmin^2)');
set(gca,'FontSize',16)
axis square
text(3.5,1.3,sprintf('p = %.3f',p),'FontSize',14)
text(3.5,1.2,sprintf('r = %.3f',r),'FontSize',14)
text(3.5,1.1,sprintf('b = %.3f',b),'FontSize',14)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/SpanVsAcuity.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/SpanVsAcuity.epsc');
%% Appendix Figure Curvature vs Speed Each SW
idx = ~isnan(fixation.First200.dsq);
params.idx = idx;
driftParamsNormalizedHist(drift, fixation, params, params.idx)
%% Curvate and Speed Histograms
[~,orderDC] = sort(allTrialsData.dsq);
clear leg
figure('Position',[2000 300 1000 500]);
subplot(2,3,[1 2 3])
leg = bar([normalizeVector(allTrialsData.dsq(orderDC));...
    normalizeVector(drift.First500.curve(orderDC));...
    normalizeVector(drift.First500.speed(orderDC))]')
xlim([0 11])
ylim([0 1.1])
ylabel('Normalized Measures')
xlabel('Subject')
legend(leg,{'DC','Curve','Speed'},'Location','southeast');

subplot(2,3,4)
bar(allTrialsData.dsq(orderDC));
xlim([0 11])
ylabel('DC');

subplot(2,3,5)
bar(drift.First500.curve(orderDC),'c');
hold on
errorbar([1:10],drift.First500.curve(orderDC),allTrialsData.curveSEM(orderDC),'.')
xlim([0 11])
ylim([9 16])
ylabel('Curvature');

subplot(2,3,6)
bar(drift.First500.speed(orderDC),'y');
hold on
errorbar([1:10],drift.First500.speed(orderDC),allTrialsData.speedSEM(orderDC),'.')
xlim([0 11])
ylim([30 62])
ylabel('Speed');

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/SpeedCurvatureOrderedBar.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/SpeedCurvatureOrderedBar.epsc');
%% Looking at Snellen Chart instead of Pelli acuity
T = table2struct(readtable...
    ('../../SummaryDocuments/ForMartina_TableDataCollection.xlsx','Range','A1:F15','Sheet','Sheet2'));

for i = 1:length(subjectsAll)
    for ii = 1:size(T)
        if strcmp(T(ii).Subject, subjectsAll{i})
            allTrialsData.eyeChartAcuity{i} = T(ii).Eye_Chart_Acuity;
        elseif strcmp(T(ii).Subject, {'Z055 (Ashley)'})
            allTrialsData.eyeChartAcuity{i} = T(ii).Eye_Chart_Acuity;
        end
    end
end
%LogMAR VA = LogMAR value of the best line read + 0.02 X (number of optotypes missed)
% IE 20/20 -1 --> -log10(20/20) + 0.02 * 1 = 0.14
%    20/15 -1 --> -log10(20/16)

allTrialsData.marEyeChartAcuity = ([-log10(20/13) + 0.02 * 1 ...
    -log10(20/13) + 0.02 * (2) ... %%changed from 20/15 8/17/2021
    -log10((20/20))+ 0.02 * 1 ...
    -log10(20/13) ...
    -log10(20/13) ...
    -log10(20/20) ...
    -log10((20/15))+ 0.02 *(2) ...
    -log10((20/15))+ 0.02 *(2) ...
    -log10((20/13))+ 0.02 *(2) ...
    -log10(20/20)]);

figure;
subplot(1,2,1)
[~,p,b,r] = LinRegression(allTrialsData.dsq,allTrialsData.acuity,0,NaN,1,0);
hold on
for ii = 1:subNum
    plot(allTrialsData.dsq(ii),allTrialsData.acuity(ii),...
        'o','MarkerFaceColor',c(bIDX == ii,:),'Color','k');
end
ylim([1 2])
ylabel('Pelli Acuity (arcmin strokwidth)');
xlabel('Diffusion Constant (arcmin^2/sec)');
axis square

% subplot(1,2,2)
% [~,p,b,r] = LinRegression(allTrialsData.dsq,(allTrialsData.marEyeChartAcuity),0,NaN,1,0);
% hold on
% ylim([-0.3 0.1])
% yticks([-0.3 -0.2 -0.1 0 0.1])
% ylabel('LogMAR Acuity');
% hold on
% yyaxis right
% [~,p,b,r] = LinRegression(allTrialsData.dsq,(allTrialsData.marEyeChartAcuity),0,NaN,1,0);
% for ii = 1:subNum
%     plot(allTrialsData.dsq(ii),allTrialsData.marEyeChartAcuity(ii),...
%         'o','MarkerFaceColor',c(bIDX == ii,:),'Color','k');
% end
% ylim([-0.3 0.1])
% yticks([-0.3 -0.2 -0.1 0 0.1])
% yticklabels({'20/10','20/12.5','20/16','20/20','20/25'})
% ylabel('Snellen Fraction');
% xlabel('Diffusion Constant (arcmin^2/sec)');
% axis square
figure;
fIdx = find(~isnan(fixation.First200.dsq));
[~,p,b,r] = LinRegression(fixation.First200.dsq(fIdx),...
    (allTrialsData.marEyeChartAcuity(fIdx)),0,NaN,1,0);
hold on
ylim([-0.3 0.1])
yticks([-0.3 -0.2 -0.1 0 0.1])
ylabel('LogMAR Acuity');
hold on
yyaxis right
[~,p,b,r] = LinRegression(fixation.First200.dsq(fIdx),...
    (allTrialsData.marEyeChartAcuity(fIdx)),0,NaN,1,0);
for ii = (fIdx)
    plot(fixation.First200.dsq(ii),allTrialsData.marEyeChartAcuity(ii),...
        'o','MarkerFaceColor',c(bIDX == ii,:),'Color','k','MarkerSize',12);
end
ylim([-0.3 0.1])
yticks([-0.3 -0.2 -0.1 0 0.1])
yticklabels({'20/10','20/12.5','20/16','20/20','20/25'})
ylabel('Snellen Fraction');
xlabel('Fixation Diffusion Constant (arcmin^2/sec)');
axis square
set(gca,'FontSize',16)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/FixationvsSnellenAcuity.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/FixationvsSnellenAcuity.epsc');

% saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/SnellenAcuityVsPelli.png');
% saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/SnellenAcuityVsPelli.epsc');
%% Correlation Fixation and Pelli
figure;
fIdx = find(~isnan(fixation.First200.dsq));
[~,p,b,r] = LinRegression(fixation.First200.dsq(fIdx),...
    (allTrialsData.acuity(fIdx)),0,NaN,1,0);
%% Correlation betwen Pelli and Snellen Acuity
figure;
subplot(1,2,1)
[~,p,b,r] = LinRegression(allTrialsData.acuity,allTrialsData.marEyeChartAcuity, 0,NaN,1,0);
for ii = 1:subNum
    plot(allTrialsData.acuity(ii),allTrialsData.marEyeChartAcuity(ii),...
        'o','MarkerFaceColor',c(bIDX == ii,:),'Color','k');
end
xlim([1 2])
ylim([-0.3 0.1])
yticks([-0.3 -0.2 -0.1 0 0.1])
ylabel('LogMAR Acuity');
yyaxis right
ylim([-0.3 0.1])
yticks([-0.3 -0.2 -0.1 0 0.1])
yticklabels({'20/10','20/12.5','20/16','20/20','20/25'})
ylabel('Snellen Fraction');
xlabel('Pelli Acuity (arcmin strokwidth)');

axis square
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/SnellenAcuityVsPelliDirect.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/SnellenAcuityVsPelliDirect.epsc');
%% MS Rate
for ii = 1:length(subjectsAll)
    msStuff = load(sprintf('MATFiles/%s_Uncrowded_Unstabilized_Drift_MS_0ecc_DriftChar.mat',subjectsAll{ii}));
    clear tempNames
    tempNames = fieldnames(msStuff.em.msAnalysis);
    for i = 1:length(tempNames)
        msInfo(ii).msRate(i) = mean(msStuff.em.msAnalysis.(tempNames{i}).microsaccadeRate);
    end
end
for ii = 1:length(subjectsAll)
    msRate(ii) = nanmean(msInfo(ii).msRate);
end
figure;
[~,p,b,r] = LinRegression(msRate,(allTrialsData.acuity),0,NaN,1,0);
%% Time on target by performance
justThreshSW = 0;
[distAllTabUnc] = ...
    buildDCStruct(justThreshSW, params, tempDataStruct.subjectUnc, tempDataStruct.subjectThreshUnc, tempDataStruct.subjectCro, tempDataStruct.subjectThreshCro, tempDataStruct.thresholdSWVals);
justThreshSW = 1;
[distThreshTabUnc, distThreshTabCro] = ...
    buildDCStruct(justThreshSW, params, tempDataStruct.subjectUnc, tempDataStruct.subjectThreshUnc, tempDataStruct.subjectCro, tempDataStruct.subjectThreshCro, tempDataStruct.thresholdSWVals);
load('bootAndSize.mat');
clear uncr
clear cro
clear corU
clear corC
clear sizeU
clear sizeC
[SubjectOrderDC,SubjectOrderDCIdx] = sort(allTrialsData.dsq);
load('MATFiles/AllTrialDCBoots.mat')
for ii = 1:subNum
    errhigh(ii) = stats500{ii}.ci(2)-allTrialsData.dsq(ii);
    errlow(ii) =  allTrialsData.dsq(ii)-stats500{ii}.ci(1);
end

ii = [];
i = [];

for ii = 1:params.subNum
    i = [];
    for i = 1:length(distAllTabUnc)
        sizeTesting = unique(round(distAllTabUnc(i).stimSize,1));
        if distAllTabUnc(i).subject == ii &&...
                ( (sizeTesting) >= 1.2 && (sizeTesting) <= 1.5)
            %                 ( sizeTesting >= .8 && sizeTesting <= 1.2) %LessThan
            %                 ( sizeTesting >= 1.5 && sizeTesting <= 2.0) %GreatThan
            %                 ( sizeTesting >= 1.2 && sizeTesting <= 1.5) %AtThresh
            %
            %
            sizeU(ii) = unique(distAllTabUnc(i).stimSize);
            uncr(ii) = distAllTabUnc(i).dCoefDsq;
            [upperX(ii), lowerX(ii)] = confInter95(bootAndSize.dc{ii,6}');
            corU(ii) = mean(distAllTabUnc(i).correct);
            corUSEM(ii) = sem(distAllTabUnc(i).correct);
        end
    end
    i = [];

end
figure;
[t,p,b1,r1] = LinRegression([uncr],...
    [corU],0,NaN,1,0);
hold on
errorbar([uncr],[corU],corUSEM,'vertical','o','Color','k');
hold on
errorbar([uncr],[corU],errhigh, errlow, 'horizontal','o','Color','k');
for ii = 1:params.subNum
    plot(uncr(ii),...
        corU(ii),'o','Color','k',...
        'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),...
        'MarkerSize',10);
end
axis([0 25 0.2 1])
axis square
text(5,.4,sprintf('p = %.3f',p))
text(5,.35,sprintf('b = %.3f',b1))
text(5,.3,sprintf('r = %.3f',r1))
title('Uncrowded')
xlabel('DC at 1.5"')
ylabel('Performance');
%% Looking at % of time on target by performance
% stimWidth = 1.5;
% areaSizeX = (stimWidth/2)+5;
% areaSizeY = ((stimWidth/2) * 5)+5;
areaSizeX = 8;
areaSizeY = 8;
for ii = 1:subNum
    x = tempDataStruct.subjectThreshUnc(ii).em.allTraces.xALL;
    y = tempDataStruct.subjectThreshUnc(ii).em.allTraces.yALL;
    temp = (x < areaSizeX & x > -areaSizeX & y < areaSizeY & y > -areaSizeY);
    
    allTrialsData.TimeWithin15stim(ii) = round(sum(temp)/length(temp),2)*500;
    allTrialsData.percentWithin15stim(ii) = ((round(sum(temp)/length(temp),2)*500)/500) * 100;
    allTrialsData.probabilityInThis(ii) = round(sum(temp)/length(temp),2)*100;
end
std(allTrialsData.TimeWithin15stim)
mean(allTrialsData.TimeWithin15stim)

std(allTrialsData.percentWithin15stim)
mean(allTrialsData.percentWithin15stim)

std(allTrialsData.probabilityInThis)
mean(allTrialsData.probabilityInThis)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/PerformanceForSmallVsLargeDCSubj.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/PerformanceForSmallVsLargeDCSubj.epsc');

figure;
subplot(1,2,1)
[t,p,b1,r1] = LinRegression([allTrialsData.probabilityInThis],...
    [corU],0,NaN,1,0);
ylim([0.25 1.00])
xlabel('Probability of Gaze Within 16x16')
ylabel('Performance');
axis square

% figure;
subplot(1,2,2)
[t,p,b1,r1] = LinRegression([allTrialsData.TimeWithin15stim],...
    [corU],0,NaN,1,0);
ylim([0.25 1.00])
xlabel('MS of Time on Target')
ylabel('Performance');
axis square

subplot(1,2,2)
[t,p,b1,r1] = LinRegression([allTrialsData.percentWithin15stim],...
    [corU],0,NaN,1,0);
ylim([0.25 1.00])
xlabel('% of Time on Target')
ylabel('Performance');
axis square
suptitle(sprintf('Performance for a %.2f SW Stim', stimWidth))
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/appendix/PercentAndTimevsPerformance.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/appendix/PercentAndTimevsPerformance.epsc');
%% PRL by Performance
for ii = 1:subNum
    allTrialsData.prlDistance(ii) = mean(tempDataStruct.subjectThreshUnc(ii).em.allTraces.prlDistance); 
end

for ii = 1:subNum
    x = tempDataStruct.subjectThreshUnc(ii).em.allTraces.xALL;
    y = tempDataStruct.subjectThreshUnc(ii).em.allTraces.yALL;
    temp(ii) = hypot(mean(x),mean(y));
    temp2(ii) = mean(sqrt((x).^2 + (y).^2)); %average after
%     
%     allTrialsData.TimeWithin15stim(ii) = round(sum(temp)/length(temp),2)*500;
%     allTrialsData.percentWithin15stim(ii) = ((round(sum(temp)/length(temp),2)*500)/500) * 100;
%     allTrialsData.probabilityInThis(ii) = round(sum(temp)/length(temp),2)*100;
end

figure;
subplot(1,2,1)
[t,p,b1,r1] = LinRegression([temp],...
    [corU],0,NaN,1,0);
ylim([0.25 1.00])
xlabel('PRL Distance (all trials)')
ylabel('Performance');
axis square

swNameFor15Stim = {'strokeWidth_3','strokeWidth_3','strokeWidth_3','strokeWidth_3',...
    'strokeWidth_3','strokeWidth_5','strokeWidth_4','strokeWidth_4',...
    'strokeWidth_4','strokeWidth_4'};
for ii = 1:subNum
    allTrialsData.prlDistance15(ii) = mean(tempDataStruct.subjectThreshUnc(ii).em.ecc_0.(swNameFor15Stim{ii}).prlDistance); 
end
% figure;
subplot(1,2,2)
[t,p,b1,r1] = LinRegression([allTrialsData.prlDistance15],...
    [corU],0,NaN,1,0);
ylim([0.25 1.00])
xlabel('PRL Distance (1.5 size trials)')
ylabel('Performance');
axis square
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/appendix/PRLFor15StimPerformance.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/appendix/PRLFor15StimPerformance.epsc');
%% Make Table
for ii = 1:length(subjectsAll)
    forTable.(subjectsAll{ii}).thresh = tempDataStruct.subjectThreshUnc(ii).thresh;
    forTable.(subjectsAll{ii}).numTrials = length(tempDataStruct.subjectThreshUnc(ii).em.allTraces.xRecentered);
    forTable.(subjectsAll{ii}).dsq = (tempDataStruct.subjectThreshUnc(ii).em.allTraces.dCoefDsq);

end


