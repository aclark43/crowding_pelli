function [small, large] = getPLFSmallLargeCondition(distAllTabCro, distThreshTabCro, params, subjectsAll)
for i = 1:params.subNum
    prl = [];
    idx = [];
    counter = 1;
    for ii = 1:length(distAllTabCro)
        subSelect = i;
        if (distAllTabCro(ii).subject == subSelect) &...
                round(distAllTabCro(ii).stimSize,1) == round(distThreshTabCro(i).stimSize,1)
%                 strcmp(distAllTabCro(ii).swSize,(cell2mat(distThreshTabCro(i).swSize)))
            %                         distAllTabCro(ii).swSize == (cell2mat(distThreshTabCro(i).swSize))
            idx(counter) = ii;
            %                     prl(counter) = distAllTabCro(ii).meanPRLDistance;
            prl{counter} = distAllTabCro(ii).PRLDistance;
            counter = counter + 1;
        end
    end
    if length(prl) > 1
        for prlIdx = 1:2
            prl{1} = [prl{1} prl{2}];
            prl{2} = [];
        end
    end
%     meanPRLAllSW = (cellfun(@mean,prl));
%     sdPRLAllSW = (cellfun(@std,prl));
    meanPRLAllSW = mean(prl{1});
    sdPRLAllSW = std(prl{1});
    %             cellIdx = [];
    counterLarge = 1;
    counterSmall = 1;
    for ii = idx
        for prlIdx = 1:length(distAllTabCro(ii).PRLDistance)
                if strcmp('Z014',subjectsAll(i))
                    stdMulti = .8;
                    smallStdMulti = .8;
                elseif strcmp('Z084',subjectsAll(i))
                    stdMulti = .6;
                    smallStdMulti = .3;
                elseif strcmp('Z046',subjectsAll(i))
                    stdMulti = .4;
                    smallStdMulti = .32;
                elseif strcmp('Z064',subjectsAll(i))
                    stdMulti = .6;
                    smallStdMulti = .72;
                elseif strcmp('Z024',subjectsAll(i))
                    stdMulti = 1;
                    smallStdMulti = .2;
                elseif strcmp('Z013',subjectsAll(i))
                    stdMulti = .2;
                    smallStdMulti = .143;
                elseif strcmp('Z002',subjectsAll(i))
                    stdMulti = .5;
                    smallStdMulti = .357;
                elseif strcmp('Z002DDPI',subjectsAll(i))
                    stdMulti = .5;
                    smallStdMulti = .357;
                elseif strcmp('Z005',subjectsAll(i))
                    stdMulti = .6;
                    smallStdMulti = .84;
                elseif strcmp('Z023',subjectsAll(i))
                    stdMulti = 1;
                    smallStdMulti = .4;
                elseif strcmp('Ashley',subjectsAll(i))
                    stdMulti = 1.2;
                    smallStdMulti = 0.857;
                elseif strcmp('AshleyDDPI',subjectsAll(i))
                    stdMulti = 1.2;
                    smallStdMulti = 0.857;
                end
            if distAllTabCro(ii).PRLDistance(prlIdx) > meanPRLAllSW + (sdPRLAllSW*stdMulti)
                large(i).prlLarge(counterLarge) = distAllTabCro(ii).PRLDistance(prlIdx);
                large(i).performance(counterLarge) = distAllTabCro(ii).correct(prlIdx);
                counterLarge = counterLarge + 1;
            elseif distAllTabCro(ii).PRLDistance(prlIdx) < meanPRLAllSW - (sdPRLAllSW*smallStdMulti)
                small(i).prlSmall(counterSmall) = distAllTabCro(ii).PRLDistance(prlIdx);
                small(i).performance(counterSmall) = distAllTabCro(ii).correct(prlIdx);
                counterSmall = counterSmall + 1;
            end
        end
    end
    small(i).meanPRL = mean(small(i).prlSmall);
    small(i).meanPerformance = mean(small(i).performance);
    large(i).meanPRL = mean(large(i).prlLarge);
    large(i).meanPerformance = mean(large(i).performance);
end
end