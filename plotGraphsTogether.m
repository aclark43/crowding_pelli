%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

close all
clc
clear

%%Figure Location & Name%%
% subject = {'Ashley'};
% condition1 = {'Uncrowded', 'Crowded', 'Crowded', 'Uncrowded'}; %Figure 1 and Figure 2
% condition2 = {'Crowded', 'Uncrowded', 'Crowded', 'Uncrowded'}; %Figure 1 and Figure 2
% stabilization1 = {'Stabilized', 'Unstabilized', 'Unstabilized', 'Stabilized'};
% stabilization2 = {'Unstabilized','Stabilized', 'Unstabilized', 'Stabilized'};
% em1 = {'Drift', 'MS', 'Drift_&_MS','Drift', 'MS', 'Drift_&_MS'};
% em2 = {'MS', 'Drift_&_MS', 'Drift', 'MS', 'Drift_&_MS', 'Drift'};
% ecc = {'0ecc'};

subjects = {'Z023'};%,'Z005','Z023','Z002','Ashley','Z013'};
subjects2 = {'Z023'};
condition1 = {'Uncrowded'}; %Figure 1 and Figure 2
condition2 = {'Crowded'}; %Figure 1 and Figure 2
stabilization1 = {'Unstabilized'};
stabilization2 = {'Unstabilized'};
em1 = {'Drift'};
em2 = {'Drift'};
ecc1 = {'0ecc'};
ecc2 = {'0ecc'};
% em3 = {'Drift_MS'};%%%%%%%%%%%%%%%%%
% ecc3 = {'15eccTemp'};%%%%%%%%%%%%%%%%%%
% em4 = {'Drift_MS'};%%%%%%%%%%%%%%%%%
% ecc4 = {'25eccTemp'};%%%%%%%%%%%%%%%%%%

% ecc2 = {'10eccNasal'};
% em3 = {'Drift_MS'};%%%%%%%%%%%%%%%%%
% ecc3 = {'15eccNasal'};%%%%%%%%%%%%%%%%%%
% em4 = {'Drift_MS'};%%%%%%%%%%%%%%%%%
% ecc4 = {'25eccNasal'};%%%%%%%%%%%%%%%%%%

for ii = 1:length(subjects)
    subject = subjects(ii);
for condIdx1 = 1:length(condition1)
    currentCond1 = condition1{condIdx1};
    for subIdx = 1:length(subject)
        currentSubj = subject{subIdx};
        for condIdx2 = 1:length(condition2)
            currentCond2 = condition2{condIdx2};
            for stabIdx1 = 1:length(stabilization1)
                currentStab1 = stabilization1{stabIdx1};
                for stabIdx2 = 1:length(stabilization2)
                    currentStab2 = stabilization2{stabIdx2};
                    for emIdx1 = 1:length(em1)
                        currentEm1 = em1{emIdx1};
                        for emIdx2 = 1:length(em2)
                            currentEm2 = em2{emIdx2};
                            for emIdx3 = 1:length(em3)%%%%%%%%%%%%%%%
                                currentEm3 = em3{emIdx3};%%%%%%%%%%%%%%
                                for emIdx4 = 1:length(em4)%%%%%%%%%%%%%%%
                                    currentEm4 = em3{emIdx4};%%%%%%%%%%%%%%
                                    for eccIdx1 = 1:length(ecc1)
                                        currentEcc1 = ecc1{eccIdx1};
                                        for eccIdx2 = 1:length(ecc2)
                                            currentEcc2 = ecc2{eccIdx2};
                                            for eccIdx3 =...
                                                    1:length(ecc3)%%%%%%%%%%%%%%%%%
                                                currentEcc3 =...
                                                    ecc3{eccIdx3};%%%%%%%%%%%%%%%%%
                                                for eccIdx4 =...
                                                        1:length(ecc4)%%%%%%%%%%%%%%%%%
                                                    currentEcc4 =...
                                                        ecc4{eccIdx4};%%%%%%%%%%%%%%%%%
                                                    
                                                    
                                                    title1 = sprintf('%s_%s_%s_%s_%s', subjects2{ii}, currentCond1, currentStab1, currentEm1, currentEcc1); %Name of File FIG 1
                                                    title2 = sprintf('%s_%s_%s_%s_%s', currentSubj, currentCond2, currentStab2, currentEm2, currentEcc2); %Name of File FIG 2
                                                    title3 =...
                                                        sprintf('%s_%s_%s_%s_%s',...
                                                        currentSubj, currentCond2,...
                                                        currentStab2, currentEm3,...
                                                        currentEcc3); %Name of File
                                                    %%%%%%%%%%FIG 2%%%%%%%%%%%%%
                                                    title4 = sprintf('%s_%s_%s_%s_%s', currentSubj, currentCond2, currentStab2, currentEm4, currentEcc4); %Name of File FIG 2
                                                    pathToData = sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/FIG', subject{subIdx}); %File Location
                                                    if strcmp(title1,title2) == 1
                                                        return
                                                    end
                                                    
                                                    color = copper(4);
                                                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %% FIGURE 1 %%
                                                    pathToData = sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/FIG', subjects2{1}); %File Location
                                                    color1 = [color(1,1) color(1,2) color(1,3)];
                                                    fH1 = createGraphTogetherMultipleSubjWithText...
                                                        (title1,pathToData,currentCond1,currentStab1,currentEm1,currentEcc1,color1,0.3);
                                                    
                                                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %% FIGURE 2 %%
                                                    %%%%%%%%%%%%%%
                                                    
                                                    pathToData = sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/FIG', subject{subIdx}); %File Location
                                                    color2 = [color(2,1) color(2,2) color(2,3)];
                                                    fH2 = createGraphTogetherMultipleSubjWithText...
                                                        (title2,pathToData,currentCond2,currentStab2,currentEm2,currentEcc2,color2,0.1);
                                                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    %% FIGURE 3 %%
                                                    %%%%%%%%%%%%%%
                                                    pathToData = sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/FIG', subject{subIdx}); %File Location
                                                    color3 = [color(3,1) color(3,2) color(3,3)];
                                                    fH3 = createGraphTogetherMultipleSubjWithText...
                                                        (title3,pathToData,currentCond2,currentStab2,currentEm3,currentEcc3,color3,-0.1);
                                                    
                                                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                    % figure 4
                                                    pathToData = sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/FIG', subject{subIdx}); %File Location
                                                    color4 = [color(4,1) color(4,2) color(4,3)];
                                                    fH4 = createGraphTogetherMultipleSubjWithText...
                                                        (title4,pathToData,currentCond2,currentStab2,currentEm4,currentEcc4,color4,-0.3);
                                                    
                                                    %% Figures Together%%
                                                    title_str = sprintf('%s', subject{1});
                                                    title(title_str);
                                                    set(gca, 'FontSize', 11, 'FontWeight', 'bold');
                                                    
                                                    ax1 = get(fH1, 'Children');
                                                    ax2 = get(fH2, 'Children');
                                                    ax2p = get(ax2(1),'Children');
                                                    ax3 = get(fH3, 'Children');
                                                    ax3p = get(ax3(1),'Children');
                                                    ax4 = get(fH4, 'Children');
                                                    ax4p = get(ax4(1),'Children');
                                                    
                                                    copyobj(ax2p, ax1(1));
                                                    copyobj(ax3p, ax1(1));
                                                    copyobj(ax4p, ax1(1));
                                                    close Figure 2
                                                    close Figure 3
                                                    close Figure 4
                                                    
                                                    title_str = sprintf('%s, %s', currentSubj, currentCond1);
                                                    title(title_str);
                                                    set(gca, 'FontSize', 20, 'FontWeight', 'bold');
                                                      axis([0 5 0 1])
                                                    
                                                                                   saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/%s.epsc', currentSubj, sprintf('%s_VS_%s',title1,title2)));
                                                    
                                                    saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/FIG/%s.fig', subject{1}, sprintf('%s_VS_%s_VS_%s',title1,title2,title3,title4)));
                                                    saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/JPEG/%s.png', subject{1}, sprintf('%s_VS_%s_VS_%s',title1,title2,title3, title4)));
                                                    saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/%s.epsc', subject{1}, sprintf('%s_VS_%s_VS_%s',title1,title2,title3, title4)));
                                                    
%                                                                                                  close all
                                                    
                                                    
                                                    
                                                    
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
end

