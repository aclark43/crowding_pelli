%%% Analyze Potential Intro of Offset
clc
clear all

% data.normal = load('X:\Ashley\HUXDataDDPI\OffsetTesting\normal.mat');
% data.cond2 = load('X:\Ashley\HUXDataDDPI\OffsetTesting\condition1.mat');
% data.cond3 = load('X:\Ashley\HUXDataDDPI\OffsetTesting\condition2.mat');
% data.cond4 = load('X:\Ashley\HUXDataDDPI\OffsetTesting\condition3.mat');
% data.cond5 = load('X:\Ashley\Acuity\Data\condition5.mat');
% 
% dataAll{1}.pptrials = convertDataToPPtrials(data.normal.eis_data);
% dataAll{2}.pptrials = convertDataToPPtrials(data.cond2.eis_data);
% dataAll{3}.pptrials = convertDataToPPtrials(data.cond3.eis_data);
% dataAll{4}.pptrials = convertDataToPPtrials(data.cond4.eis_data);
% dataAll{5}.pptrials = convertDataToPPtrials(data.cond5.eis_data);

%%%After fixing xPos
% data.cond2 = load('X:\Ashley\OLD_PreNewEyeris\Calibration_NewEyeris\09-20-2022\cond2.mat');
% dateCollect = datetime(2022,09,10);
% data.cond2 = load('X:\Ashley\Calibration_NewEyeris\cond2NoMan.mat');
% data.cond3 = load('X:\Ashley\Calibration_NewEyeris\cond3.mat');
% data.cond4 = load('X:\Ashley\Calibration_NewEyeris\cond4.mat');
% 
% dataAll{2}.pptrials = convertDataToPPtrials(data.cond2.eis_data,dateCollect);
% dataAll{2}.pptrials = convertDataToPPtrials(data.cond2.eis_data);
% dataAll{3}.pptrials = convertDataToPPtrials(data.cond3.eis_data);
% dataAll{4}.pptrials = convertDataToPPtrials(data.cond4.eis_data);

%%% Just doing !) normal and 2) horizontal offset
% data.cond1 = load('X:\Ashley\OLD_PreNewEyeris\Calibration_NewEyeris\09-28-2022\controlCalAMC.mat');
% data.cond2 = load('X:\Ashley\OLD_PreNewEyeris\Calibration_NewEyeris\09-28-2022\autoOffsetCalAMC.mat');
% dateCollect = datetime(str2double(data.cond1.eis_data.filename(5:8)),...
%         str2double(data.cond1.eis_data.filename(9:10)),...
%         str2double(data.cond1.eis_data.filename(11:12)));
% dataAll{1}.pptrials = convertDataToPPtrials(data.cond1.eis_data, dateCollect);
% dataAll{2}.pptrials = convertDataToPPtrials(data.cond2.eis_data, dateCollect);
%% Redoing diagnol offset = cond1
%  data.cond1 = load('X:\Ashley\condition0_task.mat');
%  dateCollect = datetime(str2double(data.cond1.eis_data.filename(5:8)),...
%         str2double(data.cond1.eis_data.filename(9:10)),...
%         str2double(data.cond1.eis_data.filename(11:12)));
% dataAll{1}.pptrials = convertDataToPPtrials(data.cond1.eis_data, dateCollect);

%% Testing After Alpha Implementation
data.cond1 = load('X:\Ashley\offset_test1_nooffest_alpha0.8.mat');
dateCollect = datetime(str2double(data.cond1.eis_data.filename(5:8)),...
        str2double(data.cond1.eis_data.filename(9:10)),...
        str2double(data.cond1.eis_data.filename(11:12)));
dataAll{1}.pptrials = convertDataToPPtrials(data.cond1.eis_data, dateCollect);

data.cond2 = load('X:\Ashley\offset_test1.mat'); %alpha0.8
 dateCollect = datetime(str2double(data.cond2.eis_data.filename(5:8)),...
        str2double(data.cond2.eis_data.filename(9:10)),...
        str2double(data.cond2.eis_data.filename(11:12)));
dataAll{2}.pptrials = convertDataToPPtrials(data.cond2.eis_data, dateCollect);

data.cond3 = load('X:\Ashley\offset_test2_alpha0.3.mat');
 dateCollect = datetime(str2double(data.cond3.eis_data.filename(5:8)),...
        str2double(data.cond3.eis_data.filename(9:10)),...
        str2double(data.cond3.eis_data.filename(11:12)));
dataAll{3}.pptrials = convertDataToPPtrials(data.cond3.eis_data, dateCollect);

%%%retesting same conditions
data.cond4 = load('X:\Ashley\test3\offset_test3_nooffest_alpha0.8.mat');
dateCollect = datetime(str2double(data.cond1.eis_data.filename(5:8)),...
        str2double(data.cond1.eis_data.filename(9:10)),...
        str2double(data.cond1.eis_data.filename(11:12)));
dataAll{4}.pptrials = convertDataToPPtrials(data.cond4.eis_data, dateCollect);

data.cond5 = load('X:\Ashley\test3\offset_test3_alpha0.8.mat'); %alpha0.8
 dateCollect = datetime(str2double(data.cond2.eis_data.filename(5:8)),...
        str2double(data.cond2.eis_data.filename(9:10)),...
        str2double(data.cond2.eis_data.filename(11:12)));
dataAll{5}.pptrials = convertDataToPPtrials(data.cond5.eis_data, dateCollect);

data.cond6 = load('X:\Ashley\test3\offset_test3_alpha0.3.mat');
 dateCollect = datetime(str2double(data.cond3.eis_data.filename(5:8)),...
        str2double(data.cond3.eis_data.filename(9:10)),...
        str2double(data.cond3.eis_data.filename(11:12)));
dataAll{6}.pptrials = convertDataToPPtrials(data.cond6.eis_data, dateCollect);

data.cond7 = load('X:\Ashley\test3\offset_test3_alpha0.3_largejog.mat');
 dateCollect = datetime(str2double(data.cond3.eis_data.filename(5:8)),...
        str2double(data.cond3.eis_data.filename(9:10)),...
        str2double(data.cond3.eis_data.filename(11:12)));
dataAll{7}.pptrials = convertDataToPPtrials(data.cond7.eis_data, dateCollect);
%%
figure;
for i = 1:length(dataAll)
    xAll = [];
    yAll = [];
    for ii = 1:length(dataAll{i}.pptrials)
        if ~isfield(dataAll{i}.pptrials{ii}, 'x')
            continue;
        end
        xAll = [xAll dataAll{i}.pptrials{ii}.x(find(dataAll{i}.pptrials{ii}.x < 60 &...
            dataAll{i}.pptrials{ii}.x > -60 & dataAll{i}.pptrials{ii}.y < 60 &...
            dataAll{i}.pptrials{ii}.y > -60))];
        yAll = [yAll dataAll{i}.pptrials{ii}.y(find(dataAll{i}.pptrials{ii}.x < 60 &...
            dataAll{i}.pptrials{ii}.x > -60 & dataAll{i}.pptrials{ii}.y < 60 &...
            dataAll{i}.pptrials{ii}.y > -60))];
    end
    subplot(3,3,i)
    
    generateHeatMapSimple( ...
        xAll, ...
        yAll, ...
        'Bins', 30,...
        'AxisValue', 30,...
        'StimulusSize',16*2,...
        'Uncrowded', 4,...
        'Borders', 1);
    title(sprintf('Condition %i'),i)
    
    nameCond = sprintf('cond%i',i);
    [calParams] = extractCalibrationParameters(data.(nameCond).eis_data);
    hold on
    temp1 = calParams.centers_arcmin(1,5);
    temp2 = calParams.centers_arcmin(2,5);
    plot(temp1,temp2,'*','MarkerSize',15,'Color','k')
end

figure;
cmap = colormap(parula(20))
for ii = [1:2, 4:12]
    hold on;plot((dataAll{5}.pptrials{ii}.x), ...
        dataAll{5}.pptrials{ii}.y,...
        'o','Color',cmap(ii,:))
    axis ([-10 10 -10 10])
end
axis square


