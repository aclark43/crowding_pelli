function finalTableOutput(threshStruct,finalTable,currentSubj,currentStab,currentEm)
%OUTSIDE FUNCTION: if you want to create a table with multiple columns in a
%for loop, and a counter. Then, concatinize your just created table with
%the one before using the following short code:

%[latex{counter}, T{counter}] = buildThresholdTable(threshStruct,counter,title_str);                   
% currentTable = T{counter};
% finalTable = [finalTable, currentTable];
%
% then, call this function.
% The function outputs the latex script in command window. Use 'diary' to
% save script.

rowNames = fieldnames(threshStruct);

input.tableColLabels = 'FinalThresholds';
input.tableRowLabels = rowNames;

% Now use this table as input in our input struct:
input.data = finalTable;
% Set the row format of the data values (in this example we want to use
% integers only):
input.dataFormat = {'%i'};
% Column alignment ('l'=left-justified, 'c'=centered,'r'=right-justified):
input.tableColumnAlignment = 'c';
% Switch table borders on/off:
input.tableBorders = 1;
% Switch to generate a complete LaTex document or just a table:
input.makeCompleteLatexDocument = 1;
% LaTex table caption:
input.tableCaption = 'MyTableCaption';
% LaTex table label:
input.tableLabel = 'MyTableLabel';
% Switch to generate a complete LaTex document or just a table:
input.makeCompleteLatexDocument = 1;


%%%Creates Text File
% checkFile = sprintf('../Scripts/MATFiles/%s%s%s.txt', currentSubj,currentStab,currentEm);
checkFile = ('../Scripts/MATFiles/allSubjAllEcc.txt');

% dir = ('../Data/Scripts/MATFiles');

if exist(checkFile, 'file') == 2
    delete checkFile %(sprintf('%s%s', currentSubj, '.txt'));
end

% diaryName = sprintf('../Scripts/MATFiles/%s%s%s.txt', currentSubj,currentStab,currentEm);
diaryName = ('MATFiles/allSubjAllEcc.txt');

diary(diaryName)
    latexFinal = latexTable(input);
diary off

end

