
function spanConditionHistogramPlotting( condition, meanValues, allErrors,...
    largeSWU, smallSWU, largeSWC, smallSWC )

figure
c = categorical({'Uncrowded Small Strokewidths',...
    'Crowded Small Strokewidths',...
    'Uncrowded Large Strokewidths',...
    'Crowded Large Strokewidths'});

bar(c,meanValues)

hold on
er = errorbar(c,meanValues,allErrors);
er.Color = [0 0 0];                            
er.LineStyle = 'none';  
hold off
%axis([0,4,0,10]);
[h,p] = ttest([largeSWU'],[smallSWU']);
[h,p] = ttest([largeSWC'],[smallSWC']);

[h,p] = ttest2([largeSWU'],[largeSWC']);
[h,p] = ttest2([smallSWU'],[smallSWC']);

[h,p] = ttest2([largeSWU' largeSWC'],[smallSWU' smallSWC']);
[h,p] = ttest2([largeSWU' smallSWU'],[largeSWC' smallSWC']);
% pVal = text(1.4,4,sprintf('p = %.3f', mean(p)));
% text(1.4,4.25,'(unpaired ttest)');
% hold on;
% err = (subjectCondition.stdDriftSpan);
% errorbar(subjectCondition.stimulusSize,subjectCondition.meanDriftSpan, err, 'LineStyle','none');
% if condition == 1
    xlabel('Strokewidth Size')
% elseif condition ==2
%     xlabel('Grouped Strokewidth Size')
% end
if condition == 1
    ylabel('Mean Span of Drift')
    graphTitleLine1 = title({'Mean Drift Spans' 'in Small vs Large Strokewidths'});% condTitle});
elseif condition ==2
    ylabel('Drift Coefficient')
    graphTitleLine1 = title({'Drift Coefficient' 'in Small vs Large Strokewidths'});% condTitle});
end

%     xforGraph = round(double(unique(spnAmpEvaluate.stimulusSize)),1);
%     set(gca, 'XTick', xforGraph, 'xticklabel', {xforGraph})
% condTitle = char(condition);

end