%%runDPIvsDDPI

%% Params
%%%ECC PROJECT%%%%

close all
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.fig = struct(...
    'FIGURE_ON', 0,...
    'HUX', 0,...
    'FOLDED', 1);
fig = params.fig;

% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,'Z084','Z014'}; %Drift Only Subjects,'Z084'

subjectsAll = {'Z002DDPI','AshleyDDPI','Z046DDPI','Z084DDPI','Zoe','Z091'};
% ecc = {'0ecc','10eccNasal','10eccTemp','15eccNasal','15eccTemp','25eccNasal','25eccTemp'};
% eccNames = {'Center0ecc','Nasal10ecc','Temp10ecc','Nasal15ecc','Temp15ecc','Nasal25ecc','Temp25ecc'};
% 
% subjectsAll = {'Z091'};
% ecc = {'0eccEcc','10ecc','15ecc','25ecc','40ecc','60ecc'};
% eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc','Side40ecc','Side60ecc'};
% stabilization = {'Stabilized'};
% conditions = {'Uncrowded'};


% subjectsAll = {'Zoe','Z002DDPI','AshleyDDPI','Z046DDPI','Z084DDPI','Z091'};
ecc = {'0eccEcc','10ecc','15ecc','25ecc'};
eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc'};
stabilization = {'Stabilized','Unstabilized'};
conditions = {'Uncrowded','Crowded'};
params.em = {'Drift'};
em = params.em;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
% c = jet(length(subjectsAll));
% c = brewermap(12,'Accent');
c = brewermap(12,'Dark2');

% params.c = c;

%% Load Variables

for condIdx = 1:length(conditions)
    for ii = 1:length(ecc)
        for stabilIdx = 1:length(stabilization)
            singleEcc = (eccNames{ii});
            numberSingleEcc = cell2mat(regexp(singleEcc, '\d+', 'match'));
            numberSingleEccString = sprintf('ecc_%s',numberSingleEcc);
            condition = {conditions{condIdx}};
            params.stabil = stabilization{stabilIdx};
            
            [ subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc), ...
                subjectMS.(singleEcc),...
                perform.(conditions{condIdx}).(params.stabil).(singleEcc)] = ...
                loadVariablesEcc(subjectsAll, condition, params, 0, numberSingleEccString, ecc{ii});
            for ss = 1:length(subjectsAll)
                subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc)(ss).name = ...
                    subjectsAll{ss};
                 subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc)(ss).nameIdx = ...
                    ss;
            end
        end
    end
end

for condIdx = 1:length(conditions)
    for ii = 1:length(ecc)
        for stabilIdx = 1:length(stabilization)
            singleEcc = (eccNames{ii});
            numberSingleEcc = cell2mat(regexp(singleEcc, '\d+', 'match'));
            params.stabil = stabilization{stabilIdx};
            [~,numSubs] = size(subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc));
            cond = conditions{condIdx};
            if strcmp(singleEcc,'Center0ecc')
                thresholdSWVals.(conditions{condIdx}).(params.stabil).(singleEcc) = determineThresholdSWEcc...
                    (subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    cond, numSubs, 'ecc_0');
            else
                thresholdSWVals.(conditions{condIdx}).(params.stabil).(singleEcc) = determineThresholdSWEcc...
                    (subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    cond, numSubs, sprintf('ecc_%s',numberSingleEcc));
            end
            
            for i = 1:length(subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc))
                allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).subject =...
                    subjectsAll{i};
                allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) =...
                    subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).thresh;

                allValuesThresholdsBoots.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) = ...
                    subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).threshB;
%                 [upperY, lowerY] = confInter95(subjectThreshCondition(ii).bootsAll);
%             errorbar(mDSQ(ii), thresholds(ii), upperY, lowerY, 'Color', c(ii,:));
%             hold on
                if ~isempty( allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc))
                    allValuesNumTrials.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) =...
                        sum(subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).em.valid);
                else
                    allValuesNumTrials.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) = [];
                end
            end
        end
    end
end


%% 


% ogSubjects = {'Z002DDPI','AshleyDDPI','Z046DDPI','Z084DDPI','Z023DDPI'};
% dpiOGSubjectFileNames = {'Z002','Ashley','Z046','Z084','Z023'};

ogSubjects = {'Z002DDPI','AshleyDDPI','Z023DDPI'};
dpiOGSubjectFileNames = {'Z002','Ashley','Z023'};

for ii = 1:length(ogSubjects)
    filename = sprintf('MATFiles/%s_Uncrowded_Unstabilized_Drift_0ecc_Threshold.mat',...
        ogSubjects{ii});
    tempFile = load(filename);
    
    tempThresh = thresholdSWVals.Uncrowded.Unstabilized.Center0ecc.vals;
    
    path = tempFile.threshInfo.em.allTraces;% ecc_0.(tempThresh);
    ogCompare.(ogSubjects{ii}).sw_value = tempThresh;
    ogCompare.(ogSubjects{ii}).numTrials = length(path.span);
    ogCompare.(ogSubjects{ii}).dsq = path.dCoefDsq;
    tempStruct.(ogSubjects{ii}) = biasCalculations ...
        (struct('x',path.x, 'y', path.y), 330);
%     mergestructs = @(x,y) ...cell2struct([struct2cell(x);struct2cell(y)],[fieldnames(x);fieldnames(y)]);
    
    ogCompare.(ogSubjects{ii}).curve = mean(cell2mat(path.curvature));
    counter = 1;
    for i = 1:length(path.mn_speed)
        temp = path.mn_speed{i};
        if ~isnan(temp)
            tempSpeed(counter) = temp;
            counter = counter + 1;
        end
    end
    ogCompare.(ogSubjects{ii}).speed = mean(tempSpeed);
    ogCompare.(ogSubjects{ii}).thresh = tempFile.threshInfo.thresh;
%     ogCompare.(ogSubjects{ii}).performance = path.performanceAtSize;
end

for ii = 1:length(ogSubjects)
    filename = sprintf('MATFiles/%s_Uncrowded_Unstabilized_Drift_0ecc_Threshold.mat',...
        dpiOGSubjectFileNames{ii});
    tempFile = load(filename);
    tempThresh = thresholdSWVals.Uncrowded.Unstabilized.Center0ecc.vals;
    
    path = tempFile.threshInfo.em.allTraces;% ecc_0.(tempThresh);
    dcThreshOG.(ogSubjects{ii}).sw_value = tempThresh;
    dcThreshOG.(ogSubjects{ii}).numTrials = length(path.span);
    dcThreshOG.(ogSubjects{ii}).dsq = path.dCoefDsq;
    dcThreshOG.(ogSubjects{ii}).curve = mean(cell2mat(path.curvature));
    tempStruct.(dpiOGSubjectFileNames{ii}) = biasCalculations ...
        (struct('x',path.x, 'y', path.y), 330);
    counter = 1;
    for i = 1:length(path.mn_speed)
        temp = path.mn_speed{i};
        if ~isnan(temp)
            tempSpeed(counter) = temp;
            counter = counter + 1;
        end
    end
    dcThreshOG.(ogSubjects{ii}).speed = mean(tempSpeed);
    dcThreshOG.(ogSubjects{ii}).thresh = tempFile.threshInfo.thresh;
%     dcThreshOG.(ogSubjects{ii}).performance = path.performanceAtSize;
end
% dcThreshOG.AshleyDDPI = 4.8931;
% dcThreshOG.Z002DDPI = 15.6098;
% dcThreshOG.Z046DDPI = 18.5418;
% dcThreshOG.Z084DDPI = 14.4799;
% 
% %2, 4 8 9
% dcSTDOG.AshleyDDPI = 0.244597385795577;
% dcSTDOG.Z002DDPI = 0.711774137990326;
% dcSTDOG.Z046DDPI = 2.33349133370307;
% dcSTDOG.Z084DDPI = 1.12836474143644;

%% Refmorat Variables AGAIN
for ee = 1:length(ecc)
    singleEcc = (eccNames{ee});
    for crow = 1:2
        if crow == 1
            condition = 'Uncrowded';
        else
            condition = 'Crowded';
        end
        for ii = 1:subNum
            temp = fieldnames(subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).em);
            nameEcc = temp{startsWith(temp,"ecc_",'IgnoreCase',true)};
            

            subject = subjectsAll{ii};
            
            swUnc = thresholdSWVals.(condition).Unstabilized.(singleEcc)(ii).vals;
            path = subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).em.(nameEcc).(swUnc);
            
            dataSummaries.(condition).Subject(ee,ii) = {subject};
            dataSummaries.(condition).Span(ee,ii) = mean(path.span);
            dataSummaries.(condition).dsq(ee,ii) = path.dCoefDsq;
            dataSummaries.(condition).curve(ee,ii) = mean(cell2mat(path.curvature));
            
            counter = 1;
            for i = 1:length(path.mn_speed)
                temp = path.mn_speed{i};
                if ~isnan(temp)
                    tempSpeed(counter) = temp;
                    counter = counter + 1;
                end
            end
%             dcThreshOG.(ogSubjects{ii}).speed = mean(tempSpeed);
            
            dataSummaries.(condition).speed(ee,ii) = mean(tempSpeed);
            dataSummaries.(condition).thresh(ee,ii) = subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).thresh;
%             dataSummaries.(condition).performance(ee,ii) = path.dCoefDsq;
            dataSummaries.(condition).sw_value{ee,ii} = swUnc;
            
            dataSummaries.(condition).Area(ee,ii) =  path.areaCovered;
            dataSummaries.(condition).ThresholdSW(ee,ii) = (subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).thresh);
            dataSummaries.(condition).order{ee,ii} = singleEcc;
            
       end
    end
    
end

% [heightDataSummaries, widthDataSummaries] = size(dataSummaries.Uncrowded.thresh);
for ii = 1:length(ogSubjects)
    path = dataSummaries.Uncrowded;
    ogCompare.Uncrowded.(subjectsAll{ii}).dsq = path.dsq(1,ii);
    ogCompare.Uncrowded.(subjectsAll{ii}).curve = path.curve(1,ii);
    ogCompare.Uncrowded.(subjectsAll{ii}).speed = path.speed(1,ii);
    ogCompare.Uncrowded.(subjectsAll{ii}).thresh = path.thresh(1,ii);
    ogCompare.Uncrowded.(subjectsAll{ii}).sw_value = path.sw_value{1,ii};
end
tic

%% Compare Measures between Systems
figure('units','normalized','outerposition',[0 0 .5 .5]);
ogSubjects = fieldnames(dcThreshOG);
ogMeasures = fieldnames(dcThreshOG.(ogSubjects{1}));
for i = 2:length(ogMeasures)
    subplot(3,2,i-1)
    for ii = 1:length(ogSubjects)
        %         errorbar(1,dcThreshOG.(ogSubjects{ii}).(ogMeasures{i}),...
        %             dcSTDOG.(ogSubjects{ii}),'vertical', ...
        %             'o','MarkerSize',10, 'Color', c(ii,:),...
        %             'MarkerFaceColor',c(ii,:));
        plot(1,dcThreshOG.(ogSubjects{ii}).(ogMeasures{i}),...
            'o','MarkerSize',10, 'Color', c(ii,:),...
            'MarkerFaceColor',c(ii,:));
        text(1,double(dcThreshOG.(ogSubjects{ii}).(ogMeasures{i})),...
            sprintf('%i',dcThreshOG.(ogSubjects{ii}).numTrials));
        hold on
        leg(ii) = plot(2,ogCompare.(ogSubjects{ii}).(ogMeasures{i}),'o','MarkerSize',10, 'Color', c(ii,:),...
            'MarkerFaceColor',c(ii,:));
        text(2,double(ogCompare.(ogSubjects{ii}).(ogMeasures{i})),...
            sprintf('%i',ogCompare.(ogSubjects{ii}).numTrials));

        
        line([1 2], [dcThreshOG.(ogSubjects{ii}).(ogMeasures{i}) ogCompare.(ogSubjects{ii}).(ogMeasures{i})]...
            , 'Color', c(ii,:));
    end
    set(gca,'xtick',[1 2],'xticklabel', {'DPI', 'dDPI'},'FontSize',12,...
        'FontWeight','bold')
    xlim([ 0.5 2.5])
    ylabel((ogMeasures{i}))
end
legI = legend(leg,dpiOGSubjectFileNames);
% legI.Layout.Tile = 6;
suptitle('Same Size Stimulus & Subject Comparisons')
saveas(gcf,'../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/MeasuresinDPIvsDDPI.png');
saveas(gcf,'C:/Users/Ruccilab/Box/APLab-Projects/Myopia/Myopia Empirical/Documents/Overleaf_MyopiaEmpirical/figures/MeasuresinDPIvsDDPI.png');
%% Compare Bias
allSubjectComp = [ogSubjects' dpiOGSubjectFileNames];
counter = 1;
for ii = 1:length(allSubjectComp)
    path = tempStruct.(allSubjectComp{ii});
    bias.dsqBias(counter) = path.biasMeasure;
    bias.angleRad(counter) = path.angleRad;
    bias.curveOld(counter) = path.old_curvature2;
    bias.ratioBias(counter) = abs(...
        path.original_muRecenteredALL(2)/...
        path.original_muRecenteredALL(1));
    
   
    counter = counter + 1;
end
 bias.data = [2 2 2 1 1 1];
 
 nameBias = {'dsqBias','curveOld','ratioBias'};
 figure('Name','NormalizedComparingBiasMeasures');
for ii = 1:length(bias.dsqBias)
    normDsqBias = bias.dsqBias(ii)/max(bias.dsqBias);
    normCurv = bias.curveOld(ii)/max(bias.curveOld);
    normRatio = bias.ratioBias(ii)/max(bias.ratioBias);
    if bias.data(ii) == 1
%         subplot(1,2,1)
        leg(1) = plot([1,2,3],[normDsqBias normCurv normRatio],'-o',...
            'Color','c','MarkerFaceColor','c');
        ylim([0 1])
        xticks([1 2 3]); xticklabels(nameBias);
%         ylabel('Normalized DPI Data')
%         title('DPI')
    else
%         subplot(1,2,2)
        leg(2) = plot([1,2,3],[normDsqBias normCurv normRatio],'-o',...
            'Color','m','MarkerFaceColor','m');
        ylim([0 1])
        xticks([1 2 3]); xticklabels(nameBias);
        ylabel('Normalized  Data')
        title('Pink = DDPI, Blue = DPI')
    end
 hold on
end
suptitle('Normalized Measures (across data)')
legend(leg,{'DPI','DDPI'})
saveas(gcf,'../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/MeasuresinDPIvsDDPINormalizedBias.png');
saveas(gcf,'C:/Users/Ruccilab/Box/APLab-Projects/Myopia/Myopia Empirical/Documents/Overleaf_MyopiaEmpirical/figures/MeasuresinDPIvsDDPINormalizedBias.png');



figure('Name','ComparingPValueBiasMeasures','Position', [2000 10 800 500]);
for i = 1:3
    subplot(1,3,i)
    plot(bias.data(bias.data == 1),bias.(nameBias{i})(bias.data == 1),...
        'o','Color','c','MarkerFaceColor','c')
    hold on
    plot(bias.data(bias.data == 2),bias.(nameBias{i})(bias.data == 2),...
        'o','Color','m','MarkerFaceColor','m')
    xticks([1 2]); xticklabels({'DPI','DDPI'}); xlim([.5 2.5]);
    hold on
    errorbar([1 2],[mean(bias.(nameBias{i})(bias.data == 1)) mean(bias.(nameBias{i})(bias.data == 2))],...
        [std(bias.(nameBias{i})(bias.data == 1)) std(bias.(nameBias{i})(bias.data == 2))],...
        'Color','k')
    text(bias.data,bias.(nameBias{i}),allSubjectComp)
    ylabel(nameBias{i})
    [h,p] = ttest(bias.(nameBias{i})(bias.data == 1), bias.(nameBias{i})(bias.data == 2));
    title(sprintf('Mean %s, p = %.3f',(nameBias{i}),p))
end
saveas(gcf,'../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/MeasuresinDPIvsDDPIBiasPValues.png');
saveas(gcf,'C:/Users/Ruccilab/Box/APLab-Projects/Myopia/Myopia Empirical/Documents/Overleaf_MyopiaEmpirical/figures/MeasuresinDPIvsDDPIBiasPValues.png');


%% Functions
function snellenData = biasCalculations (dataStruct, fsamp)

% for ii = 1:length(subjectFiles)
% ii = 1;
    counter = 1;
    xALLRecentered = []; yALLRecentered = [];
    for i = 1:length(dataStruct)
        path = dataStruct;
        lengthTrace = 500;
       
        if length(path(i).x) >= lengthTrace/(1000/fsamp)
            x = path(i).x(1:lengthTrace/(1000/fsamp));
            y = path(i).y(1:lengthTrace/(1000/fsamp));
            
            xALLRecentered = [xALLRecentered x-x(1)];
            yALLRecentered = [yALLRecentered y-y(1)];
            
            snellenData.x{counter} = x;
            snellenData.y{counter} = y;
            
            [snellenData.old_curvature2(counter), ~, snellenData.old_curvature3(counter)] = ...
                CalculateLengthAndCurvature(x, y);
            
            [snellenData.VX{counter},...
                snellenData.original_mu{counter},...
                snellenData.r_ellipse{counter}] = tgdt_fit_2d_gaussian(x,y,0.99);
            counter = counter + 1;
        end
    end
    
    snellenData.xALLRecentered = xALLRecentered;
    snellenData.yALLRecentered = yALLRecentered;
    
    [~,...
        snellenData.Bias,...
        snellenData.dCoefDsq, ...
        ~,...
        snellenData.Dsq, ...
        snellenData.SingleSegmentDsq,...
        snellenData.TimeDsq] = ...
        CalculateDiffusionCoef(fsamp, struct('x',snellenData.x, 'y', snellenData.y));
    
    [snellenData.VXRecenteredALL,...
        snellenData.original_muRecenteredALL,...
        snellenData.r_ellipseRecenteredALL] = ...
        tgdt_fit_2d_gaussian(snellenData.xALLRecentered,...
        snellenData.yALLRecentered,0.99);

    
    [snellenData.biasMeasure, snellenData.angleRad] = ...
        calculateBiasSingleValue(snellenData.Bias);
    
    temp(:) = eig(snellenData.VXRecenteredALL);
    snellenData.ratio = temp(2)/temp(1);
    
    snellenData.old_curvature3 = mean(snellenData.old_curvature3);
    snellenData.old_curvature2 = mean(snellenData.old_curvature2);
    
end
