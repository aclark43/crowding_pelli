function plotEccDistanceFromSpan(subjectsAll, subjectThreshUnc, c, ecc, eccNames, condition, em)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:length(subjectsAll)
    for ee = 1:length(ecc)
        subj = sprintf('%s',(subjectsAll{ii}));
        singleEcc = (eccNames{ee});
        fprintf('%s \n',singleEcc)
        
        validTrials = subjectThreshUnc.(singleEcc)(ii).em.valid;
        actualTarget = abs(subjectThreshUnc.(singleEcc)(ii).em.actualEccTargetDist(validTrials));
        variableEcc.(subj).eccTarget{ee} = round(actualTarget);
        variableEcc.(subj).eccSize{ee} = ...
            subjectThreshUnc.(singleEcc)(ii).em.size;
        variableEcc.(subj).eccPerformance{ee} = ...
            subjectThreshUnc.(singleEcc)(ii).em.performance;
        eccTargetAll{ii,ee} = round(actualTarget);
        eccPerformanceAll{ii,ee} = subjectThreshUnc.(singleEcc)(ii).em.performance(validTrials);
        eccSizeAll{ii,ee} = subjectThreshUnc.(singleEcc)(ii).em.size(validTrials);
        xTemp{ii,ee} = subjectThreshUnc.(singleEcc)(ii).em.xShift(validTrials);
        yTemp{ii,ee} = subjectThreshUnc.(singleEcc)(ii).em.y(validTrials);
        x_realTemp{ii,ee} = subjectThreshUnc.(singleEcc)(ii).em.x(validTrials);
        targetDist{ii,ee} = subjectThreshUnc.(singleEcc)(ii).em.targetEcc(validTrials);
        %         xFoldedTemp{ii,ee} = subjectThreshUnc.(singleEcc)(ii).em.targetEcc(validTrials);
        whichSes{ii,ee} = ee * ones(1,length(subjectThreshUnc.(singleEcc)(ii).em.x(validTrials)));
        %        for i = 1:length(subjectThreshUnc.(singleEcc)(ii).em.x)
        %
        %        end
    end
end

xl = [0.1, 7];
yl = [0, 1];
gamma = 0.25;
for ii = 1:length(subjectsAll)
    for ee = 1:length(ecc)
        values.x = [];
        values.y = [];
        for idx = 1:length(x_realTemp{ii,ee})
            if targetDist{ii,ee}(idx) >= 0
                x = x_realTemp{ii,ee}{idx};
            else
                x = -(x_realTemp{ii,ee}{idx});
            end
            y = yTemp{ii,ee}{idx};
            values.x = [values.x x];
            values.y = [values.y y];
        end
        
        figure;
        generateHeatMapSimple (values.x, values.y, ...
            'Bins', 40, 'AxisValue', 40, 'StimulusSize', 1, 'Offset', abs(targetDist{ii,ee}(1)));
        title(sprintf('%s %s Raw Folded', string(ecc(ee)), subjectsAll{ii}));
        saveas(gcf,...
            sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/HeatMaps/%sRawFoldedHeatMaps%s%s.png', ...
            condition, subjectsAll{ii}, string(ecc(ee))));
        saveas(gcf,...
            sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/HeatMaps/%sRawFoldedHeatMaps%s%s.epsc', ...
            condition, subjectsAll{ii}, string(ecc(ee))));
    end
end

% figure;
for ii = 1:length(subjectsAll)
    clear plotEcc
    clear plotPerf
    clear plotSize
    clear xTemp2
    clear yTemp2
    clear fromWhere
    
    counter = 1;
    [~, numEcc] = size(eccTargetAll);
    for i = 1:numEcc
        for trialIdx = 1:length(eccTargetAll{ii, i})
            plotEcc(counter) = (eccTargetAll{ii,i}(trialIdx));
            plotPerf(counter) = (eccPerformanceAll{ii,i}(trialIdx));
            plotSize(counter) = (eccSizeAll{ii,i}(trialIdx)) * 2;
            xTemp2(counter) = (xTemp{ii,i}(trialIdx));
            yTemp2(counter) = (yTemp{ii,i}(trialIdx));
            fromWhere(counter) =  whichSes{ii,i}(trialIdx);
            counter = counter + 1;
        end
    end
    
    %     uEcc = unique((round(plotEcc/5))*5); %%bins data into 5' increments
    uEcc = unique(round(plotEcc,-1));
    for numEcc = 1:length(uEcc)
        
        %          subplot(1,length(uEcc),numEcc);
        %         idx = find((round(plotEcc/5))*5 == uEcc(numEcc));
        idx = find(round(plotEcc,-1) == uEcc(numEcc));
        if numel(idx) > 50
            figure;
            
            eccUniqueEcc = double(plotEcc(idx));
            eccPlotPerf = double(plotPerf(idx));
            eccPlotSize = double(plotSize(idx));
            [thresh{ii,numEcc}, par, ~, ~, ~, chi] = psyfitCrowding(eccPlotSize, eccPlotPerf, 'DistType', 'Normal',...
                'Xlim', xl, 'Ylim', yl, 'Boots', 1,...
                'Log', 'Chance', gamma, 'Extra');
            
            numerTrialsElseWhere(ii,numEcc) = (sum(numEcc == fromWhere(idx)))/length(fromWhere(idx)) * 100;
            %             if chi > .5
            %                 forPlottingEcc{ii,numEcc} = NaN;
            %                 numelIdx{ii,numEcc} = NaN;
            %                 close all
            %             else
            %             forPlottingEcc{ii,numEcc} = mean(((round(eccUniqueEcc/5)*5)));
            forPlottingEcc{ii,numEcc} = mean(round(eccUniqueEcc, -1));
            numelIdx{ii,numEcc} = numel(idx);
            
            title(sprintf('%s %s', subjectsAll{ii}, string(uEcc(numEcc))));
            
            saveas(gcf,...
                sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/%sIndPsychCurve%s%s.png', ...
                subjectsAll{ii}, string(uEcc(numEcc))));
            saveas(gcf,...
                sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/%sIndPsychCurve%s%s.epsc', ...
                condition, subjectsAll{ii}, string(uEcc(numEcc))));
            xValues = [];
            yValues = [];
            %             xValues = xTemp2(idx);
            %             yValues = yTemp2(idx);
            for i = idx
                xValues = [xValues cell2mat(xTemp2(i))];
                yValues = [yValues cell2mat(yTemp2(i))];
                
            end
            
            figure;
            n_bins = 40;
            
            %             axisValue = 40;
            if uEcc(numEcc)> 0 || uEcc(numEcc)< 0
                dbleTar = 1;
            else
                dbleTar = 0;
            end
            generateHeatMapSimple (xValues, yValues, ...
                'Bins', n_bins, 'AxisValue', 40, ...
                'StimulusSize', 1, 'Offset', uEcc(numEcc),...
                'DoubleTargets', dbleTar);
            
            title(sprintf('%s %s', string(uEcc(numEcc)), subjectsAll{ii}));
            
            saveas(gcf,...
                sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/%sIndHeatMaps%s%s.png', ...
                condition, subjectsAll{ii}, string(uEcc(numEcc))));
            saveas(gcf,...
                sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/%sIndHeatMaps%s%s.epsc', ...
                condition, subjectsAll{ii}, string(uEcc(numEcc))));
            close all
            %             end
        end
    end
    
end

for ii = 1:length(subjectsAll) %each subject
    [~,numEcc] = size(forPlottingEcc);
    for i = 1:numEcc %each unique Ecc
        if ~isempty(cell2mat(forPlottingEcc(ii,i))) %converting cells to doubles or NaNs
            forPlotEcc(i) = double((forPlottingEcc{ii,i}));
            forPlotThresh(i) = double((thresh{ii,i}));
            forPlotNum(i) = double((numelIdx{ii,i}));
        else
            forPlotEcc(i) = NaN;
            forPlotThresh(i) = NaN;
            forPlotNum(i) = NaN;
        end
    end
    for i = 1:length(forPlotEcc) %finding the average thresh
        meanVals(ii,i) = forPlotThresh(i);
        meanEcc(ii,i) = forPlotEcc(i); %just matching ecc in same format
    end
    hold on
end

figure;
subplot(2,1,1); hold on;
for ii = 1:size(meanEcc)
    for i = 1:length(meanEcc)
        real_idx = find(meanVals(ii,:) > 0);
        leg(ii) = plot(meanEcc(ii,real_idx),meanVals(ii,real_idx),'-o','Color',c(ii,:));
        hold on
    end
end

hold on
uniqueEcc = unique(meanEcc);
counter = 1;
for u = 1:length(uniqueEcc)
    n_idx = uniqueEcc(u) == meanEcc;
    if length(find(n_idx == 1)) > 2
        plot(uniqueEcc(u), mean((meanVals(n_idx))), 'o', 'Color','k','MarkerFaceColor','k');
        x_plot(counter) = uniqueEcc(u);
        y_plot(counter) = mean((meanVals(n_idx)));
        y_plotSTD(counter) = std((meanVals(n_idx)));
        counter = counter + 1;
    end
end
errorbar(x_plot, y_plot, y_plotSTD, '-o', 'Color','k','MarkerFaceColor','k'); %average across subject point
legend(leg, subjectsAll, 'Location', 'Best')
ylim([0.2 4]);

%% Normalized
subplot(2,1,2); hold on;
for ii = 1:size(meanEcc)
    initialIdx = meanEcc(ii) == 0;
    initialThresh = meanVals(ii, initialIdx);
    for i = 1:length(meanEcc)
        real_idx = find(meanVals(ii,:) > 0);
        leg(ii) = plot(meanEcc(ii,real_idx),...
            meanVals(ii,real_idx) - initialThresh,'-o','Color',c(ii,:));
        hold on
    end
end
hold on
uniqueEcc = unique(meanEcc);
counter = 1;
meanIdxInit = 0 == meanEcc;
meanInit = mean(meanVals(meanIdxInit));
for u = 1:length(uniqueEcc)
    n_idx = uniqueEcc(u) == meanEcc;
    %     initialIdx = meanEcc(ii) == 0;
    %     initialThresh = meanVals(ii, initialIdx);
    if length(find(n_idx == 1)) > 2
        plot(uniqueEcc(u), mean((meanVals(n_idx)- meanInit)), 'o', 'Color','k','MarkerFaceColor','k');
        x_plotN(counter) = uniqueEcc(u);
        y_plotN(counter) = mean((meanVals(n_idx)- meanInit));
        y_plotNSTD(counter) = std((meanVals(n_idx)- meanInit));
        counter = counter + 1;
    end
end
errorbar(x_plotN, y_plotN, y_plotNSTD, '-o', 'Color','k','MarkerFaceColor','k'); %average across subject point
% legend(leg, subjectsAll, 'Location', 'Best')
% ylim([0.2 1.8]);
line([0,30],[0,0],'Color','k');
ylim([-1.5 1.5]);


end

