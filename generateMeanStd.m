function generateMeanStd(meanVector, errorVector, title_str)
%Mean and Standard Deviation anaylsis
%   Detailed explanation goes here

figure('position', [700 350 800 500])
h = barwitherr(errorVector, meanVector);
% h = barwitherr([Summary.Task.prob.SE_Lg_Eyes, Summary.Task.prob.SE_Sm_Eyes,Summary.Fix.prob.SE_Eyes; ...
%     Summary.Task.prob.SE_Lg_Mouth,Summary.Task.prob.SE_Sm_Mouth,Summary.Fix.prob.SE_Mouth; ...
%     Summary.Task.prob.SE_Lg_Nose, Summary.Task.prob.SE_Sm_Nose,Summary.Fix.prob.SE_Nose; ...
%     Summary.Task.prob.SE_Lg_Bkgd, Summary.Task.prob.SE_Sm_Bkgd,Summary.Fix.prob.SE_Bkgd]', ...
%     [Summary.Task.prob.Lg_Eyes, Summary.Task.prob.Sm_Eyes,Summary.Fix.prob.Eyes; ...
%     Summary.Task.prob.Lg_Mouth,Summary.Task.prob.Sm_Mouth,Summary.Fix.prob.Mouth; ...
%     Summary.Task.prob.Lg_Nose, Summary.Task.prob.Sm_Nose,Summary.Fix.prob.Nose; ...
%     Summary.Task.prob.Lg_Bkgd, Summary.Task.prob.Sm_Bkgd,Summary.Fix.prob.Bkgd]');
% set(h(1),'FaceColor','m');set(h(2),'FaceColor','g');set(h(3),'FaceColor',orange);set(h(4),'FaceColor','c')
% hold on
title(title_str)
% ylabel('Landing position probability')
% ylim([0 1])
% set(gca,'YTick',(0:0.5:1),'XTickLabel',{'Parafovea','Foveal'})
% legend('Eyes','Mouth','Nose','Back','Location','BestOutside','Orientation','horizontal');
% box off
% set(gca, 'FontSize', 20)
% set(gca, 'XLim', [0.5 4.5])
% print('-depsc','./Documents/Manuscript/Figures/Corel_Figures/Fig_2E.eps');
end

