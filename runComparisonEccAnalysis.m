%%%ECC PROJECT%%%%

close all
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.fig = struct(...
    'FIGURE_ON', 0,...
    'HUX', 0,...
    'FOLDED', 1);
fig = params.fig;

% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,'Z084','Z014'}; %Drift Only Subjects,'Z084'
% 'Z002DDPI','AshleyDDPI',
% subjectsAll = {'Z091'};%,'Z046DDPI','Z084DDPI','Zoe','Z091'};
% ecc = {'0ecc','10eccNasal','10eccTemp','15eccNasal','15eccTemp','25eccNasal','25eccTemp'};
% eccNames = {'Center0ecc','Nasal10ecc','Temp10ecc','Nasal15ecc','Temp15ecc','Nasal25ecc','Temp25ecc'};
%
% subjectsAll = {'Z091','Z091','Zoe','AshleyDDPI')
% ecc = {'0eccEcc','10ecc','15ecc','25ecc','40ecc','60ecc'};
% eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc','Side40ecc','Side60ecc'};
% stabilization = {'Stabilized'};
% conditions = {'Uncrowded'};'Zoe',


subjectsAll = {'Z002DDPI','AshleyDDPI','Z091','Z046DDPI','Z084DDPI','Z138','A132'};
newSubjects = [0 0 0 0 0 0 1];
ecc = {'0ecc','10ecc','15ecc','25ecc'};
eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc'};
eccVals = [0 10 15 25];
stabilization = {'Unstabilized'};
conditions = {'Uncrowded','Crowded'};
params.em = {'Drift'};
em = params.em;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
% c = jet(length(subjectsAll));
% c = brewermap(12,'Accent');
c = brewermap(12,'Dark2');

% params.c = c;

%% Load Variables

for condIdx = 1:length(conditions)
    for ii = 1:length(ecc)
        for stabilIdx = 1:length(stabilization)
            singleEcc = (eccNames{ii});
            numberSingleEcc = cell2mat(regexp(singleEcc, '\d+', 'match'));
            numberSingleEccString = sprintf('ecc_%s',numberSingleEcc);
            condition = {conditions{condIdx}};
            params.stabil = stabilization{stabilIdx};
            
            [ subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc), ...
                subjectMS.(singleEcc),...
                perform.(conditions{condIdx}).(params.stabil).(singleEcc)] = ...
                loadVariablesEcc(subjectsAll, condition, params, 0, numberSingleEccString, ecc{ii});
            for ss = 1:length(subjectsAll)-1
                subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc)(ss).name = ...
                    subjectsAll{ss};
                subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc)(ss).nameIdx = ...
                    ss;
            end
        end
    end
end

for condIdx = 1:length(conditions)
    for ii = 1:length(ecc)
        for stabilIdx = 1:length(stabilization)
            singleEcc = (eccNames{ii});
            numberSingleEcc = cell2mat(regexp(singleEcc, '\d+', 'match'));
            params.stabil = stabilization{stabilIdx};
            [~,numSubs] = size(subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc));
            cond = conditions{condIdx};
            if strcmp(singleEcc,'Center0ecc')
                thresholdSWVals.(conditions{condIdx}).(params.stabil).(singleEcc) = determineThresholdSWEcc...
                    (subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    cond, numSubs, 'ecc_0');
            else
                thresholdSWVals.(conditions{condIdx}).(params.stabil).(singleEcc) = determineThresholdSWEcc...
                    (subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    cond, numSubs, sprintf('ecc_%s',numberSingleEcc));
            end
            
            for i = 1:length(subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc))
                allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).subject =...
                    subjectsAll{i};
                allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) =...
                    subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).thresh;
                
                allValuesThresholdsBoots.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) = ...
                    subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).threshB;
                %                 [upperY, lowerY] = confInter95(subjectThreshCondition(ii).bootsAll);
                %             errorbar(mDSQ(ii), thresholds(ii), upperY, lowerY, 'Color', c(ii,:));
                %             hold on
                if ~isempty( allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc))
                    allValuesNumTrials.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) =...
                        sum(subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).em.valid);
                else
                    allValuesNumTrials.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) = [];
                end
            end
        end
    end
end


% for ii = find(newSubjects)
%     if strcmp(subjectsAll{ii}, 'A132')
%         data.Center0ecc.A132 = load(...
%             'X:\Ashley\VisualAcuity_Ecc\A132\0ecc_ses1.mat');
%         data.Side10ecc.A132 = load(...
%             'X:\Ashley\VisualAcuity_Ecc\A132\10ecc_ses1.mat');
%         data.Side15ecc.A132 = load(...
%             'X:\Ashley\VisualAcuity_Ecc\A132\15ecc_ses1.mat');
%         data.Side25ecc.A132 = load(...
%             'X:\Ashley\VisualAcuity_Ecc\A132\25ecc_ses1.mat');
%         for e = 1:length(eccNames)
%             temp = data.(eccNames{e}).(subjectsAll{ii});
%             dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
%                 str2double(temp.eis_data.filename(9:10)),...
%                 str2double(temp.eis_data.filename(11:12)));
%             
%             temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
%             processedPPTrials{e} = newEyerisEISRead(temppptrials);
%             
%             
%         end
%     end
% end


temp = [6.25,6.09,6.49,6.78]/5;
(60./(temp*5))*2.5;

%% Linear Regressions - FOR GRANT
% figure;
% temp = allValuesThresholds.Uncrowded.Unstabilized;
% [~,p,bU,r] = LinRegression([0 10 15],...
%     [temp.Center0ecc temp.Side10ecc temp.Side15ecc],...
%     0,NaN,1,0);
%
% temp = allValuesThresholds.Crowded.Unstabilized;
% [~,p,bC,r] = LinRegression([0 10 15],...
%     [temp.Center0ecc temp.Side10ecc temp.Side15ecc],...
%     0,NaN,1,0);



% [critFreq, ~, sumNorm] = powerAnalysis_JustCriticalFrequency ([5 20], 1);
 %% ANOVA

 %% ANOVA testing
% for ii = 1:length(subjectsAll)
    allThresh(:,1) = [allValuesThresholds.Uncrowded.Unstabilized.Center0ecc]; 
    allThresh(:,2) = [allValuesThresholds.Crowded.Unstabilized.Center0ecc]; 
    allThresh(:,3) = [allValuesThresholds.Crowded.Unstabilized.Side10ecc]; 
    allThresh(:,4) = [allValuesThresholds.Crowded.Unstabilized.Side15ecc]; 
    allThresh(:,5) = [allValuesThresholds.Crowded.Unstabilized.Side25ecc];  
% end
[p,tbl,stats] = anova1(allThresh);
results = multcompare(stats);


%% Plot Diff Thresholds at 0
figure;
uTemp = [allValuesThresholds.Uncrowded.Unstabilized.Center0ecc];
cTemp = [allValuesThresholds.Crowded.Unstabilized.Center0ecc];
% uTemp = [allValuesThresholds.Uncrowded.Stabilized.Center0ecc];
% cTemp = [allValuesThresholds.Crowded.Stabilized.Center0ecc];
for ii = 1:length(uTemp)
    forlegs(ii) = plot([1 2],[uTemp(ii) cTemp(ii)],'-o','Color',c(ii,:),...
        'MarkerFaceColor',c(ii,:))
    hold on
end
xlim([.5 2.5])
set(gca,'xtick',[1 2],'xticklabel', {'Uncrowded', 'Crowded'},'FontSize',12)
ylabel('Threshold')
hold on
errorbar([1 2],[mean(uTemp) mean(cTemp)],[std(uTemp) std(cTemp)],'-o','Color','k');
legend(forlegs,subjectsAll,'Location','southeast')
[h,p] = ttest(uTemp,cTemp);
title(sprintf('p > %.3f', p))
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iCenterThresholds.epsc',...
    params.fig.FOLDED));
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iCenterThresholds.png',...
    params.fig.FOLDED));
%% Plot Thresholds Stabilized/Unstabilized
figure('units','normalized','outerposition',[0.2 0.05 .5 .9]) %plots all absolute thresholds for each condition
counter = 1;
for cIdx = 1:length(conditions)
    for sIdx = 1:length(stabilization)
        subplot(2,2,counter);
        clear allValsSubj
        for ii = 1:length(subjectsAll)
            cond = conditions{cIdx};
            stab = stabilization{sIdx};
            
            oneRow = struct2cell(allValuesThresholds.(cond).(stab)(ii));
            oneRowNumTrials = struct2cell(allValuesNumTrials.(cond).(stab)(ii));
            for i = 1:length(eccNames)
                [upperY(i), lowerY(i)] = confInter95(allValuesThresholdsBoots.(cond).(stab)(ii).(eccNames{i}));
            end
            empties = cellfun('isempty',oneRow);
            oneRow(empties) = {0};
            empties = cellfun('isempty',oneRowNumTrials);
            oneRowNumTrials(empties) = {0};
            hold on
            allValsSubj(ii,1:4) = [oneRow{2:size(oneRow)}];
%             errorbar(eccVals,[oneRow{2:size(oneRow)}], upperY, lowerY, 'Color', c(ii,:));
            if sIdx == 1
                stabilizedData.(conditions{cIdx})(ii,1:4) = [oneRow{2:size(oneRow)}];
            end
            hold on
            leg(ii) = plot(eccVals+1,[oneRow{2:size(oneRow)}],'o','Color',c(ii,:),...
                'MarkerFace',c(ii,:),'MarkerSize', 10);
            
%             hold on
%             a = [oneRowNumTrials{1:length(eccNames)}]'; b = num2str(a); d = cellstr(b);
%             dx = 0.1; dy = 0.01; % displacement so the text does not overlay the data points
%             text(eccVals+dx, [oneRow{2:size(oneRow)}]+dy, d, 'FontSize',14);
            hold on
        end
        axis([-.5 27 .8 4.2]);
        ylabel('Threshold (arcmin)');
        title(sprintf('%s%s',cond,stab));
        x = [0 4];
        y = [0 0];
        set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14)
        allValsSubj(allValsSubj==0)=NaN;
        hold on
        errorbar(eccVals,nanmean(allValsSubj), nanstd(allValsSubj),...
            '-o','Color', 'k', 'MarkerFaceColor','k','MarkerSize',15);
        line(x,y,'Color','k','LineStyle','--')
        counter = counter + 1;
        axis square
        figure('Name','temp');
        [~,statsVals.(conditions{cIdx}).(stabilization{sIdx}).p,...
            statsVals.(conditions{cIdx}).(stabilization{sIdx}).b,...
            statsVals.(conditions{cIdx}).(stabilization{sIdx}).r] = LinRegression([0 10 15 25],nanmean(allValsSubj), 0,NaN,1,0);
       statsVals.(conditions{cIdx}).(stabilization{sIdx}).allVals = allValsSubj;
        close figure temp
        
    end
end
legend(leg,subjectsAll,'Location','southeast');
for i = 1:4
    [h,p(i)] = ttest(statsVals.Crowded.Unstabilized.allVals(:,i), ...
        statsVals.Uncrowded.Unstabilized.allVals(:,i))
end

% save('stabilizedEccData','stabilizedData');

saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iAbsoluteThresholds.epsc',...
    params.fig.FOLDED));
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iAbsoluteThresholds.png',...
    params.fig.FOLDED));




figure('units','normalized','outerposition',[0 0 1 1]); %plots relative to 0ecc thresholds for each subject
counter = 1;
for cIdx = 1:length(conditions)
    for sIdx = 1:length(stabilization)
        subplot(2,2,counter);
        for ii = 1:length(subjectsAll)
            cond = conditions{cIdx};
            stab = stabilization{sIdx};
            
            for i = 1:length(eccNames)
                [upperY(i), lowerY(i)] = confInter95(allValuesThresholdsBoots.(cond).(stab)(ii).(eccNames{i}));
            end
            oneRow = struct2cell(allValuesThresholds.(cond).(stab)(ii));
            empties = cellfun('isempty',oneRow);
            oneRow(empties) = {NaN};
            
            
            errorbar(1:length(eccNames),[oneRow{2:length(eccNames)+1}]-oneRow{2}, upperY, lowerY, 'Color', c(ii,:));
            hold on
            leg(ii) = plot(1:length(eccNames),[oneRow{2:length(eccNames)+1}]-oneRow{2},'-o','Color',c(ii,:),...
                'MarkerFace',c(ii,:),'MarkerSize', 15);
            hold on
        end
        axis([.5 length(eccNames)+.5 -2 2]);
        ylabel('\Delta Threshold (arcmin)');
        title(sprintf('%s%s',cond,stab));
        x = [0 length(eccNames)];
        y = [0 0];
        set(gca,'xtick',[1:length(eccNames)],'xticklabel',eccNames, 'FontSize', 14)
        
        line(x,y,'Color','k','LineStyle','--')
        counter = counter + 1;
    end
end
legend(leg,subjectsAll,'Location','southeast');
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iDeltaThresholds.epsc',...
    params.fig.FOLDED));
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iDeltaThresholds.png',...
    params.fig.FOLDED));
%% plot diff of crowded - uncrowded unstabilized (for grant!)
figure('units','normalized','outerposition',[1 .5 .25 .5]) %plots all absolute thresholds for each condition
plot([0 10 15],...
    [[allValuesThresholds.Crowded.Unstabilized.Center0ecc]' - [allValuesThresholds.Uncrowded.Unstabilized.Center0ecc]',...
    [allValuesThresholds.Crowded.Unstabilized.Side10ecc]' - [allValuesThresholds.Uncrowded.Unstabilized.Side10ecc]',...
    [allValuesThresholds.Crowded.Unstabilized.Side15ecc]' - [allValuesThresholds.Uncrowded.Unstabilized.Side15ecc]'],...
    '-o','MarkerSize',20);
saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\COARSES\GrantWriting_Rucci\Images\DiffCrowUncr.epsc');

%% Reformat Data

counter = 1;
for condIdx = 1:length(conditions)
    for ii = 1:length(ecc)
        for stabilIdx = 1:length(stabilization)
            singleEcc = (eccNames{ii});
            numberSingleEcc = cell2mat(regexp(singleEcc, '\d+', 'match'));
            params.stabil = stabilization{stabilIdx};
            [~,numSubs] = size(subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc));
            cond = conditions{condIdx};
            pathwayTable = subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc);
            for i = 1:length(pathwayTable)
                match = [];
                tableData(counter).subject = subjectsAll{i};
                tableData(counter).eccentricity = singleEcc;
                tableData(counter).condition = conditions{condIdx};
                tableData(counter).stabilized = params.stabil;
                if ~isempty(pathwayTable(i).em)
                    if strcmp('Center0ecc',singleEcc)
                        A = fieldnames(pathwayTable(i).em);
                        for numSW = 1:length(A)
                            if strcmp(A{numSW},'ecc_0')
                                match(numSW) = 1;
                                eccNameVar = A{numSW};
                            else
                                match(numSW) = 0;
                            end
                        end
                        %
                        if ~any(match)
                            warning('%s %s %s %s doesnt have ecc', ...
                                subjectsAll{i}, conditions{condIdx},...
                                params.stabil,singleEcc);
                            continue
                        else
                            pathwayEM = ...
                                pathwayTable(i).em.ecc_0.(thresholdSWVals.(conditions{condIdx}).(params.stabil).(singleEcc)(i).vals);
                        end
                    else
                        A = fieldnames(pathwayTable(i).em);
                        for numSW = 1:length(A)
                            if strcmp(A{numSW},'ecc')||...
                                    strcmp(A{numSW},sprintf('ecc_%s',numberSingleEcc))
                                match(numSW) = 1;
                                eccNameVar = A{numSW};
                            else
                                match(numSW) = 0;
                            end
                        end
                        %
                        if ~any(match)
                            warning('%s %s %s %s doesnt have ecc', ...
                                subjectsAll{i}, conditions{condIdx},...
                                params.stabil,singleEcc);
                            continue
                        else
                            pathwayEM = ...
                                pathwayTable(i).em.(eccNameVar).(thresholdSWVals.(conditions{condIdx}).(params.stabil).(singleEcc)(i).vals);
                        end
                    end
                    tableData(counter).numTrials = sum(subjectThreshCond.(conditions...
                        {condIdx}).(params.stabil).(singleEcc)(i).em.valid);
                    tableData(counter).Threshold = round(subjectThreshCond.(conditions...
                        {condIdx}).(params.stabil).(singleEcc)(i).thresh,2);
                    tableData(counter).DiffusionConstant = round(pathwayEM.dCoefDsq, 2)';
                    tableData(counter).Span = round(mean(pathwayEM.span),2)';
                    tableData(counter).Area = round(pathwayEM.areaCovered,2)';
                    tableData(counter).StimSize = unique(pathwayEM.stimulusSize);
                    tableData(counter).Performance = pathwayEM.performanceAtSize;
                    
                    for speedIdx = 1:length(pathwayEM.mn_speed)
                        speed(speedIdx) = pathwayEM.mn_speed{speedIdx};
                        curvature(speedIdx) = pathwayEM.curvature{speedIdx};
                    end
                    tableData(counter).Curvature = ...
                        round(nanmean(curvature),2)';
                    tableData(counter).Speed = ...
                        round(nanmean(speed),2)';
                    
                else
                    tableData(counter).DiffusionConstant = [];
                    tableData(counter).Speed = [];
                    tableData(counter).Span = [];
                    tableData(counter).Area = [];
                    tableData(counter).Curvature = [];
                end
                counter = counter + 1;
            end
        end
    end
end
tableDataT = struct2table(tableData);

%% Look at performance changes for same size
counter = 1;
for condIdx = 1:length(conditions)
    for stabilIdx = 1:length(stabilization)
        for ii = 1:length(ecc)
            condition = conditions{condIdx};
            stabil = stabilization{stabilIdx};
            eCC = eccNames{ii};
            for numSub = 1:length(subjectThreshCond.(condition).(stabil).(eCC)(:))
                pathway = subjectThreshCond.(condition).(stabil).(eCC)(numSub);
                if ~isempty(pathway.thresh)
                    %                     if strcmp(eCC,'Center0ecc')
                    %                         eccNameIdx = 'ecc_0';
                    %                     else
                    %                         eccNameIdx = 'ecc';
                    %                     end
                    tempT = fieldnames(pathway.em);
                    eccNameIdx = tempT(startsWith(tempT,"ecc_",'IgnoreCase',true));
                    allSW = [];
                    temp = fieldnames(pathway.em);
                    if isempty(eccNameIdx)%sum(ismember(temp,eccNameIdx)) == 0
                        continue;
                    end
                    allSW = char(fieldnames(pathway.em.(char(eccNameIdx))));
                    for swIdx = 1:size(allSW)
                        %                         nameSW =
                        tempnameSW = allSW(swIdx,:);
                        nameSW = erase(tempnameSW," ");
                        temppath = pathway.em.(char(eccNameIdx)).(nameSW);
                        
                        structSizePerformance(counter).Subject = pathway.nameIdx;
                        structSizePerformance(counter).SW = regexp(allSW(swIdx,:),'\d*','Match');
                        structSizePerformance(counter).StimulusSize = ...
                            unique(temppath.stimulusSize);
                        structSizePerformance(counter).Performance = ...
                            temppath.performanceAtSize;
                        if strcmp(condition,'Uncrowded')
                            structSizePerformance(counter).Uncrowded = 1;
                        else
                            structSizePerformance(counter).Uncrowded = 2;
                        end
                        if strcmp(stabil,'Stabilized')
                            structSizePerformance(counter).Stabilized = 1;
                        else
                            structSizePerformance(counter).Stabilized = 2;
                        end
                        %                         structSizePerformance(counter).Stabilization = stabil;
                        structSizePerformance(counter).Eccentricity = str2double(cell2mat(regexp(eCC,'\d*','Match')));
                        counter = counter + 1;
                    end
                end
            end
        end
    end
end

%% Plot the changes
figure('units','normalized','outerposition',[0 0 1 1]);
diffValue = [0.2 0.3 0.2 0.2 0.2 0.3];
for ii = 1:length(subjectsAll)
    subplot(2,3,ii)
    counter = 1;
    legSaved = [];
    for cc = 1:length(conditions)
        for ss = 1:length(stabilization)
            %
            idx = find([structSizePerformance(:).Subject] == ii &...
                [structSizePerformance(:).Uncrowded] == cc & ...
                [structSizePerformance(:).Stabilized] == ss);
            
            A = round([structSizePerformance(idx).StimulusSize],1);
            edges = unique(A);
            counts = histc(A(:), edges);
            hasMostOccurances = round(mean(find(counts == max(counts))));
            
            if isnan(hasMostOccurances)
                warning(sprintf('%s %s %s',...
                    subjectsAll{ii},conditions{cc},stabilization{ss}));
                continue;
            end
            %             temp = find(round([structSizePerformance(idx).StimulusSize],1) == ...
            %                 edges(hasMostOccurances));
            temp = (abs((round([structSizePerformance(idx).StimulusSize],1))-...
                edges(hasMostOccurances)));
            closestIndexes = find(temp < diffValue(ii));
            
            %             [minValue,closestIndex] = min(abs((round([structSizePerformance(idx).StimulusSize],1))-...
            %                 edges(hasMostOccurances)));
            %             closestValue = N(closestIndex)
            
            newIdx = idx(closestIndexes);
            if isempty(newIdx)
                warning(sprintf('%s %s %s',...
                    subjectsAll{ii},conditions{cc},stabilization{ss}));
                continue;
            end
            %             figure;
            legSaved(counter) = plot([structSizePerformance(newIdx).Eccentricity],...
                [structSizePerformance(newIdx).Performance],'-o',...
                'LineWidth',2,...
                'MarkerSize',10);
            
            intoyReCreate{ii}.(conditions{cc}).(stabilization{ss}).Ecc = [structSizePerformance(newIdx).Eccentricity];
            intoyReCreate{ii}.(conditions{cc}).(stabilization{ss}).Perf = [structSizePerformance(newIdx).Performance];
            intoyReCreate{ii}.(conditions{cc}).(stabilization{ss}).Sizes = round([structSizePerformance(newIdx).StimulusSize],1);
            
            hold on;
            titleCondition{counter} = sprintf('%s,%s,%.1f',...
                conditions{cc},stabilization{ss}, ...
                mean(round([structSizePerformance(newIdx).StimulusSize],1)));
            counter = counter + 1;
        end
    end
    legend(legSaved,titleCondition,'Location','southwest')
    xlim([-0.5 26]);
    ylim([0 1]);
    title(subjectsAll{ii});
    xlabel('Eccentricity')
    ylabel('Performance')
end
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iPerformanceChanges.epsc',...
    params.fig.FOLDED));
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iPerformanceChanges.png',...
    params.fig.FOLDED));

%% Make Figure Similar to Intoy 2020
figure;
% for ii = 1:4
%     val = eccNames{ii};
subplot(1,2,1)
% for i = 1:([2 5 6])
for i = 1:length(subjectsAll)
    path = intoyReCreate{i}.Uncrowded.Unstabilized;
    %         temp = [allValuesThresholds.Crowded.Unstabilized.(val)];
    plot(path.Ecc, path.Perf, '*')
    hold on
    meanVals(i,1) = mean(path.Perf((path.Ecc == 0)));
    meanVals(i,2) = mean(path.Perf((path.Ecc == 10)));
    meanVals(i,3) = mean(path.Perf((path.Ecc == 15)));
    meanVals(i,4) = mean(path.Perf((path.Ecc == 25)));
    
end
%     ylim([.3 1])
hold on
legI(1) = plot([0 10 15 25],mean(meanVals),'-o','Color','b');
ylim([.3 1])
title('Unstabilized')

subplot(1,2,2)
% figure;
counter = 1;
clear meanVals
for i = [2 5 6]
    path = intoyReCreate{i}.Uncrowded.Stabilized;
    %         temp = [allValuesThresholds.Crowded.Unstabilized.(val)];
    plot(path.Ecc, path.Perf, '*')
    hold on
    meanVals(counter,1) = mean(path.Perf((path.Ecc == 0)));
    meanVals(counter,2) = mean(path.Perf((path.Ecc == 10)));
    meanVals(counter,3) = mean(path.Perf((path.Ecc == 15)));
    meanVals(counter,4) = mean(path.Perf((path.Ecc == 25)));
    counter = counter + 1;
end
ylim([.3 1])
hold on
legI(2) = plot([0 10 15 25],mean((meanVals)),'-o','Color','r');

set(gca,'xtick',[0 10 15 25])
title('Stabilized')
% legend(legI,'Unstabilized','Stabilized')
title('Stabilized')
suptitle('Uncrowded')
% end
saveas(gcf,'../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/MimicIntoy.epsc');
saveas(gcf, ...
    '../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/MimicIntoy.png');

%% Individual Value Characteristics
for stIdx = 1:2
    for cIdx = 1:2
        cStrs = {tableData.condition};
        condIdx = (ismember(cStrs,conditions{cIdx}));
        sStrs = {tableData.stabilized};
        stabIdx = (ismember(sStrs,stabilization{stIdx}));
        %         tempTestingIdx = find(condIdx & stabIdx);
        chars = {'Threshold','DiffusionConstant','Span','Area','Curvature','Speed'};
        
        threshc = conditions{cIdx};
        clear matrixData
        figure('units','normalized','outerposition',[0 0 1 1]);
        for ch = 1:length(chars)
            subplot(2,3,ch);
            for i = 1:length(eccNames)
                for ii = 1:length(subjectsAll)
                    eStrs = {tableData.eccentricity};
                    eccIdx = (ismember(eStrs,eccNames{i}));
                    subStrs = {tableData.subject};
                    sIdx = (ismember(subStrs,subjectsAll{ii}));
                    testingIdx = find(condIdx & stabIdx & eccIdx & sIdx);
                    if isempty(testingIdx)
                        continue;
                    elseif  isempty(tableData(testingIdx).(chars{ch}))
                        continue;
                    end
                    leg(ii) = plot(i, tableData(testingIdx).(chars{ch}),...
                        '-o','LineWidth',5,'MarkerSize',10, 'Color', c(ii,:));
                    hold on;
                    matrixData.(eccNames{i})(ii,ch) = ...
                        tableData(testingIdx).(chars{ch});
                end
                title(chars{ch})
                testingMeanIdx = find(condIdx & stabIdx & eccIdx);
                errorbar(i, mean([tableData(testingMeanIdx).(chars{ch})]), ...
                    std([tableData(testingMeanIdx).(chars{ch})]), 'vertical',...
                    'o','MarkerSize',10, 'Color','k','MarkerFaceColor','k');
                hold on
            end
            
            ylabel(chars{ch});
            xlim([0.5 length(eccNames)+0.5])
            set(gca,'xtick',[1 2 3 4],'xticklabel', ecc,'FontSize',12,...
                'FontWeight','bold')
            
            %         set(gcf, 'Position', [100, 100, 1000, 800]);
        end
        suptitle(sprintf('%s%s', conditions{cIdx}, stabilization{stIdx}));
        saveas(gcf,...
            sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/FoldedDriftChar%s%s.png', ...
            conditions{cIdx}, stabilization{stIdx}));
        saveas(gcf,...
            sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/FoldedDriftChar%s%s.epsc', ...
            conditions{cIdx}, stabilization{stIdx}));
        close all
        figure('units','normalized','outerposition',[0 0 1 1]);
        
        for i = 1:length(eccNames)
            subplot(2,2,i)
            [p1,ax] = plotmatrix(matrixData.(eccNames{i}));
            axis square
            title(sprintf('%s', ...
                eccNames{i}))
            hold on
            for n = 1:length(chars)
                ax(n,1).YLabel.String = chars{n};
                ax(length(chars),n).XLabel.String = chars(n);
                
                for nn= 1:length(chars)
                    lsline(ax(n,nn));
                end
            end
        end
        suptitle(sprintf('%s %s', conditions{cIdx},...
            stabilization{stIdx}));
        saveas(gcf,...
            sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/FoldedDriftCharPoints%s%s.png', ...
            conditions{cIdx}, stabilization{stIdx}));
        saveas(gcf,...
            sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/FoldedDriftCharPoints%s%s.epsc', ...
            conditions{cIdx}, stabilization{stIdx}));
        close all
    end
end

% % %% plot the ACTUAL ecc distance
% % % % % for cIdx = 1:2 %do not run with stabilization (they are already plotted as actual ecc distance
% % % % %     threshCond = subjectThreshCond.(conditions{cIdx}).(stabilization{1});
% % % % %
% % % % %     figure;
% % % % %     % subplot(2,1,1)
% % % % %     plotEccDistanceFromSpan(subjectsAll, threshCond, c, ecc, eccNames,conditions{cIdx},string(em))
% % % % %     subplot(2,1,1);
% % % % %     hold on
% % % % %     xlabel('Eccentricity (arcmin)')
% % % % %     ylabel('Strokewidth Threshold')
% % % % %
% % % % %     subplot(2,1,2);
% % % % %     % hold on
% % % % %     xlabel('Eccentricity (arcmin)')
% % % % %     ylabel('Normalized Threshold')
% % % % %     suptitle(sprintf('%s', conditions{cIdx}))
% % % % %
% % % % %     if params.fig.FOLDED
% % % % %         saveas(gcf,...
% % % % %             '../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/FoldedGazePosActualUnc.png');
% % % % %         saveas(gcf,...
% % % % %             '../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/FoldedGazePosActualUnc.epsc');
% % % % %     else
% % % % %         saveas(gcf,...
% % % % %             '../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/GazePosActualUnc.png');
% % % % %         saveas(gcf,...
% % % % %             '../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/GazePosActualUnc.epsc');
% % % % %     end
% % % % %
% % % % %     close all;
% % % % % end
% %
% % % figure;
% % % plotEccDistanceFromSpan(subjectsAll, subjectThreshCro, c, ecc, eccNames,'Crowded',string(em))
% % % subplot(2,1,1);
% % % hold on
% % % xlabel('Eccentricity (arcmin)')
% % % ylabel('Strokewidth Threshold')
% % %
% % % subplot(2,1,2);
% % % % hold on
% % % xlabel('Eccentricity (arcmin)')
% % % ylabel('Normalized Threshold')
% % %
% % % suptitle('Crowded')
% % % % for test == 1
% % % % plot(cell2mat(eccTargetAll{ii,:}),eccPerformanceAll{ii,:})
% % % % hold on
% % % % end
% % % if params.fig.FOLDED
% % %     saveas(gcf,...
% % %         '../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/FoldedGazePosActualCro.png');
% % %     saveas(gcf,...
% % %         '../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/FoldedGazePosActualCro.epsc');
% % % else
% % %     saveas(gcf,...
% % %         '../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/GazePosActualCro.png');
% % %     saveas(gcf,...
% % %         '../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/IndSubject/GazePosActualCro.epsc');
% % % end
% %
% % %% Plot performance at each ACTUAL distance
% % % % % for ii = 1:length(subjectsAll)
% % % % %     counter1 = 1;
% % % % %     counter2 = 1;
% % % % %     counter3 = 1;
% % % % %     counter4 = 1;
% % % % %     counter5 = 1;
% % % % %     counter6 = 1;
% % % % %     counter7 = 1;
% % % % %     for ee = 1:length(ecc)
% % % % %         subj = sprintf('%s',(subjectsAll{ii}));
% % % % %         actualEcc = (str2double(cell2mat(regexp(ecc{ee},'\d*','Match'))));
% % % % %         if contains((eccNames{ee}),'Nasal')
% % % % %             actualEcc = -actualEcc;
% % % % %         end
% % % % %         eccTarget = variableEcc.(subj).eccTarget;
% % % % %         eccPerformance = variableEcc.(subj).eccPerformance;
% % % % %         for numSamp = 1:length(eccTarget)
% % % % %             if ~isempty(eccTarget{ee,numSamp})
% % % % %                 if abs(eccTarget{ee,numSamp} - actualEcc) <= 3
% % % % %                     perfEcc.(subj).exact(1,counter1) = eccTarget{ee,numSamp};
% % % % %                     perfEcc.(subj).exact(2,counter1) = eccPerformance{ee,numSamp};
% % % % %                     perfEcc.(subj).exact(3,counter1) = actualEcc;
% % % % %                     counter1 = counter1+1;
% % % % %                 elseif abs(eccTarget{ee,numSamp} - actualEcc) < 6
% % % % %                     perfEcc.(subj).six(1,counter7) = eccTarget{ee,numSamp};
% % % % %                     perfEcc.(subj).six(2,counter7) = eccPerformance{ee,numSamp};
% % % % %                     perfEcc.(subj).six(3,counter7) = actualEcc;
% % % % %                     counter7 = counter7+1;
% % % % %                 elseif abs(eccTarget{ee,numSamp} - actualEcc) < 12
% % % % %                     perfEcc.(subj).twelve(1,counter2) = eccTarget{ee,numSamp};
% % % % %                     perfEcc.(subj).twelve(2,counter2) = eccPerformance{ee,numSamp};
% % % % %                     perfEcc.(subj).twelve(3,counter2) = actualEcc;
% % % % %                     counter2 = counter2+1;
% % % % %                 elseif abs(eccTarget{ee,numSamp} - actualEcc) < 17
% % % % %                     perfEcc.(subj).seventeen(1,counter3) = eccTarget{ee,numSamp};
% % % % %                     perfEcc.(subj).seventeen(2,counter3) = eccPerformance{ee,numSamp};
% % % % %                     perfEcc.(subj).seventeen(3,counter3) = actualEcc;
% % % % %                     counter3 = counter3+1;
% % % % %                 elseif abs(eccTarget{ee,numSamp} - actualEcc) < 21
% % % % %                     perfEcc.(subj).twenone(1,counter4) = eccTarget{ee,numSamp};
% % % % %                     perfEcc.(subj).twenone(2,counter4) = eccPerformance{ee,numSamp};
% % % % %                     perfEcc.(subj).twenone(3,counter4) = actualEcc;
% % % % %                     counter4 = counter4+1;
% % % % %                 elseif abs(eccTarget{ee,numSamp} - actualEcc) < 27
% % % % %                     perfEcc.(subj).tweseven(1,counter5) = eccTarget{ee,numSamp};
% % % % %                     perfEcc.(subj).tweseven(2,counter5) = eccPerformance{ee,numSamp};
% % % % %                     perfEcc.(subj).tweseven(3,counter5) = actualEcc;
% % % % %                     counter5 = counter5+1;
% % % % %                 elseif abs(eccTarget{ee,numSamp} - actualEcc) > 27
% % % % %                     perfEcc.(subj).deg(1,counter6) = eccTarget{ee,numSamp};
% % % % %                     perfEcc.(subj).deg(2,counter6) = eccPerformance{ee,numSamp};
% % % % %                     perfEcc.(subj).deg(3,counter6) = actualEcc;
% % % % %                     counter6 = counter6+1;
% % % % %                 end
% % % % %             end
% % % % %         end
% % % % %         %         plot(eccTarget{ii,:},eccPerformance{ii,:})
% % % % %     end
% % % % % end

%% Load in Old Data for Comparison
ogSubjects = {'Z002DDPI','AshleyDDPI','Z046DDPI','Z084DDPI'};
dpiOGSubjectFileNames = {'Z002','Ashley','Z046','Z084'};
for ii = 1:length(ogSubjects)
    filename = sprintf('MATFiles/%s_Uncrowded_Unstabilized_Drift_0ecc_Threshold.mat',...
        dpiOGSubjectFileNames{ii});
    tempFile = load(filename);
    tempThresh = thresholdSWVals.Uncrowded.Unstabilized.Center0ecc.vals;
    
    path = tempFile.threshInfo.em.ecc_0.(tempThresh);
    dcThreshOG.(ogSubjects{ii}).sw_value = tempThresh;
    dcThreshOG.(ogSubjects{ii}).dsq = path.dCoefDsq;
    dcThreshOG.(ogSubjects{ii}).curve = mean(cell2mat(path.curvature));
    counter = 1;
    for i = 1:length(path.mn_speed)
        temp = path.mn_speed{i};
        if ~isnan(temp)
            tempSpeed(counter) = temp;
            counter = counter + 1;
        end
    end
    dcThreshOG.(ogSubjects{ii}).speed = mean(tempSpeed);
    dcThreshOG.(ogSubjects{ii}).thresh = tempFile.threshInfo.thresh;
    dcThreshOG.(ogSubjects{ii}).performance = path.performanceAtSize;
end

%% Reformat Variables
for ee = 1:length(ecc)
    singleEcc = (eccNames{ee});
    for crow = 1:2
        if crow == 1
            condition = 'Uncrowded';
        else
            condition = 'Crowded';
        end
        for ii = 1:subNum
            temp = fieldnames(subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).em);
            nameEcc = temp{startsWith(temp,"ecc_",'IgnoreCase',true)};
            subject = subjectsAll{ii};
            swUnc = thresholdSWVals.(condition).Unstabilized.(singleEcc)(ii).vals;
            path = subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).em.(nameEcc).(swUnc);
            
            dataSummaries.(condition).Subject(ee,ii) = {subject};
            dataSummaries.(condition).Span(ee,ii) = mean(path.span);
            dataSummaries.(condition).dsq(ee,ii) = path.dCoefDsq;
            dataSummaries.(condition).curve(ee,ii) = mean(cell2mat(path.curvature));
            counter = 1;
            for i = 1:length(path.mn_speed)
                temp = path.mn_speed{i};
                if ~isnan(temp)
                    tempSpeed(counter) = temp;
                    counter = counter + 1;
                end
            end
            dataSummaries.(condition).speed(ee,ii) = mean(tempSpeed);
            dataSummaries.(condition).thresh(ee,ii) = subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).thresh;
            dataSummaries.(condition).sw_value{ee,ii} = swUnc;
            dataSummaries.(condition).Area(ee,ii) =  path.areaCovered;
            dataSummaries.(condition).ThresholdSW(ee,ii) = (subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).thresh);
            dataSummaries.(condition).order{ee,ii} = singleEcc;
            
        end
    end
    
end

% [heightDataSummaries, widthDataSummaries] = size(dataSummaries.Uncrowded.thresh);
for ii = 1:4
    path = dataSummaries.Uncrowded;
    ogCompare.Uncrowded.(subjectsAll{ii}).dsq = path.dsq(1,ii);
    ogCompare.Uncrowded.(subjectsAll{ii}).curve = path.curve(1,ii);
    ogCompare.Uncrowded.(subjectsAll{ii}).speed = path.speed(1,ii);
    ogCompare.Uncrowded.(subjectsAll{ii}).thresh = path.thresh(1,ii);
    ogCompare.Uncrowded.(subjectsAll{ii}).sw_value = path.sw_value{1,ii};
end
tic

%% Compare Measures between Systems
clear ii; clear leg
figure('units','normalized','outerposition',[0 0 1 1]);
ogSubjects = fieldnames(dcThreshOG);
ogMeasures = fieldnames(dcThreshOG.(ogSubjects{1}));
for i = 2:length(ogMeasures)-1
    subplot(2,2,i-1)
    for ii = 1:length(ogSubjects)
        plot(1,dcThreshOG.(ogSubjects{ii}).(ogMeasures{i}),...
            'o','MarkerSize',10, 'Color', c(ii,:),...
            'MarkerFaceColor',c(ii,:));
        hold on
        leg(ii) = plot(2,ogCompare.Uncrowded.(ogSubjects{ii}).(ogMeasures{i}),'o','MarkerSize',10, 'Color', c(ii,:),...
            'MarkerFaceColor',c(ii,:));
        line([1 2], [dcThreshOG.(ogSubjects{ii}).(ogMeasures{i}) ogCompare.Uncrowded.(ogSubjects{ii}).(ogMeasures{i})]...
            , 'Color', c(ii,:));
    end
    set(gca,'xtick',[1 2],'xticklabel', {'DPI', 'dDPI'},'FontSize',12,...
        'FontWeight','bold')
    xlim([ 0.5 2.5])
    ylabel((ogMeasures{i}))
    legend(leg,dpiOGSubjectFileNames,'Location','southeast');
end
suptitle('Same Size Stimulus & Subject Comparisons')
saveas(gcf,'../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/MeasuresingOGDatavsTopology.png');

