function plotDriftCharH(em1, em2, type, control, subjectsAll, lim)
figure;
for ii = 1:length(subjectsAll)
    subplot(1,2,1)
    plot(control(ii), em1(ii), 'o')
    hold on
    subplot(1,2,2)
     if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
         continue;
     end
    plot(control(ii), em2(ii), 'o')
    hold on
end
subplot(1,2,1)
errorbar(0.2, mean(em1(~control)), nanstd(em1(~control)))
hold on
errorbar(1.2, mean(em1(find(control))), nanstd(em1(find(control))))

xlim([-.25 1.5])
xticks([0 1]);
     xticklabels({'Patient','Control'});
ylabel(type)
ylim([0 lim])
[h,p] = ttest2(em1(~control),em1(find(control))); 
title(sprintf('Fixation, p = %.3f',p))

subplot(1,2,2)
errorbar(0.2, nanmean(em2(~control)), nanstd(em2(~control)))
hold on
errorbar(1.2, nanmean(em2(find(control))), nanstd(em2(find(control))))

xlim([-.25 1.5])
xticks([0 1]);
     xticklabels({'Patient','Control'});
ylabel(type)   
ylim([0 lim])
[h,p] = ttest2(em2(~control),em2(find(control))); 
title(sprintf('Task, p = %.3f',p))

end