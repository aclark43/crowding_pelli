function em = calculateEMChar(valueGroup,params,pptrials,em)

idx = valueGroup.idx;
strokeWidth = valueGroup.strokeWidth;
ecc = valueGroup.ecc;
stimulusSize = valueGroup.stimulusSize;

counter = 1;
xValues = [];
yValues = [];

for idxValue = 1:length(idx)
    i = idx(idxValue);
    dataStruct.id(counter) = i;
    
    timeOn = round(pptrials{i}.TimeTargetON/params.samplingRate);
    timeOff = floor(min(pptrials{i}.TimeTargetOFF/params.samplingRate, pptrials{i}.ResponseTime/params.samplingRate));
    
    x = pptrials{i}.x.position(timeOn:timeOff) + pptrials{i}.pixelAngle * pptrials{i}.xoffset;
    y = pptrials{i}.y.position(timeOn:timeOff) + pptrials{i}.pixelAngle * pptrials{i}.yoffset;
    
    dataStruct.position(counter).x = x;
    dataStruct.position(counter).y = y;
    xValues = [xValues, x];
    yValues = [yValues, y];
    
    dataStruct.span(counter) = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95); %Caclulates Span
    dataStruct.flankers(counter) = pptrials{i}.FlankerOrientations;
    dataStruct.response(counter) = pptrials{i}.Response;
    dataStruct.responseTime(counter) = pptrials{i}.ResponseTime;
    dataStruct.target(counter) = pptrials{i}.TargetOrientation;
    dataStruct.amplitude(counter) = sqrt((pptrials{i}.x.position(timeOff) - pptrials{i}.x.position(timeOn))^2 + ...
        (pptrials{i}.y.position(timeOff) - pptrials{i}.y.position(timeOn))^2);
    dataStruct.correct(counter) = double(pptrials{i}.Correct);
    dataStruct.time(counter) = mean(pptrials{i}.TimeTargetOFF-pptrials{i}.TimeTargetON);
    
    dataStruct.meanSpan(counter) = mean(dataStruct.span);
    dataStruct.stdSpan(counter) = ...
            std(dataStruct.span)/...
            sqrt(length(dataStruct.span));
        
    dataStruct.meanAmplitude(counter) = mean(dataStruct.amplitude(counter));
    dataStruct.stdAmplitude(counter) = ...
            std(dataStruct.amplitude)/sqrt(length(dataStruct.amplitude));
        
    dataStruct.prlDistance(counter) = mean(sqrt(x.^2 + y.^2));     
    dataStruct.prlDistanceX(counter) = mean(abs(x));
    dataStruct.prlDistanceY(counter) = mean(abs(y));
    
    [~, dataStruct.velocity(counter), ...
        velX{counter}, ...
        velY{counter}] = ...
            CalculateDriftVelocity(dataStruct.position(counter), 1);
        
        [~, dataStruct.instSpX{counter},...
            dataStruct.instSpY{counter},...
            dataStruct.mn_speed{counter},...
            dataStruct.driftAngle{counter},...
            dataStruct.curvature{counter},...
            dataStruct.varx{counter},...
            dataStruct.vary{counter}] = ...
        getDriftChar(...
            dataStruct.position(counter).x, ...
            dataStruct.position(counter).y,...
            41, 1, 250);
        
    counter = counter + 1;
end
%% Overall Measurements
dataStruct.ccDistance = unique(stimulusSize)*1.4 - stimulusSize/2;
dataStruct.stimulusSize = unique(stimulusSize);
dataStruct.performanceAtSize = round(sum(dataStruct.correct)/length(dataStruct.correct),2);   

if strcmp('D',params.machine)
    sampling = 341;
else
    sampling = 1000;
end

forDSQCalc.x = {dataStruct.position.x};
forDSQCalc.y = {dataStruct.position.y};

[~,~,dataStruct.dCoefDsq, ~, dataStruct.Dsq, ...
    dataStruct.SingleSegmentDsq,~,~] = ...
    CalculateDiffusionCoef(sampling, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));

[ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/100)),31); %Checks strange artifacts
[ xValues, yValues] = checkXYBound(xValues, yValues, 30); %Checks boundries

limit.xmin = floor(min(xValues));
limit.xmax = ceil(max(xValues));
limit.ymin = floor(min(yValues));
limit.ymax = ceil(max(yValues));

n_bins = 20;

result = MyHistogram2(xValues, yValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
dataStruct.result = result./(max(max(result)));

dataStruct.areaCovered = CalculateTheArea(xValues,yValues ,0.68, max(abs(cell2mat(struct2cell(limit)))), n_bins);

em.(ecc).(strokeWidth) = dataStruct;
end