function reAnalyzeDriftMeasures(allVariables)


for ii = 1:length(allVariables)
    if ~isempty(allVariables(ii).snellen_drift_speed)
        idx(ii) = 1;
    else
        idx(ii) = 0;
    end
end
idxFinds = find(idx);
% idx = find(allVariables.bcea_snellen == []);

tableAll = struct2table(allVariables);


clear dataStruct
for i = idxFinds
    counter = 1;
    dataStruct =[];
    for pp = 1:length(allVariables(i).all_snellen.fix)
        
        x = allVariables(i).all_snellen.fix(pp).x;
        y = allVariables(i).all_snellen.fix(pp).y;
        
%         if length(x) < 300
%             continue
%         end
        [~,~,dcTemp] = ...
            CalculateDiffusionCoef(331, struct('x',x,'y',y));
        
%         if dcTemp < 3 || dcTemp > 100
%             continue
%         end
        
        [h1,pValue,stat,cValue] = adftest(x);
        [h2,pValue,stat,cValue] = adftest(y);

        dataStruct.cValue(counter) = mean([h h2]);
        
        buffer = round(20/ 1000 * 330);
        if rem(buffer, 2) == 0
            buffer = buffer + 1;
        end
        
        params.em.drift.sgfilt = struct('winsize', 31, 'order', 3);
        smoothing = round(params.em.drift.sgfilt.winsize / 1000 * 331);
        if rem(smoothing, 2) == 0
            smoothing = smoothing + 1;
        end
        
        [~, dataStruct.instSpX{counter},...
            dataStruct.instSpY{counter},...
            dataStruct.mn_speed(counter),...
            dataStruct.driftAngle{counter},...
            dataStruct.curvature(counter),...
            dataStruct.varx{counter},...
            dataStruct.vary{counter},...
            dataStruct.bcea{counter},...
            dataStruct.span(counter), ...
            dataStruct.amplitude(counter), ...
            dataStruct.prlDistance(counter),...
            dataStruct.prlDistanceX(counter), ...
            dataStruct.prlDistanceY(counter)] = ...
            getDriftChar(x, y, smoothing, buffer, 200, 331);
        %smoothing, cutseg (or buffer), maxSpeed
        dataStruct.duration(counter) = length(x);
        forDC(counter).x = x(buffer:end-buffer);
        forDC(counter).y = y(buffer:end-buffer);
        counter = counter + 1;
        
    end
    %     [~,dataStruct.Bias,dataStruct.dCoefDsq, ~, dataStruct.Dsq, ...
    %         dataStruct.SingleSegmentDsq,~,~] = ...
    %         CalculateDiffusionCoef(330, allVariables(i).all_snellen.fix);
    if counter > 1
        [~,dataStruct.Bias,dataStruct.dCoefDsq, ~, dataStruct.Dsq, ...
            dataStruct.SingleSegmentDsq,~,~] = ...
            CalculateDiffusionCoef(331, forDC);
        dataStruct.mean_curvature = nanmean(dataStruct.curvature);
        dataStruct.mean_speed = nanmean(dataStruct.mn_speed);
        redoneAnalysis{i} = dataStruct;
        Speed(i) = dataStruct.mean_speed;
        Curv(i) = dataStruct.mean_curvature;
        Dc(i) = dataStruct.dCoefDsq;
        Duration(i) = mean(dataStruct.duration);
        CriticalValue(i) = mean(dataStruct.cValue);
    end
end


%% 3 extreme cases
[Xe{1,1}, Ye{1,1}] = EM_Brownian(5,  331, 500, 1);
[Xe{1,2}, Ye{1,2}] = EM_Brownian(20,  331, 500, 1);
figure;
plot(Xe{1,1}, Ye{1,1});
hold on
plot(Xe{1,2}, Ye{1,2});
axis([-20 20 -20 20])
axis square



counter = 1; dataStruct = [];
for ii = 1:20
    [Xe{1,1}, Ye{1,1}] = EM_Brownian(5,  331, 500, 1);
    [~, instSpX{counter},...
        dataStruct.instSpY{counter},...
        dataStruct.mn_speed(counter),...
        dataStruct.driftAngle{counter},...
        dataStruct.curvature(counter),...
        dataStruct.varx{counter},...
        dataStruct.vary{counter},...
        dataStruct.bcea{counter},...
        dataStruct.span(counter), ...
        dataStruct.amplitude(counter), ...
        dataStruct.prlDistance(counter),...
        dataStruct.prlDistanceX(counter), ...
        dataStruct.prlDistanceY(counter)] = ...
        getDriftChar(Xe{1,1}, Ye{1,1}, smoothing, buffer, 500, 331);
    
    [~,~,dataStruct.dcTemp(counter)] = ...
        CalculateDiffusionCoef(331, struct('x',Xe{1,1}(buffer:end-buffer),'y',Ye{1,1}(buffer:end-buffer)));
%     dataStruct5.speed(counter) = mean(dataStruct.mn_speed)
%     dataStruct5.curvature
%     dataStruct5.dc
    counter = counter + 1;
end
counter = 1; dataStruct2 = [];
for ii = 1:20
    [Xe{1,1}, Ye{1,1}] = EM_Brownian(20,  331, 500, 1);
    [~, instSpX{counter},...
        dataStruct2.instSpY{counter},...
        dataStruct2.mn_speed(counter),...
        dataStruct2.driftAngle{counter},...
        dataStruct2.curvature(counter),...
        dataStruct2.varx{counter},...
        dataStruct2.vary{counter},...
        dataStruct2.bcea{counter},...
        dataStruct2.span(counter), ...
        dataStruct2.amplitude(counter), ...
        dataStruct2.prlDistance(counter),...
        dataStruct2.prlDistanceX(counter), ...
        dataStruct2.prlDistanceY(counter)] = ...
        getDriftChar(Xe{1,1}, Ye{1,1}, smoothing, buffer, 500, 331);
    
    [~,~,dataStruct2.dcTemp(counter)] = ...
        CalculateDiffusionCoef(331, struct('x',Xe{1,1}(buffer:end-buffer),'y',Ye{1,1}(buffer:end-buffer)));
    counter = counter + 1;
end


figure;
subplot(1,3,2)
boxplot([dataStruct.curvature',dataStruct2.curvature'])
xticks([1 2])
xticklabels({'5','20'});
xlabel('DC')
title('Curvature');

% figure;
subplot(1,3,1)
boxplot([dataStruct.mn_speed',dataStruct2.mn_speed'])
xticks([1 2])
xticklabels({'5','20'});
xlabel('DC')
title('Speed');

% figure;
subplot(1,3,3)
boxplot([dataStruct.dcTemp',dataStruct2.dcTemp'])
xticks([1 2])
xticklabels({'5','20'});
xlabel('DC')
title('D');

%% ReDone AMC Analysis
mod1 = fitlm([allVariables(idxFinds).ODAxialLength],...
    Speed(idxFinds))

mod2 = fitlm([allVariables(idxFinds).ODAxialLength],...
    Curv(idxFinds))

mod3 = fitlm([allVariables(idxFinds).ODAxialLength],...
    Dc(idxFinds))

figure;
subplot(1,3,1)
plot(mod1)
title('Speed')
legend off
subplot(1,3,2)
plot(mod2)
title('Curvature');
legend off
subplot(1,3,3)
plot(mod3)
title('DC');
legend off
suptitle('AMC Redone');
hold on


figure;
mod4 = fitlm([allVariables(idxFinds).ODAxialLength],...
    Duration(idxFinds))
plot(mod4)

figure;
mod5 = fitlm([allVariables(idxFinds).ODAxialLength],...
    CriticalValue(idxFinds)*100)
plot(mod5)
ylabel('Percent of Non-Brownian Trials')
xlabel('Axial Length');
legend off

%%
mod1 = fitlm([allVariables(idxFinds).ODAxialLength],...
    [allVariables(idxFinds).snellen_drift_speed])

mod2 = fitlm([allVariables(idxFinds).ODAxialLength],...
    [allVariables(idxFinds).snellen_drift_curv])

mod3 = fitlm([allVariables(idxFinds).ODAxialLength],...
    [allVariables(idxFinds).snellen_drift_diffcon])

figure;
subplot(1,3,1)
plot(mod1)
title('Speed')
legend off
subplot(1,3,2)
plot(mod2)
title('Curvature');
legend off
subplot(1,3,3)
plot(mod3)
title('DC');
legend off
suptitle('OG Analysis')
end