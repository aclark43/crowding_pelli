function probDist = calcProbability(startMicroSaccade, endMicroSaccade, condition)

probDist = struct(...
               'numTrials','',...
               'numMS','',...
               'probBGStart','',...
               'probLeftFlankerStart','',...
               'probRightFlankerStart','',...
               'probTopFlankerStart','',...
               'probBottomFlankerStart','',...
               'probTargetStart','',...
               'probBGEnd','',...
               'probLeftFlankerEnd','',...
               'probRightFlankerEnd','',...
               'probTopFlankerEnd','',...
               'probBottomFlankerEnd','',...
               'probTargetEnd','');
           
           
        
[r,c] = size(startMicroSaccade); % get the column of A
totalNumMS = length(find(startMicroSaccade~=9));
totalNumTrials = r;
nr = 0:6;  %range of the numbers
pStart = zeros(length(nr),c); % create a matrix to store the probabilities
pEnd = zeros(length(nr),c);
for j = 1:c   % check each row
    if condition
        for i = 1:length(nr)   % check each particular number
            pStart(i,j) = length(find(startMicroSaccade(:,j) == nr(i)))/totalNumMS;% count the occurrence
            pEnd(i,j) = length(find(endMicroSaccade(:,j) == nr(i)))/totalNumMS;
        end
    else
        for i = 1:length(nr)   % check each particular number
            pStart(i,j) = length(find(startMicroSaccade(:,j) == nr(i)))/r;% count the occurrence
            pEnd(i,j) = length(find(endMicroSaccade(:,j) == nr(i)))/r;
        end
    end
end
    
    
probDist.numTrials = totalNumTrials;
probDist.numMS = totalNumMS;
probDist.probBGStart = pStart(1);
probDist.probLeftFlankerStart = pStart(2);
probDist.probRightFlankerStart = pStart(3);
probDist.probTopFlankerStart = pStart(4);
probDist.probBottomFlankerStart = pStart(5);
probDist.probTargetStart = pStart(6);
probDist.probBGEnd = pEnd(1);
probDist.probLeftFlankereEnd = pEnd(2);
probDist.probRightFlankerEnd = pEnd(3);
probDist.probTopFlankerEnd = pEnd(4);
probDist.probBottomFlankerEnd = pEnd(5);
probDist.probTargetEnd = pEnd(6);

    

end