% runCombingEsandAcuity

subjects.Pelli = {'Z023','Ashley','Z005','Z002','Z013',...
   'Z024','Z064','Z046','Z084','Z014',...
   'Z091','Z138','Z181'};
thresholds.Pelli = [1.39729229164304,1.20508034100499,1.72944612192300,1.52829824806032,1.29946864089991,1.80481929427609,1.64371059893067,1.55835186235709,1.80732360129430,1.69497611018725,1.97351526673095,1.47598069196781,2.64355493168692];
acuity.Pelli = 5./(5.*(thresholds.Pelli/2))*20;
dc.Pelli = [11.4434328079224,4.86753606796265,19.6812686920166,14.3462362289429,8.66790580749512,17.9170150756836,8.90083312988281,18.7623367309570,13.3849992752075,17.5736293792725,18.7725982666016,8.33597183227539,28.3468742370605];

temp = [1 2 3 3.5 4]/2*20

temp = ([2.25:.5:5.25]/1.4)/2*20


subjects.Es = {'Sanjana','Z126','A188','Soma','Janis','A144'} ;
acuity.Es = [21.2340507330993;16.6622795649566;12.5850072230724;15.4296798906461;19.6819504120748;16.5699661474260];
thresholds.Es = ([acuity.Es']/20)*2;
dc.Es = [13.8365964889526;11.1954774856567;13.5276660919189;11.8472118377686;12.6269044876099;10.9714384078980];

mdl = fitlm([dc.Pelli dc.Es'],...
    ([thresholds.Pelli thresholds.Es]/2).*20)
figure;plot(mdl)
xlabel({'Diffusion Constant (arcmin^2/sec)'});
ylabel('Snellen Acuity (20/X)');
text(3,24,'p = 0.0003','FontSize',14)
text(3,26,'r = 0.542','FontSize',14)
% text(3,2.4,'p = 0.023')
% text(3,2.6,'r = 0.542')
title('')
legend off
hold on
plot([dc.Pelli dc.Es'],([thresholds.Pelli thresholds.Es]/2).*20,'o',...
    'MarkerFaceColor',[225 243 167]/255,'MarkerEdgeColor','k',...
     'MarkerSize',10);
set(gca,'FontSize',18)

[temp_sort, order_sort] = sort([dc.Pelli dc.Es']);
allDc = [dc.Pelli dc.Es'];
critFreq = powerAnalysis_JustCriticalFrequency (allDc((order_sort)), 0);
close all
figure;

bar(temp_sort)
ylabel({'Diffusion Constant (arcmin^2/sec)'});
yyaxis right
plot([1:length(critFreq)],critFreq,'o-','LineWidth',5);
ylim([0 35])
xlabel('Subjects')
ylabel({'Maximally Enhanced Frequency (cycles/deg)'});

%% Acuity across different ECC

PelliAcuity.ecc0 = [1.98393119133287...
    1.42876021612053...
    1.32176429344889...
    1.74508643248537...
    1.73841399973553...
    1.75269508687282...
    1.475980691967808];

PelliAcuity.ecc10 = [2.67340308322853...
    1.67577831561333...
    1.62462446497465...
    1.81946616959073...
    1.49649087470929...
    1.74427813019665...
    1.55145353894176];

PelliAcuity.ecc15 = [2.28443973341033...
    1.58885148840104...
    1.71232789257514...
    1.79934987307573...
    1.53417546099183...
    1.44873019091408...
    1.37941903557395];

PelliAcuity.ecc25 = [2.90258025131422...
    1.63536192871064...
    1.97760080910193...
    1.87448654527928...
    1.68696429832207...
    1.80813515658761...
    1.56724502332754];

SnellenAcuity.ecc0 = [17.22813793...
21.23405073...
13.32847349...
16.66227956...
12.58500722...
15.42967989...
19.68195041...
16.56996615];

SnellenAcuity.ecc10 = [16.78083287...
19.24106271...
14.0979281...
17.22885472...
16.22905904...
11.99763017...
21.64082569...
14.84420499];

SnellenAcuity.ecc15 = [17.18000495...
21.23405073...
11.50242198...
19.3650996...
17.18496837...
16.17002908...
20.95825457...
19.15998909];

SnellenAcuity.ecc25 = [21.05878096...
29.63021375...
19.05703168...
27.53271031...
19.21524499...
17.40741415...
22.46780874...
23.00209193];


%% % Change Pelli to Snellen
colorMap = brewermap(10,'Set3');

for ii = 1:length(PelliAcuity.ecc0)
    PelliAcuitySnellenConvert(ii,1) = 5./(5.*(PelliAcuity.ecc0(ii)/2))*20;
    PelliAcuitySnellenConvert(ii,2) = 5./(5.*(PelliAcuity.ecc10(ii)/2))*20;
    PelliAcuitySnellenConvert(ii,3) = 5./(5.*(PelliAcuity.ecc15(ii)/2))*20;
    PelliAcuitySnellenConvert(ii,4) = 5./(5.*(PelliAcuity.ecc25(ii)/2))*20;
end
for ii = 1:length(SnellenAcuity.ecc0)
    SnellenAcuityMatrix(ii,1) = SnellenAcuity.ecc0(ii);
    SnellenAcuityMatrix(ii,2) = SnellenAcuity.ecc10(ii);
    SnellenAcuityMatrix(ii,3) = SnellenAcuity.ecc15(ii);
    SnellenAcuityMatrix(ii,4) = SnellenAcuity.ecc25(ii);
end

allAcuity = [PelliAcuitySnellenConvert; SnellenAcuityMatrix];
[numAllAcuitySub,~] = size(allAcuity);

figure;
[~,p,b,r] = LinRegression(...
    [0 10 15 25],mean(SnellenAcuityMatrix),0,NaN,1,0);
hold on
% subplot(2,2,1)
for ii = 1:length(SnellenAcuity.ecc0)
    plot([0.5 10.5 15.5 25.5],SnellenAcuityMatrix(ii,:),'-o',...
        'Color',colorMap(ii,:),'MarkerFaceColor',colorMap(ii,:)); 
    hold on
end
title('Tumbling Es')
ylim([10 30])
ylabel('Snellen Acuity (20/X)');
xlabel('Eccentricity (arcmin)');
hold on
% errorbar([0 10 15 25],mean(SnellenAcuityMatrix),...
%     sem(SnellenAcuityMatrix),'--',...
%     'Color','k');


% subplot(2,2,3)
% for ii = 1:length(PelliAcuity.ecc0)
%     plot([0 10 15 25],PelliAcuitySnellenConvert(ii,:),'-.'); 
%     hold on
% end
% title('Pelli Es')
% ylim([10 30])
% ylabel('Snellen Acuity (20/X)');
% xlabel('Eccentricity (arcmin)');
% subplot(2,2,2)

t = repmat([0 10 15 25],length(SnellenAcuity.ecc0),1);
m = reshape(t, 1, []);
m2 = reshape(SnellenAcuityMatrix, 1, []);
[~,p,b,r] = LinRegression(m,m2,0,NaN,1,0);
% title('Tumbling Es')
ylabel('Snellen Acuity (20/X)');
xlabel('Eccentricity (arcmin)');
subplot(2,2,4)

t = repmat([0 10 15 25],length(PelliAcuity.ecc0),1);
m = reshape(t, 1, []);
m2 = reshape(PelliAcuitySnellenConvert, 1, []);
[~,p,b,r] = LinRegression(m,m2,0,NaN,1,0);
% title('Pelli Es')
ylabel('Snellen Acuity (20/X)');
xlabel('Eccentricity (arcmin)');


subplot(3,2,[5 6])
% allAcuityNormed = [PelliAcuitySnellenConvert-PelliAcuitySnellenConvert(:,1);...
%     SnellenAcuityMatrix-SnellenAcuityMatrix(:,1)];
allAcuityNormed = [PelliAcuitySnellenConvert;...
    SnellenAcuityMatrix];
t = repmat([0 10 15 25],numAllAcuitySub,1);
m = reshape(t, 1, []);
m2 = reshape(allAcuityNormed, 1, []);

[~,p,b,r] = LinRegression(m,m2,0,NaN,1,0);
hold on

% errorbar([0 10 15 25],mean(numAllAcuitySub),(allAcuity),sem(allAcuity),'-o',...
%     'Color','k','MarkerFaceColor','k')
% ylim([15 25])
xlim([-1 28])


figure;
[p,tbl,stats] = anova1(SnellenAcuityMatrix);
results = multcompare(stats);





