function [ spnAmpEvaluate ] = buildSpanAmpEvaluate( em, params, ii, strokeWidth )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

spnAmpEvaluate.meanDriftAmplitude(ii) = em.ecc_0.(strokeWidth).meanAmplitude;
spnAmpEvaluate.meanDriftSpan(ii) = em.ecc_0.(strokeWidth).meanSpan;
spnAmpEvaluate.stdDriftAmplitude(ii) = em.ecc_0.(strokeWidth).stdAmplitude;
spnAmpEvaluate.stdDriftSpan(ii) = em.ecc_0.(strokeWidth).stdSpan;
%centerDistance = (round(2*trialChar.TargetStrokewidth(driftIdx)*1.4))*params.pixelAngle;
spnAmpEvaluate.ccDist(ii) = em.ecc_0.(strokeWidth).ccDistance;
spnAmpEvaluate.stimulusSize(ii) = double(ii) * params.pixelAngle;%em.ecc_0.(strokeWidth).stimulusSize;
spnAmpEvaluate.SW(ii) = (ii);
spnAmpEvaluate.pAcrossSWForSpan = [];

 %                 spnAmpEvaluate.span = em.ecc_0.(strokeWidth).span;
end

