function [ fig, stats ] = plotStyledLinearRegression( var1, var2, c)

small = min([var1 var2]);
large = max([var1 var2]);
[stats.h, stats.p, stats.b, stats.r] = LinRegression(var1,var2,0,NaN,1,0);
hold on
for ii = 1:length(var1)
fig(ii) = scatter(var1(ii),var2(ii), 200, c(ii,:),'filled');
end
axis([small large small large]);
x = [large small];
y = [large small];
line(x,y,'Color','k','LineStyle','--')
axis square;


end

