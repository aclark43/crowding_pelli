function percents = testIfOnFlankers (x, y, Coordinates)

tempOnTarg = (x > Coordinates.centerTarget_x1 &...
    x < Coordinates.centerTarget_x2 &...
    y > Coordinates.centerTarget_y1 &...
    y < Coordinates.centerTarget_y2);
percents.centerTarget = sum(tempOnTarg)/length(tempOnTarg);

leftFlanker = (x > Coordinates.leftFlanker_x1 &...
    x < Coordinates.leftFlanker_x2 &...
    y > Coordinates.leftFlanker_y1 &...
    y < Coordinates.leftFlanker_y2);
percents.leftFlanker = sum(leftFlanker)/length(leftFlanker);

rightFlanker = (x > Coordinates.rightFlanker_x1 &...
    x < Coordinates.rightFlanker_x2 &...
    y > Coordinates.rightFlanker_y1 &...
    y < Coordinates.rightFlanker_y2);
percents.rightFlanker = sum(rightFlanker)/length(rightFlanker);

topFlanker = (x > Coordinates.topFlanker_x1 &...
    x < Coordinates.topFlanker_x2 &...
    y > Coordinates.topFlanker_y1 &...
    y < Coordinates.topFlanker_y2);
percents.topFlanker = sum(topFlanker)/length(topFlanker);

bottomFlanker = (x > Coordinates.bottomFlanker_x1 &...
    x < Coordinates.bottomFlanker_x2 &...
    y > Coordinates.bottomFlanker_y1 &...
    y < Coordinates.bottomFlanker_y2);
percents.bottomFlanker = sum(bottomFlanker)/length(bottomFlanker);