function driftParamsNormalizedHist(drift, fixation, params, idx)
numBins = 20;
curveVec = 0:2:40;
speedVec = 20:4:100;

clear F
clear N

allCurve = [];
allSpan = [];
allSpeed = [];
allDC = [];
counter = 1;
for ii = find(idx)
        [N.Curve(counter,2:numBins+1)] = histcounts(drift.First200.curveVec{ii}, curveVec,'Normalization','probability');
        [N.Span(counter,2:numBins+1)] = histcounts(drift.First200.spanVec{ii}, 0:numBins,'Normalization','probability');
        [N.Speed(counter,2:numBins+1)] = histcounts(drift.First200.speedVec{ii}, speedVec,'Normalization','probability');
        [N.DC(counter,2:numBins+1)] = histcounts(drift.First200.dsqVec{ii}, 0:numBins,'Normalization','probability');
        allCurve = [allCurve drift.First200.curveVec{ii}];
        allSpan = [allSpan drift.First200.spanVec{ii}];
        allSpeed = [allSpeed drift.First200.speedVec{ii}];
        allDC = [allDC drift.First200.dsqVec{ii}];
        counter = counter + 1;
end

fixCurve = [];
fixSpan = [];
fixSpeed = [];
counter = 1;
for ii = find(idx)
        fixCurve = [fixCurve fixation.First200.ind_curve{ii}];
        fixSpan = [fixSpan fixation.First200.ind_span{ii}];
        fixSpeed = [fixSpeed fixation.First200.ind_speed{ii}];
        fixDC = [allDC fixation.First200.dsqVec{ii}];
        [F.Curve(counter,2:numBins+1)] = histcounts(fixation.First200.ind_curve{ii}, curveVec,'Normalization','probability');
        [F.Span(counter,2:numBins+1)] = histcounts(fixation.First200.ind_span{ii}, 0:numBins,'Normalization','probability');
        [F.Speed(counter,2:numBins+1)] = histcounts(fixation.First200.ind_speed{ii}, speedVec,'Normalization','probability');
        [F.DC(counter,2:numBins+1)] = histcounts(fixation.First200.dsqVec{ii}, 0:numBins,'Normalization','probability');
        counter = counter + 1;
end

colorCompare =  brewermap(4,'Paired');
figure;
subplot(2,1,1)
h(1) = errorbar_linehistogram( N.Curve, numBins, colorCompare(4,:), curveVec );
hold on
h(2) = errorbar_linehistogram( F.Curve, numBins, colorCompare(1,:), curveVec );

% plot(1:numBins,mean(N.Curve),'Color', 'k');
% hold on
% plot(1:numBins,mean(F.Curve),'Color', 'k');
% 
% figure;
% 
% h(1) = errorbar_linehistogram( allCurve, numBins, 'g' );
% hold on
% h(2) = errorbar_linehistogram( fixCurve, numBins, 'b' );
line([nanmean((allCurve)) nanmean((allCurve))],[0 .12],'Color',colorCompare(4,:),'LineStyle','--')
line([nanmean(fixCurve) nanmean(fixCurve)],[0 .12],'Color',colorCompare(2,:),'LineStyle','--')
% ylim([0 .12]);
xlim([0 30]);
[hC,pC] = ttest(drift.First200.curve(idx), fixation.First200.curve(idx));
title(sprintf('Curvature, p = %.3f',pC));
legend(h,{'Task','Fixation'});

% subplot(4,1,2)
% h(1) = histogram(allCurve,20,'Normalization','probability','FaceColor','g');
% hold on
% h(2) = histogram(fixCurve,20,'Normalization','probability','FaceColor','b');
% line([nanmean(allCurve) nanmean(allCurve)],[0 .05],'Color','g','LineStyle','--')
% line([nanmean(fixCurve) nanmean(fixCurve)],[0 .05],'Color','b','LineStyle','--')
% title('Span')
% legend(h,{'Task','Fixation'});

% subplot(4,1,2)
% h(1) = errorbar_linehistogram( N.Span, numBins, colorCompare(4,:) );
% hold on
% h(2) = errorbar_linehistogram( F.Span, numBins, colorCompare(1,:) );
% line([mean(allSpan) mean(allSpan)],[0 .6],'Color',colorCompare(4,:),'LineStyle','--')
% line([mean(fixSpan) mean(fixSpan)],[0 .6],'Color',colorCompare(2,:),'LineStyle','--')
% xlim([0 15])
% title('Span')
% legend(h,{'Task','Fixation'});

subplot(2,1,2)
h(1) = errorbar_linehistogram( N.Speed, numBins, colorCompare(4,:), speedVec);
hold on
h(2) = errorbar_linehistogram( F.Speed, numBins, colorCompare(1,:), speedVec);
line([nanmean((allSpeed)) nanmean((allSpeed))],[0 .06],'Color',colorCompare(4,:),'LineStyle','--')
line([nanmean((fixSpeed)) nanmean((fixSpeed))],[0 .06],'Color',colorCompare(2,:),'LineStyle','--')
xlim([20 100]);
[hS,pS] = ttest(drift.First200.speed(idx), fixation.First200.speed(idx));
title(sprintf('Speed, p = %.3f',pS));
% title('Speed')
legend(h,{'Task','Fixation'});

% subplot(4,1,4)
% h(1) = errorbar_linehistogram( N.DC, numBins, colorCompare(4,:) );
% hold on
% h(2) = errorbar_linehistogram( F.DC, numBins, colorCompare(1,:) );
% line([nanmean((allDC)) nanmean((allDC))],[0 .1],'Color',colorCompare(4,:),'LineStyle','--')
% line([nanmean((fixDC)) nanmean((fixDC))],[0 .1],'Color',colorCompare(2,:),'LineStyle','--')
% xlim([0 30]);
% title('DC')
% legend(h,{'Task','Fixation'});


% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DriftCharHist.png');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DriftCharHist.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/appendix/NormalizedFixUncrHistograms_CurveSpeed.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/appendix/NormalizedFixUncrHistograms_CurveSpeed.epsc');

end