%Run all conditions of ecc crowding
%   Detailed explanation goes here

close all
clc
clear

figures.folded = 1;
% FIGURES.actualEcc = 1;
%%Figure Location & Name%%
% subjects = {'Zoe'};

% Z014 never finished the crowded condition
% Z064 looks directly at the stimulus

% subjects = {'AshleyDDPI','Z002DDPI','Z046DDPI','Z084DDPI','Z064DDPI','Zoe','Z091'};%{Z014}
% subjects = {'AshleyDDPI','Z002DDPI','Z046','Z084','Z064','Z014','Zoe','Z091'};%{Z014}
subjects = {'AshleyDDPI','Z091','Z002DDPI','Z046DDPI','Z084DDPI'};%{Z014}

condition = {'Uncrowded','Crowded'}; %Figure 1 and Figure 2
stabilization = {'Unstabilized'};
em = {'Drift'};
% colorAcross = brewermap(12,'Accent');
colorAcross = brewermap(12,'Dark2');

% ecc = {'25eccNasal',...
%     '15eccNasal',...%%%%%%%%%%%%%%%%%%
%     '10eccNasal',...
%     '0ecc',...
%     '10eccTemp',...
%     '15eccTemp',...%%%%%%%%%%%%%%%%%%
%     '25eccTemp'};
ecc = {'0eccEcc',...
    '10ecc'...
    '15ecc',...
    '25ecc'};



firstSet = copper(3);
secondSet = [firstSet(3,1) firstSet(3,2) firstSet(3,3);...
    firstSet(2,1) firstSet(2,2) firstSet(2,3);...
    firstSet(1,1) firstSet(1,2) firstSet(1,3)];


if figures.folded
    c = [[1 0 0];firstSet];
else
    c = [firstSet;[1 0 0];secondSet];
end
threshold = [];
for stab = 1:length(stabilization)
    thresholds = plotAllPyscGraphsToghether(subjects, condition, stabilization, ...
        em, ecc, c);
%     threshold = [thresholds threshold];
end
if figures.folded
    for ii = 1:length(thresholds)
        thresholds(ii).ecc = abs(thresholds(ii).ecc);
    end
end
if figures.folded
     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/FoldedALLThreshCurves%s%s.png',...
         condition{1},em{1}));
     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/FoldedALLThreshCurves%s%s.epsc',...
         condition{1},em{1}));
%      saveas(gcf, ...
%          sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/individualCurves/ECCAll%s%s%s.png',...
%          subjects{subCount},condition{1},stabilization{1}));
 else
     filename = sprintf('%sthreshInfo',condition{1});
     save(filename,'thresholds')

     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/ALLThreshCurves%s%s.png',...
         condition{1},em{1}));
     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/ALLThreshCurves%s%s.epsc',...
         condition{1},em{1}));
     
 end


figure;
subplot(3,1,1)
counter = 1;
for ii = 1:length(thresholds)
    if ~isempty(thresholds(ii).subject)
        %         leg(ii) = NaN;
        %     else
        leg(counter) = plot(thresholds(ii).ecc,thresholds(ii).thresh,'-o','Color',colorAcross(ii,:));
        for i = 1:length(thresholds(ii).numTrials)
            numberTrials = thresholds(ii).numTrials(i);
            if numberTrials > 50
                text(thresholds(ii).ecc(i)+1,thresholds(ii).thresh(i), sprintf('%i',numberTrials));
                fprintf('\n%s = %i trials for ecc%i',thresholds(ii).subject, numberTrials,thresholds(ii).ecc(i));
            else
                text(thresholds(ii).ecc(i)+1,thresholds(ii).thresh(i), sprintf('%i',numberTrials), 'Color', 'r');
                fprintf('\n%s = %i trials for ecc%i SMALL!',thresholds(ii).subject, numberTrials,thresholds(ii).ecc(i));
            end
        end
        hold on
        counter = counter + 1;
        idx(ii) = 1;
    else
        idx(ii) = 0;
    end
end

%negative = nasal, positive = temporal
if figures.folded
    xlim([0 25])
    xticks([0 10 15 25])
    xlabel('Folded Target Eccentricity (arcmin)')
else
    xlim([-25 25])
    xticks([-25 -15 -10 0 10 15 25])
    xlabel('Target Eccentricity (arcmin - Nasal to Temp)')
end
ylabel('Threshold Strokewidth')
title('Threshold Strokewidths at Different Ecc')

for ii = 1:length(thresholds)
    if idx(ii)
        for eccZero = 1:length(thresholds(ii).ecc)
            if thresholds(ii).ecc(eccZero) == 0
                baseThresh(ii) = thresholds(ii).thresh(eccZero);
            end
        end
    else
        baseThresh(ii) = NaN;
    end
end
ylim([1 4]);

subplot(3,1,2)
counter = 1;
for ii = 1:length(thresholds)
    %     normalizedThreshBase(ii) = thresholds(ii).thresh - baseThresh(ii);
    if ~isempty(thresholds(ii).subject)
        leg(counter) = plot(thresholds(ii).ecc, (thresholds(ii).thresh - baseThresh(ii)) ,...
            '-o','Color',colorAcross(ii,:));
        hold on
        counter = counter + 1;
    end
end
% ylim([-0.5,1.5])

for i = 1:length(thresholds)
    for ii = 1:length(thresholds(i).thresh)  
        if thresholds(i).ecc(ii) < 0
            eccName = sprintf('ecc%iNasal',abs(thresholds(i).ecc(ii)));
        else
            eccName = sprintf('ecc%iTemp',abs(thresholds(i).ecc(ii)));
        end
        meanThresh.(eccName)(i) = thresholds(i).thresh(ii) - baseThresh(i);
        baseAtZero(i) = baseThresh(i);
    end
end

allNames = fieldnames(meanThresh);
for ii = 1:length(allNames)
    fieldName = char(allNames(ii));
    meanThreshAll(ii) = mean(meanThresh.(fieldName));
end
hold on

if figures.folded
    if length(subjects) == 1
        plot(thresholds.ecc, (meanThreshAll) ,'-*','Color','k')
    else
        plot([0 10 15 25], (meanThreshAll) ,'-*','Color','k')
    end
else
    plot([-25 -15 -10 0 10 15 25], (meanThreshAll) ,'-*','Color','k')
end
% for ii = 1:length(thresholds)
   
%     meanThresh(ii) = mean(thresholds(ii).thresh - baseThresh(ii));
% end
% plot(thresholds(ii).ecc, (thresholds(ii).thresh - baseThresh(ii)) ,'-o');

for numCondition = 1:length(stabilization)
    hline = refline([0 0]);
    hline.Color = 'k';
    if figures.folded
        xlim([0 25])
        xticks([0,10,15,25])
        xlabel('Folded Target Eccentricity (arcmin)')
    else
        xlim([-25 25])
        xticks([-25,-15,-10,0,10,15,25])
        xlabel('Target Eccentricity (arcmin - Nasal to Temp)')
    end
    
    ylabel('\Delta Strokewidth')
    title('\Delta Threshold Strokewidths at Different Ecc')
    suptitle(sprintf('%s',condition{1}));
    
    hL = subplot(3,1,3);
    poshL = get(hL,'position');     % Getting its position
    
    lgd = legend(hL,leg,subjects{find(idx)});
    set(lgd,'position',poshL);      % Adjusting legend's position
    axis(hL,'off');
    
    set(gcf, 'Position',  [2000, 100, 600, 900])
    
    hold on
end
% legend(leg,subjects,'Location','northwest')

 if figures.folded && length(subjects) == 1
     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/%sFoldedALLThreshIncludingLargeSpan%s%s.png',...
         subjects{1},condition{1},em{1}));
     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/%sFoldedALLThreshIncludingLargeSpan%s%s.epsc',...
         subjects{1},condition{1},em{1}));
 
 elseif figures.folded
     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/FoldedALLThreshIncludingLargeSpan%s%s.png',...
         condition{1},em{1}));
     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/FoldedALLThreshIncludingLargeSpan%s%s.epsc',...
         condition{1},em{1}));
 else
     filename = sprintf('%sthreshInfo',condition{1});
     save(filename,'thresholds')

     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/ALLThreshIncludingLargeSpan%s%s.png',...
         condition{1},em{1}));
     saveas(gcf, ...
         sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/ALLThreshIncludingLargeSpan%s%s.epsc',...
         condition{1},em{1}));
 end

%% Crowding Effect
load('UncrowdedthreshInfo')
Uncrowdedthresholds = thresholds;
load('CrowdedthreshInfo')
Crowdedthresholds = thresholds;

counter = 1;
for ii = 1:length(subjects)
    if ~isempty(Crowdedthresholds(ii).subject) && ~isempty(Uncrowdedthresholds(ii).subject)
        deltaThresh(ii).subject = Crowdedthresholds(ii).subject;
        if length(Crowdedthresholds(ii).thresh) == 7 &&...
                length(Uncrowdedthresholds(ii).thresh) == 7 %%Full data set
            deltaThresh(ii).deltaThresh = Crowdedthresholds(ii).thresh - Uncrowdedthresholds(ii).thresh;
            deltaThresh(ii).uncrowdedValue = Uncrowdedthresholds(ii).thresh;
            deltaThresh(ii).ecc = Crowdedthresholds(ii).ecc;
            meanThreshEachEcc(counter,:) = deltaThresh(ii).deltaThresh;
%         elseif Crowdedthresholds(ii).ecc == Uncrowdedthresholds(ii).ecc
%             for num = 1:7
        end
    end
    counter = counter + 1;
    %     meanThreshAll(ii) = mean(meanThresh.(fieldName));
end

figure;
for ii = 1:length(deltaThresh)
    plot(deltaThresh(ii).ecc,deltaThresh(ii).deltaThresh,'-o');%,'Color',colorAcross(ii,:));
%     hold on
%     plot(deltaThresh(ii).ecc,deltaThresh(ii).uncrowdedValue,'*','Color','r');%,'Color',colorAcross(ii,:));
end

allEcc = [-25 -15 -10 0 10 15 25];
for ii = 1:length(meanThreshEachEcc)
allMeans(ii) = mean(meanThreshEachEcc(:,ii));
end
% for ii = 1:length(meanThreshAll)
%     plot(allEcc(ii),mean(meanThreshAll(:,ii)),'-*','Color','k')
      plot(allEcc,allMeans,'-*','Color','k')
% end
ylim([0 1.5])
ylabel('Crowding Effect')
xticks([-25 -15 -10 0 10 15 25])






