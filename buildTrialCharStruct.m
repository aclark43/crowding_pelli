function dataStruct = buildTrialCharStruct(pptrials)
%BUILDDATASTRUCT Summary of this function goes here
%   Detailed explanation goes here
fprintf('%s: get trial parameters\n', datestr(now));
dataStruct = struct(...
    'FlankerDist', nan(size(pptrials)),...
    'FlankerType', nan(size(pptrials)),...
    'PestLevel', nan(size(pptrials)),...
    'TargetEccentricity', nan(size(pptrials)),...
    'TargetEccentricityIndex', nan(size(pptrials)),...
    'TargetGray', nan(size(pptrials)),...
    'TargetOrientation', nan(size(pptrials)),...
    'TargetStrokewidth', nan(size(pptrials)),...
    'ResponseTime', nan(size(pptrials)),...
    'Correct', nan(size(pptrials)),...
    'Response', nan(size(pptrials)),...
    'TimeFixationON', nan(size(pptrials)),...
    'TimeFixationOFF', nan(size(pptrials)),...
    'TimeTargetOFF', nan(size(pptrials)),...
    'TimeTargetON', nan(size(pptrials)),...
    'TimeCueON', nan(size(pptrials)),...
    'TimeCueOFF', nan(size(pptrials)),...
    'Uncrowded', nan(size(pptrials)),...
    'Unstab', nan(size(pptrials))...
    );
end

%%%%%%'FixationSize', nan(size(pptrials))...