function [calParams] = extractCalibrationParameters(eis_data)

calData = eis_data.eye_data.eye_1.calibration_parameters;

calParams.centers_raw_pix = zeros(2,9);
calParams.centers_pix = zeros(2,9);
calParams.centers_arcmin = zeros(2,9);

for i=1:9 %% 5 is the center
    calParams.centers_pix(:,i) = [calData.calibration_coordinates{i}.x;calData.calibration_coordinates{i}.y];
    calParams.centers_eye_trace(:,i) = [calData.centers{i}.x;calData.centers{i}.y];
    calParams.centers_arcmin(:,i) = [calData.calibration_angles{i}.x;calData.calibration_angles{i}.y];
end

calParams.q1Coeff = zeros(2,4);
calParams.q2Coeff = zeros(2,4);
calParams.q3Coeff = zeros(2,4);
calParams.q4Coeff = zeros(2,4);
for i=1:4
    calParams.q1Coeff(:,i) = [calData.coefficients{i}.x;calData.coefficients{i}.y];
    calParams.q2Coeff(:,i) = [calData.coefficients{4+i}.x;calData.coefficients{4+i}.y];
    calParams.q3Coeff(:,i) = [calData.coefficients{8+i}.x;calData.coefficients{8+i}.y];
    calParams.q4Coeff(:,i) = [calData.coefficients{12+i}.x;calData.coefficients{12+i}.y];
end

end

