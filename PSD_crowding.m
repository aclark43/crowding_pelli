function [critFreq, power, spatial, powerDist] = PSD_crowding(dc, pixelAngle1, widthAngle)
%% power spectra from drift (Brownian motion model)
% dc = 20; % diffusion constant (arcmin^2/s)

f = linspace(1, 80, 80); % temporal frequencies (Hz)
k = logspace(-1, 2, 100); % spatial frequencies (cpd)

ps = Qfunction(f, k, dc / 60^2); % assumes isotropic

powerDist = [];

subplot(2, 2, 1);
pcolor(k, f, pow2db(ps));
colormap 'hot'; shading interp;
set(gca, 'XScale', 'log', 'YScale', 'log');
xlabel('spatial frqeuency (cpd)');
ylabel('temporal frequency (Hz)');
set(gca, 'XTick', [1, 10, 30, 50], 'YTick', [1, 10, 30, 80]);
title('PSD of BM drift');
axis square;
% 

figure; clf;
subplot(2, 2, 2);
plot(k, pow2db(nansum(ps(f >=2 & f <= 40, :), 1)), 'k-', 'linewidth', 2);
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
ylabel('PSD (db)');
title('temporal power in RGC range');
xlabel('spatial frequency (cpd)');
axis square;

 power.All = pow2db(nansum(ps(f >=2 & f <= 40, :), 1));
 spatial.All = k;

[~,I] = max(pow2db(nansum(ps(f >=2 & f <= 40, :), 1)));
critFreq = k(I);

%% stimuli (drawing sanity check)
digits = [3, 5, 6, 9];
vertical = 1;

%% power spectra of uncrowded stimuli
% works best when pixelAngle and widthAngle of the digit result in an even
% number of pixels for the width of the digit
% pixelAngle = .25; % arcmin
% widthAngle = 2; % arcmin (angular width of digit)
% pixelAngle = round(pixelAngle,2);

pixelAngle = round(pixelAngle1/5,2)*5;
widthPixel = round2(widthAngle / pixelAngle, 2); % pixel - even number please!

bufferSizeAngle = 2 * 60; % arcmin
bufferSizePixel = bufferSizeAngle / pixelAngle;

sz = 2 * bufferSizePixel;
k = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);

allSpec = cell(size(digits));

ca = [-40, 20];

kdc = k;
psD = Qfunction(f, kdc, dc / 60^2); % assumes isotropic
Dps1 = mean(psD);

for di = 1:length(digits)
    im = drawDigit(digits(di), widthPixel);
    
    % buffer image and make it square
    im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel], 1, 'both');
    [m, n] = size(im_buffered);
    df = floor((m - sz)/2);
    im_buffered = im_buffered(df:(df + sz - 1), :);
    df = floor((n - sz)/2);
    im_buffered = im_buffered(:, df:(df + sz - 1));
    
    
    %%%%%%%%% contrast reverse the image because fft pads with zeros
    % This may affect phase but does not have a major impact on power
    % except at (0, 0)
    im_buffered = 1 - im_buffered;
    
    spec = fftshift(fft2(im_buffered));
    
    allSpec{di} = struct('PS', spec, 'k', k);
    
    plot_k = [2, 10, 30, 50];
    plot_k_index = interp1(k, 1:length(k), plot_k, 'nearest');
    
end

%% compare across digits
% figure;
cols = lines(length(digits));
% figure(1); clf; % plot across digits
% figure(3); clf; % plot horizontal and vertical slices
for di = 1:length(digits)
    spec = allSpec{di}.PS;
    k = allSpec{di}.k;
    
    power.H(di,:) = pow2db(nanmean(abs(spec(:, k>0)), 1).^2);
    spatial.H(di,:) = k(k > 0);
    
    power.V(di,:) = pow2db(nanmean(abs(spec(k>0, :)), 2).^2);
    spatial.V(di,:) = k(k > 0);
    
    subplot(2, 2, 3); hold on;
    plot(k(k > 0), pow2db(nanmean(abs(spec(:, k>0)), 1).^2), 'b-',...
        'linewidth', 2, 'Color', cols(di, :));
    axis square
    
    subplot(2, 2, 4); hold on;
    plot(k(k > 0), pow2db(nanmean(abs(spec(k>0, :)), 2).^2), 'r-',...
        'linewidth', 2, 'Color', cols(di, :));
    axis square
end

subplot(2, 2, 3);
xlabel('horizontal spatial frequency (cpd)');
ylim([-10, 30]);
axis square;
ylabel('power (db)');
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlim([5, k(end)]);
hold off;
title('uncrowded target');

subplot(2, 2, 4);
xlabel('vertical spatial frequency (cpd)');
ylim([-10, 30]);
legend(cellfun(@num2str, num2cell(digits), 'UniformOutput', false));
axis square;
ylabel('power (db)');
set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
xlim([5, k(end)]);
title('uncrowded target');
hold off;
%% Power of horizontal and vertical Stimulus

close all;
figure;
for di = 1:length(digits)
    im = drawDigit(digits(di), widthPixel);
    
    % buffer image and make it square
    im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel], 1, 'both');
    [m, n] = size(im_buffered);
    df = floor((m - sz)/2);
    im_buffered = im_buffered(df:(df + sz - 1), :);
    df = floor((n - sz)/2);
    im_buffered = im_buffered(:, df:(df + sz - 1));
    
    
    %%%%%%%%% contrast reverse the image because fft pads with zeros
    % This may affect phase but does not have a major impact on power
    % except at (0, 0)
    im_buffered = 1 - im_buffered;
    
    spec = fftshift(fft2(im_buffered));
    
    allSpec{di} = struct('PS', spec, 'k', k);
    
    plot_k = [2, 10, 30, 50];
    plot_k_index = interp1(k, 1:length(k), plot_k, 'nearest');
    

    subplot(2, 2, di);
    imagesc(k, k, pow2db(abs(spec).^2));
    title(sprintf('PSD of %i', digits(di)));
    axis image; 
    xlabel('horizontal spatial frequency (cpd)');
    ylabel('vertical spatial frequency (cpd)');
    colormap hot; colorbar;
    caxis(ca);
%     
    powerDist.k {di}= k;
    powerDist.pow2db{di} = pow2db(abs(spec).^2);
    powerDist.vertAverage{di} = pow2db(nanmean(abs(spec), 2).^2);
    powerDist.horizAverage{di} = pow2db(nanmean(abs(spec), 1).^2);
    
end

function digit = drawDigit(whichDigit, pixelwidth)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
% end

end