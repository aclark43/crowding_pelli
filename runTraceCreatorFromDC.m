%% runTraceCreator

% D: standard diffusion coefficient (the double of Kuang et al, 2012)
%   fc:  sampling frequency (Hertz)
%   N:  number of simulated samples in the eye movement trace
%
%   Xe, Ye:  eye movement sequence
% this code was taken from
% : http://people.sc.fsu.edu/~jburkardt/m_src/brownian_motion_simulation/brownian_motion_simulation.m

% Bias = [Angle Amplitude StDeviation];


fc = 1000;
N = 500; %(500ms)
Bias = [90 10 1];

figure;
[Xe{1,1}, Ye{1,1}] = EM_Brownian(5,  fc, N, 1);
condition(1) = {'10 DC, No Bias'};

[Xe{1,1}, Ye{1,1}] = EM_Brownian(10,  fc, N, 1);
condition(1) = {'10 DC, No Bias'};

[Xe{1,2}, Ye{1,2}] = EM_Brownian(10,  fc, N, 1, Bias);
condition(2) = {'10 DC, 90deg 10arc Bias'};

[Xe{1,3}, Ye{1,3}] = EM_Brownian(20, fc, N, 1);
condition(3) = {'20 DC, No Bias'};

[Xe{4}, Ye{4}] = EM_Brownian(20, fc, N, 1, Bias);
condition(4) = {'20 DC, 90deg 10arc Bias'};

for ii = 1:2%length(Xe)
    leg(ii) = plot(Xe{ii},Ye{ii});
    hold on
end
axis([-10 10 -10 10])
legend(leg,condition);


for numTrials = 1:100
    [xNoBias{numTrials}, ynoBias{numTrials}] = EM_Brownian(10,  fc, N, 0);
    [x10Bias{numTrials}, y10Bias{numTrials}] = EM_Brownian(20, fc, N, 1, Bias);
end

[~,dataStruct.Bias,dataStruct.dCoefDsq, ~, dataStruct.Dsq, ...
    dataStruct.SingleSegmentDsq,~,~] = ...
    CalculateDiffusionCoef(fc, struct('x',xNoBias, 'y', ynoBias));

 [biasMeasure, angleRad] = ...
            calculateBiasSingleValue(dataStruct.Bias);

[~,dataStructBias.Bias,dataStructBias.dCoefDsq, ~, dataStructBias.Dsq, ...
    dataStructBias.SingleSegmentDsq,~,~] = ...
    CalculateDiffusionCoef(fc, struct('x',x10Bias, 'y', y10Bias));

[biasMeasure, angleRad] = ...
            calculateBiasSingleValue(dataStructBias.Bias);
        
        curvature = mean(cur(Xe{1},Ye{1}));

        curvature(2) = mean(cur(Xe{2},Ye{2}));
        
        
        r = 1 ;
        y = r*sin(0:.1:2*(pi));
        x = r*cos(0:.1:2*(pi));
        
 
 
 
 
tmpCur = abs(cur(smx(cutseg:end-cutseg), smy(cutseg:end-cutseg)));
mn_cur(1) = mean(tmpCur(tmpCur < 300));

r = 5  ; 
y = r*sin(0:.1:2*(pi));        
 x = r*cos(0:.1:2*(pi));         
tmpCur = abs(cur(smx(cutseg:end-cutseg), smy(cutseg:end-cutseg)));
mn_cur(2) = mean(tmpCur(tmpCur < 300));

