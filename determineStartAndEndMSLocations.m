function em = determineStartAndEndMSLocations(fixWithMS, eccentricity, pptrials,strokeWidth,params,FIXATION_ANALYSIS, em)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
for jj = 1:length(fixWithMS)%ii = msIdx(1,:)
    ii = fixWithMS(jj);
    
    if ~isempty(pptrials{ii}.microsaccades.start)
        [timeOn, timeOff] = timeConvertTracker(params, pptrials, ii, FIXATION_ANALYSIS);
        if timeOff > length(pptrials{ii}.x.position)
            continue
        else
            idx = ~isnan(pptrials{ii}.microsaccades.start);
            if strcmp('D',params.machine)
                microSaccadeStart = ceil(pptrials{ii}.microsaccades.start(idx)/(1000/330));
                microSaccadeEnd = floor(pptrials{ii}.microsaccades.duration(idx)/(1000/330) + microSaccadeStart - 1);
            else
                microSaccadeStart = ceil(pptrials{ii}.microsaccades.start(idx));
                microSaccadeEnd = floor(pptrials{ii}.microsaccades.duration(idx) + microSaccadeStart - 1);
            end
             
            em.ms.(eccentricity).(strokeWidth)(jj).id = ii;
            em.ms.(eccentricity).(strokeWidth)(jj).sw = (strokeWidth);
            em.ms.(eccentricity).(strokeWidth)(jj).msStartTime = pptrials{ii}.microsaccades.start;
            em.ms.(eccentricity).(strokeWidth)(jj).msEndTime = pptrials{ii}.microsaccades.start +...
                pptrials{ii}.microsaccades.duration;
            xMS = pptrials{ii}.x.position(microSaccadeStart:microSaccadeEnd) + pptrials{ii}.xoffset * pptrials{ii}.pixelAngle; % in arcmin
            yMS = pptrials{ii}.y.position(microSaccadeStart:microSaccadeEnd) + pptrials{ii}.yoffset * pptrials{ii}.pixelAngle;
            
            angleMS = pptrials{ii}.microsaccades.angle;
            em.ms.(eccentricity).(strokeWidth)(jj).MSangle = angleMS;
            
            amplitudeMS = pptrials{ii}.microsaccades.amplitude;
%             if microSaccadeStart < timeOn %partially included ms
%                 xMS = pptrials{ii}.x.position(microSaccadeStart:microSaccadeEnd) + pptrials{ii}.xoffset * params.pixelAngle; % in arcmin
%                 yMS = pptrials{ii}.y.position(microSaccadeStart:microSaccadeEnd) + pptrials{ii}.yoffset * params.pixelAngle;
%                 
%                 em.ms.(eccentricity).(strokeWidth)(jj).partialStartLocationMSx = xMS(1);
%                 em.ms.(eccentricity).(strokeWidth)(jj).partialStartLocationMSy = yMS(1);
%                 em.ms.(eccentricity).(strokeWidth)(jj).partialEndLocationMSx = xMS(end);
%                 em.ms.(eccentricity).(strokeWidth)(jj).partialEndLocationMSy = yMS(end);
%             else
                em.ms.(eccentricity).(strokeWidth)(jj).MSamplitude = amplitudeMS;
                em.ms.(eccentricity).(strokeWidth)(jj).startLocationMSx = xMS(1);
                em.ms.(eccentricity).(strokeWidth)(jj).startLocationMSy = yMS(1);
                em.ms.(eccentricity).(strokeWidth)(jj).endLocationMSx = xMS(end);
                em.ms.(eccentricity).(strokeWidth)(jj).endLocationMSy = yMS(end);
                
%                 em.ms.(eccentricity).(strokeWidth)(jj).partialStartLocationMSx = {};
%                 em.ms.(eccentricity).(strokeWidth)(jj).partialStartLocationMSy = {};
%                 em.ms.(eccentricity).(strokeWidth)(jj).partialEndLocationMSx = {};
%                 em.ms.(eccentricity).(strokeWidth)(jj).partialEndLocationMSy = {};
%             end
            
        end
    end
end

end

