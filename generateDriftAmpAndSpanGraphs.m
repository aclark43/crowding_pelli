function generateDriftAmpAndSpanGraphs( meanDriftAmplitude,  meanDriftSpan, stdDriftAmplitude, stdDriftSpan, title_str)
%UNTITLED7 Summary of this function goes here  meanDriftAmplitude,  meanDriftSpan, stdDriftAmplitude, stdDriftSpan
%   Detailed explanation goes here

                validSW = generateDriftDiffusion(em.ecc_0, trialChar, title_str);
                
                close all
                % % % UNCOMMENT ONCE WE ANALYZE DIFFERENT ECC
                % generateMeanStd(meanDriftAmplitude, stdDriftAmplitude, title_str)
                % %x axis is SW (will be ecc), y is amplitude)
                % generateMeanStd(meanDriftSpan, stdDriftSpan, title_str)
                % %x axis is SW (will be ecc), y is span)
                
                %%%Graphs Amplitude and Span Means
                % b = regexp(validSW,'\d+(\.)?(\d+)?','match');
                % validSW = str2double([b{:}]);
                % uSW = intersect(find(validSW), uSW);
                
                generateMeanStd([meanDriftAmplitude; meanDriftSpan], ...
                    [stdDriftAmplitude; stdDriftSpan], title_str)
                
                colormap parula
                set(gca, 'XTickLabel',{'Amplitude','Span'})
                % xlim([0 16])
                
                C = regexp(sprintf('uSW=%d#', uSW), '#', 'split');
%                 C(end) = [];
                legend(validSW,'Location','Northeast','Interpreter','none');
                
                xlabel('Drift Characteristics')
                ylabel('Amplitude')
                graphTitle = sprintf('Amplitude&Span_for_%s', title_str);
                title(graphTitle,'Interpreter','none');
                
                saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/Amplitude&Span%s%s.epsc', trialChar.Subject, graphTitle, title_str));
                saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/JPEG/Span&Amplitude/%s%s.jpeg', trialChar.Subject, graphTitle, title_str));
                saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/FIG/Span&Amplitude/%s%s.fig', trialChar.Subject, graphTitle, title_str));
                
                
                close all

end

