%%areaCovered

crowded = [129.18   62.634  224.467 162.302 431.197 165.049];
uncrowded = [124.82 59.760 140.04 138.01 432.0 137.170];

m = mean(uncrowded)

std(uncrowded)
[~,p1] = ttest(crowded,uncrowded);
std(crowded)
std(uncrowded)

coeffs = polyfit(crowded, uncrowded, 1);
% Get fitted values
fittedX = linspace(min(crowded), max(uncrowded), 200);
fittedY = polyval(coeffs, fittedX);

mdl = fitlm(crowded,uncrowded);
%[h,p,ci,stats] = ttest2(crowded,uncrowded)
p = mdl.Coefficients.pValue(2);

%%%%linregression

figure
scatter(ones(1,6),uncrowded,200,'filled')
hold on 
twos = [2 2 2 2 2 2];
scatter(twos,crowded,200,'filled')
axis([.75 2.25 0 500])

hold on
averageU = sum(uncrowded)/6;
averageC = sum(crowded)/6;
plot([1 2],[averageU averageC]);

names = {'Uncrowded','Crowded'};
set(gca,'xtick',[1 2],'xticklabel', names)

ylabel('Area')
pValue = sprintf('p = %.3f', p);
text(1.5,250,pValue);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Drift per subject for each SW (from Excel)
SW = [	1.516	2.021	2.526]
Drift = [	3.919	3.881	3.784]

SW=[0.7579	1.516	2.021	2.779	3.537]
Drift = [2.233	2.487	2.38	2.256	2.25]


[~,p]=ttest(SW,Drift)


SW = [0.5053	1.011	1.516	2.021]
Drift = [2.316	2.129	2.074	2.175]
figure
LinRegression(SW,Drift,0,NaN,1,0)% SW = [0.7579	1.516	2.021	2.779	3.537];
% Drift = [2.233	2.487	2.38	2.256	2.25];



figure
[~,p] = LinRegression(SW,Drift,0,NaN,1,0);