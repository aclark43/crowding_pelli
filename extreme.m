function [ output ] = extreme( V )
% Find most extreme value, whether the most negative or positive.

[~,X] = max(abs(V));
output = V(X);
end

