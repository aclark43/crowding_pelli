% function [t p b t_1 p_1 b_1] = LinRegression(X,Y,RemoveOutlayers)
%
% LinRegression computes removes the outlayers with the Tau-Thompson method in the
% dependent variable dataset and compute the linear regression for X and Y
%
% INPUT : X - independent variable data points
%         Y - dependent variable data points
%         RemoveOutlayers - 1 to apply the outlayer removal based on the
%         Tau-Thompson method. If 0 1 outlayers can still be removed with
%         the standard linear regression procedure used in the script
%
% OUTPUT : t - t value associated with the linear regression coefficient
%          p - p value associated with the linear regression coefficient
%          b - b linear regression coefficient (i.e., slope of the fitted
%              line)
%          t_1, p_1 and b_1 - t, p and b values of the linear regression
%          recalculared omitting the residual outlayers

% from - "statistical methods"  by Snedecor & Cochran. Sixth Edition.
% Chapter 6 pg. 153


function [t p b R] = LinRegression(X,Y,RemoveOutlayers,T, FIGURE_ON, angles)

t_1 = [];
p_1 = [];
b_1 = [];

% Eliminate data points that are above the 95 percentile. This helps to
% generate a better estimate of the correlation coefficient (this is not in
% the book, it is just a personal addition).

if RemoveOutlayers
    [idx1] = find_outliers_Thompson(Y, T);
    %[idx2] = find_outliers_Thompson(X, .0001);%.001
    [idx] = setxor([idx1'],[1:length(Y)]);
    X = X(idx);
    Y = Y(idx);
end

if angles
% check for spurious correlations (circular correlation)
[Cc Pc] = circ_corrcc(circ_ang2rad(X),circ_ang2rad(Y));
b1 = (Cc*std(Y))/std(X);
end

% degrees of freedom
N = length(X);
df = N-2;
x = X-mean(X);
y = Y-mean(Y);
% coefficitent estimate
b = sum(x.*y)/sum(x.^2);
Y_a = mean(Y) + b.*x;
D_yx = Y - Y_a;
S_yx2 = sum(D_yx.^2)/df;
% standard deviation of the coefficient estimate
S_b = sqrt(S_yx2/sum(x.^2));
% t stat for the null hypothesis that b=beta, where beta=0
%(absence of correlation between the two variables).
t = b/S_b;
% p value for a two tail test
p = 2*tcdf(-abs(t), df);
% 95% confidence intervals
S_yx = sqrt(S_yx2);
S_a = S_yx*sqrt((1/N)+(x.^2./sum(x.^2)));
t_95 = tinv(.05/2,df);
delta = t_95*S_a;

[R P] = corrcoef(X,Y);
R = R(2);
P = P(2);

if FIGURE_ON
%figure
scatter(X,Y, 'k', 'MarkerFaceColor', 'k')
hold on
[p1 S] = polyfit(X,Y,1);
plot(linspace(min([X Y]),max([X Y]),10), polyval(p1,linspace(min([X Y]),max([X Y]),10)),'k', 'LineWidth', 2)
[p1 S] = polyfit(X,Y+delta,1);
[X_1 idx]= sort(X);
plot(X(idx), Y_a(idx)+delta(idx), '-r')
plot(X(idx), Y_a(idx)-delta(idx), '-r')
axis([min([X Y]) max([X Y])+5 min([X Y])-5 max([X Y])+5])


% scatter(X,Y, 'k', 'MarkerFaceColor', 'k')
% hold on
% [p1 S] = polyfit(X,Y,1);
% plot(linspace(min([X Y]),max([X Y]),10), polyval(p1,linspace(min([X Y]),max([X Y]),10)),'k', 'LineWidth', 2)
% [p1 S] = polyfit(X,Y+delta,1);
% [X_1 idx]= sort(X);
% 
% curve1 = Y_a(idx)+delta(idx);
% curve2 = Y_a(idx)-delta(idx);
% x2 = [X(idx), fliplr(X(idx))];
% inBetween = [curve1, fliplr(curve2)];
% h(1) = fill(x2, inBetween, [0 0 1]);
% scatter(X,Y, 'k', 'MarkerFaceColor', 'k')

% plot(X(idx), Y_a(idx)+delta(idx), '-r')
% plot(X(idx), Y_a(idx)-delta(idx), '-r')
axis([min([X Y]) max([X Y])+5 min([X Y])-5 max([X Y])+5])

if angles
    title(sprintf('r=%.2f c=%.2f p=%.2f b=%.2f pc=%.2f b1=%.2f', R,Cc, P, b,Pc, b1), 'FontSize',15)
else
    title(sprintf('r=%.2f p=%.2f b=%.2f', R, P, b), 'FontSize',15)
end
end

% % testing a deviation that looks suspicius (from p. 157)
% % identify the suspicius value
% [M idx] = max(abs(D_yx));
% M_x = X(idx);
% M_y = Y(idx);
% idx = find(([1:N])~=idx);
% X_1 = X(idx);
% Y_1 = Y(idx);
% N_1 = length(X_1);
% df_1 = N_1-2;
% x_1 = X_1-mean(X_1);
% y_1 = Y_1-mean(Y_1);
% % recompute the regression with the omitted point
% b_1 = sum(x_1.*y_1)/sum(x_1.^2);
% Y_a_1 = mean(Y_1) + b_1.*x_1;
% D_yx_1 = Y_1 - Y_a_1;
% S_yx2_1 = sum(D_yx_1.^2)/df;
% x_s_1 = M_x-mean(X_1);
% y_as_1 = mean(Y_1)+b_1.*x_s_1;
% 
% % % test whether the deviation of the omitted point from the regression line
% % % is within the sampling error
% % Dev = M_y-y_as_1;
% % Sy_y2 = S_yx2*(1+(1/(N-1))+((x_s_1^2)/sum(x.^2)));
% % t_dev = (M_y-y_as_1)/sqrt(Sy_y2);
% % p_dev = (2*tcdf(-abs(t_dev), df))*length(X);


% 
% fprintf('r=%.2f r^2= %.2f p=%.2f \n',R, R^2, P)
% fprintf('b=%.2f p=%.2f \n', b, p)



