function [idx, fixation] =  rebuildFixationData(fix, params)

for ii = 1:params.subNum
    fixation.First200.dsq(ii) = fix.dCoefDsq.(params.subjectsAll{ii});
    if ~isnan(fixation.First200.dsq(ii))
        fixation.First200.dsqSingleSegs{ii} = fix.dsqSingleSegs.(params.subjectsAll{ii});
%         fixation.First200.area{ii} = fix.area.(params.subjectsAll{ii});
        fixation.First200.dsqTime{ii} = fix.dsqTime.(params.subjectsAll{ii});
        fixation.First200.dsqFit{ii} = fix.regfit.(params.subjectsAll{ii});
        fixation.First200.nFix{ii} = fix.Nfix.(params.subjectsAll{ii});       
        fixation.First200.bootDSQ{ii} = fix.bootDSQ.(params.subjectsAll{ii}).info;   
        fixation.First200.bootDSQci{ii} = fix.bootDSQ.(params.subjectsAll{ii}).ci;   
    else
        fixation.First200.dsqSingleSegs{ii} = NaN;
        fixation.First200.dsqTime{ii} = NaN;
        fixation.First200.dsqFit{ii} = NaN;
        fixation.First200.nFix{ii} = NaN;   
    end
    fixation.First200.dsqVec{ii} = fix.dCoefVec.(params.subjectsAll{ii});
    fixation.First200.span(ii) = fix.span.(params.subjectsAll{ii});
    fixation.First200.ind_span{ii} = fix.ind_span.(params.subjectsAll{ii});
    fixation.First200.curve(ii) =  fix.curvature.(params.subjectsAll{ii});
    fixation.First200.ind_curve{ii} =  fix.ind_curvature.(params.subjectsAll{ii});
    fixation.First200.prlDrift(ii) =  fix.prlDrift.(params.subjectsAll{ii});
    fixation.First200.speed(ii) =  fix.mn_speed.(params.subjectsAll{ii});
    fixation.First200.ind_speed{ii} =  fix.ind_speed.(params.subjectsAll{ii});
end
idx = ~isnan(fixation.First200.dsq);
end