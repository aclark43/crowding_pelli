%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

close all
clc

%%Figure Location & Name%%
% subject = {'Ashley'};
% condition1 = {'Uncrowded', 'Crowded', 'Crowded', 'Uncrowded'}; %Figure 1 and Figure 2
% condition2 = {'Crowded', 'Uncrowded', 'Crowded', 'Uncrowded'}; %Figure 1 and Figure 2
% stabilization1 = {'Stabilized', 'Unstabilized', 'Unstabilized', 'Stabilized'};
% stabilization2 = {'Unstabilized','Stabilized', 'Unstabilized', 'Stabilized'};
% em1 = {'Drift', 'MS', 'Drift_&_MS','Drift', 'MS', 'Drift_&_MS'};
% em2 = {'MS', 'Drift_&_MS', 'Drift', 'MS', 'Drift_&_MS', 'Drift'};
% ecc = {'0ecc'};

subject = {'Z063','Z013','Ashley','Z023','Z005','Z002'};
condition1 = {'Uncrowded'}; %Figure 1 and Figure 2
condition2 = {'Uncrowded'}; %Figure 1 and Figure 2
stabilization1 = {'Unstabilized'};
stabilization2 = {'Unstabilized'};
em1 = {'Drift'};
em2 = {'Drift'};
ecc1 = {'0ecc'};
ecc2 = {'0ecc'};
%  em3 = {'Drift_MS'};%%%%%%%%%%%%%%%%%
%  ecc3 = {'25ecc'};%%%%%%%%%%%%%%%%%%
c = cool(2);
                                        
                                       

for condIdx1 = 1:length(condition1)
    currentCond1 = condition1{condIdx1};
    for subIdx = 1:length(subject)
        currentSubj = subject{subIdx};
        for condIdx2 = 1:length(condition2)
            currentCond2 = condition2{condIdx2};
            for stabIdx1 = 1:length(stabilization1)
                currentStab1 = stabilization1{stabIdx1};
                for stabIdx2 = 1:length(stabilization2)
                    currentStab2 = stabilization2{stabIdx2};
                    for emIdx1 = 1:length(em1)
                        currentEm1 = em1{emIdx1};
                        for emIdx2 = 1:length(em2)
                            currentEm2 = em2{emIdx2};
                            
                            for eccIdx1 = 1:length(ecc1)
                                currentEcc1 = ecc1{eccIdx1};
                                for eccIdx2 = 1:length(ecc2)
                                    currentEcc2 = ecc2{eccIdx2};
                                    
                                    
                                    title2 = sprintf('%s_%s_%s_%s_%s', currentSubj, currentCond1, currentStab1, currentEm1, currentEcc1); %Name of File FIG 1
                                    title1 = sprintf('%s_%s_%s_%s_%s', currentSubj, currentCond2, currentStab2, currentEm2, currentEcc2); %Name of File FIG 2
                                    
                                    
%                                     pathToData = sprintf('../../Data/%s/Graphs/FIG', subject{subIdx}); %File Location
                                    pathToData = ('../../Data/AllSubjTogether/Span/AllSpan/'); %File Location

                                    
%                                     if strcmp(title1,title2) == 1
%                                         return
%                                     end
                                    
                                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                    color1 = c(1,:);
                                    title1 = sprintf('LargeSpan%s',title1);
                                    fH1 = createGraphTogetherMultipleSubj(title1,pathToData,currentCond1,currentStab1,currentEm1,currentEcc1,color1);
%                                     fH1 = createGraphTogetherMultipleSubjWithText...
%                                                         (title1,pathToData,currentCond1,currentStab1,currentEm1,currentEcc1,color1,0.3);
%                                     text(.5,9,'Large Span','Color',color1);
                                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                    %% FIGURE 2 %%
                                    %%%%%%%%%%%%%%
                                    
                                    color2 = c(2,:);
                                    title2 = sprintf('SmallSpan%s',title2);
                                    fH2 = createGraphTogetherMultipleSubj(title2,pathToData,currentCond2,currentStab2,currentEm2,currentEcc2,color2);
%                                     fH2 = createGraphTogetherMultipleSubjWithText...
%                                                         (title2,pathToData,currentCond2,currentStab2,currentEm2,currentEcc2,color2,0.1);
                                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                    
                                    
                                    %% Figures Together%%
                                    title_str = sprintf('%s', subject{1});
                                    title(title_str);
                                    set(gca, 'FontSize', 11, 'FontWeight', 'bold');
                                    
                                    ax1 = get(fH1, 'Children');
                                    ax2 = get(fH2, 'Children');
                                    ax2p = get(ax2(1),'Children');
                                    copyobj(ax2p, ax1(1));
                                    
                                    title_str = sprintf('%s', currentSubj);
                                     title(title_str);
%                                     title('RBG');
                                    set(gca, 'FontSize', 20, 'FontWeight', 'bold');
%                                     text(0.5,.9,'Large Span',color1)
                                    
                                    close Figure 2
                                   text(.5,.9,'Large Span','Color',color1);
%                                     saveas(gcf, sprintf('../../Data/%s/Graphs/FIG/%s.fig', currentSubj, sprintf('%s_VS_%s',title1,title2)));
%                                     saveas(gcf, sprintf('../../Data/%s/Graphs/JPEG/%s.png', currentSubj, sprintf('%s_VS_%s',title1,title2)));
%                                     saveas(gcf, sprintf('../../Data/%s/Graphs/%s.epsc', currentSubj, sprintf('%s_VS_%s',title1,title2)));
%                                     saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Desktop/%s.fig', sprintf('%s_VS_%s',title1,title2)));
%                                     saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Desktop/%s.png', sprintf('%s_VS_%s',title1,title2)));
                                    saveas(gcf, sprintf('../../Data/AllSubjTogether/Span/%s.png', sprintf('%s_VS_%s',title1,title2)));
                                    
                                    close all
                                    
                                    
                                    
                                    
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

