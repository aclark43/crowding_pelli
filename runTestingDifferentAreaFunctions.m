% runTestingDifferentAreaFunctions


fc = 1000;
N = 500; %(500ms)
Bias = [90 10  4 1];
dc = [5 10 20 30];
for ii = 1:length(dc)
    [x, y] = EM_Brownian(5, fc, N, 1, Bias(ii));
    
    figure;
    plot(x,y);
    [tempC] = ellipseXY(x, y, 68, [150 150 150]/255,0);
    title(sprintf('D = %i',dc(ii)))
    x1 = [tempC.Position(1) tempC.Position(1) ...
        tempC.Position(1)+tempC.Position(3) tempC.Position(1)+tempC.Position(3)];
    
    y1= [tempC.Position(2)+tempC.Position(4) tempC.Position(2)  ...
        tempC.Position(2) tempC.Position(2)+tempC.Position(4)];
    
    area_poly =  polyarea(x1,y1);
    area = CalculateTheArea(x,y,...
        0.68,20,40);
    
    c = corrcoef(x',y');
    c = c(2,1);
    Area_MP = 2*.68*pi*std(x)*std(y)*sqrt(1-c^2);
    
    
    tableData(ii).dc = dc(ii);
    tableData(ii).polyArea = area_poly;
    tableData(ii).area = area;
    tableData(ii).area_MP = Area_MP;

end
struct2table(tableData)