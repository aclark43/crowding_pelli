function leg = plotCombineTime(subjectsAll,subNum,var,stdVar,c)
for ii = 1:subNum
    yneg = [0];
    distVal = var(ii);
    distSTD = stdVar(ii);
    if strcmp('HUX4',subjectsAll{ii}) || ...
            strcmp('AshleyDDPI',subjectsAll{ii}) || ...
            strcmp('HUX18',subjectsAll{ii}) || ...
            strcmp('HUX10',subjectsAll{ii}) || ...
            strcmp('HUX12',subjectsAll{ii})
        errorbar(ii, distVal, yneg, distSTD, 'LineStyle','none','Color','k');
        hold on
%         leg(ii) =  scatter(ii,distVal,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
        leg(ii) =  scatter(ii,distVal,200,[0 0 0],'filled');
        text(ii-.3,-2,sprintf('%.2f',distVal));
    else
        errorbar(ii, distVal, yneg, distSTD, 'LineStyle','none','Color','k');
        hold on
        leg(ii) = scatter(ii,distVal,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
        text(ii-.3,-2,sprintf('%.3f',distVal));
    end
    hold on
end


end