function [pptrials, filepath] = checkDriftsForMS(drifts, trialId, pptrials, params, filepath)

filepath = sprintf('%s/%s',filepath, params.session);
%test
figure('position',[200, 100, 1500, 800])
allDriftPlot = subplot(2, 2, 1);
plot(drifts')
xlim([5, 260])
box off; axis square
xlabel('Time interval [ms]','FontWeight','bold')
ylabel('dsq','FontWeight','bold')
title('All drifts')
allDriftPlot.Position(1) = 0; % repositions the subplot on the x axis

PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    yLimit = input('What is the y limit: ');
    for driftIdx = 1:size(drifts, 1)
        indvDriftPlot = subplot(2, 2, 3);
        hold on
        currentTrialId = trialId(driftIdx);
        validDriftSegment = find(~isnan(drifts(driftIdx,:)) == 1); % finds the last non-nan
        indvCoef = drifts(driftIdx, validDriftSegment(end));
        plot(drifts(driftIdx,:)')
        title({sprintf('Trial: %i', currentTrialId); sprintf('Coef: %.2f', indvCoef)})
        xlabel('Time interval [ms]','FontWeight','bold')
        ylabel('dsq','FontWeight','bold')
        axis([0 260 0 yLimit])
        box off
        axis square
        indvDriftPlot.Position(1) = 0;
        
        trialPlot = subplot(2,2,2);
        trialPlot.Position = [0.35, 0.13, 0.6, 0.8];
        hold off % Turn hold off so that the individual traces don't plot on top of one another
        
        % Period of interest start and stop
          if strcmp('D',params.machine)
              poiStart = round(pptrials{currentTrialId}.TimeTargetON/(1000/330));
              poiEnd = floor(min(pptrials{currentTrialId}.TimeTargetOFF/(1000/330), pptrials{currentTrialId}.ResponseTime/(1000/330)));
          else
            poiStart = pptrials{currentTrialId}.TimeTargetON;
            poiEnd = min(pptrials{currentTrialId}.TimeTargetOFF, pptrials{currentTrialId}.ResponseTime);
            
          end
        % Creates a color block (green) which highlights the period of
        % interest
        
%         poi = fill([poiStart, poiStart ...
%             poiEnd, poiEnd], ...
%             [-50, 50, 50, -50], ...
%             'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
%         
        xTrace = pptrials{currentTrialId}.x.position(poiStart:poiEnd) + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pxAngle;
        yTrace = pptrials{currentTrialId}.y.position(poiStart:poiEnd) + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pxAngle;
        hold on
        hx = plot(xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
        hy = plot(yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
%         axis([poiStart - 400, poiEnd + 400, -35, 35])
%         if strcmp('D',params.machine)
%         xAxis = [poiStart:20:poiEnd];
         axis([0, length(poiStart:poiEnd), -35, 35])
%         set(gca,'XTickLabel',xAxis);
%         else
%             axis([0, 3000, -35, 35])
%         end
        title('Trial Event Sequence')
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        set(gca, 'FontSize', 12)
%         legend([poi, hx, hy], {'P.O.I','X','Y'},'FontWeight','bold')
        timeTrial = sprintf('Time of Target Presentation is %.1f ms', pptrials{currentTrialId}.TimeTargetOFF-pptrials{currentTrialId}.TimeTargetON);
        legend([hx, hy], {'X','Y'},'FontWeight','bold')
        text(20,-25,timeTrial);
        
        cont = input('Stop (s), MS in the drift(m), saccade in the drift(c), or toss (d): ','s');
        if cont == 's'
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
            break
        elseif cont == 'm'
            msStartTime = input('What is the MS start time?');
            pptrials{currentTrialId}.microsaccades.start = msStartTime+poiStart;
            msDuration = msStartTime + 20;%= input('What is the end time of the MS? ');
            pptrials{currentTrialId}.microsaccades.duration = (msDuration - msStartTime)+poiStart;
            fprintf('Saving pptrials\n')
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
            continue;
        elseif cont == 'd'
            invalidStartTime = input('What is the bad eye movement start time? ');
            pptrials{currentTrialId}.invalid.start = invalidStartTime+poiStart;
            fprintf('Saving pptrials\n')
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
            continue;
        elseif cont == 'c'
            sStartTime = input('What is the Saccade start time?');
            pptrials{currentTrialId}.saccades.start = sStartTime+poiStart;
            sDuration = input('What is the end time of the MS? ');
            pptrials{currentTrialId}.saccades.duration = (sDuration - sStartTime)+poiStart;
            fprintf('Saving pptrials\n')
            save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
            continue;
        end
        
        %save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
        %fprintf('Finished - Saving pptrials. \n')
        
    end
    
    
end

close

end
