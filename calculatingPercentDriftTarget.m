function [percentOnTarget] = calculatingPercentDriftTarget...
    (thresholdSWVals, subNum, subjectUnc, subjectCro, subjectThreshUnc, ...
    subjectThreshCro, subjectsAll, c, fig)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
condition = {'Uncrowded'};
[ percentOnTargUncr, infoOnUncrTarg, ~] = ...
    distanceFromTarget(tempDataStruct.subjectUnc, tempDataStruct.subjectThreshUnc,...
    subjectsAll, condition, c, fig, tempDataStruct.thresholdSWVals);

for ii = 1:subNum
    tableInfo(ii).threshold = tempDataStruct.thresholdSWVals(ii).thresholdSWUncrowded;
    tableInfo(ii).percentOnThresh = str2double(percentOnTargUncr.percent(ii).(tableInfo(ii).threshold));
    B = cell2mat(regexp( tableInfo(ii).threshold,'\d*','Match'));
    tableInfo(ii).percentPlusOne = str2double(percentOnTargUncr.percent(ii).(...
        sprintf('strokeWidth_%i',str2double(B)+1)));
end

condition = {'Crowded'};
[percentOnTargCro,infoOnCroTarg, ~] = ...
    distanceFromTarget(subjectCro, subjectThreshCro, subjectsAll, condition, c, fig, thresholdSWVals);

percentOnTarget.Uncrowded = percentOnTargUncr;
percentOnTarget.UncInfo = infoOnUncrTarg;
percentOnTarget.Crowded = percentOnTargCro; 
percentOnTarget.CroInfo = infoOnCroTarg;

%% Looking at trials with <50% and >50% peformance for Threshold Only%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
for ii = 1:subNum
    less50(ii) = infoOnUncrTarg(ii).perCorLess;
    more50(ii) = infoOnUncrTarg(ii).perCorMore;
    scatter(0,less50(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on
    scatter(1,more50(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on
    y1 = [less50(ii) more50(ii)];
    points = line([0 1], y1, 'Color',c(ii,:),'LineWidth',2);
    points.LineStyle = '--';
end
scatter(0,mean(less50),200,'k','filled')
scatter(1,mean(more50),200,'k','filled')
hold on
y1 = [mean(less50) mean(more50)];
points = line([0 1], y1, 'Color','k','LineWidth',2);

[h,p,ci,stats] = ttest(less50, more50);
ttestP = sprintf('(paired) p = %.3f', p);
text(0, .73, ttestP,'Fontsize',20);
% title('Uncrowded')
yMean = [mean(less50) mean(more50)];
ySTD = [std(less50) std(more50)];
ySEM = ySTD/(sqrt(subNum)); %Standard Error Of The Mean At Each Value Of �x�

CI95 = tinv([0.025 0.975], subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
yCI95 = bsxfun(@times, ySEM, CI95(:)); % Calculate 95% Confidence Intervals Of All Experiments At Each Value Of �x�

errorbar(0,yMean(1),yCI95(1),yCI95(1),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
errorbar(1,yMean(2),yCI95(2),yCI95(2),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)



clear less50
clear more50
for ii = 1:subNum
    less50(ii) = infoOnCroTarg(ii).perCorLess;
    more50(ii) = infoOnCroTarg(ii).perCorMore;
    scatter(2,infoOnCroTarg(ii).perCorLess,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on
    scatter(3,infoOnCroTarg(ii).perCorMore,200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on
    y1 = [infoOnCroTarg(ii).perCorLess infoOnCroTarg(ii).perCorMore];
    points = line([2 3], y1, 'Color',c(ii,:),'LineWidth',2);
    points.LineStyle = '--';
end
scatter(2,mean(less50),200,'k','filled')
scatter(3,mean(more50),200,'k','filled')
hold on
y1 = [mean(less50) mean(more50)];
points = line([2 3], y1, 'Color','k','LineWidth',2);

yMean = [mean(less50) mean(more50)];
ySTD = [std(less50) std(more50)];
ySEM = ySTD/(sqrt(subNum)); %Standard Error Of The Mean At Each Value Of �x�

CI95 = tinv([0.025 0.975], subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
yCI95 = bsxfun(@times, ySEM, CI95(:)); % Calculate 95% Confidence Intervals Of All Experiments At Each Value Of �x�

errorbar(2,yMean(1),yCI95(1),yCI95(1),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
errorbar(3,yMean(2),yCI95(2),yCI95(2),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)



[h,p,ci,stats] = ttest(less50, more50);
ttestP = sprintf('(paired) p = %.3f', p);
text(1.5, .73, ttestP,'Fontsize',20);

names = {'U <50%','U >50%','C <50%','C >50%'};
set(gca,'xtick',[0 1 2 3],'xticklabel', names,'FontSize',15,...
    'FontWeight','bold')
ylabel('Performance')
title('Percent of Time on Target by Performance')
% title('Crowded')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/LessMore50OnTarg.png');
%% Threshold SW
figure
clear uncr
clear cro
uncr = double.empty(subNum,0);
cro = double.empty(subNum,0);

for ii = 1:subNum
%     swColumnU = sprintf('strokeWidth_%i', swThreshUncr(ii));
    uncr{ii} = percentOnTargUncr.percent(ii).(thresholdSWVals(ii).thresholdSWUncrowded);
    
%     swColumnC = sprintf('strokeWidth_%i', swThreshCro(ii));
    cro{ii} = percentOnTargCro.percent(ii).(thresholdSWVals(ii).thresholdSWCrowded);
end

uncr = str2double(uncr);
cro = str2double(cro);
for ii = 1:length(uncr)
    s1 = scatter(0,uncr(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    s1.MarkerFaceAlpha = .4;
hold on
end
scatter(0,mean(uncr),200,'k','filled')

for ii = 1:length(cro)
    s1 = scatter(1,cro(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    s1.MarkerFaceAlpha = .4;
hold on
end
scatter(1,mean(cro),200,'k','filled')
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
    'FontWeight','bold')
ylabel('Percent of Time Spent on Target')
title({'Threshold Strokewidth (per condition)', 'Uncrowded vs Crowded'})

xS = [0 1];
for ii = 1:length(subjectsAll)
    y1 = [uncr(1,ii) cro(1,ii)];
    points = line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
    points.LineStyle = '--';
end
yMean = [mean(uncr) mean(cro)];
ySTD = [std(uncr) std(cro)];
points = line(xS, yMean, 'Color','k','LineWidth',2);

% ylim([20 100])
xlim([-0.5 1.5])
[~,p2,ci] = ttest(uncr,cro);
ttestP = sprintf('(paired) p = %.3f', p2);
text(0.19, 90, ttestP,'Fontsize',20);
% test = cellstr(subjectsAll(:)');

% yMean = [mean(uncr) mean(cro)];

% ySTD = [std(uncr) std(uncr)]; %standard deviation
ySEM = ySTD/(sqrt(subNum)); %Standard Error Of The Mean At Each Value Of �x�

CI95 = tinv([0.025 0.975], subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
yCI95 = bsxfun(@times, ySEM, CI95(:)); % Calculate 95% Confidence Intervals Of All Experiments At Each Value Of �x�

errorbar(0,yMean(1),yCI95(1),yCI95(1),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
errorbar(1,yMean(2),yCI95(2),yCI95(2),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
% legend(test,'show');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/Span5PercentTimeSpentonTargetThresh.png');
%% Fixed SW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear swColumnU
clear swColumnC
clear sameTarUncr
clear sameTarCro
figure
for ii = 1:subNum%1:6%[1 3 4 5 6]%
    swColumnU = 'strokeWidth_4';%sprintf('strokeWidth_%i', swThreshUncr(ii));
    sameTarUncr{ii} = percentOnTargUncr.percent(ii).(swColumnU);
    
    swColumnC = 'strokeWidth_4';%sprintf('strokeWidth_%i', swThreshCro(ii));
    sameTarCro{ii} = percentOnTargCro.percent(ii).(swColumnC);
end

sameTarUncr = str2double(sameTarUncr);%([86.85 93.12 90.70 88.81 90.03 77.29] * 5);
sameTarCro = str2double(sameTarCro);%([60.00 79.79 83.07 69.86 82.55 53.96] * 5);
for ii = 1:length(sameTarCro)
    scatter(0,sameTarUncr(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled')
    hold on
    scatter(1,sameTarCro(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled')
    hold on
end
scatter(0,mean(sameTarUncr),200,'k','filled')
scatter(1,mean(sameTarCro),200,'k','filled')

names = {'Uncrowded','Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
    'FontWeight','bold')
ylabel('Percent of Time Spent on Target')
title({'Same Fixed Strokewidth', 'Uncrowded vs Crowded'})
xS = [0 1];
for ii = 1:length(subjectsAll)
    y1 = [sameTarUncr(1,ii) sameTarCro(1,ii)];
    points = line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
    points.LineStyle = '--';
end

xlim([-0.5 1.5])
% ylim([20 100])
[~,p2] = ttest(sameTarUncr,sameTarCro);
ttestP = sprintf('paired p = %.3f', p2);
text(0.20, 95, ttestP,'Fontsize',20);
text(-0,300,'(Strokewidth 3)', 'Fontsize',15 , 'Color',[0.9290 0.6940 0.1250])
% legend(points,subjectsAll);


yMean = [mean(sameTarUncr) mean(sameTarCro)];
points = line(xS, yMean, 'Color','k','LineWidth',2);

ySTD = [std(sameTarUncr) std(sameTarCro)]; %standard deviation
ySEM = ySTD/(sqrt(subNum)); %Standard Error Of The Mean At Each Value Of �x�

CI95 = tinv([0.025 0.975], subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
yCI95 = bsxfun(@times, ySEM, CI95(:)); % Calculate 95% Confidence Intervals Of All Experiments At Each Value Of �x�

errorbar(0,yMean(1),yCI95(1),yCI95(1),...
    'vertical', '-ok','MarkerSize',12,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
errorbar(1,yMean(2),yCI95(2),yCI95(2),...
    'vertical', '-ok','MarkerSize',12,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)


% saveas(gcf,...
% ('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\SummaryDocuments\GraphsforSummaryDocs\ManuScriptV1\PercentTimeSpentonTargetThreshWITHAshley.png'));
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PercentTimeSpentonTargetThresh.png');

%% Only for Crowded condition: Flankers/Target Answer Based on Where Spent most time
countersubj = 1;
clear propCorrectWhenTarget
clear propCorrectWhenFlanker
for ii = 1:length(subjectsAll)
    match = [];
    spentMostTimeOn = [];
    answTarget = [];
    counter = 1;
    byTrial = percentOnTargCro.byTrial;
    for numTrials = 1:length(byTrial.onTarget)
        if ~isempty(byTrial.onTarget{ii,numTrials})
            percTarget(counter) = str2num(byTrial.onTarget{ii,numTrials});
            answTarget(counter) = (byTrial.correctTarg{ii,numTrials});
            
            percR(counter) = str2num(byTrial.percentR{ii,numTrials});
            answR(counter) = (byTrial.correctR{ii,numTrials});
            
            percL(counter) = str2num(byTrial.percentL{ii,numTrials});
            answL(counter) = (byTrial.correctL{ii,numTrials});
            
            percT(counter) = str2num(byTrial.percentT{ii,numTrials});
            answT(counter) = (byTrial.correctT{ii,numTrials});
            
            percB(counter) = str2num(byTrial.percentB{ii,numTrials});
            answB(counter) = (byTrial.correctB{ii,numTrials});
            counter = counter + 1;
        end
    end
    for idx = 1:counter - 1
        [~, idxHighPerc] = max([percTarget(idx) percR(idx) percL(idx) percT(idx) percB(idx)]);
        [~, idxAnswer] = max([answTarget(idx) answR(idx) answL(idx) answT(idx) answB(idx)]);
        spentMostTimeOn(idx) = idxHighPerc;
        if idxHighPerc == 1 && idxAnswer == 1 %Target Most Time on and Answer Target
            match(idx) = 2;
        elseif idxHighPerc == idxAnswer  %Flanker Most Time on and Answer Matched
            match(idx) = 1;
        else
            match(idx) = 0;
        end
    end
    probFlankers(ii) = mean(match == 1);
    probTarget(ii) =  mean(match == 2);
    probOther(ii) = mean(match == 0);
%     probAnswerNumMostTimeOn(ii) = mean(match == 1 | match == 2);
    probAnswerNumMostTimeOn(ii) = 1 - mean(match == 0);
    propCorrect(ii) = mean(answTarget);
    
    MostOnTargetIdx = spentMostTimeOn == 1;
    MostOnFlankerIdx = spentMostTimeOn ~= 1;
    
    probTargetIsMostSpentTimeOn(ii) = mean(MostOnTargetIdx);
    probFlankerIsMostSpentTimeOn(ii) = mean(MostOnFlankerIdx);

%     targetPerc = mean(spentMostTimeOn == 1);
    rightPerc(ii) = mean(spentMostTimeOn == 2);
    leftPerc(ii) = mean(spentMostTimeOn == 3);
    topPerc(ii) = mean(spentMostTimeOn == 4);
    bottomPerc(ii) = mean(spentMostTimeOn == 5);
    
    if numel(MostOnTargetIdx) < 40 %|| numel(propCorrectWhenFlankerIdx)  < 50
        continue;
    end
    propCorrectWhenTarget(countersubj) = mean(answTarget(MostOnTargetIdx));
    
    propCorrectWhenFlanker(countersubj) = mean(answTarget(MostOnFlankerIdx));
    countersubj = countersubj + 1;
end
%% Plot Each Flanker
figure
for ii = 1:length(subjectsAll)
    scatter(1, probTargetIsMostSpentTimeOn(ii))
    hold on
    scatter(2, rightPerc(ii))
    hold on
    scatter(3, leftPerc(ii))
    hold on
    scatter(4, topPerc(ii))
    hold on
    scatter(5, bottomPerc(ii))
    hold on
end
    errorbar(1,mean(probTargetIsMostSpentTimeOn),std(probTargetIsMostSpentTimeOn),...
        'vertical', '-ok','MarkerSize',15,...
        'MarkerEdgeColor','black',...
        'MarkerFaceColor','black',...
        'CapSize',10,...
        'LineWidth',2)
     errorbar(2,mean(rightPerc),std(rightPerc),...
        'vertical', '-ok','MarkerSize',15,...
        'MarkerEdgeColor','black',...
        'MarkerFaceColor','black',...
        'CapSize',10,...
        'LineWidth',2)
     errorbar(3,mean(leftPerc),std(leftPerc),...
        'vertical', '-ok','MarkerSize',15,...
        'MarkerEdgeColor','black',...
        'MarkerFaceColor','black',...
        'CapSize',10,...
        'LineWidth',2)
     errorbar(4,mean(topPerc),std(topPerc),...
        'vertical', '-ok','MarkerSize',15,...
        'MarkerEdgeColor','black',...
        'MarkerFaceColor','black',...
        'CapSize',10,...
        'LineWidth',2)
     errorbar(5,mean(bottomPerc),std(bottomPerc),...
        'vertical', '-ok','MarkerSize',15,...
        'MarkerEdgeColor','black',...
        'MarkerFaceColor','black',...
        'CapSize',10,...
        'LineWidth',2)
    
    names = {'Most time on Target',...
   'Most time on RightF',...
   'Most time on LeftF',...
   'Most time on TopF',...
   'Most time on BottomF'};
xtickangle(10)
xlim([0.5 5.5])
set(gca,'xtick',[1 2 3 4 5],'xticklabel', names,'FontSize',12,...
    'FontWeight','bold')
ylabel('Probability');
title('At Threshold')

%% Prob of Answer Tar/Look Tar, Flank, and Non Match
figure;
scatter(zeros(1,length(probTarget)),probTarget)
hold on
scatter(ones(1,length(probFlankers)),probFlankers)
hold on
scatter([2 2 2 2 2 2 2 2 2 2], probOther)
xlim([-.5 2.5])
ylim([0 1])
[h,p] = ttest(probTarget,probFlankers);
hold on
errorbar(0,mean(probTarget),std(probTarget),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
hold on
errorbar(1,mean(probFlankers),std(probFlankers),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
hold on
errorbar(2,mean(probOther),std(probOther),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
names = {'Most time on Target and Answered Target ',...
   'Most Time on Flanker and Answer Flanker',...
   'Most Time Spent and Answer Didnt Match'};
xtickangle(10)
set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',12,...
    'FontWeight','bold')
ylabel('Probability');
title('At Threshold')
% title('One Less Thresh')

%% Prop Correct Targ Flank%%%%%%%%%%%%%%%%%%%
figure;
scatter(zeros(1,length(propCorrectWhenTarget)),propCorrectWhenTarget)
hold on
scatter(ones(1,length(propCorrectWhenFlanker)),propCorrectWhenFlanker)
hold on
scatter(0,mean(propCorrectWhenTarget),100,'k','filled');
hold on
scatter(1,mean(propCorrectWhenFlanker),100,'k','filled');
xlim([-.5 1.5])
ylim([0 1])
[h,p] = ttest(propCorrectWhenTarget,propCorrectWhenFlanker);
hold on
errorbar(0,mean(propCorrectWhenTarget),std(propCorrectWhenTarget),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
hold on
errorbar(1,mean(propCorrectWhenFlanker),std(propCorrectWhenFlanker),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
names = {'Most Time on Target',...
   'Most Time on Flanker'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',12,...
    'FontWeight','bold')
ylabel('Proportion Correct');


figure;
scatter(zeros(1,10),probAnswerNumMostTimeOn)
ylim([0 1])
%%
% for ii = 1:length(subjectsAll)
%     probTargetIsMostSpentTimeOn(ii) = mean(spentMostTimeOn(ii,:) == 1);
%     probFlankerIsMostSpentTimeOn(ii) =  mean(...
%         spentMostTimeOn(ii,:) == 5 |...
%         spentMostTimeOn(ii,:) == 2 |...
%         spentMostTimeOn(ii,:) == 3 |...
%         spentMostTimeOn(ii,:) == 4);
% end


% allSubjFlank = mean(probFlankers);
% allSubjTarg = mean(probTarget);
% allSubjProb = mean(prob);
allSubjPropCor = mean(propCorrect);
allSubjFlank = mean(probFlankerIsMostSpentTimeOn);
allSubjTarg = mean(probTargetIsMostSpentTimeOn);
allSubjProb = mean(probAnswerNumMostTimeOn);

figure;
for ii = 1:length(subjectsAll)
    scatter(1,probFlankerIsMostSpentTimeOn(ii),100,c(ii,:),'filled')
    hold on
    scatter(2,probTargetIsMostSpentTimeOn(ii),100,c(ii,:),'filled')
    hold on
    scatter(3,probAnswerNumMostTimeOn(ii),100,c(ii,:),'filled')
    hold on
    scatter(4,propCorrect(ii),100,c(ii,:),'filled')
    hold on
end
errorbar(1,allSubjFlank,std(probFlankerIsMostSpentTimeOn),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
hold on
errorbar(2,allSubjTarg,std(probTargetIsMostSpentTimeOn),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
hold on
errorbar(3,allSubjProb,std(probAnswerNumMostTimeOn),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
errorbar(4,allSubjPropCor,std(propCorrect),...
    'vertical', '-ok','MarkerSize',15,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
hold on

names = {'Spending Most Time On Flanker',...
    'Spending Most Time On Target', ...
    'Answering the Number Spent Most Time On',...
    'Proportion Correct'};
set(gca,'xtick',[1 2 3 4],'xticklabel', names,'FontSize',12,...
    'FontWeight','bold')
xtickangle(15)                   
xlim([0.5 4.5])
ylim([0 1])
% ylabel({'Probability Answering Number', 'Spent the Most Time On'})
ylabel({'Probability'})

title({'Probability of Answering the Number', 'Spent Most Time On - Thresh'})

[h,p1,ci,stats] = ttest(probAnswerNumMostTimeOn, probFlankers);
[h,p2,ci,stats] = ttest(probAnswerNumMostTimeOn, probTarget);
[h,p3,ci,stats] = ttest(probTarget, probFlankers);
%%%%%%
%% TESTING


%%
% figure;
% for ii = 1:10%subNum
%     less50(ii) = mean([infoOnUncrTarg(ii).perCorLess infoOnCroTarg(ii).perCorLess]);
%     more50(ii) = mean([infoOnUncrTarg(ii).perCorMore infoOnCroTarg(ii).perCorMore]);
% %     less50(ii) = mean([infoOnUncrTarg(ii).perCorLess



end

