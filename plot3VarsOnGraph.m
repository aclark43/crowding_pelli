function [ legends, info ] = plot3VarsOnGraph( percentOnTarget, Condition, subNum )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
names = fieldnames(percentOnTarget.(Condition).percent);
figure;
saveSize = [];
savePerf = [];
savePerc = [];
saveSpan = [];
for ii = 1:length(names)
    sw = names{ii};
    for i = 1:subNum
        perc(i) = str2double(percentOnTarget.(Condition).percent(i).(sw));
        if isempty(percentOnTarget.(Condition).perform(i).(sw))
            perf(i) = NaN;
        else
            perf(i) = (percentOnTarget.(Condition).perform(i).(sw));
        end
        if isempty(percentOnTarget.(Condition).sizeStim(i).(sw))
            size(i) = NaN;
        else
            size(i) = (percentOnTarget.(Condition).sizeStim(i).(sw));
        end
        if isempty(percentOnTarget.(Condition).span(i).(sw))
            span(i) = NaN;
        else
            span(i) = (percentOnTarget.(Condition).span(i).(sw));
        end
    end
    info.meanPerf(ii) = mean(perf(~isnan(perf)));
    info.stdPerf(ii) = std(perf(~isnan(perf)));
    info.meanSize(ii) = mean(size(~isnan(size)));
    info.stdSize(ii) = std(size(~isnan(size)));
    info.meanPerc(ii) = mean(perc(~isnan(perc)));
    info.stdPerc(ii) = std(perc(~isnan(perc)));
    info.meanSpan(ii) = mean(span(~isnan(span)));
    info.stdSpan(ii) = std(span(~isnan(span)));
    %
    yyaxis left
    legends.leg1(ii) = scatter(perc,perf,100,'filled');
    yyaxis right
    legends.leg2(ii) = scatter(perc,size,100,'filled');
    hold on
% for tIdx = 1:length(size
saveSize = [saveSize size(:)'];
savePerf = [savePerf perf(:)'];
savePerc = [savePerc perc(:)'];
saveSpan = [saveSpan span(:)'];
% counter = length(size) + 1; 
end

idxValues = find(~isnan(savePerc) & ~isnan(savePerf));

figure;
subplot(1,2,1)
[~,info.pPercPerf,~,info.rPercPerf] = LinRegression(savePerc(idxValues),savePerf(idxValues),0,NaN,1,0);
subplot(1,2,2)
[~,info.pPercSize,~,info.rPercSize] = LinRegression(savePerc(idxValues),saveSize(idxValues),0,NaN,1,0);
close

figure
subplot(1,2,1)
[~,p,~,r] = LinRegression(savePerc(idxValues),saveSpan(idxValues),0,NaN,1,0);
xlabel('Percent on Target')
ylabel('Mean Span')
subplot(1,2,2)
[~,p,~,r] = LinRegression(saveSpan(idxValues),savePerf(idxValues),0,NaN,1,0);
xlabel('Mean Span')
ylabel('Performance')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SpanPercentonTargPerformance.png');
close

end

