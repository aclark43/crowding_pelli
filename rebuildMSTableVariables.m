function [Subject, NumMS, Target_Start, Target_End, Right_Start, Right_End,...
    Left_Start,Left_End, Top_Start, Top_End, Bottom_Start, Bottom_End,...
    Background_Start, Background_End, sumLandNumTotal, sumLandBackTotal,...
    meanAll, stdAll] = ...
    rebuildMSTableVariables(landPosE,landPosS,subNum, condition)

counter = 1;
for i = 1:subNum
    if ~isempty(landPosE(i).Subject)
        subject(i+counter) = landPosE(i).Subject;
        numMS(i+counter) = str2double(landPosE(i).totNumMS);
        val.targetE(i+counter) = str2double(landPosE(i).targetE);
        val.rightE(i+counter) =str2double( landPosE(i).rightFE);
        val.leftE(i+counter) = str2double(landPosE(i).leftFE);
        val.topE(i+counter) = str2double(landPosE(i).topFE);
        val.bottomE(i+counter) = str2double(landPosE(i).bottomFE);
        backE(i+counter) = str2double(landPosE(i).backgE);
        
        targetS(i+counter) = str2double(landPosS(i).targetS);
        rightS(i+counter) = str2double(landPosS(i).rightFS);
        leftS(i+counter) = str2double(landPosS(i).leftFS);
        topS(i+counter) = str2double(landPosS(i).topFS);
        bottomS(i+counter) = str2double(landPosS(i).bottomFS);
        backS(i+counter) = str2double(landPosS(i).backgS);
        
        counter = counter +1; 
    end
end

%%%%%%%%%%%%%%%%%%
if strcmp('Uncrowded',condition)
    for ii = 1:length(val.targetE)
        if val.targetE(ii) == 0 && val.rightE(ii) == 0 &&...
                val.leftE(ii) == 0 && val.topE(ii) == 0 &&...
                val.bottomE(ii) == 0 && backE(ii) == 0
            val.targetE(ii) = NaN;
            val.rightE(ii) = NaN;
            val.leftE(ii)= NaN;
            val.topE(ii) = NaN;
            val.bottomE(ii) = NaN;
            val.backE(ii) = NaN;
        else
            val.backE(ii) = backE(ii)+ val.rightE(ii)+val.leftE(ii)+val.topE(ii)+val.bottomE(ii);
            val.rightE(ii) = NaN;
            val.leftE(ii)= NaN;
            val.topE(ii) = NaN;
            val.bottomE(ii) = NaN;
%             val.backE(ii) = NaN;
        end
    end
    sumLandNumEach = (val.targetE);
else
    for ii = 1:length(val.targetE)
        if val.targetE(ii) == 0 && val.rightE(ii) == 0 &&...
                val.leftE(ii) == 0 && val.topE(ii) == 0 &&...
                val.bottomE(ii) == 0 && backE(ii) == 0
            val.targetE(ii) = NaN;
            val.rightE(ii) = NaN;
            val.leftE(ii)= NaN;
            val.topE(ii) = NaN;
            val.bottomE(ii) = NaN;
            val.backE(ii) = NaN;
        else
            val.backE(ii) = backE(ii);
        end
    end
    sumLandNumEach = (val.targetE + val.rightE +...
            val.leftE + val.topE +...
            val.bottomE);
end

% sumLandNumTotal = mean(sumLandNumEach(~isnan(sumLandNumEach)));
% sumLandBackTotal = mean(val.backE(~isnan(sumLandNumEach)));

sumLandNumTotal =(sumLandNumEach(~isnan(sumLandNumEach)));
sumLandBackTotal =(val.backE(~isnan(sumLandNumEach)));


figure
bar(categorical({'Landing on Target or Flanker'}),mean(sumLandNumTotal),'FaceColor', [0, .3, 0]);
hold on
bar(categorical({'Landing on Background'}),mean(sumLandBackTotal),'FaceColor', [0, .8, 0]);
xlabel('MS Landing Location');
ylabel('Probability');
title('Probabilities of MS Landing Location');

meanAll = [ mean(sumLandBackTotal) mean(sumLandNumTotal)];
stdAll = [std(val.backE(~isnan(sumLandNumEach))) std(sumLandNumEach(~isnan(sumLandNumEach)))];
hold on
er = errorbar((categorical([{'Landing on Target or Flanker'} {'Landing on Background'}]))...
    ,meanAll, stdAll);
er.Color = [0 0 0];                            
er.LineStyle = 'none';  
[~,p] = ttest(sumLandNumEach(~isnan(sumLandNumEach)), val.backE(~isnan(sumLandNumEach)));
hold off


Subject = (subject)';
NumMS = (numMS)';
Target_Start = (targetS)';
Target_End = val.targetE';
Right_Start = rightS';
Right_End = val.rightE';
Left_Start = leftS';
Left_End = val.leftE';
Top_Start = topS';
Top_End = val.topE';
Bottom_Start = bottomS';
Bottom_End = val.bottomE';
Background_Start = backS';
Background_End = backE';
end