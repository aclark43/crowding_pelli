function [Dist,nDist] = LD_dist_covar_pairwise(VXM1,VXM2)
% Compare between two set of covariance matrices
% inputs:
%   VXM1 - covariance matrices from group 1; with dimension 2 x 2 x sample size
%   VXM2 - covariance matrices from group 2; with dimension 2 x 2 x sample size
% outputs:
%   Dist - all pairwise distance measures
%   nDist - all normalized (by size) pairwise distance measures
%
% Last update on 11/16/2020 by YCL

% sample size
n1 = size(VXM1,3);
n2 = size(VXM2,3);

% calculate all pairwise distances
dist = zeros(n1,n2);
n_dist = zeros(n1,n2);
for itrial1 = 1:n1
    for itrial2 = 1:n2
        M1 = VXM1(:,:,itrial1);
        M2 = VXM2(:,:,itrial2);
        [dist(itrial1,itrial2)] = LD_distance_covariance(M1,M2);
        
        % normalized distance (by the size of the 2d Gaussian ellipses)
        nM1 = M1/sqrt(det(M1));
        nM2 = M2/sqrt(det(M2));
        [n_dist(itrial1,itrial2)] = LD_distance_covariance(nM1,nM2);
    end
end

% reshape the matrices
Dist  = reshape(dist,[n1*n2,1]);
nDist = reshape(n_dist,[n1*n2,1]);

end

