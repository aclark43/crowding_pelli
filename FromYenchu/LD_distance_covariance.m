function [FN] = LD_distance_covariance(T1,T2)
% Calculate the standard distance between covariance matrix T1 and T2 by
% looking at how close is Q = inv(T1)*T2 to the identity.
%
% The function computes: 
% Frobenius norm of the log of Q
% The sqrt of the sum of the squares of the log of the square of eigenvalues of inv(T1) * T2
%
% Inputs:
%   T1: covariance matrix 1
%   T2: cavariance matrix 2
% Output:
%   FN: the distance between two covariance matrices, using Frobenius norm difference
%
% Last update on 11/19/2020 by YCL

T1T2 = T1\T2; % equivalent of T1T2 = inv(T1)*T2;

if sum(sum(isnan(T1T2))) == 0 && sum(sum(isinf(T1T2))) == 0
    eigvalue = eig(T1T2);
    sq_FN = log(eigvalue(1))^2 + log(eigvalue(2))^2;
    FN = sqrt(sq_FN);
else
    FN = NaN;
end
   
end