function [VX,original_mu,r_ellipse] = tgdt_fit_2d_gaussian(x,y,P)
% The function fits 2d data with 2d Gaussian ellipse with certian
% percentage of the data points
% Inputs: 
%   2d data - x and y
%   P - percentage/error of the data; default is 99%
% Outputs:
%   VX - covariance matrix of the data
%   original_mu - mean of the data (for plotting purpose)
%   r_ellipse - the ellipse according to the fit (for plotting purpose)
% 
% RAddditional reference (code modify from):
% http://www.visiondummy.com/2014/04/draw-error-ellipse-representing-covariance-matrix/
%
% last update: 11/19/2020 by YCL

% recenter the dataset
M = [];
M(1,:) = x;
M(2,:) = y;
original_mu = mean(M,2);
recenter_M = M-repmat(original_mu,1,length(M));
mu = mean(recenter_M,2)'; % new mean, mu

% covariance matrix V
VX = cov(recenter_M');
% A = sqrtm(VX); % this does the same thing

% sqrtm(V) should be the eigenvalues?
[m,D] = eig(VX);

% the probability for the constant probability contour
if ~exist('P') P = 0.99; end
r = (2*log(1/(1-P)))^0.5;

% Get the largest eigenvector and eigenvalue
[ind_c, ~] = find(D == max(max(D)));
largest_eigenvec = m(:, ind_c);
largest_eigenval = max(max(D));

% Get the smallest eigenvector and eigenvalue
if(ind_c == 1)
    smallest_eigenval = max(D(:,2));
    % smallest_eigenvec = m(:,2);
else
    smallest_eigenval = max(D(:,1));
    % smallest_eigenvec = m(:,1);
end

% Calculate the angle between the x-axis and the largest eigenvector
angle = atan2(largest_eigenvec(2), largest_eigenvec(1));

% This angle is between -pi and pi
if(angle < 0)
    angle = angle + 2*pi;
end

% Get the 99% confidence interval error ellipse
theta_grid = linspace(0,2*pi);
phi = angle;
a = r*sqrt(largest_eigenval);
b = r*sqrt(smallest_eigenval);
%%% take the two axis (smallest and largets eigen, then take the ratio of
%%% the two, which will give you values between 1 (circle) and how much it
%%% deviates

% the ellipse in x and y coordinates 
ellipse_x_r = a*cos(theta_grid);
ellipse_y_r = b*sin(theta_grid);

% define a rotation matrix
R = [cos(phi) sin(phi); -sin(phi) cos(phi)];

% rotate the ellipse to some angle phi
r_ellipse = [ellipse_x_r; ellipse_y_r]' * R;

% Plot the original data and draw the error ellipse (all data points with
% the one ellipse )
% X0 = original_mu(1);
% Y0 = original_mu(2);
% figure
% plot(M(1,:), M(2,:), '.');
% hold on;
% plot(r_ellipse(:,1)+X0, r_ellipse(:,2)+Y0, '-', 'linewidth',2)
% plot(mu(1),mu(2),'.','color','k')
% axis([-30 30 -30 30])

end

