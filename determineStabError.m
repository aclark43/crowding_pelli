function [differences] = determineStabError(dataAll)

namesAll = fieldnames(dataAll);
for ii = 1:size(namesAll)
    temp = dataAll.(namesAll{ii}).eis_data;
    differences{ii}.sysRenderTime_mean = mean(temp.system_events.video_card.avg_system_rendering_time(1,13:end));
    differences{ii}.sysRenderTime_std = std(temp.system_events.video_card.avg_system_rendering_time(1,13:end));
    real_x = temp.eye_data.eye_1.calibrated_x(13:end);
    real_y = temp.eye_data.eye_1.calibrated_y(13:end);
    timingPerFrame = double(temp.eye_data.timing.spd(13/3:end));
    
    idxKeep = (real_x < 20 & real_x > -20 & ...
               real_y < 20 & real_y > -20);
%     real_x = real_xT(idxKeep);
%     real_y = real_xT(idxKeep);
    
    stabilized_x = [];
    stabilized_y = [];
    counterTime = 0;
    for i = 1:length(timingPerFrame)
        cstabilized_x = [];
        cstabilized_y = [];
        counter = timingPerFrame(i);
        cstabilized_x(1:counter) = real_x(timingPerFrame(i)+counterTime+3);
        cstabilized_y(1:counter) = real_y(timingPerFrame(i)+counterTime+3);
        counterTime = counterTime + counter;
        stabilized_x = [stabilized_x cstabilized_x];
        stabilized_y = [stabilized_y cstabilized_y];
        if idxKeep((timingPerFrame(i)+counterTime+3))
            diffx(i) = real_x(timingPerFrame(i)+counterTime) - ...
                mean(real_x(counterTime:counterTime+timingPerFrame(i)));
            diffy(i) = real_y(timingPerFrame(i)+counterTime) - ...
                mean(real_y(counterTime:counterTime+timingPerFrame(i)));
        else
            diffx(i) = NaN;
            diffy(i) = NaN;
        end
    end
%     figure;
%     plot(1:length(real_x),real_x,'-o')
%     ylim([-20 20])
%     xlim([700 880])
%     hold on
%     plot(1:length(stabilized_x),stabilized_x,'-')
    
    differences{ii}.xm = nanmean(abs(diffx));
    differences{ii}.xs =nanstd(abs(diffx));
    differences{ii}.ym = nanmean(abs(diffy));
    differences{ii}.ys =nanstd(abs(diffy));
    
    

end

for ii = 1:length(differences)
    meanOfMeans(1,ii) = differences{ii}.xm;
    meanOfMeans(2,ii) = differences{ii}.ym;

end

mean(meanOfMeans(1,:))
mean(meanOfMeans(2,:))
std(meanOfMeans(1,:))
std(meanOfMeans(2,:))


end