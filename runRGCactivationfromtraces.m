clear all
clc
close all

D = [5 20]; % diffusion constant arcmin^2/s
Fs = 1000; % sampling rate (Hz)
nSamples = 500; % number of samples
nTraces = 1; % number of traces
numberOfBlobsOG = 525;%runLabelImage('cones2x');
counter = 1;
stimSizes = [1.2 13.3 22.66];
limits = [30 30 50];
ecc = [0 2 4];
zoomFactor = [1 5 10];
[Xe, Ye] = EM_Brownian2(10, Fs, nSamples, 1);
for e = 1:length(ecc)
    
        %     numOverlapCones = [];
        for ii = 1:nTraces
            
            
            
            size_retina = 239; % pixels, keep it odd
            half_size_retina = floor(size_retina/2);
            
            pixel_angle = .25; % pixel angle = xx arcmin (of images)
            
            [img1, map, alphachannel] = imread('cones4x.png');
            conesOG = double(img1) / 255;
            subplot(1,3,e)
            axis([-limits(e) limits(e) -limits(e), limits(e)])
            axis square
%             hFigure = figure;
%             set(hFigure, 'MenuBar', 'none');
%             set(hFigure, 'ToolBar', 'none');
%             
%             
%             redChannel = img1(:,:,1);
%             greenChannel = img1(:,:,2);
%             blueChannel = img1(:,:,3);
%             % black pixels are where red channel and green channel and blue channel in an rgb image are 0;
%             blackpixelsmask = redChannel <= 50 & greenChannel <= 50 & blueChannel <= 50;
%             % show binary image where the black pixels were
%             RI = imref2d(size(blackpixelsmask));
%             RI.XWorldLimits = [-limits(e) limits(e)];
%             RI.YWorldLimits = [-limits(e) limits(e)];
%             
%             axis square
%             
%             
%             imshow(flipud(rgb2gray(img1)), RI, 'Border', 'tight')
% % %             
            hold on
% % %             
% % %             hold on
% % %             
            plot(Xe+(stimSizes(e)*1.4), Ye,'-','Color',[1, 0, 0, .7],...
                'LineWidth',stimSizes(e));
            plot(Xe-(stimSizes(e)*1.4), Ye,'-','Color',[1, 0, 0, .7],...
                'LineWidth',stimSizes(e));
            hold on
            imgWTrace = plot(Xe, Ye,'-','Color',[0, 0, 1, .7],...
                'LineWidth',stimSizes(e));
            
            
%             plot(Xe, Ye, 'bv', 'LineWidth', 2)
%             grid on;
%             % Put images at every (x,y) location.
%             fileName = '../../Images/3_20x100-2.jpg';
%             rgbImage = imread(fileName);
%             for k = 1 : length(Xe)
%                 % Create smaller axes on top of figure.
%                 xImage = Xe(k);
%                 yImage = Ye(k);
%                 ax2 = axes('Position',[xImage, yImage, .05 .05]);
%                 box on;
%                 imshow(rgbImage);
%                 axis('off', 'image');
%             end
            
            
            
%             
%             zoom on
%             zoom off
%             zoom out
%             zoom reset
%             zoom xon
%             zoom yon
%             zoom(zoomFactor(e))
%             zoom(fig, option)
%             h = zoom(hFigure)
        end

end