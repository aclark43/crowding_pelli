function [sumLandNumTotal, sumLandBackTotal,...
    meanAll, stdAll] = probLandingTable(subjectsAll, subjectMS,subNum, condition )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

finalTable = [];
for ii = 1:length(subjectsAll)
    if isfield(subjectMS(ii).em, 'ms')
        if isfield(subjectMS(ii).em.ms,'landingPattern')
            [landingPosS, landingPosE] = buildLandingStruct (ii, subjectMS, subjectsAll);
            
            landingPosS(ii) = landingPosS;
            landingPosE(ii) = landingPosE;
            landPosS(ii) = landingPosS(ii);
            landPosE(ii) = landingPosE(ii);
            
            tabS(ii).indTab = struct2table(landingPosS(ii));
            tabE(ii).indTab = struct2table(landingPosE(ii));
        end
%     else
%         landingPosS(ii) = [];
%         landingPosE(ii) = [];
%         landPosS(ii) = [];
%         landPosE(ii) = [];
%         
%         tabS(ii).indTab = [];
%         tabE(ii).indTab = [];
     end
end
%%%%%%%%%%%%%%%%%%
[Subject, NumMS, Target_Start, Target_End, Right_Start, Right_End,...
    Left_Start,Left_End, Top_Start, Top_End, Bottom_Start, Bottom_End,...
    Background_Start, Background_End, sumLandNumTotal, sumLandBackTotal,...
    meanAll, stdAll] = ...
    rebuildMSTableVariables(landPosE,landPosS,subNum, condition);
%%%%%%%%%%%%%%%%%%%%%%%%%%%
landingTabAll = table(Subject, NumMS, Target_Start, Target_End, Right_Start, Right_End,...
    Left_Start,Left_End, Top_Start, Top_End, Bottom_Start, Bottom_End,...
    Background_Start, Background_End);

temp = table2struct(landingTabAll);
numSW = size(tabE);

for i = numSW(2)
    for ii = numSW(1)
        if isempty(tabE(ii,i))
            endingLoc(ii,i) = [];
        else
            endingLoc(ii,i) = 1;
        end
        if isempty(tabS(ii,i))
            startLoc(ii,i) = [];
        else
            startLoc(ii,i) = 1;
        end
    end
end






end

