function graphMultiPsychCurves(subjectAll, condition, stabilization, em, ecc, c, SubjectOrderDCIdx)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


for ii = 1:length(subjectAll)
    titleFigs{ii} = sprintf('%s_%s_%s_%s_%s', subjectAll{ii}, condition, stabilization{1}, em{1}, ecc{1});
%     titleFigs{ii} = sprintf('NoToss%s_%s_%s_%s_%s', subjectAll{ii}, condition, stabilization{1}, em{1}, ecc{1});
    
    pathToData = sprintf('../../Data/%s/Graphs/FIG', subjectAll{ii}); %File Location
%     pathToData = ('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\AllSubjTogether'); %File Location
    color = c(ii,:);
    fH{ii} = createGraphTogetherMultipleSubj...
        (titleFigs{ii},pathToData,color);
    xlim([0 5.5]);
    title(find(SubjectOrderDCIdx == ii));
      saveas(gcf, ...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PsychSubj%s.png',...
        subjectAll{ii}));
     saveas(gcf, ...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PsychSubj%s.epsc',...
        subjectAll{ii}));
end

% pathToData = ('../../Data/'); %File Location


for ii = 1:length(subjectAll)
    ax = get(fH{ii}, 'Children');
    axP{ii} = get(ax(1),'Children');
    axSaved{ii} = ax;
end

for ii = 2:length(subjectAll)
    copyobj(axP{ii},axSaved{1})
end

%figures to keep
figs2keep = [1];
all_figs = findobj(0, 'type', 'figure');
delete(setdiff(all_figs, figs2keep));


title_str = sprintf('%s', condition);
title(title_str);
set(gca, 'FontSize', 11, 'FontWeight', 'bold');

locText = .25:.05:1;
for ii = 1:length(subjectAll)
    text(0.25,locText(ii),subjectAll{ii},...
        'Color',[c(ii,1) c(ii,2) c(ii,3)]);
end



end

