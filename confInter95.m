function [ upper, lower ] = confInter95( var1 )
%Calculates the 95% confidence intervals of var1


alpha = 0.95;
p1 = ((1.0-alpha)/2.0) * 100;
lower = nanmean(var1) - (prctile(var1  , p1));
p2 = (alpha+((1.0-alpha)/2.0)) * 100;
upper = (prctile(var1  , p2)) - nanmean(var1);


% 
% % x = randi(50, 1, 100);                       % Create Data
% SEM = std(var1)/sqrt(length(var1));            % Standard Error
% ts = tinv([0.025  0.975], length(var1)-1);      % T-Score
% % CI = mean(var1) + ts*SEM;                      % Confidence Intervals
% CI = mean(var1) + ts; %08.08.2020 MP remove SEM   
% 
% ci_fromMean(1) = abs(mean(var1) - CI(2));
% % ci_fromMean(2) = CI(1) - mean(var1);
end

