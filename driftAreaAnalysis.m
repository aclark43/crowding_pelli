function mArea = driftAreaAnalysis(subjectCondition, subjectThreshCondition, subjectsAll, condition, c, fig, thresholdSWVals, params)
%Looks at velocity calculated in runMultiple Subjects for each individual
%subject at each SW, as well as across Subj for threshold SW.


% CHECK DRIFT VELOCITY FOR THRESHOLD SW
mArea = [];
for ii = 1:length(subjectsAll)
    strokeValue =(subjectThreshCondition(ii).thresh);
    if strcmp('Uncrowded',condition)
        sw = thresholdSWVals(ii).thresholdSWUncrowded;
    elseif strcmp('Crowded',condition)
        sw = thresholdSWVals(ii).thresholdSWCrowded;
    end
    areaSubj(ii) = subjectThreshCondition(ii).em.ecc_0.(sw).areaCovered;
    errorBar(areaSubj(ii),strokeValue)
    hold on
    points(ii) = scatter(areaSubj(ii),strokeValue,100,c(ii,:),'filled');
    
    if params.machine(ii) == 1
        samp = 1000;
        conv = 1;
    elseif params.machine(ii) == 2
        samp = 341;
        conv = 1000/330;
    end
    
    bcea = [];
    xVal = [];
    yVal = [];
    xDrift = [];
    yDrift = [];
    %     xVal175 = [];
    %     yVal175 = [];
    for numTrials = 1:length(subjectThreshCondition(ii).em.ecc_0.(sw).position)
        
        xDrift(numTrials,:) = subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).x(1:500/conv);
        yDrift(numTrials,:) = subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).y(1:500/conv);
        
        %         xVal = subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).x(1:round(175/conv));
        %         yVal = subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).y(1:round(175/conv));
        
        %         limit.xmin = floor(min(xVal(numTrials,:)));
        %         limit.xmax = ceil(max(xVal(numTrials,:)));
        %         limit.ymin = floor(min(yVal(numTrials,:)));
        %         limit.ymax = ceil(max(yVal(numTrials,:)));
        
        %         rangeXY(numTrials) = max(abs(cell2mat(struct2cell(limit))));
        %               dataSmallLargeDC.stimSize(ii) = subjectThreshCondition(ii).em.ecc_0.(sw).stimulusSize;
        
        %         xDrift = [xDrift xVal];
        %         yDrift = [yDrift yVal];
        x = xDrift(numTrials,:)/60;
        y = yDrift(numTrials,:)/60;
% % %         x2 = 2.291; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
% % %         bceaMatrix =  2*x2*pi * std(x) * std(y) * ...
% % %             (1-corrcoef(x,y).^2).^0.5; %smaller BCEA correlates to more stable fixation
        
        %         not sure how you get your x2 2.291. Here is how we calculate the
        % k in the bcea equation (below). The rest of the formula looks fine. For 
        prob = 0.68; k = 1.139;
        % from Steinman 1965 (corr stands for corr coeff)
        co = corrcoef(x',y');
        co = co(2,1);
        k = -log(1-prob);
        bceaArea = 2*k*pi*std(x)*std(y)*sqrt(1-co^2);
        
        bcea(numTrials) = abs(bceaArea);
%         bcea(numTrials) = abs(bceaMatrix(1,2));
        
        SDp(numTrials) = mean([std(x) std(y)]);
        %Check to see how much of traces is spent within a 5x5 area
        areaWithin = 5; %(5x5 area)
        numTrialsWithinArea = find(xDrift(numTrials,:) < areaWithin & xDrift(numTrials,:) > -areaWithin &...
        yDrift(numTrials,:) < areaWithin & yDrift(numTrials,:) > -areaWithin);
        percentWithinArea(numTrials) = length(numTrialsWithinArea)/length(xDrift(numTrials,:)) * 100;
        
        RMS(numTrials) = mean([rms(x) rms(y)]);
        
    end
    averagePercentWithinArea(ii) = mean(percentWithinArea);
    bceaMean(ii) = mean(bcea);
    SDpMean(ii) = mean(SDp);
    RMSMean(ii) = mean(RMS);
    limit.xmin = floor(min(xDrift));
    limit.xmax = ceil(max(xDrift));
    limit.ymin = floor(min(yDrift));
    limit.ymax = ceil(max(yDrift));
    rangeXY = max( max(abs(cell2mat(struct2cell(limit)))));
    
    %     rangeXY =max( max(abs(cell2mat(struct2cell(limit)))));
    %     dataSmallLargeDC.stimSize(ii) = subjectThreshCondition(ii).em.ecc_0.(sw).stimulusSize;
    
    %     areaAll(ii) = CalculateTheArea(xDrift, yDrift, 0.68, rangeXY, 50);
    
    %     strokeValue =(subjectThreshCondition(ii).thresh);
    %     [~,thresholdIdx] = min(abs(strokeValue - subjectCondition(ii).stimulusSize));
    %     strokeWidth = sprintf('strokeWidth_%i',subjectCondition(ii).SW(thresholdIdx));
    %     fprintf('%s Num Trials = %i\n', subjectsAll{ii}, length(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).position));
    %     areaSubj(ii) = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).areaCovered;
    %     mSingleArea = mean(areaSubj);
    plotSW = zeros(length(areaSubj), 1);
    plotSW(:) = strokeValue;
    plotSW = plotSW';
    errorBar(areaSubj,strokeValue)
    hold on
    points(ii) = scatter(areaSubj(ii),strokeValue,100,c(ii,:),'filled');
end
mArea.task500.all = areaSubj;
mArea.task500.mean = mean(areaSubj);
mArea.task500.std = std(areaSubj);
mArea.averagePercentWithinArea.fiveXfive = averagePercentWithinArea;
mArea.bcea = bceaMean;
mArea.SDp = SDpMean;
mArea.RMS = RMSMean;
% xlabel('Area')
% ylabel('Threshold Strokewidth')
% title(condition{1})
% legend([points],subjectsAll)
% saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjArea%s.png', condition{1}));

% thresholds = [subjectThreshCondition.thresh];

figure;
boxplot(mArea.bcea);
set(gca, 'YScale', 'log')
ylim([0 15])
yticks([0.003 0.01 0.03 0.1 0.3 1 3 10])
ylabel('BCEA (deg^2)');
xlabel('Controls, APLab');
saveas(gcf, '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DriftOnlyArea.png');

% [~,p,~,r] = LinRegression(mArea,thresholds,0,NaN,1,0);
% for ii = 1:length(subjectsAll)
%     scatter((ii), mArea(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled')
%     hold on
% end
% rValue = sprintf('r^2 = %.3f', r);
% pValue = sprintf('p = %.3f', p);
% text(40, 3, pValue,'Fontsize',10);
% text(40,3.2,rValue,'Fontsize',10);
% xlabel('Mean Area at Threshold')
% ylabel('Strokewidth Threshold')
% graphTitle1 = 'Mean Area by Threshold Strokewidth';
% graphTitle2 = (sprintf('%s', cell2mat(condition)));
% title({graphTitle1,graphTitle2})
% xlim([30 60])
% ylim([0 4])
% saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjAreabyStrokewidth%s.png', condition{1}));
% close all

mArea.task175.all = [136.39680,50.803200,126.72000,113.04000,79.027206,151.20001,113.41760,101.99039,120.18880,260.66559]; %175 Drift Only Trials
mArea.fix175.all = [29.250000,26.527500,110.25000,92.250000,69.750000,265.50000,NaN,113.68000,83.250000,185.01999]; %DMS Segments
mArea.fix175DriftOnly.all = [71.68 45.72 151.632 144.256 116.1216 74.6496 2.704 77.44 56.1152 145.6128]; %Drift Segments
areaTask500 = mArea.task500.all;

% [~,p] = ttest(areaTask175(idx), newAreaFix175(idx)');
% [~,p] = ttest(newAreaFix175(idx), double(areaTask500(idx)));
% [~,p] = ttest(areaTask175(idx), double(areaTask500(idx)));
%
%
%
% figure;
% for ii = find(idx)
%     plot(1,areaTask175(ii),'o','Color',c(ii,:),'MarkerSize', 15, 'MarkerEdgeColor', c(ii,:),...
%     'MarkerFaceColor', c(ii,:));
%     hold on
%     plot(2,areaTask500(ii),'o','Color',c(ii,:),'MarkerSize', 15, 'MarkerEdgeColor', c(ii,:),...
%     'MarkerFaceColor', c(ii,:));
%     line([1 2],[areaTask175(ii) areaTask500(ii)], 'LineStyle','--', 'Color',c(ii,:));
%
%      plot(3,newAreaFix175(ii),'o','Color',c(ii,:),'MarkerSize', 15, 'MarkerEdgeColor', c(ii,:),...
%     'MarkerFaceColor', c(ii,:));
%      line([2 3],[areaTask500(ii) newAreaFix175(ii)], 'LineStyle','--', 'Color',c(ii,:));
% end
% xlim([0.5 3.5])
% xticks([1 2 3])
% xticklabels({'Task 175', 'Task 500', 'Fixation 175'})
%
% figure;
% for ii = find(idx)
%     plot(1,areaTask175(ii),'o','Color',c(ii,:),'MarkerSize', 15, 'MarkerEdgeColor', c(ii,:),...
%     'MarkerFaceColor', c(ii,:));
%     hold on
%     plot(2,newAreaFix175(ii),'o','Color',c(ii,:),'MarkerSize', 15, 'MarkerEdgeColor', c(ii,:),...
%     'MarkerFaceColor', c(ii,:));
%     line([1 2],[areaTask175(ii) newAreaFix175(ii)], 'LineStyle','--', 'Color',c(ii,:));
% end
% errorbar(1, mean(areaTask175), sem(areaTask175), 'o', 'Color', 'k');
% errorbar(2, nanmean(newAreaFix175), sem(areaFix175(idx)), 'o', 'Color', 'k');
% xlim([0.5 2.5])
% xticks([1 2])
% xticklabels({'Task', 'Fixation'})
%
% saveas(gcf, '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DriftOnlyArea.epsc');
% saveas(gcf, '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DriftOnlyArea.png');

end

