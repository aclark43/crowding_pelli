function checkSWThresholdPerformance(subjectThreshCro, subjectThreshUnc,c,...
    thresholdSWVals,subjectsAll,subNum)

figure;
for ii = 1:subNum
    swU = sprintf('%s',thresholdSWVals(ii).thresholdSWUncrowded);
    swC = sprintf('%s',thresholdSWVals(ii).thresholdSWCrowded);

    performanceC(ii) = mean(subjectThreshCro(ii).em.ecc_0.(swC).correct(:));
    performanceU(ii) = mean(subjectThreshUnc(ii).em.ecc_0.(swU).correct(:));
    
    leg(ii) = scatter(0,performanceU(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
    hold on
    scatter(1, performanceC(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled')
end
hold on
plot(0,mean(performanceU),'*',...
            'Color','k');
hold on
plot(1,mean(performanceC), '*',...
            'Color','k');
xlim([-1 2])
legend(leg,subjectsAll)

[~,p] = ttest(performanceU, performanceC);
text(0.5,.5,sprintf('p = %.3f',p));

names = ({'Uncrowded','Crowded'});
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
       'FontWeight','bold')
   ylabel('Performance')
   
   saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/threshTrialPicked.png');
   saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/threshTrialPicked.epsc');
end