
function timeIntervalLevel = fixationAnalysisMultipleSubjects_Simplified(fixationInfo, subjectsAll, subNum, params)

%Fixation analysis across several subjects
%   looks at the fixation behavior across several subjects. Includes
%   heatmaps of different time bins, distance from 0,0 across time bins,
%   etc.
timeIntervalLevel = [];
c = params.c;
%% Looking at specific TimeBins during Fixation
% This is serperating fixation into 10 different time bins, then
% performing a series of analysis on each of those time bins to see how
% fixation changes over time.
timeSections = [{1:10} {11:50} {51:100} {101:200} {201:300} {301:500} ...
    {501:700} {701:1000} {1001:1300} {1301:1600} {1:1600} {1:165}];


for ii = 1:subNum
    xCat = [];
    yCat = [];
    for numTrials = 1:length(fixationInfo{ii}.tracesX)
        for numTimeSect = 1:length(timeSections)
            if length(fixationInfo{ii}.tracesX{numTrials}) < max(timeSections{numTimeSect})
                bySampleX{ii}.sampleNum{numTrials, numTimeSect} = NaN;
                bySampleY{ii}.sampleNum{numTrials, numTimeSect} = NaN;
                continue;
            else
                bySampleX{ii}.sampleNum{numTrials, numTimeSect} = fixationInfo{ii}.tracesX{numTrials}(timeSections{numTimeSect});
                bySampleY{ii}.sampleNum{numTrials, numTimeSect} = fixationInfo{ii}.tracesY{numTrials}(timeSections{numTimeSect});
            end
            
        end
        xCat = [xCat fixationInfo{ii}.tracesX{numTrials}];
        yCat = [yCat fixationInfo{ii}.tracesY{numTrials}];
    end
    timeIntervalLevel.xCat{ii} = xCat;
    timeIntervalLevel.yCat{ii} = yCat;
end

for ii = 1:subNum
    xCat = [];
    yCat = [];
    x = [];
    y = [];
    for numTimeSect = 1:length(timeSections)
        
        x = [bySampleX{ii}.sampleNum{:, numTimeSect}];
        y = [bySampleY{ii}.sampleNum{:, numTimeSect}];
        
        [XValues, YValues] = cleanXYTrialsFromNans(x, y, 200);
        [height,width] = size(XValues);
        if height>width
            XValues = XValues';
            YValues = YValues';
        end
        
        nXValues = XValues(XValues < 60 & XValues > -60 &...
            YValues < 60 & YValues > -60);
        nYValues = YValues(YValues < 60 & YValues > -60 &...
            XValues < 60 & XValues > -60);
        
        timeIntervalLevel.X{ii,numTimeSect} =nXValues;
        timeIntervalLevel.Y{ii,numTimeSect} =nYValues;
        
        limit.xmin = floor(min(nXValues));
        limit.xmax = ceil(max(nXValues));
        limit.ymin = floor(min(nYValues));
        limit.ymax = ceil(max(nYValues));
        
        [result1] = MyHistogram2(nXValues, nYValues, [limit.xmin,limit.xmax,30;limit.ymin,limit.ymax,30]);
        result = result1./(max(max(result1)));
        
        timeIntervalLevel.result{ii,numTimeSect} = result;
        timeIntervalLevel.limit{ii,numTimeSect} = limit;
        timeIntervalLevel.span25(ii,numTimeSect) = quantile(sqrt((nXValues-nanmean(nXValues)).^2 + (nYValues-nanmean(nYValues)).^2), .10);
        %         figure;
        %         pcolor(linspace(limit.xmin, limit.xmax, size(result, 1)),...
        %             linspace(limit.ymin, limit.ymax, size(result, 1)),...
        %             result');
        
        x_arcmin = linspace(limit.xmin, limit.xmax, size(result, 1)); % these are the pretend arcmin corresponding to the pdf
        y_arcmin = linspace(limit.ymin, limit.ymax, size(result, 1));
        
        [~, idx] = max(result(:)); % find the max
        
        [row, col] = ind2sub(size(result), idx); % convert to row and col
        
        x_pk = x_arcmin(row); % get row and col in arcmin
        y_pk = y_arcmin(col);
        
        timeIntervalLevel.x_pk(ii,numTimeSect) = x_pk;
        timeIntervalLevel.y_pk(ii,numTimeSect) = y_pk;
        timeIntervalLevel.eucDist(ii,numTimeSect) = hypot(x_pk,y_pk);%sqrt(sum((x_pk - y_pk) .^ 2));
        %         hold on
        %         plot(x_pk, y_pk, 'o','Color', 'k');
        
         [timeIntervalLevel.com(ii,numTimeSect), ...
            timeIntervalLevel.massX(ii,numTimeSect), ...
            timeIntervalLevel.massY(ii,numTimeSect), ...
            ~,...
            ~, timeIntervalLevel.massXSTD(ii,numTimeSect), ...
            timeIntervalLevel.massYSTD(ii,numTimeSect)] ...
            = centerOfMassCalculation(nXValues, nYValues);
        
%         x2 = 2.291; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
%         temp1 = sqrt(var(nXValues));
%         temp2 = sqrt(var(nYValues));
%         bceaMatrix =  2*x2*pi * temp1 * temp2 * (1-corrcoef(nXValues,nYValues).^2).^0.5; %smaller BCEA correlates to more stable fixation
        timeIntervalLevel.bcea(ii,numTimeSect) = get_bcea(nXValues, nYValues);%abs(bceaMatrix(1,2));
%       if numTimeSect == 11
%             
%             axisVal = 40;
%             figure;
%             generateHeatMapSimple( ...
%                 nXValues, ...
%                 nYValues, ...
%                 'Bins', 30,...
%                 'StimulusSize', 16,...
%                 'AxisValue', axisVal,...
%                 'Uncrowded', 0,...
%                 'Borders', 1);
%             plot([-axisVal axisVal], [0 0], '--k', 'LineWidth',3)
%             plot([0 0], [-axisVal axisVal], '--k', 'LineWidth',3)
%             rectangle('Position',[-8, -8, 16, 16],'LineWidth',3)
% 
%             set(gca,'FontSize',14)
%             axis off
%             box on
%             if params.control(ii) == 1
%                 text(-axisVal+3, axisVal-10, sprintf('C%i', str2double(regexp(subjectsAll{ii},'\d*','Match'))), ...
%                     'FontSize',30, 'FontWeight', 'bold');
%             else
%                 text(-axisVal+3, axisVal-10, sprintf('PT%i', str2double(regexp(subjectsAll{ii},'\d*','Match'))), ...
%                     'FontSize',30, 'FontWeight', 'bold');
%             end
%             saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/ALL/%s_fixHeatMap.png', subjectsAll{ii}));
% 
%         end
    end
end

time = [10 10+40 10+40+50 10+40+50+100 10+40+50+100+100 ...
    10+40+50+100+100+200 10+40+50+100+100+200+200 ...
    10+40+50+100+100+200+200+300 10+40+50+100+100+200+200+300+300 ...
    10+40+50+100+100+200+200+300+300+300];

time = [100:160:1600];

timeBins = round((1000/341)*time);

%% PRL across time
figure;
subplot(2,1,1)
plotAcrossTime(params,timeIntervalLevel.span25(params.control == 1,1:10),...
    time,timeIntervalLevel.eucDist(params.control ==1,1:10),c);
ylabel({'Euclidean Distance(arcmin)','10% Span Error Bars'})
xlabel('Samples (330hz)');
legend(subjectsAll{params.control == 1},'Location','northwest')
title('Controls (NonCumalitive)')
ylim([0 30])

subplot(2,1,2)
plotAcrossTime(params,timeIntervalLevel.span25(params.control == 0,1:10),time,timeIntervalLevel.eucDist(params.control == 0,1:10),c);
ylabel({'Euclidean Distance(arcmin)','10% Span Error Bars'})
xlabel('Samples (330hz)');
legend(subjectsAll{params.control == 0},'Location','northwest')
title('Patients (NonCumalitive)')
ylim([0 30])
set(gcf, 'Position',  [2000, 100, 1400, 800])
% line([0 time(10)], [0 0],'Color','black','LineStyle','--')
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/EucDistAcrossTime.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/EucDistAcrossTime.png');

%% Violin Plots of X and Y
figure;
y = [];

y(1) = errorbar(1,mean([timeIntervalLevel.eucDist(params.control == 0,1)' ...
    timeIntervalLevel.eucDist(params.control == 0,2)']),...
     sem([timeIntervalLevel.eucDist(params.control == 0,1)' ...
    timeIntervalLevel.eucDist(params.control == 0,2)']),'o','Color','b');
 hold on
y(2) =  errorbar(2,mean([timeIntervalLevel.eucDist(params.control == 0,9)' ...
    timeIntervalLevel.eucDist(params.control == 0,10)']),...
     sem([timeIntervalLevel.eucDist(params.control == 0,9)' ...
    timeIntervalLevel.eucDist(params.control == 0,10)']),'o','Color','r');

hold on
errorbar(3,mean([timeIntervalLevel.eucDist(params.control == 0,1)' ...
    timeIntervalLevel.eucDist(params.control == 1,2)']),...
     sem([timeIntervalLevel.eucDist(params.control == 0,1)' ...
    timeIntervalLevel.eucDist(params.control == 1,2)']),'o','Color','b');
 hold on
errorbar(4,mean([timeIntervalLevel.eucDist(params.control == 0,9)' ...
    timeIntervalLevel.eucDist(params.control == 1,10)']),...
     sem([timeIntervalLevel.eucDist(params.control == 0,9)' ...
    timeIntervalLevel.eucDist(params.control == 1,10)']),'o','Color','r');
 xlim([0 5])
 xticks([1.5 3.5])
 xticklabels({'Patient', 'Control'})
 legend(y,{'First 50 samples','Last 600 samples'});
 ylabel('Euclidean Offset During Fixation');
 
 x = [];
 [~,x] = ttest2(timeIntervalLevel.eucDist(params.control == 0,1),...
     timeIntervalLevel.eucDist(params.control == 1,1));
 
 [~,x] = ttest([timeIntervalLevel.eucDist(params.control == 1,1)' timeIntervalLevel.eucDist(params.control == 1,2)'],...
     [timeIntervalLevel.eucDist(params.control == 1,10)' timeIntervalLevel.eucDist(params.control == 1,11)']);
 
 figure;
 for numTimeSect = 1:length(timeSections)-2
     y(1) = errorbar(numTimeSect,mean(timeIntervalLevel.eucDist(params.control == 0,numTimeSect)),...
         sem(timeIntervalLevel.eucDist(params.control == 0,numTimeSect)),'o','Color','m');
    hold on
 end
 for numTimeSect = 1:length(timeSections)-2
     y(2) = errorbar(numTimeSect,mean(timeIntervalLevel.eucDist(params.control == 1,numTimeSect)),...
         sem(timeIntervalLevel.eucDist(params.control == 1,numTimeSect)),'o','Color','b');
    hold on
 end
 
 legend(y,{'Patients','Controls'});
 xticklabels(time)
 xlim([0 11])
 ylabel('Euclidean Offset')
 xlabel('Num Samples (330hz)')
%% COM
% figure;
% subplot(2,1,1)
% plotAcrossTime(params,timeIntervalLevel.massXSTD(params.control == 1,1:10),time,timeIntervalLevel.com(params.control ==1,1:10),c);
% ylabel({'COM Distance(arcmin)','STD Error Bars'})
% xlabel('Samples (330hz)');
% legend(subjectsAll{params.control == 1},'Location','northwest')
% title('Controls (NonCumalitive)')
% ylim([0 30])
% 
% subplot(2,1,2)
% plotAcrossTime(params,timeIntervalLevel.massXSTD(params.control == 0,1:10),time,timeIntervalLevel.com(params.control == 0,1:10),c);
% ylabel({'COM Distance(arcmin)','STD Error Bars'})
% xlabel('Samples (330hz)');
% legend(subjectsAll{params.control == 0},'Location','northwest')
% title('Patients (NonCumalitive)')
% ylim([0 30])
% set(gcf, 'Position',  [2000, 100, 1400, 800])
% % line([0 time(10)], [0 0],'Color','black','LineStyle','--')
% saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/EucDistAcrossTime.png');
% saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/EucDistAcrossTime.png');


%%
figure;
axisSize = 15;
counter = 1;

subplot(1,2,1)
x = timeIntervalLevel.x_pk(:,11);
y = timeIntervalLevel.y_pk(:,11);
std = timeIntervalLevel.span25(:,11);
for ii = find(params.control == 1)
    leg(counter) =  errorbar(x(ii), y(ii), ...
        std(ii), std(ii), std(ii), std(ii),...
        's', 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',11, ...
        'LineWidth',2);
%     axis square
    hold on
    counter = counter + 1;
end
axis([-axisSize axisSize -axisSize axisSize])
% line([-15 15], [-15 15])
line([0 0],[-axisSize axisSize],'Color','k')
line([-axisSize axisSize],[0 0],'Color','k')
axis square
legend(leg,subjectsAll{params.control == 1});
ylabel('Y')
xlabel('X')
% set(gcf, 'Position',  [2000, 300, 800, 600])
title('Prob Gaze (10% Span)');

clear leg
counter = 1;
subplot(1,2,2)
for ii = find(params.control == 0)
    leg(counter) =  errorbar(x(ii), y(ii), ...
        std(ii), std(ii), std(ii), std(ii),...
        'o', 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',11, ...
        'LineWidth',2);
%     axis square
    hold on
    counter = counter + 1;
end
axis([-axisSize axisSize -axisSize axisSize])
% line([-15 15], [-15 15])
line([0 0],[-axisSize axisSize],'Color','k')
line([-axisSize axisSize],[0 0],'Color','k')
axis square
legend(leg,subjectsAll{params.control == 0});

ylabel('Y')
xlabel('X')
set(gcf, 'Position',  [2000, 100, 1400, 800])
title('Prob Gaze (10% Span)');
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/OffsetAcrossTime.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/OffsetAcrossTime.png');
%% COM
figure;
% axisSize = 20;
counter = 1;

subplot(1,2,1)
x = timeIntervalLevel.massX(:,11);
y = timeIntervalLevel.massY(:,11);
std = timeIntervalLevel.massXSTD(:,11)/2;
std2 = timeIntervalLevel.massYSTD(:,11)/2;
for ii = find(params.control == 1)
    leg(counter) =  errorbar(x(ii), y(ii), ...
        std(ii), std(ii), std2(ii), std2(ii),...
        's', 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',11, ...
        'LineWidth',2);
%     axis square
    hold on
    counter = counter + 1;
end
axis([-axisSize axisSize -axisSize axisSize])
% line([-15 15], [-15 15])
line([0 0],[-axisSize axisSize],'Color','k')
line([-axisSize axisSize],[0 0],'Color','k')
axis square
legend(leg,subjectsAll{params.control == 1});
ylabel('Y')
xlabel('X')
% set(gcf, 'Position',  [2000, 300, 800, 600])
title('COM (2 STD)');

clear leg
counter = 1;
subplot(1,2,2)
for ii = find(params.control == 0)
    leg(counter) =  errorbar(x(ii), y(ii), ...
        std(ii), std(ii), std2(ii), std2(ii),...
        'o', 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',11, ...
        'LineWidth',2);
%     axis square
    hold on
    counter = counter + 1;
end
axis([-axisSize axisSize -axisSize axisSize])
% line([-15 15], [-15 15])
line([0 0],[-axisSize axisSize],'Color','k')
line([-axisSize axisSize],[0 0],'Color','k')
axis square
legend(leg,subjectsAll{params.control == 0});

ylabel('Y')
xlabel('X')
set(gcf, 'Position',  [2000, 100, 1400, 800])
title('COM (2 STD)');
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/COMAcrossTime.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/COMAcrossTime.png');

%%
clear leg
% % % for ii = 1:length(params.control)
% % %     figure;
% % %     subplot(1,2,1)
% % %     leg(1) = errorbar(timeIntervalLevel.x_pk(ii,11), timeIntervalLevel.y_pk(ii,11), ...
% % %         timeIntervalLevel.span25(ii,11), timeIntervalLevel.span25(ii,11),...
% % %         timeIntervalLevel.span25(ii,11), timeIntervalLevel.span25(ii,11),...
% % %         'o', 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',10);
% % %     hold on
% % %     leg(2) = errorbar(timeIntervalLevel.massX(ii,11), timeIntervalLevel.massX(ii,11), ...
% % %         timeIntervalLevel.massXSTD(ii,11), timeIntervalLevel.massXSTD(ii,11),...
% % %         timeIntervalLevel.massYSTD(ii,11), timeIntervalLevel.massYSTD(ii,11),...
% % %         '*', 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',10);
% % %     line([0 0],[-20 20],'Color','k')
% % %     line([-20 20],[0 0],'Color','k')
% % %     if params.control(ii) == 1
% % %         title(sprintf('Control, %s',subjectsAll{ii}));
% % %     else
% % %         title(sprintf('Patient, %s',subjectsAll{ii}));
% % %     end
% % %     legend(leg,{'Peak','COM'});
% % %     axis square
% % %     subplot(1,2,2)
% % %     pcolor(linspace(timeIntervalLevel.limit{ii,11}.xmin, ...
% % %         timeIntervalLevel.limit{ii,11}.xmax, ...
% % %         size(timeIntervalLevel.result{ii,11}, 1)),...
% % %         linspace(timeIntervalLevel.limit{ii,11}.ymin, ...
% % %         timeIntervalLevel.limit{ii,11}.ymax, ...
% % %         size(timeIntervalLevel.result{ii,11}, 1)),...
% % %         timeIntervalLevel.result{ii,11}');
% % %     axis square
% % %     axis([-20 20 -20 20])
% % %     set(gcf, 'Position',  [2000, 100, 1000, 400])
% % % 
% % %     saveas(gcf,sprintf('../../SummaryDocuments/HuxlinPTFigures/COMvsPeak_%s.png', subjectsAll{ii}));
% % %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/COMvsPeak_%s.png', subjectsAll{ii}));
% % % end
% timeIntervalLevel.result{ii,numTimeSect}
% errorbar(find(params.control == 0), timeIntervalLevel.eucDist(params.control == 0,11),zeros(size(find(params.control == 0))),timeIntervalLevel.span25(params.control == 0,11), 'o');
% hold on
% errorbar(find(params.control == 1), timeIntervalLevel.eucDist(params.control == 1,11),zeros(size(find(params.control == 1))), timeIntervalLevel.span25(params.control == 1,11), 'o');
% set(gca,'xtick',[1:length(subjectsAll)],'XTickLabels',subjectsAll)
% xlim([0 length(subjectsAll)+1])
% xtickangle(45) 
% ylim([0 45])
% xlabel('Subject');
% ylabel('Euclidean Distance, arcmin (10% Span)')
% title('All Trials')
% saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/EucDistAndSpan.png');
% saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/EucDistAndSpan.png');

%%
% figure;
% errorbar(find(params.control == 0), timeIntervalLevel.eucDist(params.control == 0,11),zeros(size(find(params.control == 0))),timeIntervalLevel.span25(params.control == 0,11), 'o');
% hold on
% errorbar(find(params.control == 1), timeIntervalLevel.eucDist(params.control == 1,11),zeros(size(find(params.control == 1))), timeIntervalLevel.span25(params.control == 1,11), 'o');
% set(gca,'xtick',[1:length(subjectsAll)],'XTickLabels',subjectsAll)
% xlim([0 length(subjectsAll)+1])
% xtickangle(45) 
% ylim([0 45])
% xlabel('Subject');
% ylabel('Euclidean Distance, arcmin (10% Span)')
% title('All Trials')
% saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/EucDistAndSpan.png');
% saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/EucDistAndSpan.png');





    function plotAcrossTime(params,span,time,meanAXY,c)
        
        for ii = 1:size(meanAXY)
            %             if params.control(ii) == 1
            %                 plot(time,meanAXY(ii,:),'--*',...
            %                     'Color',[c(ii,1) c(ii,2) c(ii,3)],...
            %                     'LineWidth',4);
            %                 hold on;
            %             else
            errorbar(time,meanAXY(ii,:),span(ii,:),'-o',...
                'Color',[c(ii,1) c(ii,2) c(ii,3)],...
                'LineWidth',4);
            hold on;
            %                 errorbar(
            %             end
        end
        xticks(time)
        %         xticklabels(fieldnames(bySampleX))
        %         ylabel('Abs. Mean Distance')
        %         xlabel('Sample Bin');
        %
    end
    function leg = plotCombineTime(params,subNum,meanCOM,stdCOM,c,absolute)
        for ii = 1:subNum
            if absolute
                yneg = 0;
                ypos = stdCOM(ii)/2;
                xneg = 0;
                xpos = 0;
            else
                yneg = stdCOM(ii)/2;
                ypos = stdCOM(ii)/2;
                xneg = 0;
                xpos = 0;
            end
            errorbar(ii, meanCOM(ii), yneg,ypos,xneg,xpos, 'LineStyle','none','Color','k');
            if params.control(ii) == 1
                
                hold on
                leg(ii) = scatter(ii,meanCOM(ii),200,[0 0 0],'filled');
            else
                %                 errorbar(ii, meanCOM(ii), stdCOM(ii), 'LineStyle','none','Color','k');
                
                hold on
                leg(ii) = scatter(ii,meanCOM(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
            end
            hold on
        end
    end
end















