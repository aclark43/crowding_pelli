function [nXValues, nYValues, counterX, counterY] = posValueLimit(xValues, yValues)
% Removes any values that are out of boundes (hard coded to 100)

nXValues = [];
nYValues = [];
counterX = 1;
counterY = 1;
for ii = 1:length(xValues)
    if ~isempty(xValues{ii})
        nXValues = [nXValues xValues{ii}];
        counterX = counterX + 1;
    end
    if ~isempty(yValues{ii})
        nYValues = [nYValues yValues{ii}];
        counterY = counterY +1;
    end
end

for xIdx = 1:length(nXValues)
   if nXValues(xIdx) > 100 || nXValues(xIdx) < -100
       nXValues(xIdx) = NaN;
   end
   if nYValues(xIdx) > 100 ||  nYValues(xIdx) < -100
       nYValues(xIdx) = NaN;
   end
end
end