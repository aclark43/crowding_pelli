function   pptrials = cleanNEyerisTrials(pptrials)
     
     
     for ii = 1:length(pptrials)
         figure('position',[200, 100, 1500, 800])
         plot(1:length(pptrials{ii}.x), pptrials{ii}.x);
         hold on
          plot(1:length(pptrials{ii}.y), pptrials{ii}.y);
          ylim([-40 40])
         hold on
         set(gca, 'FontSize', 12)
         poiMS = [1 0 0];
         poiS = [1 0 0];
         poiD = [0 1 0];
         poiN = [0 0 0];
         poiI = [0 .423 .521];
         poiB = [.749 .019 1];
         hold on
         poiMS = plotColorsOnTraces(pptrials, ii, 'microsaccades', [1 0 0],1); %red
         poiS = plotColorsOnTraces(pptrials, ii, 'saccades', [1 0 0], 1); %red
         poiD = plotColorsOnTraces(pptrials, ii, 'drifts', [0 1 0], 1); %green
         poiN = plotColorsOnTraces(pptrials, ii, 'notracks', [0 0 0], 1); %black
         poiI = plotColorsOnTraces(pptrials, ii, 'invalid', [0 0 1], 1); %blue
         poiB = plotColorsOnTraces(pptrials, ii, 'blinks', [.749 .019 1], 1); %pink
         
     end
