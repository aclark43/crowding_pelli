function [flankerOrientations, pos] = getFlankerOrientations(trial)
% right is 1, left is 0
if trial.FlankerType < 1 % no flankers
    flankerOrientations = [];
    pos = [];
    return;
end

%% this should match the dimension in the C++ code
flankerDist = trial.FlankerDist;
optoDim = 5;
switch trial.FlankerType
    case 1 % horizontal and vertical flankers
        xFlankers = [-(optoDim + flankerDist * optoDim),...
            optoDim + flankerDist * optoDim, 0, 0 ];
        yFlankers = [0, 0, -(optoDim + flankerDist * optoDim),...
            optoDim + flankerDist * optoDim ];
    case 2 % horizontal only flankers
        xFlankers = [ -(optoDim + flankerDist * optoDim),...
            optoDim + flankerDist * optoDim ];
        yFlankers = [ 0, 0 ];
    case 3 % vertical only flankers
        xFlankers = [ 0, 0 ];
        yFlankers = [ -(optoDim + flankerDist * optoDim),...
            optoDim + flankerDist * optoDim ];
end
pos = floor([xFlankers(:), yFlankers(:)] * trial.TargetStrokewidth);

%% what are their orientations
flankerOrientations = nan(size(pos, 1), 1);
fo = pptrials.FlankerOrientations;
for i = length(flankerOrientations):-1:1
    flankerOrientations(i) = rem(fo, 10);
    fo = floor(fo / 10);
end
end