runDPIvsDDPI

%% Params
close all
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.fig = struct(...
    'FIGURE_ON', 0,...
    'HUX', 0,...
    'FOLDED', 1);
fig = params.fig;

% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,'Z084','Z014'}; %Drift Only Subjects,'Z084'

subjectsAll = {'Z002DDPI','AshleyDDPI','Z046DDPI','Z084DDPI','Zoe','Z091'};
% ecc = {'0ecc','10eccNasal','10eccTemp','15eccNasal','15eccTemp','25eccNasal','25eccTemp'};
% eccNames = {'Center0ecc','Nasal10ecc','Temp10ecc','Nasal15ecc','Temp15ecc','Nasal25ecc','Temp25ecc'};
% 
% subjectsAll = {'Z091'};
% ecc = {'0eccEcc','10ecc','15ecc','25ecc','40ecc','60ecc'};
% eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc','Side40ecc','Side60ecc'};
% stabilization = {'Stabilized'};
% conditions = {'Uncrowded'};


% subjectsAll = {'Zoe','Z002DDPI','AshleyDDPI','Z046DDPI','Z084DDPI','Z091'};
ecc = {'0eccEcc','10ecc','15ecc','25ecc'};
eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc'};
stabilization = {'Stabilized','Unstabilized'};
conditions = {'Uncrowded','Crowded'};
params.em = {'Drift'};
em = params.em;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
% c = jet(length(subjectsAll));
% c = brewermap(12,'Accent');
c = brewermap(12,'Dark2');

% params.c = c;

%% Load Variables
for condIdx = 1:length(conditions)
    for ii = 1:length(ecc)
        for stabilIdx = 1:length(stabilization)
            singleEcc = (eccNames{ii});
            numberSingleEcc = cell2mat(regexp(singleEcc, '\d+', 'match'));
            numberSingleEccString = sprintf('ecc_%s',numberSingleEcc);
            condition = {conditions{condIdx}};
            params.stabil = stabilization{stabilIdx};
            
            [ subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc), ...
                subjectMS.(singleEcc),...
                perform.(conditions{condIdx}).(params.stabil).(singleEcc)] = ...
                loadVariablesEcc(subjectsAll, condition, params, 0, numberSingleEccString, ecc{ii});
            for ss = 1:length(subjectsAll)
                subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc)(ss).name = ...
                    subjectsAll{ss};
                 subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc)(ss).nameIdx = ...
                    ss;
            end
        end
    end
end

ogSubjects = {'Z002DDPI','AshleyDDPI','Z046DDPI','Z084DDPI'};
dcThreshOG.AshleyDDPI = 4.8931;
dcThreshOG.Z002DDPI = 15.6098;
dcThreshOG.Z046DDPI = 18.5418;
dcThreshOG.Z084DDPI = 14.4799;

%2, 4 8 9
dcSTDOG.AshleyDDPI = 0.244597385795577;
dcSTDOG.Z002DDPI = 0.711774137990326;
dcSTDOG.Z046DDPI = 2.33349133370307;
dcSTDOG.Z084DDPI = 1.12836474143644;

for condIdx = 1:length(conditions)
    for ii = 1:length(ecc)
        for stabilIdx = 1:length(stabilization)
            singleEcc = (eccNames{ii});
            numberSingleEcc = cell2mat(regexp(singleEcc, '\d+', 'match'));
            params.stabil = stabilization{stabilIdx};
            [~,numSubs] = size(subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc));
            cond = conditions{condIdx};
            if strcmp(singleEcc,'Center0ecc')
                thresholdSWVals.(conditions{condIdx}).(params.stabil).(singleEcc) = determineThresholdSWEcc...
                    (subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    cond, numSubs, 'ecc_0');
            else
                thresholdSWVals.(conditions{condIdx}).(params.stabil).(singleEcc) = determineThresholdSWEcc...
                    (subjectCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    subjectThreshCond.(conditions{condIdx}).(params.stabil).(singleEcc),...
                    cond, numSubs, sprintf('ecc_%s',numberSingleEcc));
            end
            
            for i = 1:length(subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc))
                allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).subject =...
                    subjectsAll{i};
                allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) =...
                    subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).thresh;

                allValuesThresholdsBoots.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) = ...
                    subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).threshB;
%                 [upperY, lowerY] = confInter95(subjectThreshCondition(ii).bootsAll);
%             errorbar(mDSQ(ii), thresholds(ii), upperY, lowerY, 'Color', c(ii,:));
%             hold on
                if ~isempty( allValuesThresholds.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc))
                    allValuesNumTrials.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) =...
                        sum(subjectThreshCond.(conditions{condIdx}).(stabilization{stabilIdx}).(singleEcc)(i).em.valid);
                else
                    allValuesNumTrials.(conditions{condIdx}).(stabilization{stabilIdx})(i).(singleEcc) = [];
                end
            end
        end
    end
end

%% Refmorat Variables AGAIN
for ee = 1:length(ecc)
    singleEcc = (eccNames{ee});
    for crow = 1:2
        %     actualTarget = (str2double(cell2mat(regexp(eccNames{ee},'\d*','Match'))));
%         if ee == 1
%             nameEcc = ('ecc_0');
%         else
%             nameEcc = ('ecc_0');
%         end
        
        if crow == 1
            condition = 'Uncrowded';
        else
            condition = 'Crowded';
        end
        for ii = 1:subNum
            temp = fieldnames(subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).em);
            nameEcc = temp{13};

            subject = subjectsAll{ii};
            
            swUnc = thresholdSWVals.(condition).Unstabilized.(singleEcc)(ii).vals;
            path = subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).em.(nameEcc).(swUnc);
            
            dataSummaries.(condition).Subject(ee,ii) = {subject};
            dataSummaries.(condition).Span(ee,ii) = mean(path.span);
            dataSummaries.(condition).DC(ee,ii) = path.dCoefDsq;
            dataSummaries.(condition).Area(ee,ii) =  path.areaCovered;
            dataSummaries.(condition).ThresholdSW(ee,ii) = (subjectThreshCond.(condition).Unstabilized.(singleEcc)(ii).thresh);
            dataSummaries.(condition).order{ee,ii} = singleEcc;
            
            ogCompare.(condition).(subjectsAll{ii}).DC = path.dCoefDsq;
%             if ii == 1 && ee == 4
%                 swCro = 'strokeWidth_10';
%             else
%                 swCro = thresholdSWVals.(singleEcc)(ii).thresholdSWCrowded;
%             end
%             thresholdCrowded.Subject(ee,ii) = {subject};
%             thresholdCrowded.Span(ee,ii) = subjectThreshCro.(singleEcc)(ii).em.(nameEcc).(swCro).meanSpan;
%             thresholdCrowded.DC(ee,ii) = subjectThreshCro.(singleEcc)(ii).em.(nameEcc).(swCro).dCoefDsq;
%             thresholdCrowded.Area(ee,ii)=  subjectThreshCro.(singleEcc)(ii).em.(nameEcc).(swCro).areaCovered;
%             thresholdCrowded.ThresholdSW(ee,ii) = (subjectThreshCro.(singleEcc)(ii).thresh);
%             thresholdCrowded.order{ee,ii} = singleEcc;
%             ogCompareC.(subjectsAll{ii}).DC = subjectThreshCro.(singleEcc)(ii).em.(nameEcc).(swCro).dCoefDsq;
        end
    end
    
end