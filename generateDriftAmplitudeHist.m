function generateDriftAmplitudeHist(driftStruct, sw, title_str, trialChar)
%Histogram of Drift and Amplitude
%   Graphs Amplitude of Drift per trial (designed to subplot with Span)

    histSpan = (driftStruct);
    h = histogram(histSpan,100);
    xlim([0 10])
    %     h.BinEdges = single((sprintf('%s', params.spanMin):(sprintf('%s', params.spanMax))));
%     set(gcf,'units','normalized','outerposition',[0 0 1 1])
    graphTitle = sprintf('Drift Amplitude %s for SW %i', title_str, sw);
    title(graphTitle,'Interpreter','none');
    
    xlabel('Amplitude (arcmin)');
    ylabel('Number of Trials');
    set(gcf, 'Color', 'w');
    set(gca, 'FontSize', 16, 'FontWeight', 'bold');
    
   
    
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.epsc', trialChar.Subject, graphTitle));
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.jpeg', trialChar.Subject, graphTitle));
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.fig', trialChar.Subject, graphTitle));
    
end

