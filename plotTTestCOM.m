function plotTTestCOM(subjectsAll,com,comCAT)
[chronic, numTrain] = huxlinChronAcuteList (subjectsAll);
figure;
% subplot(1,3,1)
plot(ones(1,length(com.all(chronic == 1 & numTrain > 1))),...
    com.all(chronic == 1& numTrain > 1),'o',...
    'MarkerSize',15,'LineWidth',3,'MarkerFaceColor','b');
hold on
errorbar(1.3, mean(com.all(chronic == 1 & numTrain > 1)), ...
    sem(com.all(chronic == 1& numTrain > 1)),...
    'o','Color','b')

plot(ones(1,length(com.all(chronic == 1 & numTrain == 1))),...
    com.all(chronic == 1& numTrain == 1),'o',...
    'MarkerSize',15,'LineWidth',3','MarkerFaceColor','r');
hold on
errorbar(1.4, mean(com.all(chronic == 1 & numTrain == 1)), ...
    sem(com.all(chronic == 1& numTrain == 1)),...
    'o','Color','r')

errorbar(1.35, mean(com.all(chronic == 1)), ...
    sem(com.all(chronic == 1)),...
    'o','Color','g')

plot(zeros(1,length(com.all(chronic == 0))),com.all(chronic == 0),'o',...
    'MarkerSize',15,'LineWidth',3');
hold on
errorbar(0.3, mean(com.all(chronic == 0)), sem(com.all(chronic == 0)),...
    'o','Color','k')

plot(ones(1,length(com.all(chronic == 2)))*2,com.all(chronic == 2),'o',...
    'MarkerSize',15,'LineWidth',3);
errorbar(2.3, nanmean(com.all(chronic == 2)), sem(com.all(chronic == 2)),...
    'o','Color','k')
for i = 1:length(chronic)
    text(chronic(i)-.05,double(com.all(i)),char(string(numTrain(i))),'FontSize',14);
end
%     title(sprintf('p = %.3f',p));
xticks([0 1 2])
xticklabels({'SubAcute','Chronic','Controls'})
xlim([-.5 2.5]);
ylabel('COM');

[~,pChronic] = ttest2(com.all(chronic == 1 & numTrain > 1),comCAT.all(chronic == 2));
[~,pSAcute] = ttest2(com.all(chronic == 0),comCAT.all(chronic == 2));
[~,pChronic_LowTrain] = ttest2(com.all(chronic == 1 & numTrain == 1),comCAT.all(chronic == 2));
[~,pChronic_ALL] = ttest2(com.all(chronic == 1),comCAT.all(chronic == 2));
[~,pChronic_Sub] = ttest2(com.all(chronic == 1),com.all(chronic == 0));
[~,pChronic_TrainWithin] = ttest2(com.all(chronic == 1 & numTrain == 1),...
    com.all(chronic == 1 & numTrain > 1));


ylim([0 30])

line([.8 .8],[5 15])
h = text(.75,7,sprintf('p = %.3f',pChronic_TrainWithin),'Color','k');
set(h,'Rotation',90);
 
line([1 2],[27 27])
text(1.5,27.75,sprintf('p = %.3f',pChronic_ALL),'Color','g');

line([0 2],[24 24])
text(1.5,24.75,sprintf('p = %.3f',pSAcute));

line([1 2],[22.2 22.2])
text(1,22.75,sprintf('p = %.3f',...
    pChronic_LowTrain),'Color','r');
text(1.5,22.75,sprintf('p = %.3f',...
    pChronic),'Color','b');

line([0 1],[20 20])
text(0.5,20.75,sprintf('p = %.3f',...
    pChronic_Sub));
title('COM Trial Level');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/COMChronicSubTrial.png');

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%

% [chronic, numTrain] = huxlinChronAcuteList (subjectsAll);
figure;
% subplot(1,3,2)

plot(ones(1,length(comCAT.all(chronic == 1 & numTrain > 1))),...
    comCAT.all(chronic == 1& numTrain > 1),'o',...
    'MarkerSize',15,'LineWidth',3,'MarkerFaceColor','b');
hold on
errorbar(1.3, mean(comCAT.all(chronic == 1 & numTrain > 1)), ...
    sem(comCAT.all(chronic == 1& numTrain > 1)),...
    'o','Color','b')

plot(ones(1,length(comCAT.all(chronic == 1 & numTrain == 1))),...
    comCAT.all(chronic == 1& numTrain == 1),'o',...
    'MarkerSize',15,'LineWidth',3','MarkerFaceColor','r');
hold on
errorbar(1.4, mean(comCAT.all(chronic == 1 & numTrain == 1)), ...
    sem(comCAT.all(chronic == 1& numTrain == 1)),...
    'o','Color','r')

errorbar(1.4, mean(comCAT.all(chronic == 1)), ...
    sem(comCAT.all(chronic == 1)),...
    'o','Color','g')

plot(zeros(1,length(comCAT.all(chronic == 0))),comCAT.all(chronic == 0),'o',...
    'MarkerSize',15,'LineWidth',3');
hold on
errorbar(0.3, mean(comCAT.all(chronic == 0)), sem(comCAT.all(chronic == 0)),...
    'o','Color','k')

plot(ones(1,length(comCAT.all(chronic == 2)))*2,comCAT.all(chronic == 2),'o',...
    'MarkerSize',15,'LineWidth',3);
errorbar(2.3, nanmean(comCAT.all(chronic == 2)), sem(comCAT.all(chronic == 2)),...
    'o','Color','k')
for i = 1:length(chronic)
    text(chronic(i)-.05,double(comCAT.all(i)),char(string(numTrain(i))),'FontSize',14);
end
%     title(sprintf('p = %.3f',p));
xticks([0 1 2])
xticklabels({'SubAcute','Chronic','Controls'})
xlim([-.5 2.5]);
ylabel('COM');

[~,pChronic] = ttest2(comCAT.all(chronic == 1 & numTrain > 1),comCAT.all(chronic == 2));
[~,pSAcute] = ttest2(comCAT.all(chronic == 0),comCAT.all(chronic == 2));
[~,pChronic_LowTrain] = ttest2(comCAT.all(chronic == 1 & numTrain == 1),comCAT.all(chronic == 2));
[~,pChronic_ALL] = ttest2(comCAT.all(chronic == 1),comCAT.all(chronic == 2));
[~,pChronic_Sub] = ttest2(comCAT.all(chronic == 1),comCAT.all(chronic == 0));

ylim([0 30])

line([1 2],[27 27])
text(1.5,27.75,sprintf('p = %.3f',pChronic_ALL),'Color','g');

line([0 2],[24 24])
text(1.5,24.75,sprintf('p = %.3f',pSAcute));

line([1 2],[22.2 22.2])
text(1,22.75,sprintf('p = %.3f',...
    pChronic_LowTrain),'Color','r');
text(1.5,22.75,sprintf('p = %.3f',...
    pChronic),'Color','b');

line([0 1],[20 20])
text(0.5,20.75,sprintf('p = %.3f',...
    pChronic_Sub));
title('COM CAT Level');

saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/COMChronicSubCAT.png');

%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%
% [chronic, numTrain] = huxlinChronAcuteList (subjectsAll);

figure;
% subplot(1,3,3)

plot(ones(1,length(comCAT.all(chronic == 1 | chronic == 0))),...
    comCAT.all(chronic == 1| chronic == 0),'o',...
    'MarkerSize',15,'LineWidth',3);
hold on
errorbar(1.3, mean(comCAT.all(chronic == 1 | chronic == 0)), ...
    sem(comCAT.all(chronic == 1 | chronic == 0)),...
    'o','Color','k')

plot(ones(1,length(comCAT.all(chronic == 2)))*2,comCAT.all(chronic == 2),'o',...
    'MarkerSize',15,'LineWidth',3);
errorbar(2.3, nanmean(comCAT.all(chronic == 2)), sem(comCAT.all(chronic == 2)),...
    'o','Color','k')
for i = 1:length(chronic)
    text(chronic(i)-.05,double(comCAT.all(i)),char(string(numTrain(i))),'FontSize',14);
end
%     title(sprintf('p = %.3f',p));
xticks([1 2])
xticklabels({'Patient','Controls'})
xlim([.5 2.5]);
ylabel('COM');

[~,pChronic_ALL] = ttest2(comCAT.all(chronic == 1 | chronic == 0),comCAT.all(chronic == 2));

ylim([0 30])

line([1 2],[27 27])
text(1.5,27.75,sprintf('p = %.3f',pChronic_ALL),'Color','k');

title('COM CAT Level');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/COMPatientCAT.png');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
% subplot(1,3,3)

plot(ones(1,length(com.all(chronic == 1 | chronic == 0))),...
    com.all(chronic == 1| chronic == 0),'o',...
    'MarkerSize',15,'LineWidth',3);
hold on
errorbar(1.3, mean(com.all(chronic == 1 | chronic == 0)), ...
    sem(com.all(chronic == 1 | chronic == 0)),...
    'o','Color','k')

plot(ones(1,length(com.all(chronic == 2)))*2,com.all(chronic == 2),'o',...
    'MarkerSize',15,'LineWidth',3);
errorbar(2.3, nanmean(com.all(chronic == 2)), sem(com.all(chronic == 2)),...
    'o','Color','k')
for i = 1:length(chronic)
    text(chronic(i)-.05,double(com.all(i)),char(string(numTrain(i))),'FontSize',14);
end
%     title(sprintf('p = %.3f',p));
xticks([1 2])
xticklabels({'Patient','Controls'})
xlim([.5 2.5]);
ylabel('COM');

[~,pChronic_ALL] = ttest2(com.all(chronic == 1 | chronic == 0),com.all(chronic == 2));

ylim([0 30])

line([1 2],[27 27])
text(1.5,27.75,sprintf('p = %.3f',pChronic_ALL),'Color','k');

title('COM Trial Level');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/COMPatientTrial.png');



