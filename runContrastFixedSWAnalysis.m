clear all
close all
clc

data = loadInIndividualPPTrials();
%% For V5 Analysis
conditionNames = {'temp25ecc'};%'center0ecc',
for i = 1:length(conditionNames)
%     pathway = data.OpenMarker50ms.Uncrowded.(conditionNames{i});
      pathway = data.ClosedMarker500.Uncrowded.(conditionNames{i});

%     counter = 1;
%     for ii = 1:length(pathway)
%         sizeTemp = pathway{ii}.pixelAngle * double(pathway{ii}.TargetStrokewidth) * 2;
%         if round(sizeTemp,1) == 1.5
%             v3Data(i).performanceStim(counter) = pathway{ii}.Correct;
%             v3Data(i).sizeStim(counter) = sizeTemp;
%             counter = counter + 1;
%         end
%     end
    
    for ii = 1:length(pathway)
        performance(ii) = pathway{ii}.Correct;
        size(ii) = pathway{ii}.pixelAngle * double(pathway{ii}.TargetStrokewidth) * 2;
    end
    idx = performance ~= 3;
    xl = [0.1, 11];
    yl = [0, 1.05];
    gamma = 0.25;
    figure;
    [thresh, ~, boots, ~, ~] = psyfitCrowding(size(idx), performance(idx), 'DistType', 'Normal',...
        'Xlim', xl, 'Ylim', yl, 'Boots', 1000,...
        'Chance', gamma, 'Extra');
    v5Data(i).thresh = thresh;
    [upperY, lowerY] = confInter95(boots);
    v5Data(i).upperInterval = upperY;
    v5Data(i).lowerInterval = lowerY;
    clear performance; clear size;
    title(conditionNames{i});
%     saveas(gcf, sprintf('../../Data/AshleyDDPIMaxContrastShortTime/Graphs/JPEG/%sPsychoFit',...
%             conditionNames{i}));    
end

% for i = 1:length(eccNames)
    [upperY, lowerY] = confInter95(boots);
% end
%% For V3 Analysis
% conditionNames = {'center0ecc','temp15ecc','temp25ecc'};
% for i = 1:length(conditionNames)
%     pathway = data.OpenMarker.Uncrowded.(conditionNames{i});
% %     counter = 1;
% %     for ii = 1:length(pathway)
% %         sizeTemp = pathway{ii}.pixelAngle * double(pathway{ii}.TargetStrokewidth) * 2;
% %         if round(sizeTemp,1) == 1.5
% %             v3Data(i).performanceStim(counter) = pathway{ii}.Correct;
% %             v3Data(i).sizeStim(counter) = sizeTemp;
% %             counter = counter + 1;
% %         end
% %     end
%     
%     for ii = 1:length(pathway)
%         performance(ii) = pathway{ii}.Correct;
%         size(ii) = pathway{ii}.pixelAngle * double(pathway{ii}.TargetStrokewidth) * 2;
%     end
%     idx = performance ~= 3;
%     xl = [0.1, 11];
%     yl = [0, 1.05];
%     gamma = 0.25;
%     figure;
%     [thresh, ~, boots, ~, ~] = psyfitCrowding(size(idx), performance(idx), 'DistType', 'Normal',...
%         'Xlim', xl, 'Ylim', yl, 'Boots', 1000,...
%         'Chance', gamma, 'Extra');
%     v3Data(i).thresh = thresh;
%     [upperY, lowerY] = confInter95(boots);
%     v3Data(i).upperInterval = upperY;
%     v3Data(i).lowerInterval = lowerY;
%     clear performance; clear size;
%     title(conditionNames{i});
% saveas(gcf, sprintf('../../Data/Ashley_OpenMarker/Graphs/JPEG/%sPsychoFit',...
%             conditionNames{i}));    
% end
% clear temp
temp.thresholds = [1.24331008736037;1.33497334808067;1.51027780217859];
temp.upperInterval = [0.235781980482278;0.132833356275724;0.536974728769698];
temp.lowerInterval =[0.326895632789529;0.102650499974612;0.395375465680140];
for i = 1:3
    v3Data(i).thresh = temp.thresholds(i);
    v3Data(i).upperInterval = temp.upperInterval(i);
    v3Data(i).lowerInterval = temp.lowerInterval(i);
end

figure;
leg(1) = errorbar([0 15 25],[v3Data(:).thresh],...
    [v3Data(:).upperInterval], [v3Data(:).lowerInterval],'-o','LineWidth',4);
ogData.thresh = [1.32176429344889	1.60455698068060	1.67826041482044	1.91032096661271]; %without peripheral fixation marker
ogData.upperInterval = [0.196107148210092	0.152581967864895	0.211683575287993	1.43907990848454];
ogData.lowerInterval = [0.259584001548109	0.203454635543675	0.315866939790320	1.46172210085771];
hold on;
leg(2) = errorbar([0 10 15 25],ogData.thresh,ogData.upperInterval,ogData.lowerInterval,...
    '-o','LineWidth',4);
xlim([-2 28])
legend(leg,{'New Larger Marker','Old Marker'});
xlabel('Eccentricity');
ylabel('Threshold');

%% For V4 Analysis
conditionNames = {'center0ecc','temp15ecc','temp25ecc'};
for i = 1:length(conditionNames)
    pathway = data.diffContrast_PEST.LargeMarker.Uncrowded.(conditionNames{i}).em;
v4Data(i).thresh = pathway.thresh;
v4Data(i).upperInterval = pathway.upperInterval;
v4Data(i).lowerInterval = pathway.lowerInterval;

end
hold on
leg(3) = errorbar([0 15 25],[v4Data(:).thresh],...
    [v4Data(:).upperInterval], [v4Data(:).lowerInterval],'-o','LineWidth',4);

%%%add for shorter time period
leg(4) = errorbar([0 15],[v5Data(:).thresh],...
    [v5Data(:).upperInterval], [v5Data(:).lowerInterval],'-o','LineWidth',4);

legend(leg,{'New Larger Marker',...
    'Old Marker',...
    'New Larger Marker, Low Contrast',...
    'Larger Marker, High Contrast, 50ms Time'});
xlabel('Eccentricity');
ylabel('Threshold');
saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\CrowdingTopology\Documents\Overleaf_ContrastTestingCrowding\figures\OpenMarkerContrastTesting.png');
saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\CrowdingTopology\Documents\Overleaf_ContrastTestingCrowding\figures\OpenMarkerContrastTesting.epsc');


%% Playing with figure appearence
% % % % add some astrix for signif = 95% confidence bounds don't include the mean
% % % optionsCompares = {...
% % %         ogData,...
% % %         v3Data,...
% % %         v4Data};
% % %     
% % % allcombos = combntns(1:3,2);
% % % 
% % % % for ecc = 1:3
% % % %     figure;
% % % %     errorbar([0],[v3Data(1).thresh],...
% % % %         [v3Data(1).upperInterval], [v3Data(1).lowerInterval],'-o','LineWidth',5);
% % % %     hold on;
% % % %     errorbar([0],ogData.thresh(1),ogData.upperInterval(1),ogData.lowerInterval(1),...
% % % %         '-o','LineWidth',5);
% % % %     
% % % %     temp = range_intersection([ogData.upperInterval(1)+ogData.thresh(1) ogData.thresh(1)-ogData.lowerInterval(1)], ...
% % % %         [v3Data(1).thresh+v3Data(1).upperInterval v3Data(1).thresh-v3Data(1).lowerInterval])
% % % %     plot([0.1 0.1],[temp(1) temp(2)],'-o','LineWidth',5);
% % % % 
% % % % end
% % % %%% UN PAIRED TTESTS between OG vs V3 vs V$
% % % % Get the same sized stimuli
% % % stimSize = 1.5; %in arcminutes
% % % 
% % % vOG.ecc0.performance = data.OG.Uncrowded.center0ecc.em.ecc_0.strokeWidth_5.correct;
% % % vOG.ecc15.performance = data.OG.Uncrowded.temp15ecc.em.ecc_15.strokeWidth_4.correct;
% % % vOG.ecc25.performance = data.OG.Uncrowded.temp25ecc.em.ecc_25.strokeWidth_2.correct;
% % % 
% % % v3.ecc0.performance = v3Data(1).performanceStim(v3Data(1).performanceStim ~= 3);
% % % v3.ecc15.performance = v3Data(2).performanceStim(v3Data(2).performanceStim ~= 3);
% % % v3.ecc25.performance = v3Data(2).performanceStim(v3Data(3).performanceStim ~= 3);
% % % 
% % % v4.ecc0.performance = data.diffContrast_PEST.LargeMarker.Uncrowded.center0ecc.em.ecc_0.strokeWidth_1.correct;
% % % v4.ecc15.performance = data.diffContrast_PEST.LargeMarker.Uncrowded.temp15ecc.em.ecc_15.strokeWidth_2.correct;
% % % v4.ecc25.performance = data.diffContrast_PEST.LargeMarker.Uncrowded.temp25ecc.em.ecc_25.strokeWidth_3.correct;
% % % 
% % % allcombs = combntns([1:3],2);
% % % eccNames = {...
% % %     'ecc0',...
% % %     'ecc15',...
% % %     'ecc25'};
% % % for ii = 1:length(eccNames)
% % %     optionsCompares = {...
% % %         vOG.(eccNames{ii}).performance,...
% % %         v3.(eccNames{ii}).performance,...
% % %         v4.(eccNames{ii}).performance};
% % %     for i = 1:3
% % %         ttests(i).compared = sprintf('V%i_VS_V%i',allcombs(i,1),allcombs(i,2));
% % %         [~, ttests(i).(eccNames{ii})] = ttest2(...
% % %             double(optionsCompares{allcombs(i,1)}),...
% % %             double(optionsCompares{allcombs(i,2)}));
% % %     end
% % % end
% % % 
% % % dataCompares = {vOG,v3,v4};
% % % figure;
% % % clear leg
% % % for i = 1:3
% % % leg(i) = errorbar([1 15 25],[mean(dataCompares{i}.ecc0.performance),...
% % %     mean(dataCompares{i}.ecc15.performance),...
% % %     mean(dataCompares{i}.ecc25.performance)],...
% % %     [sem(double(dataCompares{i}.ecc0.performance)),...
% % %     sem(double(dataCompares{i}.ecc15.performance)),...
% % %     sem(double(dataCompares{i}.ecc25.performance))],'-o');
% % % hold on
% % % end
% % % legend(leg,{'V1 vOG','V2 LGMarker','V3 LGMarker,Low Contrast'})
% % % 



%% For V2 Analysis
conditionNames = {'center0ecc','temp10ecc','temp15ecc','temp25ecc'};
for ii = 1:4 %%large marker
    path = data.diffContrast.LargeMarker.Uncrowded.(conditionNames{ii}).em;
    allNames = fieldnames(path);
    idxForEntry = startsWith(allNames,'ecc_');
    perf.LargeMarker(ii) = path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.performanceAtSize;
    sd.LargeMarker(ii) = sem(path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.correct);
    numTrials.LargeMarker(ii) = char(length(path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.correct));
end

conditionNames = {'center0ecc','temp10ecc','temp25ecc'};
for ii = 1:3 %%large marker
    path = data.diffContrast.Smallmarker.Uncrowded.(conditionNames{ii}).em;
    allNames = fieldnames(path);
    idxForEntry = startsWith(allNames,'ecc_');
    perf.SmallMarker.Uncrowded(ii) = path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.performanceAtSize;
    sd.SmallMarker.Uncrowded(ii) = sem(path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.correct);
    numTrials.SmallMarker.Uncrowded(ii) = char(length(path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.correct));
end

conditionNames = {'center0ecc','temp10ecc','temp25ecc'};
for ii = 1:3 %%large marker
    path = data.diffContrast.Smallmarker.Crowded.(conditionNames{ii}).em;
    allNames = fieldnames(path);
    idxForEntry = startsWith(allNames,'ecc_');
    perf.SmallMarker.Crowded(ii) = path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.performanceAtSize;
    sd.SmallMarker.Crowded(ii) = sem(path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.correct);
    numTrials.SmallMarker.Crowded(ii) = char(length(path.(cell2mat(allNames(idxForEntry))).strokeWidth_1.correct));
end

figure('Position',[2200 180 1200 800],'Name','MaxContrast');
leg(1) = errorbar([1 2 3 4],perf.LargeMarker, sd.LargeMarker,'-o','LineWidth',4);
text([1-.1 2-.1 3-.1 4-.1], perf.LargeMarker,{numTrials.LargeMarker(1),numTrials.LargeMarker(2),...
    numTrials.LargeMarker(3), numTrials.LargeMarker(4)});
hold on

leg(2) = errorbar([1 2 4],perf.SmallMarker.Uncrowded, sd.SmallMarker.Uncrowded,'-o','LineWidth',4);
text([1-.1 2-.1 4-.1], perf.SmallMarker.Uncrowded,{numTrials.SmallMarker.Uncrowded(1),numTrials.SmallMarker.Uncrowded(2),...
    numTrials.SmallMarker.Uncrowded(3)});

leg(3) = errorbar([1 2 4],perf.SmallMarker.Crowded, sd.SmallMarker.Crowded,'-o','LineWidth',4);
text([1-.1 2-.1 4-.1], perf.SmallMarker.Crowded,{numTrials.SmallMarker.Crowded(1),numTrials.SmallMarker.Crowded(2),...
    numTrials.SmallMarker.Crowded(3)});


title('Max Contrast - White Stim/Black Back')
axis([.5 4.5 0 1])
line([.5 4.5],[.25 .25],'LineStyle','--','Color','k')
legend(leg,{'Uncrowded, 10arc Marker','Uncrowded, 5arc Marker', 'Crowded, 5arc Marker'});
set(gca,'XTick', [1 2 3 4]);
xticklabels({'center0ecc','temp10ecc','temp15ecc','temp25ecc'});
xlabel('Eccentricity(arcmin)');
ylabel('Performance');
% C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\CrowdingTopology\Documents\Overleaf_ContrastTestingCrowding\figures
saveas(gcf,'C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/CrowdingTopology/Documents/Overleaf_ContrastTestingCrowding/figures/MaxContrastTesting.png');

%% For V1 Analysis
types = fieldnames(data);

figure;
for ii = 1:size(types)-2
    if ii == 1
        correctVec = data.(types{ii}).em.ecc_0.strokeWidth_1.correct;
    else
        correctVec = data.(types{ii}).em.ecc_25.strokeWidth_1.correct;
    end
    performance = sum(correctVec)/length(correctVec);
    plot(ii, performance,'o','MarkerSize',15);
    text(ii-.1, performance, sprintf('%.0f',(length(correctVec))));
    hold on
end
xticks(1:size(types))
xticklabels(types)
axis([.5 4.5 .25 1.00]);
title('Ashley, Stabilized, Contrast 76, 1.9arcmin');
ylabel('Performance');
xlabel('Ecc')

%%%
figure;
leg1(1) = plot([1 2], ...
    [data.center.em.ecc_0.strokeWidth_1.performanceAtSize ...
    data.comb.em.ecc_25.strokeWidth_1.performanceAtSize],...
    '-o','MarkerSize',15);
hold on
leg1(2) = plot([1 2],...
    [data.centerUnst.em.ecc_0.strokeWidth_1.performanceAtSize ...
    data.tempUnst.em.ecc_25.strokeWidth_1.performanceAtSize],...
    '-o','MarkerSize',15);

xticks(1:2)
xticklabels({'Center','25ecc'})
axis([.5 2.5 .25 1.00]);
legend(leg1,{'Ashley, Stabilized, Contrast 78, 1.9arcmin',...
    'Ashley, NEW TASK, Unstabilized, Contrast 75, 1.9arcmin'});
ylabel('Performance');
xlabel('Ecc')

function data = loadInIndividualPPTrials()

data.center = load('MATFiles/AshleyContrast_Uncrowded_Stabilized_Drift_0ecc_DriftChar.mat');
data.temp = load('MATFiles/AshleyContrast_Uncrowded_Stabilized_Drift_25eccTemp_DriftChar.mat');
data.nasal = load('MATFiles/AshleyContrast_Uncrowded_Stabilized_Drift_25eccNasal_DriftChar.mat');
data.comb = load('MATFiles/AshleyContrast_Uncrowded_Stabilized_Drift_25ecc_DriftChar.mat');

data.centerUnst = load('MATFiles/AshleyContrast_Uncrowded_Unstabilized_Drift_0eccEcc_DriftChar.mat');
data.tempUnst = load('MATFiles/AshleyContrast_Uncrowded_Unstabilized_Drift_25eccTemp_DriftChar.mat');
%%%%%%%%%%%%%%%%%%%%%%%%%
%% V2 (Fixed Size)
data.diffContrast.LargeMarker.Uncrowded.center0ecc = load('MATFiles/AshleyLargerMarker_Uncrowded_Unstabilized_Drift_0eccEcc_DriftChar.mat');
data.diffContrast.LargeMarker.Uncrowded.temp10ecc = load('MATFiles/AshleyLargerMarker_Uncrowded_Unstabilized_Drift_10eccTemp_DriftChar.mat');
data.diffContrast.LargeMarker.Uncrowded.temp15ecc = load('MATFiles/AshleyLargerMarker_Uncrowded_Unstabilized_Drift_15eccTemp_DriftChar.mat');
data.diffContrast.LargeMarker.Uncrowded.temp25ecc = load('MATFiles/AshleyLargerMarker_Uncrowded_Unstabilized_Drift_25eccTemp_DriftChar.mat');

data.diffContrast.Smallmarker.Uncrowded.center0ecc = load('MATFiles/AshleyNewParadigm_5arcminMarker_Uncrowded_Unstabilized_Drift_0eccEcc_DriftChar.mat');
data.diffContrast.Smallmarker.Uncrowded.temp10ecc = load('MATFiles/AshleyNewParadigm_5arcminMarker_Uncrowded_Unstabilized_Drift_10eccTemp_DriftChar.mat');
data.diffContrast.Smallmarker.Uncrowded.temp25ecc = load('MATFiles/AshleyNewParadigm_5arcminMarker_Uncrowded_Unstabilized_Drift_25eccTemp_DriftChar.mat');
data.diffContrast.Smallmarker.Crowded.center0ecc = load('MATFiles/AshleyNewParadigm_5arcminMarker_Crowded_Unstabilized_Drift_0eccEcc_DriftChar.mat');
data.diffContrast.Smallmarker.Crowded.temp10ecc = load('MATFiles/AshleyNewParadigm_5arcminMarker_Crowded_Unstabilized_Drift_10eccTemp_DriftChar.mat');
data.diffContrast.Smallmarker.Crowded.temp25ecc = load('MATFiles/AshleyNewParadigm_5arcminMarker_Crowded_Unstabilized_Drift_25eccTemp_DriftChar.mat');

%% V3 Open Marker, Max Contrast (Varying Sizes)
temp = load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\Ashley_OpenMarker\Uncrowded_Unstabilized_0eccEcc\ses1\pptrials_noEyeTraces.mat');
data.OpenMarker.Uncrowded.center0ecc = temp.pptrials; clear temp;

temp = load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\Ashley_OpenMarker\Uncrowded_Unstabilized_15eccTemp\ses1\pptrials_noEyeTraces.mat');
data.OpenMarker.Uncrowded.temp15ecc = temp.pptrials; clear temp;

temp = load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\Ashley_OpenMarker\Uncrowded_Unstabilized_25eccTemp\ses1\pptrials_noEyeTraces.mat');
data.OpenMarker.Uncrowded.temp25ecc = temp.pptrials; clear temp;
%% V4 Open Marker, Low Contrast (Varying Sizes)
data.diffContrast_PEST.LargeMarker.Uncrowded.center0ecc = load('MATFiles/Ashley_OpenMarker_LowContrast_Uncrowded_Unstabilized_Drift_0eccEcc_DriftChar.mat');
data.diffContrast_PEST.LargeMarker.Uncrowded.temp15ecc = load('MATFiles/Ashley_OpenMarker_LowContrast_Uncrowded_Unstabilized_Drift_15eccTemp_DriftChar.mat');
data.diffContrast_PEST.LargeMarker.Uncrowded.temp25ecc = load('MATFiles/Ashley_OpenMarker_LowContrast_Uncrowded_Unstabilized_Drift_25eccTemp_DriftChar.mat');

temp = load('MATFiles/Ashley_OpenMarker_LowContrast_Uncrowded_Unstabilized_Drift_0eccEcc_Threshold.mat');
data.diffContrast_PEST.LargeMarker.Uncrowded.center0ecc.em.thresh = temp.threshInfo.thresh;
[upperY, lowerY] = confInter95(temp.threshInfo.threshB);
data.diffContrast_PEST.LargeMarker.Uncrowded.center0ecc.em.upperInterval = upperY;
data.diffContrast_PEST.LargeMarker.Uncrowded.center0ecc.em.lowerInterval = lowerY;
    
temp = load('MATFiles/Ashley_OpenMarker_LowContrast_Uncrowded_Unstabilized_Drift_15eccTemp_Threshold.mat');
data.diffContrast_PEST.LargeMarker.Uncrowded.temp15ecc.em.thresh = temp.threshInfo.thresh;
[upperY, lowerY] = confInter95(temp.threshInfo.threshB);
data.diffContrast_PEST.LargeMarker.Uncrowded.temp15ecc.em.upperInterval = upperY;
data.diffContrast_PEST.LargeMarker.Uncrowded.temp15ecc.em.lowerInterval = lowerY;

temp = load('MATFiles/Ashley_OpenMarker_LowContrast_Uncrowded_Unstabilized_Drift_25eccTemp_Threshold.mat');
data.diffContrast_PEST.LargeMarker.Uncrowded.temp25ecc.em.thresh = temp.threshInfo.thresh;
[upperY, lowerY] = confInter95(temp.threshInfo.threshB);
data.diffContrast_PEST.LargeMarker.Uncrowded.temp25ecc.em.upperInterval = upperY;
data.diffContrast_PEST.LargeMarker.Uncrowded.temp25ecc.em.lowerInterval = lowerY;

%% V OG Data
data.OG.Uncrowded.center0ecc = load('MATFiles/AshleyDDPI_Uncrowded_Unstabilized_Drift_0eccEcc_DriftChar.mat');
data.OG.Uncrowded.temp15ecc = load('MATFiles/AshleyDDPI_Uncrowded_Unstabilized_Drift_15ecc_DriftChar.mat');
data.OG.Uncrowded.temp25ecc = load('MATFiles/AshleyDDPI_Uncrowded_Unstabilized_Drift_25ecc_DriftChar.mat');

%%
%% V5 Open Marker, Max Contrast, 50ms Duration Time (Varying Sizes)
temp = load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\AshleyDDPIMaxContrastShortTime\0ecc_Time50ms\ses1\pptrials.mat');
data.OpenMarker50ms.Uncrowded.center0ecc = temp.pptrials; clear temp;

temp = load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\AshleyDDPIMaxContrastShortTime\15eccTemp_Time50ms\ses1\pptrials.mat');
data.OpenMarker50ms.Uncrowded.temp15ecc = temp.pptrials; clear temp;

%% Normal Condition, no eye traces
temp = load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\AshleyDDPI_NoTraces\Uncrowded_Unstabilized_0ecc\ses1\pptrials_noEyeTraces.mat');
data.ClosedMarker500.Uncrowded.center0ecc = temp.pptrials; clear temp;

temp = load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\AshleyDDPI_NoTraces\Uncrowded_Unstabilized_25eccTemp\ses1\pptrials_noEyeTraces.mat');
data.ClosedMarker500.Uncrowded.temp25ecc = temp.pptrials; clear temp;


end