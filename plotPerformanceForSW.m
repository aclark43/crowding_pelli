function plotPerformanceForSW( subjectsAll, subjectThreshCro, subjectThreshUnc, c, subNum)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:length(subjectsAll)
     swUncr(ii) = 1;
     swCro(ii) = 2;
     crowdedThresh(ii) = (subjectThreshCro(ii).threshSW4 *100);
     uncrowdedThresh(ii) = (subjectThreshUnc(ii).threshSW4 * 100);
 end

sz = 100;
figure;
scatter(swUncr,uncrowdedThresh,sz,c,'filled');
hold on
scatter(swCro,crowdedThresh,sz,c,'filled');
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[1 2],'xticklabel', names,'FontSize',15,...
       'FontWeight','bold')
ylabel('Performance','FontSize',15,'FontWeight','bold')
axis([0 3 20 100])

hold on;
x2 = 2+zeros(subNum,1)';
% scatter(x2,thresholdSize2,sz,c,'filled')
% hold on
% scatter(x2(1),mean(thresholdSize2),sz,'k','Linewidth',5);

xS = [1 2];


for ii = 1:length(subjectsAll)
    yMean = [uncrowdedThresh(1,ii) crowdedThresh(1,ii)];
    line(xS, yMean, 'Color',c(ii,:),'LineWidth',2);
    text(1.1,uncrowdedThresh(1,ii),sprintf('%.3f arcmin',...
        subjectThreshUnc(ii).actualSizeSW4),'Color',[c(ii,1) c(ii,2) c(ii,3)])
     text(2.1,crowdedThresh(1,ii),sprintf('%.3f arcmin',...
        subjectThreshCro(ii).actualSizeSW4),'Color',[c(ii,1) c(ii,2) c(ii,3)])
end

yMean = [mean(uncrowdedThresh) mean(crowdedThresh)];
scatter(xS(1),yMean(1),sz,'k','filled')
hold on
scatter(xS(2),yMean(2),sz,'k','filled')
[~,p,ci] = ttest(uncrowdedThresh,crowdedThresh);

t = text(1.25,40,sprintf('p = %.3f', p));
t(1).FontSize = 14;
line(xS, yMean, 'Color','k','LineWidth',2);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ySTD = [std(uncrowdedThresh) std(crowdedThresh)]; %standard deviation
ySEM = ySTD/(sqrt(subNum)); %Standard Error Of The Mean At Each Value Of �x�

CI95 = tinv([0.025 0.975], subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
yCI95 = bsxfun(@times, ySEM, CI95(:)); % Calculate 95% Confidence Intervals Of All Experiments At Each Value Of �x�
hold on
errorbar(1,yMean(1),yCI95(1,1),yCI95(1,1),...
    'vertical', '-ok','MarkerSize',4,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
errorbar(2, yMean(2),yCI95(1,2),yCI95(1,2),...
    'vertical', '-ok','MarkerSize',4,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)

title({'Performance for Uncrowded vs Crowded', 'Fixed Strokewidth Size'})

end

