function [ Drifts, xValues, yValues ] = buildDriftFixationStruct(...
    sw, driftIdx, Drifts, params, pptrials, counter, strokeWidth, eccentricity, xValues, yValues)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% counter = 1;

for ii = driftIdx %for each drift trials in pptrials
%     if params.D
%         for i = 1:length(pptrials{ii}.drifts.start)
%             if ~isempty(pptrials{ii}.drifts.start) && pptrials{ii}.drifts.duration(i) > 300
                timeOn = ceil(pptrials{ii}.TimeFixationON/(1000/330));
                timeOff = floor(pptrials{ii}.TimeFixationOFF/(1000/330)-1);
                if timeOn == 0
                    timeOn = 1;
                end
                if timeOff > length(pptrials{ii}.x.position)
                    continue;
                end
                Drifts.(eccentricity).(strokeWidth).position(counter).x = ...
                    [pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.xoffset * pptrials{ii}.pixelAngle];
                xValues = [xValues, Drifts.(eccentricity).(strokeWidth).position(counter).x];
                
                Drifts.(eccentricity).(strokeWidth).position(counter).y = ...
                    [pptrials{ii}.y.position(timeOn:timeOff) + pptrials{ii}.yoffset * pptrials{ii}.pixelAngle];
                yValues = [yValues, Drifts.(eccentricity).(strokeWidth).position(counter).y];
                
                x = Drifts.(eccentricity).(strokeWidth).position(counter).x;
                y = Drifts.(eccentricity).(strokeWidth).position(counter).y;
                
%                 Drifts
                position.x = x;
                position.y = y;
                
                
                
                Drifts.(eccentricity).(strokeWidth).id(counter) = ii;
                Drifts.(eccentricity).(strokeWidth).span(counter) = quantile(...
                    sqrt((Drifts.(eccentricity).(strokeWidth).position(counter).x-...
                    mean(Drifts.(eccentricity).(strokeWidth).position(counter).x)).^2 + ...
                    (Drifts.(eccentricity).(strokeWidth).position(counter).y-...
                    mean(Drifts.(eccentricity).(strokeWidth).position(counter).y)).^2), .95);%Drifts.span(ii);
                
                Drifts.(eccentricity).(strokeWidth).amplitude(counter) = sqrt((pptrials{ii}.x.position(timeOff) - ...
                    pptrials{ii}.x.position(timeOn))^2 + ...
                    (pptrials{ii}.y.position(timeOff) - pptrials{ii}.y.position(timeOn))^2);
                
                Drifts.(eccentricity).(strokeWidth).time(counter) = ...
                    mean(pptrials{ii}.TimeFixationOFF-pptrials{ii}.TimeFixationON);
                Drifts.(eccentricity).(strokeWidth).timeTargetOnSamp(counter) = timeOn;
                
                Drifts.(eccentricity).(strokeWidth).timeTargetOffSamp(counter) = timeOff;
                Drifts.(eccentricity).(strokeWidth).prlDistance(counter) = mean(sqrt(x.^2 + y.^2));
                
                 [~, Drifts.(eccentricity).(strokeWidth).velocity(counter), velX{counter}, velY{counter}] = ...
                 CalculateDriftVelocity(position, 1);
        
%                  [~, instSpX, instSpY, mn_speed, driftAngle, curvature, varx, vary] = ...
%                         getDriftChar(x, y, 41, 1, 180);
% 
%                     Drifts.(eccentricity).(strokeWidth).curvature{counter} = curvature;
%                     Drifts.(eccentricity).(strokeWidth).driftAngle{counter} = driftAngle;
%                     Drifts.(eccentricity).(strokeWidth).mn_speed{counter} = mn_speed;
%                     Drifts.(eccentricity).(strokeWidth).varX{counter}= varx;
%                     Drifts.(eccentricity).(strokeWidth).varY{counter} = vary;
                
                counter = counter +1;
%             else
%                 continue;
%             end
        end
%     elseif params.DMS
%         if strcmp('D',params.machine)
%             timeOn = round(pptrials{ii}.TimeFixationON/(1000/330));
%             timeOff = floor((pptrials{ii}.TimeFixationON)/(1000/330));
%         else
%             timeOn = round(pptrials{ii}.TimeFixationON);
%             timeOff = round(min((pptrials{ii}.TimeFixationON)));
%         end
%     end
%     if length(pptrials{ii}.x.position) < timeOff
%         timeOff = length(pptrials{ii}.x.position);
%     end
%     if timeOn == 0 || timeOff == 0
%         continue;
%     end
%     Drifts.(eccentricity).(strokeWidth).position(counter).x = ...
%         [pptrials{ii}.x.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset];
%     xValues = [xValues, Drifts.(eccentricity).(strokeWidth).position(counter).x];
%     Drifts.(eccentricity).(strokeWidth).position(counter).y = ...
%         [pptrials{ii}.y.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset];
%     yValues = [yValues, Drifts.(eccentricity).(strokeWidth).position(counter).y];
%     
%     Drifts.(eccentricity).(strokeWidth).id(counter) = ii;
%     Drifts.(eccentricity).(strokeWidth).span(counter) = quantile(...
%         sqrt((Drifts.(eccentricity).(strokeWidth).position(counter).x-...
%         mean(Drifts.(eccentricity).(strokeWidth).position(counter).x)).^2 + ...
%         (Drifts.(eccentricity).(strokeWidth).position(counter).y-...
%         mean(Drifts.(eccentricity).(strokeWidth).position(counter).y)).^2), .95);%Drifts.span(ii);
%     
%     Drifts.(eccentricity).(strokeWidth).amplitude(counter) = sqrt((pptrials{ii}.x.position(timeOff) - ...
%         pptrials{ii}.x.position(timeOn))^2 + ...
%         (pptrials{ii}.y.position(timeOff) - pptrials{ii}.y.position(timeOn))^2);
%     
%     Drifts.(eccentricity).(strokeWidth).time(counter) = mean(timeOff-timeOn);
%     Drifts.(eccentricity).(strokeWidth).timeTargetOnSamp(counter) = timeOn;
%     Drifts.(eccentricity).(strokeWidth).timeTargetOffSamp(counter) = timeOff;
%     
%     counter = counter +1;
% end

end



