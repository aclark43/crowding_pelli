function [data] = loadinAOData(subject)
%AO Data from eccentricitySummary sheet on SummarizedData.xlsx
data.ecc0.AO.PRL = 0;
data.ecc10.AO.PRL = 0;
data.ecc15.AO.PRL = 0;
data.ecc25.AO.PRL = 0;
    

if strcmp('Sanjana',subject) || strcmp('Z160',subject)
    data.ecc0.AO.PRL = 3.67*60^2;
    data.ecc10.AO.PRL = 3.24*60^2;
    data.ecc15.AO.PRL = 2.76*60^2;
    data.ecc25.AO.PRL = 2.05*60^2;
       
elseif strcmp('Z151',subject) || strcmp('Soma',subject)
    data.ecc0.AO.PRL = 3.76*60^2;
    data.ecc10.AO.PRL = 3.20*60^2;
    data.ecc15.AO.PRL = 2.73*60^2;
    data.ecc25.AO.PRL = 2.10*60^2;
  
elseif strcmp('Z126',subject)
    data.ecc0.AO.PRL = 3.46*60^2;
    data.ecc10.AO.PRL = 3.25*60^2;
    data.ecc15.AO.PRL = 3.17*60^2;
    data.ecc25.AO.PRL = 2.57*60^2;

elseif strcmp('A188',subject)
    data.ecc0.AO.PRL = 3.53*60^2;
    data.ecc10.AO.PRL = 3.49*60^2;
    data.ecc15.AO.PRL = 3.21*60^2;
    data.ecc25.AO.PRL = 2.4898 * 60^2;
    
elseif strcmp('Janis',subject) || strcmp('Z024',subject)
    data.ecc0.AO.PRL =  3.30*60^2;
    data.ecc10.AO.PRL = 3.09*60^2;
    data.ecc15.AO.PRL = 2.87*60^2;
    data.ecc25.AO.PRL = 2.48*60^2;
    
elseif strcmp('Ashley',subject) || strcmp('Z055',subject)
    data.ecc0.AO.PRL =  4.30*60^2;
    data.ecc10.AO.PRL = 3.82*60^2;
    data.ecc15.AO.PRL = 3.49*60^2;
    data.ecc25.AO.PRL = 2.80*60^2;    
    
elseif strcmp('Z091',subject) ||  strcmp('Z190',subject) 
    data.ecc0.AO.PRL =  5.14*60^2;
    data.ecc10.AO.PRL = 4.39*60^2;
    data.ecc15.AO.PRL = 3.84*60^2;
    data.ecc25.AO.PRL = 2.84*60^2;
    
 elseif strcmp('A144',subject)	
     %new tagging based on 0ecc = cdc
    data.ecc0.AO.PRL =  4.16*60^2; %cones/deg^2
    data.ecc10.AO.PRL = 3.68*60^2;
    data.ecc15.AO.PRL = 3.16*60^2;
    data.ecc25.AO.PRL = 2.53*60^2;
    
 elseif strcmp('Krish',subject) || strcmp('Z192',subject)
     %new tagging based on 0ecc = cdc
    data.ecc0.AO.PRL =  3.19*60^2; %cones/deg^2
    data.ecc10.AO.PRL = 3.13*60^2;
    data.ecc15.AO.PRL = 3.09*60^2;
    data.ecc25.AO.PRL = 2.81*60^2;
end

% % % if strcmp('Sanjana',subject)
% % %     data.ecc0.AO.PRL = 12407.5193;
% % %     data.ecc10.AO.PRL = 10495.07526;
% % %     data.ecc15.AO.PRL = 9409.152432;
% % %     data.ecc25.AO.PRL = 7150.409376;
% % %     
% % % elseif strcmp('Z151',subject) || strcmp('Soma',subject)
% % %     data.ecc0.AO.PRL = 11596.37241;
% % %     data.ecc10.AO.PRL = 10953.26901;
% % %     data.ecc15.AO.PRL = 9762.516868;
% % %     data.ecc25.AO.PRL = 7530.378259;
% % %   
% % % elseif strcmp('Z126',subject)
% % %     data.ecc0.AO.PRL = 12465.07425;
% % %     data.ecc10.AO.PRL = 12505.59958;
% % %     data.ecc15.AO.PRL = 11882.21179;
% % %     data.ecc25.AO.PRL = 9466.364659;
% % % 
% % % elseif strcmp('A188',subject)
% % %     data.ecc0.AO.PRL = 12465.07425;
% % %     data.ecc10.AO.PRL = 10525.39981;
% % %     data.ecc15.AO.PRL = 9568.3078;
% % %     data.ecc25.AO.PRL = 8072.797372;
% % %     
% % % elseif strcmp('Janis',subject)	
% % %     data.ecc0.AO.PRL =  12684.61208;
% % %     data.ecc10.AO.PRL = 11011.64207	;
% % %     data.ecc15.AO.PRL = 10216.49085;
% % %     data.ecc25.AO.PRL = 8381.606558;
% % %     
% % % elseif strcmp('Ashley',subject) || strcmp('Z055',subject)				
% % %     data.ecc0.AO.PRL =  12359.85958;
% % %     data.ecc10.AO.PRL = 10954.96097;
% % %     data.ecc15.AO.PRL = 10845.53619;
% % %     data.ecc25.AO.PRL = 10031.22563;
% % %     
% % % elseif strcmp('Z091',subject)	
% % %     data.ecc0.AO.PRL =  10048.89606;
% % %     data.ecc10.AO.PRL = 10060.29916;
% % %     data.ecc15.AO.PRL = 9571.688773	;
% % %     data.ecc25.AO.PRL = 8735.167548;
% % % end


end

