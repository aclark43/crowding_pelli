
function prob = coneAndFlankerProbability_OG(x,stimSize,multiplier)

% x = xALLTrial{1, 1}
% y = yALLTrial{1, 1}
stimSize = round(stimSize);%;2;%(arcmin) 
spacingCones = 0.5;% (arcmin)
stimSizeInCones = stimSize / spacingCones;
% multiplier = 0.4;

targetActivation = abs(round(x) + round( (x-round(x))/0.5) * 0.5);
targetActRight = targetActivation + (stimSizeInCones/2);
targetActLeft = targetActivation - (stimSizeInCones/2);

xFlanker = targetActivation + (stimSize * (multiplier/.5));
flankerActivation = abs(round(xFlanker) + ...
    round( (xFlanker-round(xFlanker))/0.5) * 0.5);
flankerActRight= flankerActivation + (stimSizeInCones/2);
flankerActLeft = flankerActivation - (stimSizeInCones/2);

[targetAll,~, numTimesT] = unique([targetActivation targetActRight targetActLeft]);
[flankerAll,~, numTimesF] = unique([flankerActivation flankerActRight flankerActLeft]);

a_countsT = accumarray(numTimesT,1);
% value_countsT = [targetAll', a_countsT];
targetAllThresh = targetAll(find(a_countsT'>4));

a_countsF = accumarray(numTimesF,1);
% value_countsF = [flankerAll', a_countsF];
flankerAllThresh = flankerAll(find(a_countsF'>5));

sameCones = intersect(targetAllThresh, flankerAllThresh);
prob = length(sameCones)/length(targetAllThresh);










