function generateSpanHist(driftStruct, sw, title_str, trialChar)

%Generate Histogram of Span
%   Creates a histogram of the different span based on strokewidth. "sw" is
%   the given strokewidth. "driftIdx" are all the valid drift trials within
%   this sw.

%  figure
    histSpan = (driftStruct);
    h = histogram(histSpan,50);
    xlim([0 10])
%         h.BinEdges = single((sprintf('%s', params.spanMin):(sprintf('%s', params.spanMax))));
%     set(gcf,'units','normalized','outerposition',[0 0 1 1])
    graphTitle = sprintf('Drift Analysis %s for SW %i', title_str, sw);
    title(graphTitle,'Interpreter','none');
    
    xlabel('Span (arcmin)');
    ylabel('Number of Trials');
    set(gcf, 'Color', 'w');
    set(gca, 'FontSize', 16, 'FontWeight', 'bold');
    
    
%     hist(driftStruct,100)
    
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.epsc', trialChar.Subject, graphTitle));
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.jpeg', trialChar.Subject, graphTitle));
%     saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s.fig', trialChar.Subject, graphTitle));
    
end

