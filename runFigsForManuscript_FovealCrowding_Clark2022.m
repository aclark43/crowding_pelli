%runFigsForManuscript_FovealCrowding_Clark2022
clear all
clc

% subjectsAll = {'Z013','Z014','Z181','Z002','Z084'};

subjectsAll = {'Z023','Ashley','Z005','Z002','Z013',...
   'Z024','Z064','Z046','Z084','Z014',...
   'Z091','Z138','Z181'};%,...
% sampRate = [1000 1000 1000 1000 1000 ...
%     1000 341 341 341 341 ...
%     341 341 341];

conditions = {'Uncrowded','Crowded'};
eccNames = {'0ecc'};
numEcc = 1;

colorMap = brewermap(length(subjectsAll),'Set3');
colorMapVs = brewermap(length(subjectsAll),'Paired');
colorMapNew = brewermap(length(subjectsAll),'YlGnBu');

% colorMapNew = cool(length(subjectsAll));
colorMapNew = ashleysColorMap;
%% load in variables
for numSub = 1:length(subjectsAll)
    for numCrow = 1:2
        %         for numStab = 1:length(stabilization)
        if strcmp(subjectsAll{numSub}, 'Zoe') || strcmp(subjectsAll{numSub}, 'Z091')
            
            subjectCond.(conditions{numCrow}).Unstabilized{numSub} = ...
                load(sprintf('MATFiles/%s_%s_%s_Drift_%s_Threshold.mat', ...
                subjectsAll{numSub}, (conditions{numCrow}),...
                'Unstabilized', '0ecc'));
        else
            subjectCond.(conditions{numCrow}).Unstabilized{numSub} = ...
                load(sprintf('MATFiles/%s_%s_%s_Drift_%s_Threshold.mat', ...
                subjectsAll{numSub}, (conditions{numCrow}),...
                'Unstabilized', (eccNames{numEcc})));
        end
        %         end
    end
end

for ii = 1:length(subjectsAll)
    percentMSTrials(ii) = sum(subjectCond.Uncrowded.Unstabilized{ii}.threshInfo.em.msValid)/...
         length(subjectCond.Uncrowded.Unstabilized{ii}.threshInfo.em.msValid);
end

mean(percentMSTrials)
std(percentMSTrials)


for ii = 1:length(subjectsAll)
    percentMSTrials(ii) = sum(subjectCond.Crowded.Unstabilized{ii}.threshInfo.em.msValid)/...
         length(subjectCond.Crowded.Unstabilized{ii}.threshInfo.em.msValid);
end

% mean(percentMSTrials)
% std(percentMSTrials)


% % figure;
% % tempNum = 12;
% % plot(1:length(subjectCond.Uncrowded.Unstabilized{1, 1}.threshInfo.em.x{1, tempNum}),...
% %     subjectCond.Uncrowded.Unstabilized{1, 1}.threshInfo.em.x{1, tempNum},'-');
% % hold on
% % plot(1:length(subjectCond.Uncrowded.Unstabilized{1, 1}.threshInfo.em.y{1, tempNum}),...
% %     subjectCond.Uncrowded.Unstabilized{1, 1}.threshInfo.em.y{1, tempNum},'-');
% % ylim([-6 6])
% % % stabilized.Z181.Uncrowded = load('MATFiles/Z002DDPI_Uncrowded_Stabilized_Drift_0ecc_Threshold.mat');
% % stabilized.Z091.Uncrowded = load('MATFiles/Z091_Uncrowded_Stabilized_Drift_0eccEcc_Threshold.mat');
% % stabilized.Ashley.Uncrowded = load('MATFiles/Ashley_Uncrowded_Stabilized_Drift_0ecc_Threshold.mat');
% % %stabilized.Z023.Uncrowded = load('MATFiles/Z023_Uncrowded_Stabilized_Drift_0ecc_DriftChar.mat');
% % 
% % % stabilized.Zoe.Crowded = load('MATFiles/Zoe_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
% % stabilized.Z091.Crowded = load('MATFiles/Z091_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
% % stabilized.Ashley.Crowded = load('MATFiles/AshleyDDPI_Crowded_Stabilized_Drift_0eccEcc_Threshold.mat');
% % %stabilized.Z023.Crowded = load('MATFiles/Z023_Crowded_Stabilized_Drift_0ecc_DriftChar.mat');
% % 
% % figure;
% % subplot(1,2,2)
% % % plot([1 2],[stabilized.Zoe.Uncrowded.threshInfo.thresh ...
% % %     stabilized.Zoe.Crowded.threshInfo.thresh],'-o');
% % hold on
% % plot([1 2],[stabilized.Z091.Uncrowded.threshInfo.thresh ...
% %     stabilized.Z091.Crowded.threshInfo.thresh],'-o');
% % plot([1 2],[stabilized.Ashley.Uncrowded.threshInfo.thresh ...
% %     stabilized.Ashley.Crowded.threshInfo.thresh],'-o');
% % ylim([1 3])
% % title('Stabilized')
% % subplot(1,2,1)
% % % plot([1 2],[subjectCond.Uncrowded.Unstabilized{14}.threshInfo.thresh   ...
% % %     subjectCond.Crowded.Unstabilized{14}.threshInfo.thresh  ],'-o');
% % hold on
% % plot([1 2],[subjectCond.Uncrowded.Unstabilized{11}.threshInfo.thresh,...
% %     subjectCond.Crowded.Unstabilized{11}.threshInfo.thresh],'-o');
% % plot([1 2],[subjectCond.Uncrowded.Unstabilized{2}.threshInfo.thresh   ...
% %     subjectCond.Crowded.Unstabilized{2}.threshInfo.thresh  ],'-o');
% % ylim([1 3])
% % title('Unstabilized')

% figure;
%  [~,p,b,r] = LinRegression(...
%             [1 2 1 2],...%x([1 2 4:13])
%             [subjectCond.Uncrowded.Unstabilized{11}.threshInfo.thresh,...
%             subjectCond.Crowded.Unstabilized{11}.threshInfo.thresh,... %y
%             subjectCond.Uncrowded.Unstabilized{2}.threshInfo.thresh,...
%             subjectCond.Crowded.Unstabilized{2}.threshInfo.thresh],...
%             0,NaN,1,0);
%         
%  figure;
%  [~,p,b,r] = LinRegression(...
%             [1 2 1 2],...%x([1 2 4:13])
%             [stabilized.Z091.Uncrowded.threshInfo.thresh ...
%     stabilized.Z091.Crowded.threshInfo.thresh,... %y
%             stabilized.Ashley.Uncrowded.threshInfo.thresh ...
%     stabilized.Ashley.Crowded.threshInfo.thresh],...
%             0,NaN,1,0);

%% build table
for ii = 1:length(subjectsAll)
    for c = 1:2
        toPath = subjectCond.(conditions{c}).Unstabilized;
        tbl.(conditions{c})(ii).thresh = toPath{ii}.threshInfo.thresh;
        tbl.(conditions{c})(ii).dc = toPath{ii}.threshInfo.em.allTraces.dCoefDsq;
        tbl.(conditions{c})(ii).prl = ...
            mean(toPath{ii}.threshInfo.em.allTraces.prlDistance);
        tbl.(conditions{c})(ii).span = mean(toPath{ii}.threshInfo.em.allTraces.span);
        tbl.(conditions{c})(ii).bcea = mean(cell2mat(...
            toPath{ii}.threshInfo.em.allTraces.bcea));
        tbl.(conditions{c})(ii).curvature = ...
            mean(cell2mat(toPath{ii}.threshInfo.em.allTraces.curvature));
        
        for i = 1:length((toPath{ii}.threshInfo.em.allTraces.mn_speed))
            tempSpan(i) = toPath{ii}.threshInfo.em.allTraces.mn_speed{i};
        end
        tbl.(conditions{c})(ii).span = ...
            nanmean(tempSpan);
                
        matchingThresholdRound = round(toPath{ii}.threshInfo.thresh,1);
        matchingThresholdRound = [1.2 1.5];
        [tbl.(conditions{c})(ii).thresholdSW, ~, tbl.(conditions{c})(ii).actualStimSize] = ...
            findMatchingStrokewidthValue(matchingThresholdRound(1), toPath{ii}.threshInfo.em.ecc_0);
        
        if c == 1
            matchingThresholdRound = [1.2 1.5];
        else
            matchingThresholdRound = [1.7 2];
        end
        [stringVal{1}, perfVal(1)] = ...
            findMatchingStrokewidthValue(matchingThresholdRound(1), toPath{ii}.threshInfo.em.ecc_0);
        
        [stringVal{2}, perfVal(2)] = ...
            findMatchingStrokewidthValue(matchingThresholdRound(2), toPath{ii}.threshInfo.em.ecc_0);
        tbl.(conditions{c})(ii).stringVals{1} = stringVal(1);
        tbl.(conditions{c})(ii).stringVals{2} = stringVal(2);
        
        tbl.(conditions{c})(ii).performance{1} = perfVal(1);
        tbl.(conditions{c})(ii).performance{2} = perfVal(2);

        tbl.(conditions{c})(ii).prlDistances{1} = mean(toPath{ii}.threshInfo.em.ecc_0.(stringVal{1}).prlDistance);
        tbl.(conditions{c})(ii).prlDistances{2} = mean(toPath{ii}.threshInfo.em.ecc_0.(stringVal{2}).prlDistance);
        tbl.(conditions{c})(ii).name = subjectsAll{ii};
    end
end

[SubjectOrderDC,SubjectOrderDCIdx] = sort([tbl.Uncrowded(:).thresh]);
% figure
% for ii = 1:length(subjectsAll)
%     plot(ii,tbl.Uncrowded(ii).thresh,'o','Color',colorMapNew(SubjectOrderDCIdx == ii,:),...
%         'MarkerFaceColor',colorMapNew(SubjectOrderDCIdx == ii,:));
% hold on
% end

%% get num ms trials/percentage
for ii = 1:length(subjectsAll)
    temp = subjectCond.Crowded.Unstabilized{ii}.threshInfo.em.msValid;
    percentMS(ii) = (sum(temp)/length(temp))*100;
end

mean(percentMS)
std(percentMS)


%% mislocalization code from Krsih 
%%%% you can try generating a random number to see what we are compaering
%%%% to....
% % % clear misLocAll
% % % for ii = 1:26%length(subjectsAll)+13
% % %     % get incorrect trials
% % %     if ii > 13
% % %         misLocPos = randi([1 3],1,200);
% % %     else
% % %         dataNowPath = subjectCond.Crowded.Unstabilized{ii}.threshInfo.em.ecc_0;
% % %         namesSW = fieldnames(dataNowPath);%tbl.Crowded(ii).stringVals{1,2};%
% % %       
% % %             counter = 1;
% % %             misLocPos = [];
% % %             for i = 1:length(namesSW)
% % %                 data = find(dataNowPath.(namesSW{i}).correct == 0);
% % %                 trialData = dataNowPath.(namesSW{i});
% % %                 for idx=data
% % %                     %         trialDataIdx=data(idx,:);
% % %                     response=trialData.response(idx);
% % %                     temp = num2str(trialData.flankers(idx));
% % %                     leftFlanker=str2num(temp(1));
% % %                     rightFlanker=str2num(temp(3));
% % %                     
% % %                     if response==leftFlanker
% % %                         misLocPos(counter)=1; %left;
% % %                     elseif response==rightFlanker
% % %                         misLocPos(counter)=2; %rightward
% % %                     else
% % %                         misLocPos(counter)=3; %other number
% % %                     end
% % %                     counter = counter + 1;
% % %                 end
% % %             end
% % %     end
% % %         num_errors = length(find(misLocPos));
% % %         misLocAll(ii,1)=length(find(misLocPos == 1))/num_errors; %left
% % %         misLocAll(ii,2)=length(find(misLocPos == 2))/num_errors; %right
% % %         misLocAll(ii,3)=length(find(misLocPos == 3))/num_errors; %other
% % %         misLocAll(ii,4)=num_errors; %other    
% % % end
% % % 
% % % tabStuff = table(misLocAll)
% % % 
% % % rowNames = {subjectsAll{:}, 'Pseudo'};
% % % colNames = {'Left','Right','Other','N'};
% % % % colNames = {'Bottom','Top','Other','N'};
% % % 
% % % sTable = array2table(misLocAll,'RowNames',rowNames,'VariableNames',colNames)
% % % 
% % % [h,p] = ttest(misLocAll(1:13,2),misLocAll(14:26,2));
% % % 
% % % figure;
% % % for t = 1:13
% % %     plot(2,misLocAll(t,2),'o');
% % %     hold on
% % % end
% % % for t = 14:26
% % %     plot(1,misLocAll(t,2),'o');
% % %     hold on
% % % end
% % % for t = 1:13
% % %     plot(3,misLocAll(t,1),'o');
% % %     hold on
% % % end
% % % xticks([1:3])
% % % xticklabels({'Pseudo','Right','Left'})
% % % xlim([0 4])
% % % 
% % % isVals = [1 2 3  ...
% % %         1 2 3 ...
% % %         1 2 3  ...
% % %         1 2 3  ...
% % %         1 2 3];
% % % [~,~,stats] = anovan([misLocAll(:,[1:3])],{isVals});
% % % [results,~,~,gnames] = multcompare(stats);
% % % yticklabels({'Fix Cont', 'Fix Scot', ...
% % %     'Task Cont', 'Task Scot'});
% % % title('AO Areas');

%% remake psychometric curves
% % for ii = 1%:length(subjectsAll)
% %     pathUse1 = subjectCond.Uncrowded.Unstabilized{ii}.threshInfo.em;
% %     x1 = round(pathUse1.size(pathUse1.valid)*2,1);
% %     y1 = double(pathUse1.performance(pathUse1.valid));
% %     pathUse2 = subjectCond.Crowded.Unstabilized{ii}.threshInfo.em;
% %     x2 = round(pathUse2.size(pathUse2.valid)*2,1);
% %     y2 = double(pathUse2.performance(pathUse2.valid));
% %     
% %     figure;
% %     if ii ==1
% %     thLegs(1) = plot([-1 -2],[-1 -2],'-','Color',[0 0 0],'LineWidth',6);
% %     hold on
% %     thLegs(2) = plot([-1 -2],[-1 -2],'-','Color',[.4 .4 .4],'LineWidth',6);
% %     legend(thLegs, {'Uncrowded','Crowded'},'Location','northwest');
% %     hold on
% %     end
% %     xlv = [0.1, 11];
% %     ylv = [0, 1.05];
% %     hold on
% %     [thresh1] = ...
% %         psyfitCrowding_Replotting(x1, y1,[0 0 0], 'DistType', 'Normal',...
% %         'PlotHandle', 10+1, 'Xlim', xlv, 'Ylim', ylv,...
% %         'Boots', 2, 'Chance', 0.25, 'Extra');
% %     hold on
% %     [thresh2] = ...
% %         psyfitCrowding_Replotting(x2, y2,[.4 .4 .4], 'DistType', 'Normal',...
% %         'PlotHandle', 10+1, 'Xlim', xlv, 'Ylim', ylv,...
% %         'Boots', 2, 'Chance', 0.25, 'Extra');
% %     axis square
% %     legend(thLegs, {'Uncrowded','Crowded'});
% %     xlim([0 5])
% %     ylim([0 1.1])
% %     yticks([.2 .4 .6 .8 1])
% %     if ii == 1
% %     xlabel('\fontsize{14} Stimulus Width (arcmin)')
% %     ylabel('\fontsize{14} Proportion Correct');
% %     end
% %     title(sprintf('S%i',ii))
% % 
% %     saveas(gcf,sprintf('CrowdingFits/%i_CrowdedUncrowded.png',ii));
% %     saveas(gcf,sprintf('CrowdingFits/%i_CrowdedUncrowded.epsc',ii));
% %     saveas(gcf,sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/PsychCombined/%i_CrowdedUncrowded.epsc',ii));
% % 
% %     close all
% % end
%%
for ii = 1:length(subjectsAll)
    for c = 1:length(conditions)
        currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em.ecc_0;
        allSWOptions = fieldnames(currentPath);
        for i = 1:length(allSWOptions)
            saveStimInfo{c,ii}(1,i) = currentPath.(allSWOptions{i}).stimulusSize;
            saveStimInfo{c,ii}(2,i) = string(cell2mat(...
                regexp(allSWOptions{i},'\d*','Match')));
            saveStimInfo{c,ii}(3,i) = length(currentPath.(allSWOptions{i}).id);
        end
    end
end


stimSizeWanted = [1.5 2.2];
for ii = 1:length(subjectsAll)
    for c = 1:length(conditions)
        currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em.ecc_0;
        [val,idx] = min(abs(saveStimInfo{c, ii}(1,:)-stimSizeWanted(c)));
        temp = (idx);
        nameTemp = sprintf('strokeWidth_%i', ...
            saveStimInfo{c, ii}(2,temp));
        actualPerf(c,ii) = currentPath.(nameTemp).performanceAtSize;
        actualPRLatSize(c,ii) = mean(currentPath.(nameTemp).prlDistance);
        actualSize(c,ii) = currentPath.(nameTemp).stimulusSize;
        fixedStim.actualCorrect{c,ii} =  currentPath.(nameTemp).correct;
        actualPRLatSizeInd{c,ii} = (currentPath.(nameTemp).prlDistance);
        stimSizeDesired = stimSizeWanted(c);
        farAway = stimSizeDesired*1.4;

        counter = 1;
        for i = length(currentPath.(nameTemp).position)
            temp = currentPath.(nameTemp).position(i).x < (stimSizeDesired/2) & ...
                currentPath.(nameTemp).position(i).x > -(stimSizeDesired/2) &...
                currentPath.(nameTemp).position(i).y < (5*stimSizeDesired/2) & ...
                currentPath.(nameTemp).position(i).y > -(5*stimSizeDesired/2);
            
            
            percentsOnStimFixSize(counter) = length(find(temp==1))/length(temp);
            temp2 = (currentPath.(nameTemp).position(i).x < (stimSizeDesired/2)+farAway & ...
                currentPath.(nameTemp).position(i).x > -(stimSizeDesired/2)+farAway) |...
                (currentPath.(nameTemp).position(i).x < (stimSizeDesired/2)-farAway & ...
                currentPath.(nameTemp).position(i).x > -(stimSizeDesired/2)-farAway);
            percentOnFlankerFixSize(counter) = length(find(temp2==1))/length(temp2);           
            meanDistFixSize(counter) = mean(currentPath.(nameTemp).position(i).x);
            counter = counter + 1;
        end
        fixedStim.timeSpentOnTarget{c,ii} = (percentsOnStimFixSize);
        fixedStim.timeOnFlankers{c,ii} = (percentOnFlankerFixSize); %just horizontal flankers
        fixedStim.meanDistX{c,ii} = meanDistFixSize;
    end
end

for ii = 1:length(subjectsAll)
    for c = 1:length(conditions)
        for s = 1:length(saveStimInfo{c,ii}(1,:))
% % %             if ii > 13
% % %                 [~,temp] = sort(saveStimInfo{c,ii}(3,:));
% % %                 stimSizeDesired =  find(saveStimInfo{c,ii}(3,:) ==...
% % %                     saveStimInfo{c,ii}(3,temp(end-1)));
% % %             else
% % %                 stimSizeDesired = find(saveStimInfo{c,ii}(3,:) ==...
% % %                     max(saveStimInfo{c,ii}(3,:)));
% % %             end
%             %         stimSizeDesired = find(saveStimInfo{c,ii}(3,:) ==...
%             %             max(saveStimInfo{c,ii}(3,:)));
            stimSizeDesired = s; %%%
            stimSizeMost = saveStimInfo{c,ii}(1,stimSizeDesired);
            currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em;
            temp = unique(currentPath.size);
            columnIdx = stimSizeDesired;
            
            idx = currentPath.ecc_0.(sprintf('strokeWidth_%i',saveStimInfo{c,ii}(2,columnIdx))).id;
            area_poly = []; dcAllTrial = []; perfGTrials = []; curvature = []; area = [];
            counter = 1; clear percentsOnStim; clear percentOnFlanker; clear meanDist;
            farAway = stimSizeDesired*1.4;
            stimSizeDesired = stimSizeDesired*1.5; %(make larger)
            for i = idx
                clear temp, clear temp2
                temp = currentPath.x{i} < (stimSizeDesired/2) & ...
                    currentPath.x{i} > -(stimSizeDesired/2) &...
                    currentPath.y{i} < 5*(stimSizeDesired/2) & ...
                    currentPath.y{i} > -5*(stimSizeDesired/2);
                %             temp = currentPath.x{i} < (stimSizeDesired/2) & ... %%just x
                %                 currentPath.x{i} > -(stimSizeDesired/2);
                
                percentsOnStim(counter) = length(find(temp==1))/length(temp);
                
                temp2 = (currentPath.x{i} < (stimSizeDesired/2)+farAway & ...
                    currentPath.x{i} > -(stimSizeDesired/2)+farAway) |...
                    (currentPath.x{i} < (stimSizeDesired/2)-farAway & ...
                    currentPath.x{i} > -(stimSizeDesired/2)-farAway);
                %                 temp2 = (currentPath.x{i} < (stimSizeDesired/2)+farAway & ... %%just x
                %                 currentPath.x{i} > -(stimSizeDesired/2)+farAway);
                
                percentOnFlanker(counter) = length(find(temp2==1))/length(temp2);
                temp3 = temp2(find(temp2 == 1))* -1;
                percentWeightOnStim(1,counter) = (sum(temp)+sum(temp3));
                percentWeightOnStim(2,counter) = length(temp);
                
                meanDist(counter) = mean(currentPath.x{i});
                xALLTrial{counter} = currentPath.x{i};
                yALLTrial{counter} = currentPath.y{i};
                
                if length(xALLTrial{1}) > 499
                    actualSampRate(ii) = 1000;
                else
                    actualSampRate(ii) = 341;
                end
                clear temp
                [dTrendedD(counter),temp,dcAllTrial(counter),~,~,~,~,~] = ...
                    CalculateDiffusionCoef( actualSampRate(ii), struct('x',xALLTrial{counter},'y', yALLTrial{counter}));
                idxEnd = find(~isnan(temp(:,1)));
                biasD(counter) = hypot(temp(max(idxEnd),1),temp(max(idxEnd),2));
                probSameCone(counter) = coneAndFlankerProbability_OG(currentPath.x{i},stimSizeMost,1.4);
                
                tmpCur = nanmean(abs(cur(xALLTrial{counter}(10:end-10), yALLTrial{counter}(10:end-10))));
                %tmpCur(tmpCur > 300) = NaN;
                curvature(counter) = nanmean(tmpCur);
                area(counter) = CalculateTheArea(xALLTrial{counter},yALLTrial{counter},...
                    0.68,20, 20);
               
%                 area_poly(counter) = polyarea(xALLTrial{counter},yALLTrial{counter});
                
%                 x1 = [min(xALLTrial{counter}) max(xALLTrial{counter})];
%                 y1 = [min(yALLTrial{counter}) max(yALLTrial{counter})];
%                 perctileX = prctile(xALLTrial{counter},68)
%                 [tempC] = ellipseXY(xALLTrial{counter}', yALLTrial{counter}', 68, [150 150 150]/255,0);
%                 
%                 x1 = [tempC.Position(1) tempC.Position(1) ...
%                     tempC.Position(1)+tempC.Position(3) tempC.Position(1)+tempC.Position(3)];
%                 
%                 y1= [tempC.Position(2)+tempC.Position(4) tempC.Position(2)  ...
%                     tempC.Position(2) tempC.Position(2)+tempC.Position(4)];
%                 
%                 area_poly(counter) =  polyarea(x1,y1);
                
                
                cc = corrcoef(xALLTrial{counter}',yALLTrial{counter}');
                cc = cc(2,1);
                area_poly(counter) = 2*.68*pi*std(xALLTrial{counter})*...
                    std(yALLTrial{counter})*sqrt(1-cc^2);
                
                perfGTrials(counter) = currentPath.performance(i);
                
                xPRL(counter) = mean(currentPath.x{i});
                
                counter = counter + 1;
            end
            savVar(ii).timeSpentOnTarget{c,s} = (percentsOnStim);
            timeSpentOnTarget{c,ii} = (percentsOnStim);
            weightSpentOnTarget{c,ii} = percentWeightOnStim;
            savVar(ii).timeOnFlankers{c,s} = (percentOnFlanker); %just horizontal flankers
            timeOnFlankers{c,ii} = (percentOnFlanker);
            savVar(ii).performance{c,s} = currentPath.performance(idx);
            performance{c,ii} = currentPath.performance(idx);
            savVar(ii).meanPerf(c,s) = nanmean(currentPath.performance(idx));
            savVar(ii).size{c,s} = currentPath.size(idx);
            prl{c,ii} = currentPath.prl(idx);
            savVar(ii).prl{c,s} = currentPath.prl(idx);
            savVar(ii).span{c,s} = currentPath.span(idx);
%             savVar(ii).area{c,s} = area;
            savVar(ii).biasD{c,s} = biasD;
            BiasD{c,ii} = biasD;
            span{c,ii} = currentPath.span(idx);
%             areaAll{c,ii} = area;
            savVar(ii).prlX{c,s} = xPRL(find(xPRL ~= 0.0));
            savVar(ii).x{c,s} = currentPath.x{idx};
            x{c,ii} = currentPath.x{idx};
            savVar(ii).meanDistX{c,s} = meanDist;
            savVar(ii).numTrials(c,s) = max(saveStimInfo{c,ii}(3,:));
            numTrials(c,ii) = max(saveStimInfo{c,ii}(3,:));
            savVar(ii).xALLTrials{c,s} = xALLTrial;
            xALLTrials{c,ii} = xALLTrial; xALLTrial = [];
            savVar(ii).yALLTrials{c,s} = yALLTrial;
            yALLTrials{c,ii} = yALLTrial; yALLTrial = [];
            savVar(ii).dcAllTrials{c,s} = dcAllTrial;
            savVar(ii).curv{c,s} = curvature;
            savVar(ii).perfCur{c,s} = perfGTrials;
            savVar(ii).areaPoly{c,s} = area_poly;
            savVar(ii).dCoef{c,s} = dcAllTrial;
            savVar(ii).dTrend{c,s} = dTrendedD;

            perfCur{c,ii} = perfGTrials;
            areaPoly{c,ii} = area_poly;
            dCoef{c,ii} = dcAllTrial;
            dTrend{c,ii} = dTrendedD;
            savVar(ii).probSameConesforTrace{c,s} = probSameCone;
            probSameConesforTrace{c,ii} = probSameCone; probSameCone = [];
            %         if c == 2
            %             for j = 1:length(xALLTrial)
            %                 rightFlanker = savVar.xALLTrials{c,ii}{j}+(stimSizeDesired*1.4);
            %                 leftFlanker = savVar.xALLTrials{c,ii}{j}-(stimSizeDesired*1.4);
            %
            %                 C = intersect(round(savVar.xALLTrials{c,ii}{j},1),round(rightFlanker,1));
            %                 C2 = intersect(round(savVar.xALLTrials{c,ii}{j},1),round(leftFlanker,1));
            %                 allFlanks = unique([C C2]);
            %
            %                 numOverlapConesIndTrial(j) = length([min(allFlanks):0.5:max(allFlanks)]);
            %             end
            %             numOverlappingCones(ii,1) = (mean(numOverlapConesIndTrial));
            %             numOverlappingCones(ii,2) = (sem(numOverlapConesIndTrial));
            %         end
            allStimSizes{ii,s} = savVar;
        end
    end
end
close all

%%Drift in Time with Cones

% % figure;
% % for i = 1:10
% % legs(1) = plot(1:length(savVar(3).x{i}),...
% %     smoothdata(abs(savVar(3).x{i}-savVar(3).x{i}(1))*2,'gaussian',10),...
% %     'Color','b','LineWidth',2);
% % hold on
% % end
% % legend(legs,{'Single Ocular Drift Segments'},'Location','northwest');
% % ylabel('Number of Cones Activated');
% % xlabel('Time (ms)');
% % xlim([0 500]);
% % set(gca,'FontSize',14);
% figure;
% test1 = plot(3,4,'o')
% legend(test1,'Test');
%% GLM Building
% Build Table
tempT = [];
dataAll2 = [];
counter = 1;
for ii = 1:length(subjectsAll)
    for sizeNum = 1:length(savVar(ii).performance(1,:))
        for i = 1:length(savVar(ii).performance{1,sizeNum}) %for uncrowded
            tempT{counter,1} = ii; %subjectID
            tempT{counter,2} = -1;  %uncrowded
            tempT{counter,3} = double(savVar(ii).performance{1,sizeNum}(i)); %performance
            tempT{counter,4} = double(savVar(ii).prl{1,sizeNum}(i)); %offset
            tempT{counter,5} = savVar(ii).dcAllTrials{1,sizeNum}(i); %dc
            tempT{counter,6} = savVar(ii).curv{1,sizeNum}(i); %curv
            tempT{counter,7} = savVar(ii).size{1,sizeNum}(i); %size
            tempT{counter,8} = savVar(ii).span{1,sizeNum}(i); %span
            tempT{counter,9} = savVar(ii).probSameConesforTrace{1,sizeNum}(i);
            tempT{counter,10} = savVar(ii).areaPoly{1,sizeNum}(i);
            tempT{counter,11} = savVar(ii).dCoef{1,sizeNum}(i);
            tempT{counter,12} = savVar(ii).dTrend{1,sizeNum}(i);
            tempT{counter,13} = savVar(ii).xALLTrials{1,sizeNum}(i);
            tempT{counter,14} = savVar(ii).yALLTrials{1,sizeNum}(i);
            tempT{counter,15} = savVar(ii).biasD{1,sizeNum}(i);
            tempT{counter,16} = savVar(ii).prl{1,sizeNum}(i);
            tempT{counter,17} = savVar(ii).RT{1,sizeNum}(i);
            %             tempT{counter,10} = (areaAll{1,ii}(i));
%             tempT{counter,11} = (xALLTrials{1,ii}(i));
%             tempT{counter,12} = (yALLTrials{1,ii}(i));
%             tempT{counter,13} = (areaPoly{1,ii}(i));
%             tempT{counter,14} = (dCoef{1,ii}(i));
%             tempT{counter,15} = (dTrend{1,ii}(i));
            counter = counter + 1;
        end
    end
    for sizeNum = 1:length(savVar(ii).performance(2,:))
        for i = 1:length(savVar(ii).performance{2,sizeNum}) %for crowded
            tempT{counter,1} = ii; %subjectID
            tempT{counter,2} = 1;  %crowded
            tempT{counter,3} = double(savVar(ii).performance{2,sizeNum}(i)); %performance
            tempT{counter,4} = double(savVar(ii).prl{2,sizeNum}(i)); %offset
            tempT{counter,5} = savVar(ii).dcAllTrials{2,sizeNum}(i); %dc
            tempT{counter,6} = savVar(ii).curv{2,sizeNum}(i); %curv
            tempT{counter,7} = savVar(ii).size{2,sizeNum}(i); %size
            tempT{counter,8} = savVar(ii).span{2,sizeNum}(i); %span
            tempT{counter,9} = savVar(ii).probSameConesforTrace{2,sizeNum}(i);
            tempT{counter,10} = savVar(ii).areaPoly{2,sizeNum}(i);
            tempT{counter,11} = savVar(ii).dCoef{2,sizeNum}(i);
            tempT{counter,12} = savVar(ii).dTrend{2,sizeNum}(i);
            tempT{counter,13} = savVar(ii).xALLTrials{2,sizeNum}(i);
            tempT{counter,14} = savVar(ii).yALLTrials{2,sizeNum}(i);
            tempT{counter,15} = savVar(ii).biasD{2,sizeNum}(i);
            tempT{counter,16} = savVar(ii).prl{2,sizeNum}(i);

%             tempT{counter,10} = (areaAll{2,ii}(i));
%             tempT{counter,11} = (xALLTrials{2,ii}(i));
%             tempT{counter,12} = (yALLTrials{2,ii}(i));
%             tempT{counter,13} = (areaPoly{2,ii}(i));
%             tempT{counter,14} = (dCoef{2,ii}(i));
%             tempT{counter,15} = (dTrend{2,ii}(i));
            counter = counter + 1;
        end
    end
end

dataAll2 = cell2table(tempT,"VariableNames",...
    ["SubjectID" "Condition" "Perf" "Offset" "DC" ...
    "Curv" "Size" "Span" "SameConeProb" "Area" "D" "Dtrend"...
    "X" "Y" "Bias" "prl"]);
% dataAll = cell2table(tempT,"VariableNames",...
%     ["SubjectID" "Condition" "Perf" "Offset" "DC" ...
%     "Curv" "Size" "Span" "SameConeProb" "Area" "X" "Y" "AreaP"...
%     "D" "Dtrend"]);

% writetable(dataAll,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Scripts\crowdingPelli_RAnalysis\myData.txt','Delimiter',' ')  
%%% H1: offset --> reduced accuracy
%   H2: offset --> diff cone stim --> reduced accuracy

modelspec = 'Perf ~ Area*Condition + (1 + Size|SubjectID)';
modelspec2 = 'Perf ~ Offset*SameConeProb + (1 + Offset*SameConeProb|SubjectID)';
modelspec3 = 'Perf ~ Offset*(Condition + SameConeProb) +  (1 + Offset*(Condition + SameConeProb)|SubjectID)';
modelspec_V2 = 'Perf ~ Condition/Offset + (1 + Offset*Condition|SubjectID)'; %looking at the effect on condition embedded in offset

% mdl = fitglm(dataAll(find(dataAll.Condition == 2),:),modelspec)
mdl_1 = fitglme(dataAll,modelspec,'Distribution','binomial')
mdl_1_V2 = fitglme(dataAll,modelspec_V2,'Distribution','binomial')

mdl_2 = fitglme(dataAll,modelspec2,'Distribution','binomial')
mdl_3 = fitglme(dataAll,modelspec3,'Distribution','binomial')


mdl1 = fitglm(dataAll(find(dataAll.Condition == 2),:),'ResponseVar','Perf');
terms = 'Condition +  Size  + DC + Curv + Span'; % terms to remove
Model_Uncrowded = removeTerms(mdl1,terms)

mdl2 = fitglm(dataAll(find(dataAll.Condition == 2),:),'ResponseVar','Perf');
terms = 'Condition + SubjectID + Size'; % terms to remove
Model_Crowded = removeTerms(mdl2,terms)

mdl3 = fitglme(dataAll(find(dataAll.Condition == 2),:),...
    'Perf ~ 1 + Offset + DC + ( 1 | Size)')

modelspec = 'Perf ~ Offset*Condition*SameConeProb - Offset:Condition:SameConeProb';
modelspec = 'Perf ~ 1 + Offset';

mdl = fitglm(dataAll(find(dataAll.Condition == 2),:),modelspec)
mdl = fitglm(dataAll,modelspec,'Distribution','binomial')
figure;plot(mdl)

temp = nanmean(dataAll(find(dataAll.Condition == 1),:).SameConeProb);
plotCompareVars(dataAll,'SameConeProb','Perf','AreaP',temp)
title('68% Ellipse Area')


num = 4 %2
figure;
legnd(1) = plot(1:length(dataAll.X{num}),dataAll.X{num},'-','Color','r');
hold on
legnd(2) = plot(1:length(dataAll.Y{num}),dataAll.Y{num},'-','Color','b');
xlabel('Time(ms)');
ylabel('Position (arcmin)');
legend(legnd,{'X','Y'},'Location','northwest')
xlim([0 500])
ylim([-5 5]);


num = 12; %4 %2
figure;
legnd(1) = plot(sgolayfilt(double(dataAll.X{num}),4,21),...
    sgolayfilt(double(dataAll.Y{num}-3),4,21),'-','Color','k');
hold on
% legnd(2) = plot(1:length(dataAll.Y{num}),dataAll.Y{num},'-','Color','b');
xlabel('X (arcmin)');
ylabel('Y (arcmin)');
% legend(legnd,{'X','Y'},'Location','northwest')
% xlim([0 500])
% ylim([-5 5]);
axis square
axis([-4 4 -4 4])

%% %%%%%
figure;
for ii = 1:length(subjectsAll)
    avConeProb1(ii) = mean([dataAll(find(dataAll.Condition == 1 &...
        dataAll.SubjectID == ii & dataAll.Perf == 1),:).SameConeProb]');
    avConeProb0(ii) = mean([dataAll(find(dataAll.Condition == 1 &...
        dataAll.SubjectID == ii & dataAll.Perf == 0),:).SameConeProb]');
    
    avArea1(ii) = mean([dataAll(find(dataAll.Condition == 1 &...
        dataAll.SubjectID == ii & dataAll.Perf == 1),:).AreaP]');
    avArea0(ii) = mean([dataAll(find(dataAll.Condition == 1 &...
        dataAll.SubjectID == ii & dataAll.Perf == 0),:).AreaP]');
    %     mdl{ii} = fitlm([dataAll(find(dataAll.Condition == 1 &...
%         dataAll.SubjectID == ii),:).Offset]',[dataAll(find(dataAll.Condition == 1 &...
%         dataAll.SubjectID == ii),:).SameConeProb]')
%     subplot(3,4,ii);plot(mdl{ii});
%     title(subjectsAll{ii})
%     xlabel('Offset');
%     ylabel('Probability of Same Cone');
%     pvals(ii) = mdl{ii}.CoefficientCovariance(2,2)
end
figure;
subplot(1,2,1)
errorbar([0 1],[mean(avConeProb0) mean(avConeProb1)],...
    [sem(avConeProb0) sem(avConeProb1)],'-o');
axis([-.5 1.5 .1 .5])
xticks([0 1])
xticklabels({'Wrong ','Correct'});
ylabel('Cone Overlap Probability');
xlabel('Trial Type');
subplot(1,2,2)
errorbar([0 1],[mean(avArea0) mean(avArea1)],...
    [sem(avArea0) sem(avArea1)],'-o');
axis([-.5 1.5 5 25])
xticks([0 1])
xticklabels({'Wrong ','Correct'});
ylabel('Area');
xlabel('Trial Type')


mdl{1}
figure;
[~,p,b,r] = LinRegression(...
    avConeProb,...%x
    avOffsetProb,... %y
    0,NaN,1,0);
xlabel('Average Cone Prob/Subject');
ylabel('Average Offset/Subject');


temp = nanmean(dataAll(find(dataAll.Condition == -1),:).SameConeProb);
figure;
errorbar([(mean(dataAll(find(dataAll.Condition == -1),:).SameConeProb(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb < temp))')),...
    (mean(dataAll(find(dataAll.Condition == -1),:).SameConeProb(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb > temp))'))],...
    [mean(dataAll(find(dataAll.Condition == -1),:).Perf(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb < temp))') ...
    mean(dataAll(find(dataAll.Condition == -1),:).Perf(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb > temp))')],...
    [sem(dataAll(find(dataAll.Condition == -1),:).Perf(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb < temp))') ...
    sem(dataAll(find(dataAll.Condition == -1),:).Perf(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb > temp))')]);
hold on
ylabel('Performance')
ylabel('Same Cone F&T Stimulated Probability')
yyaxis right
errorbar([(mean(dataAll(find(dataAll.Condition == -1),:).SameConeProb(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb < temp))')),...
    (mean(dataAll(find(dataAll.Condition == -1),:).SameConeProb(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb > temp))'))],...
    [(mean(dataAll(find(dataAll.Condition == -1),:).Offset(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb < temp))')),...
    (mean(dataAll(find(dataAll.Condition == -1),:).Offset(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb > temp))'))],...
    [(sem(dataAll(find(dataAll.Condition == -1),:).Offset(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb < temp))')),...
    (sem(dataAll(find(dataAll.Condition == -1),:).Offset(...
    find(dataAll(find(dataAll.Condition == -1),:).SameConeProb > temp))'))]);
% ylim([4 10])
ylabel('Offset')
title('Uncrowded');

figure;
newData = [dataAll.Condition] == 1;
mdl1 = fitlm([newData.SameConeProb],[newData.AreaP])

%%cone%%
indSubBinUpper = ones(1,length(subjectsAll))*50;
indSubBinLower = ones(1,length(subjectsAll))*40;

%%span%%%
% indSubBinUpper(1) = 65;
% indSubBinLower(1) = 40;
% indSubBinUpper(3) = 50;
% indSubBinLower(3) = 45;
% indSubBinUpper(5) = 50;
% indSubBinLower(5) = 45;
% indSubBinUpper(11) = 65;
% indSubBinLower(11) = 35;
% indSubBinUpper(13) = 50;
% indSubBinLower(13) = 45;

%%
% Area
smallD = [];
largeD = [];
indSubBinUpper = ones(1,length(subjectsAll))*40;
indSubBinLower = ones(1,length(subjectsAll))*40;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,areaPoly,'Area',1,saveStimInfo,smallD,largeD);
[indSubBinUpper,indSubBinLower] = indicateAreaBinCro(subjectsAll);
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,areaPoly,'Area',2,saveStimInfo,smallD,largeD);
[~,pU] = ttest(smallD.Unc.Performance,largeD.Unc.Performance);
[~,pC] = ttest(smallD.Cro.Performance,largeD.Cro.Performance);

makePlotPerfDiffUnityLine([smallD.Unc.Performance;smallD.Cro.Performance],...
    [largeD.Unc.Performance; largeD.Cro.Performance], pU, pC);
suptitle('Area')

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/AreaUvsC.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/AreaUvsC.epsc');


figure;
errorbar([1 2],[mean(smallD.Unc.Performance),...
    mean(largeD.Unc.Performance)],[sem(smallD.Unc.Performance),...
    sem(largeD.Unc.Performance)],'-d','Color','b',...
    'MarkerFaceColor','b','MarkerSize',12);
hold on
errorbar([1 2],[mean(smallD.Cro.Performance),...
    mean(largeD.Cro.Performance)],[sem(smallD.Cro.Performance),...
    sem(largeD.Cro.Performance)],'-d','Color','r',...
    'MarkerFaceColor','r','MarkerSize',12);
xlim([0.5 2.5])
xticks([1 2])
xticklabels({'Small','Large'})
xlabel('Fixation Area')
ylabel('Proportion Correct')

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/AreaUvsCErrorBar.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/AreaUvsCErrorBar.epsc');

%%
%D
smallD = [];
largeD = [];
indSubBinUpper = ones(1,length(subjectsAll))*40;
indSubBinLower = ones(1,length(subjectsAll))*40;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,dCoef,'D',1,saveStimInfo,smallD,largeD);
% [indSubBinUpper,indSubBinLower] = indicateAreaBinCro(subjectsAll);
indSubBinUpper = ones(1,length(subjectsAll))*40;
indSubBinLower = ones(1,length(subjectsAll))*40;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,dCoef,'D',2,saveStimInfo,smallD,largeD);
[~,pU] = ttest(smallD.Unc.Performance,largeD.Unc.Performance);
[~,pC] = ttest(smallD.Cro.Performance,largeD.Cro.Performance);

makePlotPerfDiffUnityLine([smallD.Unc.Performance;smallD.Cro.Performance],...
    [largeD.Unc.Performance; largeD.Cro.Performance], pU, pC);
suptitle('D')
%%
%DTrend
smallD = [];
largeD = [];
indSubBinUpper = ones(1,length(subjectsAll))*40;
indSubBinLower = ones(1,length(subjectsAll))*40;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,dTrend,'Dtrend',1,saveStimInfo,smallD,largeD);
% [indSubBinUpper,indSubBinLower] = indicateAreaBinCro(subjectsAll);
indSubBinUpper = ones(1,length(subjectsAll))*50;
indSubBinLower = ones(1,length(subjectsAll))*50;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,dTrend,'Dtrend',2,saveStimInfo,smallD,largeD);
[~,pU] = ttest(smallD.Unc.Performance,largeD.Unc.Performance);
[~,pC] = ttest(smallD.Cro.Performance,largeD.Cro.Performance);

makePlotPerfDiffUnityLine([smallD.Unc.Performance;smallD.Cro.Performance],...
    [largeD.Unc.Performance; largeD.Cro.Performance], pU, pC);
suptitle('D trend')
%%
%PRL 
smallD = [];
largeD = [];
indSubBinUpper = ones(1,length(subjectsAll))*40;
indSubBinLower = ones(1,length(subjectsAll))*40;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,prl,'prl',1,saveStimInfo,smallD,largeD);
% [indSubBinUpper,indSubBinLower] = indicateAreaBinCro(subjectsAll);
indSubBinUpper = ones(1,length(subjectsAll))*50;
indSubBinLower = ones(1,length(subjectsAll))*50;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,prl,'prl',2,saveStimInfo,smallD,largeD);
[~,pU] = ttest(smallD.Unc.Performance,largeD.Unc.Performance);
[~,pC] = ttest(smallD.Cro.Performance,largeD.Cro.Performance);

makePlotPerfDiffUnityLine([smallD.Unc.Performance;smallD.Cro.Performance],...
    [largeD.Unc.Performance; largeD.Cro.Performance], pU, pC);
suptitle('PRL')
% %%
% %Cone Prob
smallD = [];
largeD = [];
indSubBinUpper = ones(1,length(subjectsAll))*40;
indSubBinLower = ones(1,length(subjectsAll))*40;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,probSameConesforTrace,'SameConeProb',1,saveStimInfo,smallD,largeD);
% [indSubBinUpper,indSubBinLower] = indicateAreaBinCro(subjectsAll);
indSubBinUpper = ones(1,length(subjectsAll))*50;
indSubBinLower = ones(1,length(subjectsAll))*50;
[smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,probSameConesforTrace,'SameConeProb',2,saveStimInfo,smallD,largeD);
[~,pU] = ttest(smallD.Unc.Performance,largeD.Unc.Performance);
[~,pC] = ttest(smallD.Cro.Performance,largeD.Cro.Performance);

makePlotPerfDiffUnityLine([smallD.Unc.Performance;smallD.Cro.Performance],...
    [largeD.Unc.Performance; largeD.Cro.Performance], pU, pC);
suptitle('Cone Prob')
%%
%Bias

%%
%ConeProb
for ii = 1:length(subjectsAll)
    idxTrials = find(dataAll.SubjectID == ii & ...
        dataAll.Condition == 1 & ...
        dataAll.Area < 200);
    halfwayPoint = mean(dataAll.Area(idxTrials));
    halfwayPointSTD = std(dataAll.Area(idxTrials));

    perfSmall(ii) = mean(dataAll.SameConeProb(find(dataAll.SubjectID == ii & ...
        dataAll.Condition == 1 & ...
        dataAll.Area < 200 & ...
        dataAll.Area < halfwayPoint)));
    perfLarge(ii) = mean(dataAll.SameConeProb(find(dataAll.SubjectID == ii & ...
        dataAll.Condition == 1 & ...
        dataAll.Area < 200 & ...
        dataAll.Area > halfwayPoint)));
end

figure;
errorbar([1 2],[mean(perfSmall),...
    mean(perfLarge)],[sem(perfSmall),...
    sem(perfLarge)],'-d','Color','g',...
    'MarkerFaceColor','g','MarkerSize',12);
% hold on
% errorbar([1 2],[mean(smallD.Cro.Performance),...
%     mean(largeD.Cro.Performance)],[sem(smallD.Cro.Performance),...
%     sem(largeD.Cro.Performance)],'-d','Color','r',...
%     'MarkerFaceColor','r','MarkerSize',12);
xlim([0.5 2.5])
xticks([1 2])
xticklabels({'Small','Large'})
xlabel('Fixation Area')
ylabel({'Probability of Cone Being Stimulated','by both Target and Flanker'})
[~,pC] = ttest(perfSmall,perfLarge);

%% Group by small large D THRESHOLDS
indSubBinUpper = ones(1,length(subjectsAll))*50;
indSubBinLower = ones(1,length(subjectsAll))*50;

for ii = 1:length(subjectsAll) %Uncrowded
    c = 1;
    forPercent(1,:) = ones(1,length(subjectsAll))*indSubBinUpper(ii);
    forPercent(2,:) = ones(1,length(subjectsAll))*indSubBinLower(ii);
    bin(ii,1) = prctile(dTrend{c, ii},forPercent(1,ii),"all");
    bin(ii,2) = prctile(dTrend{c, ii},forPercent(2,ii),"all");
    
    smallD.Unc.TrialsIdx = find([dataAll.SubjectID] == ii &...
        [dataAll.Condition] == -1 & ...
        [dataAll.Dtrend] < bin(ii,2));
    largeD.Unc.TrialsIdx = find([dataAll.SubjectID] == ii &...
        [dataAll.Condition] == -1 & ...
        [dataAll.Dtrend] > bin(ii,1));
    smallD.Unc.trialNum(ii) = length(smallD.Unc.TrialsIdx);
    largeD.Unc.trialNum(ii) = length(largeD.Unc.TrialsIdx);
    
     [smallD.Unc.thresh(ii)] = ...
        psyfitCrowding(dataAll.Size(smallD.Unc.TrialsIdx), ...
        dataAll.Perf(smallD.Unc.TrialsIdx), 'DistType', ...
        'Normal','Chance', .25, 'Extra');
    
    [largeD.Unc.thresh(ii)] = ...
        psyfitCrowding(dataAll.Size(largeD.Unc.TrialsIdx), ...
        dataAll.Perf(largeD.Unc.TrialsIdx), 'DistType', ...
        'Normal','Chance', .25, 'Extra');
%     smallD.Unc.Performance(ii) = mean(dataAll.Perf(smallD.Unc.TrialsIdx));
%     largeD.Unc.Performance(ii) = mean(dataAll.Perf(largeD.Unc.TrialsIdx));
% 
%     smallD.Unc.NumTrials(ii) = length(dataAll.Perf(smallD.Unc.TrialsIdx));
%     largeD.Unc.NumTrials(ii) = length(dataAll.Perf(largeD.Unc.TrialsIdx));
end

for ii = 1:length(subjectsAll) %Crowded
    c = 2;
    forPercent(1,:) = ones(1,length(subjectsAll))*indSubBinUpper(ii);
    forPercent(2,:) = ones(1,length(subjectsAll))*indSubBinLower(ii);
    bin(ii,1) = prctile(dTrend{c, ii},forPercent(1,ii),"all");
    bin(ii,2) = prctile(dTrend{c, ii},forPercent(2,ii),"all");
    
    smallD.Cro.TrialsIdx = find([dataAll.SubjectID] == ii &...
        [dataAll.Condition] == 1 & ...
        [dataAll.Dtrend] < bin(ii,2));
    largeD.Cro.TrialsIdx = find([dataAll.SubjectID] == ii &...
        [dataAll.Condition] == 1 & ...
        [dataAll.Dtrend] > bin(ii,1));
    
    smallD.Cro.trialNum(ii) = length(smallD.Cro.TrialsIdx);
    largeD.Cro.trialNum(ii) = length(largeD.Cro.TrialsIdx);
    
     [smallD.Cro.thresh(ii)] = ...
        psyfitCrowding(dataAll.Size(smallD.Cro.TrialsIdx), ...
        dataAll.Perf(smallD.Cro.TrialsIdx), 'DistType', ...
        'Normal','Chance', .25, 'Extra');
    
    [largeD.Cro.thresh(ii)] = ...
        psyfitCrowding(dataAll.Size(largeD.Cro.TrialsIdx), ...
        dataAll.Perf(largeD.Cro.TrialsIdx), 'DistType', ...
        'Normal','Chance', .25, 'Extra');
    
%     smallD.Cro.Performance(ii) = mean(dataAll.Perf(smallD.Cro.TrialsIdx));
%     largeD.Cro.Performance(ii) = mean(dataAll.Perf(largeD.Cro.TrialsIdx));
% 
%     smallD.Cro.NumTrials(ii) = length(dataAll.Perf(smallD.Cro.TrialsIdx));
%     largeD.Cro.NumTrials(ii) = length(dataAll.Perf(largeD.Cro.TrialsIdx));
end

[~,pU] = ttest(smallD.Unc.thresh,largeD.Unc.thresh)
[~,pC] = ttest(smallD.Cro.thresh,largeD.Cro.thresh);

[h,p] = ttest([smallD.Cro.thresh-smallD.Unc.thresh],...
        [largeD.Cro.thresh - largeD.Unc.thresh])

% 
figure;
subplot(1,2,1)
plot(smallD.Unc.thresh,largeD.Unc.thresh,'o');
title(sprintf('Uncrowded %.3f', pU));
line([.2 2],[.2 2])
axis square
hold on
errorbar(mean(smallD.Unc.thresh),mean(largeD.Unc.thresh),...
    sem(smallD.Unc.thresh),sem(smallD.Unc.thresh),...
    sem(largeD.Unc.thresh),sem(largeD.Unc.thresh),...
    'o','MarkerFaceColor','k','Color','k')
axis([.5 2 .5 2])

subplot(1,2,2)
plot(smallD.Cro.thresh,largeD.Cro.thresh,'o');
title(sprintf('Crowded %.3f',pC));
line([.2 2],[.2 2])
axis square
hold on
errorbar(mean(smallD.Cro.thresh),mean(largeD.Cro.thresh),...
    sem(smallD.Cro.thresh),sem(smallD.Cro.thresh),...
    sem(largeD.Cro.thresh),sem(largeD.Cro.thresh),...
    'o','MarkerFaceColor','k','Color','k')
axis([.5 2 .5 2])


figure;
for ii = [1:2,4:12]
    plot([1 2],[smallD.Cro.thresh(ii)-smallD.Unc.thresh(ii)...
        largeD.Cro.thresh(ii) - largeD.Unc.thresh(ii)],'-o');
hold on
% plot([1 2],[smallD.Unc.thresh(ii) largeD.Unc.thresh(ii)],'-o');
% hold on
% plot([3 4],[smallD.Cro.thresh(ii) largeD.Cro.thresh(ii)],'-o');
end
errorbar([1 2],[mean([smallD.Cro.thresh-smallD.Unc.thresh])...
        mean([largeD.Cro.thresh - largeD.Unc.thresh])],...
        [sem([smallD.Cro.thresh-smallD.Unc.thresh])...
        sem([largeD.Cro.thresh - largeD.Unc.thresh])],'-o',...
        'MarkerFaceColor','k','Color','k');
ylabel('Threshold Difference (C - U)');
xlabel('PRL')
xticks([1:2])
xticklabels({'Small','Large'});
xlim([0 3])

% xticks([1:4])
% xticklabels({'Small U','Large U','Small C','Large C'});

%%
indSubBinUpper = ones(1,length(subjectsAll))*70;
indSubBinLower = ones(1,length(subjectsAll))*40;
% indSubBinUpper(1) = 85;
% indSubBinLower(1) = 20;
% indSubBinUpper(2) = 60;
% indSubBinLower(2) = 20;
% indSubBinUpper(3) = 89;
% indSubBinLower(3) = 70;
% indSubBinUpper(4) = 70;
% indSubBinLower(4) = 20;
% indSubBinUpper(5) = 70;
% indSubBinLower(5) = 35;
% indSubBinUpper(6) = 90;
% indSubBinLower(6) = 65;
% indSubBinUpper(7) = 80;
% indSubBinLower(7) = 40;
% indSubBinUpper(8) = 60;
% indSubBinLower(8) = 30;
% indSubBinUpper(9) = 80;
% indSubBinLower(9) = 30;
% indSubBinUpper(10) = 90;
% indSubBinLower(10) = 20;
% indSubBinUpper(11) = 97;
% indSubBinLower(11) = 23;
% indSubBinUpper(12) = 60;
% indSubBinLower(12) = 25;
% indSubBinUpper(13) = 80;
% indSubBinLower(13) = 25;
maxVal = 300;
var = areaPoly;%areaPoly;%areaAll;%span;
% figure;
% counter = 1;
for ii = 1:length(subjectsAll)%[1:10 12];%
    for c = 1
        forPercent(1,:) = ones(1,length(subjectsAll))*indSubBinUpper(ii);
        forPercent(2,:) = ones(1,length(subjectsAll))*indSubBinLower(ii);
        bin(ii,1) = prctile(var{c, ii},forPercent(1,ii),"all");
        bin(ii,2) = prctile(var{c, ii},forPercent(2,ii),"all");
        smallPerf(c,ii) = mean(perfCur{c,ii}(find(var{c,ii} < bin(ii,2) & ...
           var{c,ii} < maxVal )));
        lengthSP(c,ii) = length(perfCur{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        largePerf(c,ii) = mean(perfCur{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
        lengthLP(c,ii) = length(perfCur{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
        findMeanSmall(c,ii) = mean(var{c,ii}(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal));
        findMeanLarge(c,ii) = mean(var{c,ii}(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal));
        meanpercentOnTS(c,ii) = mean(timeSpentOnTarget{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        meanpercentOnTL(c,ii) = mean(timeSpentOnTarget{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));%      
        meanpercentOverlapS(c,ii) = mean(probSameConesforTrace{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        meanpercentOverlapL(c,ii) = mean(probSameConesforTrace{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
       
         idxSmallTrials = find([dataAll.SubjectID] == ii &...
           [dataAll.Area] < bin(ii,2) & ...
           [dataAll.Condition] == -1);
        idxLargeTrials = find([dataAll.SubjectID] == ii &...
           [dataAll.Area] > bin(ii,1)& ...
           [dataAll.Condition] == -1);          
       
       dcSmallArea(c,ii) = mean(dataAll.DC(idxSmallTrials));
       dcSmallPerf(c,ii) = mean(dataAll.Perf(idxSmallTrials));
       dcLargeArea(c,ii) = mean(dataAll.DC(idxLargeTrials));
       dcLargePerf(c,ii) = mean(dataAll.Perf(idxLargeTrials));
    end
end
indSubBinUpper = ones(1,length(subjectsAll))*70;
indSubBinLower = ones(1,length(subjectsAll))*40;
indSubBinUpper(1) = 75;
indSubBinLower(1) = 30;
indSubBinUpper(2) = 90;
indSubBinLower(2) = 85;
indSubBinUpper(3) = 89;
indSubBinLower(3) = 70;
indSubBinUpper(4) = 70;
indSubBinLower(4) = 20;
indSubBinUpper(5) = 40;
indSubBinLower(5) = 30;
indSubBinUpper(6) = 90;
indSubBinLower(6) = 59;
indSubBinUpper(7) = 90;
indSubBinLower(7) = 30;
indSubBinUpper(8) = 60;
indSubBinLower(8) = 50;
indSubBinUpper(9) = 60;
indSubBinLower(9) = 30;
indSubBinUpper(10) = 9;
indSubBinLower(10) = 90;
indSubBinUpper(11) = 50;
indSubBinLower(11) = 33;
indSubBinUpper(12) = 50;
indSubBinLower(12) = 25;
indSubBinUpper(14) = 70;
indSubBinLower(14) = 30;

% indSubBinUpper = ones(1,length(subjectsAll))*90;
% indSubBinLower = ones(1,length(subjectsAll))*10;
for ii = 1:length(subjectsAll)%[1:10 12];%
    for c = 2 
          forPercent(1,:) = ones(1,length(subjectsAll))*indSubBinUpper(ii);
        forPercent(2,:) = ones(1,length(subjectsAll))*indSubBinLower(ii);
        bin(ii,1) = prctile(var{c, ii},forPercent(1,ii),"all");
        bin(ii,2) = prctile(var{c, ii},forPercent(2,ii),"all");
        smallPerf(c,ii) = nanmean(perfCur{c,ii}(find(var{c,ii} < bin(ii,2) & ...
           var{c,ii} < maxVal )));
        lengthSP(c,ii) = length(perfCur{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        largePerf(c,ii) = mean(perfCur{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
        lengthLP(c,ii) = length(perfCur{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
        findMeanSmall(c,ii) = mean(var{c,ii}(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal));
        findMeanLarge(c,ii) = mean(var{c,ii}(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal));
        meanpercentOnTS(c,ii) = mean(timeSpentOnTarget{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        meanpercentOnTL(c,ii) = mean(timeSpentOnTarget{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));%      
        meanpercentOverlapS(c,ii) = mean(probSameConesforTrace{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        meanpercentOverlapL(c,ii) = mean(probSameConesforTrace{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));%    
       
       idxSmallTrials = find([dataAll.SubjectID] == ii &...
           [dataAll.Area] < bin(ii,2)& ...
           [dataAll.Condition] == 1);
        idxLargeTrials = find([dataAll.SubjectID] == ii &...
           [dataAll.Area] > bin(ii,1)& ...
           [dataAll.Condition] == 1);       
       
       dcSmallArea(c,ii) = mean(dataAll.DC(idxSmallTrials));
       dcSmallPerf(c,ii) = mean(dataAll.Perf(idxSmallTrials));
       dcLargeArea(c,ii) = mean(dataAll.DC(idxLargeTrials));
       dcLargePerf(c,ii) = mean(dataAll.Perf(idxLargeTrials));

    end
end


[~,pU] = ttest2(smallPerf(1,:),largePerf(1,:));
[~,pC] = ttest2(smallPerf(2,:),largePerf(2,:))
lengthSP
lengthLP
mean(smallPerf(2,:))
mean(largePerf(2,:))

mean(meanpercentOverlapS(2,:))
mean(meanpercentOverlapL(2,:))

figure;
plot(smallPerf(2,:),largePerf(2,:),'o');
line([.4 1],[.4 1])
subplot(1,2,1)
plot(smallPerf(1,:),largePerf(1,:),'o');
axis([0.3 1 0.3 1])
xlabel('Small Area');
ylabel('Large Area');
line([0 1],[0 1])
hold on
errorbar(mean(smallPerf(1,:)),...
mean(largePerf(1,:)),sem(smallPerf(1,:)),...
sem(largePerf(1,:)),sem(smallPerf(1,:)),...
sem(largePerf(1,:)),'o','MarkerSize',10);
title(sprintf('Uncrowded, p = %.3f', pU))
axis square
subplot(1,2,2)
plot(smallPerf(2,:),largePerf(2,:),'o');
axis([0.3 1 0.3 1])
xlabel('Small Area');
ylabel('Large Area');
line([0 1],[0 1])
hold on
errorbar(mean(smallPerf(2,:)),...
mean(largePerf(2,:)),sem(smallPerf(2,:)),sem(smallPerf(2,:)),...
sem(largePerf(2,:)),sem(largePerf(2,:)),'o','MarkerSize',10)
title(sprintf('Crowded, p = %.3f',pC))
axis square
%% same as above but now JUST for D

indSubBinUpper = ones(1,length(subjectsAll))*70;
indSubBinLower = ones(1,length(subjectsAll))*40;
% indSubBinUpper(1) = 85;
% indSubBinLower(1) = 20;
% indSubBinUpper(2) = 60;
% indSubBinLower(2) = 20;
% indSubBinUpper(3) = 89;
% indSubBinLower(3) = 70;
% indSubBinUpper(4) = 70;
% indSubBinLower(4) = 20;
% indSubBinUpper(5) = 70;
% indSubBinLower(5) = 35;
% indSubBinUpper(6) = 90;
% indSubBinLower(6) = 65;
% indSubBinUpper(7) = 80;
% indSubBinLower(7) = 40;
% indSubBinUpper(8) = 60;
% indSubBinLower(8) = 30;
% indSubBinUpper(9) = 80;
% indSubBinLower(9) = 30;
% indSubBinUpper(10) = 90;
% indSubBinLower(10) = 20;
% indSubBinUpper(11) = 97;
% indSubBinLower(11) = 23;
% indSubBinUpper(12) = 60;
% indSubBinLower(12) = 25;
% indSubBinUpper(13) = 80;
% indSubBinLower(13) = 25;
maxVal = 300;
var = areaPoly;%areaPoly;%areaAll;%span;
% figure;
% counter = 1;
for ii = 1:length(subjectsAll)%[1:10 12];%
    for c = 1
        forPercent(1,:) = ones(1,length(subjectsAll))*indSubBinUpper(ii);
        forPercent(2,:) = ones(1,length(subjectsAll))*indSubBinLower(ii);
        bin(ii,1) = prctile(var{c, ii},forPercent(1,ii),"all");
        bin(ii,2) = prctile(var{c, ii},forPercent(2,ii),"all");
        smallPerf(c,ii) = mean(perfCur{c,ii}(find(var{c,ii} < bin(ii,2) & ...
           var{c,ii} < maxVal )));
        lengthSP(c,ii) = length(perfCur{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        largePerf(c,ii) = mean(perfCur{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
        lengthLP(c,ii) = length(perfCur{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
        findMeanSmall(c,ii) = mean(var{c,ii}(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal));
        findMeanLarge(c,ii) = mean(var{c,ii}(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal));
        meanpercentOnTS(c,ii) = mean(timeSpentOnTarget{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        meanpercentOnTL(c,ii) = mean(timeSpentOnTarget{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));%      
        meanpercentOverlapS(c,ii) = mean(probSameConesforTrace{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        meanpercentOverlapL(c,ii) = mean(probSameConesforTrace{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
       
         idxSmallTrials = find([dataAll.SubjectID] == ii &...
           [dataAll.AreaP] < bin(ii,2) & ...
           [dataAll.Condition] == -1);
        idxLargeTrials = find([dataAll.SubjectID] == ii &...
           [dataAll.AreaP] > bin(ii,1)& ...
           [dataAll.Condition] == -1);          
       
       dcSmallArea(c,ii) = mean(dataAll.DC(idxSmallTrials));
       dcSmallPerf(c,ii) = mean(dataAll.Perf(idxSmallTrials));
       dcLargeArea(c,ii) = mean(dataAll.DC(idxLargeTrials));
       dcLargePerf(c,ii) = mean(dataAll.Perf(idxLargeTrials));
    end
end
indSubBinUpper = ones(1,length(subjectsAll))*70;
indSubBinLower = ones(1,length(subjectsAll))*40;
indSubBinUpper(1) = 75;
indSubBinLower(1) = 30;
indSubBinUpper(2) = 90;
indSubBinLower(2) = 85;
indSubBinUpper(3) = 89;
indSubBinLower(3) = 70;
indSubBinUpper(4) = 70;
indSubBinLower(4) = 20;
indSubBinUpper(5) = 40;
indSubBinLower(5) = 30;
indSubBinUpper(6) = 90;
indSubBinLower(6) = 59;
indSubBinUpper(7) = 90;
indSubBinLower(7) = 30;
indSubBinUpper(8) = 60;
indSubBinLower(8) = 50;
indSubBinUpper(9) = 60;
indSubBinLower(9) = 30;
indSubBinUpper(10) = 9;
indSubBinLower(10) = 90;
indSubBinUpper(11) = 50;
indSubBinLower(11) = 33;
indSubBinUpper(12) = 50;
indSubBinLower(12) = 25;
indSubBinUpper(14) = 70;
indSubBinLower(14) = 30;

% indSubBinUpper = ones(1,length(subjectsAll))*90;
% indSubBinLower = ones(1,length(subjectsAll))*10;
for ii = 1:length(subjectsAll)%[1:10 12];%
    for c = 2 
        forPercent(1,:) = ones(1,length(subjectsAll))*indSubBinUpper(ii);
        forPercent(2,:) = ones(1,length(subjectsAll))*indSubBinLower(ii);
        bin(ii,1) = prctile(var{c, ii},forPercent(1,ii),"all");
        bin(ii,2) = prctile(var{c, ii},forPercent(2,ii),"all");
        smallPerf(c,ii) = nanmean(perfCur{c,ii}(find(var{c,ii} < bin(ii,2) & ...
           var{c,ii} < maxVal )));
        lengthSP(c,ii) = length(perfCur{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        largePerf(c,ii) = mean(perfCur{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
        lengthLP(c,ii) = length(perfCur{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));
        findMeanSmall(c,ii) = mean(var{c,ii}(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal));
        findMeanLarge(c,ii) = mean(var{c,ii}(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal));
        meanpercentOnTS(c,ii) = mean(timeSpentOnTarget{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        meanpercentOnTL(c,ii) = mean(timeSpentOnTarget{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));%      
        meanpercentOverlapS(c,ii) = mean(probSameConesforTrace{c,ii}(find(var{c,ii} < bin(ii,2)& ...
           var{c,ii} < maxVal)));
        meanpercentOverlapL(c,ii) = mean(probSameConesforTrace{c,ii}(find(var{c,ii} > bin(ii,1)& ...
           var{c,ii} < maxVal)));%    
       
       idxSmallTrials = find([dataAll.SubjectID] == ii &...
           [dataAll.AreaP] < bin(ii,2)& ...
           [dataAll.Condition] == 1);
        idxLargeTrials = find([dataAll.SubjectID] == ii &...
           [dataAll.AreaP] > bin(ii,1)& ...
           [dataAll.Condition] == 1);       
       
       dcSmallArea(c,ii) = mean(dataAll.DC(idxSmallTrials));
       dcSmallPerf(c,ii) = mean(dataAll.Perf(idxSmallTrials));
       dcLargeArea(c,ii) = mean(dataAll.DC(idxLargeTrials));
       dcLargePerf(c,ii) = mean(dataAll.Perf(idxLargeTrials));

    end
end

[~,pU] = ttest2(smallPerf(1,:),largePerf(1,:));
[~,pC] = ttest2(smallPerf(2,:),largePerf(2,:))
lengthSP
lengthLP
mean(smallPerf(2,:))
mean(largePerf(2,:))

mean(meanpercentOverlapS(2,:))
mean(meanpercentOverlapL(2,:))

figure;
plot(smallPerf(2,:),largePerf(2,:),'o');
line([.4 1],[.4 1])
subplot(1,2,1)
plot(smallPerf(1,:),largePerf(1,:),'o');
axis([0.3 1 0.3 1])
xlabel('Small Area');
ylabel('Large Area');
line([0 1],[0 1])
hold on
errorbar(mean(smallPerf(1,:)),...
mean(largePerf(1,:)),sem(smallPerf(1,:)),...
sem(largePerf(1,:)),sem(smallPerf(1,:)),...
sem(largePerf(1,:)),'o','MarkerSize',10);
title(sprintf('Uncrowded, p = %.3f', pU))
axis square
subplot(1,2,2)
plot(smallPerf(2,:),largePerf(2,:),'o');
axis([0.3 1 0.3 1])
xlabel('Small Area');
ylabel('Large Area');
line([0 1],[0 1])
hold on
errorbar(mean(smallPerf(2,:)),...
mean(largePerf(2,:)),sem(smallPerf(2,:)),sem(smallPerf(2,:)),...
sem(largePerf(2,:)),sem(largePerf(2,:)),'o','MarkerSize',10)
title(sprintf('Crowded, p = %.3f',pC))
axis square

%%

% ylim([.6 .7])
% [h,p] = ttest2((dataAll(find(dataAll.Condition == 1),:).Perf(...
%     find(dataAll(find(dataAll.Condition == 1),:).SameConeProb < temp))'), ...
%     (dataAll(find(dataAll.Condition == 1),:).Perf(...
%     find(dataAll(find(dataAll.Condition == 1),:).SameConeProb > temp))'))

% figure;
% forleg(1) = errorbar([1 2],[mean(findMeanSmall(1,:)) mean(findMeanSmall(2,:))],...
%     [sem(findMeanSmall(1,:)) sem(findMeanSmall(2,:))],'-o');
% hold on
% forleg(2) = errorbar([1 2],[mean(findMeanLarge(1,:)) mean(findMeanLarge(2,:))],...
%     [sem(findMeanLarge(1,:)) sem(findMeanLarge(2,:))],'-o');
% xticks([1 2])
% xlim([0 3])
% xticklabels({'Uncrowded','Crowded'});
% ylabel('Area')
% legend(forleg,{'Small Area','Large Area'});
figure;
forleg(1) = errorbar([.9 1.9],[mean(findMeanSmall(1,:)) mean(findMeanLarge(1,:))],...
    [sem(findMeanSmall(1,:)) sem(findMeanLarge(1,:))],'s','MarkerSize',15);
hold on
forleg(2) = errorbar([1.2 2.2],[mean(findMeanSmall(2,:)) mean(findMeanLarge(2,:))],...
    [sem(findMeanSmall(2,:)) sem(findMeanLarge(2,:))],'s','MarkerSize',15);
xticks([1 2])
xlim([0.5 2.5])
ylim([0 50])
xticklabels({'Small Area','Large Area'});
ylabel('Area')
legend(forleg,{'Uncrowded','Crowded'},'Location','northwest');
[~,ps] = ttest2(findMeanSmall(1,:),findMeanSmall(2,:));
[~,pl] = ttest2(findMeanLarge(1,:),findMeanLarge(2,:));

%% Look at Relative Small and Large Values: PRL, DC, and Curve
% run chunk 154 before
smallPerf = [];
largePerf = [];
prlAll = [];
% nameSeperateVar = dcAllTrials;%dcAllTrials; %prl
figure(50);
figureCounter = 1;
nameVars = {prl,areaPoly,span};%,dcAllTrials,curv};
nameVarsString = {'gaze offset','dcAllTrials','span'};
for t = 2%1%:3
    nameSeperateVar = nameVars{t};
    
    clear findMeanSmall; clear findMeanLarge;
    clear smallPerf; clear largePerf;
    clear smallPRLX; clear largePRLX; %     8
    
    for ii = 1:length(subjectsAll)
        for c = 1:length(conditions)
            if t == 1
                [chooseTrialsS, chooseTrialsL] = determineThreshSize(ii,c);
                findMeanSmall(c,ii) = mean(prl{c, ii}) - (sem(prl{c, ii})*chooseTrialsS);
                findMeanLarge(c,ii) = mean(prl{c, ii}) + (sem(prl{c, ii})*chooseTrialsL);
                if ii == 12 || ii == 11 || ii == 10  || ii == 13
                    findMeanSmall(c,ii) = mean(nameSeperateVar{c, ii}) - (sem(nameSeperateVar{c, ii}));
                    findMeanLarge(c,ii) = mean(nameSeperateVar{c, ii}) + (sem(nameSeperateVar{c, ii}));
                elseif ii == 1
                    findMeanSmall(c,ii) = mean(nameSeperateVar{c, ii}) - (sem(nameSeperateVar{c, ii})*10);
                    findMeanLarge(c,ii) = mean(nameSeperateVar{c, ii}) + (sem(nameSeperateVar{c, ii})*5);  
                elseif ii == 4
                    findMeanSmall(c,ii) = mean(nameSeperateVar{c, ii}) - (sem(nameSeperateVar{c, ii})*4);
                    findMeanLarge(c,ii) = mean(nameSeperateVar{c, ii}) + (sem(nameSeperateVar{c, ii})*2); 
                elseif ii == 6 
                    findMeanSmall(c,ii) = mean(nameSeperateVar{c, ii}) - (sem(nameSeperateVar{c, ii})*7);
                    findMeanLarge(c,ii) = mean(nameSeperateVar{c, ii}) + (sem(nameSeperateVar{c, ii})*9);
                elseif ii == 2
                    findMeanSmall(c,ii) = mean(nameSeperateVar{c, ii}) - (sem(nameSeperateVar{c, ii})*13);
                    findMeanLarge(c,ii) = mean(nameSeperateVar{c, ii}) + (sem(nameSeperateVar{c, ii})*11);
                elseif  ii == 3 
                    findMeanSmall(c,ii) = mean(nameSeperateVar{c, ii}) - (sem(nameSeperateVar{c, ii})*2);
                    findMeanLarge(c,ii) = mean(nameSeperateVar{c, ii}) + (sem(nameSeperateVar{c, ii})*6);
                
                end

            smallPerf(c,ii) = mean(performance{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
            lengthSP(c,ii) = length(performance{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
            largePerf(c,ii) = mean(performance{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));
            lengthLP(c,ii) = length(performance{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));
            smallCP(c,ii) = mean(probSameConesforTrace{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
            largeCP(c,ii) = mean(probSameConesforTrace{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));
            
%             smallVar(c,ii) = mean(areaPoly{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
%             largeVar(c,ii) = mean(areaPoly{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));
            
            meanpercentOnTS(c,ii) = mean(timeSpentOnTarget{c,ii}(find(prl{c,ii} < findMeanSmall(c,ii))));
            meanpercentOnTL(c,ii) = mean(performance{c,ii}(find(prl{c,ii} > findMeanLarge(c,ii))));%                 if  ii == 12 || ii == 2
            %
            elseif t == 2
                forPercent(2,:) = [70,75,85,75,53,82,50,70,70,75,60,55,68];
                forPercent(1,:) = [35,25,20,25,18,20,19,30,30,25,20,20,35];
                bin(ii,1) = prctile(nameSeperateVar{c, ii},forPercent(1,ii),"all");
                bin(ii,2) = prctile(nameSeperateVar{c, ii},forPercent(2,ii),"all");
                
                findMeanSmall(c,ii) = bin(ii,1);%mean(nameSeperateVar{c, ii}) - (std(nameSeperateVar{c, ii})/3);
                findMeanLarge(c,ii) = bin(ii,2);%mean(nameSeperateVar{c, ii}) + (std(nameSeperateVar{c, ii})/3);
            elseif t == 3 
%                 forPercent(2,:) = [55,65,52,65,65,70,70,70,70,70,60,60,52];
%                 forPercent(1,:) = [45,35,48,35,35,30,30,30,30,30,40,40,48];
%                 forPercent(2,:) = [70,75,85,75,53,82,50,70,70,75,68,67,68];
%                 forPercent(1,:) = [35,25,30,25,18,20,19,30,30,25,35,30,35];
                forPercent(2,:) = ones(1,length(subjectsAll))*55;
                forPercent(1,:) = ones(1,length(subjectsAll))*45;
                bin(ii,1) = prctile(nameSeperateVar{c, ii},forPercent(1,ii),"all");
                bin(ii,2) = prctile(nameSeperateVar{c, ii},forPercent(2,ii),"all");
                
                findMeanSmall(c,ii) = bin(ii,1);%mean(nameSeperateVar{c, ii}) - (std(nameSeperateVar{c, ii})/3);
                findMeanLarge(c,ii) = bin(ii,2);%mean(nameSeperateVar{c, ii}) + (std(nameSeperateVar{c, ii})/3);
            end
            
% %             if t == 3
% %                 smallPerf(c,ii) = mean(perfCur{c,ii}(find(nameSeperateVar{c,ii} < findMeanSmall(c,ii))));
% %                 lengthSP(c,ii) = length(perfCur{c,ii}(find(nameSeperateVar{c,ii} < findMeanSmall(c,ii))));
% %                 largePerf(c,ii) = mean(perfCur{c,ii}(find(nameSeperateVar{c,ii} > findMeanLarge(c,ii))));
% %                 lengthLP(c,ii) = length(perfCur{c,ii}(find(nameSeperateVar{c,ii} > findMeanLarge(c,ii))));
% %                 
% %                 meanpercentOnTS(c,ii) = mean(timeSpentOnTarget{c,ii}(find(nameSeperateVar{c,ii} < findMeanSmall(c,ii))));
% %                 meanpercentOnTL(c,ii) = mean(perfCur{c,ii}(find(nameSeperateVar{c,ii} > findMeanLarge(c,ii))));
% %             else
                smallPerf(c,ii) = mean(performance{c,ii}(find(nameSeperateVar{c,ii} < findMeanSmall(c,ii))));
                lengthSP(c,ii) = length(performance{c,ii}(find(nameSeperateVar{c,ii} < findMeanSmall(c,ii))));
                largePerf(c,ii) = mean(performance{c,ii}(find(nameSeperateVar{c,ii} > findMeanLarge(c,ii))));
                lengthLP(c,ii) = length(performance{c,ii}(find(nameSeperateVar{c,ii} > findMeanLarge(c,ii))));
                
                meanpercentOnTS(c,ii) = mean(timeSpentOnTarget{c,ii}(find(nameSeperateVar{c,ii} < findMeanSmall(c,ii))));
                meanpercentOnTL(c,ii) = mean(performance{c,ii}(find(nameSeperateVar{c,ii} > findMeanLarge(c,ii))));
% %             end
            %         figure;
            %         histogram(nameSeperateVar{c, ii},20)
            
            smallPRLX{c,ii} = (xALLTrials{c,ii}(find(nameSeperateVar{c,ii} < findMeanSmall(c,ii))));
            smallPRLY{c,ii} = (yALLTrials{c,ii}(find(nameSeperateVar{c,ii} < findMeanSmall(c,ii))));
            largePRLX{c,ii} = (xALLTrials{c,ii}(find(nameSeperateVar{c,ii} > findMeanLarge(c,ii))));
            largePRLY{c,ii} = (yALLTrials{c,ii}(find(nameSeperateVar{c,ii} > findMeanLarge(c,ii))));
            
         
        end
        
    end

    idx1 = find(lengthSP(1,:) >= 16 & lengthLP(1,:) >= 16); %uncrowded
    idx2 = find(lengthSP(2,:) >= 16 & lengthLP(2,:) >= 16); %crowded
    
    [h,pU] = ttest(smallPerf(1,idx1), largePerf(1,idx1));
    [h,pC] = ttest(smallPerf(2,idx2), largePerf(2,idx2));
    [h,Thp] = ttest([tbl.Uncrowded(:).thresh], [tbl.Crowded(:).thresh]);
    
    %smallPerf(2,idx2) - largePerf(2,idx2)
%     if t == 2
        subplot(3,3,figureCounter)
        figureCounter = figureCounter + 1;
        figure;
        plot([ones(1,length(idx1)); ones(1,length(idx1))*2],...
            [smallPerf(1,idx1); largePerf(1,idx1)],...
            '-');
        hold on;
        errorbar([1 2], [mean(smallPerf(1,idx1)) mean(largePerf(1,idx1))],...
            [sem(smallPerf(1,idx1)) sem(largePerf(1,idx1))],'-o','MarkerFaceColor','k',...
            'Color','k');
        xlim([0 3])
        ylim([.25 1])
        ylabel('performance');
        xticks([1 2])
        xticklabels({sprintf('Small %s',nameVarsString{t}),sprintf('Large %s',nameVarsString{t})});
        subtitle(sprintf('Uncrowded, p = %.3f',pU));
        hold on
        yyaxis right
         errorbar([1 2], [mean(smallCP(1,idx1)) mean(largeCP(1,idx1))],...
            [sem(smallCP(1,idx1)) sem(largeCP(1,idx1))],'-o','MarkerFaceColor','r',...
            'Color','r');
           ylim([.26 .42])
        subplot(3,3,figureCounter)
        figureCounter = figureCounter + 1;
       figure;
        plot([ones(1,length(idx2)); ones(1,length(idx2))*2],[smallPerf(2,idx2); largePerf(2,idx2)],...
            '-');
        hold on;
        errorbar([1 2], [mean(smallPerf(2,idx1)) mean(largePerf(2,idx1))],...
            [sem(smallPerf(2,idx1)) sem(largePerf(2,idx1))],'-o','MarkerFaceColor','k',...
            'Color','k');
        xlim([0 3])
        ylim([.25 1])
        ylabel('performance');
        xticks([1 2])
        xticklabels({sprintf('Small %s',nameVarsString{t}),sprintf('Large %s',nameVarsString{t})});
        subtitle(sprintf('Crowded, p = %.3f',pC));
         hold on
        yyaxis right
         errorbar([1 2], [mean(smallCP(2,idx1)) mean(largeCP(2,idx1))],...
            [sem(smallCP(2,idx1)) sem(largeCP(2,idx1))],'-o','MarkerFaceColor','r',...
            'Color','r');
        ylim([.26 .42])
%     end
    
  % small vs large
    [SubjectOrderDC,SubjectOrderDCIdx] = sort([tbl.Uncrowded.thresh]);
    figure;
    for c = 1:length(conditions)
        for i = 1:length(subjectsAll)
            ii = i;%(SubjectOrderDCIdx(i));
            stimSizeDesired = find(saveStimInfo{c,ii}(3,:) ==...
                max(saveStimInfo{c,ii}(3,:)));
            stimSizeMost = saveStimInfo{c,ii}(1,stimSizeDesired);
            if c == 2
                coneSpacing = 0:.5:30;
                fightFlanker = [];
                percentAllStim = [];
                for j = 1:length(smallPRLX{c,ii})
                    for s = 1:length(coneSpacing)
                        
                        for m = 1:length(smallPRLX{c,ii}{j})
                            
                            acc = 0.5;
                            activatedCones = ...
                                round(abs(smallPRLX{c,ii}{j}(m)))/acc*acc;
                            if activatedCones == coneSpacing(s) 
                                percentStim{ii}.stim{j}(s,m) = 1;
                                percentStim{ii}.stim{j}(s+1,m) = 1;
                                 if s > 1
                                     percentStim{ii}.stim{j}(s-1,m) = 1;
                                 end
                            elseif activatedCones == coneSpacing(s) + ...
                                    round(1.4*stimSizeMost)/acc*acc
                                percentStim{ii}.stim{j}(s,m) = 2;
                                if s > 1
                                    percentStim{ii}.stim{j}(s-1,m) = 2;
                                end
                                percentStim{ii}.stim{j}(s+1,m) = 2; 
                            else
                                percentStim{ii}.stim{j}(s,m) = 0;
                                
                            end
                        end
                    end
                    percentAllStim = [percentAllStim percentStim{ii}.stim{j}];
                    %                     rightFlanker = abs(smallPRLX{c,ii}{j})+(stimSizeMost*1.4);
                    %                     C = ismember(round(abs(smallPRLX{c,ii}{j})),unique(round(rightFlanker)));
                    %                     numOverlapConesIndTrial1(j) = (sum(C)/length(C));
                end
%                 numOverlappingConesSmall(ii,1) = (mean(numOverlapConesIndTrial1));
                counter = 1;
                for s = 1:length(coneSpacing)
                    if length(find(percentAllStim(s,:) == 0)) == length(percentAllStim(s,:))
                        continue;
                    end
                    indConePercent.Target(counter) = length(find(percentAllStim(s,:) == 1))/...
                        length(percentAllStim(s,:));%length(percentAllStim);
                    indConePercent.Flankers(counter) = length(find(percentAllStim(s,:) == 2))/...
                       length(percentAllStim(counter,:));% length(percentAllStim);
                    counter = counter + 1;
                   %                     indConePercent.None(s) = length(find(percentAllStim(s,:) == 0))/...
%                         length(percentAllStim);
                end
                probSameConeSmallOffset(ii) = length(find(indConePercent.Flankers > 0 & ...
                    indConePercent.Flankers > 0))/length(indConePercent.Flankers);
%                 probNoConeSmallOffset(ii) = mean(indConePercent.None);

                fightFlanker = [];
                percentAllStim = [];
                clear indConePercent
                for j = 1:length(largePRLX{c,ii})
                    for s = 1:length(coneSpacing)
                        for u = 1:length(largePRLX{c,ii}{j})
                            acc = 0.5;
                            activatedCones = ...
                                round(abs(largePRLX{c,ii}{j}(u)))/acc*acc;
                            if activatedCones == coneSpacing(s)
                                percentStim{ii}.stim{j}(s,u) = 1;
                                
                            elseif activatedCones == coneSpacing(s) + round(1.4*stimSizeMost)/acc*acc
                                percentStim{ii}.stim{j}(s,u) = 2;
                                
                            else
                                percentStim{ii}.stim{j}(s,u) = 0;
                                
                            end
                        end
                    end
                    percentAllStim = [percentAllStim percentStim{ii}.stim{j}];
                    %                     rightFlanker = abs(largePRLX{c,ii}{j})+(stimSizeMost*1.4);
                    %                     C = ismember(abs(round(largePRLX{c,ii}{j})),unique(round(rightFlanker)));% ...
                    %                     numOverlapConesIndTrial2(j) = (sum(C)/length(C));
                end
                counter = 1;
                for s = 1:length(coneSpacing)
                    if length(find(percentAllStim(s,:) == 0)) == length(percentAllStim(s,:))
                        continue;
                    end
                    indConePercent.Target(counter) = length(find(percentAllStim(s,:) == 1))/...
                        length(percentAllStim(s,:));%length(percentAllStim);
                    indConePercent.Flankers(counter) = length(find(percentAllStim(s,:) == 2))/...
                       length(percentAllStim(counter,:));% length(percentAllStim);
                    counter = counter + 1;
                   %                     indConePercent.None(s) = length(find(percentAllStim(s,:) == 0))/...
%                         length(percentAllStim);
                end
                probSameConeLargeOffset(ii) = length(find(indConePercent.Flankers > 0 & ...
                    indConePercent.Flankers > 0))/length(indConePercent.Flankers);
%                 numOverlappingConesLarge(ii,1) = (mean(numOverlapConesIndTrial2));
            end
        end
    end

    idx = idx2;
    subplot(3,3,figureCounter)
    figureCounter = figureCounter + 1;
   for i = idx
       plot([0 1],[probSameConeSmallOffset(i) probSameConeLargeOffset(i)],'-o');
       hold on
   end
   [h,p] = ttest(probSameConeSmallOffset(idx),probSameConeLargeOffset(idx));
   errorbar([0 1],[mean(probSameConeSmallOffset(idx)) mean(probSameConeLargeOffset(idx))],...
        [sem(probSameConeSmallOffset(idx)) sem(probSameConeLargeOffset(idx))], '-o',...
        'Color','k','MarkerFaceColor','k');
    hold on
    xlim([-.5 1.5])
    xticks([0 1])
    
    xticklabels({sprintf('Small %s',nameVarsString{t}),sprintf('Large %s',nameVarsString{t})});
    
    ylabel({'Probability of the Same Cone Stimulated by', 'both Target and Flanker'})
    ylim([0.3 1])
    %     text(0.25,.9,sprintf('p < %.2f',p),'FontSize',12);
    subtitle(sprintf('%s, p = %.3f',conditions{c}, p));

%     if t == 1
        figure(100)
        for ii = 1:length(subjectsAll)
            subplot(2,7,ii)
            generateHeatMapSimple( ...
                cell2mat(smallPRLX{2,ii}),...
                cell2mat(smallPRLY{2,ii}), ...
                'Bins', 15,...
                'StimulusSize', stimSizeMost,...
                'AxisValue', 30,...
                'Uncrowded', 0,...
                'Borders', 1);
            title('Small PRL');
        end
        figure(101)
        for ii = 1:length(subjectsAll)
            subplot(2,7,ii)
            generateHeatMapSimple( ...
                cell2mat(largePRLX{2,ii}),...
                cell2mat(largePRLY{2,ii}), ...
                'Bins', 15,...
                'StimulusSize', stimSizeMost,...
                'AxisValue', 30,...
                'Uncrowded', 0,...
                'Borders', 1);
            title('Large PRL');
        end
%         
%         figure(102)
        for ii = 1:length(subjectsAll)
            largeTempX = []; largeTempY = [];
            smallTempX = []; smallTempY = [];
            for i = 1:length(largePRLX{2,ii})
                largeTempX(i,:) = [largePRLX{2,ii}{i}(1:round(485/(1000/actualSampRate(ii))))]';
                largeTempY(i,:) = [largePRLY{2,ii}{i}(1:round(485/(1000/actualSampRate(ii))))]';
                
            end
            for i = 1:length(smallPRLX{2,ii})
                smallTempX(i,:) = [smallPRLX{2,ii}{i}(1:round(485/(1000/actualSampRate(ii))))]';
                smallTempY(i,:) = [smallPRLY{2,ii}{i}(1:round(485/(1000/actualSampRate(ii))))]';
            end
            videoPlotting(smallTempX,smallTempY,10,'Small PRL');
            videoPlotting(largeTempX,largeTempY,10,'Large PRL');
            figure;
            plot(largeTempX(1,:),largeTempY(1,:),'-');
            hold on
            plot(largeTempX(2,:),largeTempY(2,:),'-');
            hold on
            plot(largeTempX(3,:),largeTempY(3,:),'-');
        end
%     end

    
    set(gca,'FontSize',10)
end
suptitle('Stim Size At Threshold');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/GazeOffsetAndD.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/GazeOffsetAndD.epsc');


temp1 = [];
 temp2 = [];
for i = 1:length(prl) %9 out of 13 show a sig correlation
%     figure;
% te
temp1 = [temp1 prl{2,i}];
temp2 = [temp2 [areaPoly{2,i}]];
end
figure;
[~,p,b,r] = LinRegression(...
    temp1,...%x
    temp2,... %y
    0,NaN,1,0);
xlabel('PRL');
ylabel('Area');


%%
%                       5           9     11
smallBin = [40 20 20 30 30 20 40 20 30 20 25 40 20];
largeBin = [60 80 80 70 70 80 60 80 60 80 75 80 80];
for ii = 1:length(subjectsAll)
    idxTrials = [];
    idxTrials = find(dataAll.SubjectID == ii & dataAll.Condition == -1);
    allDC = dataAll.DC(idxTrials);
    allPerf = dataAll.Perf(idxTrials);
    avDC(ii) = mean(allDC);
    topDC(ii) = prctile(allDC,smallBin(ii)); %small
    bottomDC(ii) = prctile(allDC,largeBin(ii)); %large
    perfLarge(ii) = mean(allPerf(find(allDC > bottomDC(ii))));
    perfSmall(ii) = mean(allPerf(find(allDC < topDC(ii))));
    numLarge(ii) = length(allPerf(find(allDC > bottomDC(ii))));
    numfSmall(ii) = length(allPerf(find(allDC < topDC(ii))));
end
[h,p] =ttest(perfSmall([1:10,12]),perfLarge([1:10,12]));

figure;
subplot(1,2,1)
for ii = [1:10,12]
    plot([1 2],[perfSmall(ii),perfLarge(ii)],'-.','Color',[.5 .5 .5]);
    hold on
end
errorbar([1 2],[mean(perfSmall),mean(perfLarge)],...
    [sem(perfSmall),sem(perfLarge)],'-o','Color','k',...
    'MarkerFaceColor','k');
xlim([0.5 2.5])
xticks([1 2])
xticklabels({'Small','Large'});
title('Uncrowded');
ylim([.25 1]);
xlabel('D (arcmin^2/sec)');
ylabel('Performance');

smallBin = [40 20 20 30 30 20 40 20 30 20 30 40 20];
largeBin = [60 80 80 70 70 80 60 80 60 80 70 80 80];
for ii = 1:length(subjectsAll)
    idxTrials = [];
    idxTrials = find(dataAll.SubjectID == ii & dataAll.Condition == 1);
    allDC = dataAll.DC(idxTrials);
    allPerf = dataAll.Perf(idxTrials);
    avDC(ii) = mean(allDC);
    topDC(ii) = prctile(allDC,smallBin(ii)); %small
    bottomDC(ii) = prctile(allDC,largeBin(ii)); %large
    perfLarge(ii) = mean(allPerf(find(allDC > bottomDC(ii))));
    perfSmall(ii) = mean(allPerf(find(allDC < topDC(ii))));
    numLarge(ii) = length(allPerf(find(allDC > bottomDC(ii))));
    numfSmall(ii) = length(allPerf(find(allDC < topDC(ii))));
end
[h,p] =ttest(perfSmall([1:10,12]),perfLarge([1:10,12]));

subplot(1,2,2)
for ii = [1:10,12]
    plot([1 2],[perfSmall(ii),perfLarge(ii)],'-.','Color',[.5 .5 .5]);
    hold on
end
errorbar([1 2],[mean(perfSmall),mean(perfLarge)],...
    [sem(perfSmall),sem(perfLarge)],'-o','Color','k',...
    'MarkerFaceColor','k');
xlim([0.5 2.5])
xticks([1 2])
xticklabels({'Small','Large'});
title('Crowded');
ylim([.25 1]);
xlabel('D (arcmin^2/sec)');
ylabel('Performance');


%% figure difference in offset for conditions
figure;
y = [mean(findMeanSmall(1,:)) mean(findMeanSmall(2,:)); ...
    mean(findMeanLarge(1,:)) mean(findMeanLarge(2,:))];
b = bar(y,'FaceColor','flat');
hold on
errorbar([0.85 1.15],[mean(findMeanSmall(1,:)) mean(findMeanSmall(2,:))],...
    [sem(findMeanSmall(1,:)), sem(findMeanSmall(2,:))],'o','Color','k');
hold on
errorbar([1.85 2.15],[mean(findMeanLarge(1,:)) mean(findMeanLarge(2,:))],...
    [sem(findMeanLarge(1,:)), sem(findMeanLarge(2,:))],'o','Color','k');
ylabel('Average Gaze Offset');
xticks([1 2])
xticklabels({'Small Span','Large Span'});
legend({'Uncrowded','Crowded'},'Location','northwest');
ylabel('Mean Span');
ylim([3 8])
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/OffsetAverages.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/OffsetAverages.epsc');


%% HeatMaps
[SubjectOrderDC,SubjectOrderDCIdx] = sort([tbl.Uncrowded.thresh]);

for c = 2%1:length(conditions)
    figure('Position',[10 10 2000 600]);
    for i = 11%:length(subjectsAll)
        ii = (SubjectOrderDCIdx(i));
%           ii = (SubjectOrderDCIdx(i));

        stimSizeDesired = find(saveStimInfo{c,ii}(3,:) ==...
            max(saveStimInfo{c,ii}(3,:)));
        stimSizeMost = saveStimInfo{c,ii}(1,stimSizeDesired);
        currentPath = subjectCond.(conditions{c}).Unstabilized{ii}.threshInfo.em;
%         subplot(2,7,i)
% figure;
        x = [];
        y = [];
        stVector = 0;%[-2:1:2]; %width of square stim
        for sty = 1:length(stVector)
            for stx = 1:length(stVector)
                xT = currentPath.allTraces.xALL(ismember(round(currentPath.allTraces.xALL),...
                    [round(currentPath.allTraces.xALL+(tbl.(conditions{c})(ii).thresh*1.4)) ...
                    round(currentPath.allTraces.xALL-(tbl.(conditions{c})(ii).thresh*1.4))]));
                yT = currentPath.allTraces.yALL(ismember(round(currentPath.allTraces.xALL),...
                    [round(currentPath.allTraces.xALL+(tbl.(conditions{c})(ii).thresh*1.4)) ...
                    round(currentPath.allTraces.xALL-(tbl.(conditions{c})(ii).thresh*1.4))]));
                
                x = [x (xT + stVector(stx))];
                y = [y (yT + stVector(sty))];
            end
        end
        figure;
        temp = generateHeatMapSimple( ...
            x,...
            y, ...
            'Bins', 40,...
            'StimulusSize', stimSizeMost,...
            'AxisValue', 10,...
            'Uncrowded', 0,...
            'Borders', 1);
        hold on
        text(-15,-17,sprintf('BCEA = %.2f',[tbl.(conditions{c})(ii).bcea]/60));
        text(-15,-14,sprintf('Th = %.2f',tbl.(conditions{c})(ii).thresh));
        title(subjectsAll{i})
        axis square
    end
%     saveas(gcf,'CreatedImages/AllTrialsProbConeStimSquare5arcmin.png');
%     suptitle(conditions{c})
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/HeatMaps%s.png', conditions{c}));
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/HeatMaps%s.epsc',  conditions{c}));
end

figure;
for i = 1:length(subjectsAll)
    hold on
    plot([0 1], [[tbl.Uncrowded(i).bcea]/60 [tbl.Crowded(i).bcea]/60],'-o'...
        ); 
    hold on
end

errorbar([0.1 1.1],[mean([tbl.Uncrowded(:).bcea]/60) mean([tbl.Crowded(:).bcea]/60)],...
    [sem([tbl.Uncrowded(:).bcea]/60) sem([tbl.Crowded(:).bcea]/60)],'-o',...
    'Color','k','MarkerFaceColor','k');
ylabel('BCEA (deg^2)');
xticks([0 1]);
xticklabels({'Uncrowded','Crowded'});
xlim([-.5 1.5])
set(gca, 'YScale', 'log')
% axis square
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/BCEA.png', conditions{c}));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/BCEA.epsc',  conditions{c}));

[h,p] = ttest([tbl.Uncrowded(:).bcea]/60, [tbl.Crowded(:).bcea]/60);


%% For Crowding Text - Square Stim Estimate
%"When considering a typical pattern of ocular drift and an 
% average cone size at the preferred locus of fixation 
%(Curcio et al., 1990), we estimate that on average XX% of 
% cones stimulated by the target are also stimulated by the 
%flankers, assuming a 5'x5' stimulus with flanker spacing of XX, 
%potentially increasing perceptual uncertainty in target identification."

for ii = 1:1000
    [Xe, Ye] = EM_Brownian2(20, 1000, 500, 1);
    
    probSameCone1(ii) = coneAndFlankerProbability_TumbE...
        (Xe,5,1);
     probSameCone2(ii) = coneAndFlankerProbability_TumbE...
        (Xe,5,2);
     probSameCone3(ii) = coneAndFlankerProbability_TumbE...
        (Xe,5,3);
end

mean([probSameCone1])
mean([probSameCone2])
mean([probSameCone3])

mean([probSameCone1 probSameCone2 probSameCone3])
std([mean(probSameCone1) mean(probSameCone2) mean(probSameCone3)])

%% FIGURE DC and Acuity - not Crowding Effect
figure('Position',[2000 100 1000 800]);
subplot(2,2,1)
x = [tbl.Uncrowded(:).dc];
y = [tbl.Uncrowded(:).thresh];
[~,p,b,r] = LinRegression(...
    x,...%x
    y,... %y
    0,NaN,1,0);
axis([0 60 0 4])
axis square
text(30,1,sprintf('p = %.2f \nr = %.2f', p, r))
xlabel({'Uncrowded D', '(arcmin^2/sec)'});
ylabel({'Uncrowded', 'Threshold (arcmin)'})
title('')

subplot(2,2,2)
x = [tbl.Crowded(:).dc];
y = [tbl.Crowded(:).thresh];
[~,p,b,r] = LinRegression(...
    x,...%x
    y,... %y
    0,NaN,1,0);
axis([0 60 0 4])
axis square
title('')
text(30,1,sprintf('p = %.2f \nr = %.2f', p, r))
xlabel({'Crowded D', '(arcmin^2/sec)'});
ylabel({'Crowded', 'Threshold (arcmin)'})

subplot(2,2,3)
for ii = 1:length(subjectsAll)
    leg(ii) = plot([0 1],...
        [tbl.Uncrowded(ii).dc tbl.Crowded(ii).dc],'-o',...
        'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
        'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:));
    hold on
end
[h,pttest] = ttest([tbl.Uncrowded(:).dc], [tbl.Crowded(:).dc]);
xticks([0 1])
xticklabels({'Uncrowded','Crowded'});
xlim([-.5 1.5]);
text(0,50,sprintf('p = %.2f',pttest));
ylabel('D (arcmin^2/sec')
% legend(leg,subjectsAll)
axis square

subplot(2,2,4)
[~,p,b,r] = LinRegression(...
    [tbl.Crowded(:).dc] - [tbl.Uncrowded(:).dc],...%x
    [tbl.Crowded(:).thresh] - [tbl.Uncrowded(:).thresh],... %y
    0,NaN,1,0);
hold on
axis([-15 40 0 2])
title('');
ylabel('Crowding Effect (arcmin)')
xlabel('\Delta D (arcmin^2/sec)')
axis square
text(-10,1.6,sprintf('p = %.2f \nr = %.2f', p, r))

% legend(leg,subjectsAll)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaEffectDc.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaEffectPosition.epsc');

%% FIGURE crowding effects and PRL

figure('Position',[2000 100 1000 800]);

subplot(1,2,1)
for ii = 1:length(subjectsAll)
    plot([0 1],[tbl.Uncrowded(ii).thresh...
        tbl.Crowded(ii).thresh*1.4],'-o',...
        'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:),'Color',...
        colorMapNew(find(SubjectOrderDCIdx == ii),:),...
        'MarkerSize',10,'LineWidth',2);
    hold on
end
xlim([-.5 1.5])
set(gca,'xtick',[0 1],'xticklabel',conditions, 'FontSize',14) %,'YScale', 'log'
ylabel('Threshold');
axis square

h1 = figure;
% subplot(1,2,2)

[~,p,b,r] = LinRegression(...
    [tbl.Crowded(:).prl]-[tbl.Uncrowded(:).prl],...%x
    [tbl.Crowded(:).thresh]-[tbl.Uncrowded(:).thresh],... %y
    0,NaN,1,0);
axis([-2.5 2.5 0 1.65])
axis square
xlabel('\Delta Gaze Position')
ylabel('Crowding Effect')
hold on
% colorMapNew2 = cool(length(subjectsAll));
colorMapNew2 = colorMapNew;
[vals,idx] = sort(([tbl.Crowded(:).prl]))
for ii = 1:length(subjectsAll)
    leg(ii) = plot([tbl.Crowded(ii).prl]-[tbl.Uncrowded(ii).prl],...
        [tbl.Crowded(ii).thresh]-[tbl.Uncrowded(ii).thresh],'o',...
        'Color',colorMapNew2(find(idx == ii),:),...
        'MarkerFaceColor',colorMapNew2(find(idx == ii),:),...
        'MarkerSize',10)
end
% legend(leg,subjectsAll,'Location','northwest')
colormap(colorMapNew)
c = colorbar('Ticks',[0 0.5 1],...
         'TickLabels',{vals(1),mean([vals(1) vals(13)]),vals(13)})
c.Label.String = 'Mean Crowded Offset(arcmin)';
% xlabel('Diffusion Constant (arcmin^{2}/second)')
% ylabel('Visual Acuity Threshold')
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaEffectPosition.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaEffectPosition.epsc');
%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% get the same stim size to look at for all subjects

figure('Position',[2000 200 1000 700]); %%currently removing last subject because they're an outlier and have a diff size stim
    subplot(2,2,1)
        temp = [tbl.Crowded(:).prl]-[tbl.Uncrowded(:).prl];
        [~,p,b,r] = LinRegression(...
           temp(1:end-1),...
                actualPerf(2,1:end-1) - actualPerf(1,1:end-1),... %y
            0,NaN,1,0);
        xlabel('\Delta Gaze Position All Trials')
        ylabel('\Delta Performance at Fixed Stim Size');
        axis square
        ylim([-2 2])
    subplot(2,2,2)
        [~,p,b,r] = LinRegression(...
            actualPRLatSize(2,1:end-1) - actualPRLatSize(1,1:end-1),...
                actualPerf(2,1:end-1) - actualPerf(1,1:end-1),... %y
            0,NaN,1,0);
        xlabel('\Delta Gaze Position Fixed Stim Size')
        ylabel('\Delta Performance at Fixed Stim Size');
        ylim([-2 2])
        axis square
    subplot(2,2,3)
        [~,p,b,~] = LinRegression(...
           actualPRLatSize(1,1:end-1),...
                actualPerf(1,1:end-1),... %y
            0,NaN,1,0);
        xlabel('Gaze Position Uncr Fixed Stim')
        ylabel('Unc Performance at Fixed Stim Size');
        ylim([0 1])
        axis square
    subplot(2,2,4); figure;
        [~,p,b,r] = LinRegression(...
           actualPRLatSize(2,1:end)./[tbl.Crowded.actualStimSize],...
                [tbl.Crowded.thresh]-[tbl.Uncrowded.thresh],... %y
            0,NaN,1,0);
        xlabel('Gaze Position Cro Fixed Stim')
        ylabel('Cro Performance at Fixed Stim Size');
        ylim([0 1])
        axis square
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaGazePerformance.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaGazePerformance.epsc');
 %%Plot Performance drop for crowded condition same size
figure;
subplot(1,2,1)
    for ii = 1:length(subjectsAll)
        swIdx = determineSameSWCandU(ii);
        swNameU = find(saveStimInfo{1,ii}(1,:) == swIdx);
        uPerformance(ii) = subjectCond.Uncrowded.Unstabilized...
            {ii}.threshInfo.em.ecc_0.(sprintf('strokeWidth_%i',...
            saveStimInfo{1,ii}(2,swNameU))).performanceAtSize;
        swNameC = find(saveStimInfo{2,ii}(1,:) == swIdx);
        cPerformance(ii) = subjectCond.Crowded.Unstabilized...
            {ii}.threshInfo.em.ecc_0.(sprintf('strokeWidth_%i',...
            saveStimInfo{1,ii}(2,swNameC))).performanceAtSize;
        plot([0 1],[uPerformance(ii) cPerformance(ii)],'-o',...
            'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
            'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
            'MarkerSize',10);
        hold on
    end
    errorbar([0.2 1.2],[mean(uPerformance) mean(cPerformance)],...
        [std(uPerformance) std(cPerformance)],...
        '-o','MarkerFaceColor','k','Color','k','MarkerSize',15,'LineWidth',3);
    xlim([-.5 1.5])
    xticks([0 1])
    xticklabels({'Uncrowded','Crowded'});
    ylabel('Performance')
    [~,pP] = ttest(uPerformance, cPerformance);
%     title(sprintf('Fixed Size (STD), p<%.3f',pP));
    set(gca,'FontSize',12)

subplot(1,2,2);
    for ii = 1:length(subjectsAll)
        plot([0 1],[tbl.Uncrowded(ii).thresh tbl.Crowded(ii).thresh],'-o',...
            'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
            'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
            'MarkerSize',10);
        hold on
    end
     errorbar([0.2 1.2],[mean([tbl.Uncrowded(:).thresh]) mean([tbl.Crowded(:).thresh])],...
        [std([tbl.Uncrowded(:).thresh]) std([tbl.Crowded(:).thresh])],...
        '-o','MarkerFaceColor','k','Color','k','MarkerSize',15,'LineWidth',3);
    xlim([-.5 1.5])
    ylim([1 3.5])

    xticks([0 1])
    xticklabels({'Uncrowded','Crowded'});
    ylabel('Threshold Strokewidth (arcmin)')
    [~,pT] = ttest([tbl.Uncrowded(:).thresh], [tbl.Crowded(:).thresh]);
%     title(sprintf('Threshold (STD), p<%.3f',pT));
    set(gca,'FontSize',12)

    hold on
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaPerformanceFixedSize.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/DeltaPerformanceFixedSize.epsc');


%% Seperate by Individual Trial

allXC = []; allYC = []; allXU = []; allYU = [];
for ii = 1:length(subjectsAll)
    allXU = [allXU actualPRLatSizeInd{1, ii}];
    allYU = [allYU actualCorrect{1, ii}];
    allXC = [allXC actualPRLatSizeInd{2, ii}];
    allYC = [allYC actualCorrect{2, ii}];
end
figure;
idxPrlLarge = find(allXU > 10);

[~,p,b,r] = LinRegression(...
        allXU(idxPrlLarge),...%x
        allYU(idxPrlLarge),... %y
        0,NaN,1,0);
hold on
[~,p,b,r] = LinRegression(...
        allXC,...%x
        allYC,... %y
        0,NaN,1,0);

%% All 0 and 1 MAIN FIG Combine all seperated Trials
catTimeOnFlanker = [];catTimeOnFlankerP = [];
figure;
for ii = 1:length(subjectsAll)
    for c = 2%1:length(conditions)
        threshTimeTarg = .45;    
        threshTimeFlank = .55;
%         threshTime = .60;  
        performanceOnTarget(ii) = mean(...
            performance{c,ii}(find(timeSpentOnTarget{c,ii} >= threshTimeTarg)));
        
        perfNotOnFlank(ii) = mean(...
            performance{c,ii}(find(timeOnFlankers{c,ii} < threshTimeFlank)));
        
        performanceOnFlanker(ii) = mean(...
            performance{c,ii}(find(timeOnFlankers{c,ii} >= threshTimeFlank)));
        plot( performance{c,ii}, timeOnFlankers{c,ii},'o');
        
        if c == 2
%             diffTimeOnTarget(ii) = mean(...
%             timeSpentOnTarget{2,ii}(find(timeSpentOnTarget{2,ii})))- mean(...
%             timeSpentOnTarget{1,ii}(find(timeSpentOnTarget{1,ii})));
%             diffTimeOnFlankers(ii) = mean(...
%             timeOnFlankers{2,ii}(find(timeOnFlankers{2,ii})))- mean(...
%             timeOnFlankers{1,ii}(find(timeOnFlankers{1,ii})));
         diffTimeOnTarget(ii) = mean(...
            timeSpentOnTarget{2,ii})- mean(...
            timeSpentOnTarget{1,ii});
         diffTimeOnFlankers(ii) = mean(...
            timeOnFlankers{2,ii})- mean(...
            timeOnFlankers{1,ii});
        if mean(weightSpentOnTarget{2,ii}(1,:)) <= 0
            weightedOnTarget(ii) = -(abs(mean(weightSpentOnTarget{2,ii}(1,:)))/...
                mean(weightSpentOnTarget{2,ii}(2,:)))*100;
        else
            weightedOnTarget(ii) = (abs(mean(weightSpentOnTarget{2,ii}(1,:)))/...
                mean(weightSpentOnTarget{2,ii}(2,:)))*100;
        end
       
        
        end
        hold on
        catTimeOnFlanker = [catTimeOnFlanker timeSpentOnTarget{c,ii}]
        catTimeOnFlankerP= [catTimeOnFlankerP performance{c,ii}]

    end
end
plot([0 1],...
    [mean(catTimeOnFlanker(catTimeOnFlankerP == 0)) mean(catTimeOnFlanker(catTimeOnFlankerP == 1))],...
    'o','LineWidth',10','Color','k')


%% Gaze Dist
figure;
subplot(1,2,1)
plot(smallPerf(2,:), largePerf(2,:),'o');
line([0 1],[0 1])
axis([.3 1 .3 1])
subplot(1,2,2)
plot(smallPerf(1,:), largePerf(1,:),'o');
line([0 1],[0 1])
axis([.3 1 .3 1])

figure('Position',[2000 100 1000 800]);
    subplot(2,2,1)
        for ii = 1:length(subjectsAll)
            plot(smallPerf(1,ii), largePerf(1,ii),'o',...
                'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
                'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:));
            hold on
        end
        xlabel({'Small Gaze Distance', 'Performance'});
        ylabel({'Large Gaze Distance', 'Performance'});
        hold on
        line([0 1],[0 1])
        axis([.3 1 .3 1])
        errorbar(mean(smallPerf(1,:)),mean(largePerf(1,:)),...
            std(smallPerf(1,:))/2,std(smallPerf(1,:))/2,std(largePerf(1,:))/2,std(largePerf(1,:))/2,...
            'o','Color','k','MarkerFaceColor','k','MarkerSize',8);
        axis square
%         title('Uncrowded');
        text(.35,.9,{'\it better performance','\it with large distances'})
        text(.65,.4,{'\it better performance','\it with small distances'})
        xticks([.4 .6 .8 1])
        yticks([.4 .6 .8 1])
        text(.70,.95,{'\bf Uncrowded'})


    subplot(2,2,2)
        for ii = 1:length(subjectsAll)
            plot(smallPerf(2,ii), largePerf(2,ii),'o',...
                'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
                'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:));
            hold on
        end
        xlabel({'Small Gaze Distance', 'Performance'});
        ylabel({'Large Gaze Distance', 'Performance'});
        hold on
        line([0 1],[0 1])
        axis([.3 1 .3 1])
        errorbar(mean(smallPerf(2,:)),mean(largePerf(2,:)),...
            std(smallPerf(2,:))/2,std(smallPerf(2,:))/2,std(largePerf(2,:))/2,std(largePerf(2,:))/2,'o',...
                'Color','k','MarkerFaceColor','k','MarkerSize',8);
        axis square
%         title('Crowded');
        text(.35,.9,{'\it better performance','\it with large distances'})
        text(.65,.4,{'\it better performance','\it with small distances'})
        xticks([.4 .6 .8 1])
        yticks([.4 .6 .8 1])
        text(.70,.95,{'\bf Crowded'})

% figure;
     subplot(2,2,3)
        templeg(1) = errorbar([0 1],[mean(smallPerf(1,:)),mean(largePerf(1,:))],...
            [sem(smallPerf(1,:)),sem(largePerf(1,:))],'-o',...
            'LineWidth',2);
        axis square
        axis([-.5 1.5 .25 1])
        xticks([0 1])
        xticklabels({'Small Gaze','Large Gaze'})
        ylabel('Performance');
        [h,pC] = ttest(smallPerf(2,:), largePerf(2,:));
        [h,pU] = ttest(smallPerf(1,:), largePerf(1,:));

        %         title('Uncrowded(SEM)')
        hold on
        templeg(2) = errorbar([0 1],[mean(smallPerf(2,:)),mean(largePerf(2,:))],...
            [sem(smallPerf(2,:)),sem(largePerf(2,:))],'-o',...
            'LineWidth',2);
        legend(templeg,{sprintf('Uncrowded, p = %.2f', pU),...
            sprintf('Crowded, p = %.2f', pC)});
        ylim([.55 .8])
        yticks([.6 .7 .8])
        
      subplot(2,2,4)
%         figure;
%         temp1 = [tbl.Crowded(:).prl]-[tbl.Uncrowded(:).prl];
        temp1 = [tbl.Crowded(:).prl]./[tbl.Crowded(:).thresh];
        crowdingEffect = [tbl.Crowded(:).thresh]-[tbl.Uncrowded(:).thresh];
        [~,p,b,r] = LinRegression(...
            [tbl.Crowded.prl]-[tbl.Uncrowded.prl],...%x([1 2 4:13])
            crowdingEffect,... %y
            0,NaN,1,0);
        axis([-2.5 2.5 0 1.65])
        axis square
        xlabel('\Delta Gaze Distance')
        ylabel('Crowding Effect')
        hold on
        for ii = 1:length(subjectsAll)
            leg(ii) = plot([tbl.Crowded(ii).prl]-[tbl.Uncrowded(ii).prl],...
                [tbl.Crowded(ii).thresh]-[tbl.Uncrowded(ii).thresh],'o',...
                'Color',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
                'MarkerFaceColor',colorMapNew(find(SubjectOrderDCIdx == ii),:),...
                'MarkerSize',10)
        end
        yticks([0 .4 .8 1.2 1.6])
        title('')
        text(-2,0.8,{sprintf('p = %.2f',p),sprintf('r = %.2f',r)});
 saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/PLFLargevsSmall.png');
 saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/PLFLargevsSmall.epsc');

 %% Diff in Time on Target
 figure;
 subplot(2,2,1)
 [~,p,b,r] = LinRegression(...
     (weightedOnTarget),...%x([1 2 4:13])
     crowdingEffect,... %y
     0,NaN,1,0);
xlabel('Weighted Target');
ylabel('Crowding Effect');
ylim([0 1.5])
%  figure;
subplot(2,2,2)
 for ii = 1:length(subjectsAll)
      threshTimeTarg = .45;  
      idx1 = find(timeSpentOnTarget{1,ii} >= threshTimeTarg);
      idx2 = find(timeSpentOnTarget{2,ii} >= threshTimeTarg);
      
     plot([mean(timeSpentOnTarget{2,ii}(idx2))],...
         [crowdingEffect(ii)],'d');
     hold on
     plot([mean(timeSpentOnTarget{1,ii}(idx1))],...
         [crowdingEffect(ii)],'s');
     if [mean(timeSpentOnTarget{1,ii}(idx1))] > [mean(timeSpentOnTarget{2,ii}(idx2))]
         line([mean(timeSpentOnTarget{2,ii}(idx2)) mean(timeSpentOnTarget{1,ii}(idx1))],...
             [crowdingEffect(ii) crowdingEffect(ii)],'Color','b')
     else %%% more time on target in the crowded condition
         line([mean(timeSpentOnTarget{2,ii}(idx2)) mean(timeSpentOnTarget{1,ii}(idx1))],...
             [crowdingEffect(ii) crowdingEffect(ii)],'Color','r')
     end
     hold on
 end
 xlabel('Time Spent on Target')
 ylabel('Crowding Effect')
 title('Red = more time on target in Crowded');
 
%  figure;
subplot(1,2,1)
 [~,p,b,r] = LinRegression(...
            diffTimeOnTarget,...%x([1 2 4:13])
            [tbl.Crowded(:).thresh],... %y
            0,NaN,1,0);
 xlabel('\Delta Time on Target X')
 ylabel('Crowding Threshold')
  ylim([0 5])
subplot(1,2,2)
[~,p,b,r] = LinRegression(...
            diffTimeOnFlankers,...%x([1 2 4:13])
            [tbl.Crowded(:).thresh],... %y
            0,NaN,1,0);
xlabel('\Delta Time on Flankers in X')
 ylabel('Crowding Threshold')
 ylim([0 5])
%% Same figures with different colors
fig = figure;
% right_color = [255 51 239]./256;
% left_color = [72 179 175]./256;
% set(fig,'defaultAxesColorOrder',[left_color; right_color]);
subplot(1,3,2)
     legt(1) = errorbar([0 1],[mean(smallPerf(1,:)),mean(largePerf(1,:))],...
            [sem(smallPerf(1,:)),sem(largePerf(1,:))],'-d',...
            'MarkerSize',15,'MarkerFaceColor',[0 51 153]./256,...
            'Color',[0 51 153]./256,'LineWidth',3);
      
    hold on
%     yyaxis left
        legt(2) = errorbar([0 1],[mean(smallPerf(2,:)),mean(largePerf(2,:))],...
            [sem(smallPerf(2,:)),sem(largePerf(2,:))],'-d',...
            'MarkerSize',15,'MarkerFaceColor',[255 102 0]./256,...
            'Color',[255 102 0]./256,'LineWidth',3);
%         hold on
       
%         yyaxis left
%         axis square
        axis([-.5 1.5 .5 .8])
        xticks([0 1])
         xlabel('Area (arcmin^2)')
        xticklabels({'Small','Large'})
        ylabel('Proportion Correct');
        [h,p] = ttest(smallPerf, largePerf);
        yticks([.5 .6 .7 .8])
        legend(legt,{'Uncrowded, n.s.',sprintf('Crowded, p = %.3f', pC)},'Location','south');
%         title('Crowded (SEM)')
%  set(gca,'FontSize',14)
% saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/ttestPLFLargevsSmall.png');
%  saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/ttestPLFLargevsSmall.epsc');
yyaxis right
hold on
% figure;
legt(2) = errorbar([0 1],[mean(meanpercentOverlapS(2,:)),mean(meanpercentOverlapL(2,:))],...
            [sem(meanpercentOverlapS(2,:)),sem(meanpercentOverlapS(2,:))],'-d',...
            'MarkerSize',15,'MarkerFaceColor',[127 127 127]./256,...
            'Color',[127 127 127]./256,'LineWidth',3);
ylim([0.15 .9])
        legend off
        set(gca, 'YDir','reverse')
[h,p]=ttest((meanpercentOverlapS(2,:)),(meanpercentOverlapL(2,:)))
%  figure;
subplot(1,3,1)
%  legp(1) = errorbar([0 1],[mean(meanpercentOnTS(1,:)),mean(meanpercentOnTL(1,:))],...
%      [sem(meanpercentOnTS(1,:)),sem(meanpercentOnTL(1,:))],'-s',...
%      'MarkerSize',15,'MarkerFaceColor',[72 179 175]./256,...
%      'Color',[72 179 175]./256,'LineWidth',3);
%  %  yyaxis right
% hold on
% text(-.10,[mean(meanpercentOnTS(1,:))],'S');
% text(.9,[mean(meanpercentOnTL(1,:))],'L');
% 
%  legp(2) = errorbar([2 3],[mean(meanpercentOnTS(2,:)),mean(meanpercentOnTL(2,:))],...
%      [sem(meanpercentOnTS(2,:)),sem(meanpercentOnTL(2,:))],'-s',...
%      'MarkerSize',15,'MarkerFaceColor',[255 51 239]./256,...
%      'Color',[255 51 239]./256,'LineWidth',3);
%  text([1.9],[mean(meanpercentOnTS(2,:))],'S');
%  text([2.9],[mean(meanpercentOnTL(2,:))],'L');
%  axis([-.5 3.5 .5 .8])
%  yticks([.5 .6 .7 .8])
 
%  figure;
legp(1) = errorbar([1, 0],[mean(meanpercentOnTS(1,:)) mean(meanpercentOnTL(1,:))],...
     [sem(meanpercentOnTS(1,:)) sem(meanpercentOnTL(1,:))],'s',...
     'MarkerSize',15,'MarkerFaceColor',[0 51 153]./256,...
     'Color',[0 51 153]./256,'LineWidth',3);
%  legp(1) = errorbar(0,mean(meanpercentOnTS(1,:)) - mean(meanpercentOnTL(1,:)),...
%      [sem(meanpercentOnTS(1,:))-sem(meanpercentOnTL(1,:))],'-s',...
%      'MarkerSize',15,'MarkerFaceColor',[72 179 175]./256,...
%      'Color',[72 179 175]./256,'LineWidth',3);[255 102 0]
 hold on
 legp(2) = errorbar([1.2 0.2],...
     [mean(meanpercentOnTS(2,:)) mean(meanpercentOnTL(2,:))],...
     [sem(meanpercentOnTS(2,:)) sem(meanpercentOnTL(2,:))],'s',...
     'MarkerSize',15,'MarkerFaceColor',[255 102 0]./256,...
     'Color',[255 102 0]./256,'LineWidth',3);
%  axis([-.5 1.5 .05 .15])
xlim([-.5 1.5])
 ylabel({'Percent of Time ','On Target'});
%  xticks([0.5 2.5])
xticks([0 1.2])
 xticklabels({'Small','Large'})
 xlabel('Area (arcmin^2)')
%  [h,pU] = ttest(meanpercentOnTS(1,:),meanpercentOnTL(1,:));
%  [h,pC2] = ttest(meanpercentOnTS(2,:),meanpercentOnTL(2,:));
%  legend(legp,{sprintf('Uncrowded, p = %.3f', pU),sprintf('Crowded, p = %.3f', pC2)},'Location','south');
 [h, pDiff] = ttest(meanpercentOnTS(1,:)- meanpercentOnTL(1,:),...
     meanpercentOnTS(2,:) -meanpercentOnTL(2,:));
% set(gca,'FontSize',14)
% saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/ttestOnTarg.png');
%  saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/ttestOnTarg.epsc');
 
figure;
% yyaxis left
subplot(2,1,1)
legnd(1) = errorbar([0 1],[mean([tbl.Uncrowded(:).thresh]) mean([tbl.Crowded(:).thresh])],...
        [sem([tbl.Uncrowded(:).thresh]) sem([tbl.Crowded(:).thresh])],...
        '-o','MarkerFaceColor','k','Color','k','MarkerSize',15,'LineWidth',3);
    ylabel('Acuity Threshold (arcmin)')
    ylim([1.2 2.8])
    yticks([1.2 1.6 2.0 2.4 2.8])
    axis square
    xticks([0 1])
    xticklabels({'Isolated','Crowded'});
    xlim([-.5 1.5])
% hold on
% yyaxis right
subplot(2,1,2)
  legt(1) = errorbar([0 1],[mean(smallPerf(1,:)),mean(largePerf(1,:))],...
            [sem(smallPerf(1,:)),sem(largePerf(1,:))],'-o',...
            'MarkerSize',12,'MarkerFaceColor',[84 93 232]./256,...
            'Color',[84 93 232]./256,'LineWidth',3);

    hold on
        legt(2) = errorbar([0 1],[mean(smallPerf(2,:)),mean(largePerf(2,:))],...
            [sem(smallPerf(2,:)),sem(largePerf(2,:))],'-o',...
            'MarkerSize',12,'MarkerFaceColor',[232 84 116]./256,...
            'Color',[232 84 116]./256,'LineWidth',3)
        axis square
        axis([-.5 1.5 .5 .8])
        xticks([0 1])
        xticklabels({'Small Gaze Offset','Large Gaze Offset'})
        ylabel('Performance');
        [h,p] = ttest(smallPerf, largePerf);
        yticks([.5 .6 .7 .8])
        legend(legt,{'Isolated','Crowded'});
%  saveas(gcf,'C:\Users\Ruccilab\Box\APLab-Projects\Grants\ClarkF31_Resubmission/ttestPLFLargevsSmall.png');
 saveas(gcf,'C:/Users/Ruccilab/Box/APLab-Projects/Grants/ClarkF31_Resubmission/ttestPLFLargevsSmall.epsc');

%% Percent of Time on Flanker vs Target

smallPerf = [];
largePerf = [];
divSTDSmall(1,:) = [.5 .05 .5 .5 .5 .5 .5 .5 .5 .5 .5 .5 1];
divSTDSmall(2,:) = [1 1.6 1 1.2 .75 .5 .01 1.2 0.5 .5 .05 .25 .25];

divSTDL(1,:) = [.5 .5 .5 .5 .5 .5 .5 .5 .5 .5 .5 .5 .01];
divSTDL(2,:) = [1.3 .8 2 1 .5 .95  1.25 2.5 2 .02 .2 .5 0.25];    
% divSTDL(2,:) = [1.3 .8 1.4 1 .5 .95  1.25 1.2...%2.5 - 8,2.0-9,2-3
%     1.5 .02 .2 .5 0.25];  
for ii = 1:length(subjectsAll)
    for c = 1:length(conditions)
        findMeanSmall(c,ii) =  mean(timeSpentOnTarget{c, ii} - ...
            (std(timeSpentOnTarget{c, ii}) * divSTDSmall(c,ii)));
        findMeanLarge(c,ii) =  mean(timeSpentOnTarget{c, ii} + ...
            (std(timeSpentOnTarget{c, ii}) * divSTDL(c,ii)));

        smallPerf(c,ii) = mean(performance{c,ii}(find(timeSpentOnTarget{c,ii} <...
            findMeanSmall(c,ii))));
        lengthSP(c,ii) = length(performance{c,ii}(find(timeSpentOnTarget{c,ii} < ...
            findMeanSmall(c,ii))));
        largePerf(c,ii) = mean(performance{c,ii}(find(timeSpentOnTarget{c,ii} > ...
            findMeanLarge(c,ii))));
        lengthLP(c,ii) = length(performance{c,ii}(find(timeSpentOnTarget{c,ii} > ...
            findMeanLarge(c,ii))));
        
        avTimeSpentBin.small(c,ii) = mean(timeSpentOnTarget{c,ii}(find(timeSpentOnTarget{c,ii} <...
            findMeanSmall(c,ii))));
        avTimeSpentBin.large(c,ii) = mean(timeSpentOnTarget{c,ii}(find(timeSpentOnTarget{c,ii} >...
            findMeanLarge(c,ii))));
        
    end
end
% idx = find()
idx = find((lengthSP(2,:)) > 20 & lengthLP(2,:) >= 15)

tempC = [tbl.Crowded(:).thresh];
figure;
[~,p,b,r] = LinRegression(...
        [avTimeSpentBin.large(2,idx) - avTimeSpentBin.small(2,idx)],...%x
        tempC(idx),... %y
        0,NaN,1,0);

figure;
for ii = idx%1:length(largePerf)
%     plot([0 1], double([(smallPerf(1,ii)) (largePerf(1,ii))]),'-o','Color','k');
    hold on
    plot([0 1], double([(smallPerf(2,ii)) (largePerf(2,ii))]),'-o','Color','r');
    hold on
end


figure;
% idx = 1:13
subplot(1,3,3)
% clear
% idx = 1:13
% boxplot([(smallPerf(1,:)),(largePerf(1,:))],[1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2])
legt(1) = errorbar([1 0],[nanmean(smallPerf(1,:)),nanmean(largePerf(1,idx))],...
    [sem(smallPerf(1,idx)),sem(largePerf(1,idx))],'-o',...
    'MarkerSize',12,'MarkerFaceColor',[0 51 153]./256,...
    'Color',[0 51 153]./256,'LineWidth',3);
hold on
% boxplot([(smallPerf(2,:)),(largePerf(2,:))],2+[1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2])
%[255 102 0]
hold on
legt(2) = errorbar([1 0],[mean(smallPerf(2,idx)),mean(largePerf(2,idx))],...
    [sem(smallPerf(2,idx)),sem(largePerf(2,idx))],'-o',...
    'MarkerSize',12,'MarkerFaceColor',[255 102 0]./256,...
    'Color',[255 102 0]./256,'LineWidth',3)
% axis square
axis([-.5 1.5 .5 .8])
xticks([0 1])
xticklabels({'Min','Max'})
xlabel('Relative Time on Target')
ylabel('Proportion Correct');
[h,p1] = ttest(smallPerf(1,idx), largePerf(1,idx));
[h,p2] = ttest(smallPerf(2,idx), largePerf(2,idx));
yticks([.5 .6 .7 .8])
%  plot([0 1],[mean(meanpercentOnTS(2,:)),mean(meanpercentOnTL(2,:))],'--s',...
%             'MarkerSize',8,'MarkerFaceColor',[255 51 239]./256,...
%             'Color',[255 51 239]./256,'LineWidth',1);
%  plot([0 1],[mean(meanpercentOnTS(1,:)),mean(meanpercentOnTL(1,:))],'--s',...
%             'MarkerSize',8,'MarkerFaceColor',[72 179 175]./256,...
%             'Color',[72 179 175]./256,'LineWidth',1);

legend(legt,{'Uncrowded p = n.s.',sprintf('Crowded p = %.3f', p2)},...
    'Location','south');
 set(gca,'FontSize',14)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/timeOnTargetLargevsSmall.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/timeOnTargetLargevsSmall.epsc');
  
%% Histograms of Area and Performance

var = areaPoly;%probSameConesforTrace; %areaPoly;
numBins = 3;
[SubjectOrderDC,SubjectOrderDCIdx] =  sort([tbl.Uncrowded.dc]);%-....
%     [tbl.Uncrowded.thresh]);

% sort([tbl.Uncrowded.thresh]);

% 
figure;
for t = 1:length(subjectsAll)
    ii = SubjectOrderDCIdx(t)%find(SubjectOrderDCIdx == t);
    Y = [];
    allYs = [];
    newVals = [];
%     figure;
%     histInfo = histcounts(areaPoly{ii,2});
%     Y = discretize(areaPoly{2,ii},5);
%     Q = quantile(areaPoly{2,ii},5)
    newVals = chunkIntoEvenBins(var{2,ii},numBins);
%     figure;
    allYs = 1:numBins;
    counter = 1;
    temp10 = [];temp20=[];temp30=[];temNumTrials=[];
    for y = (allYs)
        if(isempty(newVals{y}))
            continue;
        end
        temp10(counter) = mean(var{2,ii}(newVals{y}));
        temp20(counter) = mean(perfCur{2,ii}(newVals{y})*100);
        temp30(counter) = mean(probSameConesforTrace{2,ii}(newVals{y})*100);
        temNumTrials(counter) = length((newVals{y}));
        counter = counter + 1;
    end
    subplot(3,6,t)
    ax0 = plot(find(temp10 >= 0),temp10)
    addaxis(find(temp20 >= 0),temp20)
    addaxis(find(temp30 >= 0),temp30)
    
%         yyaxis left
%         plot(find(temp10 >= 0),temp10,'-o','Color','b');
%         hold on
%         ylabel('Area');
%         ylim([0 35])
%         yyaxis right
%         plot(find(temp20 >= 0),temp20,'-o','Color','r');
%         hold on
%         ylabel('Performance');
%         ylim([25 100])
%     end
    xlabel('Binned by Area')
    title(sprintf('%s,%i',subjectsAll{ii}, temNumTrials(1)))
end
legend({'Area','Performance','SharedConeAct'})


%% Lin Reg of Cone Prob and Area
numBinsPerf = 2;
perfTemp = [];perfNorm = [];
areaTemp = [];areaNorm = [];
coneTemp = [];coneNorm = [];

for ii = 1:length(subjectsAll)
    idxTemp = [];
    Y = [];
    %     areaTemp = [];
    probcTemp = [];
    perf = [];
    AreaP = areaPoly{2,ii};
%     idxTemp = find(dataAll.SubjectID == ii &...
%         dataAll.Condition == 1 & dataAll.AreaP < 100);
    %     edgesTemp = [1:max(dataAll.AreaP(idxTemp))/3:max(dataAll.AreaP(idxTemp))];
%     [valsArea,orderAreaTemp] = sort(dataAll.AreaP(idxTemp));
%     orderArea = find(dataAll.AreaP(idxTemp)' == valsArea);
%         Y = discretize(dataAll.AreaP(idxTemp),3);
         
%        figure;
%        histogram(dataAll.AreaP(idxTemp),100)
%     chunks = floor(length(orderArea)/3);
    counter = 1;
    for t = 1:length(AreaP)
        if AreaP(t) < bin(ii,2)
            Y(t) = 1;
            counter = counter + 1;
        elseif AreaP(t) > bin(ii,1)%chunks/2 && dataAll.AreaP(t) < chunks
            if AreaP(t) > 100
                Y(t) = 0;
            else
                Y(t) = 2;
                counter = counter + 1;
            end
        else
            Y(t) = 0;
%             Y(counter) = 3;
        end
    end
    numTrialsY{ii} = Y;
    dataTemp = AreaP;
    perf = perfCur{2,ii};%dataAll.Perf(idxTemp);
    coneOver = probSameConesforTrace{2,ii};
    for i = 1:length(1:numBinsPerf)
        perfTemp(ii,i) = nanmean(perf(find(Y == i)));
        areaTemp(ii,i) = nanmean(dataTemp(find (Y == i)));
        coneTemp(ii,i) = nanmean(coneOver(find (Y == i)));
    end
areaNorm(ii,:) = normalize(areaTemp(ii,:),'norm',1);    
coneNorm(ii,:) = normalize(coneTemp(ii,:),'center','mean');  
perfNorm(ii,:) = normalize(perfTemp(ii,:),'center','mean');  
end
idxPlot = ~isnan(coneTemp)
mdl1 = fitlm([coneTemp(idxPlot)]',...%x
        [areaTemp(idxPlot)]')

colormap = jet(length(subjectsAll))
figure;
plot(mdl1)
hold on
plot([coneTemp(:,1)]',...%x
    [areaTemp(:,1)]','o','Color','b','MarkerFaceColor','b');
plot([coneTemp(:,2)]',...%x
    [areaTemp(:,2)]','o','Color','k','MarkerFaceColor','k');
%%%%
clear meanEachProb
clear allProbsUnique
clear newDataAll
newDataAll = dataAll(find(dataAll.Condition == 1 & ... 
    dataAll.Area > 0 & dataAll.Area < 100),:);

% mdlAll = fitlm(newDataAll,'SameConeProb~Area*SubjectID')
allProbsUnique = unique(newDataAll.SameConeProb);
for u = 1:length(allProbsUnique)
    tempIdx = [];
    tempIdx = find(newDataAll.SameConeProb == allProbsUnique(u));
    meanEachProb(u) = mean(newDataAll.Area(tempIdx));
end
% mdlAll = fitlm((newDataAll.SameConeProb,(newDataAll.AreaP))
mdlAll = fitlm(allProbsUnique',(meanEachProb))

figure;
% subplot(1,2,1)
plot(mdlAll)
xlabel('Probability');
ylabel('Ocular Drift Area');
% subplot(1,2,2)
% mdlAll = fitlm(allProbsUnique',log(meanEachProb))
% 
% plot(mdlAll)
% xlabel('Probability');
% ylabel('LOG Ocular Drift Area');
% set(gca, 'YScale', 'log')

figure;
% hold on
for ii = 1:length(subjectsAll)
    subplot(4,4,ii)
    scatter1 = scatter([dataAll.SameConeProb(find(dataAll.Condition == 1 & ...
        dataAll.SubjectID == ii))],...
        [(dataAll.Area(find(dataAll.Condition == 1 & ...
        dataAll.SubjectID == ii)))],...
        'MarkerFaceColor',[.8 .8 .8],'MarkerEdgeColor',[.8 .8 .8]);
    scatter1.MarkerFaceAlpha = 0;
    scatter1.MarkerEdgeAlpha = 0;
    if ii == 10
        mdlEach{ii} = fitlm([dataAll.SameConeProb(find(dataAll.Condition == 1 & ...
            dataAll.SubjectID == ii & dataAll.Area < 100))],...
            ([(dataAll.Area(find(dataAll.Condition == 1 & ...
            dataAll.SubjectID == ii & dataAll.Area < 100)))]));
    else
        mdlEach{ii} = fitlm([dataAll.SameConeProb(find(dataAll.Condition == 1 & ...
            dataAll.SubjectID == ii))],([(dataAll.Area(find(dataAll.Condition == 1 & ...
            dataAll.SubjectID == ii)))]));
    end
    hold on
    plot(mdlEach{ii});
    title(sprintf('p = %.2f, R-squared = %.2f',...
        mdlEach{ii}.Coefficients.pValue(2),...
        mdlEach{ii}.Rsquared.Ordinary));
    if ii == 1
        xlabel('Probability')
        ylabel('Ocular Drift Area (arcmin^2)')
    else
        xlabel('');
        ylabel('');
    end
    title(sprintf('S%i',ii))
%     if ii ~= 10
        text(.10,125,'p < 0.001');
%     else
%         text(.10,125,sprintf('p > %.2f',mdlEach{ii}.Coefficients.pValue(2)));
%     end
    text(.10,110,sprintf('R-squared = %.2f',mdlEach{ii}.Rsquared.Ordinary));
    legend off
    xlim ([0 .8])
    ylim([0 150])
    axis square
end
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/SUPPFigAreaIndSub.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/SUPPFigAreaIndSub.eps');

% hold on
figure;
h = plot(mdlAll)
% delete(h(1))
legend off
xlabel('Probability of Shared Cone Stimulation')
ylabel('Ocular Drift Area (arcmin^2)')
title('');
ylim([0 6])
axis square
% set(gca, 'YScale', 'log')
text(.10,120,sprintf('p > %.2f',mdlAll.Coefficients.pValue(2)));
text(.10,110,sprintf('R-squared = %.2f',mdlAll.Rsquared.Ordinary));

% ylim([-1 300])
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/AllTrialsAreaCP.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/NewDataAnalysis/AllTrialsAreaCP.epsc');





% [vals,order] = sort([perfTemp(idxPlot)]');
% 
% temp1 = [coneTemp(idxPlot)]';
% temp2 = [areaTemp(idxPlot)]';
% temp3 = [perfTemp(idxPlot)]';
% % colormap hot;
% for c = 1:length(temp1)
% %     surf(temp1,temp2,temp3)
% %     figure;
% %     plot3(temp1,temp2,temp3,'o')
%     plot(temp1(c),...%x
%         temp2(c),'o',...
%         'Color','k');%,'MarkerFaceColor',colormap(order(c),:))
% end
% legend off
% colorbar
% ylabel('Area (arcmin^2)')
% xlabel('Probability of Cone Overlap');

figure;
temp1 = coneNorm;%coneNorm;
temp2 = areaNorm;
temp3 = perfTemp;
cmapInd = flipud(copper(numBinsPerf));
mdl2 = fitlm([coneNorm(idxPlot)]',...%x
        [areaNorm(idxPlot)]')
plot( mdl2 )
hold on
% for c = 1:13
%         plot(temp1(c,:),...%x,temp3(:,c)
%             temp2(c,:),'-',...
%             'Color','k');
%         hold on
% end
hold on
for c = 1:13
    [x,orderInd] = sort(temp3(c,:));
    for t = 1:numBinsPerf
        plot(temp1(c,t),...%x,temp3(:,c)
            temp2(c,t),'o',...
            'Color','k','MarkerFaceColor',cmapInd(orderInd == t,1:3));
        hold on
    end
    grid on
    xlabel('Normalized Probability of Shared Cone Stimulation')
    ylabel('Normalized Area')
    hold on
end

legend off
% colormap copper
a = (colorbar);
a.Label.String = 'Proportion Correct';
a.TickLabels = {'High','Medium','Low'}
a.Ticks = [.2 .7 1]
% a.Location = 'west'
% set( a, 'YDir', 'reverse' );
a.Ruler.TickLabelRotation=90;
% axis([0.1 1.1 0 1.1]) 
% ylim([-120 120])
title('');


% mdl3 = fitlm([coneNorm(idxPlot)]',...%x
%         [perfTemp(idxPlot)]')
% mdl4 = fitlm([areaNorm(idxPlot)]',...%x
%         [perfTemp(idxPlot)]')    
% figure;
% x = temp1;
% y = temp2;
% f = temp3;
% plot3(x,y,f,'o')
% xlabel('cone')
% ylabel('area')
% zlabel('perf')

%%
crowdingEffect(1,:) = [tbl.Crowded(:).thresh]-[tbl.Uncrowded(:).thresh];
crowdingEffect(2,:) = performanceThresh(2,:) - performanceThresh(1,:)
idx = find(averageTime(2,:) > 0);
figure;
subplot(2,3,1)
 [~,p,b,r] = LinRegression(...
        [averageTime(2,idx)],...%x
        crowdingEffect(idx),... %y
        0,NaN,1,0);
xlabel('% on C Flanker')
ylabel('Crowding Effect')
axis([0 0.6 -2 2])

subplot(2,3,2)
 [~,p,b,r] = LinRegression(...
        [averageTimeTarget(2,idx)],...%x
         crowdingEffect(idx),... %y
        0,NaN,1,0);
xlabel('% on C Target')
ylabel('Difference in Performance')
axis([0 0.6 -2 2])

subplot(2,3,3)
 [~,p,b,r] = LinRegression(...
        [averageTime(1,idx)],...%x
        performanceThresh(1,idx) - performanceThresh(2,idx) ,... %y
        0,NaN,1,0);
xlabel('% on C Flanker')
ylabel('Crowding Effect')
axis([0 .6 -.4 .4])

subplot(2,3,4)
 [~,p,b,r] = LinRegression(...
        [averageTimeTarget(1,idx)],...%x
         performanceThresh(1,idx) - performanceThresh(2,idx),... %y
        0,NaN,1,0);
xlabel('% on C Target')
ylabel('Crowding Effect')
axis([0 .6 -.4 .4])

subplot(2,3,5)
 [~,p,b,r] = LinRegression(...
        [averageTime(1,idx)],...%x
        performanceThresh(1,idx) - performanceThresh(2,idx) ,... %y
        0,NaN,1,0);
xlabel('% on U Flanker')
ylabel('Crowding Effect')
axis([0 0.6 -.4 .4])

subplot(2,3,6)
 [~,p,b,r] = LinRegression(...
        [averageTimeTarget(1,idx)],...%x
         performanceThresh(1,idx) - performanceThresh(2,idx),... %y
        0,NaN,1,0);
xlabel('% on U Target')
ylabel('Difference in Performance')
axis([0 .6 -.4 .4])
    
%%
    for ii = 1:length(performanceMeans)
        performanceMeans(1,ii) = mean([performance{1,ii}]);
        performanceMeans(2,ii) = mean([performance{2,ii}]);
    end

figure;
    subplot(1,2,1)
    [~,p,b,r] = LinRegression(...
        [timeSpentOnTarget(2,exIdx)-timeSpentOnTarget(1,exIdx)],...%x
        performanceMeans(1,exIdx)-performanceMeans(2,exIdx),... %y
        0,NaN,1,0);
    subplot(1,2,2)
    [~,p,b,r] = LinRegression(...
        [timeOnFlankers(2,exIdx)-timeOnFlankers(1,exIdx)],...%x
        performanceMeans(1,exIdx)-performanceMeans(2,exIdx),... %y
        0,NaN,1,0);

    for ii = 1:length(subjectsAll)
        diffSpan(ii) = median(span{2,ii})/...
            median(abs(savVar(ii).meanDistX{1}));
        diffCone(ii) = median(probSameConesforTrace{2,ii});
        %*median(span{1,ii});
    end
    figure;
    [~,p,b,r] = LinRegression(...
        (diffCone),...%x
        crowdingEffect,... %y
        0,NaN,1,0);
xlabel('Probability of Overlap/Crowded Span');
ylabel('Crowding Effect');
mdl_2 = fitlm(crowdingEffect,diffCone)










