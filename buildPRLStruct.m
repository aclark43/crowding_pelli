function [distTabUnc, distTabCro] = buildPRLStruct...
    (justThreshSW, params, subjectUnc, subjectThreshUnc, subjectCro, subjectThreshCro, thresholdSWVals)

%Builds PRL struct through different SW
counterC = 1;
counterU = 1;

if justThreshSW
    for ii = 1:params.subNum
        if ii == 11
            distTabUnc(counterU).subject = 2;
            distTabCro(counterC).subject = 2;
        elseif ii == 12
            distTabUnc(counterU).subject = 4;
            distTabCro(counterC).subject = 4;
        elseif ii == 13
            distTabUnc(counterU).subject = 8;
            distTabCro(counterC).subject = 8;
        elseif ii == 14
            distTabUnc(counterU).subject = 9;
            distTabCro(counterC).subject = 9;
        else
            distTabUnc(counterU).subject = ii;
            distTabCro(counterC).subject = ii;
        end
        strokewidth = thresholdSWVals(ii).thresholdSWUncrowded;
        
        
        distTabUnc(counterU).swSize = regexp(thresholdSWVals(ii).thresholdSWUncrowded,'\d*','Match');
        distTabUnc(counterU).cond = 'uncrowded';
        distTabUnc(counterU).stimSize = ...
            double(subjectThreshUnc(ii).em.ecc_0.(strokewidth).stimulusSize);
        distTabUnc(counterU).meanPRLDistance = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistance);
        distTabUnc(counterU).meanXPRLDistance = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistanceX);
        distTabUnc(counterU).meanYPRLDistance = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistanceY);
        distTabUnc(counterU).sizePerform = subjectThreshUnc(ii).em.ecc_0.(strokewidth).performanceAtSize;
        distTabUnc(counterU).threshold = subjectThreshUnc(ii).thresh;
        distTabUnc(counterU).span = subjectThreshUnc(ii).em.ecc_0.(strokewidth).meanSpan;
        distTabUnc(counterU).area = subjectThreshUnc(ii).em.ecc_0.(strokewidth).areaCovered;
        distTabUnc(counterU).dc = subjectThreshUnc(ii).em.ecc_0.(strokewidth).dCoefDsq;
        
        if strcmp('Drift_MS',params.em)
            distTabUnc(counterU).meanPRLDistanceWholeTrace = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistanceWholeTrace);
            distTabUnc(counterU).spanWholeTrace = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).spanWholeTrace);
        end
        for xPos = 1:length(subjectThreshUnc(ii).em.ecc_0.(strokewidth).position)
            x(xPos) = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).x(:));
            y(xPos) = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).y(:));
        end
%         figure;
        
        distTabUnc(counterU).PRLDistanceFromEdge = single(getPRLFromTargetEdge...
            (distTabUnc(counterU).stimSize, x, y));
        close all;
        %              title(sprintf('Uncrowded %i',ii));
        counterU = counterU + 1;
        
        
        strokewidth = thresholdSWVals(ii).thresholdSWCrowded;
        
        
        distTabCro(counterC).swSize = regexp(thresholdSWVals(ii).thresholdSWCrowded,'\d*','Match');
        distTabCro(counterC).cond = 'crowded';
        distTabCro(counterC).stimSize = subjectThreshCro(ii).em.ecc_0.(strokewidth).stimulusSize;
        distTabCro(counterC).meanPRLDistance = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).prlDistance);
        distTabCro(counterC).meanXPRLDistance = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).prlDistanceX);
        distTabCro(counterC).meanYPRLDistance = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).prlDistanceY);
        distTabCro(counterC).sizePerform = subjectThreshCro(ii).em.ecc_0.(strokewidth).performanceAtSize;
        distTabCro(counterC).threshold = subjectThreshCro(ii).thresh;
        distTabCro(counterC).span = subjectThreshCro(ii).em.ecc_0.(strokewidth).meanSpan;
        distTabCro(counterC).area = subjectThreshCro(ii).em.ecc_0.(strokewidth).areaCovered;
        distTabCro(counterC).dc = subjectThreshCro(ii).em.ecc_0.(strokewidth).dCoefDsq;

        if strcmp('Drift_MS',params.em)
            distTabCro(counterU).meanPRLDistanceWholeTrace = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistanceWholeTrace);
            distTabCro(counterU).spanWholeTrace = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).spanWholeTrace);
        end
        for xPos = 1:length(subjectThreshCro(ii).em.ecc_0.(strokewidth).position)
            x(xPos) = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).x(:));
            y(xPos) = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).y(:));
        end
        %             figure;
        
        distTabCro(counterC).PRLDistanceFromEdge = single(getPRLFromTargetEdge...
            (distTabCro(counterC).stimSize, mean(x), mean(y)));
        title(sprintf('Subject %s',params.subjectsAll{ii}));
        legend('Uncrowded','Crowded');
        counterC = counterC + 1;
        saveas(gcf,sprintf('../../Data/AllSubjTogether/meanXYOnMarker%s.png', params.subjectsAll{ii}));
        
        
    end
else
    for ii = 1:(params.subNum)
        %         sw = nonzeros(subjectUnc(ii).SW)';
        sw = fieldnames(subjectThreshUnc(ii).em.ecc_0);
        for i = 1:length(sw)
            %                         strokewidth = sprintf('strokeWidth_%i',sw{i});
            strokewidth = char(sw(i));
            if ii == 11
                distTabUnc(counterU).subject = 2;
                distTabCro(counterC).subject = 2;
            elseif ii == 12
                distTabUnc(counterU).subject = 4;
                distTabCro(counterC).subject = 4;
            elseif ii == 13
                distTabUnc(counterU).subject = 8;
                distTabCro(counterC).subject = 8;
            elseif ii == 14
                distTabUnc(counterU).subject = 9;
                distTabCro(counterC).subject = 9;
            else
                distTabUnc(counterU).subject = ii;
                distTabCro(counterC).subject = ii;
            end
            distTabUnc(counterU).swSize = (cell2mat(regexp(char(sw(i)),'\d*','Match')));
            distTabUnc(counterU).cond = 'uncrowded';
            distTabUnc(counterU).stimSize = ...
                double(subjectThreshUnc(ii).em.ecc_0.(strokewidth).stimulusSize);
            distTabUnc(counterU).meanPRLDistance = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistance);
            distTabUnc(counterU).meanXPRLDistance = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistanceX);
            distTabUnc(counterU).meanYPRLDistance = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistanceY);
            distTabUnc(counterU).PRLDistance = subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistance;
            
            for xPos = 1:length(subjectThreshUnc(ii).em.ecc_0.(strokewidth).position)
                x(xPos) = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).x(:));
                y(xPos) = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).y(:));
            end
            
            distTabUnc(counterU).PRLDistanceFromEdge = single(getPRLFromTargetEdge...
                (distTabUnc(counterU).stimSize, mean(x), mean(y)));
            close all;
            
            distTabUnc(counterU).correct = subjectThreshUnc(ii).em.ecc_0.(strokewidth).correct;
            
            distTabUnc(counterU).sizePerform = subjectThreshUnc(ii).em.ecc_0.(strokewidth).performanceAtSize;
            distTabUnc(counterU).threshold = subjectThreshUnc(ii).thresh;
            distTabUnc(counterU).span = subjectThreshUnc(ii).em.ecc_0.(strokewidth).meanSpan;
            distTabUnc(counterU).area = subjectThreshUnc(ii).em.ecc_0.(strokewidth).areaCovered;
            if strcmp('Drift_MS',params.em)
                distTabUnc(counterU).meanPRLDistanceWholeTrace = ...
                    mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).prlDistanceWholeTrace);
                distTabUnc(counterU).spanWholeTrace = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).spanWholeTrace);
            end
            counterU = counterU + 1;
        end
        sw = fieldnames(subjectThreshCro(ii).em.ecc_0);
        %         sw = nonzeros(subjectCro(ii).SW)';
        for i = 1:length(sw)
            %             strokewidth = sprintf('strokeWidth_%i',i);
            strokewidth = char(sw(i));
            if ii == 11
%                 distTabUnc(counterU).subject = 2;
                distTabCro(counterC).subject = 2;
            elseif ii == 12
%                 distTabUnc(counterU).subject = 4;
                distTabCro(counterC).subject = 4;
            elseif ii == 13
%                 distTabUnc(counterU).subject = 8;
                distTabCro(counterC).subject = 8;
            elseif ii == 14
%                 distTabUnc(counterU).subject = 9;
                distTabCro(counterC).subject = 9;
            else
%                 distTabUnc(counterU).subject = ii;
                distTabCro(counterC).subject = ii;
            end
            distTabCro(counterC).swSize = (cell2mat(regexp(char(sw(i)),'\d*','Match')));
            distTabCro(counterC).cond = 'crowded';
            distTabCro(counterC).stimSize = subjectThreshCro(ii).em.ecc_0.(strokewidth).stimulusSize;
            distTabCro(counterC).meanPRLDistance = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).prlDistance);
            distTabCro(counterC).meanXPRLDistance = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).prlDistanceX);
            distTabCro(counterC).meanYPRLDistance = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).prlDistanceY);
            distTabCro(counterC).PRLDistance = subjectThreshCro(ii).em.ecc_0.(strokewidth).prlDistance;
            
            for xPos = 1:length(subjectThreshCro(ii).em.ecc_0.(strokewidth).position)
                x(xPos) = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).x(:));
                y(xPos) = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).y(:));
            end
            
            distTabCro(counterC).PRLDistanceFromEdge = single(getPRLFromTargetEdge...
                (distTabCro(counterC).stimSize, mean(x), mean(y)));
            close all;
            
            distTabCro(counterC).correct = subjectThreshCro(ii).em.ecc_0.(strokewidth).correct;
            
            distTabCro(counterC).sizePerform = subjectThreshCro(ii).em.ecc_0.(strokewidth).performanceAtSize;
            distTabCro(counterC).threshold = subjectThreshCro(ii).thresh;
            distTabCro(counterC).span = subjectThreshCro(ii).em.ecc_0.(strokewidth).meanSpan;
            distTabCro(counterC).area = subjectThreshCro(ii).em.ecc_0.(strokewidth).areaCovered;
            if strcmp('Drift_MS',params.em)
                distTabCro(counterU).meanPRLDistanceWholeTrace = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).prlDistanceWholeTrace);
                distTabCro(counterU).spanWholeTrace = mean(subjectThreshCro(ii).em.ecc_0.(strokewidth).spanWholeTrace);
            end
            counterC = counterC + 1;
            %         end
        end
    end
end

end

