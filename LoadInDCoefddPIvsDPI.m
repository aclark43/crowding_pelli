

%Crowded
Names = {'Ashley', 'Ashley', 'Ashley', 'Z023', 'Z023', 'Z023'}';
Condition = {'Normal_DPI','3Sample_DPI','dDPI',...
             'Normal_DPI','3Sample_DPI','dDPI'}';

         
DiffCoef(1) = load('DC_AllSamp_Ashley_Crowded_Unstabilized_Drift_0ecc.mat');
DiffCoef(2) = load('DC_Every3Samp_Ashley_Crowded_Unstabilized_Drift_0ecc.mat');
DiffCoef(3) = load('DC_DDPI_AshleyDDPI_Crowded_Unstabilized_Drift_0ecc.mat');
DiffCoef(4) = load('DC_AllSamp_Z023_Crowded_Unstabilized_Drift_0ecc.mat');
DiffCoef(5) = load('DC_Every3Samp_Z023_Crowded_Unstabilized_Drift_0ecc.mat');
DiffCoef(6) = load('DC_DDPI_Z023DDPI_Crowded_Unstabilized_Drift_0ecc.mat');

for ii = 1:length(DiffCoef)
    DiffCoef1{ii} = DiffCoef(ii);
end
DiffCoef1 = [19.018 13.767 15.993 58.075 43.535 25.401]';

crowdedTab = table(Names,Condition,DiffCoef1);


Names = {'Ashley', 'Ashley', 'Ashley', 'Z023', 'Z023', 'Z023'}';
Condition = {'Normal_DPI','3Sample_DPI','dDPI',...
             'Normal_DPI','3Sample_DPI','dDPI'}';

         
DiffCoef2(1,1) = load('DC_AllSamp_Ashley_Uncrowded_Unstabilized_Drift_0ecc.mat');
DiffCoef2(1,2) = load('DC_Every3Samp_Ashley_Uncrowded_Unstabilized_Drift_0ecc.mat');
DiffCoef2(1,3) = load('DC_DDPI_AshleyDDPI_Uncrowded_Unstabilized_Drift_0ecc.mat');
DiffCoef2(1,4) = load('DC_AllSamp_Z023_Uncrowded_Unstabilized_Drift_0ecc.mat');
DiffCoef2(1,5) = load('DC_Every3Samp_Z023_Uncrowded_Unstabilized_Drift_0ecc.mat');
DiffCoef2(1,6) = load('DC_DDPI_Z023DDPI_Uncrowded_Unstabilized_Drift_0ecc.mat');

DiffCoef3 = [21.22 16.47 8.96 40.18 29.99 33.09]';

uncrowdedTab = table(Names,Condition,DiffCoef3);
       