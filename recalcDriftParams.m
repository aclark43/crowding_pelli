function  [ speed, curvature, span ] =  recalcDriftParams (x, y)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

span = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95);

 [~, instSpX, instSpY, speed, driftAngle, curvature, varx, vary] = ...
            getDriftChar(x, y, 41, 1, 180);
end
