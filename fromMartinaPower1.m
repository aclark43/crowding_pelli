function fromMartinaPower1()

close all

%% stimuli 
digits = [3, 5, 6, 9];
% vertical or horizontal fft
vertical = 1;


%% power spectra of uncrowded stimuli
% works best when pixelAngle and widthAngle of the digit result in an even
% number of pixels for the width of the digit
pixelAngle = .25; % arcmin
widthAngle = 1; % arcmin (angular width of digit)
widthPixel = widthAngle / pixelAngle; % pixel - even number please!

bufferSizeAngle = 2*60;% arcmin
bufferSizePixel = bufferSizeAngle / pixelAngle;

sz = 2 * bufferSizePixel;
k = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);

allSpec = cell(size(digits));

ca = [-40, 20];


%% power spectra from drift (Brownian motion model)
dc = 30; % diffusion constant (arcmin^2/s)

f = linspace(1, 80, 80); % temporal frequencies (Hz)
kdc = k;%logspace(-1, 2, 100); % spatial frequencies (cpd)

psD = Qfunction(f, kdc, dc / 60^2); % assumes isotropic
Dps1 = mean(psD);


for di = 1:length(digits)
    im = drawDigit(digits(di), widthPixel);
    
    if vertical == 1
        % buffer image and make it square
        if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
            %im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel],0, 'both');
            im_buffered = padarray(im-mean(im(:,size(im,2)/2)), [bufferSizePixel, bufferSizePixel],0, 'both');
        else
            im_buffered = padarray(im-mean(im(:,(size(im,2)/2)+1)), [bufferSizePixel, bufferSizePixel],0, 'both');
        end
    else
        im_buffered = padarray(im-mean(im(round(size(im,1)/2)-1,:)), [bufferSizePixel, bufferSizePixel],0, 'both');
    end
   
    
    [m, n] = size(im_buffered);
    df = floor((m - sz)/2);
    im_buffered = im_buffered(df:(df + sz - 1), :);
    df = floor((n - sz)/2);
    im_buffered = im_buffered(:, df:(df + sz - 1));
   
    
if vertical == 1
    if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
        spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)+1)));
    else
        spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)+2)));
    end
else
     spec = fftshift(fft(im_buffered((length(im_buffered)/2)-1,:)));
end

    DpsIm = spec.*Dps1;
    Phase_spec = angle(spec);
    
    allSpec{di} = struct('PS', spec, 'k', k);
    allPhase{di} = struct('Ph', Phase_spec, 'k', k);
    
    plot_k = [2, 10, 30, 50];
    plot_k_index = interp1(k, 1:length(k), plot_k, 'n-mean(mean(im_buffered))earest');
    
    figure(4)
    semilogx(k,  pow2db((abs(spec)).^2));
    hold on
    semilogx(k,pow2db(mean(psD).^2),'r')
    a = ((abs(spec)).^2);
    b = (mean(psD).^2);
    semilogx(k,pow2db(a'.*b),'g')
    title('average vertical');
    xlabel('spatial frequency (cpd)');
    %ylim([-10, 50]);
    xlim([0.5 60])
    legend({'3','5','6', '9'}, 'Location', 'Best')
end


function digit = drawDigit(whichDigit, pixelwidth)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth + 1;
            digit(si:ei, :) = 0;
        end 
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1; 
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
end

