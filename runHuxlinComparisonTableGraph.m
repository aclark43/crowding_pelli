%Runs Comparisons Across Subjects FOR THE HUXLIN PROJECT!!!!!
close all
clear
clc

%%%%%%%%%%
%%%Controls: AshleyDDPI, HUX4, HUX10, HUX12, HUX17, HUX18, HUX21, Martina,
%%%HUX22, HUX23
%%%Patients: HUX1, HUX3, HUX5, HUX6, HUX7, HUX8, HUX9, HUX11, HUX13, HUX14, HUX16
%% HUX5 is Only fixation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%FOR DRIFT ONLY ANALYSIS
subjectsAll = {'HUX1','HUX3','HUX5','HUX6','HUX7','HUX8','HUX9','HUX11','HUX13','HUX14','HUX16',...
    'HUX4','HUX18','HUX10','HUX12','HUX17','HUX21','HUX22','HUX23','Martina','HUX24',...
    'HUX25','HUX27','HUX28','HUX29','HUX30'}; %,'AshleyDDPI'};HUX17
% subjectsAll = {'HUX21','HUX22;,HUX23'}; %,'AshleyDDPI'};HUX17

%%
ageAll = [29 42 52 43 55 61 75 48 68 60 68 50 46 25 40 42 28 59 59 41 50 40 49 68 72 28];
sexAll = [1 1 2 2 2 2 2 2 2 2 2 1 2 1 1 1 1 2 1 1 1 1 1 2 2 1];
weeksPostLes = [186.1...
5.4...
280.7...
11.9...
249.6...
14.4...
123.9...
35 ...
27.6...
520.4...
10.3...
0 0 0 0 0 0 0 0 0 0 ...
26.6...
24 ...
159.7...
53.1...
14.6...
];

affHemText = {'partial hemianope, central 10° spared'...
'partial hemianope, central 10° spared',...
'bilateral continuous scotoma, central 10° affected bilaterally'...
'clean hemianope, central 10° affected'...
'partial hemianope, central 10° affected'...
'partial hemianope, central 10° affected'...
'upper quadrantanope, central 10° affected'...
'scotoma, central 10d° affected'...
'large scotoma, central 10° partially affected'...
'hemianope, central 10° spared'...
'clean hemianope, central 10° affected'...
'','','','','','','','','','',...
'upper left quandrantanopia, central 10° affected'...
'partial right hemianope, central 10° affected in upper field'...
'Partial right hemianope, central 10° largely spared'...
'lower right quadrantanope, central 10° affected in upper field'...
'partial right hemianope, central 10° affected in upper field'};

% for ii = 1:subNum
%     if control(ii) == 1
%         angleOfLoss(ii) = NaN;
%     else
%         angleOfLoss(ii) = vfResults.patientInfo.(huxID{ii}).middleAngleLoss
%     end
% end

for ii = 1:length(subjectsAll)
    if strcmp('HUX4',subjectsAll{ii}) || ...
            strcmp('AshleyDDPI',subjectsAll{ii}) || ...
            strcmp('HUX12',subjectsAll{ii}) || ...
            strcmp('HUX18',subjectsAll{ii}) || ...
            strcmp('HUX17',subjectsAll{ii}) || ...
            strcmp('HUX21',subjectsAll{ii}) || ...
            strcmp('Martina',subjectsAll{ii}) || ...
            strcmp('HUX22',subjectsAll{ii}) || ...
            strcmp('HUX23',subjectsAll{ii}) || ...
            strcmp('HUX24',subjectsAll{ii}) || ...
            strcmp('HUX10',subjectsAll{ii})
        group{ii} = 'Control';
        control(ii) = 1;
        params.group{ii} = 'Control';
        params.control(ii) = 1;
    else
        params.group{ii} = 'Patient';
        params.control(ii) = 0;
        group{ii} = 'Patient';
        control(ii) = 0;
        
    end
end

em = {'Fix'}; %Drift, Drift_MS, Fix
% c = lines(length(subjectsAll));
c = brewermap(length(subjectsAll),'Paired');
subNum = length(subjectsAll);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load in Variables
params.em = em;
params.c = c;
params.fig = struct('FIGURE_ON', 0,...
    'FOLDED',0);
fig = params.fig;

subNum = length(subjectsAll);

vfResults = load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\SummaryDocuments\HuxlinPTFigures\VFs\Scripts\VisualFieldAnalysis\patientInfoMat.mat');

for ii = 1:length(subjectsAll)
    if strcmp('HUX3',subjectsAll{ii})
        huxID{ii} = 'RBG';
    elseif strcmp('HUX5',subjectsAll{ii})
        huxID{ii} = 'JKM';
    elseif strcmp('HUX6',subjectsAll{ii})
        huxID{ii} = 'JMS';
    elseif strcmp('HUX8',subjectsAll{ii})
        huxID{ii} = 'SSS';
    elseif strcmp('HUX1',subjectsAll{ii})
        huxID{ii} = 'RAR';
    elseif strcmp('HUX7',subjectsAll{ii})
        huxID{ii} = 'DBM';
    elseif strcmp('HUX9',subjectsAll{ii})
        huxID{ii} = 'DLF';
    elseif strcmp('HUX11',subjectsAll{ii})
        huxID{ii} = 'SAM';
    elseif strcmp('HUX13',subjectsAll{ii})
        huxID{ii} = 'BPC';
    elseif strcmp('HUX14',subjectsAll{ii})
        huxID{ii} = 'DSA';
    elseif strcmp('HUX16',subjectsAll{ii})
        huxID{ii} = 'MAT';
    elseif strcmp('HUX25',subjectsAll{ii})
        huxID{ii} = 'GXA'; 
    elseif strcmp('HUX27',subjectsAll{ii})
        huxID{ii} = 'CFJ';
    elseif strcmp('HUX28',subjectsAll{ii})
        huxID{ii} = 'FXH';
    elseif strcmp('HUX29',subjectsAll{ii})
        huxID{ii} = 'RSF';
    elseif strcmp('HUX30',subjectsAll{ii})
        huxID{ii} = 'AXM';
    else
        huxID{ii} = [];
    end
end

% load(file1);
for ii = 1:length(subjectsAll)
    fileName1 = sprintf('%s_FixationResults.mat', subjectsAll{ii});
    folderName = 'MATFiles\';
    file1 = fullfile(folderName, fileName1);
    load(file1);
    fixationInfo{ii} = fixation;
    
    file2 = fullfile(folderName, sprintf('%s_Ses1FixationMSInfo.mat', subjectsAll{ii}));
    load(file2);
    msInformation{ii} = msInfo;
end

condition = {'Uncrowded'};

[subjectUnc, subjectThreshUnc, subjectMSUnc, performU] = ...
    loadVariablesHuxlin(subjectsAll, {condition{1}}, params, 1, '0ecc');
counter = 1;
for ii = 1:subNum
    if isempty(subjectUnc{ii})
      acuitySnellen(counter) = NaN;
      counter = counter + 1;
        continue;
    end
    thresholdSizeSWRound = round(subjectUnc{ii}.stimulusSize,1);
    matchingThresholdRound = round(subjectThreshUnc(ii).thresh,1);
    [~,thresholdIdxUnc] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    thresholdVals(ii).thresholdSWUncrowded = ...
        sprintf('strokeWidth_%i', subjectUnc{ii}.SW(thresholdIdxUnc));
    if ii == 3
        continue;
    end
    acuitySnellen(counter) = (subjectThreshUnc(ii).thresh/2)*20';
    counter = counter + 1;
end
[h,p] = ttest2(acuitySnellen(control == 1), acuitySnellen(control == 0));


% % %% Offset Analysis
% % allTempsControlX = [];allTempsControlY = [];
% % allTempsControlXoff = []; allTempsControlYoff = [];
% % for ii = find(control)
% %     load(sprintf('Offset/offset%s.mat',subjectsAll{ii}));
% %     for i = 2:length(offset.xOffsets)
% %         offset.xOffsetsN(i) = diff([offset.xOffsets(i-1) offset.xOffsets(i)]);
% %         offset.yOffsetsN(i) = diff([offset.yOffsets(i-1) offset.yOffsets(i)]);
% %     end
% %     figure;
% %     subplot(2,1,1)
% %     histogram(offset.xOffsetsN(offset.xOffsetsN < 60 & offset.xOffsetsN > -60),30)
% %     hold on
% %     histogram(offset.yOffsetsN(offset.yOffsetsN < 60 & offset.yOffsetsN > -60),30)
% %     xlabel('Offsets');
% %     xlim([-50 50])
% %     subplot(2,1,2)
% %     temp1(1) = histogram(offset.xStart(offset.xStart<60 & offset.xStart >-60));
% %     hold on
% %     temp1(2) = histogram(offset.yStart(offset.yStart<60 & offset.yStart >-60));
% %     xlim([-50 50])
% %     xlabel('Start Positions');
% %     legend(temp1,{'X','Y'});
% %     suptitle(sprintf('C%s',subjectsAll{ii}))
% %     allTempsControlX = [allTempsControlX offset.xStart];
% %     allTempsControlY = [allTempsControlY offset.yStart];
% %     
% %     allTempsControlXoff = [allTempsControlXoff offset.xOffsetsN];
% %     allTempsControlYoff = [allTempsControlYoff offset.yOffsetsN];
% %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sCOffset.png',subjectsAll{ii}));
% %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sCOffset.epsc',subjectsAll{ii}));
% %     perSubjOffY(ii) = mode(allTempsControlYoff(find(allTempsControlYoff > -50 & allTempsControlYoff < 50)));
% %     allTempsControlYoff = [];
% % end
% % 
% % for ii = find(~control)
% %     if ii == 3
% %         continue;
% %     end
% %      load(sprintf('Offset/offset%s.mat',subjectsAll{ii}));
% %      for i = 2:length(offset.xOffsets)
% %         offset.xOffsetsN(i) = diff([offset.xOffsets(i-1) offset.xOffsets(i)]);
% %         offset.yOffsetsN(i) = diff([offset.yOffsets(i-1) offset.yOffsets(i)]);
% %     end
% %     figure;
% %     subplot(2,1,1)
% %     histogram(offset.xOffsetsN(offset.xOffsetsN < 60 & offset.xOffsetsN > -60),30)
% %     hold on
% %     histogram(offset.yOffsetsN(offset.yOffsetsN < 60 & offset.yOffsetsN > -60),30)
% %     xlabel('Offsets');
% %     xlim([-50 50])
% %     subplot(2,1,2)
% %     temp1(1) = histogram(offset.xStart(offset.xStart<60 & offset.xStart >-60))
% %     hold on
% %     temp1(2) = histogram(offset.yStart(offset.yStart<60 & offset.yStart >-60))
% %     xlim([-50 50])
% %     xlabel('Start Positions');
% %     legend(temp1,{'X','Y'});
% %     suptitle(sprintf('P%s',subjectsAll{ii}))
% %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sPOffset.png',subjectsAll{ii}));
% %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sPOffset.epsc',subjectsAll{ii}));
% %      
% % end
% % 
% % allTemps = [];
% % allPTTemps = [];
% % for ii = find(~control)
% %     if ii == 3
% %         continue;
% %     end
% %     load(sprintf('Offset/offset%s.mat',subjectsAll{ii}));
% %      for i = 2:length(offset.xOffsets)
% %         offset.xOffsetsN(i) = diff([offset.xOffsets(i-1) offset.xOffsets(i)]);
% %         offset.yOffsetsN(i) = diff([offset.yOffsets(i-1) offset.yOffsets(i)]);
% %     end
% %     temp = [];
% %     rotateAngle = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
% %     if ii == 1
% %         rotateAngle = 315;
% %     elseif ii == 2
% %         rotateAngle = 50;
% %     elseif ii == 3
% %         rotateAngle = 145;
% %     elseif ii == 4
% %         rotateAngle = 90;
% %     elseif ii == 10
% %         rotateAngle = 90;
% %     elseif ii == 22
% %         rotateAngle = 315;
% %     elseif ii == 23
% %         rotateAngle = 90;
% %     end
% %     theta = -rotateAngle;
% %     R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
% %     v = [offset.xStart;... 
% %         offset.yStart]';
% %     v2 = [offset.xOffsetsN;... 
% %         offset.yOffsetsN]';
% %     temp = v*R;
% %     allTemps = [allTemps temp'];
% %     temptemp = v2*R;
% %     allPTTemps = [allPTTemps temptemp'];
% %     temp2 = ...
% %         atan2d(temp(:,2),temp(:,1));
% %     
% %     figure;
% %     temp1(1) = histogram(temp(find(temp(:,1) < 120 & temp(:,1) > -120)),30);
% %     hold on
% %      temp1(2) = histogram(temp(find(temp(:,2) < 120 & temp(:,2) > -120)),30);
% %     xlim([-50 50])
% %     xlabel('Start Positions');
% %     legend(temp1,{'X','Y'});
% %     suptitle(sprintf('Rotated P%s',subjectsAll{ii}))
% %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sPOffset.png',subjectsAll{ii}));
% %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sPOffset.epsc',subjectsAll{ii}));
% %     
% %     perSubjOffY(ii) = mode(allPTTemps(2,find(allPTTemps(2,:) < 50 & allPTTemps(2,:) > -50)));
% %     allPTTemps = [];
% % end
% % conrolY = allTempsControlY(allTempsControlY > -120 & allTempsControlY < 120);
% % ptY = allTemps(2,find(allTemps(2,:) > -120 & allTemps(2,:) < 120)');
% % [h,p]=ttest2(conrolY, ptY);
% % 
% % figure;
% % errorbar([1 2], [mean(conrolY) mean(ptY)],...
% %     [sem(conrolY) sem(ptY)],'-o');
% % xlim([0 3])
% % xticks([1 2])
% % xticklabels({'Controls','Patients'});
% % ylabel('Mean Starting Y Position');
% % title(sprintf('p=%.3f',p))
% % 
% % figure
% % % v1 = allTempsControlYoff(find(allTempsControlYoff < 50 & allTempsControlYoff > -50));
% % % v2 = allPTTemps(2,find(allPTTemps(2,:) > -50 & allPTTemps(2,:) < 50)');
% % v1 = perSubjOffY(find(control));
% % v2 = perSubjOffY(find(~control));
% % errorbar([1 2], [mean(abs(v1)) mean(abs(v2))],...
% %     [0 0],[sem(v2) sem(v2)],'vertical','-o');
% % xlim([0 3])
% % xticks([1 2])
% % xticklabels({'Controls','Patients'});
% % ylabel('Mean Starting Y Offset');
% % [h,p2]=ttest2(abs(v1), abs(v2));
% % title(sprintf('p=%.3f',p2))
% % ylim([-0.2 2])

%% get the angles for rotation as circ_mean
% for ii = find(~control)
%     allDriftAngleRot = [];
%     rotateAngle(ii) = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
%     lossIdx = find(vfResults.patientInfo.(huxID{ii}).deficit);
%     lossAngles = vfResults.patientInfo.(huxID{ii}).rAngles(lossIdx);
%     meanAngleRad(ii) = circ_mean(lossAngles');
%     meanAngleDeg(ii) = mod(rad2deg(meanAngleRad),360);
%     
% end


%% MS Analysis
 huxMSAnalysis(subjectsAll, subjectThreshUnc, vfResults, control, huxID)

%% Drift Analysis
huxDriftAnalysis(fixationInfo, subjectsAll, subjectThreshUnc, vfResults, control, huxID)
%% Fixation Analysis
% if strcmp('Fix',em)
%     fixStruct = fixationAnalysisMultipleSubjects(fixationInfo, subjectsAll, subNum, params);
    [indSessions] = plotSessionInfo(subjectsAll,group,c,control);
    fixStruct = fixationAnalysisMultipleSubjects_Simplified(fixationInfo, subjectsAll, subNum, params);
%     fixStruct = fixationAnalysisMultipleSubjects(fixationInfo, subjectsAll, subNum, params);
    [chronic, numTrain] = huxlinChronAcuteList (subjectsAll);
    for ii = 1:subNum
        dataClean(ii).subject = subjectsAll{ii};
        dataClean(ii).group = group{ii};
        dataClean(ii).numberFixationTrials = length(fixationInfo{ii}.tracesX);
        
        if strcmp(subjectsAll{ii},'HUX5') || strcmp(subjectsAll{ii},'HUX24') || strcmp(subjectsAll{ii},'HUX9')
            %         continue;
            dataClean(ii).Fixation = length(fixationInfo{ii}.tracesX);
            fileName = sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/TossedInformation.xlsx',subjectsAll{1});
            [~,Labels] = xlsread(fileName, 1,'A1:U1');
            for i = 1:length(Labels)
                dataClean(ii).(Labels{i}) = 0;
            end
            dataClean(ii).TotalTossed = 0;
            %         dataClean(ii).numberValidTaskTrials = 0;
            %         %         dataClean(ii).totalValidTaskFixationTrials = length(fixationInfo(ii).tracesX);
            %         %         dataClean(ii).totalTaskTrials = 0;
            %         dataClean(ii).totalTrials = length(fixationInfo(ii).tracesX);
            %         dataClean(ii).tossedTaskTrials = NaN;
            %         dataClean(ii).tossedForMS = NaN;
            %         %         dataClean(ii).numberTaskTrials = NaN;
            dataClean(ii).thresholds = NaN;
        else
            fileName = sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/TossedInformation.xlsx',subjectsAll{ii});
            [~,Labels] = xlsread(fileName, 1,'A1:U1');
            [TrialNumbers,~] = xlsread(fileName,1,'A2:U2');
            
            for i = 1:length(Labels)
                %             name = cell2mat(Labels(
                dataClean(ii).(Labels{i}) = TrialNumbers(i);
            end
            
            dataClean(ii).TotalTossed = sum([TrialNumbers(1:8) ...
                TrialNumbers(10:11) TrialNumbers(14:20)]);
            %         dataClean(ii).numberValidTaskTrials = length(subjectThreshUnc(ii).em.allTraces.span);
            %         %         dataClean(ii).totalValidTaskFixationTrials = subjectThreshUnc(ii).em.numValidTrials+length(fixationInfo(ii).tracesX);
            %         %         dataClean(ii).totalTaskTrials = 0;
            %         %         dataClean(ii).totalTaskTrials = length(subjectThreshUnc(ii).em.valid);
            %         dataClean(ii).totalTrials = length(subjectThreshUnc(ii).em.valid);
            %         dataClean(ii).tossedTaskTrials = (length(subjectThreshUnc(ii).em.valid) - sum(subjectThreshUnc(ii).em.valid)) ...
            %             - length(fixationInfo(ii).tracesX);
            %         dataClean(ii).tossedForMS = sum(subjectThreshUnc(ii).em.msValid);
            dataClean(ii).thresholds = subjectThreshUnc(ii).thresh;
        end
        
    end
    data = struct2table(dataClean);
    writetable(data,'..\..\SummaryDocuments\huxpatientsummaries\LaTex\Overleaf_StrokeStudy\figures\DataOutputs\TossedInformation.xlsx');
%% create single data struct for all data
tableInfo = [];
counter = 1;
for ii = 1:subNum
    if ii == 3
        continue
    end
    taskCount = 1;
    for pp = 1:length(subjectThreshUnc(ii).em.valid)
        
        if subjectThreshUnc(ii).em.valid(pp) ~= 1
            continue
        end
        tableInfo(counter).Subject = ii;
        tableInfo(counter).Control = control(ii);
        tableInfo(counter).Task = 1;
        tableInfo(counter).Performance = subjectThreshUnc(ii).em.performance(pp);
        tableInfo(counter).Size = subjectThreshUnc(ii).em.size(pp);
        tableInfo(counter).x = subjectThreshUnc(ii).em.x{pp};
        tableInfo(counter).y = subjectThreshUnc(ii).em.y{pp};
        tableInfo(counter).SamplingRate = 341;
        tableInfo(counter).DriftOnly = 1 ;
        tableInfo(counter).MS = 0 ;
        tableInfo(counter).curvature = subjectThreshUnc(ii).em.allTraces.curvature{taskCount};
        tableInfo(counter).bcea = subjectThreshUnc(ii).em.allTraces.bcea{taskCount};
        tableInfo(counter).span = subjectThreshUnc(ii).em.allTraces.span(taskCount);
        tableInfo(counter).amplitude = subjectThreshUnc(ii).em.allTraces.amplitude(taskCount);
        tableInfo(counter).mean_speed = (subjectThreshUnc(ii).em.allTraces.mn_speed{1,taskCount});
        tableInfo(counter).instX = subjectThreshUnc(ii).em.allTraces.instSpX{taskCount};
        tableInfo(counter).instY = subjectThreshUnc(ii).em.allTraces.instSpY{taskCount};
        tableInfo(counter).angle = subjectThreshUnc(ii).em.allTraces.driftAngle{taskCount};
        counter = counter + 1;
        taskCount = taskCount + 1;
    end
end

for ii = 1:subNum
    for pp = 1:length(fixationInfo{ii}.tracesX)
         
        tableInfo(counter).Subject = ii;
        tableInfo(counter).Control = control(ii);
        tableInfo(counter).Task = 0;
        tableInfo(counter).Performance = NaN;
        tableInfo(counter).Size = 16;
        tableInfo(counter).x = fixationInfo{ii}.tracesX{pp};
        tableInfo(counter).y = fixationInfo{ii}.tracesY{pp};
        tableInfo(counter).SamplingRate = 341;
        tableInfo(counter).instX = fixationInfo{ii}.velocityX{pp};
        tableInfo(counter).instY = fixationInfo{ii}.velocityY{pp};
        
        
        [spanV1, ...
            instSpX, ...
            instSpY, ...
            mn_speed, ...
            driftAngle, ...
            mn_cur, ...
            varx, ...
            vary, ...
            bcea,...
            span, ...
            amplitude, ...
            prlDistance,...
            prlDistanceX, ...
            prlDistanceY] = ...
            getDriftChar(fixationInfo{ii}.tracesX{pp},...
            fixationInfo{ii}.tracesY{pp}', 41, 1, 250,341);
        
        tableInfo(counter).curvature = mn_cur;
        tableInfo(counter).bcea = bcea;
        tableInfo(counter).span = span;
        tableInfo(counter).amplitude = amplitude;
        tableInfo(counter).mean_speed = mn_speed;
        tableInfo(counter).angle = driftAngle;     
        counter = counter + 1;
    end
end
for ii = 1:subNum
    for pp = 1:length(subjectMSUnc(ii).em.ms.ecc_0.strokeWidth_All)
        tableInfo(counter).ID = mn_cur;
        
    end
end


    
save('tableInfoStoke.mat','tableInfo');
save('msTaskStroke.mat','msTask');

counter = 1;
for ii = 1:subNum
    table1(counter).ID = ii;
    if isempty(huxID{ii})
        table1(counter).Name = subjectsAll{ii};
    else
        table1(counter).Name = sprintf('%s, %s',subjectsAll{ii},huxID{ii});
    end
    table1(counter).Stroke = (~control(ii));
    table1(counter).Sex = sexAll(ii);
    table1(counter).Age = ageAll(ii);
    table1(counter).Acuity = acuitySnellen(ii);
    if ii == 3
         table1(counter).Acuity = 200;
    end
    table1(counter).WeeksPostLesion = weeksPostLes(ii);
    table1(counter).TextHemifield = affHemText{ii};
    table1(counter).AngleOfLoss = angleOfLoss(ii);
    counter = counter+1;
end
save('sumTableStroke.mat','table1');

    %% example trace
% figure;
subjectIdxMap = 7;
num = 8;
z = [];
for t = num
    figure;
%     subplot(1,2,1)
    x = fixationInfo{subjectIdxMap}.tracesX{t};
    y = fixationInfo{subjectIdxMap}.tracesY{t};
    newIdx = find( x < 60 & x > -60 & y < 60 & y > -60);
    x = x(newIdx);
    y = y(newIdx);
%     z = 1:length(x);
    
%     valsS = 30;
%     vals = 900;
    x = x([41:203,255:301,345:474,513:566,609:661,706:758,852:882,...
        932:955,996:1087,1120:1149,1178:1230,1281:1333,1380:1498,1553:1616]);
    y = y([41:203,255:301,345:474,513:566,609:661,706:758,852:882,...
        932:955,996:1087,1120:1149,1178:1230,1281:1333,1380:1498,1553:1616]);
    z = (1:length(x));
    
    b = surf([x(:) x(:)], [y(:) y(:)], [z(:) z(:)], ...  % Reshape and replicate data
        'FaceColor', 'none', ...    % Don't bother filling faces with color
        'EdgeColor', 'interp', ...  % Use interpolated color for edges
        'LineWidth', 2);
    shading interp
    view(2)
    axisVal = 25;
    c = colorbar;
    c.Label.String = 'Time (ms)';
    
    caxis([1, length(x)])
    axis([-axisVal axisVal, -axisVal axisVal])
    axis square
    hold on
    rectangle('Position',[-8,-8,16,16])
%     subplot(1,2,2)
%     J = (vfResults.patientInfo.(huxID{subjectIdxMap}).matrix{1});
%     imagesc(J);
%     axis square
%     suptitle(sprintf('%i',t))
end
% num = 35; %2 = 25, 30, 35
% clrs = colormap(parula(length(subjectThreshUnc(2).em.allTraces.x{num})));
% figure;
% plot(subjectThreshUnc(2).em.allTraces.x{num},...
%         subjectThreshUnc(2).em.allTraces.y{num},...
%         '-');
%     hold on
% for t = 1:length(subjectThreshUnc(2).em.allTraces.x{num})
%     plot(subjectThreshUnc(2).em.allTraces.x{num}(t),...
%         subjectThreshUnc(2).em.allTraces.y{num}(t),...
%         'o','Color',clrs(t,:),'MarkerFaceColor',clrs(t,:))
%     hold on
% end
% axisVal = 20
% c = colorbar;
% c.Label.String = 'Time (ms)';
% caxis([0, 500])
% axis([-axisVal axisVal, -axisVal axisVal])
% text(-10,12,{'<- Blind Field'}) 


subjectIdxMap = 7;
num = 12;
z = [];
for t = num
    figure;
%     subplot(1,2,1)
    x = fixationInfo{subjectIdxMap}.tracesX{t};
    y = fixationInfo{subjectIdxMap}.tracesY{t};
    newIdx = find( x < 60 & x > -60 & y < 60 & y > -60);
    x = x(newIdx);
    y = y(newIdx);
    z = 1:length(x);
    
%     valsS = 30;
%     vals = 900;
    x = x([1:77,208:314,347:363,404:415,507:550,637:998,1032:1082]);
    y = y([1:77,208:314,347:363,404:415,507:550,637:998,1032:1082]);
    z = (1:length(x));
    
    b = surf([x(:) x(:)], [y(:) y(:)], [z(:) z(:)], ...  % Reshape and replicate data
        'FaceColor', 'none', ...    % Don't bother filling faces with color
        'EdgeColor', 'interp', ...  % Use interpolated color for edges
        'LineWidth', 2);
    shading interp
    view(2)
    axisVal = 25;
    c = colorbar;
    c.Label.String = 'Time (ms)';
    
    caxis([1, length(x)])
    axis([-axisVal axisVal, -axisVal axisVal])
    axis square
    hold on
    rectangle('Position',[-8,-8,16,16])
%     subplot(1,2,2)
%     J = (vfResults.patientInfo.(huxID{subjectIdxMap}).matrix{1});
%     imagesc(J);
%     axis square
%     suptitle(sprintf('%i',t))
end





 %%  %% Euc Dist From Heat Maps ***ACCURATE*** :-) %%%
   % choosing session 2 across all subjects as the most stable
   
    for ii = 1:length(subjectsAll)
        %         eucDist.all(ii) = indSessions.ses2{ii}.eucDist;
        %         eucDist.span(ii)= indSessions.ses2{ii}.span25;
%         if ii == 15 %large offset for last session from fatigue
%             eucDist.all(ii) = indSessions.ses2{ii}.eucDist;
%             eucDist.span(ii)= indSessions.ses1{ii}.span25;
%         else
            eucDist.all(ii) = fixStruct.eucDist(ii,11);
            eucDist.span(ii)= fixStruct.span25(ii,11);
            eucDist.x(ii) = fixStruct.x_pk(ii,11);
            eucDist.y(ii) = fixStruct.y_pk(ii,11);
%         end
        %         eucDist.allSes(ii) = fixStruct.eucDist(ii,11);
        %         eucDist.spanSes(ii)= fixStruct.span25(ii,11);
        if ii == 15 %Hux12 with LOTS of fatigue
            comDist.all(ii) = indSessions.ses1{ii}.com;
            comDist.stdX(ii)= indSessions.ses1{ii}.massXSTD;
            comDist.stdY(ii)= indSessions.ses1{ii}.massYSTD;
            comDist.span(ii)= mean([indSessions.ses1{ii}.massYSTD ...
                indSessions.ses1{ii}.massXSTD]);
            comDist.x(ii) = indSessions.ses1{ii}.massX;
            comDist.y(ii) = indSessions.ses1{ii}.massY;
        else
            comDist.all(ii) = fixStruct.com(ii,11);
            comDist.stdX(ii)= fixStruct.massXSTD(ii,11);
            comDist.stdY(ii)= fixStruct.massYSTD(ii,11);
            comDist.span(ii)= mean([fixStruct.massYSTD(ii,11) fixStruct.massXSTD(ii,11)]);
            comDist.x(ii) = fixStruct.massX(ii,11);
            comDist.y(ii) = fixStruct.massY(ii,11);
        end
    end
    
    [h,p,~,stats] = ttest2(eucDist.all(control == 1), eucDist.all(control == 0));
    figure;
    boxplot(eucDist.all,control)
    xticklabels({'Patient','Control'});
    ylabel('Euc. Dist (arcmin)');
    title(sprintf('p = %.2f',p));
    %% xy plots for each
    figure;
    axisSize = 15;
    counter = 1;
    xName = {eucDist.x, comDist.x};
    yName = {eucDist.y, comDist.y};
    stdName1 = {eucDist.span, comDist.stdX/2};
    stdName2 = {eucDist.span, comDist.stdY/2};
    for i = 1:2
        subplot(2,2,counter)
        x = xName{i};
        y = yName{i};
        std1 = stdName1{i};
        std2 = stdName2{i};
        counterNew = 1;
        for ii = find(control == 1) %find(params.control == 1)
            subplot (2,2,counter)
            markerType = 's';
            leg(counterNew) =  errorbar(x(ii), y(ii), ...
                std1(ii), std1(ii), std2(ii), std2(ii),...
                markerType, 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',11, ...
                'LineWidth',2);
            hold on
            counterNew = counterNew + 1;
        end
        axis([-axisSize axisSize -axisSize axisSize])
        line([0 0],[-axisSize axisSize],'Color','k')
        line([-axisSize axisSize],[0 0],'Color','k')
        axis square
        ylabel('Y')
        xlabel('X')
        legend(leg,subjectsAll{control == 1});
        % set(gcf, 'Position',  [2000, 300, 800, 600])
%         title('Prob Gaze (10% Span)');
        counter = counter + 1;
        counterNew = 1;
        subplot(2,2,counter)
        for ii = find(control == 0) %find(params.control == 1)
            markerType = 's';
            leg(counterNew) =  errorbar(x(ii), y(ii), ...
                std1(ii), std1(ii), std2(ii), std2(ii),...
                markerType, 'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',11, ...
                'LineWidth',2);
            hold on
            counterNew = counterNew + 1;
        end
        axis([-axisSize axisSize -axisSize axisSize])
        line([0 0],[-axisSize axisSize],'Color','k')
        line([-axisSize axisSize],[0 0],'Color','k')
        axis square
        ylabel('Y')
        xlabel('X')
        legend(leg,subjectsAll{control == 0});
        % set(gcf, 'Position',  [2000, 300, 800, 600])
%         title('Prob Gaze (10% Span)');
        counter = counter + 1;
%         end
    end
     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/EucDistAndSpanSes2.png');
     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/EucDistAndSpanSes2.epsc');
     
     %% Testing CAT data
     count = 0;
     for ii = 1:length(subjectsAll)
         x = fixStruct.xCat{ii};
         y = fixStruct.yCat{ii};
         distAllTrials(ii) = hypot(nanmean(x(~isinf(x))),nanmean(y(~isinf(y))));
         bceaTrials(ii) = fixStruct.bcea(ii);
         msRates (ii) = mean(msInformation{ii}.msRates);
         spanF(ii) = fixStruct.span25(ii);
         msStartPosition = [];
         msEndPosition = [];
         for i = 1:length(msInformation{ii}.startPos)
             %              sPos = msInformation{ii}.startTime{i}
             temp1 = (msInformation{ii}.endPos{i}(1,:)) > 400;
             temp2 = (msInformation{ii}.endPos{i}(2,:)) > 400;
             if sum(temp1') ~= 0 || sum(temp2') ~= 0
                 count = 1+count;
                 starts = [msInformation{ii}.startPos{i}(1,~temp1&~temp2);...
                    msInformation{ii}.startPos{i}(2,~temp1&~temp2)];
                 ends = [msInformation{ii}.endPos{i}(1,~temp1&~temp2);...
                    msInformation{ii}.endPos{i}(2,~temp1&~temp2)];
                 msStartPosition = [msStartPosition starts];
                 msEndPosition = [msEndPosition ends];
             else
                 msStartPosition = [msStartPosition msInformation{ii}.startPos{i}];
                 msEndPosition = [msEndPosition msInformation{ii}.endPos{i}];
             end
         end
         msStartPos{ii} = msStartPosition;
         msEndPos{ii} = msEndPosition;
         
         if ii == 3 || ii == 24 || ii == 7
             bceaTrialsTask(ii) = NaN;
             thresholds(ii) = NaN;
             continue;
         end
%          x2 = 2.291; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
         xT = subjectThreshUnc(ii).em.allTraces.xALL; 
         yT = subjectThreshUnc(ii).em.allTraces.yALL;
         
         offsetTaskTrials(ii) = nanmean(subjectThreshUnc(ii).em.allTraces.prlDistance);
%          temp1 = sqrt(var(xT));
%          temp2 = sqrt(var(yT));
%          bceaMatrix =  2*x2*pi * temp1 * temp2 * (1-corrcoef(xT,yT).^2).^0.5; %smaller BCEA correlates to more stable fixation
%          bceaReDone(ii) = bceaMatrix(1);
         bceaTrialsTask(ii) = get_bcea(xT,yT);
%          bceaTrialsTask(ii) = mean([subjectThreshUnc(ii).em.allTraces.bcea{:}]);
         span(ii) = mean(subjectThreshUnc(ii).em.allTraces.span);
         thresholds(ii) = subjectThreshUnc(ii).thresh;
         numTrials(ii) = sum(subjectThreshUnc(ii).em.valid);
     end
%      figure;
%      heatMapIm = generateHeatMapSimple( ...
%             fixStruct.X{ii,11}, ...
%             fixStruct.Y{ii,11}, ...
%             'Bins', 30,...
%             'StimulusSize', 0,...
%             'AxisValue', 40,...
%             'Uncrowded', 4,...
%             'Borders', 0);
%         hold on
% %         line(x,y,'Color','red','LineStyle','--')
%         line([0 0],[-40 40],'LineStyle','--','Color','k','LineWidth',3); 
%         line([-40 40],[0 0],'LineStyle','--','Color','k','LineWidth',3)
%       saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/NeedsRotateEM%s.png',subjectsAll{ii}));

        figure;
     plot((chronic == 0 | chronic == 1),distAllTrials,'o')
     xlim([-.5 1.5])
     
     dataPlotIdx = (numTrials > 80);
    
     for ii = 1:length(subjectsAll)
         x = fixStruct.xCat{ii}((~isnan(fixStruct.xCat{ii}) & ~isnan(fixStruct.yCat{ii})) & ...
             (fixStruct.xCat{ii} < 120 & ~isnan(fixStruct.yCat{ii}) < 120 &...
             fixStruct.xCat{ii} > -120 & ~isnan(fixStruct.yCat{ii}) > -120));
         y = fixStruct.yCat{ii}((~isnan(fixStruct.xCat{ii}) & ~isnan(fixStruct.yCat{ii})) & ...
             (fixStruct.xCat{ii} < 120 & ~isnan(fixStruct.yCat{ii}) < 120 &...
             fixStruct.xCat{ii} > -120 & ~isnan(fixStruct.yCat{ii}) > -120));
%          x2 =  1.13;% 2.291; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
%          bceaMatrix =  2*x2*pi * std(x) * std(y) * (1-corrcoef(x,y).^2).^0.5; %smaller BCEA correlates to more stable fixation
         bcea.fixation(ii) = get_bcea(x/60,y/60);
         
         if ii == 3 || ii == 24 || ii == 7
             continue
         end
         xT = subjectThreshUnc(ii).em.allTraces.xALL;
         yT = subjectThreshUnc(ii).em.allTraces.yALL;
%          bceaMatrix =  2*x2*pi * std(xT) * std(yT) * (1-corrcoef(xT,yT).^2).^0.5; %smaller BCEA correlates to more stable fixation
         bcea.task(ii) = get_bcea(xT/60,yT/60);
     end
     figure;
     tempIdx =(find(offsetTaskTrials ~= 0));
     boxplot(offsetTaskTrials(find(offsetTaskTrials ~= 0)),...
         control(find(offsetTaskTrials ~= 0)))
     xticklabels({'Patient','Control'})
     title('Task')
     [h,p] = ttest2(offsetTaskTrials(...
         find(offsetTaskTrials ~= 0 & control == 1)),...
         offsetTaskTrials(...
         find(offsetTaskTrials ~= 0 & control == 0)));
     
     ylabel('Euc. Dist (arcmin)');
     figure;
     subplot(1,2,1)
%      tempBCEA = abs(log(bcea.fixation));
%     tempBCEA = abs(log(40/60));

     plot(control,bcea.fixation,'o')
     hold on
     set(gca, 'YScale', 'log')
     ax = gca; % axes handle
     xlim([-.5 1.5])
     xlabel('Condition');
     %      ylim([0.01 10])
     %      yticks([0.01 0.03 0.1 0.3 1 3 10])
     ylabel('BCEA');
     xticks([0 1]);
     xticklabels({'Patient','Control'});
     title('Fixation')
          set(gca, 'YTick', [0.01 0.05 0.1 0.5 1 3 5 10 12])
          [h,pF] = ttest2(bcea.fixation(control == 0),bcea.fixation(control == 1))

     subplot(1,2,2)
%      tempBCEA = abs(log(bcea.task/60));
     plot(control,bcea.task,'o')
     hold on
     set(gca, 'YScale', 'log')
     ax = gca; % axes handle
     xlim([-.5 1.5])
     xlabel('Condition');
     %      ylim([0.01 10])
     %      yticks([0.01 0.03 0.1 0.3 1 3 10])
     ylabel('BCEA');
     xticks([0 1]);
     xticklabels({'Patient','Control'});
     title('Task')
     set(gca, 'YTick', [0.01 0.05 0.1 0.2])

     figure;
     subplot(1,2,1)
     boxplot(bcea.fixation,control);
      hold on
     set(gca, 'YScale', 'log')
     ax = gca; % axes handle
%      xlim([-.5 1.5])
     xlabel('Condition');
     ylabel('BCEA');
%      xticks([0 1]);
     xticklabels({'Patient','Control'});
     set(gca, 'YTick', [0.01 0.05 0.1 0.5 1 3 5 10 12])

     subplot(1,2,2)
     boxplot(spanF,control)
%     xlim([-.5 1.5])
     xlabel('Condition');
     ylabel('Span');
%      xticks([0 1]);
     xticklabels({'Patient','Control'}); 
    
    
     huxBCEA.Controls.taskBCEA = bceaTrialsTask(find(control));
     huxBCEA.Controls.fixationBCEA = bceaTrials(find(control));
     huxBCEA.Controls.taskBCEAAllTrials = bcea.task(find(control));
     huxBCEA.Controls.fixationBCEAAllTrials = bcea.fixation(find(control));
     
     counter = 1;
     for ii = find(control)
         huxBCEA.Controls.Subjects{counter} = (subjectsAll{ii});
         counter = counter + 1;
     end

     save('huxBCEA.mat','huxBCEA');
     
     figure;
     subplot(1,3,1)
%      tempBCEA = abs(log(bceaTrials/60));
     boxplot(bcea.task,control,'PlotStyle','compact')
     set(gca, 'YScale', 'log')
%      ax = gca; % axes handle
%      xlim([-.5 1.5])
     xlabel('Condition');
     ylim([0.02 1])
     yticks([0.01 0.03 0.1 0.3 1 2])
     xticks([1 2]);
     xticklabels({'Patient','Control'});

     [h,pT] = ttest2(bcea.task(control == 0),bcea.task(control == 1))
%      [h,p] = ttest2(bceaTrials((control == 0)),bceaTrials(control == 1));
     ylabel('BCEA (deg^2)');
     title({sprintf('p = %.3f',pT), 'Task Trials'});
     
     %%%% task trials %%%%%%
%      figure;
     subplot(1,3,3)
%      threshVals = [subjectThreshUnc(:).thresh];

     boxplot(acuitySnellen(dataPlotIdx),control(dataPlotIdx),'PlotStyle','compact')
%      xlim([-.5 1.5])
     xlabel('Condition');
     xticks([1 2]);
     xticklabels({'Patient','Control'});
     ylabel('Snellen Acuity (20/X)');
     [h,pTh] = ttest2(log(acuitySnellen(control == 1 & dataPlotIdx)),...
         log(acuitySnellen(control == 0 & dataPlotIdx)))
     title({sprintf('p = %.3f',pTh), 'Task Thresholds'});
set(gca, 'YScale', 'log')

     subplot(1,3,2)
     boxplot(span,control,'PlotStyle','compact')
%      set(gca, 'YScale', 'log')
     xlabel('Condition');
%      ylim([0.1 12])
%      yticks([0.3 1 2 10 12])
     xticks([1 2]);
     xticklabels({'Patient','Control'});
     [h,pT] = ttest2(bcea.fixation(control == 0),bcea.fixation(control == 1))
     ylabel('Span (arcmin ^2)');
     title({sprintf('p = %.3f',pT), 'Task Trials'});
%      figure;
% % %      subplot(1,2,2)
% % %      dataPlotIdx = (numTrials > 80);
% % %      plot(control(dataPlotIdx),abs(log(bceaTrialsTask(dataPlotIdx)/60)),'o')
% % %      set(gca, 'YScale', 'log')
% % %      ax = gca; % axes handle
% % % %      ax.YAxis.Exponent = 0;
% % %      xlim([-.5 1.5])
% % %      ylim([0.01 10])
% % %      yticks([0.01 0.03 0.1 0.3 1 3 10])
% % %      xlabel('Condition');
% % %      xticks([0 1]);
% % %      xticklabels({'Patient','Control'});
% % %      ylabel('BCEA (deg^2)');
% % %      temp = (log(bceaTrialsTask/60))
% % %      [h,pT] = ttest2(temp(control == 0 & (dataPlotIdx)),...
% % %          temp(control == 1 & (dataPlotIdx)))
% % %      title({sprintf('p = %.3f',pT), 'Task Trials'});
% % %      
% % %      subplot(2,2,4)
% % % %      figure;
% % %      plot(control(dataPlotIdx),span(dataPlotIdx),'o')
% % %      xlim([-.5 1.5])
% % %      xlabel('Condition');
% % %      xticks([0 1]);
% % %      xticklabels({'Patient','Control'});
% % %      ylabel('Span (arcmin)');
% % %      [h,pT] = ttest2(span(control == 1 & dataPlotIdx),...
% % %          span(control == 0 & dataPlotIdx))
% % %      title({sprintf('p = %.3f',pT), 'Task Trials'});
     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/BCEAMeasures.png');
    saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/BCEAMeasures.epsc');

    figure;
    plot(control,msRates,'o')
    xlim([-.5 1.5])
    xlabel('Condition');
    xticks([0 1]);
    xticklabels({'Patient','Control'});
    ylabel('MS Rate / sec');
    [h,pM] = ttest2(msRates(control == 1 & dataPlotIdx),...
        msRates(control == 0 & dataPlotIdx));
    title({sprintf('p = %.3f',pM), 'Fixation Trials'});
%     suptitle('Task Trials');

figure;
  mdl1 = fitlm(eucDist.all(find(control)),acuitySnellen(find(control)))
plot(mdl1)

mdl2 = fitlm(eucDist.all(find(~control)),acuitySnellen(find(~control)))
plot(mdl2)

%% Create Super Compressed and Analyzed Data (not trial by trial)
% load('msNum.mat')
dataTable = [];
counter = 1;
for ii = 1:length(subjectsAll)
dataTable(counter).ID = ii;
dataTable(counter).Stroke = ~control(ii);
dataTable(counter).Sex = sexAll(ii);
dataTable(counter).Age = ageAll(ii);
dataTable(counter).Acuity = acuitySnellen(ii);
dataTable(counter).WeeksPostLesion = weeksPostLes(ii);
% dataTable(counter).TextHemifield = ~control(ii);
if ~control(ii)
dataTable(counter).AngleOfLoss = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
end
dataTable(counter).bceaTask = bcea.task(ii);
dataTable(counter).bceaFix = bcea.fixation(ii);
dataTable(counter).fixX = fixStruct.xCat{ii};
dataTable(counter).fixY = fixStruct.yCat{ii};
dataTable(counter).fixationGroupX = fixStruct.X(ii,11);
dataTable(counter).fixationGroupY = fixStruct.Y(ii,11);
% dataTable(counter).fixationGroupX500 = fixStruct.X(ii,10);
% dataTable(counter).fixationGroupY500 = fixStruct.Y(ii,10);
dataTable(counter).eucDist = eucDist.all(ii);
if ii ~= 3
dataTable(counter).taskX = subjectThreshUnc(ii).em.allTraces.xALL;
dataTable(counter).taskY = subjectThreshUnc(ii).em.allTraces.yALL;
end
if control(ii)
    dataTable(counter).allRotated = allControl(ii);
    dataTable(counter).allRotated500 = allControl500(ii);
    dataTable(counter).allRotatedTask = allControlTask(ii);
    dataTable(counter).vfMap = [];
else
    dataTable(counter).allRotated = allRotated(ii);
    dataTable(counter).allRotated500 = allRotated500(ii);
    dataTable(counter).allRotatedTask = allRotatedTask(ii);
    dataTable(counter).vfMap = vfResults.patientInfo.(huxID{ii}).matrix{1};
end
% dataTable(counter).msInformationFixation = msNum{ii}; 
 
counter = counter + 1;
end

save('dataTableStroke.mat','dataTable')
%% For poster - BCEA and Acuity
  figure;
  isnanVals = isnan(thresholds);
     subplot(1,2,1)
     tempBCEA = abs(log(bceaTrials/60));
%      plot(control,tempBCEA,'o')
      errorbar([1 0], ...
         [mean(tempBCEA(dataPlotIdx & control)) nanmean(tempBCEA(dataPlotIdx & ~control))],...
         [sem(tempBCEA(dataPlotIdx & control)) sem(tempBCEA(dataPlotIdx & ~control & ~isnanVals))],'o');
     set(gca, 'YScale', 'log')
     ax = gca; % axes handle
     xlim([-.5 1.5])
     xlabel('Condition');
%      ylim([0.01 10])
%      yticks([0.01 0.03 0.1 0.3 1 3 10])
     ylim([1 10])
     yticks([1 2 3 6 8 10])
     xticks([0 1]);
     xticklabels({'Patient','Control'});
    [h,pBCEA] = ttest2(tempBCEA(control == 1 & dataPlotIdx),...
             tempBCEA(control == 0 & dataPlotIdx))
    title({sprintf('p = %.3f',pBCEA), 'BCEA'});    
    
  subplot(1,2,2)
%      threshVals = [subjectThreshUnc(:).thresh];
%      plot(control(dataPlotIdx),thresholds(dataPlotIdx),'o')
     errorbar([1 0], ...
         [mean(thresholds(dataPlotIdx & control)) nanmean(thresholds(dataPlotIdx & ~control))],...
         [sem(thresholds(dataPlotIdx & control)) sem(thresholds(dataPlotIdx & ~control & ~isnanVals))],'o');
     xlim([-.5 1.5])
     xlabel('Condition');
     xticks([0 1]);
     xticklabels({'Patient','Control'});
     ylabel('Thresholds');
     [h,pTh] = ttest2(thresholds(control == 1 & dataPlotIdx),...
         thresholds(control == 0 & dataPlotIdx))
     title({sprintf('p = %.3f',pTh), 'Thresholds'});
    ylim([0.05 5])
     suptitle('Task Trials')
     
  saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/BCEAThreshMeasures.png');
  saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/BCEAThreshMeasures.epsc');

    %% look at overall offset
    figure;
    subplot(1,2,1)
    errorbar(find(params.control == 0), eucDist.all(params.control == 0),...
        zeros(size(find(params.control == 0))),eucDist.span(params.control == 0), 'o');
    hold on
    errorbar(find(params.control == 1), eucDist.all(params.control == 1),...
        zeros(size(find(params.control == 1))), eucDist.span(params.control == 1), 'o');
    set(gca,'xtick',[1:length(subjectsAll)],'XTickLabels',subjectsAll)
    xlim([0 length(subjectsAll)+1])
    xtickangle(45)
    xlabel('Subject');
    ylabel('Euclidean Distance, arcmin (10% Span)')
%     title('Session 2')
title('All Trials')
 
    ylim([0 30])
    
    subplot(1,2,2)
    errorbar(find(params.control == 0), comDist.all(params.control == 0),...
        zeros(size(find(params.control == 0))),comDist.span(params.control == 0), 'o');
    hold on
    errorbar(find(params.control == 1), comDist.all(params.control == 1),...
        zeros(size(find(params.control == 1))), comDist.span(params.control == 1), 'o');
    set(gca,'xtick',[1:length(subjectsAll)],'XTickLabels',subjectsAll)
    xlim([0 length(subjectsAll)+1])
    xtickangle(45)
    xlabel('Subject');
    ylabel('COM, arcmin (Mean STD)')
    title('All Sessions')
    ylim([0 30])

    set(gcf, 'Position',  [2000, 200, 1500, 400])

    saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/EucDistAndSpanSes2.png');
    saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/EucDistAndSpanSes2.png');

    %% Euclidean Distance Ttest Chronic vs Subacute
%     eucDist.all(chronic == 1);
    [~,pChronic] = ttest2(eucDist.all(chronic == 1),eucDist.all(chronic == 2));
    [~,pSAcute] = ttest2(eucDist.all(chronic == 0),eucDist.all(chronic == 2));
    [~,pControl] = ttest2(eucDist.all(chronic == 0 | chronic == 1),eucDist.all(chronic == 2));
        
    [~,pChronicC] = ttest2(comDist.all(chronic == 1),comDist.all(chronic == 2));
    [~,pSAcuteC] = ttest2(comDist.all(chronic == 0),comDist.all(chronic == 2));
    [~,pControlC] = ttest2(comDist.all(chronic == 0 | chronic == 1),comDist.all(chronic == 2));
    
    group1 = chronic;
%     group2 = control;
    y = [eucDist.all(chronic == 0),...
        eucDist.all(chronic == 1), ...
        eucDist.all(chronic == 2)];
    [p,tbl,stats] = anovan(y,{group1});
    figure;
    boxplot(y,group1)
    c = multcompare(stats);
    % The first two columns of c show the groups that are compared. 
    %The fourth column shows the difference between the estimated 
    %group means. The third and fifth columns show the lower and 
    %upper limits for 95% confidence intervals for the true mean 
    %difference. The sixth column contains the p-value for a hypothesis 
    %test that the corresponding mean difference is equal to zero. 
    y = [comDist.all(chronic == 0),...
        comDist.all(chronic == 1), ...
        comDist.all(chronic == 2)];
    [p,tbl,stats] = anovan(y,{group1});
    figure;
    boxplot(y,group1)
    c = multcompare(stats);
    
    
    
    
    figure;
    set(gcf, 'Position',  [2000, 100, 1000, 800])
    subplot(2,2,1)
     y = [{eucDist.all(chronic == 0)} {eucDist.all(chronic == 1)} {eucDist.all(chronic == 2)}];
    temp = struct2table(cell2struct(y, {'Subacute','Chronic','Control'},2));
    violinplot(temp,{'Subacute';'Chronic';'Control'})%,...
    ylabel('Prob Dist of Gaze (arcmin)');
    ylim([0 12])

    subplot(2,2,2)
    y = [{comDist.all(chronic == 0)} {comDist.all(chronic == 1)} {comDist.all(chronic == 2)}];
    temp = struct2table(cell2struct(y, {'Subacute','Chronic','Control'},2));
    violinplot(temp,{'Subacute';'Chronic';'Control'})%,...
    ylim([0 12])
    ylabel('COM (arcmin)');

   
     subplot(2,2,3)
     y = [{eucDist.all(control == 0)} {eucDist.all(control == 1)}];
    temp = struct2table(cell2struct(y, {'Patient','Control'},2));
    violinplot(temp,{'Patient';'Control'})%,...
    ylabel('Prob Dist of Gaze (arcmin)');
    ylim([0 12])
    
    subplot(2,2,4)
    y = [{comDist.all(control == 0)} {comDist.all(control == 1)}];
    temp = struct2table(cell2struct(y, {'Patient','Control'},2));
    violinplot(temp,{'Patient';'Control'})%,...
    ylim([0 12])
    ylabel('COM of Gaze (arcmin)');
    
        set(findall(gcf,'-property','FontSize'),'FontSize',14)

     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TTESTOfEucDistAllTrials.png');
     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/ViolinPlotEucDistAllTrials.png');
     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/ViolinPlotEucDistAllTrials.epsc');

     %% Euclidean Distance: Training Sessions, Not Currently Significant
%      eucDist.controls = eucDist.all(chronic == 2);
%      figure;
%      plot(ones(1,length(eucDist.all(chronic == 1 & numTrain > 1))),...
%          eucDist.all(chronic == 1& numTrain > 1),'o',...
%          'MarkerSize',15,'LineWidth',3');
%      hold on
%      errorbar(1, mean(eucDist.all(chronic == 1 & numTrain > 1)), ...
%          sem(eucDist.all(chronic == 1& numTrain > 1)),...
%          'o','Color','k')
%      
%      plot(ones(1,length(eucDist.all(chronic == 1 & numTrain == 1))),...
%          eucDist.all(chronic == 1& numTrain == 1),'o',...
%          'MarkerSize',15,'LineWidth',3','MarkerFaceColor','g');
%      hold on
%      errorbar(1, mean(eucDist.all(chronic == 1 & numTrain == 1)), ...
%          sem(eucDist.all(chronic == 1& numTrain == 1)),...
%          'o','Color','g')
%      
%      plot(zeros(1,length(eucDist.all(chronic == 0))),eucDist.all(chronic == 0),'o',...
%          'MarkerSize',15,'LineWidth',3');
%      hold on
%      errorbar(0, mean(eucDist.all(chronic == 0)), sem(eucDist.all(chronic == 0)),...
%          'o','Color','k')
%      
%      plot(ones(1,length(eucDist.controls))*2,eucDist.controls,'o',...
%          'MarkerSize',15,'LineWidth',3);
%      errorbar(2, nanmean(eucDist.controls), sem(eucDist.controls(~isnan(eucDist.controls))),...
%          'o','Color','k')
%      for i = 1:length(chronic)
%          text(chronic(i)-.05,double(eucDist.all(i)),char(string(numTrain(i))),'FontSize',14);
%      end
%      %     title(sprintf('p = %.3f',p));
%      xticks([0 1 2])
%      xticklabels({'SubAcute','Chronic','Controls'})
%      xlim([-.5 2.5]);
%      ylabel('PRL');
%      
%      [~,pChronic] = ttest2(eucDist.all(chronic == 1 & numTrain > 1),eucDist.controls);
%      [~,pSAcute] = ttest2(eucDist.all(chronic == 0),eucDist.controls);
%      [~,pChronic_LowTrain] = ttest2(eucDist.all(chronic == 1 & numTrain == 1),eucDist.controls);
%      [~,pChronic_ALL] = ttest2(eucDist.all(chronic == 1),eucDist.controls);
%      
%      line([1 2],[27 27])
%      text(1.5,27.5,sprintf('p = %.3f',pChronic));
%      
%      line([0 2],[24 24])
%      text(1.5,24.5,sprintf('p = %.3f',pSAcute));
%      
%      line([1 2],[22 22])
%      text(1.5,22.5,sprintf('Ones p = %.3f, all p = %.3f',...
%          pChronic_LowTrain, pChronic_ALL),'Color','g');
    
   %% Task Euc Dist Calculation
   for ii = 1:length(subjectsAll)
       nXValues = [];
       nYValues = [];
       nXValuesO = [];
       nYValuesO = [];
       if ii == 3
           continue;
       end
       nXValuesO = subjectThreshUnc(ii).em.allTraces.xALL;
       nYValuesO = subjectThreshUnc(ii).em.allTraces.yALL;
        
       nXValues = nXValuesO(nXValuesO < 180 & nXValuesO > -180 &...
           nYValuesO < 180 & nYValuesO > -180);
       nYValues = nYValuesO(nXValuesO < 180 & nXValuesO > -180 &...
           nYValuesO < 180 & nYValuesO > -180);
       
       
       limit.xmin = floor(min(nXValues));
       limit.xmax = ceil(max(nXValues));
       limit.ymin = floor(min(nYValues));
       limit.ymax = ceil(max(nYValues));
       
       [result1] = MyHistogram2(nXValues, nYValues, [limit.xmin,limit.xmax,30;limit.ymin,limit.ymax,30]);
       result = result1./(max(max(result1)));
       taskComp.span25(ii) = quantile(sqrt((nXValues-nanmean(nXValues)).^2 + (nYValues-nanmean(nYValues)).^2), .10);

       x_arcmin = linspace(limit.xmin, limit.xmax, size(result, 1)); % these are the pretend arcmin corresponding to the pdf
       y_arcmin = linspace(limit.ymin, limit.ymax, size(result, 1));
       
       [~, idx] = max(result(:)); % find the max
       
       [row, col] = ind2sub(size(result), idx); % convert to row and col
       
       x_pk = x_arcmin(row); % get row and col in arcmin
       y_pk = y_arcmin(col);
       
       taskComp.x_pk(ii) = x_pk;
       taskComp.y_pk(ii) = y_pk;
       taskComp.eucDist(ii) = hypot(x_pk,y_pk);%sqrt(sum((x_pk - y_pk) .^ 2));
       
       taskComp.curvature(ii) = mean(cell2mat(...
           subjectThreshUnc(ii).em.allTraces.curvature));
%        taskComp.speed(ii) = subjectThreshUnc(ii).em.allTraces.mn_speed;
       taskComp.span(ii) = mean(subjectThreshUnc(ii).em.allTraces.span);
       taskComp.bcea(ii) = mean(cell2mat(subjectThreshUnc(ii).em.allTraces.bcea));
       
        taskComp.dsq(ii) = subjectThreshUnc(ii).em.allTraces.dCoefDsq;

       [taskComp.com(ii), ...
            taskComp.massX(ii), ...
            taskComp.massY(ii), ...
            ~,...
            ~, taskComp.massXSTD(ii), ...
            taskComp.massYSTD(ii)] ...
            = centerOfMassCalculation(nXValues, nYValues);
        
       axisVal = 20;
       figure;
       generateHeatMapSimple( ...
           nXValues, ...
           nYValues, ...
           'Bins', 30,...
           'StimulusSize', subjectThreshUnc(ii).thresh,...
           'AxisValue', axisVal,...
           'Uncrowded', 1,...
           'Borders', 1);
%                title(params.subject);
%        colorbar
       caxis([0.1 1])
       hold on
       colorbar
       plot([-axisVal axisVal], [0 0], '--k', 'LineWidth',3)
       plot([0 0], [-axisVal axisVal], '--k', 'LineWidth',3)
       set(gca,'FontSize',14)
       axis off
       if control(ii) == 1
           text(-17, 12, sprintf('C%i', str2double(regexp(subjectsAll{ii},'\d*','Match'))), ...
               'FontSize',30, 'FontWeight', 'bold');
       else
           text(-17, 12, sprintf('PT%i', str2double(regexp(subjectsAll{ii},'\d*','Match'))), ...
               'FontSize',30, 'FontWeight', 'bold');
       end
       saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/ALL/%s_taskHeatMap.png', subjectsAll{ii}));
%        saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/EucDistAndSpanSes2.png');
   end
   
   figure;
   subplot(1,3,1)
   thresholds = [subjectThreshUnc.thresh];
   [~,p,b,r] = LinRegression(taskComp.eucDist, thresholds,0,NaN,1,0);
%    axis([1940 2000 0 20])
    subplot(1,3,2)
   [~,p,b,r] = LinRegression(taskComp.eucDist(chronic == 2), thresholds(chronic == 2),0,NaN,1,0);
    subplot(1,3,3)
   [~,p,b,r] = LinRegression(taskComp.eucDist(chronic == 0 | chronic == 1), ...
       thresholds(chronic == 0 | chronic == 1),0,NaN,1,0);
   
    [~,pChronic] = ttest2(taskComp.eucDist(chronic == 1),taskComp.eucDist(chronic == 2));
    [~,pSAcute] = ttest2(taskComp.eucDist(chronic == 0),taskComp.eucDist(chronic == 2));
    [~,pControl] = ttest2(taskComp.eucDist(chronic == 0 | chronic == 1),taskComp.eucDist(chronic == 2));
        
    [~,pChronicC] = ttest2(taskComp.com(chronic == 1),taskComp.com(chronic == 2));
    [~,pSAcuteC] = ttest2(taskComp.com(chronic == 0),taskComp.com(chronic == 2));
    [~,pControlC] = ttest2(taskComp.com(chronic == 0 | chronic == 1),taskComp.com(chronic == 2));
    
    group1 = chronic;
%     group2 = control;
    y = [taskComp.eucDist(chronic == 0),...
        taskComp.eucDist(chronic == 1), ...
        taskComp.eucDist(chronic == 2)];
    [p,tbl,stats] = anovan(y,{group1});
    figure;
    boxplot(y,group1)
    c = multcompare(stats);
   %%%%
    y = [taskComp.com(chronic == 0),...
        taskComp.com(chronic == 1), ...
        taskComp.com(chronic == 2)];
    [p,tbl,stats] = anovan(y,{group1});
    figure;
    boxplot(y,group1)
    c = multcompare(stats);
    
   figure;
   subplot(2,2,1)
   y = [{taskComp.eucDist(chronic == 0)} {taskComp.eucDist(chronic == 1)} {taskComp.eucDist(chronic == 2)}];
   temp = struct2table(cell2struct(y, {'Subacute','Chronic','Control'},2));
   violinplot(temp,{'Subacute';'Chronic';'Control'})%,...
   ylabel('Prob Dist of Gaze (arcmin)');
   ylim([0 15])
   
   subplot(2,2,2)
   y = [{taskComp.com(chronic == 0)} {taskComp.com(chronic == 1)} {taskComp.com(chronic == 2)}];
   temp = struct2table(cell2struct(y, {'Subacute','Chronic','Control'},2));
   violinplot(temp,{'Subacute';'Chronic';'Control'})%,...
   ylim([0 15])
   ylabel('COM (arcmin)');
   
   subplot(2,2,3)
   y = [{taskComp.eucDist(control == 0)} {taskComp.eucDist(control == 1)}];
   temp = struct2table(cell2struct(y, {'Patient','Control'},2));
   violinplot(temp,{'Patient';'Control'})%,...
   ylabel('Prob Dist of Gaze (arcmin)');
   ylim([0 15])
   
   subplot(2,2,4)
   y = [{taskComp.eucDist(control == 0)} {taskComp.eucDist(control == 1)}];
   temp = struct2table(cell2struct(y, {'Patient','Control'},2));
   violinplot(temp,{'Patient';'Control'})%,...
   ylim([0 15])
   ylabel('COM of Gaze (arcmin)');
   set(gcf, 'Position',  [2000, 100, 1000, 800])
   set(findall(gcf,'-property','FontSize'),'FontSize',14)
   
   suptitle('Task PLF');
   
   saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TTESTOfEucDistTaskTrials.png');
   saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/ViolinPlotEucDistTaskTrials.png');
   saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/ViolinPlotEucDistTaskTrials.epsc');
 
   figure;
     y = [taskComp.bcea(chronic == 0),...
        taskComp.bcea(chronic == 1), ...
        taskComp.bcea(chronic == 2)];
    [p,tbl,stats] = anovan(y,{group1});
%     subplot(1,3,1);
    boxplot(y,group1)
    c = multcompare(stats);
    
     figure;
     y = [taskComp.span(chronic == 0),...
        taskComp.span(chronic == 1), ...
        taskComp.span(chronic == 2)];
    [p,tbl,stats] = anovan(y,{group1});
%     subplot(1,3,1);
    boxplot(y,group1)
    c = multcompare(stats);
    
     figure;
     y = [taskComp.curvature(chronic == 0),...
        taskComp.curvature(chronic == 1), ...
        taskComp.curvature(chronic == 2)];
    [p,tbl,stats] = anovan(y,{group1});
%     subplot(1,3,1);
    boxplot(y,group1)
    c = multcompare(stats);
   
     %% Task vs Fixation
     [chronic, numTrain] = huxlinChronAcuteList (subjectsAll);
     clear leg; clear taskVsfix;
     figure;
     ylabel('EucDistance');
%      xticks([1 2])
%      xticklabels({'Fixation', 'Task'})
     %      subplot(1,3,1)
     for ii = 1:length(chronic)
         if ii == 3 %couldn't do the task
             continue;
         end
         if chronic(ii) == 0
             subplot(1,3,1)
             title('Subacute')
             leg(ii) = plot([1 2], ...
                 [fixStruct.eucDist(ii,11) taskComp.eucDist(ii)],...
                 '-o','Color','r');
             hold on
             taskVsfix.subacute.fix(ii) = fixStruct.eucDist(ii,12);
             taskVsfix.subacute.task(ii) = taskComp.eucDist(ii);
             xticks([1 2])
             xticklabels({'Fixation', 'Task'})
         elseif chronic(ii) == 1
             subplot(1,3,2)
             title('Chronic')
             leg(ii) = plot([1 2], ...
                 [fixStruct.eucDist(ii,11) taskComp.eucDist(ii)],...
                 '-o','Color','b');
             taskVsfix.chronic.fix(ii) = fixStruct.eucDist(ii,12);
             taskVsfix.chronic.task(ii) = taskComp.eucDist(ii);
             xticks([1 2])
             xticklabels({'Fixation', 'Task'})
         elseif chronic(ii) == 2
             subplot(1,3,3)
             title('Control');
             leg(ii) =  plot([1 2], ...
                 [fixStruct.eucDist(ii,11) taskComp.eucDist(ii)],...
                 '-o','Color','g');
             taskVsfix.control.fix(ii) = fixStruct.eucDist(ii,12);
             taskVsfix.control.task(ii) = taskComp.eucDist(ii);
             xticks([1 2])
             xticklabels({'Fixation', 'Task'})
         end
         ylim([0 16])
         ylabel('Euc Distance Offset (arcmin)');
         xlim([.75 2.25]);
         hold on
     end
     
        taskVsfix.control.fix(taskVsfix.control.fix == 0) = [];
        taskVsfix.control.task(taskVsfix.control.task == 0) = [];
        taskVsfix.chronic.fix(taskVsfix.chronic.fix == 0) = [];
        taskVsfix.chronic.task(taskVsfix.chronic.task == 0) = [];
        taskVsfix.subacute.fix(taskVsfix.subacute.fix == 0) = [];
        taskVsfix.subacute.task(taskVsfix.subacute.task == 0) = [];
        
     [~,pControl] = ttest(taskVsfix.control.fix,taskVsfix.control.task);
     subplot(1,3,3); line([1 2],[24 24])
        text(1,24.5,sprintf('p = %.3f',...
        pControl),'Color','g');
    
     [~,pSubacute] = ttest(taskVsfix.subacute.fix,taskVsfix.subacute.task);
     subplot(1,3,1); line([1 2],[24 24])
        text(1,24.5,sprintf('p = %.3f',...
        pSubacute),'Color','r');
    
     [~,pChronic] = ttest(taskVsfix.chronic.fix,taskVsfix.chronic.task);
     subplot(1,3,2); line([1 2],[24 24])
        text(1,24.5,sprintf('p = %.3f',...
        pChronic),'Color','b');

      [~,pAll] = ttest([taskVsfix.control.fix taskVsfix.subacute.fix taskVsfix.chronic.fix],...
        [taskVsfix.control.task taskVsfix.subacute.task taskVsfix.chronic.task]);
    
     suptitle(sprintf('Euc Dist, First 500ms Task vs Fixation, p < %.2f', pAll));
     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TaskVsFixationPRL.png');
    
     %% Age vs COM
    figure;
    clear leg
    [chronic, numTrain, birthYear] = huxlinChronAcuteList (subjectsAll);
    [~,p,b,r] = LinRegression(...
        birthYear, eucDist.all,...
        0,NaN,1,0);
    hold on
    leg(1) = plot(birthYear(control == 1), eucDist.all (control == 1), 'o', 'MarkerFaceColor','g');
    leg(2) = plot(birthYear(chronic == 1), eucDist.all (chronic == 1), 'o', 'MarkerFaceColor','r');
    leg(3) = plot(birthYear(chronic == 0), eucDist.all (chronic == 0), 'o', 'MarkerFaceColor','b');

    axis([1940 2000 0 20])
    % mdl1 = fitlm(prl.all(control==1), thresholds(control==1));
    % plot(mdl1)
    ylabel('eucDist trial Level');
    xlabel('Age');
    legend(leg,{'Controls','Chronic','SubAcute'});
    saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/AgeAndCOMTrial.png');

%     figure;
%     clear leg
%     [chronic, numTrain, birthYear] = huxlinChronAcuteList (subjectsAll);
%     [~,p,b,r] = LinRegression(...
%         birthYear, comCAT.all,...
%         0,NaN,1,0);
%     hold on
%     leg(1) = plot(birthYear(control == 1), comCAT.all (control == 1), 'o', 'MarkerFaceColor','g');
%     leg(2) = plot(birthYear(chronic == 1), comCAT.all (chronic == 1), 'o', 'MarkerFaceColor','r');
%     leg(3) = plot(birthYear(chronic == 0), comCAT.all (chronic == 0), 'o', 'MarkerFaceColor','b');
%     axis([1940 2000 0 20])
%     ylabel('COM CAT Level');
%     xlabel('Age');
%     legend(leg,{'Controls','Chronic','SubAcute'});
%     
%     
%     % mdl1 = fitlm(prl.all(control==1), thresholds(control==1));
%     % plot(mdl1)
% %     ylabel('COM trial Level');
%     xlabel('Age');
%     legend(leg,{'Controls','Chronic','SubAcute'});
%     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/AgeAndCOMCAT.png');

     
    
    figure;
    subplot(3,1,1)
    [~,p,b,r] = LinRegression(birthYear(control == 1), eucDist.all(control == 1),0,NaN,1,0);
    axis([1940 2000 0 20])
    subplot(3,1,2)
    [~,p,b,r] = LinRegression(birthYear(chronic == 1), eucDist.all(chronic == 1),0,NaN,1,0);
    axis([1940 2000 0 20]) 
    subplot(3,1,3)
    [~,p,b,r] = LinRegression(birthYear(chronic == 0), eucDist.all(chronic == 0),0,NaN,1,0);
     axis([1940 2000 0 20])
    %% Mean Angle - doesn't make sense to analyze this currently
    % [~,p] = ttest2(abs(ang.patients),abs(ang.controls));
    % figure;
    % plot(zeros(1,length(ang.patients)),abs(ang.patients),'o',...
    %     'MarkerSize',10,'LineWidth',3');
    % hold on
    % plot(ones(1,length(ang.controls)),abs(ang.controls),'o',...
    %     'MarkerSize',10,'LineWidth',3);
    % title(sprintf('p = %.2f',p));
    % xticks([0 1])
    % xticklabels({'Patients','Controls'})
    % xlim([-.5 1.5]);
    % saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TTESTOfPRL.png');
    %% Rotate Traces
%     concatAllRotatedY = [];
for ii = find(control == 0)
    rotateAngle = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
    if ii == 1
        rotateAngle = 315;
    elseif ii == 2
        rotateAngle = 50;
    elseif ii == 3
        rotateAngle = 145;
    elseif ii == 4
        rotateAngle = 90;
    elseif ii == 10
        rotateAngle = 90;
    elseif ii == 22
        rotateAngle = 315;
    elseif ii == 23
        rotateAngle = 90;
    end
    
    theta = -rotateAngle;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    
    v = [fixStruct.X{ii,11};fixStruct.Y{ii,11}]';
    allRotated{ii} = v*R;
    
    v500 = [fixStruct.X{ii,10};fixStruct.Y{ii,10}]'; 
    allRotated500{ii} = v500*R;
    
%     vMStart = [msStartPos{ii}(1,:);msStartPos{ii}(2,:)]';
%     msStartRotated{ii} = vMStart*R;
%     
%     vMEnd = [msEndPos{ii}(1,:);msEndPos{ii}(2,:)]';
%     msEndRotated{ii} = vMEnd*R;
    
    if strcmp(subjectsAll{ii},'HUX5')
        allRotatedTask{ii} = NaN;
        continue
    else
        vTask = [subjectThreshUnc(ii).em.allTraces.xALL;...
            subjectThreshUnc(ii).em.allTraces.yALL]';
        allRotatedTask{ii} = vTask*R;
    end
    
end

for ii = find(control == 1)
    allControl{ii} = [fixStruct.X{ii,11};fixStruct.Y{ii,11}]';
    allControlTask{ii} = [subjectThreshUnc(ii).em.allTraces.xALL;...
        subjectThreshUnc(ii).em.allTraces.yALL]';
    allControl500{ii} = [fixStruct.X{ii,10};fixStruct.Y{ii,10}]';
%     msControlRotatedStart{ii} = [msStartPos{ii}(1,:);msStartPos{ii}(2,:)]';
%     msControlRotatedEnd{ii} = [msEndPos{ii}(1,:);msEndPos{ii}(2,:)]';

end

%% Plot Historgram of Rotated X and Y


[pY_FullFixation, meanValsCL, meanValsPT, meansValsIndY] = histogramNormalizedForRotation(subjectsAll, control, ...
    allRotated, allControl, 2, 0); %x = 1 or y = 2
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/AllYHistogram.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/AllYHistogram.epsc');
[pX_FullFixation, meanValsCLX, meanValsPTX,meansValsIndX] = histogramNormalizedForRotation(subjectsAll, control, ...
    allRotated, allControl, 1, 0); %x = 1 or y = 2
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/AllXHistogram.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/AllXHistogram.epsc');

[pY_500Fixation, meanValsCL500Y, meanValsPT500Y,] = histogramNormalizedForRotation(subjectsAll, control, ...
    allRotated500, allControl500, 2, 0); %x = 1 or y = 2
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/500YHistogram.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/500YHistogram.epsc');

[pX_500Fixation, meanValsCL500X, meanValsPT500X] = histogramNormalizedForRotation(subjectsAll, control, ...
    allRotated500, allControl500, 1, 0); %x = 1 or y = 2
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/500XHistogram.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/500XHistogram.epsc');

[pY_Task, meanValsCLT, meanValsPTT, temp] = histogramNormalizedForRotation(subjectsAll, control, ...
    allRotatedTask, allControlTask, 2, 1); %x = 1 or y = 2
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TaskYHistogram.epsc');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TaskYHistogram.png');

[pX_Task, meanValsCLTX, meanValsPTTX] = histogramNormalizedForRotation(subjectsAll, control, ...
    allRotatedTask, allControlTask, 1, 1); %x = 1 or y = 2
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TaskXHistogram.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TaskXHistogram.epsc');

% prl.fixation.x = meansValsIndX;
% prl.fixation.y = meansValsIndY;



figure;
subplot(3,2,1)
    errorbar([0 1],[meanValsCL.mean meanValsPT.mean],[meanValsCL.sem meanValsPT.sem],...
        '-o');
    xlim([-.2 1.2])
    ylim([-5 5])
    title('Y')
    ylabel('Full Fixation')
subplot(3,2,2)
    errorbar([0 1],[meanValsCLX.mean meanValsPTX.mean],[meanValsCLX.sem meanValsPTX.sem],...
        '-o');
    xlim([-.2 1.2])
    ylim([-5 5])
    title('X')

subplot(3,2,3)
    errorbar([0 1],[meanValsCL500Y.mean meanValsPT500Y.mean],[meanValsCL500Y.sem meanValsPT500Y.sem],...
        '-o');
    xlim([-.2 1.2])
    ylim([-5 5])
    title('Y');
    ylabel('500ms Fixation')
subplot(3,2,4)
    errorbar([0 1],[meanValsCL500X.mean meanValsCL500X.mean],[meanValsCL500X.sem meanValsCL500X.sem],...
        '-o');
    xlim([-.2 1.2])
    ylim([-5 5])
    title('X')
    
subplot(3,2,5)
    errorbar([0 1],[meanValsCLT.mean meanValsCLT.mean],[meanValsCLT.sem meanValsCLT.sem],...
        '-o');
    xlim([-.2 1.2])
    ylim([-5 5])
    title('Y');
    ylabel('500ms Task')
subplot(3,2,6)
    errorbar([0 1],[meanValsCLTX.mean meanValsCLTX.mean],[meanValsCLTX.sem meanValsCLTX.sem],...
        '-o');
    xlim([-.2 1.2])
    ylim([-5 5])
    title('X')
    
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HistScatter.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HistScatter.epsc');

%% MS analysis
figure;
subplot(2,3,1)
for ii = find(control == 0)
    leg(1) = plot(msStartRotated{ii}(:,1),msStartRotated{ii}(:,2),'.')
    hold on
    leg(2) = plot(msEndRotated{ii}(:,1),msEndRotated{ii}(:,2),'*')
end
legend(leg,{'Start','End'});
axis([-60 60 -60 60])
axis square
title({'Patients','Start and End MS Positions'})

subplot(2,3,2)
for ii = find(control == 1)
    plot(msControlRotatedStart{ii}(:,1),msControlRotatedStart{ii}(:,2),'.')
    hold on
    plot(msControlRotatedEnd{ii}(:,1),msControlRotatedEnd{ii}(:,2),'*')
end
axis([-60 60 -60 60])
axis square
title({'Controls','Start and End MS Positions'})

subplot(2,3,3)
tempX = []; tempY = [];
for ii = find(control == 0)
    plot(msEndRotated{ii}(:,1)-msStartRotated{ii}(:,1),...
        msEndRotated{ii}(:,2)-msStartRotated{ii}(:,2),'.')
    hold on
    tempX = [tempX msEndRotated{ii}(:,1)'-msStartRotated{ii}(:,1)'];
    tempY = [tempY msEndRotated{ii}(:,2)'-msStartRotated{ii}(:,2)'];
    stdPTY(ii) = std(msEndRotated{ii}(:,2)'-msStartRotated{ii}(:,2)');
%     plot(msEndRotated{ii}(:,1),msEndRotated{ii}(:,2),'*')
end
% figure;
axis([-60 60 -60 60])
PTtempX = (tempX);
PTtempY = (tempY);
groupPT = zeros(1,length(PTtempY));
axis square
title({'Patients','Adjusted End MS Positions'})

subplot(2,3,4)
tempX = []; tempY = [];
for ii = find(control == 1)
    plot(msControlRotatedEnd{ii}(:,1)-msControlRotatedStart{ii}(:,1),...
        msControlRotatedEnd{ii}(:,2)-msControlRotatedStart{ii}(:,2),'.')
    hold on
    tempX = [tempX msControlRotatedEnd{ii}(:,1)'-msControlRotatedStart{ii}(:,1)'];
    tempY = [tempY msControlRotatedEnd{ii}(:,2)'-msControlRotatedStart{ii}(:,2)'];
    stdCTY(ii) = std(msControlRotatedEnd{ii}(:,2)'-msControlRotatedStart{ii}(:,2)');
end
CTtempX = (tempX);
CTtempY = (tempY);
groupCT = ones(1,length(CTtempY));
% groupCT(groupCT == 1) = ['control'];
axis([-60 60 -60 60])
axis square
title({'Controls','Adjusted End MS Positions'})

subplot(2,3,5:6)
for ii = find(control == 0)
    plf{ii} = [meansValsIndX(ii) meansValsIndY(ii)];
    offset(ii) = hypot(plf{ii}(1), plf{ii}(2)) ;
    startMS{ii} = [msStartRotated{ii}(:,1)';msStartRotated{ii}(:,2)'];
    endMS{ii} = [msEndRotated{ii}(:,1)';msEndRotated{ii}(:,2)'];
    for i = 1:length(startMS{ii})
        d1(i) = pdist([startMS{ii}(1,i),startMS{ii}(2,i); plf{ii}(1), plf{ii}(2)],...
            'euclidean');
        d2(i) = pdist([endMS{ii}(1,i),endMS{ii}(2,i); plf{ii}(1), plf{ii}(2)],...
            'euclidean');
        d3(i) = pdist([endMS{ii}(1,i),endMS{ii}(2,i); 0, 0],...
            'euclidean');
        d4(i) = pdist([startMS{ii}(1,i),startMS{ii}(2,i); 0, 0],...
            'euclidean');
    end
    averageDist(ii,:) = [mode(d1) mode(d2) mode(d3) mode(d4)];
end

for ii = find(control == 1)
    plf{ii} = [meansValsIndX(ii) meansValsIndY(ii)];
    offset(ii) = hypot(plf{ii}(1), plf{ii}(2)) ;
    startMS{ii} = [msControlRotatedStart{ii}(:,1)';msControlRotatedStart{ii}(:,2)'];
    endMS{ii} = [msControlRotatedEnd{ii}(:,1)';msControlRotatedEnd{ii}(:,2)'];
    for i = 1:length(startMS{ii})
        d1(i) = pdist([startMS{ii}(1,i),startMS{ii}(2,i); plf{ii}(1), plf{ii}(2)],...
            'euclidean');
        d2(i) = pdist([endMS{ii}(1,i),endMS{ii}(2,i); plf{ii}(1), plf{ii}(2)],...
            'euclidean');
        d3(i) = pdist([endMS{ii}(1,i),endMS{ii}(2,i); 0, 0],...
            'euclidean');
        d4(i) = pdist([startMS{ii}(1,i),startMS{ii}(2,i); 0, 0],...
            'euclidean');
    end
    averageDist(ii,:) = [mode(d1) mode(d2) mode(d3) mode(d4)];
end

% figure;
for ii = find(control == 0)
    leg(1) = plot([1 2],averageDist(ii,1:2),'-o','Color','r');
    hold on
    plot([3 4],averageDist(ii,3:4),'-o','Color','r');
end
for ii = find(control == 1)
    leg(2) = plot([1 2],averageDist(ii,1:2),'-o','Color','b');
    hold on
    plot([3 4],averageDist(ii,3:4),'-o','Color','b');
end
hold on
errorbar([1 2],[mean(averageDist(control == 0,1)) mean(averageDist(control == 0,2))],...
    [sem(averageDist(control == 0,1)) sem(averageDist(control == 0,2))],...
    '-s','MarkerSize',10,'Color','r','LineWidth',5,...
    'MarkerEdgeColor','r','MarkerFaceColor','r')
errorbar([1 2],[mean(averageDist(control == 1,1)) mean(averageDist(control == 1,2))],...
    [sem(averageDist(control == 1,1)) sem(averageDist(control == 1,2))],...
    '-s','MarkerSize',10,'Color','b','LineWidth',5,...
    'MarkerEdgeColor','b','MarkerFaceColor','b')
errorbar([3 4],[mean(averageDist(control == 0,3)) mean(averageDist(control == 0,4))],...
    [sem(averageDist(control == 0,3)) sem(averageDist(control == 0,4))],...
    '-s','MarkerSize',10,'Color','r','LineWidth',5,...
    'MarkerEdgeColor','r','MarkerFaceColor','r')
errorbar([3 4],[mean(averageDist(control == 1,3)) mean(averageDist(control == 1,4))],...
    [sem(averageDist(control == 1,3)) sem(averageDist(control == 1,4))],...
    '-s','MarkerSize',10,'Color','b','LineWidth',5,...
    'MarkerEdgeColor','b','MarkerFaceColor','b')
hold on
errorbar([2 3],[mean(averageDist(control == 1,2)) mean(averageDist(control == 1,3))],...
    [sem(averageDist(control == 1,2)) sem(averageDist(control == 1,3))],...
    '-s','MarkerSize',10,'Color','b','LineWidth',5,...
    'MarkerEdgeColor','b','MarkerFaceColor','b')
errorbar([2 3],[mean(averageDist(control == 0,2)) mean(averageDist(control == 0,3))],...
    [sem(averageDist(control == 0,2)) sem(averageDist(control == 0,3))],...
    '-s','MarkerSize',10,'Color','r','LineWidth',5,...
    'MarkerEdgeColor','r','MarkerFaceColor','r')

axis([.5 4.5 0 6])
xticks([1 2 3 4]);
xticklabels({'StartMS - PLF','EndMS - PLF',...
    'EndMS - 0,0','StartMS - 0,0'});
ylabel('Mode of Euclidean Distances (arcmin)');
legend(leg,{'Patient','Control'});
% [h,p1] = ttest((averageDist(control == 0,1)), (averageDist(control == 0,2)))
% [h,p2] = ttest((averageDist(control == 1,1)), (averageDist(control == 1,2)))
% [h,p3] = ttest((averageDist(control == 0,3)), (averageDist(control == 0,4)))
% [h,p4] = ttest((averageDist(control == 1,3)), (averageDist(control == 1,4)))
[h,p5] = ttest((averageDist(control == 1,2)), (averageDist(control == 1,3)))
[h,p6] = ttest((averageDist(control == 0,2)), (averageDist(control == 0,3)))

title('Using Alternate PLF?')

% figure;
% h2 = scatterhist([CTtempX PTtempX],[CTtempY PTtempY] ,'Group',[groupCT groupPT],'Kernel','on',...
%     'Location','SouthWest','Direction','out','Marker','.');
% axis([-60 60 -60 60])
% axis square
% [h,p] = ttest2(stdPTY(control == 0), stdCTY(control == 1));
% title({'Rotated Centered MS Landing Positions', sprintf('STD diff Y, p = %.3f',p)})

    %% Plot Offset with VF Loss Simple but Rotated SCATTERHIST and/or ellipse heat map
    figure;
    subplot(1,2,1)
    patientsAllX = [];
   patientsAllY = [];

    for ii = find(control == 0)  
         rotateAngle = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
        if ii == 1
            rotateAngle = 315;
        elseif ii == 2
            rotateAngle = 50;
        elseif ii == 3
            rotateAngle = 145;
        elseif ii == 4
            rotateAngle = 90;
        elseif ii == 10
            rotateAngle = 90;
        elseif ii == 22
            rotateAngle = 315;
        elseif ii == 23
            rotateAngle = 90;
        end
        rotateAngle = 0;
        
        v = [fixStruct.X{ii,11};fixStruct.Y{ii,11}]';
        theta = -rotateAngle;
        R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
        vR = v*R;
 
        patientsAllY = [patientsAllY vR(:,2)'];
        patientsAllX = [patientsAllX vR(:,1)'];
        axisVal = 20;
        hold on
        ellipseXY(vR(:,1)', vR(:,2)', 68, [150 150 150]/255,0)
        axis([-axisVal axisVal -axisVal axisVal])

        hold on
        plot(0,0,'o','MarkerSize',1,'LineWidth',3,'MarkerEdgeColor','k');
        line([0 0],[-axisVal axisVal],'Color','k','LineWidth',1,'LineStyle',':')
        line([-axisVal axisVal],[0 0],'Color','k','LineWidth',1,'LineStyle',':')
       
        axis square;
        
        set(gca,'XTick',[],'YTick',[],'xlabel',[],'ylabel',[])
        title(sprintf('%s',subjectsAll{ii}));
        
    end
   title('Patients') 
   ellipseXY(patientsAllX, patientsAllY, 65, 'm' ,0)
   plot(mean(patientsAllX),mean(patientsAllY),'d','Color','m')

   
    saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/EMandHisEllipse%s.png'...
            ,subjectsAll{ii}));
        saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/EMandHisEllipse%s.epsc'...
            ,subjectsAll{ii}));
    
    %% Plot VF 30-2+10-2 with Matching Rotations
counter = 1;
figure;
for ii = find(control == 0)
       rotateAngle = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
        if ii == 1
            rotateAngle = 315;
        elseif ii == 2
            rotateAngle = 50;
        elseif ii == 3
            rotateAngle = 145;
        elseif ii == 4
            rotateAngle = 90;
        elseif ii == 10
            rotateAngle = 90;
        elseif ii == 22
            rotateAngle = 315;
        elseif ii == 23
            rotateAngle = 90;
        end
   
    
    subplot(4,4,counter)
    temp = (vfResults.patientInfo.(huxID{ii}).matrix{1});
    temp(isnan(temp)) = -20;
    

%     J = imrotate((temp),rotateAngle);
    J = imrotate((temp),0); %Create figures with just general
    J(find(J == -20)) = max(max(J));

    imagesc(J);
    axis square;
    set(gca,'XTick',[],'YTick',[])
%     J = customcolormap([0 0.9 1],{'#ffffff','#ff0000','#000000'});%'#808080'
%     colormap(J);
%     caxis([-15 28])
    colormap((gray))
%     colorbar
%     ylabel(sprintf('Rotated %i',rotateAngle))
if ii == 1
    ylabel('Degrees')
    xlabel('Degrees')
    yticks([1 420])
    xticks([1 540])
    xticklabels({'-30','30'})
    yticklabels({'-30','30'})
else
    %     axis off
    % ax = axes;
    % ax.YAxis.Visible = 'off';
    % ax.XAxis.Visible = 'off';
    yticks([1 420])
    xticks([1 540])
    xticklabels({'',''})
    yticklabels({'',''})
end

    title(sprintf('P%s',cell2mat(regexp(subjectsAll{ii},'\d*','Match'))));

counter = counter + 1;
%     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/VFRotated%s.png'...
%         ,subjectsAll{ii}));
    
%     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/VF%i.png'...
%         ,ii));
    
end
    
    %% Comparing with VF loss
    for ii = find(control == 0)
        if ~isempty(huxID{ii})
            clear leg
            figure('units','normalized','outerposition',[0 0 .9 .6]);
            idx = find(vfResults.patientInfo.(huxID{ii}).deficit);
            
            edge1Idx = min(idx);
            edge2Idx = max(idx);
            
            temp = (vfResults.patientInfo.(huxID{ii}).angles);
            reverseAngles = length(vfResults.patientInfo.(huxID{ii}).angles)-1;
            
            angleEM = round(atan2d(fixStruct.massY(ii,11),fixStruct.massX(ii,11)),-1);
            if angleEM < 0
                angleEM = angleEM + 360;
            end
            [angleVF1, angleVF2] = anglesOfLossHuxlin(ii); %hard coded angle of loss from VF fields
            
            clear yEquation
            clear xEquation
            for i = 1:2
                if i == 1
                    angle = angleVF1;
                elseif i ==2
                    angle = angleVF2;
                end
                if angle > 0 && angle < 90
                    quad = 1;
                elseif angle > 0 && angle > 90
                    quad = 2;
                elseif angle < 0 && angle < -90
                    quad = 3;
                else
                    quad = 4;
                end
                slope(i) = cosd(angle); %y/x
                if angle < 0
                    slope(i) = -slope(i);
                end
                %             slope2 = cos(angleVF2);
                m  = slope(i);  % Specify your slope
                x1 = 0; % Specify your starting x
                y1 = 0;  % Specify your starting y
                yEquation(i,:) = -500:0.25:500;
                %             Calculate the corresponding y-values:
                xEquation(i,:) = m*(yEquation(i,:) - x1) + y1;
                
                switch quad
                    case 1
                        xEquation(i,xEquation(i,:)<0) = 0;
                        yEquation(i,yEquation(i,:)<0) = 0;
                        
                    case 2
                        xEquation(i,xEquation(i,:)>0) = 0;
                        yEquation(i,yEquation(i,:)<0) = 0;
                    case 3
                        xEquation(i,xEquation(i,:)>0) = 0;
                        yEquation(i,yEquation(i,:)>0) = 0;
                    case 4
                        xEquation(i,xEquation(i,:)<0) = 0;
                        yEquation(i,yEquation(i,:)>0) = 0;
                end
                if ii == 3 && i == 1
                    yEquation(i,xEquation(i,:)==0) = 0;
                end
                quadSave(i) = quad;
            end
            
            
            xq = round(fixStruct.X{ii,11});
            yq = round(fixStruct.Y{ii,11});
            
            
            smallX = extreme(xEquation(1,:));
            largeX = extreme(xEquation(2,:));
            smallY = extreme(yEquation(1,:));
            largeY = extreme(yEquation(2,:));
            subplot(1,3,1)

            if ii == 4
                r = [0, -500, 500, 1000];
                X = [r(1), r(1)+r(3), r(1)+r(3), r(1), r(1)];
                Y = [r(2), r(2), r(2)+r(4), r(2)+r(4), r(2)];
                plot(X, Y)
                axis([-62 62 -62 62])
                axis square
                [in,on] = inpolygon(xq,yq,...
                    X',...
                    Y');
                smallX = 0;
                largeX = 0;
                largeY = -500;
                smallY = 500;
            elseif  ii == 11
                r = [-500, -500, 500, 1000];
                X = [r(1), r(1)+r(3), r(1)+r(3), r(1), r(1)];
                Y = [r(2), r(2), r(2)+r(4), r(2)+r(4), r(2)];
                plot(X, Y)
                axis([-62 62 -62 62])
                axis square
                [in,on] = inpolygon(xq,yq,...
                    X',...
                    Y');
                smallX = 0;
                largeX = 0;
                largeY = -500;
                smallY = 500;
                allX = X;
                allY = Y;
            else
                if ii == 2 || ii == 6
                    completeTriangleX = round(linspace(round(min(smallX,largeX)),...
                        round(max(smallX,largeX)),...
                        length(xEquation)));
                    
                    completeTriangleY = round(linspace(500,...
                        -500,...
                        length(xEquation)));
                    smallY = 500;
                    largeY = -500;
                elseif ii == 8 || ii == 9
                    completeTriangleX = (linspace(min(xEquation(1,:)),0,...
                        length(xEquation)));
                    
                    completeTriangleY = (linspace(2000,...
                        -2000,...
                        length(xEquation)));
                    smallX = min(xEquation(1,:));
                    largeX = 0;
                    smallY = 2000;
                    largeY = -2000;
                else
                    completeTriangleX = round(linspace(round(min(smallX,largeX)),...
                        round(max(smallX,largeX)),...
                        length(xEquation)));
                    
                    completeTriangleY = round(linspace(round(min(smallY,largeY)),...
                        round(max(smallY,largeY)),...
                        length(xEquation)));
                end
%                 figure;
                plot(xEquation(1,:),yEquation(1,:));
                hold on;plot(xEquation(2,:),yEquation(2,:));
                axis([-62 62 -62 62])
                hold on
                plot(completeTriangleX, completeTriangleY);
                
                [allX, idx] = sort([xEquation(1,:) xEquation(2,:) completeTriangleX]);
                temp = [yEquation(1,:) yEquation(2,:) completeTriangleY];
                allY = temp(idx);
                
                [in,on] = inpolygon(xq,yq,...
                    round(allX'),...
                    round(allY'));
            end
                
            % figure;
            hold on
            plot(xq(~in),yq(~in),'bo') % points outside
            plot(xq(in),yq(in),'r+') % points inside
            hold off
            axis square
%                             axis([-500 500 -500 500])

% figure;
%  line([0, smallX],[0,smallY]); hold on
%  line([0, largeX],[0, largeY]);

            clear val
            for q = 1:length(xq)
                if in(q)
                    val(q) = -1;
                else
                    if ii == 10 || ii == 3 || ii == 5
                        v1 = [0,smallX];
                        v2 = [0,smallY];
                        v3 = [0,largeX];
                        v4 = [0, largeY];
                    elseif ii == 2
                         v1 = [0, smallX];
                         v2 = [0,largeY];
                         v3 = [0, largeX];
                         v4 = [0, smallY];
                    else
                        v1 = [smallX,0];
                        v2 = [0,smallY];
                        v3 = [largeX,0];
                        v4 = [0, largeY];
                    end
                    pt = [xq(q),yq(q)];
                    val(q) = min([point_to_line_distance(pt, v1, v2)...
                        point_to_line_distance(pt, v3, v4)]);
                end
            end
            distances.patient{ii,1} = val;
            clear in; clear val;
            for numC = find(control == 1) %check every control base don this blind field
                clear val
                xqC = round(fixStruct.X{numC,11});
                yqC = round(fixStruct.Y{numC,11});
                clear in; clear on;
                [in,on] = inpolygon(xqC,yqC,...
                    round(allX'),...
                    round(allY'));
                figure;
                plot(xqC(~in),yqC(~in),'bo') % points outside
                plot(xqC(in),yqC(in),'r+') % points inside
                
                for q = 1:length(xqC)
                    if in(q)
                        val(q) = -1;
                    else
                        if ii == 10 || ii == 3 || ii == 5
                            v1 = [0,smallX];
                            v2 = [0,smallY];
                            v3 = [0,largeX];
                            v4 = [0, largeY];
                        elseif ii == 2
                            v1 = [0, smallX];
                            v2 = [0,largeY];
                            v3 = [0, largeX];
                            v4 = [0, smallY];
                        else
                            v1 = [smallX,0];
                            v2 = [0,smallY];
                            v3 = [largeX,0];
                            v4 = [0, largeY];
                        end
                        pt = [xqC(q),yqC(q)];
                        val(q) = min([point_to_line_distance(pt, v1, v2)...
                            point_to_line_distance(pt, v3, v4)]);
                    end
                end
                distances.control{ii,numC} = val;
            end
%    
%              checking(ii).edgeVF1 = angleVF1;
%              checking(ii).edgeVF2 = angleVF2;
%              checking(ii).angleEM = angleEM;
%              checking(ii).sizeOffset = fixStruct.eucDist(ii,11);
%              checking(ii).control = 0;

             
             if isAngBetween(deg2rad(angleEM),deg2rad(angleVF1), deg2rad(angleVF2)) 
                 inBoundry(ii) = 1;
             else
                 inBoundry(ii) = 0;
             end

            clear x
            clear y
            lineLength = 2;
            angle = double(angleEM);
            x(1) = 0;
            y(1) = 0;
            x(2) = x(1) + lineLength * cosd(angle);
            y(2) = y(1) + lineLength * sind(angle);
            hold on; % Don't blow away the image.
            plot(x, y);
            
          
         subplot(1,3,2)          
            generateHeatMapSimple( ...
                fixStruct.X{ii,11}, ...
                fixStruct.Y{ii,11}, ...
                'Bins', 30,...
                'StimulusSize', 0,...
                'AxisValue', 60,...
                'Uncrowded', 4,...
                'Borders', 1);
            hold on
%             plot(fixStruct.x_pk(ii,11), fixStruct.y_pk(ii,11), ...
%                 'o', 'Color', 'k', 'LineWidth',2);
           title('All Sessions')
         
        ax(1) = subplot(1,3,3);
             imagesc(vfResults.patientInfo.(huxID{ii}).matrix{1});
            axis square;
            [height,width] = size(vfResults.patientInfo.(huxID{ii}).matrix{1});
            lineLength = 270;
            x(1) = width/2;
            y(1) = height/2;
            x(2) = x(1) + lineLength * cosd(angle);
            y(2) = y(1) - lineLength * sind(angle); %image y axis is backwards
            hold on;
             plot(x,y,'-','LineWidth',8,'Color','r');
            colormap(ax(1), bone)
            
            saveas(gcf,sprintf('../../SummaryDocuments/HuxlinPTFigures/%sVFFieldMapsWithEM.png',subjectsAll{ii}));
            saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sVFFieldMapsWithEM.png',subjectsAll{ii}));
        else
%         checking(ii).edgeVF1 = NaN;
%         checking(ii).edgeVF2 = NaN;
%         checking(ii).angleEM =  round(atan2d(fixStruct.massY(ii,11),...
%             fixStruct.massX(ii,11)),-1);
%         checking(ii).sizeOffset = fixStruct.eucDist(ii,11);
%         checking(ii).control = 1;

        end
    end
   writetable(struct2table(checking),'Clark_AnglesData.csv')
    save('Clark_AnglesData.csv','checking')
    
    %%%check t test distributions
    for ii = find(control == 0)
        [h,p,ci,stats] =  ttest2(mean(distances.patient{ii}),...
            [mean(distances.control{ii,12})...;%...
            mean(distances.control{ii,13}) ...
            mean(distances.control{ii,14}) ...
            mean(distances.control{ii,15}) ...
            mean(distances.control{ii,16}) ...
            mean(distances.control{ii,17}) ...
            mean(distances.control{ii,18}) ...
            mean(distances.control{ii,19}) ...
            mean(distances.control{ii,20})]);
    end
% end
%% looking at ttest distribution for weighted em
% save('HUXFunctions/distancesAll','distances');
load('HUXFunctions/distancesAll.mat');
for ii = find(control == 0)
    distances.av_pt(ii) = nanmean(distances.patient{ii});
    distances.prop_pt(ii) = length(find(distances.patient{ii} == -1))/...
        length(distances.patient{ii}) * 100;
    counter = 1;
    for i = find(control == 1)
        distances.av_c{ii}(counter) = nanmean(distances.control{ii,i});
        val1 = length(find(distances.control{ii,i} == -1));
        val2 = length(distances.control{ii,i});
        distances.prop_c{ii}(counter) = (val1/val2)*100;
        counter = counter + 1;
    end
end
figure;
counter = 1;
for ii = find(control == 0)
    subplot(3,4,counter)
    %     y1 = tpdf(x,9);
    group1 = [1 0 0 0 0 0 0 0 0 0];
    distances.av_c_all(ii) = mean(distances.av_c{ii});
    distances.prop_c_all(ii) = mean(distances.prop_c{ii});

    y = [distances.av_pt(ii),...
        distances.av_c{ii}];
    %     [p(ii),tbl,stats] = anovan(y,{group1});
    %     figure;
    boxplot(y,group1)
    %     c = multcompare(stats);
    title(subjectsAll{ii});
    tempNorm = normalizeVector([distances.av_pt(ii),...
        distances.av_c{ii}])
    distances.norm_pt(ii) = tempNorm(1);
    distances.norm_c(ii,:) = tempNorm(2:end);
    counter = counter + 1;
    %     [h,p,ci,stats] =  ttest2( );
end
[h,pAv,tbl,stats] = ttest2(distances.av_pt, distances.av_c_all);

figure;
subplot(1,2,1)
%      y = [{distances.norm_pt} {mean(distances.norm_c)}];
y = [{distances.av_pt} {distances.av_c_all}];
temp = struct2table(cell2struct(y, {'Patient','Control'},2));
violinplot(temp,{'Patient';'Control'})%,...
ylabel('Distance of Gaze from Boundary');
%     ylim([0 12])
% group2 = [ones(1,11) zeros(1,11))
% [p(ii),tbl,stats] = anovan([distances.av_pt,distances.av_c_all],control);

% end
% save('fixationStructureHuxlin.mat', 'fixStruct');
set(gca,'FontSize',15)

subplot(1,2,2)
%      y = [{distances.norm_pt} {mean(distances.norm_c)}];
y = [{distances.prop_pt} {distances.prop_c_all}];
temp = struct2table(cell2struct(y, {'Patient','Control'},2));
violinplot(temp,{'Patient';'Control'})%,...
ylabel('Proportion of Gaze In/Out');
[h,pProp,tbl,stats] = ttest2(distances.prop_pt, distances.prop_c_all);

saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/TTESTOfGazeDistances.png');

if ~strcmp('Fix',em)
    %% Check DSQ
    figure;
    for ii = 1:subNum
        %      figure
        swUnc = thresholdVals(ii).thresholdSWUncrowded;
        %     thresholdUncrowdedSeg{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;%subjectUnc(ii).meanDriftSpan(thresholdIdx);
        %     fprintf('Current Mean Span for %s = %.3f\n', subjectsAll{ii}, thresholdUncrowdedSpan{ii})
        %     checkSpan = [];
        dataStruct(ii).subject = subjectsAll{ii};
        dataStruct(ii).group = group{ii};
        dataStruct(ii).condition = 'Task';
        if strcmp('HUX5',subjectsAll{ii})
            continue;
        end
        for i = 1:length(subjectThreshUnc(ii).em.ecc_0.(swUnc).position)
            x = subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).x;
            y = subjectThreshUnc(ii).em.ecc_0.(swUnc).position(i).y;
            %         checkSpan(i) = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95);
        end
        %         singleSegs = subjectThreshUnc(ii).em.ecc_0.(swUnc).SingleSegmentDsq;
        %         figure;
        %         for i = 1:length(singleSegs)
        %             plot(singleSegs(i))
        %             hold on
        %         end
        %         title(subjectThreshUnc(ii).Condition)
        span = subjectThreshUnc(ii).em.ecc_0.(swUnc).span;
        subplot(4,4,ii)
        histogram(span)
        xlabel('Span')
        % xlim([0 900]);
        title(subjectsAll{ii},'interpreter','latex')
        %         fprintf('%s = \n Mean DSQ %.3f \n Mean SingSeg%.3f \n MeanSpan %.3f \n DSQ %.3f \n\n',subjectThreshUnc(ii).Condition, ...
        %             mean(subjectThreshUnc(ii).em.ecc_0.(swUnc).Dsq),...
        %             mean(subjectThreshUnc(ii).em.ecc_0.(swUnc).SingleSegmentDsq),...
        %             subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan,...
        %             subjectThreshUnc(ii).em.ecc_0.(swUnc).dCoefDsq);
        dataStruct(ii).DC = subjectThreshUnc(ii).em.ecc_0.(swUnc).dCoefDsq;
        dataStruct(ii).Span = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;
        dataStruct(ii).Curvature = mean(cell2mat(subjectThreshUnc(ii).em.ecc_0.(swUnc).curvature));
        for i = 1:length(subjectThreshUnc(ii).em.ecc_0.(swUnc).mn_speed)
            if ~isnan(subjectThreshUnc(ii).em.ecc_0.(swUnc).mn_speed{i})
                speed(i) = subjectThreshUnc(ii).em.ecc_0.(swUnc).mn_speed{i};
            else
                speed(i) = NaN;
            end
        end
        
        dataStruct(ii).Speed = nanmean(speed);
        dataStruct(ii).PRLDistance = mean(subjectThreshUnc(ii).em.ecc_0.(swUnc).prlDistance);
        
        %         dataStruct(ii).
    end
    % xlabel('DPI Span')
    % ylabel('SubSampl Span')
    %     for ii = 1:subNum
    %         dataStruct(ii).subject = subjectsAll{ii};
    %         dataStruct(ii).group = group{ii};
    %         dataStruct(ii).condition = 'Task';
    %     end
    %% percentOfTimeonTargetByPerformance
    condition = {'Uncrowded'};
    [ percentOnTargUncr, sdOnUncrTarg, numTrialsUncr] = ...
        distanceFromTarget(subjectUnc, subjectThreshUnc, subjectsAll, condition, c, fig);
    
    figure
    for ii = 1:subNum
        timeOnTarg{ii} = percentOnTargUncr(ii).(thresholdVals(ii).thresholdSWUncrowded);
        threshold(ii) = subjectThreshUnc(ii).thresh;
    end
    timeOnTarg = str2double(timeOnTarg);
    figure;
    [~,p,~,r] = LinRegression(timeOnTarg,threshold,0,NaN,1,0);
    axis([0 100 0 4])
    xlabel('Percent of time spent on Target')
    ylabel('Threshold')
    % title('Percent of Time by Threshold - Drift Only')
    
    %% AreaCovered and Threshold%%%%%%%%%%%%%
    figure
    for ii = 1:subNum
        swUnc = thresholdVals(ii).thresholdSWUncrowded;
        thresholdUncrowdedArea{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).areaCovered;
        thresholdSW{ii} = (subjectThreshUnc(ii).thresh);
    end
    thresholdSize1 = cell2mat(thresholdSW);
    thresholdArea1 = cell2mat(thresholdUncrowdedArea);
    
    sz = 200;
    % c = jet(subNum);%linspace(1,20,length(thresholdSize1));
    
    
    [~,p,~,r] = LinRegression(thresholdArea1,thresholdSize1,0,NaN,1,0);
    scatter(thresholdArea1,thresholdSize1,sz,c,'filled');%,'Linewidth',5)
    
    axis([1 200 0 4])%([1 7 .75 4])
    xlabel('Area Covered (arcmin^2)')
    ylabel('Threshold')
    title('Area by Threshold')
    
    %% Span and Threshold%%%%%%%%%%%%%%%%%%%%%%5
    figure
    for ii = 1:subNum
        swUnc = thresholdVals(ii).thresholdSWUncrowded;
        thresholdUncrowdedSpan{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;
        thresholdSW{ii} = (subjectThreshUnc(ii).thresh);
    end
    thresholdSize1 = cell2mat(thresholdSW);
    thresholdSpan1 = cell2mat(thresholdUncrowdedSpan);
    
    sz = 200;
    % c = jet(subNum);%linspace(1,20,length(thresholdSize1));
    
    
    [~,p,~,r] = LinRegression(thresholdSpan1,thresholdSize1,0,NaN,1,0);
    for ii = 1:subNum
        leg(ii) = scatter(thresholdSpan1(ii),thresholdSize1(ii),sz,[c(ii,1) c(ii,2) c(ii,3)],'filled');%,'Linewidth',5)
    end
    xValues = unique(round(sort(thresholdSpan1),1));
    % set(gca, 'XTick', xValues, 'xticklabel', {xValues})
    xlabel('Mean Drift Span')
    
    yValues = unique(round(sort(thresholdSize1),1));
    ylabel('Threshold Strokewidth')
    
    % axis([1 7 0 4])%([1 7 .75 4])
    % ylim([1 6])
    text(2,3,sprintf('p = %3f.',p))
    text(2,2.5,sprintf('r = %3f.',r))
    dx = 0.1; dy = 0.1; % displacement so the text does not overlay the data points
    
    legend(leg,subjectsAll)
    % for ii = 1:subNum
    %     text(double(thresholdSpan1(1,ii)+dx), thresholdSize1(1,ii), subjectsAll{ii});
    % end
    
    coeffs = polyfit(thresholdSpan1, thresholdSize1, 1);
    % Get fitted values
    fittedX = linspace(min(thresholdSpan1), max(thresholdSpan1), 200);
    fittedY = polyval(coeffs, fittedX);
    hold on;
    title({'Mean Drift Span by Threshold';sprintf('%s for %s',cell2mat(em),cell2mat(group))});
    saveas(gcf,sprintf('../../SummaryDocuments/HuxlinPTFigures/SpanbyThreshDriftOnly%s.png',cell2mat(group)));
    %
    
    %% prl distance
    % counterU = 1;
    for ii = 1:(subNum)
        swUnc = thresholdVals(ii).thresholdSWUncrowded;
        thresholdUncrowdedSpan{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;
        thresholdSW{ii} = (subjectThreshUnc(ii).thresh);
        distTabUnc(ii).stimSize = ...
            double(subjectThreshUnc(ii).em.ecc_0.(swUnc).stimulusSize);
        distTabUnc(ii).meanPRLDistance = ...
            mean(subjectThreshUnc(ii).em.ecc_0.(swUnc).prlDistance);
        distTabUnc(ii).sizePerform = ...
            subjectThreshUnc(ii).em.ecc_0.(swUnc).performanceAtSize;
    end
    sizePerform0 = [distTabUnc.sizePerform];
    nanVals = ~isnan(sizePerform0);
    sizePerform = sizePerform0(nanVals);
    prlDist = [distTabUnc.meanPRLDistance];
    prlDist = prlDist(nanVals);
    
    % Mean PRL Distance and Threshold
    figure
    [~,p,b,r] = LinRegression([distTabUnc.meanPRLDistance],...
        [distTabUnc.stimSize],0,NaN,1,0);
    for ii = 1:subNum
        leg(ii) =  scatter([distTabUnc(ii).meanPRLDistance],[distTabUnc(ii).stimSize],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
        % leg(ii) = scatter([distTabUnc.meanPRLDistance],[distTabUnc.stimSize],[c(ii,1) c(ii,2) c(ii,3)]);
    end
    xlabel('PRL Distance');
    ylabel('Threshold Stimulus Size');
    axis([0 25 0 5])
    title(sprintf('Uncrowded - %s for %s', cell2mat(em), cell2mat(group)));
    text(10,.9,sprintf('p = %.3f',p))
    legend(leg,subjectsAll)
    % sanity check
    % % figure;
    % % [~,p,b,r] = LinRegression([distTabUnc.stimSize],...
    % %     [distTabUnc.sizePerform],0,NaN,1,0);
    % % xlabel('StimSize');
    % % ylabel('Performance');
    % % ylim([0 1])
    saveas(gcf,sprintf('../../SummaryDocuments/HuxlinPTFigures/PRLDIstbyPerform%s.png',cell2mat(group)));
    % Looking at total Fixation across subjects
    
    
end

%% MS Analysis

%% Drift Analysis
threshDSQUncr = dsqCoefAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, {'Uncrowded'}, c, fig);


% %%%%%%%%%%%%%%%%%%%%%%%
%Calculate Percent of Time Spend on Target
calculatingPercentDriftTarget(subNum, subjectUnc, subjectCro, ...
    subjectThreshUnc, subjectThreshCro, subjectsAll, c, fig);

%Create histogram of Span for the Smallest and Largest SW
spanHistEvaluation(subjectUnc, subjectCro, subNum, 0,...
    subjectThreshUnc, subjectThreshCro); % 0 = measures the DSQ histogram

%Calculates the ms landing probabilities
calculateProbLandingTable(subjectUnc, subjectMSUnc, subNum, subjectCro, subjectMSCro);

condition = {'Uncrowded'};
threshVelUncr = driftVelocityAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, condition, c, fig);
threshCurvUncr = curvatureAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, condition, c, fig);
threshDSQUncr = dsqCoefAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, condition, c, fig);


condition = {'Crowded'};
threshVelCro = driftVelocityAnalysis(subjectCro, subjectThreshCro, subjectsAll, condition, c, fig);
threshCurvCro = curvatureAnalysis(subjectCro, subjectThreshCro, subjectsAll, condition, c, fig);
threshDSQCro = dsqCoefAnalysis(subjectCro, subjectThreshCro, subjectsAll, condition, c, fig);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Look at PValues across variables
checkPValues (subjectUnc, subjectCro, subjectsAll, threshDSQUncr, threshDSQCro,...
    threshCurvUncr, threshCurvCro, threshVelUncr, threshVelCro)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Looks at the difference in performance for small span and large span
%trials
differentSpanTrialsWithPerformances (subNum,performU,performC)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Rate of MS
findUncrowdedvsCrowdedRates(subjectMSUnc,subjectMSCro, subNum, subjectThreshCro, subjectThreshUnc)


%
plotPerformanceForSW( subjectsAll, subjectThreshCro, subjectThreshUnc, c, subNum)
plotPerformanceForThreshold( subjectsAll, subjectThreshCro, subjectThreshUnc, c, subNum)
uncrcrowThreshold(c,subjectThreshUnc, subjectThreshCro, subNum)
%% Graphs THRESHOLD SW by it's Mean Fixation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
meanFixationbySWThresh(subjectUnc, subjectCro, ...
    subjectThreshUnc, subjectThreshCro, subNum, subjectsAll)

%% Graphs each SW and it's mean Fixation
%%NOTNEEDED
meanFixationbyEachSW(subjectUnc, subjectCro)

%% Creates Uncrowded vs Crowded Graph for Span of Drift(Uses values near threshold)
crowdingCondbyMeanFix(subjectUnc, subjectCro, subjectThreshUnc, subjectThreshCro, subNum)

% Plots Span by the Difference in SW

%%Span for Crowded and Uncrowded%%%
for ii = 1:length(subjectCro)
    swDelta{ii} = subjectThreshCro(ii).thresh - subjectThreshUnc(ii).thresh;
    
    thresholdSizeSWRoundUnc = round(subjectUnc(ii).stimulusSize,1);
    thresholdSizeSWRoundCro = round(subjectCro(ii).stimulusSize,1);
    
    matchingThresholdRoundUnc = round(subjectThreshUnc(ii).thresh,1);
    matchingThresholdRoundCro = round(subjectThreshCro(ii).thresh,1);
    
    [~,thresholdIdxUnc] = min(abs(matchingThresholdRoundUnc - thresholdSizeSWRoundUnc));
    [~,thresholdIdxCro] = min(abs(matchingThresholdRoundCro - thresholdSizeSWRoundCro));
    
    swUnc = sprintf('strokeWidth_%i', thresholdIdxUnc);
    swCro = sprintf('strokeWidth_%i', thresholdIdxCro);
    thresholdUncrowdedSpan{ii} = subjectUnc(ii).meanDriftSpan(thresholdIdxUnc);
    thresholdCrowdedSpan{ii} = subjectCro(ii).meanDriftSpan(thresholdIdxCro);
    thresholdUncrowdedSpan{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;
    thresholdCrowdedSpan{ii} = subjectThreshCro(ii).em.ecc_0.(swCro).meanSpan;
    
    thresholdSWUnc{ii} = subjectThreshUnc(ii).thresh;
    thresholdSWCro{ii} = subjectThreshCro(ii).thresh;
end

plotDeltaSW = cell2mat(swDelta);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function multiplSubjVelAnalysis
figure
deltaVel = threshVelCro-threshVelUncr;
[~,p] = LinRegression(deltaVel,plotDeltaSW,0,NaN,1,0);
hold on
figure
scatter(deltaVel,plotDeltaSW,100,c,'filled')
xlabel('\Delta Mean Velocity')
ylabel('\Delta Strokewidth')
title('Difference of Velocity by Difference of SW')
format longG
pValue = sprintf('p = %.3f', p);
text(6, 0, pValue,'Fontsize',25);
axis([-5 14 -1 2])
saveas(gcf,'../../Data/AllSubjTogether/deltaVelocity.png');

figure
scatter(zeros(1,length(threshVelUncr)),threshVelUncr,200,c,'filled')%,'filled')
hold on
scatter(ones(1,length(threshVelCro)),threshVelCro,200,c,'filled')%,'filled')
axis([-0.5 1.5 30 80])
scatter(x,thresholdSize1,sz,c,'filled')%,'filled')
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
    'FontWeight','bold')
ylabel('Mean Velocity at Threshold','FontSize',15,'FontWeight','bold')

xS = [0 1];
for ii = 1:subNum
    y1 = [threshVelUncr(1,ii) threshVelCro(1,ii)];
    line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
end
ttest
[~,p2] = ttest2(threshVelCro,threshVelUncr);
ttestP = sprintf('unpaired ttestP= %.3f', p2);
text(0, 66, ttestP,'Fontsize',18);
title('Velocity; Uncrowded vs Crowded')



saveas(gcf,'../../Data/AllSubjTogether/CvsUVelocity.png');
end

function multiplSubjCurveAnalysis
figure
deltaCurv = threshCurvCro - threshCurvUncr;
[~,p] = LinRegression(deltaCurv,plotDeltaSW,0,NaN,1,0);
hold on
scatter(deltaCurv,plotDeltaSW,100,c,'filled')
xlabel('\Delta Mean Curvature')
ylabel('\Delta Strokewidth')
title('Difference of Curvature by Difference of SW')
format longG
pValue = sprintf('p = %.3f', p);
text(-3, 0, pValue,'Fontsize',25);
axis([-4 3 -1 2])
[~,p2] = ttest2(threshCurvCro,threshCurvUncr);
ttestP = sprintf('paired ttest p = %.3f', p2);
text(-3, -0.5, ttestP,'Fontsize',25);
saveas(gcf,'../../Data/AllSubjTogether/deltaCurvature.png');
end

function multiplSubjDSQAnalysis
figure
sz = 400;
deltaDSQ = threshDSQCro-threshDSQUncr;
[~,p,~,r] = LinRegression(deltaDSQ,plotDeltaSW,0,NaN,1,0);
hold on
scatter(deltaDSQ,plotDeltaSW,sz,c,'filled','d')
xlabel('\Delta Mean Diffusion Coefficient')
ylabel('\Delta Strokewidth')
title('Difference of DC by Difference of SW')
format longG
pValue = sprintf('p = %.3f', p);
text(5, 2, pValue,'Fontsize',25);
axis([-30 80 -1 3])
rValue = sprintf('r^2 = %.3f', r);
pValue = sprintf('p = %.3f', p);
text(-0.5, 1.4, pValue,'Fontsize',10);
text(-0.5,1.6,rValue,'Fontsize',10);
[~,p2] = ttest2(threshDSQCro,threshDSQUncr);
ttestP = sprintf('paired ttest p = %.3f', p2);
text(5, 2.25, ttestP,'Fontsize',25);
saveas(gcf,'../../Data/AllSubjTogether/deltaDSQ.png');
end

function spansToMats%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plotDeltaSpan = (cell2mat(thresholdCrowdedSpan)) - (cell2mat(thresholdUncrowdedSpan));
thresholdSize = cell2mat(thresholdSW);
thresholdUncrowdedSpan = cell2mat(thresholdUncrowdedSpan);
thresholdCrowdedSpan = cell2mat(thresholdCrowdedSpan);

end

function multipleSubjUncrandCroAll
linspace(1,50,length(subjectCro));
scatter(x,y,sz,c,'filled')
sz = 200;
c = jet(subNum);

figure
scatter(thresholdUncrowdedSpan,plotDeltaSW,sz,c)
hold on
scatter(thresholdCrowdedSpan, plotDeltaSW,sz,c, 'filled')

for ii = 1:subNum
    x1 = [thresholdUncrowdedSpan(ii) thresholdCrowdedSpan(ii)];
    y1 = [plotDeltaSW(ii) plotDeltaSW(ii)];
    line(x1, y1);
end

legend('Uncrowded','Crowded')
xlabel('Span of Drift')
ylabel('Delta SW (Crowded-Uncrowded)')
title('Span by SW Difference per Subject')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function multipleSubjDeltaSpan%%%

sz = 400;

figure
[~,p,~,r] = LinRegression(plotDeltaSpan,plotDeltaSW,0,NaN,1,0);
scatter(plotDeltaSpan, plotDeltaSW,sz,c,'filled','d')

coeffs = polyfit(plotDeltaSpan, plotDeltaSW, 1);
Get fitted values
fittedX = linspace(min(plotDeltaSpan), max(plotDeltaSpan), 200);
fittedY = polyval(coeffs, fittedX);
Plot the fitted line
hold on;
plot(fittedX, fittedY, 'b--', 'LineWidth', 1);

meanSpan = mean(plotDeltaSpan);
stdSpan = std(plotDeltaSpan);
totalNumSpan = subNum;

meanSW = mean(plotDeltaSW);
stdSW = std(plotDeltaSW);
totalNumSpan = subNum;


[h,p,ci,stats] = ttest(plotDeltaSpan,plotDeltaSW);

mdl = fitlm(plotDeltaSpan,plotDeltaSW);
p = mdl.Coefficients.pValue(2);

[~,p] = ttest2(plotDeltaSpan,plotDeltaSW);


text(0,2,(sprintf('r^2 = %i',mdl.Rsquared)));

xlabel('\Delta Span')
ylabel('\Delta Strokewidth')
title('Difference of Span by Difference of SW')

format longG
rSquare = round(mdl.Rsquared.Ordinary,3);

rValue = sprintf('r^2 = %.3f', r);
pValue = sprintf('p = %.3f', p);
text(-0.5, 1.4, pValue,'Fontsize',10);
text(-0.5,1.6,rValue,'Fontsize',10);

axis([-1 3 -1 2.5])

saveas(gcf,'../../Data/AllSubjTogether/deltaSpan.png');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LargeSpanCr = [1.72 1.92 2.97 1.53 2.04 1.70];
SmallSpanCr = [1.77 1.33 3.90 1.49 2.21 1.50];


LargeSpanUncr = [1.18 1.54 2.03 1.27 1.43 0.97];
SmallSpanUncr = [1.19 1.19 1.55 1.27 1.35 0.89];

[~,p1] = ttest2(SmallSpanUncr,LargeSpanUncr);
[~,p2] = ttest2(SmallSpanCr,LargeSpanCr);

smallAll = [SmallSpanCr SmallSpanUncr];
largeAll = [LargeSpanCr LargeSpanUncr ];
[~,p] = ttest2(smallAll,largeAll);
end


function makeErrorBarPlotsSessions(idxS,averageAll,stdAll,titleStr,ylabelStr)
title(titleStr)
ylabel(ylabelStr)
xticks([1:3])
xlabel('session')
% for i = 1:3
%     averageAll(i) = mean(y(idxS,i));
%     stdAll(i) = std(y(idxS,i));
% end
hold on
errorbar(1:3,averageAll,stdAll,'-d',...
    'MarkerFaceColor','k','Color','k','MarkerSize',10);
% ylim([5 45]);
xlim([0.75 3.25]);

end
