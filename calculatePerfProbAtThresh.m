function [smallD,largeD] = calculatePerfProbAtThresh(indSubBinUpper,indSubBinLower,subjectsAll,...
    dataAll,var,var2,c,sizeFixTemp, smallD,largeD)

if c == 1
    cVal = -1;
    name = 'Unc';
elseif c ==2
    cVal = 1;
    name = 'Cro';
end

for ii = 1:length(subjectsAll)%[1:10 12];%
    
    %     sizeFix = round(...
    %         sizeFixTemp{c,ii}(1,find(sizeFixTemp{c,ii}(3,:) == ...
    %         max(sizeFixTemp{c,ii}(3,:)))),1);
    
    %     unique(dataAll.Size\
    
    temp1 = find([dataAll.SubjectID] == ii &...
        [dataAll.Condition] == cVal);
    
    newIdx = find([dataAll.Size(temp1)] == mode(dataAll.Size(temp1)));
    sizePick = mode(dataAll.Size(temp1));
    
    forPercent(1,ii) = indSubBinUpper(ii);
    forPercent(2,ii) = indSubBinLower(ii);
    bin(ii,1) = prctile(var{c, ii},forPercent(1,ii),"all");
    bin(ii,2) = prctile(var{c, ii},forPercent(2,ii),"all");
    
    %     find([dataAll.SubjectID] == ii) &...
    %     find([dataAll.Condition] == cVal) & ...
    %     find([dataAll.(var2)] < bin(ii,2))&...
    %     find(round([dataAll.Size],1) == sizeFix)
    
    
    smallD.(name).TrialsIdx = find([dataAll.SubjectID] == ii &...
        [dataAll.Condition] == cVal & ...
        [dataAll.(var2)] < bin(ii,2)&...
        [dataAll.Size] == sizePick);
    
    largeD.(name).TrialsIdx = find([dataAll.SubjectID] == ii &...
        [dataAll.Condition] == cVal & ...
        [dataAll.(var2)] > bin(ii,1)&...
        [dataAll.Size] == sizePick);
    
    smallD.(name).ConeProb(ii) = mean(dataAll.SameConeProb(smallD.(name).TrialsIdx));
    largeD.(name).ConeProb(ii) = mean(dataAll.SameConeProb(largeD.(name).TrialsIdx));
    smallD.(name).ConeProbSEM(ii) = sem(dataAll.SameConeProb(smallD.(name).TrialsIdx));
    largeD.(name).ConeProbSEM(ii) = sem(dataAll.SameConeProb(largeD.(name).TrialsIdx));
    
%     smallD.(name).TrialsIdx = find([dataAll.Size(temp1)] == mode(dataAll.Size(temp1)));
%     largeD.(name).TrialsIdx = find([dataAll.Size(temp2)] == mode(dataAll.Size(temp2)));
    smallD.(name).stimSize(ii) = sizePick;
    largeD.(name).stimSize(ii) = sizePick;
 
    smallD.(name).trialNum(ii) = length(smallD.(name).TrialsIdx);
    largeD.(name).trialNum(ii) = length(largeD.(name).TrialsIdx);
    smallD.(name).Performance(ii) = mean(dataAll.Perf(smallD.(name).TrialsIdx));
    largeD.(name).Performance(ii) = mean(dataAll.Perf(largeD.(name).TrialsIdx));
    
    smallD.(name).NumTrials(ii) = length(dataAll.Perf(smallD.(name).TrialsIdx));
    largeD.(name).NumTrials(ii) = length(dataAll.Perf(largeD.(name).TrialsIdx));
    
    smallD.(name).varz(ii) = mean(dataAll.(var2)(smallD.(name).TrialsIdx));
    largeD.(name).varz(ii) = mean(dataAll.(var2)(largeD.(name).TrialsIdx));
    
end