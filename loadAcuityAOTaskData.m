function [dataTogetherAll] = loadAcuityAOTaskData(subject, manCheck)
% All Subject individual data loads:

% % %%% BARS
% % % load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\Sanjana\Uncrowded_Unstabilized_0ecc\ses1\pptrials.mat');
% %
% % %%% Es
% % % load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\Sanjana\Uncrowded_Unstabilized_0ecc\ses2\pptrials.mat');

%%%% New Eyeris Es
if ~exist(sprintf('SavedPPTrials_CleanAOAcuityData/%s',subject), 'dir') || manCheck == 0
    if strcmp('Sanjana',subject)
        data.ecc0 = load('X:\Ashley\OLD_PreNewEyeris\VisualAcuity\Data\SK\SK\Uncrowded_Unstabilized_0ecc_TE.mat');
        data.ecc10 = load('X:\Ashley\OLD_PreNewEyeris\VisualAcuity\Data\SK\SK\Uncrowded_Unstabilized_10ecc_TE.mat');
        data.ecc15 = load('X:\Ashley\OLD_PreNewEyeris\VisualAcuity\Data\SK\SK\Uncrowded_Unstabilized_15ecc_TE.mat');
        data.ecc25 = load('X:\Ashley\OLD_PreNewEyeris\VisualAcuity\Data\SK\SK\Uncrowded_Unstabilized_25ecc_TE.mat');
    elseif strcmp('Z151',subject) || strcmp('Soma',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z151\te_0ecc_1.mat');
        data.ecc0_2 = load('X:\Ashley\VisualAcuity_Ecc\Z151\te_0ecc_2.mat');
        data.ecc10 = load('X:\Ashley\VisualAcuity_Ecc\Z151\te_10ecc_1.mat');
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Z151\te_15ecc_1.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Z151\te_25ecc_1.mat');
    elseif strcmp('Z126',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z126\0ecc_ses1.mat');
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Z126\15ecc_ses1.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Z126\25ecc_ses1.mat');
        data.ecc0_2 = load('X:\Ashley\VisualAcuity_Ecc\Z126\tumb_e_0ecc_s1.mat');
        data.ecc15_2 = load('X:\Ashley\VisualAcuity_Ecc\Z126\tumb_e_15ecc_s1.mat');
        data.ecc10 = load('X:\Ashley\VisualAcuity_Ecc\Z126\tumb_e_10ecc_s1.mat');
    elseif strcmp('A188',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\A188\tumbE_0ecc_ses1.mat');
        data.ecc0_2 = load('X:\Ashley\VisualAcuity_Ecc\A188\tumbE_0ecc_ses2.mat');
        data.ecc0_3 = load('X:\Ashley\VisualAcuity_Ecc\A188\tumbE_0ecc_ses3.mat');
        data.ecc10_ses1 = load('X:\Ashley\VisualAcuity_Ecc\A188\tumbE_10ecc_ses1.mat');
        data.ecc15_ses1 = load('X:\Ashley\VisualAcuity_Ecc\A188\tumbE_15ecc_ses1.mat');
        data.ecc25_1 = load('X:\Ashley\VisualAcuity_Ecc\A188\tumbE_25ecc_ses1.mat');
        data.ecc25_2 = load('X:\Ashley\VisualAcuity_Ecc\A188\tumbE_25ecc_ses2.mat');
    elseif strcmp('Janis',subject)
        data.ecc0 = load('X:\Ashley\OLD_PreNewEyeris\VisualAcuity\Data\Janis\te_0eccc_1.mat');
        data.ecc10 = [load('X:\Ashley\OLD_PreNewEyeris\VisualAcuity\Data\Janis\te_10eccc_1.mat')];% ...
        data.ecc15 = load('X:\Ashley\OLD_PreNewEyeris\VisualAcuity\Data\Janis\te_15eccc_2.mat');
        data.ecc25 = load('X:\Ashley\OLD_PreNewEyeris\VisualAcuity\Data\Janis\te_25eccc_1.mat');
    elseif strcmp('Ashley',subject) || strcmp('Z055',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z055\tumE0ecc_1.mat');
        data.ecc10 = load('X:\Ashley\VisualAcuity_Ecc\Z055\tumE10ecc_1.mat');% ...
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Z055\tumE15ecc_1.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Z055\tumE25ecc_1.mat');
    elseif strcmp('A144',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\A144\Uncrowded_Unstabilized_0ecc.mat');
        data.ecc10 = load('X:\Ashley\VisualAcuity_Ecc\A144\Uncrowded_Unstabilized_10ecc.mat');% ...
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\A144\Uncrowded_Unstabilized_15ecc.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\A144\Uncrowded_Unstabilized_25ecc.mat');
    elseif strcmp('A132',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\A132\0ecc_ses1.mat');
        data.ecc10_1 = load('X:\Ashley\VisualAcuity_Ecc\A132\10ecc_ses1.mat');% ...
        data.ecc10_2 = load('X:\Ashley\VisualAcuity_Ecc\A132\10ecc_ses2.mat');% ...
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\A132\15ecc_ses1.mat');
        data.ecc25_1 = load('X:\Ashley\VisualAcuity_Ecc\A132\25ecc_ses1.mat');
        data.ecc25_2 = load('X:\Ashley\VisualAcuity_Ecc\A132\25ecc_ses1.mat');
    elseif strcmp('Krish',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Krish\Krish\0ecc_ses1.mat');
%         data.ecc10_1 = load('X:\Ashley\VisualAcuity_Ecc\Krish\Krish\10ecc_ses1.mat');% ...
        data.ecc10 = load('X:\Ashley\VisualAcuity_Ecc\Krish\Krish\10ecc_ses2.mat');% ...
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Krish\Krish\15ecc_ses1.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Krish\Krish\25ecc_ses1.mat');
    elseif strcmp('A169',subject)|| strcmp('Z169',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\A169\0ecc_ses1.mat');
%         data.ecc10 = load('X:\Ashley\VisualAcuity_Ecc\Krish\Krish\10ecc_ses1.mat');% ...
%         data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Krish\Krish\15ecc_ses1.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\A169\25ecc_ses1.mat');
    elseif strcmp('Z091',subject)
        %     data.ecc0 = load('MATFiles/Z091_Uncrowded_Unstabilized_Drift_0ecc_Threshold.mat');
        %     data.ecc10 =load('MATFiles/Z091_Uncrowded_Unstabilized_Drift_10ecc_Threshold.mat');
        %     data.ecc15 =load('MATFiles/Z091_Uncrowded_Unstabilized_Drift_15ecc_Threshold.mat');
        %     data.ecc25 =load('MATFiles/Z091_Uncrowded_Unstabilized_Drift_25ecc_Threshold.mat');
        temp0= load('../../Data/Z091/Uncrowded_Unstabilized_0ecc/ses1/pptrials.mat');
        data.ecc0 = temp0.pptrials;
        temp10= load('../../Data/Z091/Uncrowded_Unstabilized_10eccTemp/ses1/pptrials.mat');
        data.ecc10 = temp10.pptrials;
        temp15= load('../../Data/Z091/Uncrowded_Unstabilized_15eccTemp/ses1/pptrials.mat');
        data.ecc15 = temp15.pptrials;
        temp25= load('../../Data/Z091/Uncrowded_Unstabilized_25eccTemp/ses1/pptrials.mat');
        data.ecc25 = temp25.pptrials;
        data.ecc25_2 = load('X:\Ashley\VisualAcuity_Ecc\A132\25ecc_ses1.mat');
    elseif strcmp('Z187',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z187\Z187_0ecc_ses1.mat');
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z187\Z187_0ecc_ses2.mat');
        data.ecc10= load('X:\Ashley\VisualAcuity_Ecc\Z187\Z187_10ecc_ses2.mat');
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Z187\Z187_ecc15_ses4.mat');
     elseif strcmp('Z127',subject)
        data.ecc0_1 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc0_ses2.mat');
        data.ecc0_2 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc0_ses3.mat');
        data.ecc0_3 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc0_ses4.mat');
        data.ecc10_1 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc10_ses3.mat');
        data.ecc10_2 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc10_ses5.mat');
        data.ecc15_1 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc15_ses3.mat');
        data.ecc15_2 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc15_ses5.mat');
        data.ecc25_1 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc25_ses2.mat');
        data.ecc25_2 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc25_ses2_2.mat');
        data.ecc25_3 = load('X:\Ashley\VisualAcuity_Ecc\Z127\Z127_ecc25_ses5.mat');
    elseif strcmp('Z149',subject) || strcmp('Soma',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_0ecc_ses1.mat');
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_15ecc_ses1.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_25ecc_ses1.mat');
    elseif strcmp('Z197',subject) || strcmp('Soma',subject)
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z197\0ecc_ses1.mat');
        data.ecc0_2 = load('X:\Ashley\VisualAcuity_Ecc\Z197\0ecc_ses2.mat');
        data.ecc0_3 = load('X:\Ashley\VisualAcuity_Ecc\Z197\0ecc_ses2_2.mat');
        data.ecc10 = load('X:\Ashley\VisualAcuity_Ecc\Z197\10ecc_ses3.mat');
        data.ecc10_2 = load('X:\Ashley\VisualAcuity_Ecc\Z197\10ecc_ses3_2.mat');
        data.ecc15= load('X:\Ashley\VisualAcuity_Ecc\Z197\15ecc_ses2.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Z197\25ecc_ses2.mat');
        data.ecc25_2 = load('X:\Ashley\VisualAcuity_Ecc\Z197\25ecc_ses2_2.mat');
        data.ecc25_3 = load('X:\Ashley\VisualAcuity_Ecc\Z197\25ecc_ses3.mat');
   
    elseif strcmp('Z185',subject) 
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_0ecc_ses1.mat');
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_15ecc_ses1.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_25ecc_ses1.mat');
    elseif strcmp('A169',subject) 
        data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_0ecc_ses1.mat');
        data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_15ecc_ses1.mat');
        data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_25ecc_ses1.mat');
   
    elseif strcmp('Z118',subject)
        data.ecc0_1 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_0ecc_ses1.mat');
        data.ecc0_2 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_0ecc_ses4.mat');
        data.ecc0_3 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_0ecc_ses5.mat');
        data.ecc0_4 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_0ecc_ses6.mat');
        data.ecc0_5 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_0ecc_ses6_2.mat');
        data.ecc10_1 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses2.mat');
        data.ecc10_2 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses3.mat');
        data.ecc10_3 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses4.mat');
        data.ecc10_4 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses5.mat');
        data.ecc10_5 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses5.mat');
        data.ecc15_1 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses2.mat');
        data.ecc15_2 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses3.mat');
        data.ecc15_3 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses5.mat');
        data.ecc15_4 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_15ecc_ses6.mat');
        data.ecc25_1 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_25ecc_ses3.mat');
        data.ecc25_2 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_25ecc_ses5.mat');
        data.ecc25_3 = load('X:\Ashley\VisualAcuity_Ecc\Z118\Z118_25ecc_ses6.mat');

%     elseif strcmp('A189',subject)
%         data.ecc0 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_0ecc_ses1.mat');
%         data.ecc15 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_15ecc_ses1.mat');
%         data.ecc25 = load('X:\Ashley\VisualAcuity_Ecc\Z149\Z149_25ecc_ses1.mat');
    end
else
    data.ecc0 = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc0.mat',subject));
    data.ecc10 = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc10.mat',subject));
    data.ecc15 = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc15.mat',subject));
    data.ecc25 = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc25.mat',subject));
end
allNamesEcc = fieldnames(data);
processedPPTrials = [];
if manCheck == 0
    for d = 1:length(allNamesEcc)
        temp = data.(allNamesEcc{d});
        dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
            str2double(temp.eis_data.filename(9:10)),...
            str2double(temp.eis_data.filename(11:12)));
        
        temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
        processedPPTrials{d} = newEyerisEISRead(temppptrials);
        
    end 
    num_strs = regexp(allNamesEcc,'\d*','Match')';
    % eccs = [0 10 15 25];
    % correctPPtrials = [];
    correctPPtrials0 = [];
    correctPPtrials10 = [];
    correctPPtrials15 = [];
    correctPPtrials25 = [];
    for ii = 1:length(num_strs)
        vals = num_strs{ii};
        name = string(sprintf('ecc%s',(vals{1})));
        if strcmp(vals{1},'0')
            correctPPtrials0 =  [correctPPtrials0 processedPPTrials{ii}];
        elseif strcmp(vals{1},'10')
            correctPPtrials10 =  [correctPPtrials10 processedPPTrials{ii}];
        elseif strcmp(vals{1},'15')
            correctPPtrials15 =  [correctPPtrials15 processedPPTrials{ii}];
        elseif strcmp(vals{1},'25')
            correctPPtrials25 =  [correctPPtrials25 processedPPTrials{ii}];
        end
    end
    if ~isempty(correctPPtrials0)
        dataTogetherAll.ecc0 = correctPPtrials0;
    end
    if ~isempty(correctPPtrials10)
        dataTogetherAll.ecc10 = correctPPtrials10;
    end
    if ~isempty(correctPPtrials15)
        dataTogetherAll.ecc15 = correctPPtrials15;
    end
    if ~isempty(correctPPtrials25)
        dataTogetherAll.ecc25 = correctPPtrials25;
    end
else
    dataTogetherAll = data;
end
% end
end