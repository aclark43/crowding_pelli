close all
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.fig = struct('FIGURE_ON', 0,...
    'FOLDED',0);
fig = params.fig;
subjectsAll = {'Z023','Ashley','Z002','Z023DDPI','AshleyDDPI','Z002DDPI'};%,...
params.driftSize = 175;
params.em = {'Drift'};

em = params.em;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
c = brewermap(6,'Set1');
% c = jet(length(subjectsAll));
params.c = c;


condition = {'Uncrowded','Crowded'};
[subjectUnc, subjectThreshUnc, subjectMSUnc, performU] = ...
    loadVariables(subjectsAll, {condition{1}}, params, 0, '0ecc');
[subjectCro, subjectThreshCro, subjectMSCro, performC ] = ...
    loadVariables(subjectsAll, {condition{2}}, params, 0, '0ecc');



thresholdSWVals = determineThresholdSW(subjectUnc,subjectThreshUnc,...
    subjectThreshCro, subjectCro, params.subNum,subjectsAll);
% thresholdSWVals = determineThresholdSW(subjectUnc,performU,...
%     performC, subjectCro, params.subNum,subjectsAll);

for ii = 1:subNum
    if strcmp('Z023',subjectsAll{ii}) || strcmp('Ashley',subjectsAll{ii}) ||...
             strcmp('Z002',subjectsAll{ii})
        params.machine(ii) = 1; %DPI
        params.conversionFac(ii) = 1;
        params.sampling(ii) = 1000;
%     elseif strcmp('Z024',subjectsAll{ii}) || strcmp('Z064',subjectsAll{ii}) ||...
%             strcmp('Z046',subjectsAll{ii}) || strcmp('Z084',subjectsAll{ii}) ||...
%             strcmp('Z014',subjectsAll{ii})
    else
        params.machine(ii) = 2; %dDPI
        params.conversionFac(ii) = 1000/330;
        params.sampling(ii) = 341;
    end
end

halfNum = length(subjectsAll)/2;

%% Plot example comparison traces
% for ii = 1:halfNum
%     numberCompareTrials = min([length(subjectThreshUnc(ii).em.allTraces.x)...
%         length(subjectThreshUnc(ii+3).em.allTraces.x)]);
%     for i = 1:numberCompareTrials
%         figure('Position', [2000 10 900 600]);
%         x1 = subjectThreshUnc(ii).em.allTraces.x{i};
%         y1 = subjectThreshUnc(ii).em.allTraces.y{i};
%         leg(1) = plot(x1,'Color','k');
%         hold on
%         leg(2) = plot(y1,'Color','r');
%         
%         x2 = subjectThreshUnc(ii+3).em.allTraces.x{i};
%         y2 = subjectThreshUnc(ii+3).em.allTraces.y{i};
%         leg(3) = plot(1:(1000/330):length(x2)*(1000/330),...
%             x2,'Color','b');
%         leg(4) = plot(1:(1000/330):length(y2)*(1000/330),...
%             y2,'Color','m');
%         ylim([-15 15])
%         legend(leg,{'X DPI','Y DPI','X DDPI','Y DDPI'});
%         title(subjectsAll{ii});
%     end
% end

%% Organize Data

for ii = 1:halfNum
    data.dsq(ii,1) = subjectThreshUnc(ii).em.allTraces.dCoefDsq;
    data.dsq(ii,2) = subjectThreshUnc(ii+3).em.allTraces.dCoefDsq;
    
    data.span(ii,1) = mean(subjectThreshUnc(ii).em.allTraces.span);
    data.span(ii,2) = mean(subjectThreshUnc(ii+3).em.allTraces.span);
    
    data.area(ii,1) = (subjectThreshUnc(ii).em.allTraces.areaCovered);
    data.area(ii,2) = (subjectThreshUnc(ii+3).em.allTraces.areaCovered);
    
    data.angle(ii,1) = mean(cell2mat(subjectThreshUnc(ii).em.allTraces.driftAngle));
    data.angle(ii,2) = mean(cell2mat(subjectThreshUnc(ii+3).em.allTraces.driftAngle));
    
    data.amplitude(ii,1) = mean(subjectThreshUnc(ii).em.allTraces.amplitude);
    data.amplitude(ii,2) = mean(subjectThreshUnc(ii+3).em.allTraces.amplitude);
    
    data.curve(ii,1) = mean(cell2mat(subjectThreshUnc(ii).em.allTraces.curvature));
    data.curve(ii,2) = mean(cell2mat(subjectThreshUnc(ii+3).em.allTraces.curvature));
    
    idx = ~cellfun(@isnan,subjectThreshUnc(ii).em.allTraces.mn_speed);
    data.speed(ii,1)= mean(cell2mat(subjectThreshUnc(ii).em.allTraces.mn_speed(idx)));
    
    idx = ~cellfun(@isnan,subjectThreshUnc(ii+3).em.allTraces.mn_speed);
    data.speed(ii,2)= mean(cell2mat(subjectThreshUnc(ii+3).em.allTraces.mn_speed(idx)));
    
    data.numTrials(ii,1) = length(subjectThreshUnc(ii).em.allTraces.x);
    data.numTrials(ii,2) = length(subjectThreshUnc(ii+3).em.allTraces.x);
    
end
%% Other Drift Comparisons

namesChar = {'dsq','span','area','speed','curve','amplitude','angle','numTrials'};
figure('Position', [2000 -20 900 1000]);
hold on
for numChar = 1:length(namesChar)
    subplot(3,3,numChar)
    d1 = [];
    d2 = [];
    for ii = 1:halfNum
        d1(ii) = data.(namesChar{numChar})(ii,1);
        d2(ii) = data.(namesChar{numChar})(ii,2);
        leg2(ii) = scatter(0, d1(ii),100, params.c(ii,:), 'filled');
        hold on
        scatter(1, d2(ii),100, params.c(ii,:), 'filled');
        line([0 1],[d1(ii) ...
            d2(ii)],'Color',params.c(ii,:));
    end
    names = ({'DPI','dDPI'});
    set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',12)

    ylabel(namesChar{numChar})
    [~,p] = ttest(d1, d2);
    title(sprintf('p = %.3f', p));

    legend(leg2,subjectsAll{1:3},'Location','northwest');
    axis([-0.5 1.5 min([d1 d2]) max([d1 d2])*1.7])
end
suptitle('Drift Characteristics');
saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\DPIvsDDPI\Reports\images\DriftChar.png');




