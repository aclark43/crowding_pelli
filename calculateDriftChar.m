function em = calculateDriftChar(valueGroup,params,pptrials,em)

idx = valueGroup.idx;
strokeWidth = valueGroup.strokeWidth;
ecc = valueGroup.ecc;
stimulusSize = valueGroup.stimulusSize;

counter = 1;
xValues = [];
yValues = [];
xOffset = [];
yOffset = [];
xValuesShort = [];
yValuesShort = [];

for idxValue = 1:length(idx)
    i = idx(idxValue);
    dataStruct.id(counter) = i;
    
    timeOn = round(pptrials{i}.TimeTargetON/(1000/params.sampling));
    timeOff = floor(min(pptrials{i}.TimeTargetOFF/(1000/params.sampling), ...
        pptrials{i}.ResponseTime/(1000/params.sampling)));
    
    %     x = pptrials{i}.x.position(timeOn:timeOff) + pptrials{i}.pixelAngle * pptrials{i}.xoffset;
    %     y = pptrials{i}.y.position(timeOn:timeOff) + pptrials{i}.pixelAngle * pptrials{i}.yoffset;
    x = pptrials{i}.x.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.xoffset * params.pixelAngle(i);
    y = pptrials{i}.y.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.yoffset * params.pixelAngle(i);
    
    
    if params.FIXED_SIZE
        dataStruct.contrast = pptrials{i}.fixedContrast;
    end
    dataStruct.position(counter).x = x;
    dataStruct.position(counter).y = y;
    
    if strcmp(params.stabilization ,'Stabilized')
        dataStruct.stabInfo(counter).stabX = pptrials{i}.XStab.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.xoffset * params.pixelAngle(i); %+ pptrials{i}.xoffset * params.pixelAngle(i);
        dataStruct.stabInfo(counter).stabY = pptrials{i}.YStab.position(ceil(timeOn):floor(timeOff)) + pptrials{i}.yoffset * params.pixelAngle(i);
        dataStruct.stabInfo(counter).stabXDiff = x - dataStruct.stabInfo(counter).stabX;
        dataStruct.stabInfo(counter).stabYDiff = y - dataStruct.stabInfo(counter).stabY;
    end
    
    dataStruct.recenteredposition(counter).x = x-x(1);
    dataStruct.recenteredposition(counter).y = y-y(1);
    
    xValues = [xValues, x];
    yValues = [yValues, y];
    
    xOffset = [xOffset, pptrials{i}.xoffset * params.pixelAngle(i)];
    xOffset = [xOffset, pptrials{i}.yoffset * params.pixelAngle(i)];
    
    dataStruct.flankers(counter) = pptrials{i}.FlankerOrientations;
    dataStruct.response(counter) = pptrials{i}.Response;
    dataStruct.responseTime(counter) = pptrials{i}.ResponseTime;
    dataStruct.target(counter) = pptrials{i}.TargetOrientation;
    dataStruct.correct(counter) = double(pptrials{i}.Correct);
    
    % Full Trial Length
    %     [ dataStruct.span(counter), ...
    %         dataStruct.amplitude(counter), ...
    %         dataStruct.meanSpan(counter), ...
    %         dataStruct.stdSpan(counter), ...
    %         dataStruct.meanAmplitude(counter), ...
    %         dataStruct.stdAmplitude(counter), ...
    %         dataStruct.prlDistance(counter),...
    %         dataStruct.prlDistanceX(counter), ...
    %         dataStruct.prlDistanceY(counter)] = ...
    %         calculateCrowdingDriftStats(x, y);
    
    [~, dataStruct.velocity(counter), ...
        velX{counter}, ...
        velY{counter}] = ...
        CalculateDriftVelocity(dataStruct.position(counter), 1);
    
    [~, dataStruct.instSpX{counter},...
        dataStruct.instSpY{counter},...
        dataStruct.mn_speed{counter},...
        dataStruct.driftAngle{counter},...
        dataStruct.curvature{counter},...
        dataStruct.varx{counter},...
        dataStruct.vary{counter},...
        dataStruct.bcea{counter},...
        dataStruct.span(counter), ...
        dataStruct.amplitude(counter), ...
        dataStruct.prlDistance(counter),...
        dataStruct.prlDistanceX(counter), ...
        dataStruct.prlDistanceY(counter)] = ...
        getDriftChar(x, y, 41, 1, 250);
    
    % Shorter Trial Length
    xShort = x(1:round(175/((1000/params.sampling))));
    yShort = y(1:round(175/((1000/params.sampling))));
    
    dataStruct.shortAnalysis.position(counter).x = x(1:round(175/((1000/params.sampling))));
    dataStruct.shortAnalysis.position(counter).y = y(1:round(175/((1000/params.sampling))));
    
    xValuesShort = [xValuesShort, xShort];
    yValuesShort = [yValuesShort, yShort];
    
    [~, dataStruct.shortAnalysis.velocity(counter), ...
        velX{counter}, ...
        velY{counter}] = ...
        CalculateDriftVelocity(dataStruct.shortAnalysis.position(counter), 1);
    
    [~, dataStruct.shortAnalysis.instSpX{counter},...
        dataStruct.shortAnalysis.instSpY{counter},...
        dataStruct.shortAnalysis.mn_speed{counter},...
        dataStruct.shortAnalysis.driftAngle{counter},...
        dataStruct.shortAnalysis.curvature{counter},...
        dataStruct.shortAnalysis.varx{counter},...
        dataStruct.shortAnalysis.vary{counter}, ...
        dataStruct.shortAnalysis.bcea{counter}, ...
        dataStruct.shortAnalysis.span(counter), ...
        dataStruct.shortAnalysis.amplitude(counter), ...
        dataStruct.shortAnalysis.prlDistance(counter),...
        dataStruct.shortAnalysis.prlDistanceX(counter), ...
        dataStruct.shortAnalysis.prlDistanceY(counter)] = ...
        getDriftChar(xShort, yShort, 41, 1, 250);
    
    %% Other Analysis
    % % %     %From Yenchu
    % r_ellipse can be used for plotting ; original_mu offset
%     [dataStruct.VX{counter},...
%         dataStruct.original_mu{counter},...
%         dataStruct.r_ellipse{counter}] = tgdt_fit_2d_gaussian(x,y,0.99);
%     
    %From Zhetuo
    %     plot_2D_density(x,y,ones(1,length(x))*-30,ones(1,length(y))*-30)
    %     plot_2D_density(x,y,-10:0.2:10, -10:0.2:10);
    
    %% Curvature
    dist_from_start = cumsum( [0, sqrt((x(2:end)-x(1:end-1)).^2 + ...
        (y(2:end)-y(1:end-1)).^2)] );
    
    dataStruct.old_curvatureAshley{counter}  = dist_from_start(length(dist_from_start))/...
        mean(sqrt(x.^2 + y.^2));
    
    [dataStruct.old_curvature2{counter},...
        dataStruct.old_curvatureLength{counter},...
        dataStruct.old_curvature_Use{counter}] = ...
        CalculateLengthAndCurvature(x, y);
    
    %% %Short %%%%%%%%%%%%%%
    dist_from_startSHORT = cumsum( [0, sqrt((xShort(2:end)-xShort(1:end-1)).^2 + ...
        (yShort(2:end)-yShort(1:end-1)).^2)] );
    
    dataStruct.shortAnalysis.old_curvatureAshley  = dist_from_startSHORT(length(dist_from_startSHORT))/...
        mean(sqrt(xShort.^2 + yShort.^2));
    
    counter = counter + 1;
end
%% Overall Measurements
%      save
%Distance, and Normalized by the size Distance
% [Dist,nDist] = LD_dist_covar_pairwise(VXM1,VXM2);

dataStruct.ccDistance = unique(stimulusSize)*1.4 - stimulusSize/2;
dataStruct.stimulusSize = unique(stimulusSize);
dataStruct.performanceAtSize = round(sum(dataStruct.correct)/length(dataStruct.correct),2);



%Full Length
forDSQCalc.x = {dataStruct.position.x};
forDSQCalc.y = {dataStruct.position.y};

[~,dataStruct.Bias,dataStruct.dCoefDsq, ~, dataStruct.Dsq, ...
    dataStruct.SingleSegmentDsq,~,~] = ...
    CalculateDiffusionCoef(params.sampling, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));


[ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/100)),31); %Checks strange artifacts
[ xValues, yValues] = checkXYBound(xValues, yValues, 30); %Checks boundries

limit.xmin = floor(min(xValues));
limit.xmax = ceil(max(xValues));
limit.ymin = floor(min(yValues));
limit.ymax = ceil(max(yValues));

n_bins = 20;

result = MyHistogram2(xValues, yValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
dataStruct.result = result./(max(max(result)));

% dataStruct.areaCovered = CalculateTheArea(xValues,yValues ,0.68, max(abs(cell2mat(struct2cell(limit)))), n_bins);
dataStruct.xAll = xValues;
dataStruct.yAll = yValues;

dataStruct.xOffset = xOffset;
dataStruct.yOffset = yOffset;
% if ~params.crowded
%     plot_2D_density(xValues,yValues,-20:0.2:20, -20:0.2:20);
%     title(params.subject);
%     saveas(gcf, ...
%         sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/BiasVector/BiasPlots%s%.2f.png',...
%         params.subject,valueGroup.stimulusSize));
% end
%Short Length%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
forDSQCalcShort.x = {dataStruct.shortAnalysis.position.x};
forDSQCalcShort.y = {dataStruct.shortAnalysis.position.y};

[~,dataStruct.shortAnalysis.Bias,dataStruct.shortAnalysis.dCoefDsq, ~, dataStruct.shortAnalysis.Dsq, ...
    dataStruct.shortAnalysis.SingleSegmentDsq,~,~] = ...
    CalculateDiffusionCoef(params.sampling, struct('x',forDSQCalcShort.x, 'y', forDSQCalcShort.y));

[ xValuesShort, yValuesShort] = checkXYArt(xValuesShort, yValuesShort, min(round(length(xValuesShort)/100)),31); %Checks strange artifacts
[ xValuesShort, yValuesShort] = checkXYBound(xValuesShort, yValuesShort, 30); %Checks boundries
dataStruct.shortAnalysis.areaCovered = CalculateTheArea(xValuesShort,yValuesShort,0.68, max(abs(cell2mat(struct2cell(limit)))), n_bins);
dataStruct.shortAnalysis.xAll = xValuesShort;
dataStruct.shortAnalysis.yAll = yValuesShort;

em.(ecc).(strokeWidth) = dataStruct;


end