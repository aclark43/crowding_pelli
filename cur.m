function curvatur=cur(x,y)
%% Curvature Estimation
%%% Bigger curvature value -> greater curvature 
%%% (smaller radius of fit of circle to 3 consecutive points).
%%% Current version 2022 - AMC, AP Lab

    dx = gradient(x);
    ddx = gradient(dx);
    dy = gradient(y);
    ddy = gradient(dy);

    num = dx .* ddy - ddx .* dy;
    denom = dx .* dx + dy .* dy;
    denom = sqrt(denom);
    denom = denom .* denom .* denom;
    curvatur = num ./ denom;
%     curvatur(denom < 0) = NaN;
end