function [dataTogetherAll] = loadAcuityPelliData(subject, manCheck)
% All Subject individual data loads:

% % %%% BARS
% % % load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\Sanjana\Uncrowded_Unstabilized_0ecc\ses1\pptrials.mat');
% %
% % %%% Es
% % % load('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\Sanjana\Uncrowded_Unstabilized_0ecc\ses2\pptrials.mat');

%%%% New Eyeris Es
if ~exist(sprintf('SavedPPTrials_CleanPelli/%s',subject), 'dir') || manCheck == 0
    if strcmp('Z160',subject) || strcmp('Sanjana',subject)
        data.Unstab = load('X:\Ashley\Crowding\Z160\Uncrowded_Unstabilized_0ecc_PEST.mat');
        data.Stab = load('X:\Ashley\Crowding\Z160\Uncrowded_Stabilized_0ecc_PEST.mat');
    end
else
    %     data.ecc0 = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc0.mat',subject));
    %     data.ecc10 = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc10.mat',subject));
    %     data.ecc15 = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc15.mat',subject));
    %     data.ecc25 = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc25.mat',subject));
end
allNamesStab = fieldnames(data);
processedPPTrials = [];
if manCheck == 0
    for d = 1:length(allNamesStab)
        temp = data.(allNamesStab{d});
        dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
            str2double(temp.eis_data.filename(9:10)),...
            str2double(temp.eis_data.filename(11:12)));
        
        temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
        processedPPTrials{d} = newEyerisEISRead(temppptrials);
        
    end 
%     num_strs = regexp(allNamesStab,'\d*','Match')';
    % eccs = [0 10 15 25];
    % correctPPtrials = [];
    correctPPtrialsU = [];
    correctPPtrialsS = [];
%     correctPPtrials15 = [];
%     correctPPtrials25 = [];
    for ii = 1:2%length(num_strs)
        vals = ii;
        if ii == 1
            name = 'Unstabilized';
        elseif ii == 2
            name = 'Stabilized';
        end
%         name = string(sprintf('%s',(vals{1})));
        if ii == 1
            correctPPtrialsU =  [correctPPtrialsU processedPPTrials{ii}];
        else
            correctPPtrialsS =  [correctPPtrialsS processedPPTrials{ii}];
        end
    end
    if ~isempty(correctPPtrialsU)
        dataTogetherAll.Unstab = correctPPtrialsU;
    end
    if ~isempty(correctPPtrialsS)
        dataTogetherAll.Stab = correctPPtrialsS;
    end
else
    dataTogetherAll = data;
end
% end
end