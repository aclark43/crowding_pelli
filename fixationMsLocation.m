function fixationMsLocation(fixAllSW, params, sw, trialChar, title_str, em)

%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

sz = 40;


[allStartLocationMSx, allEndLocationMSx, allStartLocationMSy, allEndLocationMSy,...
   allAngle, allAmplitude, allID] = buildMSDetails(fixAllSW);

if length(allStartLocationMSx) > 10
    
subplot(1,3,1);
scatter(allStartLocationMSx,allStartLocationMSy,'MarkerEdgeColor',...
    [0 .5 .5],'MarkerFaceColor',[0 .7 .7],'LineWidth',1.5);
hold on
scatter(allEndLocationMSx, allEndLocationMSy,'MarkerFaceColor',...
    [255/255 153/255 255/255],'MarkerEdgeColor',[102/255 0/255 51/255],'LineWidth',1.5);

% if FIXATION_ANALYSIS
    if strcmp('D',params.machine)
        plot(0,0,'-ks','MarkerSize',100*params.pixelAngle);
        graphTitleLine1 = ('Fixation_Trial_Drift_Heat_Map');
    else
        plot(0,0,'-ks','MarkerSize',60*params.pixelAngle);
        graphTitleLine1 = ('Fixation_Trial_Drift_Heat_Map');
    end
    
% else
%     stimuliSize = sw * params.pixelAngle;
%     if params.crowded == true
%         rectangle('Position',[-(2*stimuliSize+(2*stimuliSize*1.4)) -stimuliSize*5 2*stimuliSize 10*stimuliSize],'LineWidth',1)
%         rectangle('Position',[(2*stimuliSize*1.4) -stimuliSize*5 2*stimuliSize 10*stimuliSize], 'LineWidth',1)
%         rectangle('Position',[(-stimuliSize) -(3*stimuliSize+(10*stimuliSize*1.4)) (2*stimuliSize) (10*stimuliSize)], 'LineWidth',1)
%         rectangle('Position',[(-stimuliSize) ((10*stimuliSize*1.4)-7*stimuliSize) (2*stimuliSize) (10*stimuliSize)], 'LineWidth',1)
%     end
%     rectangle('Position',[(-stimuliSize) (-stimuliSize*5) (2*stimuliSize) (10*stimuliSize)],'LineWidth',1)
% end
title('MS Start & End');

n = length(allID);
% number = numel(n);
text (-20,-20,(sprintf('N=%d', n)));

axisValue = 30;
axis tight
axis square
axis([-axisValue axisValue -axisValue axisValue]);
legend ('Start Location','End Location');
xlabel('X [arcmin]')
ylabel('Y [arcmin]')

%%%Plots the #of MS in a given direction/Angle
microsaccadeDirections = allAngle;
c = linspace(0, 2*pi, 30);
n1 = hist(microsaccadeDirections, c); % histogram of saccade directions
n1 = n1/ sum(n1); % normalize for empirical probability distribution
subplot(1,3,2);
h1p = polar([c, c(1)], [n1, n1(1)]);  % repeat first point so that it goes all the way around
title({'MS Direction','(# of MS per Direction)'})
set(gcf, 'Position',  [600, 300, 1200, 700])


%%%Plot a HeatMap of Angle and Amplitude (using end-start)

%%%Histogram of #MS by Amplitude
subplot(1,3,3);
microsaccadeAmlpitude = allAmplitude;
averageMSAmp = mean(allAmplitude);
averageMSAmpTitle = sprintf('Mean = %.3f', averageMSAmp);
stdMSAmp = std(allAmplitude);
stdMSAmpTitle = sprintf('STD %s %.3f', char(177), stdMSAmp);

histogram(microsaccadeAmlpitude,50,'FaceColor','r');
xlabel('Amplitude(arcmin)')
ylabel('Number of MS')
title({'MS Amplitude',averageMSAmpTitle, stdMSAmpTitle});
axis square

averageMSRate = mean(em.microsaccadeRate);

graphTitleLine1 = ('All Strokewidths for Fixation');
oldgraphTitleLine2 = sprintf('%s', (title_str));
newgraphTitleLine2 = strrep(oldgraphTitleLine2,'_',' ');
graphTitleLine3 = sprintf('Average MS Rate = %.3f', averageMSRate);
suptitle({graphTitleLine1,newgraphTitleLine2,graphTitleLine3})

filepath = 'C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/';

saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/IMAGE/%s.png',...
    filepath, trialChar.Subject, sprintf('%s_MS_Direction_&_Amplitude_Analysis_for_Fixation',trialChar.Subject)));
saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/%s.epsc',...
    filepath, trialChar.Subject, sprintf('%s_MS_Direction_&_Amplitude_Analysis_for_Fixation',trialChar.Subject)));
saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/FIG/%s.fig',...
    filepath, trialChar.Subject, sprintf('%s_MS_Direction_&_Amplitude_Analysis_for_Fixation',trialChar.Subject)));
% close all




%%%%%Velocity HeatMap
figure
% heatmapX = [allStartLocationMSx - allEndLocationMSx];
% heatmapY = [allStartLocationMSy - allEndLocationMSy];
 heatmapX = [allEndLocationMSx - allStartLocationMSx];
 heatmapY = [allEndLocationMSy - allStartLocationMSy];
ndhist(heatmapX, heatmapY, 'bins',.5, 'radial','axis',[-30 30 -30 30],'nr','filt');
graphTitleLine1 = ('Fixations_MS_Map_for_StartandEnd_Locations');
graphTitleLine2 = title_str;

title({graphTitleLine1,graphTitleLine2},'Interpreter','none');
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
shading interp
saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/IMAGE/%s.png',...
    filepath, trialChar.Subject, sprintf('%s_MS_Landing_for_Fixation_MS_%i_%s',trialChar.Subject,sw,title_str)));
saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/%s.epsc',...
    filepath, trialChar.Subject, sprintf('%s_MS_Landing_for_Fixation_MS_%i_%s',trialChar.Subject,sw,title_str)));
saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/FIG/%s.fig',...
    filepath, trialChar.Subject, sprintf('%s_MS_Landing_for_Fixation_MS_%i_%s',trialChar.Subject,sw,title_str)));
% close all
end
end



