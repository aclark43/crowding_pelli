function validSW = generateDriftDiffusion(driftData, trialChar, title_str)
%Plots drift coefficient 
%   Detailed explanation goes here

figure('Position', [400, 500, 1500, 500]);
hold on

sw = fieldnames(driftData);


swColors = parula(length(sw));
%swColors = {'r','b','g','k','y','m'};
jj = 1;
%swColors = linspace(0,1); %parula(length(sw));

%%%Graph each individual SW%%%
for ii = 1:length(sw)
    if ~ isfield(driftData, 'position')
        strokewidth = sw{ii};
        if length(driftData.(strokewidth).id) > 30
            validSW{jj} = strokewidth;
            subplot(2,3,ii);
            hold on
            ylim([0 18])
            xlim([0 250])
            shadedErrorBar(driftData.(strokewidth).timeDsq*1000, ...
                mean(driftData.(strokewidth).singleSegmentDsq, 1), ...
                std(driftData.(strokewidth).singleSegmentDsq, 1)/sqrt(size(driftData.(strokewidth).singleSegmentDsq,1)), ...
                'lineprops', {swColors(ii,:)});
            xlabel('Temporal interval [ms]')
            ylabel('Dsq [arcmin^2]')
            graphTitle = sprintf('%s Drift_Diffusion_for_%s',trialChar.Subject, strokewidth);
            title(graphTitle,'Interpreter','none');
            
            n = length(driftData.(strokewidth).id);
            text (50,15,(sprintf('N=%d', n)));
            
            jj = jj + 1;
            saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/Individual_%s%s.epsc', trialChar.Subject, graphTitle, title_str));
            saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/JPEG/DriftDiffusion_with_DSQ/Individual_%s%s.jpeg', trialChar.Subject, graphTitle, title_str));
            saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/FIG/DriftDiffusion_with_DSQ/Individual_%s%s.fig', trialChar.Subject, graphTitle, title_str));
        end
    end
end



%%%Graph all SW Together    
figure('Position', [400, 500, 500, 400]);
if exist('validSW','var')
    for ii = 1:length(validSW)
        if exist('validSW','var')
            strokewidth = validSW{ii};
            % text(51,16, sprintf('Fixation: %.2f arcmin^2/s', mean(tmp_fix_dcoef)))
            % text(51,15, sprintf('Foveola: %.2f arcmin^2/s', mean(tmp_sm_dcoef)))
            % text(51,14, sprintf('P-value: %.4f', p))
            hold on
            h = shadedErrorBar(driftData.(strokewidth).timeDsq*1000, ...
                mean(driftData.(strokewidth).singleSegmentDsq, 1), ...
                std(driftData.(strokewidth).singleSegmentDsq, 1)/sqrt(size(driftData.(strokewidth).singleSegmentDsq,1)), ...
                'lineprops', {swColors(ii,:)});
            H(ii) = h.mainLine;
        end
    end


legend(H, validSW, 'Fontsize',10,'Location','Northwest','Interpreter','none');


xlabel('Temporal interval [ms]')
ylabel('Displacement square [arcmin^2]')
graphTitle1 =('Drift_Diffusion_All_StrokeWidths');
graphTitle2 =(title_str);
title({graphTitle1, graphTitle2},'Interpreter','none');
set(gca, 'FontSize',12)
xlim([20 250])
box off

saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/%s%s.epsc', trialChar.Subject, graphTitle, title_str));
saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/JPEG/DriftDiffusion_with_DSQ/%s%s.jpeg', trialChar.Subject, graphTitle, title_str));
saveas(gcf, sprintf('../Data/%s/Graphs/DriftAnalysis/FIG/DriftDiffusion_with_DSQ/%s%s.fig', trialChar.Subject, graphTitle, title_str));

close all
end

