% Paired T-Test
%
% Check statistical significance within subjects by means of T-test
% For each point the null hp mean 1= mean 2 is tested vs. mean 1 n.e. mean
% 2 with a alpha confidence.
%
%   [E1, E2, t, p, hh] = T_Test(x1, x2, alpha, Test Type, Pairing, print)
%
%    E1, E2: Within subjects confidence intervals of data points in conditions 1 and 2
%    t: t-score
%    p probability beyond t
%    hh: significance (1 significant, 0 not significant)
%
%  x1 and x2 contain the mean score of the various subjects in the two
%  conditions to be tested.
%
%  Optional parameters:
%  alpha: significance level (default 0.05)
%  Test Type:  "One Sided" or "Two Sided" (default Two Sided)
%  Pairing Method:  "Paired" or "Unpaired" samples (default: paired)
%  print:  verbose, 0 no written output (default verbose).
%
%  Michele Rucci - revised June 3, 2010.
function [E1, E2, t, p, hh, E] = T_Test(varargin)
x1 = varargin{1};
x2 = varargin{2};
alpha = 0.05;
TestType = 2;
PrintFlag = 1;
Pairing = 1;
if (nargin>2)
   alpha = varargin{3};
end
if (nargin>3)
   switch (lower(varargin{4}))
     case 'one sided'
       TestType = 1;
     case 'two sided'
       TestType = 2;
   end
end
% if (nargin>4)
%    PairingString = varargin{5};
%    switch (lower(varargin{5}))
%      case 'paired'
%        Pairing = 1;
%      case 'unpaired'
%        Pairing = 0;
%    end
% end
if (nargin > 5)
   PrintFlag = varargin{6};
end
if(Pairing)
   n = size(x1,1);
   m = mean(x1-x2);
   s = std(x1-x2);
   sm = s/sqrt(n);
   t = m/(sm);
   df = n-1;
else
   n1 = size(x1,1);
   n2 = size(x2,1);
   df =  n1 + n2 -2;
   ss = (var(x1)*(n1-1)+var(x2)*(n2-1))/df;
   sm = sqrt(ss * ((n1+n2)/(n1*n2)) );
   t = (mean(x1) - mean(x2))/sm;
end
if (TestType == 1)
   tc = tinv(1-alpha, df);
   p = 1-tcdf(abs(t),df);
else
   tc = tinv(1-alpha/2, df);
   p = 2*(1-tcdf(abs(t),df));
end
hh = abs(t)  > tc ;
if (Pairing)
   E1 = tc * (std(x1)/sqrt(n)) ;
   E2 = tc * (std(x2)/sqrt(n));
   E = tc * (s/sqrt(n)); % Careful. This is the confidence interval on the mean of the difference
else
   E1 = 0;
   E2 = 0;
end
if (PrintFlag)
%    fprintf('\n %s ',PairingString);
   if (TestType == 2)
       fprintf('Two-sided t-test (p1 n.e. p2): %d (t = %f, tc = %f dof = %d)\n',[hh t tc df]');
   else
       fprintf('One-sided t-test (p1 > or < p2): %d (t = %f, tc = %f dof = %d)\n',[hh t tc df]');
   end
end











