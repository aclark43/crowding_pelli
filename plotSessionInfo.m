function [offsetSessions] = plotSessionInfo(subjectsAll,group,c,control)
for ii = 1:length(subjectsAll)
    offsetSessions.ses1{ii} = load(sprintf...
        ('MATFiles/%s_Uncrowded_Unstabilized_Drift_MS_0ecc_Threshold_Fixation_ses1', ...
        subjectsAll{ii}));
    offsetSessions.ses2{ii} = load(sprintf...
        ('MATFiles/%s_Uncrowded_Unstabilized_Drift_MS_0ecc_Threshold_Fixation_ses2', ...
        subjectsAll{ii}));
    offsetSessions.ses3{ii} = load(sprintf...
        ('MATFiles/%s_Uncrowded_Unstabilized_Drift_MS_0ecc_Threshold_Fixation_ses3', ...
        subjectsAll{ii}));
end

%% create ind session heat maps again
for ii = 1:length(subjectsAll)
    
    for offNum = 1:3
        sesName = sprintf('ses%i',offNum);
        temp = offsetSessions.(sesName){ii};
        xValues = [];
        yValues = [];
        result = [];

        for i = 1:length(temp.em.ecc_0.fixation.position)
            tempx = temp.em.ecc_0.fixation.position(i).x;
            tempy = temp.em.ecc_0.fixation.position(i).y;
            
            xValues = [xValues tempx];
            yValues = [yValues tempy];
        end
        [xValues, yValues] = cleanXYTrialsFromNans(xValues, yValues, 200);
        
        xValuesNew = xValues(xValues < 60 & xValues > -60 &...
            yValues < 60 & yValues > -60);
        yValuesNew = yValues(yValues < 60 & yValues > -60 &...
            xValues < 60 & xValues > -60);
        
       [height,width] = size(xValuesNew);
        if height>width
            xValuesNew = xValuesNew';
            yValuesNew = yValuesNew';
        end
        
        binNum = 30;
        limit.xmin = floor(min(xValuesNew));
        limit.xmax = ceil(max(xValuesNew));
        limit.ymin = floor(min(yValuesNew));
        limit.ymax = ceil(max(yValuesNew));
        
        result1 = MyHistogram2(xValuesNew, yValuesNew, [limit.xmin,limit.xmax,binNum;limit.ymin,limit.ymax,binNum]);
        result = result1./(max(max(result1)));
        
        x_arcmin = linspace(limit.xmin, limit.xmax, size(result, 1)); % these are the pretend arcmin corresponding to the pdf
        y_arcmin = linspace(limit.ymin, limit.ymax, size(result, 1));
        
        [~, idx] = max(result(:)); % find the max
        
        [row, col] = ind2sub(size(result), idx); % convert to row and col
        
        x_pk = x_arcmin(row); % get row and col in arcmin
        y_pk = y_arcmin(col);
        
        offsetSessions.(sesName){ii}.x_pk = x_pk;
        offsetSessions.(sesName){ii}.y_pk = y_pk;
        offsetSessions.(sesName){ii}.eucDist = hypot(x_pk, y_pk); %sqrt(sum((x_pk - y_pk) .^ 2));
        offsetSessions.(sesName){ii}.eucDist = hypot(x_pk, y_pk);
        offsetSessions.(sesName){ii}.span25 = ...
            quantile(sqrt((xValuesNew-nanmean(xValuesNew)).^2 + (yValuesNew-nanmean(yValuesNew)).^2), .10);
        
          [ offsetSessions.(sesName){ii}.com, ...
             offsetSessions.(sesName){ii}.massX, ...
             offsetSessions.(sesName){ii}.massY, ...
            ~,...
            ~,  offsetSessions.(sesName){ii}.massXSTD, ...
             offsetSessions.(sesName){ii}.massYSTD] ...
            = centerOfMassCalculation(xValuesNew, yValuesNew);
        
        [offsetSessions.(sesName){ii}.row offsetSessions.(sesName){ii}.column]=...
            find(result == (max(max(result))));
        
        axisVal = 60;
% % %         figure('units','normalized','outerposition',[1.2 0.05 .8 .6]) %plots all absolute thresholds for each condition
% % %         subplot(1,3,1)
% % %         pcolor(result')
% % %         hold on
% % %         leg(2) = plot(...
% % %             offsetSessions.(sesName){ii}.row, ...
% % %             offsetSessions.(sesName){ii}.column,...
% % %             'o','Color','m','MarkerSize',10,'LineWidth',2);
% % %         axis square
% % %         
% % %         %         figure;
% % %         subplot(1,3,2)
% % %         generateHeatMapSimple( ...
% % %             xValuesNew, ...
% % %             yValuesNew, ...
% % %             'Bins', binNum,...
% % %             'StimulusSize', 0,...
% % %             'AxisValue', axisVal,...
% % %             'Uncrowded', 4,...
% % %             'Borders', 1);
% % %         hold on
% % %         
% % %         leg(1) = plot(...
% % %             x_pk, ...
% % %             y_pk,...
% % %             'o','Color','k','MarkerSize',10,'LineWidth',2);
% % %         
% % %         subplot(1,3,3)
% % %         plot(xValuesNew,yValuesNew,'.');
% % %         axis square
% % %         hold on
% % %         leg(2) = plot(...
% % %             x_pk, ...
% % %             y_pk,...
% % %             'o','Color','k','MarkerSize',10,'LineWidth',2);
% % %         rectangle('Position',[-8, -8, 16, 16],'LineWidth',2,'LineStyle','-')
% % %         axis([-axisVal axisVal -axisVal axisVal])
% % %         set(gca, 'FontSize', 12)
% % %         axis square
% % %         caxis([0 ceil(max(max(result)))])
% % %         
% % %         saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sHeatMapsWithProbMeasures%s.png', ...
% % %             subjectsAll{ii}, (sesName)));
        
    end
    
end
%%
% for ii = 1:length(subjectsAll)
%     for i = 1:3
%         structData{ii,i} = load(sprintf('MATFiles/%s_Uncrowded_Unstabilized_Drift_MS_0ecc_Threshold_Fixation_ses%i',...
%             subjectsAll{ii},i));
%         allXValues = [];
%         allYValues = [];
%         for t = 1:length(structData{ii,i}.em.ecc_0.fixation.position)
%             %         for t = 1:length(structData{ii,i}.em.x)
%             
%             x = [];
%             y = [];
%             x = structData{ii,i}.em.ecc_0.fixation.position(t).x;
%             y = structData{ii,i}.em.ecc_0.fixation.position(t).y;
%             
%             [nXValues, nYValues] = cleanXYTrialsFromNans(x, y, 200);
%             [trialLevel(i).com(ii,t), ...
%                 trialLevel(i).massX(ii,t), ...
%                 trialLevel(i).massY(ii,t), ...
%                 trialLevel(i).prl(ii,t)] ...
%                 = centerOfMassCalculation(nXValues, nYValues);
%             
%             [h,w] = size(nXValues);
%             if h>w
%                 nXValues = nXValues';
%                 nYValues = nYValues';
%             end
%             
%             allXValues = [allXValues nXValues];
%             allYValues = [allYValues nYValues];
%             
%         end
%         
%         massLevel.stdX(ii,i) = nanstd(allXValues);
%         massLevel.stdY(ii,i) = nanstd(allYValues);
%         
%         [massLevel.com(ii,i), ...
%             massLevel.massX(ii,i), ...
%             massLevel.massY(ii,i), ...
%             massLevel.prl(ii,i)] ...
%             = centerOfMassCalculation(allXValues, allYValues);
%         massLevel.x{ii} = allXValues;
%         massLevel.y{ii} = allYValues;
%     end
% end
% 
% angles.trialLevel = trialLevel;
% angles.massLevel = massLevel;

figure('Position', [10 10 1600 600])
for ii = 1:length(subjectsAll)
    
    if strcmp(group{ii},'Control')
        subplot(1,2,1)
        leg(ii) = plot(1:3,[offsetSessions.ses1{ii}.eucDist,...
            offsetSessions.ses2{ii}.eucDist,...
            offsetSessions.ses3{ii}.eucDist],'-o',...
            'MarkerFaceColor','k','Color','k','MarkerSize',10);
        %         hold on
        %         subplot(1,4,4)
        %         leg2(ii) = plot(1:3,massLevel.com(ii,1:3),'-o',...
        %             'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',10);
        hold on
        title('Control')
        xlabel('Session')
        set(gca,'xtick',[1:3])
     else
        subplot(1,2,2)
        leg(ii) = plot(1:3,[offsetSessions.ses1{ii}.eucDist,...
            offsetSessions.ses2{ii}.eucDist,...
            offsetSessions.ses3{ii}.eucDist],'-o',...
            'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',10);
        %         hold on
        %         subplot(1,4,3)
        %         leg2(ii) = plot(1:3,massLevel.com(ii,1:3),'-o',...
        %             'MarkerFaceColor',c(ii,:),'Color',c(ii,:),'MarkerSize',10);
        hold on
        title('Patient')
        set(gca,'xtick',[1:3])
        xlabel('Session')
    end
    ylim([0 45]);

end
ylabel('Euclidean Distance (arcmin)')
%%%%%%%%%
% subplot(1,2,1)
% hold on
% makeErrorBarPlotsSessions(find(~control),...
%     mean(massLevel.prl(find(~control),:)),...
%     std(massLevel.prl(find(~control),:)),...
%     'Patients','PLF')
%%%%%%%%%
% subplot(1,4,2)
% hold on
% makeErrorBarPlotsSessions(find(control), ...
%     mean(massLevel.prl(find(control),:)),...
%     std(massLevel.prl(find(control),:)),...
%     'Controls','PLF')
% legend(leg,subjectsAll);
% ylim([5 45]);
% 
% %%%%%%%%%%
% subplot(1,4,3)
% hold on
% makeErrorBarPlotsSessions(find(~control),...
%     mean(massLevel.com(find(~control),:)),...
%     std(massLevel.com(find(~control),:)),...
%     'Patients','COM')
% ylim([0 30]);
% %%%%%%%%%
% subplot(1,4,4)
% hold on
% makeErrorBarPlotsSessions(find(control),...
%     mean(massLevel.com(find(control),:)),...
%     std(massLevel.com(find(control),:)),...
%     'Controls','COM')
% ylim([0 30]);

suptitle('Across Sessions, CAT Trials')
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/CenterofMassPRLSessions.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/CenterofMassPRLSessions.png');
saveas(gcf,'../../SummaryDocuments/HuxlinPTFigures/CenterofMassPRLSessions.epsc');
    function makeErrorBarPlotsSessions(idxS,averageAll,stdAll,titleStr,ylabelStr)
        title(titleStr)
        ylabel(ylabelStr)
        xticks([1:3])
        xlabel('session')
        % for i = 1:3
        %     averageAll(i) = mean(y(idxS,i));
        %     stdAll(i) = std(y(idxS,i));
        % end
        hold on
        errorbar(1:3,averageAll,stdAll,'-d',...
            'MarkerFaceColor','k','Color','k','MarkerSize',10);
        % ylim([5 45]);
        xlim([0.75 3.25]);
        
    end
end
