function [ params ] = determineParams( params, currentEm, currentCond )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

switch currentCond
    case 'Crowded'
        if strcmp('Drift_MS',currentEm)
            params.DMS = true;
        elseif strcmp('Drift',currentEm)
            params.D = true;
        elseif strcmp('MS',currentEm)
            params.MS = true;
        end
    case 'Uncrowded'
        params.crowded = false;
        if strcmp('Drift_MS',currentEm)
            params.DMS = true;
        elseif strcmp('Drift',currentEm)
            params.D = true;
        elseif strcmp('MS',currentEm)
            params.MS = true;
        end
end
end

