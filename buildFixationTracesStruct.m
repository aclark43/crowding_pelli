function [ traces, counter, counterDrift, saveIdx ] = buildFixationTracesStruct( i, condNum, params, allTrials, subjectsAll, fixationOnlyTrialsIdx, counter, counterDrift, traces, saveIdx )
%UNTITLED2 Summary of this function goes here
%   condNum = 1 uncrowded, 2 crowded

if condNum == 1
    cond = 'uncr';
elseif condNum ==2
    cond = 'cro';
end

subj = (subjectsAll{i});
% counter = 1;
% counterDrift = 1;
for ii = fixationOnlyTrialsIdx.(subj)(condNum,:) %%Uncrowded
    pptrials = allTrials{i,condNum};
    if ~ii == 0
        timeOn = round(pptrials{ii}.TimeFixationON)+1;
        %length(pptrials{ii}.x.position);
        if strcmp('D',params.machine)
            timeOff = 500/(1000/330);
            pxAngle = pptrials{ii}.pixelAngle;
            traces.(subj).x{counter} = pptrials{ii}.x.position((ceil(timeOn/(1000/330))):timeOff) + pxAngle * pptrials{ii}.xoffset;
            traces.(subj).y{counter} = pptrials{ii}.y.position((ceil(timeOn/(1000/330))):timeOff) + pxAngle * pptrials{ii}.yoffset;
        else
            timeOff = 500;
            pxAngle = pptrials{ii}.pxAngle;
            traces.(subj).x{counter} = pptrials{ii}.x.position(timeOn:timeOff) + pxAngle * pptrials{ii}.xoffset;
            traces.(subj).y{counter} = pptrials{ii}.y.position(timeOn:timeOff) + pxAngle * pptrials{ii}.yoffset;
        end
        counter = counter + 1;
        for driftIdx = 1:length(pptrials{ii}.drifts.start)
            throwOut = 0;
            for msIdx = 1:length(pptrials{ii}.microsaccades.start)
                if ~isempty(pptrials{ii}.microsaccades.start(msIdx))
                    continue;
                elseif pptrials{ii}.microsaccades.start(msIdx) < timeOff
                    throwOut = 1;
                end
            end
            
            for sIdx = 1:length(pptrials{ii}.saccades.start)
                if ~isempty(pptrials{ii}.saccades.start(sIdx))
                    continue;
                elseif pptrials{ii}.saccades.start(sIdx) < timeOff
                    throwOut = 1;
                end
            end
            if throwOut > 0
                break;
            end
            if ~isempty(pptrials{ii}.drifts.start)
                %                     if pptrials{ii}.drifts.start(driftIdx) + pptrials{ii}.drifts.duration(driftIdx) > timeOff
                if pptrials{ii}.drifts.duration(driftIdx) - pptrials{ii}.drifts.start(driftIdx) >= timeOff %%same length as normal trial
                    
                    idxStart = pptrials{ii}.drifts.start(driftIdx);
                    idxEnd = pptrials{ii}.drifts.start(driftIdx) + timeOff;
                    
                    traceX = pptrials{ii}.x.position(idxStart:idxEnd) + ...
                        pxAngle * pptrials{ii}.xoffset;
                    
                    traceY = pptrials{ii}.y.position(idxStart:idxEnd) + ...
                        pxAngle * pptrials{ii}.yoffset;
                    
                    if saveIdx.(subj).(cond)(ii) == 1
%                         figure;
%                         plot(1:length(traceX),traceX)
%                         hold on
%                         plot(1:length(traceY),traceY)
%                         ylim([-20, 20])
%                         title(cond)
%                         cont = input('Include Trial (y/s)','s');
%                         if cont == 's'
%                             saveIdx.(subj).(cond)(ii) = 0;
%                             break;
%                         elseif cont == 'y'
%                             saveIdx.(subj).(cond)(ii) = 1;
                            traces.(subj).xDrift{counterDrift} = traceX;
                            traces.(subj).yDrift{counterDrift} = traceY;
                            
                            counterDrift = counterDrift + 1;
                            break;
%                         end
                    end
                end
            end
        end
        
    end
end
end

