function fH1 = createGraphTogetherMultipleSubj(title,pathToData,color)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

fH1 = openfig(fullfile(pathToData, title));
set(gcf, 'Units', 'Normalized');%,'OuterPosition', [0.1, 0.1, 0.5, 0.7]);

%%Finds the value text to move
h = findobj({'Color','[0.30 0.70 0.50]','-or',...
    'Color','[0.30 0.30 0.50]','-or',...
    'Color','[77 204 179]/256','-or',...
    'Color','[77 204 178]/256','-or',...
    'Color','[0.5 0.5 0.5]'});
k = (axis);
textPosition = k(2) - (k(2)/3);
for ii = 1:length(h)
    delete(h(ii));
end

%%Color of Data Points
colorArray = color;
numColors = size(colorArray,1);

%Getting axes objects
aH = findall(fH1,'Type','axes').';

for ii = aH
    lH = findall(ii,'Type','line').';
    cnt = 0;
    for jj = lH
        idx = mod(cnt,numColors) + 1;
        jj.Color = colorArray(idx,:);
        jj.LineWidth = 5;
        if cnt == 8 || cnt < 3
            jj.Marker = 'none';
        elseif cnt < 8 || cnt >= 9
            jj.Marker = 's';
        end
        cnt = cnt + 1;
    end
end
end

