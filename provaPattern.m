clear all
clc
% close all
% play around with the sizpad to see the effect of a larger or smaller
% padding and with repN (number of repetition of the pattern) to see the
% effect of pattern repetition (e.g. 0 vs. 5)

% %% Plotting Different Stimulus Sizes
% figure
% counter = 1;
% for stimSize = [1.5 3]
%     widthPixel = 10; %size should be multiple of 2
%     pixelAngle = stimSize/widthPixel; % 1.5 arcmin stim width
%     bufferSizeAngle = 2 * 60; % arcmin
%     bufferSizePixel = bufferSizeAngle / pixelAngle;
%     sz = 2 * bufferSizePixel;
% 
%     digit = drawDigit(3, widthPixel);
% 
% 
%     ashleyV1 = padarray(digit-mean(digit(:,size(digit,2)/2)), [bufferSizePixel, bufferSizePixel],0, 'both');
%     [m, n] = size(ashleyV1);
%     df = floor((m - sz)/2);
%     ashleyV1 = ashleyV1(df:(df + sz - 1), :);
%     df = floor((n - sz)/2);
%     ashleyV1 = ashleyV1(:, df:(df + sz - 1));
%     spec = fftshift(fft(ashleyV1(:,(length(ashleyV1)/2)-1)));
%     k2 = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);
% 
%     for rep = [10]
%         repN = rep;
%         digitSlice = digit(:,size(digit,2)/2);
%         sizpad = 1;%size(digit,1)/4;%length(find(digitSlice==1))/4;
%         stim = [ones(1,sizpad)'*0.5; digitSlice; ones(1,sizpad)'*0.5];
%         for ii = 1:repN
%             stim = [stim; stim];
%         end
%         stim = stim-mean(stim);
%         k = ((-size(stim,1)/2 ):(size(stim,1)/2)-1) / size(stim,1) / (pixelAngle/60);
%         %stim = stim(size(stim,1)/2 : end);
%         %k = k(length(k)/2 : end);
%         if stimSize == 1.5
%             color = 'k';
%             counter = 1;
% %             idx = [1 2]
%         else
%             color = 'r';
%             counter = 2;
% %             idx = [3 4]
%         end
% 
% %         subplot(3,2,idx(1))
% figure;
% yyaxis left
% semilogx(k2,pow2db((abs(spec)).^2),'-','Color',color);
% axis([3 100 -25 45])
% ylabel('Single Stimulus (Old Version)')
% hold on
% yyaxis right
% leg(counter) = plot(k,abs(fftshift(fft(stim))),'Color','k')
% axis([3 100 -3000 10000])
% ylabel('10x Stimulus (New Version)')
% xlabel('Spatial Frequency (k)')
% set(gca, 'XTick', [-100:10:100])
% hold on
% %
% % %         subplot(3,2,idx(2))
% %         loglog(k,abs(fftshift(fft(stim))),'Color',color)
% %         xlim([2.5 100])
% %         set(gca, 'XScale', 'log','XTick', [1, 10, 30, 50]);
% %         title(rep)
% %         hold on
% %
% %         temp{counter} = abs(fftshift(fft(stim)))
% %         subplot(3,2,[5 6])
% %         loglog(k,abs(fftshift(fft(stim))),'Color',color)
% %         xlim([2.5 100])
% %         set(gca, 'XScale', 'log','XTick', [1, 10, 30, 50]);
% %         title(rep)
% %         hold on
% 
%     end
% 
% end
% 
%  legend(leg,{'1.5 Stim Size','3 Stim Size'});
%     %     counter = counter + 1;
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerSimsProvaPatternOnTop%i.png', rep));
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerSimsProvaPatternOnTop%i.epsc',rep));
% 
% %     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerSimsProvaPattern%i.png', rep));
% %     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerSimsProvaPattern%i.epsc',rep));

%% Plotting Different Numbers (Same Size Stimulus)
figure('Position', [1000 10 400 900]);
counter = 1;
for stimSize = [1.5]
    widthPixel = 10; %size should be multiple of 2
    pixelAngle = stimSize/widthPixel; % 1.5 arcmin stim width
    bufferSizeAngle = 2 * 60; % arcmin
    bufferSizePixel = bufferSizeAngle / pixelAngle;
    sz = 2 * bufferSizePixel;
    
    for numbers = [3]% 5 6 9]
%         digit = drawDigit(numbers, widthPixel);
        
        %     ashleyV1 = padarray(digit-mean(digit(:,size(digit,2)/2)), [bufferSizePixel, bufferSizePixel],0, 'both');
        %     [m, n] = size(ashleyV1);
        %     df = floor((m - sz)/2);
        %     ashleyV1 = ashleyV1(df:(df + sz - 1), :);
        %     df = floor((n - sz)/2);
        %     ashleyV1 = ashleyV1(:, df:(df + sz - 1));
        %     spec = fftshift(fft(ashleyV1(:,(length(ashleyV1)/2)-1)));
        %     k2 = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);
        grayImage = imread('CaptureD.png');
%         figure;digit = (grayImage(:,:))
% figure;imagesc(digit)
        digit = (grayImage(1:436,1:357));
        numbers = 5;
%         figure;
%         grayImage = imread('CaptureD.png');
        for rep = [1]
            repN = rep;
            if numbers == 3
                color = 'k';
                counter = 1;
                digitSlice = digit(:,size(digit,2)/2);
                %             idx = [1 2]
            elseif numbers == 5
                color = 'r';
                counter = 2;
                digitSlice = digit(:,size(digit,2)/2);
                %             idx = [3 4]
            elseif numbers == 9
                color = 'm';
                counter = 4;
                digitSlice = digit(:,size(digit,2)/2);
            else
                color = 'g';
                counter = 3;
                digitSlice = digit(:,size(digit,2)/2+1);
            end
            
            sizpad = 1;%size(digit,1)/4;%length(find(digitSlice==1))/4;
            stim = [ones(1,sizpad)'*0.5; digitSlice; ones(1,sizpad)'*0.5];
            for ii = 1:repN
                stim = [stim; stim];
            end
            stim = stim-mean(stim);
            k = ((-size(stim,1)/2 ):(size(stim,1)/2)-1) / size(stim,1) / (pixelAngle/60);
            %stim = stim(size(stim,1)/2 : end);
            %k = k(length(k)/2 : end);
            
            
            %         subplot(3,2,idx(1))
            % figure;
            % yyaxis left
            y{counter} = abs(fftshift(fft(stim)));
            
            X2=y{counter};%store the FFT results in another array
            %detect noise (very small numbers (eps)) and ignore them
%             threshold = (abs(X2))/10000; %tolerance threshold
            threshold = (abs(X2))/90000; %tolerance threshold

            X2(abs(y{counter})<threshold) = 0; %maskout values that are below the threshold
            %*180/pi; %phase information multiply by 180/pi to convert to deg
            Phase_spec = (angle(X2'));
            allPhase(counter,:) = Phase_spec;
            allSpec(counter,:) = pow2db(abs(y{counter}).^2);
            
            
            subplot(4,1,counter)
            leg(counter) = plot(k,y{counter},'Color',color);
%             axis([3 100])
            xlim([3 100]);
            ylabel('10x Stimulus (New Version)')
            xlabel('Spatial Frequency (k)')
            set(gca, 'XTick', [-100:10:100])
            set(gca,'Xscale','log')
            hold on
            
        end
    end
    
end

legend(leg,{'3','5','6'});
%% Variance

D1_P = circ_dist2(allPhase(1,:),allPhase(2,:));
D2_P = circ_dist2(allPhase(1,:)-allPhase(3,:));
D3_P = circ_dist2(allPhase(1,:)-allPhase(4,:));
D4_P = circ_dist2(allPhase(2,:)-allPhase(3,:));
D5_P = circ_dist2(allPhase(2,:)-allPhase(4,:));
D6_P = circ_dist2(allPhase(3,:)-allPhase(4,:));
D1_A = allSpec(1,:)-allSpec(2,:);
D2_A = allSpec(1,:)-allSpec(3,:);
D3_A = allSpec(1,:)-allSpec(4,:);
D4_A = allSpec(2,:)-allSpec(3,:);
D5_A = allSpec(2,:)-allSpec(4,:);
D6_A = allSpec(3,:)-allSpec(4,:);
DP_var = var([D1_P;D2_P;D3_P;D4_P;D5_P;D6_P]);
DA_var = var([D1_A;D2_A;D3_A;D4_A;D5_A;D6_A]);
allSpec(allSpec<0)= 0;
M_A = var(allSpec)-mean(allSpec);
M_P = circ_mean(allPhase);
figure;
semilogx(k,(DP_var*10)-mean(DP_var*10), 'r', 'LineWidth', 2)
hold on
semilogx(k,(DA_var/100), 'c', 'LineWidth', 2)
semilogx(k,M_A/10, 'k', 'LineWidth', 2)
xlim([0.5 60])
title('Phase variance difference')
ylim([-10 20])


hold on
DP_var2 = var([D6_P]);
DA_var2 = var([D6_A]);
semilogx(k,(DP_var2*10)-mean(DP_var2*10), 'g', 'LineWidth', 2)


%% Add DC Curves for all trials
dc = [4.868 19.681];
subplot(4,1,4)
critFreq = powerAnalysis_JustCriticalFrequency (dc, 2)
xlim([3 100])
%     counter = counter + 1;
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerSimsProvaPatternOnTop%i.png', rep));
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerSimsProvaPatternOnTop%i.epsc',rep));

%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerSimsProvaPattern%i.png', rep));
%     saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PowerSimsProvaPattern%i.epsc',rep));



function digit = drawDigit(whichDigit, pixelwidth, patterned)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;

switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
        % end
end

%     if exist('patterned','var')
%         digit = [digit digit digit digit digit;
%             digit digit digit digit digit];
%     end
end