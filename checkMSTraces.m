function pptrials = checkMSTraces(trialId, pptrials, params, filepath, singleSegs, nameFile)
% nameFile : string name of how your .mat files are saved(ie: pptrials)
if strcmp('D',params.machine)
    filepath = sprintf('%s/%s',filepath, params.session);
else
    filepath = filepath;
end
%test

if isempty(trialId)
    trialId = 1:length(pptrials);
end

PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    
%     snellen = input('Is this the snellen task? (y/n)','s');
    figure('position',[2300, 100, 1500, 800])
    trialCounter = 1;
    for driftIdx = 1:inf
        
        if (trialCounter) > length(trialId)
            return;
        end
        currentTrialId = trialId(trialCounter);
        hold off
%         figure;
        if ~isempty('params')
            if params.fixation
                if pptrials{currentTrialId}.TimeTargetON > 0
                    continue;
                end
                poiStart = pptrials{currentTrialId}.TimeFixationON;
                poiEnd = min(pptrials{currentTrialId}.TimeFixationOFF);
                axisWindow = 60;
            else
                if pptrials{currentTrialId}.TimeTargetOFF <= 0
                    continue;
                end
                poiStart = pptrials{currentTrialId}.TimeTargetON;
                poiEnd = min(pptrials{currentTrialId}.TimeTargetOFF);
                axisWindow = 35;
            end
        else
%             if snellen == 'y'
%                 poiStart = pptrials{currentTrialId}.timeRampStop;
%                 poiEnd = min(pptrials{currentTrialId}.ResponseTimeList5);
%                 axisWindow = 120;
%                 pptrials{currentTrialId}.xoffset = pptrials{currentTrialId}.xoffset1;
%                 pptrials{currentTrialId}.yoffset = pptrials{currentTrialId}.yoffset1;
%                 pptrials{currentTrialId}.pxAngle = pptrials{currentTrialId}.pixelAngle;
%                 params.machine = 'D';
%             else
%                 nameStart = input('when do you want trace to start?','s');
%                 nameStop =  input('when do you want trace to stop?','s');
                poiStart = pptrials{currentTrialId}.TimeFixationON;
                poiEnd = min(pptrials{currentTrialId}.TimeFixationOFF);
                axisWindow = 35;
%             end
        end
        fprintf('\n%s,trial %i of loop %i\n',pptrials{currentTrialId}.ses, pptrials{currentTrialId}.id, driftIdx);
        poi = fill([poiStart, poiStart ...
            poiEnd, poiEnd], ...
            [-50, 50, 50, -50], ...
            'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0);
        
        xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pxAngle;
        yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pxAngle;
        
        hold on
        if strcmp('D',params.machine)
            hx = plot(1:(1000/330):length(xTrace)*(1000/330),xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
            hy = plot(1:(1000/330):length(yTrace)*(1000/330),yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
            axis([0 180 0 150])
        else
            hx = plot(xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
            hy = plot(yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
            axis([0 180 0 150])
        end
        axis([poiStart - 400, poiEnd + 400, -axisWindow, axisWindow]) 
%         xticks([poiStart - 400:50:poiEnd + 400])
%         xticklabels(round([poiStart - 400:50:poiEnd + 400]))
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        title(sprintf('Trial: %i', currentTrialId)); %
        
        set(gca, 'FontSize', 12)
        poiMS = [1 0 0];
        poiS = [1 0 0];
        poiD = [0 1 0];
        poiN = [0 0 0];
        poiI = [0 .423 .521];
        poiB = [.749 .019 1];
        if strcmp('D',params.machine)
            convert = (1000/330);
        else
            convert = 1;
        end
            poiMS = plotColorsOnTraces(pptrials, currentTrialId, 'microsaccades', [1 0 0],convert); %red
            poiS = plotColorsOnTraces(pptrials, currentTrialId, 'saccades', [1 0 0],convert); %red
            poiD = plotColorsOnTraces(pptrials, currentTrialId, 'drifts', [0 1 0],convert); %green
            poiN = plotColorsOnTraces(pptrials, currentTrialId, 'notracks', [0 0 0],convert); %black
            poiI = plotColorsOnTraces(pptrials, currentTrialId, 'invalid', [0 0 1],convert); %blue
            poiB = plotColorsOnTraces(pptrials, currentTrialId, 'blinks', [.749 .019 1],convert); %pink
%         else
%             for i = 1:length(pptrials{currentTrialId}.microsaccades.start)
%                 msStartTime = pptrials{currentTrialId}.microsaccades.start(i);
%                 msDurationTime = pptrials{currentTrialId}.microsaccades.duration(i);
%                 poiMS = fill([msStartTime, msStartTime ...
%                     msStartTime + msDurationTime, ...
%                     msStartTime + msDurationTime], ...
%                     [-35, 35, 35, -35], ...
%                     'r', 'EdgeColor', 'r', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
%             end
%         end
%         if ~isempty(poiMS) || ~isempty(poiS) || ~isempty(poiD) || ~isempty(poiI)
% figure;
%             legend([hx, hy, poiMS, poiD, poiN, poiI, poiB], ...
%                 {'X','Y','MS/S','D','NoTrack','Invalid','Blink'},'FontWeight','bold')
%         end

contType = input...
    ('Saccade(1), Microsaccade(2), Drift(3), \n No Track(4), Invalid(5), Blinks(6), \n Back a trial(7), Stop(0), AutoPrune(8), Next Trial(enter)?\n');
        cont = [];
        if contType == 1
            cont = input('Tag a Saccade(a) or Change Saccade Labelled(b)?','s');
            em = 'saccades'; % Post analysis will check amps and organize as MS or S
        elseif contType == 2
            cont = input('Tag a microsaccade(a) or Change Microsaccade Labelled(b)?','s');
            em = 'microsaccades'; % +- 10msec
        elseif contType == 3
            cont = input('Tag a Drift(a) or Change Drift Labelled(b)?','s');
            em = 'drifts'; 
        elseif contType == 4
            cont = input('Tag a No Track(a) or Change no track Labelling?(b)','s');
            em = 'notracks'; %flat no tracks
        elseif contType == 5
            cont = input('Tag a Invalid(a) or Change invalid Labelling?(b)','s');
            em = 'invalid'; %bad drifts
        elseif contType == 6
            cont = input('Tag a Blink(a) or Change a Blink Labelling?(b)','s');
            em = 'blinks';
        elseif contType == 7
            cont = 'z';
        elseif contType == 0
            cont = 's';
        elseif contType == 8
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 50 );
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 8);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 1000);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 1000);
            pptrials = autoPrune( pptrials, currentTrialId, 'notracks', 1000);
            continue;
        else
            trialCounter = trialCounter + 1;
            continue;
        end
        
        if cont == 's'
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            break;
        elseif cont == 'a'
            numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;%input('Which ones (in order) do you want to tag?');
            startTime = input('What is the start time?');
            duration = input('What is the end time?');
            if strcmp('D',params.machine)
                pptrials{currentTrialId}.(em).start(numTaggedInTrace) = round(startTime/(1000/330));
                pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = round((duration/(1000/330)) - round(startTime/(1000/330)));
            else
                pptrials{currentTrialId}.(em).start(numTaggedInTrace) = round(startTime);
                pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = round((duration) - round(startTime));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'b'
            fprintf('Total start times: %.0f \n', round(pptrials{currentTrialId}.(em).start, 3)*(1000/330))
            startTime = input('When does the wrong EM start?');
            newStartTime = input('When does the EM actually start? (set to 0 if not present)');
            newDurationTime = input('When does the EM end? (set to 0 if not present)');
            for numW = 1:length(startTime)
                wrong = find((pptrials{currentTrialId}.(em).start) == round(startTime(numW)/(1000/330)));
                pptrials{currentTrialId}.(em).start(wrong) = round(newStartTime(numW)/(1000/330));
                pptrials{currentTrialId}.(em).duration(wrong) = ...
                    (round(newDurationTime(numW)/(1000/330))) - round(newStartTime(numW)/(1000/330));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'z'
            trialCounter = trialCounter-1;
            continue;
        end
        save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
        trialCounter = trialCounter + 1;
    end
end

close

end
