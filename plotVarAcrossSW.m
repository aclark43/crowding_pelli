function [leg, p] = plotVarAcrossSW (varVector, varCell, params)

[~,p] = ttest(varVector(1,:),varVector(2,:));
uMean = mean(varVector(1,:));
uSD = std(varVector(1,:));

cMean = mean(varVector(2,:));
cSD = std(varVector(2,:));

figure;
subplot(1,2,2)
for ii = 1:params.subNum
    leg(ii) = plot(cell2mat(varCell{ii}.Cro),'-o');
    hold on;
end

subplot(1,2,1);
for ii = 1:params.subNum
    plot(cell2mat(varCell{ii}.Unc),'-o');
    hold on;
end


end