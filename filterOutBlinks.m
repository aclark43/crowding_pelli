function [ data ] = filterOutBlinks( data )
%Takes all blinks and turns them into no tracks
%   Hacky way of resolving the issue where the entire trace is thrown out
%   due to too many blink no tracks - which is only happening because they
%   are quickly flittering back and forth during a trial.

for ii = 1:length(data.triggers)
%     for blinkIdx = 1:length(data.triggers{ii}.blink)
        blinksIdx = find(data.triggers{ii}.blink);
        notracksIdx = find(data.triggers{ii}.notrack);
        
      if ~isempty(blinksIdx)
         for i = 1:length(blinksIdx)
            data.triggers{ii}.notrack(blinksIdx(i)) = 1;
            data.triggers{ii}.blink(blinksIdx(i)) = 0;
         end
      end
     fullNoTracksIdx = find(data.triggers{ii}.notrack);
     
     for i = 1:length(data.x(ii))
         difNoTracks = diff(cell2mat(data.x(ii)));
         idxDifNT = find(difNoTracks == 0);
         for iii = 1:length(idxDifNT)
             data.triggers{ii}.notrack(idxDifNT(iii)) = 1;
         end
     end
end

%      for i = 1:length(notrackIdx)
%     numNoTracks = length(fullNoTracksIdx);
    
%          for iii = 1:numNoTracks-1
%              if iii > 1
%                  if data.triggers{ii}.notrack(fullNoTracksIdx(iii))
%                      data.triggers{ii}.notrack(fullNoTracksIdx(iii)+1) = 1;
%                      data.triggers{ii}.notrack(fullNoTracksIdx(iii)-1) = 1;
%                  end
%              else
%                  data.triggers{ii}.notrack(fullNoTracksIdx(iii)+1) = 1;
%              end
%          end
%      end
     
% if notrack(ii) == 1 && notrack(ii-1

%      for iii = 1:length(fullNoTracksIdx)
%          if fullNoTracksIdx(iii+1) 
%              if fullNoTracksIdx(iii+2) || fullNoTracksIdx(iii)
%                  fullNoTracksIdx(iii+1) = 1;
%                  fullNoTracksIdx(iii-1) = 1;
%              end
%          end
%      end

end

