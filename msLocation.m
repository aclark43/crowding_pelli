function msLocation(MS, params, sw, trialChar, strokeWidth, ...
    uEcc, title_str, msRate, FIXATION_ANALYSIS)

%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

eccentricity = sprintf('ecc_%i', abs(uEcc));
subplot(1,3,1);
counter = 0;
for ii = 1:length(MS.(eccentricity).(strokeWidth))
startLoc = scatter(MS.(eccentricity).(strokeWidth)(ii).startLocationMSx,...
    MS.(eccentricity).(strokeWidth)(ii).startLocationMSy,...
    'MarkerEdgeColor',[0 .5 .5],'MarkerFaceColor',[0 .7 .7],'LineWidth',1.5);
hold on
counter = counter + 1;
end

hold on
for ii = 1:length(MS.(eccentricity).(strokeWidth))
    endLoc = scatter(MS.(eccentricity).(strokeWidth)(ii).endLocationMSx,...
    MS.(eccentricity).(strokeWidth)(ii).endLocationMSy,...
    'MarkerEdgeColor',[102/255 0/255 51/255],'LineWidth',1.5);
hold on
end

if FIXATION_ANALYSIS
    if strcmp('D',params.machine)
        plot(0,0,'-ks','MarkerSize',20*params.pixelAngle);
        graphTitleLine1 = ('Fixation_Trial_Drift_Heat_Map');
    else
        plot(0,0,'-ks','MarkerSize',16*params.pixelAngle);
        graphTitleLine1 = ('Fixation_Trial_Drift_Heat_Map');
    end
else
    if sw == 0
        allSW = extractfield(MS.(eccentricity).(strokeWidth),'sw');
        uAllSW = unique(allSW);
        for ii = 1:length(uAllSW)
            num(ii) = string(regexp(uAllSW(ii), '\d+', 'match'));
        end
        stimuliSize = str2double(num) * params.pixelAngle;
    else
        stimuliSize = sw * params.pixelAngle;
    end
    for ii = 1:length(stimuliSize)
        if params.crowded == true
%             rectangle('Position',[-(2*stimuliSize(ii)+(2*stimuliSize*1.4))+uEcc, -stimuliSize*5, 2*stimuliSize, 10*stimuliSize],'LineWidth',1)
%             rectangle('Position',[(2*stimuliSize*1.4)+arcminEcc, -stimuliSize*5, 2*stimuliSize, 10*stimuliSize], 'LineWidth',1)
%             rectangle('Position',[(-stimuliSize+arcminEcc), -(3*stimuliSize+(10*stimuliSize*1.4)), (2*stimuliSize), (10*stimuliSize)], 'LineWidth',1)
%             rectangle('Position',[(-stimuliSize+arcminEcc), ((10*stimuliSize*1.4)-7*stimuliSize), (2*stimuliSize), (10*stimuliSize)], 'LineWidth',1)
            rectangle('Position',[-(2*stimuliSize(ii)+(2*stimuliSize(ii)*1.4))+uEcc -stimuliSize(ii)*5 2*stimuliSize(ii) 10*stimuliSize(ii)],'LineWidth',.5)
            rectangle('Position',[(2*stimuliSize(ii)*1.4)+uEcc -stimuliSize(ii)*5 2*stimuliSize(ii) 10*stimuliSize(ii)], 'LineWidth',.5)
            rectangle('Position',[(-stimuliSize(ii)+uEcc) -(3*stimuliSize(ii)+(10*stimuliSize(ii)*1.4)) (2*stimuliSize(ii)) (10*stimuliSize(ii))], 'LineWidth',.5)
            rectangle('Position',[(-stimuliSize(ii)+uEcc) ((10*stimuliSize(ii)*1.4)-7*stimuliSize(ii)) (2*stimuliSize(ii)) (10*stimuliSize(ii))], 'LineWidth',.5)
        end
        rectangle('Position',[(-stimuliSize(ii)+uEcc) (-stimuliSize(ii)*5) (2*stimuliSize(ii)) (10*stimuliSize(ii))],'LineWidth',.5)
        hold on
        if uEcc > 0
            plot(0,0,'-ks','MarkerSize',16*params.pixelAngle);
        end
    end
end
hold off
title('MS Start & End');

% n = extractfield(MS.(eccentricity).(strokeWidth),'id');
n = counter;
% number = numel(n);
text (-20,-20,(sprintf('N=%d', n)));

axisValue = 30;
axis tight
axis square
axis([-axisValue axisValue -axisValue axisValue]);
legend([startLoc endLoc], ...
    'Start Location','End Location');
xlabel('X [arcmin]')
ylabel('Y [arcmin]')

%% %Plots the #of MS in a given direction/Angle
microsaccadeDirections = (extractfield(MS.(eccentricity).(strokeWidth),'MSangle'));
c = linspace(0, 2*pi, 30);
n1 = hist(microsaccadeDirections, c); % histogram of saccade directions
n1 = n1/ sum(n1); % normalize for empirical probability distribution
subplot(1,3,2);
h1p = polar([c, c(1)], [n1, n1(1)]);  % repeat first point so that it goes all the way around
title({'MS Direction','(# of MS per Direction)'})
set(gcf, 'Position',  [600, 300, 1200, 700])


%% %Plot a HeatMap of Angle and Amplitude (using end-start)

%% %Histogram of #MS by Amplitude
subplot(1,3,3);
microsaccadeAmlpitude = (extractfield(MS.(eccentricity).(strokeWidth),'MSamplitude'));
averageMSAmp = mean(extractfield(MS.(eccentricity).(strokeWidth),'MSamplitude'));
averageMSAmpTitle = sprintf('Mean = %.3f', averageMSAmp);
stdMSAmp = std(extractfield(MS.(eccentricity).(strokeWidth),'MSamplitude'));
stdMSAmpTitle = sprintf('STD %s %.3f', char(177), stdMSAmp);

histogram(microsaccadeAmlpitude,50,'FaceColor','r');
xlabel('Amplitude(arcmin)')
ylabel('Number of MS')
title({'MS Amplitude',averageMSAmpTitle, stdMSAmpTitle});
axis square

averageMSRate = mean(msRate);

% graphTitleLine1 = sprintf('Strokewidth %i',(sw));
if FIXATION_ANALYSIS
    sizeStimuli = 'Fixation';
else
    sizeStimuli = str2num(sprintf('%.3f',sw*params.pixelAngle));
end
graphTitleLine1 = sprintf('Size %s', sizeStimuli);
oldgraphTitleLine2 = sprintf('%s', (title_str));
newgraphTitleLine2 = strrep(oldgraphTitleLine2,'_',' ');
graphTitleLine3 = sprintf('Average MS Rate = %s', averageMSRate);
suptitle({graphTitleLine1,newgraphTitleLine2,graphTitleLine3})

filepath = 'C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/';

saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/IMAGE/%s.png',...
    filepath, trialChar.Subject, sprintf('%s_MS_Direction_&_Amplitude_Analysis_for_Size_%i_%s',trialChar.Subject,sw*params.pixelAngle,title_str)));
saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/%s.epsc',...
    filepath, trialChar.Subject, sprintf('%s_MS_Direction_&_Amplitude_Analysis_for_Size_%i_%s',trialChar.Subject,sw*params.pixelAngle,title_str)));
saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/FIG/%s.fig',...
    filepath, trialChar.Subject, sprintf('%s_MS_Direction_&_Amplitude_Analysis_for_Size_%i_%s',trialChar.Subject,sw*params.pixelAngle,title_str)));
%%
% figure;





%%
% figure
% scatter(extractfield(MS.(eccentricity).(strokeWidth),'endLocationMSx'),...
%     extractfield(MS.(eccentricity).(strokeWidth),'endLocationMSy'),'MarkerFaceColor',[255/255 153/255 255/255],...
%     'MarkerEdgeColor',[102/255 0/255 51/255],'LineWidth',1.5);
% 
% if isfield(MS.(eccentricity).(strokeWidth),'partialEndLocationMSx')
%     %     function [plotEndX, plotEndY] = extractMSLandingPositions(MS)
%     partEndLocationMSx = [extractfield(MS.(eccentricity).(strokeWidth),'partialEndLocationMSx')];
%     plotEndX = cell2mat(partEndLocationMSx(~cellfun('isempty',partEndLocationMSx)));
%     partEndLocationMSy = [extractfield(MS.(eccentricity).(strokeWidth),'partialEndLocationMSy')];
%     plotEndY = cell2mat(partEndLocationMSy(~cellfun('isempty',partEndLocationMSy)));
%     numeFirstLand = length(plotEndX);
% 
% hold on
% scatter(plotEndX,plotEndY,75,'MarkerFaceColor',[55/255 153/255 255/255],...
%     'MarkerEdgeColor',[77/255 0/255 51/255],'LineWidth',1.5);
% else
%     plotEndX = 0;
%     numeFirstLand = 0;
% end
% if FIXATION_ANALYSIS
%     if strcmp('D',params.machine)
%         plot(0,0,'-ks','MarkerSize',20*params.pixelAngle);
%         graphTitleLine1 = ('Fixation_Trial_Drift_Heat_Map');
%     else
%         plot(0,0,'-ks','MarkerSize',16*params.pixelAngle);
%         graphTitleLine1 = ('Fixation_Trial_Drift_Heat_Map');
%     end
% else
%     if sw == 0
%         allSW = extractfield(MS.(eccentricity).(strokeWidth),'sw');
%         uAllSW = unique(allSW);
%         for ii = 1:length(uAllSW)
%             num(ii) = string(regexp(uAllSW(ii), '\d+', 'match'));
%         end
%         stimuliSize = str2double(num) * params.pixelAngle;
%     else
%         stimuliSize = sw * params.pixelAngle;
%     end
%     for ii = 1:length(stimuliSize)
%         if params.crowded == true
%             rectangle('Position',[-(2*stimuliSize(ii)+(2*stimuliSize(ii)*1.4)) -stimuliSize(ii)*5 2*stimuliSize(ii) 10*stimuliSize(ii)],'LineWidth',.5)
%             rectangle('Position',[(2*stimuliSize(ii)*1.4) -stimuliSize(ii)*5 2*stimuliSize(ii) 10*stimuliSize(ii)], 'LineWidth',.5)
%             rectangle('Position',[(-stimuliSize(ii)) -(3*stimuliSize(ii)+(10*stimuliSize(ii)*1.4)) (2*stimuliSize(ii)) (10*stimuliSize(ii))], 'LineWidth',.5)
%             rectangle('Position',[(-stimuliSize(ii)) ((10*stimuliSize(ii)*1.4)-7*stimuliSize(ii)) (2*stimuliSize(ii)) (10*stimuliSize(ii))], 'LineWidth',.5)
%         end
%         rectangle('Position',[(-stimuliSize(ii)) (-stimuliSize(ii)*5) (2*stimuliSize(ii)) (10*stimuliSize(ii))],'LineWidth',.5)
%         hold on
%     end
% end
% hold off
% title('MicroSaccade End Locations');
% 
% n = extractfield(MS.(eccentricity).(strokeWidth),'id');
% number = numel(n)-length(plotEndX);
% 
% text (-20,-20,(sprintf('N=%d + %d(partial MS)', number, numeFirstLand)));
% 
% axisValue = 30;
% axis tight
% axis square
% axis([-axisValue axisValue -axisValue axisValue]);
% legend ('End - MS During Trial','End - MS Before Target Presentation');
% xlabel('X [arcmin]')
% ylabel('Y [arcmin]')
% saveas(gcf, sprintf('%s/Data/%s/Graphs/MSAnalysis/IMAGE/%s.png',...
%     filepath, trialChar.Subject, sprintf('%s_MS_Landing_for_Size_%i_%s',trialChar.Subject,sw*params.pixelAngle,title_str)));
% 
% close all
end


