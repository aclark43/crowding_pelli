function plotAreaSummaries(params,distAllTabUnc,distAllTabCro)
countC = 1;
countU = 1;
for ii = 1:params.subNum
    counter = 1;
    for i = 1:length(distAllTabUnc)
        if distAllTabUnc(i).subject == ii
            areaSummaries{ii}.Unc{counter} = distAllTabUnc(i).area;
            allAreas(1,countU) = distAllTabUnc(i).area;
            allDC(1,countU) = distAllTabUnc(i).dCoefDsq;
            prlEdgeSummaries{ii}.Unc{counter} = distAllTabUnc(i).PRLDistanceFromEdge;
            allPRLEdge(1,countU) = distAllTabUnc(i).PRLDistanceFromEdge;
            prlSummaries{ii}.Unc{counter} = distAllTabUnc(i).meanPRLDistance;
            allPRL(1,countU) = distAllTabUnc(i).meanPRLDistance;
            counter = 1 + counter;
            countU = countU + 1 ;
        end
    end
    counter = 1;
    for i = 1:length(distAllTabCro)
        if distAllTabCro(i).subject == ii
            areaSummaries{ii}.Cro{counter} = distAllTabCro(i).area;
            allAreas(2,countC) = distAllTabCro(i).area;
            allDC(2,countU) = distAllTabCro(i).dCoefDsq;
            prlEdgeSummaries{ii}.Cro{counter} = distAllTabCro(i).PRLDistanceFromEdge;
            allPRLEdge(2,countC) = distAllTabCro(i).PRLDistanceFromEdge;
            prlSummaries{ii}.Cro{counter} = distAllTabCro(i).meanPRLDistance;
            allPRL(2,countC) = distAllTabCro(i).meanPRLDistance;
            counter = 1 + counter;
            countC = countC + 1 ;
        end
    end
end
%% PRL Edge
leg = plotVarAcrossSW (allPRLEdge, prlEdgeSummaries, params);

legend(leg, params.subjectsAll);
title('Uncrowded');
xlabel('PRL From Edge of Target')
%% PRL Center
leg = plotVarAcrossSW (allPRL, prlSummaries, params);

legend(leg, params.subjectsAll,'Location','southeast');
title('Uncrowded');
ylabel('PRL From Center of Target')
xlabel('Sample Num')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllPLFsBySW.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllPLFsBySW.epsc');

%%
leg = plotVarAcrossSW (allAreas, areaSummaries, params);

legend(leg, params.subjectsAll);
title('Uncrowded');
xlabel('Area')
leg = plotVarAcrossSW (allAreas, areaSummaries, params);
%%
leg = plotVarAcrossSW (allDC, areaSummaries, params);

legend(leg, params.subjectsAll);
title('Uncrowded');
xlabel('DC')
end
