function [Drifts,xValues,yValues] = ...
    driftAnalysis(pptrials, Drifts, params, driftIdx, sw, ...
    uEcc, title_str, trialChar, ~, figures, stimulusSize)


%%%Generate Drift Analysis 

%drift characteristics
counter = 1;
strokeWidth = sprintf('strokeWidth_%i', sw);
% eccentricity = params.ecc;
% printEcc = abs(round(ceil(uEcc/5)*5));
% if uEcc > 0
%     eccentricity = sprintf('ecc_%i_Temp', printEcc);
% elseif uEcc < 0
%     eccentricity = sprintf('ecc_%i_Nasal', printEcc);
if uEcc == 0
    eccentricity = sprintf('ecc_%i', uEcc);
else
    eccentricity = ('ecc');
end

for ii = 1:length(pptrials)
    if strcmp(params.machine,'A')
        pptrials{ii}.pixelAngle = pptrials{ii}.pxAngle;
    end
end
% end

xValues = [];
yValues = [];
for ii = driftIdx %by trial
    if figures.FIXATION_ANALYSIS%%%%CHANGE FOR FIXATION TRIALS
        [ Drifts, xValues, yValues ] = buildDriftFixationStruct(...
            sw, driftIdx, Drifts, params, pptrials, counter, ...
            'fixation', eccentricity, xValues, yValues);
    else
        
        if params.D
            if strcmp('D',params.machine)
                timeOn = round(pptrials{ii}.TimeTargetON/(1000/330));
                timeOff = floor(min(pptrials{ii}.TimeTargetOFF/(1000/330), pptrials{ii}.ResponseTime/(1000/330)));
                
            elseif strcmp('A',params.machine)
                timeOn = round(pptrials{ii}.TimeTargetON);
                timeOff = round(min(pptrials{ii}.TimeTargetOFF, pptrials{ii}.ResponseTime));
                pptrials{ii}.pixelAngle = pptrials{ii}.pxAngle;
            end
            Drifts.(eccentricity).(strokeWidth).pixelAngle(counter) =  pptrials{ii}.pixelAngle;
            
%             Drifts.(eccentricity).(strokeWidth).position(counter).x = ...
%                 pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
            Drifts.(eccentricity).(strokeWidth).position(counter).x = ...
                pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.xoffset * pptrials{ii}.pixelAngle;
            
            xValues = [xValues, Drifts.(eccentricity).(strokeWidth).position(counter).x];
            
%             Drifts.(eccentricity).(strokeWidth).position(counter).y = ...
%                 pptrials{ii}.y.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
            Drifts.(eccentricity).(strokeWidth).position(counter).y = ...
                pptrials{ii}.y.position(timeOn:timeOff) +  pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
            
            yValues = [yValues, Drifts.(eccentricity).(strokeWidth).position(counter).y];
            
        elseif params.DMS
            if strcmp('D',params.machine)
                timeOn = round(pptrials{ii}.TimeTargetON/(1000/330));
                timeOff = floor((pptrials{ii}.TimeTargetON+300)/(1000/330));
            else
                timeOn = round(pptrials{ii}.TimeTargetON);
                timeOff = round(min((pptrials{ii}.TimeTargetON+300)));
                pptrials{ii}.pixelAngle = pptrials{ii}.pxAngle;
            end
            
            Drifts.(eccentricity).(strokeWidth).pixelAngle(ii) =  pptrials{ii}.pixelAngle;
            
%             Drifts.(eccentricity).(strokeWidth).positionWholeTrace(counter).x = ...
%                 pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
            Drifts.(eccentricity).(strokeWidth).positionWholeTrace(counter).x = ...
                pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.xoffset * pptrials{ii}.pixelAngle;
            
            xValues = [xValues, Drifts.(eccentricity).(strokeWidth).positionWholeTrace(counter).x];
            
            Drifts.(eccentricity).(strokeWidth).positionWholeTrace(counter).y = ...
                pptrials{ii}.y.position(timeOn:timeOff) + pptrials{ii}.yoffset * pptrials{ii}.pixelAngle;
            
            yValues = [yValues, Drifts.(eccentricity).(strokeWidth).positionWholeTrace(counter).y];
            
            msDuringBeginning = [];
            msInTrial = [];
            
            if ~isempty(length(pptrials{ii}.microsaccades.start))
                for i = 1:length(pptrials{ii}.microsaccades.start)
                    msStartTime = pptrials{ii}.microsaccades.start(i);
                    msDurationTime = pptrials{ii}.microsaccades.duration(i);
                    if (timeOn <= msStartTime) && ...%%MS happening in beginning
                            (timeOff >= msStartTime)
                        msDuringBeginning(i) = 1;
                    else
                        msDuringBeginning(i) = 0;
                    end
                end
            end
            if isempty(length(pptrials{ii}.microsaccades.start)) || sum(msDuringBeginning) == 0
                Drifts.(eccentricity).(strokeWidth).position(counter).x = ...
                    pptrials{ii}.x.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.xoffset;
                
                xValues = [xValues, Drifts.(eccentricity).(strokeWidth).position(counter).x];
                
                Drifts.(eccentricity).(strokeWidth).position(counter).y = ...
                    pptrials{ii}.y.position(timeOn:timeOff) + pptrials{ii}.pixelAngle * pptrials{ii}.yoffset;
                
                yValues = [yValues, Drifts.(eccentricity).(strokeWidth).position(counter).y];
                
%                 continue;
            end
        end
        
        Drifts.(eccentricity).(strokeWidth).id(counter) = ii;
        
        %         xtemp = Drifts.(eccentricity).(strokeWidth).position(counter).x;
        %         ytemp = Drifts.(eccentricity).(strokeWidth).position(counter).y;
        
        Drifts.(eccentricity).(strokeWidth).span(counter) = ...
            Drifts.span(ii);
        Drifts.(eccentricity).(strokeWidth).flankers(counter) = pptrials{ii}.FlankerOrientations;
        Drifts.(eccentricity).(strokeWidth).response(counter) = pptrials{ii}.Response;
        Drifts.(eccentricity).(strokeWidth).responseTime(counter) = pptrials{ii}.ResponseTime;
        Drifts.(eccentricity).(strokeWidth).target(counter) = pptrials{ii}.TargetOrientation;
        %             quantile(sqrt((xtemp-mean(xtemp)).^2 + (ytemp-mean(ytemp)).^2), .95);
        
        Drifts.(eccentricity).(strokeWidth).spanWholeTrace(counter) = Drifts.span(ii);
        Drifts.(eccentricity).(strokeWidth).amplitude(counter) = sqrt((pptrials{ii}.x.position(timeOff) - pptrials{ii}.x.position(timeOn))^2 + ...
            (pptrials{ii}.y.position(timeOff) - pptrials{ii}.y.position(timeOn))^2);
        Drifts.(eccentricity).(strokeWidth).correct(counter) =...
            double(pptrials{ii}.Correct);
        
        counter = counter + 1;
        Drifts.(eccentricity).(strokeWidth).time(counter) = mean(pptrials{ii}.TimeTargetOFF-pptrials{ii}.TimeTargetON);
    end
end

if figures.FIXATION_ANALYSIS
    
%     Drifts.(eccentricity).fixation.meanAmplitude = mean(Drifts.(eccentricity).(strokeWidth).amplitude);
%     Drifts.(eccentricity).fixation.stdAmplitude = std(Drifts.(eccentricity).(strokeWidth).amplitude)/...
%         sqrt(length(Drifts.(eccentricity).(strokeWidth).amplitude));
    Drifts.(eccentricity).fixation.span = Drifts.span;
    Drifts.(eccentricity).fixation.meanSpan = mean(Drifts.span);
    Drifts.(eccentricity).fixation.stdSpan = std(Drifts.span)/sqrt(length(Drifts.span));
    Drifts.(eccentricity).fixation.stimulusSize = 10;
    
else
    clear ii
    counter = 1;
    for ii = 1:length(Drifts.(eccentricity).(strokeWidth).position)
        position = Drifts.(eccentricity).(strokeWidth).position(:,ii);
        if isempty(position.x)
            continue;
        end
        
        %Mean Amplitude
        Drifts.(eccentricity).(strokeWidth).meanAmplitude(counter) = ...
            mean(Drifts.(eccentricity).(strokeWidth).amplitude(counter));
        
        %STD Amplitude
        Drifts.(eccentricity).(strokeWidth).stdAmplitude(counter) = ...
            std(Drifts.(eccentricity).(strokeWidth).amplitude)/sqrt(length(Drifts.(eccentricity).(strokeWidth).amplitude));
        
        %PRL Distance
        Drifts.(eccentricity).(strokeWidth).prlDistance(counter) = ...
            mean(sqrt(position.x.^2 + position.y.^2));
        
        Drifts.(eccentricity).(strokeWidth).prlDistanceX(counter) = ...
            mean(abs(position.x));
        
        Drifts.(eccentricity).(strokeWidth).prlDistanceY(counter) = ...
            mean(abs(position.y));
        
        if params.DMS
            positionWholeTrace = Drifts.(eccentricity).(strokeWidth).positionWholeTrace(:,ii);
            Drifts.(eccentricity).(strokeWidth).prlDistanceWholeTrace(ii) = ...
                mean(sqrt(positionWholeTrace.x.^2 + positionWholeTrace.y.^2));
        end
        
        %VELOCITY (& x & y) and SPEED
        [~, Drifts.(eccentricity).(strokeWidth).velocity(counter), velX{ii}, velY{ii}] = ...
            CalculateDriftVelocity(position, 1);
        
        [~, instSpX, instSpY, mn_speed, driftAngle, curvature, varx, vary] = ...
            getDriftChar(position.x, position.y, 41, 1, 180);
        
        Drifts.(eccentricity).(strokeWidth).curvature{ii} = curvature;
        Drifts.(eccentricity).(strokeWidth).driftAngle{ii} = driftAngle;
        Drifts.(eccentricity).(strokeWidth).mn_speed{ii} = mn_speed;
        Drifts.(eccentricity).(strokeWidth).varX{ii}= varx;
        Drifts.(eccentricity).(strokeWidth).varY{ii} = vary;
        
        if params.D
            if strcmp('D',params.machine)
                Drifts.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:(500/(1000/330)));
                Drifts.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:(500/(1000/330)));
            else
                Drifts.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:500);
                Drifts.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:500);
            end
        elseif params.DMS
            if strcmp('D',params.machine)
                Drifts.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:(300/(1000/330)));
                Drifts.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:(300/(1000/330)));
            else
                Drifts.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:300);
                Drifts.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:300);
            end
        end
        
        %Mean Span
        Drifts.(eccentricity).(strokeWidth).meanSpan(counter) = mean(Drifts.(eccentricity).(strokeWidth).span);
        
        %STD of Span
        Drifts.(eccentricity).(strokeWidth).stdSpan(counter) = ...
            std(Drifts.(eccentricity).(strokeWidth).span)/...
            sqrt(length(Drifts.(eccentricity).(strokeWidth).span));
        
        counter = counter + 1;
    end
%     for ii = 1:length((Drifts.(eccentricity).(strokeWidth).position))
        Drifts.(eccentricity).(strokeWidth).ccDistance = unique(stimulusSize)*1.4 - stimulusSize/2;%(round(2*sw*1.4))* pptrials{ii}.pixelAngle;
        %      Drifts.(eccentricity).(strokeWidth).stimulusSize(ii) = (2*double(sw))* pptrials{ii}.pixelAngle;
        Drifts.(eccentricity).(strokeWidth).stimulusSize = unique(stimulusSize);
        
%     end
    %%%performance at size
    for numSize = 1:length(unique(Drifts.(eccentricity).(strokeWidth).stimulusSize))
        numTrialsAtSize(numSize) = sum(...
            Drifts.(eccentricity).(strokeWidth).stimulusSize(numSize) == trialChar.TargetSize);
        
        numCorrectTrialsatSize(numSize) = sum(...
            (Drifts.(eccentricity).(strokeWidth).stimulusSize(numSize) == trialChar.TargetSize) &...
            (trialChar.Correct == 1));
    end
    Drifts.(eccentricity).(strokeWidth).performanceAtSize = ...
        round(numCorrectTrialsatSize/ numTrialsAtSize,2);

    forDSQCalc.x = {Drifts.(eccentricity).(strokeWidth).position.x};
    forDSQCalc.y = {Drifts.(eccentricity).(strokeWidth).position.y};

    if ~params.MS
        if strcmp('D',params.machine)
            sampling = 341;
        else
            sampling = 1000;
        end
        [~,~,Drifts.(eccentricity).(strokeWidth).dCoefDsq, ~, Drifts.(eccentricity).(strokeWidth).Dsq, ...
                Drifts.(eccentricity).(strokeWidth).SingleSegmentDsq,~,~] = ...
                CalculateDiffusionCoef(sampling, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
    end
    
    %%
%     forDSQCalc.x = {Drifts.(eccentricity).(strokeWidth).position.x(1:300)};
%     forDSQCalc.y = {Drifts.(eccentricity).(strokeWidth).position.y(1:300)};
% 
%     if ~params.MS
%         if strcmp('D',params.machine)
%             sampling = 341;
%         else
%             sampling = 1000;
%         end
%         [~,~,Drifts.(eccentricity).(strokeWidth).dCoefDsq_300, ~, Drifts.(eccentricity).(strokeWidth).Dsq_300, ...
%                 Drifts.(eccentricity).(strokeWidth).SingleSegmentDsq_300,~,~] = ...
%                 CalculateDiffusionCoef(sampling,struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
%     end
    %%
    
    Drifts.(eccentricity).(strokeWidth).meanAmplitude = mean(Drifts.(eccentricity).(strokeWidth).amplitude);
    Drifts.(eccentricity).(strokeWidth).stdAmplitude = std(Drifts.(eccentricity).(strokeWidth).amplitude)/sqrt(length(Drifts.(eccentricity).(strokeWidth).amplitude));
    
    Drifts.(eccentricity).(strokeWidth).meanSpan = mean(Drifts.(eccentricity).(strokeWidth).span);
    Drifts.(eccentricity).(strokeWidth).stdSpan = std(Drifts.(eccentricity).(strokeWidth).span)/sqrt(length(Drifts.(eccentricity).(strokeWidth).span));
end


if ~figures.FIXATION_ANALYSIS
    t = round(mean(Drifts.(eccentricity).(strokeWidth).time));
    [ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/100)),31); %Checks strange artifacts
    [ xValues, yValues] = checkXYBound(xValues, yValues, 30); %Checks boundries
else
    t = round(mean(Drifts.(eccentricity).fixation.time));
end

limit.xmin = floor(min(xValues));
limit.xmax = ceil(max(xValues));
limit.ymin = floor(min(yValues));
limit.ymax = ceil(max(yValues));
if figures.HUX_RUN
    n_bins = 20;
elseif uEcc == 0
    n_bins = 20;
else
    n_bins = 20;
end

figure;
result = MyHistogram2(xValues, yValues, [limit.xmin,limit.xmax,n_bins;limit.ymin,limit.ymax,n_bins]);
result = result./(max(max(result)));

if ~params.MS
    if ~figures.FIXATION_ANALYSIS
        limit = generateHeatMap(30, sw, result, xValues, ...
            yValues, stimulusSize, title_str, trialChar, params, driftIdx, t, figures, uEcc, pptrials);
        
        figure;
        %Generates histogram of Spans
        generateSpanHist(Drifts.(eccentricity).(strokeWidth).span, unique(stimulusSize), title_str, trialChar);
        
        %Generates histogram of span amplitude
        generateDriftAmplitudeHist(Drifts.(eccentricity).(strokeWidth).amplitude, sw, title_str, trialChar);
        
        %Generates heat map of velocity
        generateDriftVelocityMap(Drifts.(eccentricity).(strokeWidth).instSpX, ...
            Drifts.(eccentricity).(strokeWidth).instSpY, stimulusSize, params, title_str, trialChar)
        
        rangeXY = max(abs(cell2mat(struct2cell(limit))));
        %         F = GetTheFreq(result, rangeXY, n_bins);
        area = CalculateTheArea(xValues,yValues ,0.68, rangeXY, n_bins);
        Drifts.(eccentricity).(strokeWidth).areaCovered = area;
        
        format longG
        areaRound = round(area,5);
%         arcminSW = sw*2*unique(round(params.pixelAngle,2));
%         fprintf('Area for %s SW%i(actual arcmin %.3f) = %.3f\n', trialChar.Subject,sw,arcminSW,areaRound);
        
        filepath = ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/');
        
        saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/%s.epsc', filepath, trialChar.Subject, sprintf('%s_Drift_Anaylsis',trialChar.Subject)));
        saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/JPEG/Span&Amplitude/%s.png', filepath, trialChar.Subject, sprintf('%s_Drift_Anaylsis',trialChar.Subject)));
        saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/FIG/Span&Amplitude/%s.fig', filepath, trialChar.Subject, sprintf('%s_Drift_Anaylsis',trialChar.Subject)));
    end
end

% close all


end


