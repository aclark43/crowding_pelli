function [indSubBinUpper,indSubBinLower] = indicateAreaBinCro(subjectsAll)

indSubBinUpper = ones(1,length(subjectsAll))*80;
indSubBinLower = ones(1,length(subjectsAll))*60;


% indSubBinUpper(1) = 75;
% indSubBinLower(1) = 25;
% % % indSubBinUpper(2) = 80;
% % % indSubBinLower(2) = 20;
% % % indSubBinUpper(3) = 40;
% % % indSubBinLower(3) = 20;
% % % indSubBinUpper(4) = 70;
% % % indSubBinLower(4) = 20;
% % % indSubBinUpper(5) = 80;
% % % indSubBinLower(5) = 30;
% % % indSubBinUpper(6) = 20;
% % % indSubBinLower(6) = 20;
% % % indSubBinUpper(7) = 90;
% % % indSubBinLower(7) = 30;
% % % indSubBinUpper(8) = 80;
% % % indSubBinLower(8) = 20;
% % % indSubBinUpper(9) = 60;
% % % indSubBinLower(9) = 30;
% % % indSubBinUpper(10) = 50;
% % % indSubBinLower(10) = 40;
% % % indSubBinUpper(11) = 80;
% % % indSubBinLower(11) = 33;
% % % indSubBinUpper(12) = 80;
% % % indSubBinLower(12) = 25;
% % % indSubBinUpper(13) = 70;
% % % indSubBinLower(13) = 30;

% indSubBinUpper(1) = 75;
% indSubBinLower(1) = 30;
% indSubBinUpper(2) = 80;
% indSubBinLower(2) = 30;
% indSubBinUpper(3) = 60;
% indSubBinLower(3) = 40;
% indSubBinUpper(4) = 70;
% indSubBinLower(4) = 20;
% indSubBinUpper(5) = 80;
% indSubBinLower(5) = 30;
% indSubBinUpper(6) = 60;
% indSubBinLower(6) = 59;
% indSubBinUpper(7) = 90;
% indSubBinLower(7) = 30;
% indSubBinUpper(8) = 80;
% indSubBinLower(8) = 20;
% indSubBinUpper(9) = 60;
% indSubBinLower(9) = 30;
% indSubBinUpper(10) = 50;
% indSubBinLower(10) = 40;
% indSubBinUpper(11) = 80;
% indSubBinLower(11) = 33;
% indSubBinUpper(12) = 80;
% indSubBinLower(12) = 25;
% indSubBinUpper(13) = 70;
% indSubBinLower(13) = 30;
% 
% 
% 
% 
indSubBinUpper(1) = 75;
indSubBinLower(1) = 30;
indSubBinUpper(2) = 90;
indSubBinLower(2) = 85;
indSubBinUpper(3) = 89;
indSubBinLower(3) = 70;
indSubBinUpper(4) = 70;
indSubBinLower(4) = 20;
indSubBinUpper(5) = 40;
indSubBinLower(5) = 30;
indSubBinUpper(6) = 80;
indSubBinLower(6) = 59;
indSubBinUpper(7) = 90;
indSubBinLower(7) = 30;
indSubBinUpper(8) = 60;
indSubBinLower(8) = 50;
indSubBinUpper(9) = 60;
indSubBinLower(9) = 30;
indSubBinUpper(10) = 9;
indSubBinLower(10) = 90;
indSubBinUpper(11) = 50;
indSubBinLower(11) = 33;
indSubBinUpper(12) = 50;
indSubBinLower(12) = 25;
indSubBinUpper(13) = 70;
indSubBinLower(13) = 30;

% % indSubBi