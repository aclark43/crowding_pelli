function printTrialCounts(data, params)
vd = 2:3;% for drift and ms

% general overview table
fprintf('& Total & Valid & MS & S & NT/B & Yield\\\\\n');
fprintf('\\hline\n');
fprintf('All');
for ii = 1:length(data.count.total)
    fprintf('& %i', data.count.total(ii)); 
end
fprintf('& %1.2f\\%%', 100 * sum(data.count.total(vd), 2) / data.count.total(1));
fprintf('\\\\\n');

fprintf('Uncrowded');
for ii = 1:length(data.count.uncrowded.total)
    fprintf('& %i', data.count.uncrowded.total(ii)); 
end
fprintf('& %1.2f\\%%', 100 * sum(data.count.uncrowded.total(vd), 2) / data.count.uncrowded.total(1));
fprintf('\\\\\n');

fprintf('Crowded');
for ii = 1:length(data.count.crowded.total)
    fprintf('& %i', data.count.crowded.total(ii)); 
end
fprintf('& %1.2f\\%%', 100 * sum(data.count.crowded.total(vd), 2) / data.count.crowded.total(1));
fprintf('\\\\\n');

fprintf('Fixation');
for ii = 1:length(data.count.fixations)
    fprintf('& %i', data.count.fixations(ii)); 
end
fprintf('& %1.2f\\%%', 100 * sum(data.count.fixations(vd), 2) / data.count.fixations(1));
fprintf('\\\\\n');
fprintf('\\hline\n');

%% by eccentricity tables
fprintf('\n\n\n\n');
fprintf(' & \\multicolumn{6}{|c|}{Uncrowded} & \\multicolumn{6}{|c|}{Crowded}\\\\\n');
fprintf('Ecc. & Total & Valid & MS & S & NT/B & Yield & Total & Valid & MS & S & NT/B & Yield\\\\\n');
fprintf('\\hline\n');
for ee = 1:length(data.uniqueEccentricities)
    fprintf('%i', round(data.uniqueEccentricities(ee) * params.pixelAngle));
    
    for ii = 1:size(data.count.uncrowded.byEccentricity, 2)
        fprintf('& %i', data.count.uncrowded.byEccentricity(ee, ii)); 
    end
    fprintf('& %i: %1.2f\\%%', ...
        sum(data.count.uncrowded.byEccentricity(ee, vd), 2),...
        100 * sum(data.count.uncrowded.byEccentricity(ee, vd), 2) / data.count.uncrowded.byEccentricity(ee, 1));
    
    for ii = 1:size(data.count.crowded.byEccentricity, 2)
        fprintf('& %i', data.count.crowded.byEccentricity(ee, ii)); 
    end
    fprintf('& %i: %1.2f\\%%',...
        sum(data.count.crowded.byEccentricity(ee, vd), 2),...
        100 * sum(data.count.crowded.byEccentricity(ee, vd), 2) / data.count.crowded.byEccentricity(ee, 1));
    fprintf('\\\\\n');
end
fprintf('\\hline\n');

end