function [nXValues, nYValues] = cleanXYTrialsFromNans(xValStruct, yValStruct, max)

noTrackX = find(diff(xValStruct) == 0);
noTrackY = find(diff(yValStruct) == 0);
xValStruct(noTrackX) = NaN;
yValStruct(noTrackY) = NaN;

if any(xValStruct(:) > max) || any(xValStruct(:) < -max)...
        || any(yValStruct(:) < -max)|| any(yValStruct(:) < -max)
    xIdx = find(xValStruct(:) > max | xValStruct(:) < -max ...
        | yValStruct(:) < -max | yValStruct(:) < -max);
    
    xValStruct(xIdx) = NaN;
    yValStruct(xIdx) = NaN;
    
    [ nXValues, nYValues] = checkXYArt(xValStruct(:), yValStruct(:), min(round(length(xValStruct)/5)), 61);
    [ nXValues, nYValues] = checkXYBound(nXValues, nYValues, 60); %Checks boundries
    
    nXValues(find(diff(nXValues)) == 0) = NaN;
    nYValues(find(diff(nYValues)) == 0) = NaN;
else
    nXValues = xValStruct;
    nYValues = yValStruct;
end

end