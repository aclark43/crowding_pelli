function valid = countingTrialsAOAcuity(pptrials)

valid = buildValidStruct(pptrials);
%Checks for GAZE OFF CENTER
counter = [];
% figure;
for ii = 1
    x = pptrials{ii}.x;
    y = pptrials{ii}.y;
%     plot(1:length(x),x,'-');
%     hold on
%     plot(1:length(y),y,'-');
%     ylim([-20 20])
%     title(sprintf('%i',pptrials{1}.id))
    if pptrials{ii}.TargetEccentricity > 0 || pptrials{ii}.TargetEccentricity < 0
        if mean(sqrt(x.^2 + y.^2)) > 60 %%%Gaze Off Center/Span too large
            pptrials{ii}.GazeOffCenter = 1;
            valid.gazeoffcenter(ii) = true;
            output(ii) = 1;
            continue;
        end
        %                         targetDistThresh = 15;
        %                         dist_thresh = 10;
        pptrials{ii}.ActualEccTarget = double(mean(x)); %%mean span from 0, essentially causing target to be a specific distance
        em.actualEccTarget(ii) = double(mean(x));
        stimSize = pptrials{ii}.pixelAngle * double(pptrials{ii}.TargetStrokewidth) * 2;
        uEcc = double(pptrials{ii}.TargetEccentricity * pptrials{ii}.pixelAngle);
        centerTarget = (rectangle('Position',[(-stimSize)+uEcc ...
            (-stimSize*5) ...
            (2*stimSize) ...
            (10*stimSize)]));
        pointsCenterTarget = bbox2points(centerTarget.Position);
        onTarget = find(x > pointsCenterTarget(1,1) & ...
            x < pointsCenterTarget(2,1));% & %...
%             y > pointsCenterTarget(1,2) & ...
%             y < pointsCenterTarget(3,2));
        close all;
        if sum(length(onTarget)) > 12 ||...
                double(mean(x)) > uEcc %%if looking at the target (or halfway there)
            pptrials{ii}.LookingAtTarget = 1;
            valid.lookingAtTarget(ii) = true;
            output(ii) = 1;
                    continue;
        else
            em.actualEccTarget(ii) = double(mean(x));
        end
        %                     else
        %                         em.actualEccTarget(ii) = double(mean(x));
        %                     end
    elseif mean(sqrt(x.^2 + y.^2)) > 15 %%%Gaze Off Center/Span too large
        pptrials{ii}.GazeOffCenter = 1;
        valid.gazeoffcenter(ii) = true;
        output(ii) = 1;
        continue;
    end
    %Checks for LARGE OFFSET
    if sum(abs(x) > 120) > 0 || ...
            sum(abs(y) > 120) > 0
        pptrials{ii}.BigOffset = 1;
        valid.bigoffset(ii) = true;
        output(ii) = 1;
        continue;
        % end
        % else
        %     huxOffset = 180;
        %     %         if strcmp(params.subject,'HUX25')
        %     %             huxOffset = 180;
        %     %         end
        %     if sum(abs(x) > huxOffset) > 0 || ...
        %             sum(abs(y) > huxOffset) > 0
        %         pptrials{ii}.BigOffset = 1;
        %         valid.bigoffset(ii) = true;
        %         counter.BigOffset = counter.BigOffset+1;
        %         output(ii) = 1;

        %     end
    end
    %% Checks for BLINKS
    if ~isempty(pptrials{ii}.blinks.start)%|...
        %         sum(x > 500 | x < -500) > 0 | sum(y > 500 | y < -500) > 0%%%BLINKS
        valid.blink(ii) = true;
        output(ii) = 1;
        continue;
    end
    %% Checks for NO TRACKS
    if  ~isempty(pptrials{ii}.notracks.start)
        valid.notrack(ii) = true;
        output(ii) = 1;
        continue;
    end
    
    %% Checks for MANUAL DISCARD
    if  ~isempty(pptrials{ii}.invalid.start)
        valid.manualdiscard(ii) = true;
        output(ii) = 1;
        continue;
    end
    
    %% Saccade Check
    if ~isempty(length(pptrials{ii}.saccades.start))
        
        valid.s(ii) = true;
        
    end
    %% Microsaccade Check
    if ~isempty(length(pptrials{ii}.microsaccades.start))
        valid.ms(ii) = true;
        valid.dms(ii) = true;
        continue;
    end
    %% Drift Check
    if ~isempty(pptrials{ii}.drifts)
        valid.d(ii) = true;
        
        continue;
    else
        %% Didn't Fufill any other category
        valid.unknownToss = true;
    end
end

% if ~isfield(params,'OnlyTemp')
tossOptions = fieldnames(valid);
for ss = 1:length(tossOptions)
    if valid.(tossOptions{ss}) == 1
        fprintf('Trial %i Tossed for %s \n',pptrials{1}.id,tossOptions{ss});
    end
end
%     fprintf('\nThere are %i in total trials prior to filter.\n\n', numTotalTrials)
%     
%     fprintf('There are %i trials that have incorrect start time.\n',counter.timesOFF)
%     fprintf('There are %i trials that have gaze too far off center.\n',counter.GazeOffCenter)
%     fprintf('There are %i trials that have looking at target (Ecc > 0).\n',counter.LookingAtTarget)
%     fprintf('There are %i trials that have too short.\n',counter.TooShort)
%     fprintf('There are %i trials that have large offset.\n',counter.BigOffset)
%     fprintf('There are %i trials that have blinks.\n',counter.Blinks)
%     fprintf('There are %i trials that have no track during stimulus.\n',counter.NoTracks)
%     fprintf('There are %i trials that have no response.\n',counter.NoResponse)
%     fprintf('There are %i trials that have saccades.\n',counter.Saccades)
%     fprintf('There are %i trials that are fixation.\n',counter.Fixation)
%     fprintf('There are %i trials that have large offset for fixation trials.\n',counter.BigOffsetFixation)
%     fprintf('There are %i trials that do not fit within the span.\n',counter.Span)
%     fprintf('There are %i trials that have microsaccades.\n',counter.Microsaccades)
%     fprintf('There are %i trials that have microsaccades only partially in trail.\n',counter.partialMS)
%     fprintf('There are %i trials that have drifts only.\n',counter.Drifts)
%     fprintf('There are %i trials that have been manually discarded.\n',counter.manualDiscard)
%     fprintf('There are %i trials that have been tossed for no category.\n',counter.unknownToss)
%     fprintf('There are %i trials that have been tossed for being the wrong ecc.\n',counter.wrongecc)
%     fprintf('There are %i trials after filter.\n\n',  sum(cell2mat(struct2cell(counter))) - counter.TotalTask)
%     total = sum(cell2mat(struct2cell(counter))) - counter.TotalTask;
%     fprintf('There are %.1f percent of trials tossed for large gaze.\n',  ...
%         (counter.GazeOffCenter/total)*100 );
% end
end
