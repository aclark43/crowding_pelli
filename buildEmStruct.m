function dataStruct = buildEmStruct(pptrials)

uEcc = unique(cellfun(@(z) z.TargetEccentricity, pptrials));
dataStruct = struct(...;
    'microsaccadeRate', NaN(size(pptrials)),...;
    'saccadeRate', NaN(size(pptrials)),...;
    'fixationSaccadeRate', NaN(size(pptrials)),...;
    'fixationMicrosaccadeRate', NaN(size(pptrials)),...;
    'saccadeRateByEcc', NaN(length(uEcc) ,size(pptrials, 2)),...;
    'microsaccadeRateByEcc', NaN(length(uEcc) ,size(pptrials, 2)),...;
    'span', NaN(size(pptrials)));

end