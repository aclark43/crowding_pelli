function mVel = driftVelocityAnalysis(subjectCondition, subjectThreshCondition, subjectsAll, condition, c, fig)
%Looks at velocity calculated in runMultiple Subjects for each individual
%subject at each SW, as well as across Subj for threshold SW.


if fig.FIGURE_ON
    for ii = 1:length(subjectsAll)
        figure
        for swIdx = 1:length(subjectCondition(ii).SW)
            strokeValue = (subjectCondition(ii).SW(swIdx));
            if strokeValue == 0
                continue
            end
            strokeWidth = sprintf('strokeWidth_%i',strokeValue);
            instantSpX = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).instSpX;
            instantSpY = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).instSpY;

            numTrials = length(instantSpX);
            %         figure
            
             subplot( 2,round(length(subjectCondition(ii).SW)/2),swIdx)
            for trialIdx = 1:numTrials
                 for timeIdx = 1:500    
                speed(trialIdx,timeIdx) = (abs(instantSpX{1,trialIdx}(1,timeIdx))+...
                    abs(instantSpY{1,trialIdx}(1,timeIdx)))/2;
                 end
            end
            for timIdx = 1:length(speed)
                plot(timIdx,mean((speed(:,timIdx))),'o')
                hold on
            end
                axis([0 500 0 80]);
                hold on
                hold on
            
            text(250,60,sprintf('%i Trials,SW = %i',numTrials,strokeValue));
            saveas(gcf, sprintf('../Data/AllSubjTogether/IndividualVelocity%s%s.png',...
                subjectsAll{ii}, condition{1}));
        end
        suptitle(sprintf('Speed for All Strokewidths, %s',subjectsAll{ii}));
    end
    close all
    
    clear ii
    
    for ii = 1:length(subjectsAll)
        figure
        for swIdx = 1:length(subjectCondition(ii).SW)
            strokeValue = (subjectCondition(ii).SW(swIdx));
            if strokeValue == 0
                continue
            end
            strokeWidth = sprintf('strokeWidth_%i',strokeValue);
            velSubj = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).velocity;
            plotSW = zeros(length(velSubj), 1);
            plotSW(:) = strokeValue;
            plotSW = plotSW';
            scatter(velSubj,plotSW,100,'filled');
            
            hold on
        end
        xlabel('Velocity')
        ylabel('Strokewidth')
        title(sprintf('%s, %s',subjectsAll{ii}, condition{1}))
        xlabel('Velocity')
        ylabel('Strokewidth')
        saveas(gcf, sprintf('../Data/AllSubjTogether/%s_VelPersubj,%s.png', subjectsAll{ii}, condition{1}));
    end
end

% close all

figure
% CHECK DRIFT VELOCITY FOR THRESHOLD SW

for ii = 1:length(subjectsAll)
    strokeValue =(subjectThreshCondition(ii).thresh);
    [~,thresholdIdx] = min(abs(strokeValue - subjectCondition(ii).stimulusSize));
    strokeWidth = sprintf('strokeWidth_%i',subjectCondition(ii).SW(thresholdIdx));
    velocitySubj = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).velocity;
    plotSW = zeros(length(velocitySubj), 1);
    plotSW(:) = strokeValue;
    plotSW = plotSW';
    mVel(ii) = mean(velocitySubj);
    
    errorBar(velocitySubj,strokeValue)
    hold on
    points(ii) = scatter(mVel(1,ii),strokeValue,100,c(ii,:),'filled');
end
xlabel('Velocity')
ylabel('Threshold Strokewidth')
title(condition{1})
legend([points],subjectsAll)
saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjVelocity%s.png', condition{1}));

thresholds = [subjectThreshCondition.thresh];

figure
[~,p,~,r] = LinRegression(mVel,thresholds,0,NaN,1,0);
for ii = 1:length(thresholds)
    scatter(mVel(ii), thresholds(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled')
end
rValue = sprintf('r^2 = %.3f', r);
pValue = sprintf('p = %.3f', p);
text(40, 3, pValue,'Fontsize',10);
text(40,3.2,rValue,'Fontsize',10);
xlabel('Mean Velocity at Threshold')
ylabel('Strokewidth Threshold')
graphTitle1 = 'Mean Velocity by Threshold Strokewidth';
graphTitle2 = (sprintf('%s', cell2mat(condition)));
title({graphTitle1,graphTitle2})
xlim([30 60])
ylim([0 4])
saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjVelbyStrokewidth%s.png', condition{1}));
close all
end

