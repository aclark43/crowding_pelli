function [traces, condition, trialChar] = quickAnalysisOneSubject(params, filepath, figures, ~, ~, title_str)

load(fullfile(filepath, 'pptrials.mat'), 'pptrials');

%%%%%%%%% TRIAL PARAMETERS %%%%%%%%%%%%%%%%%%
%Gathering information from each trial from pptrials
trialChar = buildTrialCharStruct(pptrials);

em = buildEmStruct(pptrials);


for ii = 1:length(pptrials)
    %      spTrial = sprintf('pptrials{1,%i}.pixelAngle', ii);
    if isfield(pptrials{1,ii}, 'pixelAngle')%exist(sprintf('pptrials{1,%i}.pixelAngle', ii),'var')
        params.pixelAngle = pptrials{1,ii}.pixelAngle;
        pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
    else
        params.pixelAngle = pptrials{1,ii}.pxAngle;
    end
end

flds = fields(trialChar);
for fi = 1:length(flds)
    trialChar.(flds{fi}) = cellfun(@(z) z(:).(flds{fi}), pptrials);
end


%% Converts pixel --> arcmin
eccPixelAngle = unique(trialChar.TargetEccentricity);
eccArcMin = (double(eccPixelAngle))*params.pixelAngle;
trialChar.TargetEccentricity(:) = eccArcMin;

%have a copy of all the data collected
trialChar.Subject = pptrials{1}.Subject;

%%%%%%%% TRIAL COUNTS %%%%%%%%%%%
%sorting all inavlid trials from pptrials
%counting the number of each type of invlaid trial
[traces, counter, valid, em] = countingTrials(pptrials, em, params, figures);

traces.swIdx = trialChar.TargetStrokewidth(valid.d);

%%%%%%%%%%%%%%%%%%%%%%%
%% Graphing Function %%
if figures.FIXATION_ANALYSIS
else
    condition = Psychometric_Graphing(params, eccArcMin, valid, trialChar, params.crowded, title_str);
    fprintf('Threshold SW(Size) for %s  = %.3f\n', trialChar.Subject,condition.thresh);
end

if figures.FIGURE_ON 
    figure
    if params.DMS
         MarkerPlotting(params, pptrials, valid.dms, trialChar, title_str, valid)
    elseif params.MS
        MarkerPlotting(params, pptrials, valid.ms, trialChar, title_str, valid)
    else
        MarkerPlotting(params, pptrials, valid.d, trialChar, title_str, valid)
    end
end

if params.crowded
    crowded.trialChar = trialChar;
    crowded.valid = valid;
    crowded.em = em;
    crowded.counter = counter;
    crowded.params = params;
    save(sprintf('%s_crowded_summary.mat', trialChar.Subject), 'crowded')
else
    uncrowded.trialChar = trialChar;
    uncrowded.valid = valid;
    uncrowded.em = em;
    uncrowded.counter = counter;
    uncrowded.params = params;
    save(sprintf('%s_uncrowded_summary.mat', trialChar.Subject), 'uncrowded')
end
end

