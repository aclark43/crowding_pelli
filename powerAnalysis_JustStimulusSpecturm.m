% dc = 15; %diffusion constant

f = linspace(1, 80, 80); % temporal frequencies (Hz)
k = logspace(-1, 2, 100); % spatial frequencies (cpd)

digits = [3, 5, 6, 9];
vertical = 1;

pixelAngle = 0.25;
widthPixel = 6;

bufferSizeAngle = 2 * 60; % arcmin
bufferSizePixel = bufferSizeAngle / pixelAngle;

sz = 2 * bufferSizePixel;
k = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);


ca = [-40, 20];

kdc = k;

figure;
for patternVS1 = 1:2
    for di = 1%:length(digits)
        im = drawDigit(digits(di), widthPixel);
        if patternVS1 == 1
            temp = padarray(im-mean(im(:,size(im,2)/2)),[4,4],0,'both');
            im_buffered = repmat(temp,bufferSizePixel,bufferSizePixel);
        else
            if vertical == 1
                % buffer image and make it square
                if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
                    %im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel],0, 'both');
                    im_buffered = padarray(im-mean(im(:,size(im,2)/2)), [bufferSizePixel, bufferSizePixel],0, 'both');
                else
                    im_buffered = padarray(im-mean(im(:,(size(im,2)/2)+1)), [bufferSizePixel, bufferSizePixel],0, 'both');
                end
            else
                im_buffered = padarray(im-mean(im(round(size(im,1)/2)-1,:)), [bufferSizePixel, bufferSizePixel],0, 'both');
            end
        end
        [m, n] = size(im_buffered);
        df = floor((m - sz)/2);
        im_buffered = im_buffered(df:(df + sz - 1), :);
        df = floor((n - sz)/2);
        im_buffered = im_buffered(:, df:(df + sz - 1));
        
        
        if vertical == 1
            if patternVS1 == 1
                spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)-6)));
            else
                if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
                    spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)-1)));
                else
                    spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)+1)));
                end
            end
        else
            spec = fftshift(fft(im_buffered((length(im_buffered)/2)-1,:)));
        end
        
%         DpsIm = spec.*Dps1;
        
%         a = ((abs(spec)).^2);
%         b = (mean(psD).^2);
%  
        
        hold on
        semilogx(k,...
            pow2db((abs(spec)).^2),'-');
    end
end

 function digit = drawDigit(whichDigit, pixelwidth, patterned)
        % pixelwidth is ideally an even number
        digit = ones(pixelwidth*5, pixelwidth);
        strokewidth = pixelwidth/2;
        
        switch whichDigit
            case 3
                for ii = [1, 5.5 10]
                    si = (ii - 1)*strokewidth + 1;
                    ei = ii * strokewidth;
                    digit(si:ei, :) = 0;
                end
                
                digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
            case 5
                for ii = [1, 5.5 10]
                    si = (ii - 1)*strokewidth + 1;
                    ei = ii * strokewidth;
                    digit(si:ei, :) = 0;
                end
                digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
                digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
            case 6
                for ii = [1, 6:10]
                    si = (ii - 1)*strokewidth + 1;
                    ei = ii * strokewidth;
                    digit(si:ei, :) = 0;
                end
                
                digit(:, 1:strokewidth) = 0;
            case 9
                for ii = [1:5, 10]
                    si = (ii - 1)*strokewidth + 1;
                    ei = ii * strokewidth;
                    digit(si:ei, :) = 0;
                end
                digit(:, (strokewidth+1):(2*strokewidth)) = 0;
                % end
        end
        
        %     if exist('patterned','var')
        %         digit = [digit digit digit digit digit;
        %             digit digit digit digit digit];
        %     end
    end