function [bcea] = get_bcea(x, y)
x2 = 1.13; %chi-square variable with two degrees of freedom, encompasing 68% of the highest density points
idx = find(~isnan(x) & ~isnan(y));

bceaMatrix =  2*x2*pi * nanstd(x) * nanstd(y) * ...
    (1-corrcoef(x(idx),y(idx)).^2).^0.5; %smaller BCEA correlates to more stable fixation 
bcea = abs(bceaMatrix(1,2));
end