1%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

close all
clc
clear

%%Figure Location & Name%%
% subjectAll = {'HUX1','HUX3','HUX5','HUX6','HUX7','HUX8','HUX9',...
%     'HUX11','HUX13','HUX14','HUX16','HUX4','HUX10','HUX12','HUX18'};
% subjectAll = {'HUX1','HUX3','HUX5','HUX6','HUX7','HUX8','HUX9','HUX11','HUX13','HUX14','HUX16',...
%     'HUX4','HUX18','HUX10','HUX12','HUX17'}; %,'AshleyDDPI'};HUX17
% subjectAll = {'Z023','Ashley','Z005','Z002',...
%     'Z013','Z024','Z064','Z046','Z084','Z014'};
subjectAll = {'Z023','Ashley','Z005','Z002','Z013',...
    'Z024','Z064','Z046','Z084','Z014',...
    'Z091','Z138','Z181'};%,...
% subjectAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,...
% subjectAll = {'Z023','Ashley','Z005'}; %%Z002
%weirdos Z048, Z070, Z090

% subjectAll = {'Z090'};

condition = {'Uncrowded'}; %Figure 1 and Figure 2
stabilization = {'Unstabilized'};
em = {'Drift'};
ecc = {'0ecc'};

counter = 1;
% c = brewermap(12,'Dark2');
% c = brewermap(length(subjectAll),'Paired');
% ctemp = flip(brewermap(length(subjectAll),'RdYlBu'));
ctemp = winter(length(subjectAll)+20);

% ctemp = ashleysColorMap;
% SubjectOrderDCIdx = [2,12,5,7,1,9,4,10,6,8,11,3,13];
SubjectOrderDCIdx = [2,5,1,12,4,8,7,10,3,6,9,11,13];
% SubjectOrderDCIdx = [2,5,7,1,9,4,10,6,8,3];
for ii = 1:length(subjectAll)
    newOrder(ii) = find(SubjectOrderDCIdx == ii);
    subjOrder{ii} = subjectAll{newOrder(ii)};
    c(ii,:) = ctemp(SubjectOrderDCIdx == ii,:);
end

% tempc = colormap(jet(16-11));
% for cc = 12:16
%     c(cc,:) = tempc(cc-11,:);
% end
% for cc = 12:16
%     c(cc,:) = [0 0 0];
% end
% c = jet(length(subjectAll));
% c = jet(10);
for condCounter = 1:length(condition)
    
    graphMultiPsychCurves(subjectAll, condition{condCounter}, stabilization, em, ecc, c, SubjectOrderDCIdx)
    xlim([0 4.5])
    for ii = 1:length(subjectAll)
        currentS = (condition{1});
        fileName = sprintf('%s_%s_Unstabilized_%s_0ecc_Threshold.mat', subjectAll{ii},condition{condCounter},em{1});
        folderName = 'MATFiles/';
        file1 = fullfile(folderName, fileName);
        load(file1);
        %                                             subjectThresh(ii) = threshInfo;
        allThresh(ii) = threshInfo.thresh;
        
      
    end
    
    allThresholds(counter,:) = allThresh;
    [h,p,ci,stats] = ttest(allThresh);
    averageThresh = mean(allThresh);
    corThresh = 0.625;
    sigma = std(allThresh);
    sigma2 = std(corThresh);
    
    hold on
%     errorbar(averageThresh,corThresh,abs((ci(1)-ci(2))/2),...
%         'horizontal', '-ok','MarkerSize',10,...
%         'MarkerEdgeColor','black',...
%         'MarkerFaceColor','black',...
%         'CapSize',18,...
%         'LineWidth',5)
      errorbar(averageThresh,corThresh,sigma,...
        'horizontal', '-ok','MarkerSize',10,...
        'MarkerEdgeColor','black',...
        'MarkerFaceColor','black',...
        'CapSize',18,...
        'LineWidth',5)
    counter = counter+1;
    
    xlim([0.25 5])
    xticks([0.5 1 1.5 2 2.5 3 3.5 4 4.5 5])
    xticklabels({'0.5','1','1.5','2','2.5','3', '3.5','4','4.5'})
    %
    % xlim([0 8])
    % xticks([1:1:8])
    % xticklabels({'1','2','3','4','5','6','7','8'})
    
    xlabel('Strokewidth in arcmin');
    
    saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/UncrowdedPsychCurves.png');
    saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/UncrowdedPsychCurves.epsc');

    saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/CrowdedPsychCurves.png');
    saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/CrowdedPsychCurves.epsc');

    
    saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\COARSES\GrantWriting_Rucci\Images\PsychCurvesCente.png');
    saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\COARSES\GrantWriting_Rucci\Images\PsychCurvesCenter%s.epsc');
    
    saveas(gcf, ...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllSubj%s.png',...
        condition{condCounter}));
    saveas(gcf, ...
        sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllSubj%s.epsc',...
        condition{condCounter}));
    
    %     saveas(gcf, ...
%         sprintf('../../Data/AllSubjTogether/NoTossPsych/ALLSubj%s.png',...
%         condition{1}));
%     saveas(gcf, ...
%         sprintf('../../Data/AllSubjTogether/NoTossPsych/ALLSubj%s.epsc',...
%         condition{1}));
end
% saveas(gcf, '../../SummaryDocuments/HuxlinPTFigures/UncrowdedPsychAll.png');

% close all


