function differentSpanTrialsWithPerformances (subNum,subjectThreshUnc,subjectThreshCro,subjectUnc,subjectCro)


%Look at Span Differences
%creating a structure that has the performance(c1) and span values(c2) for 
%each subject

% for ii = 1:subNum
%     info(ii).spanU = subjectThreshUnc(ii).em.span;
%     info(ii).correctU = double(subjectThreshUnc(ii).em.correct);
%     info(ii).sizeU = subjectThreshUnc(ii).em.size;
%     info(ii).spanC = subjectThreshCro(ii).em.span;
%     info(ii).correctC = double(subjectThreshCro(ii).em.correct);
%     info(ii).sizeC = subjectThreshCro(ii).em.size;
% end

for ii = 1:subNum
    counterU = 1;
    counterC = 1;
    testedSW = subjectUnc(ii).SW;
    for jj = testedSW
        if ~jj == 0
            sw = sprintf('strokeWidth_%i',jj);
            perfUnc(ii).correct{counterU,:} = subjectThreshUnc(ii).em.ecc_0.(sw).correct;
            perfUnc(ii).span{counterU,:} = subjectThreshUnc(ii).em.ecc_0.(sw).span;
            perfUnc(ii).meanSpan(counterU) = subjectThreshUnc(ii).em.ecc_0.(sw).meanSpan;
            perfUnc(ii).sdSpan(counterU) = subjectThreshUnc(ii).em.ecc_0.(sw).stdSpan;
            counterU = counterU + 1;
        end
    end
    testedSW = subjectCro(ii).SW;
    for jj = testedSW
        if ~jj == 0
            sw = sprintf('strokeWidth_%i',jj);
            perfCro(ii).correct{counterC,:} = subjectThreshCro(ii).em.ecc_0.(sw).correct;
            perfCro(ii).span{counterC,:} = subjectThreshCro(ii).em.ecc_0.(sw).span;
            perfCro(ii).meanSpan(counterC) = subjectThreshCro(ii).em.ecc_0.(sw).meanSpan;
            perfCro(ii).sdSpan(counterC) = subjectThreshCro(ii).em.ecc_0.(sw).stdSpan;
            counterC = counterC + 1;
        end
    end
end

perforSmallU = [];
perforLargeU = [];
perCorSmallC = [];
perCorLargeC = [];

for ii = 1:subNum
    counterA = 1;
    counterB = 1;
    for i = 1:length(perfUnc(ii).span)
        for jj = 1:length(perfUnc(ii).span{i})
            if perfUnc(ii).span{i}(jj) < perfUnc(ii).meanSpan(i) - (perfUnc(ii).sdSpan(i)/2)
                 perforSmallU(i).correct(counterA) = perfUnc(ii).correct{i}(jj);
                 counterA = counterA + 1;
            elseif perfUnc(ii).span{i}(jj) > perfUnc(ii).meanSpan(i) + (perfUnc(ii).sdSpan(i)/2)
                 perforLargeU(i).correct(counterB) = perfUnc(ii).correct{i}(jj);
                 counterB = counterB + 1;
            end   
        end
        perCorSmallU(ii,i) = mean(perforSmallU(i).correct);
        perCorLargeU(ii,i) = mean(perforLargeU(i).correct);
    end
    
    counterA = 1;
    counterB = 1;
    for i = 1:length(perfCro(ii).span)
        for jj = 1:length(perfCro(ii).span{i})
            if perfCro(ii).span{i}(jj) < perfCro(ii).meanSpan(i) - (perfCro(ii).sdSpan(i)/2)
                 perforSmallC(i).correct(counterA) = perfCro(ii).correct{i}(jj);
                 counterA = counterA + 1;
            elseif perfCro(ii).span{i}(jj) > perfCro(ii).meanSpan(i) + (perfCro(ii).sdSpan(i)/2)
                 perforLargeC(i).correct(counterB) = perfCro(ii).correct{i}(jj);
                 counterB = counterB + 1;
            end   
        end
        perCorSmallC(ii,i) = mean(perforSmallC(i).correct);
        perCorLargeC(ii,i) = mean(perforLargeC(i).correct);
    end
end
perforSmallU = perCorSmallU((perCorSmallU > 0));
perforLargeU = perCorLargeU((perCorLargeU > 0));
perforSmallC = perCorSmallC((perCorSmallC > 0));
perforLargeC = perCorLargeC((perCorLargeC > 0));

sizeSpan = {'SmallSpans','LargeSpans'};

%takes just the more extreme SW value performance and spans for the bar 
%graph; whether the span is smaller/larger than one standard deviation 
%from the mean

% % % for ii = 1:subNum
% % % %     meanSpanUnc = mean(perfUnc(ii).info(:,2));
% % % %     sdSpanUnc = std(perfUnc(ii).info(:,2));
% % %     for i = 1:length(perfUnc(ii).info)
% % %         if perfUnc(ii).info(i,2) < meanSpanUnc - (sdSpanUnc/2)
% % %             perforSmallU(ii,i) = perfUnc(ii).info(i,1);
% % %             perforLargeU(ii,i) = NaN;
% % %         elseif perfUnc(ii).info(i,2) > meanSpanUnc + (sdSpanUnc/2)
% % %             perforLargeU(ii,i) = perfUnc(ii).info(i,1);
% % %             perforSmallU(ii,i) = NaN;
% % %         end
% % %     end
% % %     meanSpanCro = mean(perfCro(ii).info(:,2));
% % %     sdSpanCro = std(perfCro(ii).info(:,2));
% % %     for i = 1:length(perfCro(ii).info)
% % %         if perfCro(ii).info(i,2) < meanSpanCro - (sdSpanCro/2)
% % %             perforSmallC(ii,i) = perfCro(ii).info(i,1);
% % %             perforLargeC(ii,i) = NaN;
% % %         elseif perfCro(ii).info(i,2) > meanSpanCro + (sdSpanCro/2)
% % %             perforLargeC(ii,i) = perfCro(ii).info(i,1);
% % %             perforSmallC(ii,i) = NaN;
% % %         end
% % %     end
% % % end
% % % 
% % % uncrOnes = ones(1,subNum);
% % % uncrTwos = [2 2 2 2 2 2];
% % % 
% % % %removing all NaN and 0 values
% % % perforSmallU = perforSmallU(~isnan(perforSmallU) & (perforSmallU > 0));
% % % perforLargeU = perforLargeU(~isnan(perforLargeU) & (perforLargeU > 0));
% % % perforSmallC = perforSmallC(~isnan(perforSmallC) & (perforSmallC > 0));
% % % perforLargeC = perforLargeC(~isnan(perforLargeC) & (perforLargeC > 0));

% [h,p]=ttest(perforSmallU, performLargeU)
% [h,p]=ttest(meanUncr, meanCrow) 

c = categorical({'Uncrowded Small Span',...
    'Uncrowded Large Span',...
    'Crowded Small Span',...
    'Crowded Large Span'});

c = reordercats(c,{'Uncrowded Small Span',...
    'Uncrowded Large Span',...
    'Crowded Small Span',...
    'Crowded Large Span'});


meanUncr = [mean(perforSmallU) mean(perforLargeU)];
meanCrow = [mean(perforSmallC) mean(perforLargeC)];

meanAll = [mean(perforSmallU) mean(perforLargeU) mean(perforSmallC) mean(perforLargeC)];
stdAll = [std(perforSmallU) std(perforLargeU)  std(perforSmallC) std(perforLargeC)];
% stdCrow = [std(perform2C(1,:)) std(perform2C(2,:))];
% bar(c,meanUncr)
% bar(c, [meanUncr meanCrow]);
figure;
bar(categorical({'Uncrowded Small Span'}), mean(perforSmallU), 'FaceColor', [0, 0.5, 0]);
hold on
bar(categorical({'Uncrowded Large Span'}), mean(perforLargeU), 'FaceColor', [1, 0.5, 0]);
hold on
bar(categorical({'Crowded Small Span'}), mean(perforSmallC), 'FaceColor', [0, 0.5, 0]);
hold on
bar(categorical({'Crowded Large Span'}), mean(perforLargeC), 'FaceColor', [1, 0.5, 0]);

xlabel('Condition')
ylabel('Performance at Extreme SW Sizes')
title('Performance by Span Trials')

for ii = 1:4
    yMean = meanAll(ii);
    % points = line(xS, yMean, 'Color','k','LineWidth',2);
    
    ySTD = stdAll(ii); %standard deviation
    ySEM = ySTD/(sqrt(subNum)); %Standard Error Of The Mean At Each Value Of �x�
    
    CI95 = tinv([0.025 0.975], subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
    yCI95(ii) = bsxfun(@times, ySEM, CI95(2));
end

hold on
er = errorbar(c,meanAll, [yCI95(1) yCI95(2) yCI95(3) yCI95(4)]);
er.Color = [0 0 0];
er.LineStyle = 'none';
legend('Small Spans','Large Spans')

hold off
% scatter(uncrOnes,perform2U(1,:),'r')
% hold on
% scatter(uncrTwos,perform2U(2,:),'b');
% hold on
% scatter(uncrOnes,perform2C(1,:),'o')
% hold on
% scatter(uncrTwos,perform2C(2,:),'g');
%
% axis([0 3 .25 1])
%
% idxX = find(round(performU(1).SmallSpans.sizesTestedX{1, 1}, 1) == 2);
% performanceY = performU(1).SmallSpans.performanceAtSizesY{1, 1}.(idxX)
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/BarChartSmallLargeSpanPerformance.png');
end