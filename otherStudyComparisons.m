%% Other study comparisons

%% Cesarelli et al 2000 Nystagmus Paper - further analysis for reports
% SDp = STD of eye position during foveations (in degrees)
sdP.measures = [1.39 1.23 0.77 1.02 1.58 2.40 0.87 0.89 1.81 1.99 1.29 1.31 1.68 1.60 1.21 ...
    2.24 0.87 0.56 0.34 0.43 0.55 0.53 0.70 0.69 1.40 1.52 1.74 2.25 2.12 1.14 1.12 ...
    1.31 1.44 2.07 3.20 2.99 1.78 0.94 2.36 1.50];

sdP.mean = mean(sdP.measures);
sdP.std = std(sdP.measures);


%% Simmers et al Nystagmus Paper 1999
rms.PT.measures = [0.32 0.541 0.832 0.42 0.88];
rms.PT.mean = mean(rms.PT.measures);
rms.PT.std = std(rms.PT.measures);

rms.Control.measures = [.1 .2 .2 .17 .21 .23 .23 .24 .3]; %these are estimates, no value table
rms.Control.mean = mean(rms.Control.measures);
rms.Control.std = std(rms.Control.measures);

%% Chung et al Ambylopia Paper 2015
bcea.control.median = 0.0625;
bcea.control.range = [0.0263 0.4828];
bcea.anisoAE.median = 0.0770 ;
bcea.anisoAE.range = [0.0405 1.8710];

bcea.strabAE.median = 0.2058 ;
bcea.strabAE.range = [0.0603 11.4099];


%% Save Variables
otherStudies.Cesarelli2000.sdP = sdP;
otherStudies.Simmers1999.rms = rms;
otherStudies.Chung2015.bcea = bcea;

save('MATFiles/otherStudyComparisonFEM','otherStudies');