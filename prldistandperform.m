function [distAllTabUnc, distAllTabCro] = ...
    prldistandperform(params, subjectUnc, subjectThreshUnc, subjectCro, ...
    subjectThreshCro, thresholdSWVals, em, c, subjectsAll)



%% Look at PRL dist vs All Sw Size Performances
%All SW
[distAllTabUnc, distAllTabCro] = ...
    buildPRLStruct(0, params, subjectUnc, subjectThreshUnc, subjectCro, subjectThreshCro, thresholdSWVals);
plotPRLDistTabs(distAllTabUnc, distAllTabCro, 0, params)

%ThresholdSW
[distThreshTabUnc, distThreshTabCro] = ...
    buildPRLStruct(1, params, subjectUnc, subjectThreshUnc, subjectCro, subjectThreshCro, thresholdSWVals);
plotPRLDistTabs(distThreshTabUnc, distThreshTabCro, 1, params)


plotAreaSummaries(params,distAllTabUnc,distAllTabCro);

% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/PFLHistorgrams/CroPLFThreshold.png');
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/PFLHistorgrams/CroPLFThreshold.epsc');

%% Look just at threshold PRL dist with performance
% plotPRLDistTabs(distThreshTabUnc, distThreshTabCro, justThreshSW, params)

%sanity check

%% All PLF for Distribution Histograms
for ii = 1:params.subNum
    figure;
    subplot(1,2,1)
    uncrPLF{ii} = subjectThreshUnc(ii).em.ecc_0.(thresholdSWVals(ii).thresholdSWUncrowded).prlDistance; 
    sd1(1,ii) = std(uncrPLF{ii});
    histogram(uncrPLF{ii},50,'FaceColor',[.5 .5 0]);
    xlabel('Uncrowded')
    subplot(1,2,2)
    croPLF{ii} = subjectThreshCro(ii).em.ecc_0.(thresholdSWVals(ii).thresholdSWCrowded).prlDistance; 
    sd1(2,ii) = std(croPLF{ii});
    histogram(croPLF{ii},50);
    xlabel('Crowded')
    suptitle(sprintf('PLF Distance for %s',cell2mat(subjectsAll(ii))));
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/PFLHistorgrams/%sPLFHistorgram.png', ...
        cell2mat(subjectsAll(ii))));
end
close all;
%% PLF for Threshold SW
figure;
subplot(1,2,1)
for ii = 1:params.subNum
    uncrThresh(ii) = distThreshTabUnc(ii).meanPRLDistance;
    croThresh(ii) = distThreshTabCro(ii).meanPRLDistance;
end

p = plotCondition(croThresh,uncrThresh,params);
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',12);%,...
   % 'FontWeight','bold')
ylabel({'Mean Gaze Distance', 'at Thresh'})
title(sprintf('p = %.3f',p));
% title({'Mean Gaze at Threshold(per condition)', 'Uncrowded vs Crowded'})
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PLFonTargetThresh.png');

% subplot(1,2,2);
figure;
for ii = 1:length(subjectCro)
  swDelta{ii} = subjectThreshCro(ii).thresh - subjectThreshUnc(ii).thresh;
    
    swUnc = thresholdSWVals(ii).thresholdSWUncrowded;
    swCro = thresholdSWVals(ii).thresholdSWCrowded;
end
plotDeltaSW = cell2mat(swDelta);
% scatter([(cro-uncr)],plotDeltaSW)
deltaPLF = (croThresh-uncrThresh);

[~,p,~,r2] = LinRegression(deltaPLF,...
   plotDeltaSW,0,NaN,1,0);
hold on
for ii = 1:length(subjectCro)
     scatter(deltaPLF(ii),plotDeltaSW(ii),400,c(ii,:),'filled');
end
axis square
set(gca,'FontSize',14);
xlabel('\Delta Gaze Position (at threshold)')
ylabel('Crowding Effect')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PLFonTargetThresh.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PLFonTargetThresh.epsc');

%%%Plot Crowding Effect by Crowded Gaze Position
figure;
[~,p,~,r2] = LinRegression(croThresh,...
   plotDeltaSW,0,NaN,1,0);
hold on
for ii = 1:length(subjectCro)
     scatter(croThresh(ii),plotDeltaSW(ii),200,c(ii,:),'filled');
end
xlabel('Crowded Gaze Position (at threshold)')
ylabel('Crowding Effect')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/CrowdedGazeCrowdingEffect.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/CrowdedGazeCrowdingEffect.epsc');


%% PLF for Same SW

clear uncr
clear cro
clear corU 
clear corC
clear sizeU
clear sizeC

ii = [];
i = [];

for ii = 1:params.subNum
    i = [];
    for i = 1:length(distAllTabUnc)
        sizeTesting = (round(distAllTabUnc(i).stimSize,1));
        if distAllTabUnc(i).subject == ii &&...
                ( sizeTesting >= 1.2 && sizeTesting <= 1.5)
            %                 ( sizeTesting >= 1.5 && sizeTesting <= 2.0) %GreatThan
            %                 ( sizeTesting >= .8 && sizeTesting <= 1.2) %LessThan
            
            sizeU(ii) = distAllTabUnc(i).stimSize;
            uncr(ii) = distAllTabUnc(i).meanPRLDistance;
            corU(ii) = mean(distAllTabUnc(i).correct);
        end
    end
    i = [];
    for i = 1:length(distAllTabCro)
        sizeTesting = (round(distAllTabCro(i).stimSize,1));
        if isempty(distAllTabCro(i).subject)
            continue;
        elseif distAllTabCro(i).subject == ii &&...
                ( sizeTesting >= 1.7 && sizeTesting <= 2.0)
            %                 ( sizeTesting >= 2.0 && sizeTesting <= 2.3) %GreatThan
            %                 ( sizeTesting >= 1.2 && sizeTesting <= 1.5) %LessThan
            sizeC(ii) = distAllTabCro(i).stimSize;
            cro(ii) = distAllTabCro(i).meanPRLDistance;
            corC(ii) = mean(distAllTabCro(i).correct);
        end
    end
end
figure;
subplot(1,2,1)
[t,p,b1,r1] = LinRegression([uncr],...
    [corU],0,NaN,1,0);
for ii = 1:params.subNum
%     if uncr(ii) == uncrThresh(ii)
%         scatter(uncr(ii),...
%             corU(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'d','filled');
%     else
        scatter(uncr(ii),...
            corU(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
%     end
end
axis([3 11 0 1])
axis square
text(5,.2,sprintf('p = %.3f',p))
text(5,.15,sprintf('b = %.3f',b1))
text(5,.1,sprintf('r = %.3f',r1))
title('Uncrowded')
xlabel('Gaze Position Distance')
ylabel('Performance');

subplot(1,2,2);
[t,p,b2,r2] = LinRegression(double(nonzeros(cro)'),...
    double(nonzeros(corC)'),0,NaN,1,0);
for ii = 1:params.subNum
%     if cro(ii) == croThresh(ii)
%         scatter(cro(ii),...
%             corC(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'d','filled');
%     else
        scatter(cro(ii),corC(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
%     end
end
axis([3 11 0 1])
axis square
text(5,.2,sprintf('p = %.3f',p))
text(5,.15,sprintf('b = %.3f',b2))
text(5,.1,sprintf('r = %.3f',r2))
title('Crowded')
xlabel('Gaze Position Distance')
ylabel('Performance');
% suptitle('One Greater Than Threshold')
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/GreaterThreshPRLByPerformanceSameTargetSize.png');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/GreaterThreshPRLByPerformanceSameTargetSize.epsc');

saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PRLByPerformanceSameTargetSize.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PRLByPerformanceSameTargetSize.epsc');
%% PLF X
clear uncr
clear cro
clear corU 
clear corC
clear sizeU
clear sizeC

ii = [];
i = [];

for ii = 1:params.subNum
    i = [];
    for i = 1:length(distAllTabUnc)
        sizeTesting = (round(distAllTabUnc(i).stimSize,1));
        if distAllTabUnc(i).subject == ii &&...
                ( sizeTesting >= 1.2 && sizeTesting <= 1.5)
            sizeU(ii) = distAllTabUnc(i).stimSize;
            uncr(ii) = distAllTabUnc(i).meanXPRLDistance;
            corU(ii) = mean(distAllTabUnc(i).correct);
        end
    end
    i = [];
    for i = 1:length(distAllTabCro)
        sizeTesting = (round(distAllTabCro(i).stimSize,1));
        if isempty(distAllTabCro(i).subject)
            continue;
        elseif distAllTabCro(i).subject == ii &&...
                ( sizeTesting >= 1.7 && sizeTesting <= 2.0)
            sizeC(ii) = distAllTabCro(i).stimSize;
            cro(ii) = distAllTabCro(i).meanXPRLDistance;
            corC(ii) = mean(distAllTabCro(i).correct);
        end
    end
end
figure;
subplot(1,2,1)
[t,p,b1,r1] = LinRegression([uncr],...
    [corU],0,NaN,1,0);
for ii = 1:params.subNum
        scatter(uncr(ii),...
            corU(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end
axis([0 8 0 1])
text(5,.2,sprintf('p = %.3f',p))
text(5,.15,sprintf('b = %.3f',b1))
text(5,.1,sprintf('r = %.3f',r1))
title('Uncrowded')
xlabel('Gaze X Position Distance')
ylabel('Performance');

subplot(1,2,2);
[t,p,b2,r2] = LinRegression(double(nonzeros(cro)'),...
    double(nonzeros(corC)'),0,NaN,1,0);
for ii = 1:params.subNum
        scatter(cro(ii),corC(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end
axis([0 8 0 1])
text(5,.2,sprintf('p = %.3f',p))
text(5,.15,sprintf('b = %.3f',b2))
text(5,.1,sprintf('r = %.3f',r2))
title('Crowded')
xlabel('Gaze X Position Distance')
ylabel('Performance');

saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/XPRLByPerformanceSameTargetSize.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/XPRLByPerformanceSameTargetSize.epsc');
%% PLF Y
clear uncr
clear cro
clear corU 
clear corC
clear sizeU
clear sizeC

ii = [];
i = [];

for ii = 1:params.subNum
    i = [];
    for i = 1:length(distAllTabUnc)
        sizeTesting = (round(distAllTabUnc(i).stimSize,1));
        if distAllTabUnc(i).subject == ii &&...
                ( sizeTesting >= 1.2 && sizeTesting <= 1.5)
            sizeU(ii) = distAllTabUnc(i).stimSize;
            uncr(ii) = distAllTabUnc(i).meanYPRLDistance;
            corU(ii) = mean(distAllTabUnc(i).correct);
        end
    end
    i = [];
    for i = 1:length(distAllTabCro)
        sizeTesting = (round(distAllTabCro(i).stimSize,1));
        if isempty(distAllTabCro(i).subject)
            continue;
        elseif distAllTabCro(i).subject == ii &&...
                ( sizeTesting >= 1.7 && sizeTesting <= 2.0)
            sizeC(ii) = distAllTabCro(i).stimSize;
            cro(ii) = distAllTabCro(i).meanYPRLDistance;
            corC(ii) = mean(distAllTabCro(i).correct);
        end
    end
end
figure;
subplot(1,2,1)
[t,p,b1,r1] = LinRegression([uncr],...
    [corU],0,NaN,1,0);
for ii = 1:params.subNum
        scatter(uncr(ii),...
            corU(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end
axis([0 10 0 1])
text(5,.2,sprintf('p = %.3f',p))
text(5,.15,sprintf('b = %.3f',b1))
text(5,.1,sprintf('r = %.3f',r1))
title('Uncrowded')
xlabel('Gaze Y Position Distance')
ylabel('Performance');

subplot(1,2,2);
[t,p,b2,r2] = LinRegression(double(nonzeros(cro)'),...
    double(nonzeros(corC)'),0,NaN,1,0);
for ii = 1:params.subNum
        scatter(cro(ii),corC(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end
axis([0 10 0 1])
text(5,.2,sprintf('p = %.3f',p))
text(5,.15,sprintf('b = %.3f',b2))
text(5,.1,sprintf('r = %.3f',r2))
title('Crowded')
xlabel('Gaze Y Position Distance')
ylabel('Performance');

saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/YPRLByPerformanceSameTargetSize.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/YPRLByPerformanceSameTargetSize.epsc');
%% Plot Delta DSQ by Delta PLF

deltaDSQ = threshDSQCro-threshDSQUncr;

figure;
[~,p,~,r2] = LinRegression(deltaPLF,...
   deltaDSQ,0,NaN,1,0);
hold on
for ii = 1:length(subjectCro)
     scatter(deltaPLF(ii),deltaDSQ(ii),200,c(ii,:),'filled');
end
axis square
axis([-2 2 -20 30])
xlabel('\Delta Gaze Position')
ylabel('\Delta DSQ')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DeltaDSQDeltaPLF.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DeltaDSQDeltaPLF.epsc');
%%
figure;
xlabel('PLF Distance');
ylabel('Performance');
ylim([0 1])
title('Uncrowded SW = 4')
text(5,.9,sprintf('p = %.3f',p))

plotCondition(cro,uncr, params)
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
    'FontWeight','bold')
ylabel('Mean PLF Distance')
title({'Mean PLF at Same Strokewidth', 'Uncrowded vs Crowded'})
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PLFonSameTargetSize.png');

%% plot Delta SW by Delta PLF
% figure;
% for ii = 1:length(subjectCro)
%   swDelta{ii} = subjectThreshCro(ii).thresh - subjectThreshUnc(ii).thresh;
%     
%     swUnc = thresholdSWVals(ii).thresholdSWUncrowded;
%     swCro = thresholdSWVals(ii).thresholdSWCrowded;
% 
%     thresholdUncrowdedSpan{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;
%     thresholdCrowdedSpan{ii} = subjectThreshCro(ii).em.ecc_0.(swCro).meanSpan;
%     
%     thresholdSWUnc{ii} = subjectThreshUnc(ii).thresh;
%     thresholdSWCro{ii} = subjectThreshCro(ii).thresh;
% end
% figure;
% plotDeltaSW = cell2mat(swDelta);
% % scatter([(cro-uncr)],plotDeltaSW)
% deltaPLF = [(cro-uncr)];
% [~,p,~,r2] = LinRegression(deltaPLF,...
%    plotDeltaSW,0,NaN,1,0);
% hold on
% for ii = 1:length(subjectCro)
%      scatter(deltaPLF(ii),plotDeltaSW(ii),200,c(ii,:),'filled');
% end
% xlabel('\Delta Gaze Position same size stim')
% ylabel('\Delta Strokewidth(arcmin)')
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DeltaGazePosDeltaThresh.epsc');


% %%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
[~,p,~,r2] = LinRegression([cro],...
   plotDeltaSW,0,NaN,1,0);
hold on
for ii = 1:length(subjectCro)
     scatter(cro(ii),plotDeltaSW(ii),200,c(ii,:),'filled');
end
xlabel('Crowded Gaze Position (same size stim)')
ylabel('\Delta Strokewidth(arcmin)')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/CrowGazePosDeltaThresh.png');

saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/CrowGazePosDeltaThresh.epsc');


%% < or > 50% Performance PLF

%%%%Final figure = make size of stimulus color bars;
figure;
% subplot(1,2,2)
[smallCro, largeCro] = getPLFSmallLargeCondition(distAllTabCro, distThreshTabCro, params, subjectsAll);
[smallUnc, largeUnc] = getPLFSmallLargeCondition(distAllTabUnc, distThreshTabUnc, params, subjectsAll);

% small

for ii = 1:length(largeCro)
    far(1,ii) = largeUnc(ii).meanPerformance;
    farPRL (1,ii) = largeUnc(ii).meanPRL;
    numTrialsF(1,ii) = length(largeUnc(ii).prlLarge);
    closer(1,ii) = smallUnc(ii).meanPerformance;
    closerPRL (1,ii) = smallUnc(ii).meanPRL;
    numTrialsC(1,ii) = length(smallUnc(ii).prlSmall);
end
for ii = 1:length(largeCro)
    far(2,ii) = largeCro(ii).meanPerformance;
    farPRL(2,ii) = largeCro(ii).meanPRL;
    numTrialsF(2,ii) = length(largeCro(ii).prlLarge);
    closer(2,ii) = smallCro(ii).meanPerformance;
    closerPRL(2,ii) = smallCro(ii).meanPRL;
    numTrialsC(2,ii) = length(smallCro(ii).prlSmall);
end
%%
figure;
subplot(1,2,1)
% plot(sd1,closer-far,'-o')
[~,p,~,r] = LinRegression([sd1(1,:) ],...
    [closer(1,:)-far(1,:)],0,NaN,1,0);
% Lin closer(2,:)-far(2,:)sd1(2,:)
title('Uncrowded')
xlabel('Standard Deviation (Variance)')
ylabel('Difference in Perforamance(Small - Large)');
text(0,-.1,sprintf('p = %.3f',p));
subplot(1,2,2)
[~,p,~,r] = LinRegression([sd1(2,:) ],...
    [closer(2,:)-far(2,:)],0,NaN,1,0);
% Lin closer(2,:)-far(2,:)sd1(2,:)
title('Crowded')
xlabel('Standard Deviation (Variance)')
ylabel('Difference in Perforamance(Small - Large)');
text(0,0,sprintf('p = %.3f',p));
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/VarVsDifPerfSmalLargePLF.png');
%%

vecOne = ones(1,20);
vecZero = zeros(1,20);
% figure;
counter = 1;
for ii = 1:params.subNum
    totalNumTrialsF = sum([numTrialsF(1,ii),numTrialsF(2,ii)]);
    totalNumTrialsC = sum([numTrialsC(1,ii),numTrialsC(2,ii)]);
    if totalNumTrialsF > 25 && totalNumTrialsC > 25
        keepIdx(ii) = 1;
        meanCloser(counter) = mean([closer(1,ii) closer(2,ii)]);
        meanPRLClose(counter)= mean([closerPRL(1,ii) closerPRL(2,ii)]);
        meanFar(counter) = mean([far(1,ii) far(2,ii)]);
        meanPRLFar(counter) = mean([farPRL(1,ii) farPRL(2,ii)]);
        counter = counter + 1;
    else
        keepIdx(ii) = 0;
    end
    
end

allPRLCloseFar = ([meanPRLClose meanPRLFar]);
colorDistance = parula(length(allPRLCloseFar));
[b,i] = sort(allPRLCloseFar);
maxVal = max(allPRLCloseFar);
minVal = min(allPRLCloseFar);

figure;
for ii = 1:length(meanCloser)
  scatter(0, meanCloser(i(ii)),200,[colorDistance(i(ii),:)],'filled'); 
  hold on
end

for nii = 1:length(meanFar)
  leg(nii) = scatter(1, meanFar(i(nii)),200,[colorDistance(i(nii+ii),:)],'filled'); 
  hold on
end
cb = colorbar;
set(cb, 'Ticks', [0 .5 1], 'TickLabels', {minVal, mean([minVal maxVal]), maxVal})

xS = [1 0];
for ii = 1:params.subNum
    if keepIdx(ii)
    y1 = [mean([far(1,ii),far(2,ii)]) mean([closer(1,ii) closer(2,ii)])];
    points = line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
    points.LineStyle = '--';
    end
end

hold on
scatter(1, mean([far(1,find(keepIdx)) far(2,find(keepIdx))]), 100,'k','filled')
hold on
errorbar(1, mean([far(1,find(keepIdx)) far(2,find(keepIdx))]),std(([far(1,find(keepIdx)) far(2,find(keepIdx))])),'MarkerSize',20,...
    'MarkerEdgeColor','k')
hold on
scatter(0, mean([closer(1,find(keepIdx)) closer(2,find(keepIdx))]), 100,'k','filled')
hold on
errorbar(0, mean([closer(1,find(keepIdx)) closer(2,find(keepIdx))]),std(([closer(1,find(keepIdx)) closer(2,find(keepIdx))])),'MarkerSize',20,...
    'MarkerEdgeColor','k')

xS = [1 0];
y1 = [mean([far(1,find(keepIdx)) far(2,find(keepIdx))]), mean([closer(1,find(keepIdx)) closer(2,find(keepIdx))])];
line(xS, y1, 'Color','k','LineWidth',2);

xticks([0 1])
xticklabels({'Small PLFs','Large PLFs'})
ylabel('Performance')
title('Crowded and Uncrowded Combined PLFs by Performance')
xlim([-.5 1.5])
ylim([0.25 .8])
[h,p] = ttest([far(1,find(keepIdx)) far(2,find(keepIdx))],[closer(1,find(keepIdx)) closer(2,find(keepIdx))])
% legend(leg(keepIdx),subjectsAll(keepIdx));
text(0,.35,sprintf('p = %.3f',p))

saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SmallvsLargePLFonSameTargetSizeUncrandCroDifBins.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SmallvsLargePLFonSameTargetSizeUncrandCroDifBins.epsc');
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SmallvsLargePLFonSameTargetSizeUncrandCroDifBins.bmp');

%%%%%%%%%%
% plotCondition([far far],[closer(1,:) closer(2,:)],params)
% ylim([0 1])
% for ii = 1:length(largeCro)
%     text(1,far(ii),sprintf('%i',numTrialsF(ii)))
%     text(0,closer(ii),sprintf('%i',numTrialsC(ii)))
% end
% [~,p] = ttest(closer,far);
% text(0,.2,sprintf('p = %.3f',p))
% names = {'Small PLF','Large PLF'};
% set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
%     'FontWeight','bold')
% ylabel('Performance')
% title('Crowded')
% 
% subplot(1,2,1)
% [smallUnc, largeUnc] = getPLFSmallLargeCondition(distAllTabUnc, distThreshTabUnc, params);
% for ii = 1:length(largeCro)
%     far(ii) = largeUnc(ii).meanPerformance;
%     numTrialsF(ii) = length(largeUnc(ii).prlLarge);
%     closer(ii) = smallUnc(ii).meanPerformance;
%     numTrialsC(ii) = length(smallUnc(ii).prlSmall);
% end
% plotCondition(far,closer,params)
% ylim([0 1])
% for ii = 1:length(largeCro)
%     text(1,far(ii),sprintf('%i',numTrialsF(ii)))
%     text(0,closer(ii),sprintf('%i',numTrialsC(ii)))
% end
% [~,p] = ttest(closer,far);
% text(0,.2,sprintf('p = %.3f',p))
% names = {'Small PLF','Large PLF'};
% set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
%     'FontWeight','bold')
% ylabel('Performance')
% title('Uncrowded')
% set(gcf, 'Position',  [2000, 100, 800, 800])
% suptitle('PLF Distances (1SD) for Threshold SW by Performance')
% saveas(gcf, '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PerformancebyPLFSmallLarge.png');

end