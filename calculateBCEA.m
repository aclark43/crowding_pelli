function bcea = calculateBCEA(x,y)
%   calculates the bivariate contour ellipse of eye position.
%   x = vector of x positions
%   y = vector of y positions
%   f = probability (typically one standard deviation of the distrubution)
%   or degrees of freedom
f = std([x y]);
chi2 = chi2pdf([x y],f);
sigmaX = std(x);
sigmaY = std(y);
p = corrcoef(x,y);

bcea = pi*chi2*sigmaX*sigmaY(1-p^2)^(1/2);

end