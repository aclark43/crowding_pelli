Z055_AO = (imread(...
    'X:\Adaptive_Optics\APLab_System\Z190_2022_06_03\ProcessedImages\contrastEnhanced\manually_tagged\R_5_07_V010_dropFr_trimmed1_251.tif'));

figure;
imshow(Z055_AO);

if manualtag == 1
    temp = imfindcircles(Z055_AO,20);
    
    hold on
    for i = 1:length(temp)
        plot(temp(1),temp(2),'*','Color','b','MarkerSize',10);
    end
    
    %% Find the Red colored dots
    colored_image = Z055_AO;
    target1R = 255; target1G = 0; target1B = 0;
    R = colored_image(:,:,1);
    G = colored_image(:,:,2);
    B = colored_image(:,:,3);
    matches1 = R == target1R & G == target1G & B == target1B;
    [r1, c1] = find(matches1); %location of each red point
    figure;imagesc(matches1);colormap(flipud(bone))

else
    temp = importdata('X:\Adaptive_Optics\APLab_System\Z190_2022_06_03\ProcessedImages\contrastEnhanced\manually_tagged\R_5_07_V010_dropFr_trimmed1_251_07272022_AMC.txt');
    r1 = temp(:,1);
    c1 = temp(:,2);
    hold on;plot(c1',r1','*','Color','r');
end

%% Get the distances of each neighbor
Y = r1;
X = c1;
dist = hypot(r1 - r1.', c1 - c1.'); %calculate distance between each pair of point. Creates a symmetric matrix
mdistances = mean(dist(tril(true(size(dist)), -1)));  %calculate mean of lower triangular matrix below diagonal of dist. i.e. distance between each unique pair of points

for i = 1: length(X) 
    
    theDistance = sqrt((X - X(i)).^2 + (Y - Y(i)).^2); %distance between a cone and all other cones
    theDistance(i) = Inf; %we don't want the distance between a cone and itself
    
    [theNearestNeighborDistance, theNearestNeighborIndex] = min(theDistance); %finds the nearest neighbor 
    
    allNearestNeighborDistance(i) = theNearestNeighborDistance; %saves each nearest neighbor distance
end
%% Figure Out STD
simplemean = mean(allNearestNeighborDistance); %simple mean of the data
perc = floor(length(X)/10); trim = sort(allNearestNeighborDistance); trim(1:perc)=[]; trim(length(trim)-perc:length(trim)); 
trimmedmean = mean(trim); %trims the top/bottom ten percent of data and takes the mean
simplemode = mode(allNearestNeighborDistance); %simple mode of the data
binnedmode = mode(floor(allNearestNeighborDistance) + floor( (allNearestNeighborDistance-floor(allNearestNeighborDistance))/0.5) * 0.5);
%bins the data into histogram-like bins of width 0.5 and takes the mode
roundedmode = mode(round(allNearestNeighborDistance.*2)./2); %rounds the data to nearest 0.5 and takes the mode
medien = median(allNearestNeighborDistance); %simple median

if simplemode ~= 5
    allNNCopy = allNearestNeighborDistance; allNNCopy(allNearestNeighborDistance == simplemode) = [];
    modeWanted = mode(allNNCopy);
else
    modeWanted = simplemode;
end
rho = simplemean;
% rho = modeWanted;%max(allNearestNeighborDistance); %simply change which variable is rho for a different output mean

rng default
[bootstat,bootsam]= bootstrp(1000,@median,allNearestNeighborDistance);
se = std(bootstat);
%% From Sanajana
umperpixel = .5; %conversion in microns per pixel HARD CODES FROM SANJ
umPerDegGiven = 278.81; %HARD CODES FROM SANJ

rho_um = rho*umperpixel; % converts all the pixel values to microns
se_um = se*umperpixel;

density_circ = 1/(pi*(rho_um/2)^2); % circular formula
density_hex = 2/((rho_um)^2 * sqrt(3)); % hexagonal formula

% imageNames{end+1,1} = imgName;
% densities{end+1,1} = round(density_hex*(umPerDegGiven)^2);
%% finds the cone spacing in the image from the nearest neighbor distances

simplemean = mean(allNearestNeighborDistance); %simple mean of the data
perc = floor(length(X)/10); trim = sort(allNearestNeighborDistance); trim(1:perc)=[]; trim(length(trim)-perc:length(trim)); 
trimmedmean = mean(trim); %trims the top/bottom ten percent of data and takes the mean
simplemode = mode(allNearestNeighborDistance); %simple mode of the data
binnedmode = mode(floor(allNearestNeighborDistance) + floor( (allNearestNeighborDistance-floor(allNearestNeighborDistance))/0.5) * 0.5);
%bins the data into histogram-like bins of width 0.5 and takes the mode
roundedmode = mode(round(allNearestNeighborDistance.*2)./2); %rounds the data to nearest 0.5 and takes the mode
medien = median(allNearestNeighborDistance); %simple median
%% plot of cone marks
img = matches1;
figure; clf;

imagesc(img); hold on;
colormap('gray');
plot(X, Y, 'r+');
axis equal; axis tight;

% nearest neighbor density (from QuickAnalysisOfConeDensity.m)

figure;  clf;
imagesc(img);  hold on;
axis equal; axis tight; 
colormap('gray');
plot(X, Y, '.r');

figure
hold on;
hist(allNearestNeighborDistance);
histHandle = findobj(gca,'Type','patch');
set(histHandle,'FaceColor',[0, 0, 1], 'EdgeColor', [1, 1, 1]);


%% Creating Heat Map






