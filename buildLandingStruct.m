function [landPosS, landPosE] = buildLandingStruct (ii, subjectMS, subjectsAll)
%UNTITLED12 Summary of this function goes here
%   Detailed explanation goes here

landPosS.Subject = subjectsAll(ii);
% landPosS.Strokewidth = strokeValue;
landPosE.Subject = subjectsAll(ii);
% landPosE.Strokewidth = strokeValue;

location = subjectMS(ii).em.ms.landingPattern;

% for i = 1:length(location.startingMSLocation)
%     totNumMsS(i) = location.startingMSLocation(i).NumOfMS;
%     totTar(i) = location.startingMSLocation(i).Target;
%     totR(i)= location.startingMSLocation(i).RFlanker;
%     totL(i)= location.startingMSLocation(i).LFlanker;
%     totT(i)= location.startingMSLocation(i).TFlanker;
%     totB(i)= location.startingMSLocation(i).BFlanker;
%     totBack(i)= location.startingMSLocation(i).Background;
% end
% totalS = length(location.startingMSLocation)*100; %%%Divide but how many %?
% 
% for i = 1:length(location.endingMSLocation)
%     totNumMsE(i) = location.endingMSLocation(i).NumOfMS;
%     totTarE(i) = location.endingMSLocation(i).Target;
%     totRE(i)= location.endingMSLocation(i).RFlanker;
%     totLE(i)= location.endingMSLocation(i).LFlanker;
%     totTE(i)= location.endingMSLocation(i).TFlanker;
%     totBE(i)= location.endingMSLocation(i).BFlanker;
%     totBackE(i)= location.endingMSLocation(i).Background;
% end
for i = 1:length(location.startingMSLocation)
    totNumMsS(i) = location.startingMSLocation(i).microsaccades.NumOfMS;
    totTar(i) = location.startingMSLocation(i).microsaccades.Target;
    totR(i)= location.startingMSLocation(i).microsaccades.RFlanker;
    totL(i)= location.startingMSLocation(i).microsaccades.LFlanker;
    totT(i)= location.startingMSLocation(i).microsaccades.TFlanker;
    totB(i)= location.startingMSLocation(i).microsaccades.BFlanker;
    totBack(i)= location.startingMSLocation(i).microsaccades.Background;
end
totalS = length(location.startingMSLocation)*100; %%%Divide but how many %?

for i = 1:length(location.endingMSLocation)
    totNumMsE(i) = location.endingMSLocation(i).microsaccades.NumOfMS;
    totTarE(i) = location.endingMSLocation(i).microsaccades.Target;
    totRE(i)= location.endingMSLocation(i).microsaccades.RFlanker;
    totLE(i)= location.endingMSLocation(i).microsaccades.LFlanker;
    totTE(i)= location.endingMSLocation(i).microsaccades.TFlanker;
    totBE(i)= location.endingMSLocation(i).microsaccades.BFlanker;
    totBackE(i)= location.endingMSLocation(i).microsaccades.Background;
end
totalE = length(location.endingMSLocation)*100;

landPosS.totNumMS = num2str(sum(totNumMsS));
landPosS.targetS = num2str(round(sum(totTar)/totalS, 4)*100, '%.2f');
landPosS.rightFS = num2str(round(sum(totR)/totalS, 4)*100, '%.2f');
landPosS.leftFS = num2str(round(sum(totL)/totalS, 4)*100, '%.2f');
landPosS.topFS = num2str(round(sum(totT)/totalS, 4)*100, '%.2f');
landPosS.bottomFS = num2str(round(sum(totB)/totalS, 4)*100, '%.2f');
landPosS.backgS = num2str(round(sum(totBack)/totalS, 4)*100, '%.2f');
% landPosS.timeOutS = num2str(round(location.startingMSLocation.TimeOut, 4)*100, '%.2f');


landPosE.totNumMS = num2str(sum(totNumMsE));
landPosE.targetE = num2str(round(sum(totTarE)/totalE, 4)*100, '%.2f');
landPosE.rightFE = num2str(round(sum(totRE)/totalE, 4)*100, '%.2f');
landPosE.leftFE = num2str(round(sum(totLE)/totalE, 4)*100, '%.2f');
landPosE.topFE = num2str(round(sum(totTE)/totalE, 4)*100, '%.2f');
landPosE.bottomFE = num2str(round(sum(totBE)/totalE, 4)*100, '%.2f');
landPosE.backgE = num2str(round(sum(totBackE)/totalE, 4)*100, '%.2f');
% landPosE.timeOutE = num2str(round(location.endingMSLocation.TimeOut, 4)*100, '%.2f');

end

