function [meanDriftCoefAcrossSubjSmall, meanDriftCoefAcrossSubjLarge,...
    largeDriftCoef, smallDriftCoef, STDDCoefAcrossSubjLarge, STDDCoefAcrossSubjSmall]...
    = reBuildDSQInfo( subjectCondition, subNum, subjectThreshCondition)

% Rebuild all drift coefficient information to create a bar graphs later on

%% Building driftCoef Structure for all driftCoefs
for i = 1:subNum
    for ii = 1:length(subjectCondition(i).stimulusSize)
        if subjectCondition(i).SW(ii) == 0
            SW(i,ii) = NaN;
            stimulusSize(i,ii) = NaN;
            driftCoef(i,ii) = NaN;
        else
            SW(i,ii) = subjectCondition(i).SW(ii);
            stimulusSize(i,ii) = subjectCondition(i).stimulusSize(ii);
            sw = sprintf('strokeWidth_%i', SW(i,ii));
            driftCoef(i,ii) = subjectThreshCondition(i).em.ecc_0.(sw).dCoefDsq;
%             stdDriftSpan(i,ii) = subjectCondition(i).stdDriftSpan(ii);
        end
    end
    idxActualSWVal{i} = find(~isnan(stimulusSize(i,:)) & stimulusSize(i,:)>0);
    numberSWTested(i) = length(idxActualSWVal{i});
end

%% Getting the DCoef for the smallest SW and the largest SW tested
for jj = 1:subNum
    nonNanDCValues = find(~isnan(driftCoef(jj,:))>0);
    firstDCoef = driftCoef(jj,nonNanDCValues(1));
    smallDriftCoef(jj,1) = firstDCoef;
%     smallErrorsAbs(jj,1) = stdDriftSpan(jj,nonNanDCValues(1));
    %      end
    for large = idxActualSWVal{jj}(length(idxActualSWVal{jj}))
        largeDriftCoef(jj,1) = driftCoef(jj,large);
%         largeErrorsAbs(jj,1) = stdDriftSpan(jj, large);
    end
end

%% Getting the means and the std
meanDriftCoefAcrossSubjSmall = mean(smallDriftCoef);
STDDCoefAcrossSubjSmall = std(smallDriftCoef);
meanDriftCoefAcrossSubjLarge = mean(largeDriftCoef);
STDDCoefAcrossSubjLarge = std(largeDriftCoef);


end