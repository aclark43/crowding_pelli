function pptrials = checkMSTraces_new(trialId, pptrials, params, filepath, singleSegs, nameFile)


% microfaces data cleaning
% MAC - 4/17/2020 by JI - mostly good, some quantization errors
% M008 - 4/17/2020 by JI - some good, many quantization errors


if strcmp('D',params.machine)
    filepath = sprintf('%s/%s',filepath, params.session{1});
else
    filepath = filepath;
end
%test

if isempty(trialId)
    trialId = 1:length(pptrials);
end

PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    
    %     snellen = input('Is this the snellen task? (y/n)','s');
    figure('position',[200, 100, 1500, 800])
    
    trialCounter = 1;
    for driftIdx = 1:inf
        if (trialCounter) > length(trialId)
            return;
        end
        currentTrialId = trialId(trialCounter);
        
        if params.fixation
        else
            subplot(2,2,3:4)
        end
        hold off
        %         figure;
        %         if isempty('params')
        if params.fixation
            poiStart = ceil(pptrials{currentTrialId}.TimeFixationON);
            poiEnd = min(pptrials{currentTrialId}.TimeFixationOFF);
            axisWindow = 60;
        else
            if pptrials{currentTrialId}.TimeTargetOFF <= 0
                return;
            end
            poiStart = pptrials{currentTrialId}.TimeTargetON;
            poiEnd = min(pptrials{currentTrialId}.TimeTargetOFF);
            axisWindow = 40;
        end
        
        
        poi = fill([poiStart, poiStart ...
            poiEnd, poiEnd], ...
            [-50, 50, 50, -50], ...
            'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0);
        
        xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle;
        yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle;
        
        hold on
        if strcmp('D',params.machine)
            hx = plot(1:(1000/330):length(xTrace)*(1000/330),xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
            hy = plot(1:(1000/330):length(yTrace)*(1000/330),yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
            sampling = 1000/330;
            axis([0 180 0 150])
        else
            hx = plot(xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
            hy = plot(yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
            sampling = 1;
            axis([0 180 0 150])
        end
        axis([poiStart - 400, poiEnd + 400, -axisWindow, axisWindow])
        xlabel('Time','FontWeight','bold')
        ylabel('arcmin','FontWeight','bold')
        title(sprintf('Trial: %i out of %i', currentTrialId, length(pptrials))); %
        
        if strcmp('Stabilized', params.stabilization)
            %             for ii = 1:length(pptrials)
            if isfield(pptrials{currentTrialId},'XStab')
                hold on
                plot(pptrials{currentTrialId}.XStab.ts*(1000/330),...
                    pptrials{currentTrialId}.XStab.stream,'-*','Color',[0 0 200] / 255, 'LineWidth', 2);
                
                hold on
                plot(pptrials{currentTrialId}.YStab.ts*(1000/330),...
                    pptrials{currentTrialId}.YStab.stream,'-*','Color',[0 180 0] / 255, 'LineWidth', 2);
            end
        end
        if params.fixation
        else
            subplot(2,2,1)
            
            [~,~,dc,~,~,singleSegs,~,~] = ...
                CalculateDiffusionCoef(sampling,struct('x',xTrace(ceil(poiStart/sampling):floor(poiEnd/sampling)), ...
                'y',yTrace(ceil(poiStart/sampling):floor(poiEnd/sampling))));
            %         [singleSegs,x,y] = getDriftSegmentInfo (msTRIALS, pptrials, params);
            plot(singleSegs);
            ylim([0 400]);
            title(dc);
            if singleSegs(140) > 300
                warning('This trial has large DC!')
            end
            subplot(2,2,3:4)
        end
        
        
        set(gca, 'FontSize', 12)
        poiMS = [1 0 0];
        poiS = [1 0 0];
        poiD = [0 1 0];
        poiN = [0 0 0];
        poiI = [0 .423 .521];
        poiB = [.749 .019 1];
        if strcmp('D',params.machine)
            poiMS = plotColorsOnTraces(pptrials, currentTrialId, 'microsaccades', [1 0 0], 1000/330); %red
            poiS = plotColorsOnTraces(pptrials, currentTrialId, 'saccades', [1 0 0], 1000/330); %red
            poiD = plotColorsOnTraces(pptrials, currentTrialId, 'drifts', [0 1 0], 1000/330); %green
            poiN = plotColorsOnTraces(pptrials, currentTrialId, 'notracks', [0 0 0], 1000/330); %black
            poiI = plotColorsOnTraces(pptrials, currentTrialId, 'invalid', [0 0 1], 1000/330); %blue
            poiB = plotColorsOnTraces(pptrials, currentTrialId, 'blinks', [.749 .019 1], 1000/330); %pink
        else
            for i = 1:length(pptrials{currentTrialId}.microsaccades.start)
                msStartTime = pptrials{currentTrialId}.microsaccades.start(i);
                msDurationTime = pptrials{currentTrialId}.microsaccades.duration(i);
                poiMS = fill([msStartTime, msStartTime ...
                    msStartTime + msDurationTime, ...
                    msStartTime + msDurationTime], ...
                    [-35, 35, 35, -35], ...
                    'r', 'EdgeColor', 'r', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25);
            end
        end
        %         if ~isempty(poiMS) || ~isempty(poiS) || ~isempty(poiD) || ~isempty(poiI)
        % figure;
        %         legend([hx, hy, poiMS, poiD, poiN, poiI, poiB], ...
        %             {'X','Y','MS/S','D','NoTrack','Invalid','Blink'},'FontWeight','bold')
        %         end
        pptrials{currentTrialId}.checkedManually = 1;
        contType = input...
            ('Saccade(1), Microsaccade(2), Drift(3), \n No Track(4), Invalid(5), Blinks(6), \n Back a trial(7), Stop(0), AutoPrune(8), Clear All (9), \n Fill In With Drift (10), Fix Spike (11), Undo spike fix (12), \n Next Trial(enter)?\n');
        cont = [];
        if contType == 1
            cont = input('Tag a Saccade(a) or Change Saccade Labelled(b)?','s');
            em = 'saccades'; % Post analysis will check amps and organize as MS or S
        elseif contType == 2
            cont = input('Tag a microsaccade(a) or Change Microsaccade Labelled(b)?','s');
            em = 'microsaccades'; % +- 10msec
        elseif contType == 3
            cont = input('Tag a Drift(a) or Change Drift Labelled(b)?','s');
            em = 'drifts';
        elseif contType == 4
            cont = input('Tag a No Track(a) or Change no track Labelling?(b)','s');
            em = 'notracks'; %flat no tracks
        elseif contType == 5
            cont = input('Tag a Invalid(a) or Change invalid Labelling?(b)','s');
            em = 'invalid'; %bad drifts
        elseif contType == 6
            cont = input('Tag a Blink(a) or Change a Blink Labelling?(b)','s');
            em = 'blinks';
        elseif contType == 7
            cont = 'z';
        elseif contType == 11
            cont = 'y';
        elseif contType == 12
            cont = 'u';
        elseif contType == 0
            cont = 's';
        elseif contType == 8
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 75);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 30);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10);
            
            MinSaccSpeed = 60;
            MinSaccAmp = 30;
            MinMSaccSpeed = 1;
            MinMSaccAmp = 2;
            MaxMSaccAmp = 60;
            MinVelocity = 10;
            
            pptrialsTemp1 = findSaccades(pptrials{currentTrialId}, ...
                'minvel',MinSaccSpeed, ...
                'minsa',MinSaccAmp);
            
            Trial = pptrials{currentTrialId};
            Events = findEvents(Trial.velocity, ...
                'minvel', MinVelocity, ...
                'minterval', 25, ...
                'mduration', 25 );                %%% YB@2018.10.27.
            
            % Filter all movements within a blink event
            Events = filterIntersected(Events, Trial.blinks);
            
            % Filter all movements within the track trace
            Events = filterIntersected(Events, Trial.notracks);
            
            % Filter all movements within the invalid list
            Events = filterIntersected(Events, Trial.invalid);
            
            % Filter all movements that do not fall within
            % the stimulus timeframe
            Events = filterNotIncluded(Events, Trial.analysis);
            
            % Calculate the amplitudes/angles of the movements
            counter = 1;
            startTemp = [];
            counterB = 1;
            blinks = [];
            %             if isempty(Events.start)
            for i = 1:Trial.samples-15
                if diff([Trial.x.position(i+15), Trial.x.position(i)]) > 6 && ...
                        diff([Trial.x.position(i+15), Trial.x.position(i)]) < 200
                    startTemp(counter) = abs(i-8);%Trial.x.position(i);
                    counter = counter + 1;
                elseif diff([Trial.x.position(i+5), Trial.x.position(i)]) > 75
                    blinks(counterB) = abs(i-8);
                    counterB = counterB + 1;
                    
                end
            end
            %             end
            
            for l = 1:length(startTemp)-1
                if diff([startTemp(l), startTemp(l+1)]) < 30
                    startTemp(l+1) = startTemp(l);
                end
            end
            
            for l = 1:length(blinks)-1
                if diff([blinks(l), blinks(l+1)]) < 30
                    blinks(l+1) = blinks(l);
                end
            end
            b = unique(floor(blinks));
            idxB = [];
            temp = [];
            for i = 1:length(b)
                temp = blinks(find(b(i) == blinks));
                 idxB(i,1) = temp(1);
                 idxB(i,2) = length(temp)+30;
            end
            b = [];
           
            idxB(idxB == 0) = 1;
            if isempty(idxB)
                pptrials{currentTrialId}.blinks.start = [];
                pptrials{currentTrialId}.blinks.duration = [];
            else
                pptrials{currentTrialId}.blinks.start = round(idxB(:,1)');
                pptrials{currentTrialId}.blinks.duration = idxB(:,2)';
            end
            x = Trial.x.position;
            idxN = [];
            idxN = find(diff(x) == 0)
            for l = 1:length(idxN)-1
                if diff([idxN(l), idxN(l+1)]) < 30
                    idxN(l+1) = idxN(l);
                end
            end
            n = unique(floor(idxN));
            n(n == 0) = 1;
            n = unique(floor(n));
            for i = 1:length(n)
                temp = idxN(find(n(i) == idxN));
                 idxNo(i,1) = temp(1);
                 idxNo(i,2) = length(temp)+15;
            end
%             pptrials{currentTrialId}.notracks.start = round(idxNo(:,1)');
%             pptrials{currentTrialId}.notracks.duration = idxNo(:,2)';
%             
            t = unique(floor(startTemp));
            t(t == 0) = 1;
            Events.start = t;
            Events.duration = ones(1,length(Events.start))*25;
            Events = updateAmplitudeAngle(Trial, Events);
            
            % Filter all movements with an amplitude according to
            % minimum and maximum amplitude
            Events = filterBy('amplitude', Events, ...
                MinMSaccAmp, MaxMSaccAmp);
            
            % Save the filtered events into the trial
            if ~isempty(Events)
                pptrialsTemp2.microsaccades = Events;
            end
            
            pptrials{currentTrialId}.saccades = pptrialsTemp1.saccades;
            pptrials{currentTrialId}.microsaccades = pptrialsTemp2.microsaccades;
            
            continue;
        elseif contType == 9
            pptrials = autoPrune( pptrials, currentTrialId, 'drifts', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'microsaccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'saccades', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'blinks', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'invalid', 10000);
            pptrials = autoPrune( pptrials, currentTrialId, 'notracks', 10000);
            continue;
        elseif contType == 10
            
            Trial = pptrials{currentTrialId};
            AllMovements = joinEvents(Trial.saccades, Trial.microsaccades);
            AllMovements = joinEvents(AllMovements, Trial.blinks);
            AllMovements = joinEvents(AllMovements, Trial.notracks);
            AllMovements = joinEvents(AllMovements, Trial.invalid);
            Events = invertEvents(AllMovements, Trial.samples);
            Events = updateAmplitudeAngle(Trial, Events);
            pptrials{currentTrialId}.drifts  = Events;
            cont = 'f';
        else
            trialCounter = trialCounter + 1;
            continue;
        end
        
        if cont == 's'
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            break;
        elseif cont == 'a'
            numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;%input('Which ones (in order) do you want to tag?');
            startTime = input('What is the start time?');
            duration = input('What is the end time?');
            if strcmp('D',params.machine)
                pptrials{currentTrialId}.(em).start(numTaggedInTrace) = ...
                    round(startTime/(1000/330));
                pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = ...
                    round((duration/(1000/330)) - round(startTime/(1000/330)));
            else
                pptrials{currentTrialId}.(em).start(numTaggedInTrace) = ...
                    round(startTime);
                pptrials{currentTrialId}.(em).duration(numTaggedInTrace) = ...
                    round((duration) - round(startTime));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'b'
            fprintf('Total start times: %.0f \n', round(pptrials{currentTrialId...
                }.(em).start, 3)*(1000/330))
            startTime = input('When does the wrong EM start?');
            newStartTime = input('When does the EM actually start? (set to 0 if not present)');
            newDurationTime = input('When does the EM end? (set to 0 if not present)');
            for numW = 1:length(startTime)
                wrong = find((pptrials{currentTrialId}.(em).start) == round(startTime(numW)/(1000/330)));
                pptrials{currentTrialId}.(em).start(wrong) = round(newStartTime(numW)/(1000/330));
                pptrials{currentTrialId}.(em).duration(wrong) = ...
                    (round(newDurationTime(numW)/(1000/330))) - round(newStartTime(numW)/(1000/330));
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'z'
            trialCounter = trialCounter-1;
            continue;
        elseif cont == 'f'
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'y' %%fixing spike
            man_or_auto = input('Manual (1) or Automatic (2)?');
            %             numTaggedInTrace = length(pptrials{currentTrialId}.(em).start)+1;%input('Which ones (in order) do you want to tag?');
            if man_or_auto == 2
                hold on
                [~,peakidxX] = findpeaks(abs(pptrials{currentTrialId}.x.position),'MinPeakDistance',25);
                hold on
                plot(peakidxX*(1000/330),pptrials{currentTrialId}.x.position(peakidxX)+ pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle,'o')
                for i = 1:length(peakidxX)
                    text(peakidxX(i)*(1000/330),...
                        double(pptrials{currentTrialId}.x.position(peakidxX(i)))+ ...
                        pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pixelAngle,...
                        string(i),'FontSize',14);
                end
                xpeaks = input('Which X Peaks?');
                pptrials{currentTrialId}.x.position(peakidxX(xpeaks)) = ...
                    pptrials{currentTrialId}.x.position(peakidxX(xpeaks) - 1);
                pptrials{currentTrialId}.y.position(peakidxX(xpeaks)) = ...
                    pptrials{currentTrialId}.y.position(peakidxX(xpeaks) - 1);
                
                [~,peakidxY] = findpeaks(abs(pptrials{currentTrialId}.y.position),'MinPeakDistance',25);
                hold on
                plot(peakidxY*(1000/330),pptrials{currentTrialId}.y.position(peakidxY)+ ...
                    pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle,'o')
                for i = 1:length(peakidxY)
                    text(peakidxY(i)*(1000/330),...
                        double(pptrials{currentTrialId}.y.position(peakidxY(i))+ pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pixelAngle),...
                        string(i),'FontSize',14);
                end
                ypeaks = input('Which Y Peaks?');
                pptrials{currentTrialId}.y.position(peakidxY(ypeaks)) = ...
                    pptrials{currentTrialId}.y.position(peakidxY(ypeaks) - 1);
                pptrials{currentTrialId}.x.position(peakidxY(ypeaks)) = ...
                    pptrials{currentTrialId}.x.position(peakidxY(ypeaks) - 1);
                
            elseif man_or_auto == 1
                startTime = input('What is the start time?');
                duration = input('What is the end time?');
                
                saveTillNextTime{1} = pptrials{currentTrialId}.x.position;
                saveTillNextTime{2} = pptrials{currentTrialId}.y.position;
                if strcmp('D',params.machine)
                    pptrials{currentTrialId}.x.position...
                        (startTime/(1000/330):duration/(1000/330)) = ...
                        pptrials{currentTrialId}.x.position(round(startTime/(1000/330)));
                    
                    pptrials{currentTrialId}.y.position...
                        (startTime/(1000/330):duration/(1000/330)) = ...
                        pptrials{currentTrialId}.y.position(round(startTime/(1000/330)));
                    
                else
                    pptrials{currentTrialId}.x.position...
                        (startTime:duration) = ...
                        pptrials{currentTrialId}.x.position(startTime);
                    
                    pptrials{currentTrialId}.y.position...
                        (startTime:duration) = ...
                        pptrials{currentTrialId}.y.position(startTime);
                end
            end
            
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        elseif cont == 'u'
            fprintf('Redoing last spike tag \n')
            %             if strcmp ('y',answer)
            %                 saveTillNextTime{1} = pptrials{currentTrialId}.x.position;
            %                 saveTillNextTime{2} = pptrials{currentTrialId}.y.position;
            if strcmp('D',params.machine)
                pptrials{currentTrialId}.x.position...
                    (startTime/(1000/330):duration/(1000/330)) = ...
                    saveTillNextTime{1}(startTime/(1000/330):duration/(1000/330));
                
                pptrials{currentTrialId}.y.position...
                    (startTime/(1000/330):duration/(1000/330)) = ...
                    saveTillNextTime{2}(startTime/(1000/330):duration/(1000/330));
                
            else
                pptrials{currentTrialId}.x.position...
                    (startTime:duration) = ...
                    saveTillNextTime{1}(startTime:duration);
                
                pptrials{currentTrialId}.y.position...
                    (startTime:duration) = ...
                    saveTillNextTime{2}(startTime:duration);
            end
            fprintf('Saving pptrials\n')
            save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
            continue;
        end
        
        save(fullfile(sprintf('%s',filepath), nameFile), 'pptrials')
        trialCounter = trialCounter + 1;
    end
end

close

end
