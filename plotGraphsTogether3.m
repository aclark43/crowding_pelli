%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
clear all
close all
clc

%%Figure Location & Name%%
subject1 = {'Z023','Ashley','Z005','Z002','Z013',...
    'Z024','Z064','Z046','Z084','Z014',...
    'Z091','Z138','Z181'};
subject2 = {'Z023','Ashley','Z005','Z002','Z013',...
    'Z024','Z064','Z046','Z084','Z014',...
    'Z091','Z138','Z181'};

% subject = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014','Z070','Z063'};
condition1 = {'Uncrowded'}; %Figure 1 and Figure 2
condition2 = {'Crowded'}; %Figure 1 and Figure 2
stabilization1 = {'Unstabilized'};
stabilization2 = {'Unstabilized'};
em1 = {'Drift'};
em2 = {'Drift'};
ecc1 = {'0ecc'};
ecc2 = {'0ecc'};
%  em3 = {'Drift_MS'};%%%%%%%%%%%%%%%%%
%  ecc3 = {'25ecc'};%%%%%%%%%%%%%%%%%%
% c =  brewermap(2,'Paired');
% c =  brewermap(2,'Paired');
c = [0 0 0; .5 .5 .5];



for ii = 1:length(subject1)
    title1 = sprintf('%s_%s_%s_%s_%s', subject1{ii}, condition1{1}, stabilization1{1},...
        em1{1}, ecc1{1}); %Name of File FIG 1
    title2 = sprintf('%s_%s_%s_%s_%s', subject2{ii}, condition2{1}, stabilization2{1},...
        em2{1}, ecc2{1}); %Name of File FIG 2
    %                                     title2 = sprintf('NoToss%s_%s_%s_%s_%s', currentSubj, currentCond2, currentStab2, currentEm2, currentEcc2); %Name of File FIG 2
    currentSubj = subject1{ii};
    
    pathToData1 = sprintf('../../Data/%s/Graphs/FIG', subject1{ii}); %File Location
    pathToData2 = sprintf('../../Data/%s/Graphs/FIG', subject2{ii}); %File Location
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    color1 = c(1,:);
    
   fH1 = createGraphTogetherMultipleSubjWithText...
        (title1,pathToData1,condition1{1}, stabilization1{1},...
        em1{1}, ecc1{1},color1,0.1);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% FIGURE 2 %%
    %%%%%%%%%%%%%%
    
    color2 = c(2,:);
    fH2 = createGraphTogetherMultipleSubjWithText...
        (title2,pathToData2,condition2{1}, stabilization2{1},...
        em2{1}, ecc2{1},color2,0.1);
%     xlim([0 4])
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %% Figures Together%%
    title_str = sprintf('%s', subject1{1});
    title(title_str);
    set(gca, 'FontSize', 8, 'FontWeight', 'bold');
    
    ax1 = get(fH1, 'Children');
    ax2 = get(fH2, 'Children');
    ax2p = get(ax2(1),'Children');
    copyobj(ax2p, ax1(1));
    
   
    %                                     title('RBG');
    close Figure 2
    title_str = sprintf('%s', currentSubj);
    title(sprintf('S%i',ii));
    set(gca, 'FontSize', 14);
    yticks([.2 .4 .6 .8 1])
    xlabel('Threshold Acuity (arcmin)')
    if ii == 3
        xlim([0 10])
    else
    xlim([0 5])
    end
    
    %                                         saveas(gcf, sprintf('../../Data/%s/Graphs/FIG/%s.fig', currentSubj, sprintf('%s_VS_%s',title1,title2)));
    %                                         saveas(gcf, sprintf('../../Data/%s/Graphs/JPEG/%s.png', currentSubj, sprintf('%s_VS_%s',title1,title2)));
    %                                         saveas(gcf, sprintf('../../Data/%s/Graphs/%s.epsc', currentSubj, sprintf('%s_VS_%s',title1,title2)));
    saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/PsychCombined/%i_CrowdedUncrowded.epsc', ii));
%     saveas(gcf, sprintf('../../Data/AllSubjTogether/NoTossPsych/%s.png', sprintf('%s_VS_%s',title1,title2)));
    
%     saveas(gcf, sprintf('../../Data/AllSubjTogether/NoTossPsych/%s.epsc', sprintf('%s_VS_%s',title1,title2)));
    close all
    % saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PyschCurvesForSmallVsLargeSubject.png');
    %    saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/PyschCurvesForSmallVsLargeSubject.epsc');
    %
end


% ddpi = [1.43 2.16 1.3 1.48 2.11 1.797]
% dpi = [1.43 2.11 1.19 1.9 1.4 1.77]
% [~,p] = ttest(ddpi,dpi);
%
% % subj = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014','Z063','Z070'};
% thresh = [1.40    1.19   1.64   1.43   1.23   1.77    1.61   1.48   1.74   1.67   1.09  2.88 ]; %Uncrowded
% threshNoToss = [1.59    1.2    1.51   1.08   1.22   1.84    1.85   0.93   1.72   1.60   1.05  2.86 ]; %Uncrowded
%
% % % subj =     {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014','Z063','Z070'};
% % thresh =  [2.11   1.77     3.18   1.90   1.51   2.35   2.10   1.97   2.30   1.93   1.83  3.15 ];%Crowded
% % threshNoToss=[2.03     1.77      3.27   1.75   1.5    2.39   2.35   1.99   2.27   1.87   1.79  3.30 ];%Crowded
%
% nc = brewermap(12,'Set3');
%
% figure
% scatter(thresh,threshNoToss)
% axis([0 4 0 4])
% hline = refline;
% hline.Color = 'k'
% % axis([0 3 0 3])
% hold on
% for ii = 1:length(thresh)
%     scatter(thresh(ii),threshNoToss(ii),100,nc(ii,:),'filled')
%     hold on
% end
% % axis([0 3 0 3])
% % hline = refline;
% % hline.Color = 'k'
% % axis([0 3 0 3])
% [~,p,~,r] = LinRegression(thresh,threshNoToss,0,NaN,1,0);


