%runGetTossParamsHux

for ii = 1:length(subjectsAll)
    filePath = sprintf(...
        'C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/TossedInformation.xlsx',subjectsAll{ii})
C = readcell(filePath,"Sheet","Sheet1","Range","B2:U2")

biggOffset = C{1};
totalTrials = C{20};
propToss(ii) = 100*(biggOffset/totalTrials);

end

mean(propToss)