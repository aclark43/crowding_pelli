function [critFreq, leg, sumPower] = powerAnalysis_JustCriticalFrequency (dsq, normalize)
% dsq           vector of diffusion constants
% normalize     logical whether you want the power normalized or not
%
% OUTPUTS
% critFreq      the spatial frequency that corresponds to the highest power
% leg           legend entries if multiple subjects
% sumPower      the summed power between 30 and 60cpd (defined on line 52)
%               only outputs from normalized values.

% figure;

for ii = 1:length(dsq) %number of subjects
    c = bone(length(dsq));
    f = linspace(1, 80, 80); % temporal frequencies (Hz)
    k = logspace(-1, 2, 100); % spatial frequencies (cpd)
    
    dc = dsq(ii);
    ps = Qfunction(f, k, dc / 60^2); % assumes isotropic
    powerDist = [];

    if normalize
        if ii == 1
            c = 'k';
        else
            c = [0.5 0.5 0.5];
        end
        tempNonNorm = pow2db(nansum(ps(f >=2 & f <= 40, :), 1)); 
        temp = (-(normalizeVector(pow2db(nansum(ps(f >=2 & f <= 40, :), 1))))+2)/10+.9; %%add specific values to get from 0 to 1 for graphing purposes
        leg(ii) = plot(k, temp, ...
            '-', 'linewidth', 4, 'Color', c);
        set(gca, 'XScale', 'log', 'XTick', [1, 10, 20, 30, 50], ...
            'FontSize', 17,'box','off');
        line([60 60],[.7 1.05]);
%         set(gca, 'YScale', 'log', 'XTick', [1, 10, 30, 50], ...
%             'FontSize', 17,'box','off');
%           set(gca, 'XTick', [1, 10, 30, 50], ...
%             'FontSize', 17,'box','off');
        xlim([-6 80])
        ylim([0 1.1])
        ylabel({'Normalized','Temporal Power'});
        xlabel('Spatial Frequency (cpd)');
        axis square;
        hold on
        power.All = pow2db(nansum(ps(f >=2 & f <= 40, :), 1));
        spatial.All = k;
        
        [~,I] = max(pow2db(nansum(ps(f >=2 & f <= 40, :), 1)));
        critFreq(ii) = k(I);
        line([critFreq(ii) critFreq(ii)],[0 2],...
            'Color',c,'LineStyle','--','LineWidth',2);

        spatialEnhancedTemp = spatial.All(find(power.All > -7));
        spatialEnhanced(ii,1) = max(spatialEnhancedTemp);
        spatialEnhanced(ii,2) = min(spatialEnhancedTemp);
        hold on
        
        windowK = find(k >= 30 & k<= 60);
        sumPower(ii) = sum(tempNonNorm(windowK));
    else
        leg(ii) = plot(k, pow2db(nansum(ps(f >=2 & f <= 40, :), 1)), ...
            '-', 'linewidth', 2, 'Color', c(ii,:));
        set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
%         set(gca, 'YScale', 'log', 'XTick', [1, 10, 30, 50]);
%         set(gca, 'XTick', [1, 10, 30, 50]);

        ylabel('PSD (db)');
        title('temporal power in RGC range');
        xlabel('spatial frequency (cpd)');
        %         axis square;
        hold on
        
        power.All = pow2db(nansum(ps(f >=2 & f <= 40, :), 1));
        spatial.All = k;
        
        [~,I] = max(pow2db(nansum(ps(f >=2 & f <= 40, :), 1)));
        critFreq(ii) = k(I);
        
        
%         spatialEnhancedTemp = spatial.All(find(power.All > -7));
%         spatialEnhanced(ii,1) = max(spatialEnhancedTemp);
%         spatialEnhanced(ii,2) = min(spatialEnhancedTemp);
        sumPower = NaN;
    end
end


