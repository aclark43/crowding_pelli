% runPerformance

nameTrials = fieldnames(eis_data.user_data.variables);

counter = 1;
for ii = 1:length(nameTrials)
    if startsWith(nameTrials{ii},'trial')
        path = eis_data.user_data.variables.(nameTrials{ii});
        temp = regexp(nameTrials{ii},'\d*','Match');
        data(counter).id = str2double(temp{1});
        data(counter).correct = path.Correct;
        data(counter).size = ((path.TargetStrokewidth/5)*path.pixelAngle); 
        counter = counter + 1;
    end
end
   

  [thresh, par, threshB, xi, yi, chiSq] = ...
        psyfitCrowding([data.size], ...
        [data.correct], 'DistType', ...
        'Normal','Chance', .25, 'Extra');
    
 snellenAcuity = (thresh)*20
