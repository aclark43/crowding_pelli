clear
clc
close all

%% Subject Notes
%   Z048 = seems to need ~50-100 of practice during beginning of session
%   in order to get more stable performance.
%   Z070 = quit study before we could collect more data - Dift Only Low
%   Z063 = quit study before we could collect more data - Drift Only Low
%   Z090 = pyschometric curves were inconsistent across sessions
% HUX 13 = NOISY; lots of head movement
% HUX 11 = High saccade/ms rate
% HUX 16 = ses2 NOISY; not a lot of accurate tags
% HUX4 = ses2 not very brownian drift - head motion?
%% Params

figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial with traces in 2D space
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',0,... %Plots psychometric curves
    'QUICK_ANALYSIS',1,... %Only plots psychometric curves
    'CHECK_TRACES',0,... %will plot traces
    'HUX_RUN',0,... %will discard trials differently
    'FOLDED', 0,...
    'FIXED_SIZE', 0,...
    'COMPARE_SPANS',0); %0 = all spans, 1 = small spans, 2 = large spans

machine = {'D'}; %(D = DDPI, A = DPI)
% subjectsAll = {'Z084DDPI', 'Z002DDPI','AshleyDDPI','Z046DDPI','Zoe','Z091'};%,'Z014'};%'Z023','Ashley','Z005','Z002','Z013'};%
% subjectsAll = {'Z023DDPI','AshleyDDPI','Z002DDPI'};
% subjectsAll = {'HUX21','HUX22','HUX23','Martina'};
% subjectsAll = {'Z024','Z064','Z046','Z084','Z014'};% DDPI SUBJECTS
% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013'};%};% DPI SUBJECTS
% subjectsAll = {'Z002DDPI','Z084DDPI','Z046DDPI'};%,'Ashley','Z005','Z002','Z013','Z063'};%
subjectsAll = {'Z024','Z064','Z046','Z084','Z014',...
   'Z091','Z138','Z181'};%

% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013',...
%    };%,

% subjectsAll = {'HUX1','HUX3','HUX6','HUX7','HUX8','HUX9','HUX11','HUX13','HUX14','HUX16',...
%     'HUX4','HUX18','HUX10','HUX12','HUX17','HUX21','HUX22','HUX23','Martina','HUX24',...
%     'HUX25','HUX27','HUX28','HUX29','HUX30'};

% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013'};%,...
% subjectsAll = {'Z024','Z064','Z046','Z084','Z014',...
%     'Z091','Z138','Z181'};
% subjectsAll = {'Z024','Z046','Z064','Z084','Z014',...
%    'Z091','Z138','Z181','Z091','Z138','Z181'};
% subjectsAll = {'Z138','Z181'};%'Ashley','Z023','Z013','Z005'};%,'Z091',}; %zoe
% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013'};%,...
%     subjectsAll = {'Z024',
% 'Z064','Z046','Z084','Z014',...
%     'Z138','Z181'};%,...; %cntrols'Z091',  ,
% subjectsAll = {'Z046DDPI'};%'HUX27','HUX28','HUX29','HUX30'};
% subjectsAll = {'HUX27'};%{'Zoe','Z091'};
% {'Ashley','Z013','Z023','Z005':'HUX5','HUX1','HUX3','HUX4','HUX6','HUX7','HUX8','HUX9','HUX11',...
%     'HUX13','HUX14','HUX16','HUX4','HUX18','HUX10','HUX12','
%
% subjectsAll = {'HUX17','HUX21','HUX22',...
%     'HUX23','Martina','HUX24','HUX25','HUX27','HUX28','HUX29','HUX30'}; %,'AshleyDDPI'};HUX17
% 0,0,1,0,0,0,0,0,...
%     0,0,0,1,1,0,0,
% sampingRateChange = [0,1,1,...
%     1,1,1,1,1,1,1,1];

conditionsAll = {'Uncrowded','Crowded'};
eccAll = {'0ecc'};
% eccAll = {'120ecc'};
% 
% eccAll = {'10ecc','15ecc','25ecc'};
% eccAll = {'15eccTemp','25eccTemp'};%'10eccNasal',
% eccAll = {'10eccTemp','15eccTemp','25eccTemp'};
% eccAll = {'10eccNasal','15eccNasal','25eccNasal'...
%     '10eccTemp','15eccTemp','25eccTemp'};%Uncrowded Nasal Unstabilized 10

session = {'ALL'}; %%ALL


for ii = 1:length(subjectsAll)
    for jj = 1:length(conditionsAll)
        for kk = 1:length(eccAll)
            
            subject = subjectsAll(ii);%'Z002','Z023','Z013','Z005','Z002','Z063'};%Z063
         
            condition = conditionsAll(jj); %Crowded or Uncrowded
            ecc = eccAll(kk);
            
            em = {'Drift'}; %Drift,MS,Drift_MS
            stabParam = {'Unstabilized'};
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% Based on parameters set above%%
            
            params.subject = subjectsAll{ii};
            params = buildParams(machine, session, figures.COMPARE_SPANS, subject, condition);
            params.fixation = figures.FIXATION_ANALYSIS;
            params.ecc = ecc;
            params.FIXED_SIZE = figures.FIXED_SIZE;
            close all
            finalTable = [];
            counter = 1;
            
            for emIdx = 1:length(em)
                currentEm = em{emIdx};
                for condIdx = 1:length(condition)
                    currentCond = condition{condIdx};
                    for subIdx = 1:length(subject)
                        currentSubj = subject{subIdx};
                        for stabIdx = 1:length(stabParam)
                            currentStab = stabParam{stabIdx};
                            for eccIdx = 1:length(ecc)
                                currentEcc = ecc{eccIdx};
                                
                                params = determineParams(params, currentEm, currentCond);
                                params.ecc = currentEcc;
                                params.ses = session{1};
                                params.subject = currentSubj;
                                params.em = currentEm;
                                params.stabilization = currentStab;
                                params.session = session;
                                filepath = sprintf...
                                    ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/%s_%s_%s', ...
                                    currentSubj, currentCond, currentStab, currentEcc);
                                
                                title_str = sprintf('%s_%s_%s_%s_%s',currentSubj, currentCond, ...
                                    currentStab, currentEm, currentEcc);
                                
                                pathtodata = fullfile(filepath, params.session);
                                if strcmp('D',params.machine)
                                    params.sampling = 341;
%                                     if sampingRateChange(ii) == 0
%                                         params.sampling = 330;
%                                     else
%                                         params.sampling = 341;
%                                     end
                                else
                                    params.sampling = 1000;
                                end
                                
                                if strcmp('ALL',params.session) && figures.CHECK_TRACES
                                    error('You cant check traces for ALL sessions silly! Feed in only one at a time.');
%                                 else
%                                     dirT = sprintf('%s/pptrials.mat', pathtodata);
%                                     if exist(dirT, 'file') == 0 %%%if this session & condition doesn't exist
%                                         warning('This condition doesnt exist, skipping!');
%                                         continue;
%                                     end
                                end
                            end
                            if (strcmp('0ecc',params.ecc) || strcmp('0eccEcc', params.ecc)) && figures.FOLDED
                                error('You cant fold 0ecc data.');
                            end
                            %                                 if strcmp('Drift_MS',currentEm) && figures.CHECK_TRACES
                            %                                     error('Make sure youre looking for drift OR ms in these!')
                            %                                 end
                            if figures.COMPARE_SPANS == 1 || figures.COMPARE_SPANS == 2
                                if ~figures.QUICK_ANALYSIS
                                    error('Must use Quick Analysis in order to check different spans.');
                                end
                            end
                            if figures.COMPARE_SPANS == 1 || figures.COMPARE_SPANS == 2
                                if ~strcmp('Drift',currentEm)
                                    error('Must use DRIFT trials only.');
                                end
                            end
                            
                            %%
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            [traces, threshInfo, trialChar, em] = ...
                                oneSubjectPelli(params, filepath, figures, stabParam, condition, title_str, currentSubj);
                           

                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            %%
                            
%                             figure;
%                             for tt = 1:size(em.allTraces.SingleSegmentDsq)
%                                 plot(em.allTraces.SingleSegmentDsq(tt,1:255));
%                                 hold on ;
%                             end
                            
                            if ~figures.QUICK_ANALYSIS
                                driftsFilename = sprintf('MATFiles/%s_DriftChar.mat',title_str);
                                save(driftsFilename,'em')
                                
                                if ~figures.FIXATION_ANALYSIS
                                    threshInfo.em = em;
                                    %                                     filename = sprintf('MATFiles/%s_ThresholdTestingAfterEmAnalysisChange.mat',title_str);
                                    filename = sprintf('MATFiles/%s_Threshold.mat',title_str);
                                    save(filename,'threshInfo')
                                    
                                    threshStruct = buildThreshStuct (currentSubj,threshInfo,currentCond,...
                                        currentStab, currentEm, currentEcc);
                                    %
                                elseif figures.FIXATION_ANALYSIS %&& ~strcmp('ALL',session{1})
%                                     threshInfo.em = em;
                                    filename = sprintf('MATFiles/%s_Threshold_Fixation_%s.mat',title_str,session{1});
                                    save(filename,'em')
                                    
%                                     threshStruct = buildThreshStuct (currentSubj,threshInfo,currentCond,...
%                                         currentStab, currentEm, currentEcc); 
                                end
                            else
                                filename = sprintf('MATFiles/%s_BootstrapThreshold.mat',title_str);
                                save(filename,'threshInfo')
                                
                                if figures.COMPARE_SPANS == 1
                                    
                                    threshStruct = buildThreshStuct (currentSubj,threshInfo,currentCond,...
                                        currentStab, currentEm, currentEcc);
                                    
                                    filename = sprintf('MATFiles/%s_SmallSpanPerform.mat',title_str);
                                    save(filename,'threshStruct')
                                    
                                    
                                elseif figures.COMPARE_SPANS == 2
                                    threshStruct = buildThreshStuct (currentSubj,threshInfo,currentCond,...
                                        currentStab, currentEm, currentEcc);
                                    
                                    filename = sprintf('MATFiles/%s_LargeSpanPerform.mat',title_str);
                                    save(filename,'threshStruct')
                                    
                                end
                            end
                        end
                    end
                end
                
            end
            
        end
    end
end



% figure;
% for ii = 1:size(SingleSegmentDsq)
%     plot(SingleSegmentDsq(ii,1:255));
%     hold on ; 
% end

% end
% close all


