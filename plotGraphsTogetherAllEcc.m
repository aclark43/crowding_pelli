%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

close all
clc
clear

%%Figure Location & Name%%
subjects = {'AshleyDDPI'};
% subjects = {'AshleyDDPI','Z002DDPI','Z005','Z023','Z002','Ashley','Z013','Zoe'};
% subjects = {'Zoe'};
condition = {'Uncrowded'}; %Figure 1 and Figure 2
stabilization = {'Unstabilized'};
em = {'Drift_MS'};

% ecc = {'25eccTemp',...
%     '15eccTemp',...%%%%%%%%%%%%%%%%%%
%     '10eccTemp',...
%     '0ecc',...
%     '10eccNasal',...
%     '15eccNasal',...%%%%%%%%%%%%%%%%%%
%     '25eccNasal'};

ecc = {'0ecc',...
'10ecc',...
'15ecc',...%%%%%%%%%%%%%%%%%%
'25ecc'};%%%%%%%%%%%%%%%%%%

for subCount = 1:length(subjects)
%     c = copper(length(ecc));
    c = brewermap(4,'Dark2');
    for ii = 1:length(ecc)
        titleFigs{ii} = sprintf('%s_%s_%s_%s_%s', ...
            subjects{subCount}, condition{1}, stabilization{1}, em{1}, ecc{ii});
        
        pathToData = sprintf('../../Data/%s/Graphs/FIG', subjects{subCount}); %File Location
        color = c(ii,:);
        fH{ii} = createGraphTogetherMultipleSubj(titleFigs{ii},pathToData,color);
    end
    pathToData = ('../../Data/'); %File Location
    
    
    for ii = 1:length(ecc)
        ax = get(fH{ii}, 'Children');
        axP{ii} = get(ax(1),'Children');
        axSaved{ii} = ax;
    end
    
    for ii = 2:length(ecc)
        copyobj(axP{ii},axSaved{1})
    end
    
    %figures to keep
    figs2keep = [1];
    all_figs = findobj(0, 'type', 'figure');
    delete(setdiff(all_figs, figs2keep));
    
    
    title_str = sprintf('%s,%s', subjects{subCount}, condition{1});
    title(title_str);
    set(gca, 'FontSize', 11, 'FontWeight', 'bold');
%     xlim([1.25 2.5])
    
    locText = .25:.05:1;
    for ii = 1:length(ecc)
        text(0.25,locText(ii),ecc{ii},...
            'Color',[c(ii,1) c(ii,2) c(ii,3)]);
    end
    
    saveas(gcf, ...
    sprintf('../../Data/%s/Graphs/ECCAll%s%s.png',...
    subjects{subCount},condition{1}));
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/individualCurves/ECCAll%s%s%s.png',...
    subjects{subCount},condition{1},stabilization{1}));
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/individualCurves/ECCAll%s%s%s.epsc',...
    subjects{subCount},condition{1},stabilization{1}));
    
    for ii = 1:length(ecc)
        currentS = (condition{1});
        fileName = sprintf('%s_%s_Unstabilized_%s_%s_Threshold.mat', ...
            subjects{subCount},condition{1},em{1},ecc{ii});
        folderName = 'MATFiles/';
        file1 = fullfile(folderName, fileName);
        load(file1);
        %                                             subjectThresh(ii) = threshInfo;
        allThresh(ii) = threshInfo.thresh;
        
        thresholds(subCount).subject = subjects{subCount};
        thresholds(subCount).thresh(ii) = allThresh(ii);
        thresholds(subCount).numTrials(ii) = threshInfo.em.numValidTrials;
        k = strfind(ecc{ii},'Temp');
        if isempty(k)
            thresholds(subCount).ecc(ii) = str2double(cell2mat(regexp(ecc{ii},'\d*','Match')));
        else
            thresholds(subCount).ecc(ii) = -(str2double(cell2mat(regexp(ecc{ii},'\d*','Match'))));
        end
    end
    
end
% %
% %
% % allThresholds(counter,:) = allThresh;
% % [h,p,ci,stats] = ttest(allThresh);
% % averageThresh = mean(allThresh);
% % corThresh = 0.625;
% % sigma = std(allThresh);
% % sigma2 = std(corThresh);
% %
% % hold on
% % errorbar(averageThresh,corThresh,abs((ci(1)-ci(2))/2),...
% %     'horizontal', '-ok','MarkerSize',10,...
% %     'MarkerEdgeColor','black',...
% %     'MarkerFaceColor','black',...
% %     'CapSize',18,...
% %     'LineWidth',5)
% % counter = counter+1;
% % xlim([0 4])
% % xticks([0.5 1 1.5 2 2.5 3 3.5])
% % xticklabels({'0.5','1','1.5','2','2.5','3', '3.5'})

% xlim([0 8])
% xticks([1:1:8])
% xticklabels({'1','2','3','4','5','6','7','8'})
% xlabel('Strokewidth in arcmin');
figure;
for ii = 1:length(thresholds)
    plot(thresholds(ii).ecc,thresholds(ii).thresh,'-o')
    for i = 1:length(thresholds(ii).numTrials)
    text(thresholds(ii).ecc(i)+2,thresholds(ii).thresh(i), sprintf('%i',thresholds(ii).numTrials(i)));
    end
    hold on
end
% legend(thresholds(1).subject, (thresholds(2).subject))
legend(subjects)

%negative = nasal, positive = temporal
xticks([-25,-15,-10,0,10,15,25])
xlabel('Target Eccentricity (arcmin)')
ylabel('Threshold Strokewidth')
title(sprintf('%s Threshold Strokewidths at Different Ecc', condition{1}))

saveas(gcf, ...
    sprintf('../../SummaryDocuments/GraphsforSummaryDocs/IndEcc/ALLThresh%s.png',...
    condition{1}));
% saveas(gcf, '../../SummaryDocuments/HuxlinPTFigures/UncrowdedPsychAll.png');

close all



% title_str = sprintf('%s, %s', subjects{1}, condition1{1});
% title(title_str);
% set(gca, 'FontSize', 20, 'FontWeight', 'bold');
% axis([0 5 0 1])
%
% % saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/%s.epsc', currentSubj, sprintf('%s_VS_%s',title1,title2)));
% saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/%s.png', subjects{1}, sprintf('%s_VS_%s',title1,title2)));

% saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/FIG/%s.fig', subject{1}, sprintf('%s_VS_%s_VS_%s',title1,title2,title3,title4)));
% saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/JPEG/%s.png', subject{1}, sprintf('%s_VS_%s_VS_%s',title1,title2,title3, title4)));
% saveas(gcf, sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/Graphs/%s.epsc', subject{1}, sprintf('%s_VS_%s_VS_%s',title1,title2,title3, title4)));

%                                                                                                  close all






