load('X:\SchizVIs\Collected_Ashley09202022\P07\uncrow_ses1.mat');

load('X:\SchizVIs\Collected_Ashley09202022\P08_S\ses1.eis')

fname = ['X:\SchizVIs\Collected_Ashley09202022\P08_S\ses1.eis','/results.mat'];
% data = eis_eisdir2mat(pt, list, fname);
Trial = eis_openTrial(['X:\SchizVIs\Collected_Ashley09202022\P08_S\' 'ses1']);

dateCollect = datetime(2022,09,20);
pptrials = convertDataToPPtrials(eis_data, dateCollect);




xFix = []; yFix = []; counterfix = 1;
xAll = []; yAll = []; countertrial = 1;
for ii = 1:length(pptrials)
    idx =  find( pptrials{ii}.x < 60 &  pptrials{ii}.x > -60 & ...
         pptrials{ii}.y < 60 &  pptrials{ii}.y > -60);
    if pptrials{ii}.fixationTrial
        xFix = [xFix pptrials{ii}.x(idx)];
        yFix = [yFix pptrials{ii}.y(idx)];
        counterfix = counterfix + 1;
    else
        xAll = [xAll pptrials{ii}.x(idx)];
        yAll = [yAll pptrials{ii}.y(idx)];
        countertrial = countertrial + 1;
    end
end

figure;
subplot(1,2,1)
generateHeatMapSimple( ...
    xFix, ...
    yFix, ...
    'Bins', 30,...
    'AxisValue', 30,...
    'StimulusSize',8,...
    'Uncrowded', 1,...
    'Borders', 1);
title(('Task'))

subplot(1,2,2)
generateHeatMapSimple( ...
    xAll, ...
    yAll, ...
    'Bins', 30,...
    'AxisValue', 30,...
    'StimulusSize',16,...
    'Uncrowded', 4,...
    'Borders', 1);
title(('Fixation'))
suptitle('SO6');

% data = eis_eisdir2mat(pt, list, fname);