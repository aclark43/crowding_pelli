% %%% This function will plot on a theoretical cone image the eye trace and 
%     subjectract how many of the cones the eye trace ineracts with, giving
%     a number of stimulation.
%     
%     Created by Ashley M. Clark


clear all
% clc
% close all

D = [5 20]; % diffusion constant arcmin^2/s
Fs = 1000; % sampling rate (Hz)
nSamples = 500; % number of samples
nTraces = 1; % number of traces
numberOfBlobsOG = 525;%runLabelImage('cones2x');
counter = 1;

for d = [4 20]
%     numOverlapCones = [];
    for ii = 1:nTraces

%         figure;
%         xholder = (ones(1,401));
        axisForCones = 15
        yholder = ([-axisForCones:.5:axisForCones]);
        counter = 1;
        for i = 1:(length(yholder))
            if counter == 1
                x(i,:) = ones(1,length(yholder))*(yholder(i));
                y(i,:) = yholder;
                counter = counter - 1;
            else
                x(i,:) = (ones(1,length(yholder))*(yholder(i)));
                y(i,:) = yholder-.25;
                counter = counter + 1;
            end
        end
        %         x = xholder.*y
        figure;
        subplot(1,2,1)
        voronoiAMC(x,y);    
        axis square
        axis([-axisForCones axisForCones -axisForCones axisForCones])
        [Xe, Ye] = EM_Brownian2(30, Fs, nSamples, 1);
        hold on
        AlphaVal = 1; %.01;
        for i = 1%:length(Xe)
            rectangle('Position',[Xe(i)-2.5,Ye(i)-2.5,5,5],...
                'EdgeColor',[0 0 1 AlphaVal],'FaceColor', [0 0 1 AlphaVal])
            hold on
        end
        factor = 5*1.4;
        for i = 1%:length(Xe)
            rectangle('Position',[(Xe(i)-2.5)+factor,Ye(i)-2.5,5,5],...
                'EdgeColor',[1 0 0 AlphaVal],'FaceColor', [1 0 0 AlphaVal])
            hold on
        end
        for i = 1%:length(Xe)
            rectangle('Position',[(Xe(i)-2.5)-factor,Ye(i)-2.5,5,5],...
                'EdgeColor',[1 0 0 AlphaVal],'FaceColor', [1 0 0 AlphaVal])
            hold on
         end

        ylabel('Y Position (arcmin')
        xlabel('X Position (arcmin')

        subplot(1,2,2)
        voronoiAMC(x,y);
        axis square
        axis([-axisForCones axisForCones -axisForCones axisForCones])
        hold on
        AlphaVal = .01;
        for i = 1:length(Xe)
            rectangle('Position',[Xe(i)-2.5,Ye(i)-2.5,5,5],...
                'EdgeColor',[0 0 1 AlphaVal],'FaceColor', [0 0 1 AlphaVal])
            hold on
        end
        factor = 5*1.4;
        for i = 1:length(Xe)
            rectangle('Position',[(Xe(i)-2.5)+factor,Ye(i)-2.5,5,5],...
                'EdgeColor',[1 0 0 AlphaVal],'FaceColor', [1 0 0 AlphaVal])
            hold on
        end
        for i = 1:length(Xe)
            rectangle('Position',[(Xe(i)-2.5)-factor,Ye(i)-2.5,5,5],...
                'EdgeColor',[1 0 0 AlphaVal],'FaceColor', [1 0 0 AlphaVal])
            hold on
         end

        ylabel('Y Position (arcmin')
        xlabel('X Position (arcmin')
         saveas(gcf,'CreatedImages/NoMotionVsMotion.epsc');
        saveas(gcf,'CreatedImages/NoMotionVsMotion.png');
        
        area = polyarea(Xe,Ye)

        size_retina = 239; % pixels, keep it odd
        half_size_retina = floor(size_retina/2);
        
        pixel_angle = 22/2; % pixel angle = xx arcmin (of images)
        
        % mv = nan(size_retina, size_retina, nSamples);
        [img1, map, alphachannel] = imread('cones4x.png');
        conesOG = double(img1) / 255;
        
        hFigure = figure;
%         subplot(1,3,1)
        set(hFigure, 'MenuBar', 'none');
        set(hFigure, 'ToolBar', 'none');figure;
        redChannel = img1(:,:,1);
        greenChannel = img1(:,:,2);
        blueChannel = img1(:,:,3);
        % black pixels are where red channel and green channel and blue channel in an rgb image are 0;
        blackpixelsmask = redChannel <= 50 & greenChannel <= 50 & blueChannel <= 50;
        % show binary image where the black pixels were
        RI = imref2d(size(blackpixelsmask));
        RI.XWorldLimits = [-4 4];
        RI.YWorldLimits = [-4 4];
        
        imshow(flipud(rgb2gray(img1)), RI, 'Border', 'tight')
        
        hold on
%         plot(Xe2, Ye2,'-','Color','r','LineWidth',5)
       
        hold on
       %%% plot with flankers (number of overall cones) 
        plot(Xe+(5*1.4), Ye,'-','Color',[1, 0, 0, ],...
            'LineWidth',25*1.2);
        plot(Xe-(5*1.4), Ye,'-','Color',[1, 0, 0, ],...
            'LineWidth',25*1.2);
         hold on
          imgWTrace = plot(Xe, Ye,'-','Color',[0, 0, 1,],...
            'LineWidth',5*1.2);
        
        %%% plot overlap only
        figure;
        x1 = intersect(round(Xe,1),round(Xe+(5*1.4),1));
        x2 = intersect(round(Xe,1),round(Xe-(5*1.4),1));
        x3 = ismember(round(Xe,1),[x1 x2]);
        newXe = round(Xe(x3),1);
%         plot(newXe, Ye(x3),'+','Color','r','LineWidth',10)
        plot(Xe+(5*1.4), Ye,'-','Color',[1, 0, 0, 0.025],...
            'LineWidth',50*1.2);
        plot(Xe-(5*1.4), Ye,'-','Color',[1, 0, 0, 0.025],...
            'LineWidth',50*1.2);
         hold on
          imgWTrace = plot(Xe, Ye,'-','Color',[0, 0, 1, 0.022],...
            'LineWidth',50*1.2);
        
        
        
         acc = 0.5;
        plot(Xe(ismember(round(Xe/acc)*acc,round([Xe+(5*1.4) Xe-(5*1.4)]/.5)*.5)), Ye,'-','Color',[1, 0, 0, 0.05],...
            'LineWidth',50*1.2);
        rightFlanker = Xe+(1.2*1.4);
        leftFlanker = Xe-(1.2*1.4);
        
        C = intersect(round(Xe,1),round(rightFlanker,1));
        C2 = intersect(round(Xe,1),round(leftFlanker,1));
        allFlanks = unique([C C2]);
        
        numOverlapCones(ii) = [min(allFlanks):0.5:max(allFlanks)];
       
        imgWTrace = plot(Xe, Ye,'-','Color','w','LineWidth',40*1.2);
        F = getframe(gcf);
        [X, Map] = frame2im(F);
        axis off
        nameFile = sprintf('CreatedImages/Img%i_dc%i',ii,d);
%         ax = gca;
%         ax.Toolbar.Visible = 'off';
        imwrite(X,sprintf('%s.png',nameFile))
        % saveas(gcf,'CreatedImages/Img1.png');
        saveas(gcf,'CreatedImages/Img1.epsc');
%         figure;
%         subplot(1,3,[2 3])
%         leg(1) = plot(1:length(Xe),Xe,'-','LineWidth',4,'Color', [154/255 242/255 40/255]);
%         hold on
%         leg(2) = plot(1:length(Xe),Ye,'-','LineWidth',4,'Color', [39/255 125/255 8/255]);
%         xlabel('Time (ms)')
%         ylabel('Position (arcminutes)')
%         legend(leg,{'X','Y'})
%         set(gca,'FontSize',16)
%         xticks([0 50 100 150 200])
%         ylim([-2.3 2.3])
%         yticks([-2 -1 0 1 2])
%         saveas(gcf,'CreatedImages/SmallTrace.epsc');
        numberOfBlobsWithTrace(ii) = runLabelImage(nameFile);
        
        numberActivatedCones(ii) = numberOfBlobsOG - numberOfBlobsWithTrace;
    end
    meanActivatedCones(counter,:) = (numberActivatedCones);
%     numberOverlapCones(counter,:) = numOverlapCones;
    counter = counter + 1;
end
data.activatedCones = mean(meanActivatedCones');
data.SEMactivatedCones(1) = sem(meanActivatedCones(1,:));
data.SEMactivatedCones(2) = sem(meanActivatedCones(2,:));

data.dc = D;
data.numRuns = nTraces;
save('dataTraceVsCones.mat', 'data');
close all
% conesIMG = padarray(conesOG,[(340-length(conesOG))/2, ...
%     (340-length(conesOG))/2],1);

% % % size_img = size(conesOG, 1);
% % %
% % % img = conesOG; % vertical image coordinates are reversed
% % % x_img_arcmin = ((1:size_img) - (size_img + 1) / 2) * pixel_angle; % angular position of each pixel in image
% % %
% % % counter = 1;
% % % for tracei = 1:nTraces
% % %     for ti = 1:nSamples
% % %         x_eye_arcmin = Xe(tracei,ti); % current eye position
% % %         y_eye_arcmin = Ye(tracei,ti);
% % %
% % %         % get eye position in units of image pixels
% % %         x_eye_pixel = interp1(x_img_arcmin, 1:size_img, x_eye_arcmin, 'nearest');
% % %         y_eye_pixel = interp1(x_img_arcmin, 1:size_img, y_eye_arcmin, 'nearest');
% % %
% % %         mv(:, :, counter) = img(...
% % %             y_eye_pixel + (-half_size_retina:half_size_retina),...
% % %             x_eye_pixel + (-half_size_retina:half_size_retina));
% % %         counter = counter + 1;
% % %     end
% % % end
% % %
% % %
% % % % Advanced Matlab Exercise:
% % % %   If you have to do many of these you can speed up computation by making
% % % %   this movie without the use of any for loops.
% % %
% % % playRetinalInputMovie(mv, x_img_arcmin, Xe, Ye, Fs);

