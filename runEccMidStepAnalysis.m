close all
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.fig = struct(...
    'FIGURE_ON', 0,...
    'HUX', 0,...
    'FOLDED', 1);
fig = params.fig;

% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,'Z084','Z014'}; %Drift Only Subjects,'Z084'

% subjectsAll = {'Z002DDPI','AshleyDDPI'};%,'Z046DDPI','Z084DDPI','Zoe','Z091'};
% ecc = {'0ecc','10eccNasal','10eccTemp','15eccNasal','15eccTemp','25eccNasal','25eccTemp'};
% eccNames = {'Center0ecc','Nasal10ecc','Temp10ecc','Nasal15ecc','Temp15ecc','Nasal25ecc','Temp25ecc'};
%
% subjectsAll = {'AshleyDDPI'};%{'Z091','Zoe'};
% ecc = {'0eccEcc','10ecc','15ecc','25ecc','40ecc','60ecc'};
% eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc','Side40ecc','Side60ecc'};
% stabilization = {'Stabilized'};
% conditions = {'Uncrowded'};


subjectsAll = {'Z138','Z091','AshleyDDPI',};
ecc = {'0ecc','10ecc','15ecc','25ecc','60ecc','120ecc', '240ecc', '360ecc'};
eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
    'Side60ecc','Side120ecc','Side240ecc','Side360ecc'};
eccVals = [0 10 15 25 60 120 240 360];
stabilization = {'Unstabilized'};
conditions = {'Uncrowded','Crowded'};
params.em = {'Drift'};
em = params.em;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
% c = jet(length(subjectsAll));
% c = brewermap(12,'Accent');
c = brewermap(12,'Dark2');

% params.c = c;

%% Load Variables
for numSub = 1:length(subjectsAll)
    for numEcc = 1:length(eccVals)
        for numCrow = 1:2
            for numStab = 1:length(stabilization)
                if strcmp(sprintf('%s_%s_%s_Drift_%s_Threshold.mat', ...
                        subjectsAll{numSub}, (conditions{numCrow}),...
                        (stabilization{numStab}), (ecc{numEcc})), ...
                        'Zoe_Uncrowded_Unstabilized_Drift_0eccEcc_Threshold.mat')
                    
                      subjectCond.(conditions{numCrow}).(stabilization{numStab}).(eccNames{numEcc}) =...
                          load('MATFiles/ZoeOpenMarker_Uncrowded_Unstabilized_Drift_0eccEcc_Threshold.mat');
                else
                    subjectCond.(conditions{numCrow}).(stabilization{numStab}).(eccNames{numEcc}) = ...
                        load(sprintf('MATFiles/%s_%s_%s_Drift_%s_Threshold.mat', ...
                        subjectsAll{numSub}, (conditions{numCrow}),...
                        (stabilization{numStab}), (ecc{numEcc})));
                end
            end
        end
    end
end
save('MATFiles/Zoe_MidStepVars','subjectCond');

%% Linear Regressions - FOR GRANT
% figure;
% temp = allValuesThresholds.Uncrowded.Unstabilized;
% [~,p,bU,r] = LinRegression([0 10 15],...
%     [temp.Center0ecc temp.Side10ecc temp.Side15ecc],...
%     0,NaN,1,0);
%
% temp = allValuesThresholds.Crowded.Unstabilized;
% [~,p,bC,r] = LinRegression([0 10 15],...
%     [temp.Center0ecc temp.Side10ecc temp.Side15ecc],...
%     0,NaN,1,0);


%% Plot Diff Thresholds at 0
% figure;
% uTemp = [allValuesThresholds.Uncrowded.Unstabilized.Center0ecc];
% cTemp = [allValuesThresholds.Crowded.Unstabilized.Center0ecc];
% % uTemp = [allValuesThresholds.Uncrowded.Stabilized.Center0ecc];
% % cTemp = [allValuesThresholds.Crowded.Stabilized.Center0ecc];
% for ii = 1:length(uTemp)
%     plot([1 2],[uTemp(ii) cTemp(ii)],'-o','Color',c(ii,:),...
%         'MarkerFaceColor',c(ii,:))
%     hold on
% end
% xlim([.5 2.5])
% set(gca,'xtick',[1 2],'xticklabel', {'Uncrowded', 'Crowded'},'FontSize',12)
% ylabel('Threshold')
% hold on
% errorbar([1 2],[mean(uTemp) mean(cTemp)],[std(uTemp) std(cTemp)],'-o','Color','k');
% saveas(gcf, ...
%     sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iCenterThresholds.epsc',...
%     params.fig.FOLDED));
% saveas(gcf, ...
%     sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iCenterThresholds.png',...
%     params.fig.FOLDED));
%% Plot Thresholds Stabilized/Unstabilized
figure('units','normalized','outerposition',[0.2 0.05 .4 .6]) %plots all absolute thresholds for each condition
counter = 1;
for cIdx = 1:length(conditions)
    for sIdx = 1:length(stabilization)
%         subplot(2,2,counter);
        clear allValsSubj
        for ii = 1:length(subjectsAll)
            cond = conditions{cIdx};
            stab = stabilization{sIdx};
         
            for numEcc = 1:length(ecc)
%                 if strcmp(sprintf('%s_%s_%s_%s', ...
%                         subjectsAll{numSub}, (cond),...
%                         (stab), (ecc{numEcc})), ...
%                         'Zoe_Uncrowded_Unstabilized_60ecc')
%                     oneRow(numEcc) = NaN;
%                     oneRowEcc(numEcc) = 60;
%                 else
                    oneRow(numEcc) = subjectCond.(cond).(stab).(eccNames{numEcc}).threshInfo.thresh;
                    oneRowEcc(numEcc) = eccVals(numEcc);
                    dcEachCond{cIdx}(numEcc) = subjectCond.(cond).(stab).(eccNames{numEcc}).threshInfo.em.allTraces.dCoefDsq;
%                 end
            end 
            hold on
            yyaxis left
            leg(counter) = plot(eccVals(~isnan(oneRow)),oneRow(~isnan(oneRow)),'-o','Color',c(counter,:),...
                'MarkerFace',c(counter,:),'MarkerSize', 10);
%             leg(counter) = plot(eccVals(~isnan(oneRow)&eccVals<150),oneRow(~isnan(oneRow)&eccVals<150),'-o','Color',c(counter,:),...
%                 'MarkerFace',c(counter,:),'MarkerSize', 10);
%             hold on
%             BreakPlot(eccVals(~isnan(oneRow)),oneRow(~isnan(oneRow)),10,35,'Line');
            hold on
            logMarVals = log10(oneRow/2);
            sd = 20*(10.^(logMarVals));
%             yyaxis right
%             leg(counter) = plot(eccVals(~isnan(oneRow)),oneRow(~isnan(oneRow)),'-o','Color',c(counter,:),...
        
        end
%         xlim([-.5 max(eccVals)]);% .8 4.2]);
       
        counter = counter + 1;
%         axis square
    end
end
% breakxaxis([130 300])
% 
yyaxis left
ylim([0 40])
hold on
ylabel('Threshold (arcmin)');
% title(sprintf('%s %s',subjectsAll{ii}),stabilization{sIdx});
x = [30 30];
y = [0 42];
set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14)
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14,'YScale', 'log')


xlabel('Eccentricity (arcmin)')
xtickangle(60)
line(x,y,'Color','k','LineStyle','--')
% axis square
title('Zoe')
legend(leg,{'Uncrowded','Crowded'},'Location','southeast');
% breakyaxis([10 34])
yyaxis right
ylabel('Snellen 20/X');
set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14,'YScale', 'log')

% for i = 1:4
%     [h,p(i)] = ttest(statsVals.Crowded.Unstabilized.allVals(:,i), ...
%         statsVals.Uncrowded.Unstabilized.allVals(:,i))
% end



saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iAbsoluteThresholdsWithPeripheral.epsc',...
    params.fig.FOLDED));
saveas(gcf, ...
    sprintf('../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/%iAbsoluteThresholdsWithPeripheral.png',...
    params.fig.FOLDED));

%%
counter = 1;
for cIdx = 1:length(conditions)
    for sIdx = 1:length(stabilization)
        clear allValsSubj
        for ii = 1:length(subjectsAll)
            cond = conditions{cIdx};
            stab = stabilization{sIdx};
            
            for numEcc = 1:length(ecc)
                if strcmp(sprintf('%s_%s_%s_%s', ...
                        subjectsAll{numSub}, (cond),...
                        (stab), (ecc{numEcc})), ...
                        'Zoe_Uncrowded_Unstabilized_60ecc')
                    oneRow(numEcc) = NaN;
                    oneRowEcc(numEcc) = 60;
                else
                    oneRow(numEcc) = subjectCond.(cond).(stab).(eccNames{numEcc}).threshInfo.thresh;
                    oneRowEcc(numEcc) = eccVals(numEcc);
                end
            end
            
        end
        hold on
        
        leg(counter) = plot(eccVals(~isnan(oneRow)),oneRow(~isnan(oneRow)),'-o','Color',c(counter,:),...
            'MarkerFace',c(counter,:),'MarkerSize', 10);
        
        
        togetherRow(counter,:) = oneRow;
        counter = counter + 1;
        %         delete( findobj(ph))
        %         axis square
    end
end
figure('units','normalized','outerposition',[0.2 0.05 .4 .6]) %plots all absolute thresholds for each condition
leg1(1) = plot(eccVals,togetherRow(1,:),'-o')
hold on
leg1(2) = plot(eccVals,togetherRow(2,:),'-o')
hAx(1)=gca;
yy=20*(10.^(log10(hAx(1).YTick/2)));
% yy=fliplr(yy);           % axes tick values are always minimum up
yyaxis right
hAx(2)=gca;
ylim([yy(1) yy(end)])
% hAx(2).YDir='reverse';
% hAx(2).YTick=yy;
ylabel('Snellen Acuity')

yyaxis left
hold on
ylabel('Threshold (arcmin)');
x = [30 30];
y = [0 50];
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14,'YScale', 'log')
set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14)
% xlabel('Eccentricity (arcmin)')
xtickangle(60)
line(x,y,'Color','k','LineStyle','--')
legend(leg1,{'Uncrowded','Crowded'},'Location','southeast');

