function [ params ] = buildParams(machine, session, compSpans, subject, condition)
%Builds parameters for "what to look at" during analysis - some values get
%written over later

params = struct(...
    'nBoots', 1,... %Number of boots for psychometric fits
    'crowded', true, ... %Gets rewritten based on currentCond
    'DMS', false, ...
    'D', false, ...
    'machine', machine, ... %DPI "A" vs dDPI "D"
    'session', session, ... %Which number session
    'spanMax', 100,... %The Maximum Span
    'spanMin', 0,...%The Minimum Span
    'compSpans', compSpans,...
    'MS', false );

%% Different Value for Large/Small Spans per Subject
if compSpans > 0
    if strcmp('Uncrowded',condition)
        if strcmp('Ashley',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 1.75;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 2.5;
                params.spanMax = 6;
            end
        end
        if strcmp('Z063',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 2.75;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 3.0;
                params.spanMax = 8;
            end
        end
        if strcmp('Z013',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 1.75;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 3.25;
                params.spanMax = 8;
            end
        end
        if strcmp('Z002',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 3;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 4;
                params.spanMax = 6;
            end
        end
        if strcmp('Z005',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 3;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 5;
                params.spanMax = 8;
            end
        end
        if strcmp('Z023',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 2.25;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 3.25;
                params.spanMax = 6;
            end
        end
        if strcmp('Z070',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 3;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 3.5;
                params.spanMax = 12;
            end
        end
    elseif strcmp('Crowded',condition)
        if strcmp('Ashley',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 1.75;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 2.5;
                params.spanMax = 6;
            end
        end
        if strcmp('Z063',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 2.85;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 3.10;
                params.spanMax = 100;
            end
        end
        if strcmp('Z013',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 2;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 5;
                params.spanMax = 8;
            end
        end
        if strcmp('Z002',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 3.25;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 4.5;
                params.spanMax = 8;
            end
        end
        if strcmp('Z005',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 4.5;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 6.75;
                params.spanMax = 10;
            end
        end
        if strcmp('Z023',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 2.75;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 4;
                params.spanMax = 8;
            end
        end
        if strcmp('Z070',subject)
            if compSpans == 1 %%%Small Spans
                params.spanMin = 0;
                params.spanMax = 3;
            elseif compSpans == 2 %%%Large Spans
                params.spanMin = 3.5;
                params.spanMax = 12;
            end
        end
    end
end
end

