function fH1 = createGraphTogetherMultipleSubjWithText(title,pathToData,currentCond,currentStab,currentEm,currentEcc,color,moveText)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

fH1 = openfig(fullfile(pathToData, title));
set(gcf, 'Units', 'Normalized');%,'OuterPosition', [0.1, 0.1, 0.5, 0.7]);
xlim([0 5])
%%Finds the value text to move
% h = findobj({'Color','[0.30 0.70 0.50]','-or','Color','[0.30 0.30 0.50]'});
% k = (axis);
% textPosition = k(2) - (k(2)/3);
% for ii = 1:length(h)
%     h(ii).Position(1) = textPosition;
%     h(ii).Position(2) = (h(ii).Position(2))-moveText;
%     h(ii).Color = color;
%     h(ii).FontSize = 8;
% end
% text1 = (sprintf('%s %s ', currentCond, currentStab));
% text2 = (sprintf('%s %s ', currentEm, currentEcc));
% t = text(2.0,0.5,({text1,text2}));
% t.FontSize = 12;
% t.Color = color;
% t.Position(1) = textPosition;
% t.Position(2) = t.Position(2)-moveText;
h = findobj({'Color','[0.30 0.70 0.50]','-or',...
    'Color','[0.30 0.30 0.50]','-or',...
    'Color','[77 204 179]/256','-or',...
    'Color','[77 204 178]/256','-or',...
    'Color','[0.5 0.5 0.5]'});
k = (axis);
textPosition = k(2) - (k(2)/3);
for ii = 1:length(h)
    delete(h(ii));
end



%%Color of Data Points
colorArray = color;
numColors = size(colorArray,1);

%Getting axes objects
aH = findall(fH1,'Type','axes').';

for ii = aH
    lH = findall(ii,'Type','line').';
    cnt = 0;
    for jj = lH
        idx = mod(cnt,numColors) + 1;
        jj.Color = colorArray(idx,:);
        %          if cnt == 8 || cnt < 3
        %             jj.Marker = 'none';
        %         elseif cnt <= 9
%         jj.Marker = 'o';
%         jj.MarkerSize = 10;
        %          end
        jj.LineWidth = 5;
        cnt = cnt + 1;
    end
end

set(gca, 'FontSize', 20);

end