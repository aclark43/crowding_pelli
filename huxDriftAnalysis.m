function huxDriftAnalysis(fixationInfo, subjectsAll, subjectThreshUnc, vfResults, control, huxID)

%% Drift Data Fixation
load('MATFiles/HUXKeepFixationTrials2_175.mat'); %For Fixation Data
subNum = length(subjectsAll);
% for ii = 1:subNum
%     if ii == 3
%         continue
%     end
%     traces.(subjectsAll{ii}).xDrift = subjectThreshUnc(ii).em.x(find(...
%         ~cellfun(@isempty,subjectThreshUnc(ii).em.x)));
%     traces.(subjectsAll{ii}).yDrift = subjectThreshUnc(ii).em.y(find(...
%         ~cellfun(@isempty,subjectThreshUnc(ii).em.y)));
%     traces.(subjectsAll{ii}).x = subjectThreshUnc(ii).em.x(find(...
%         ~cellfun(@isempty,subjectThreshUnc(ii).em.x)));
%     traces.(subjectsAll{ii}).y = subjectThreshUnc(ii).em.y(find(...
%         ~cellfun(@isempty,subjectThreshUnc(ii).em.y)));
% end

% subNum = length(subjectsAll);
for ii = 1:subNum
    %     if ii == 3
    %         continue;
    %     end
    forDSQCalc.x = traces.(subjectsAll{ii}).xDrift;
    forDSQCalc.y = traces.(subjectsAll{ii}).yDrift;
    
    [~,~,~, ~, ~, ...
        SingleSegmentDsq,~,~] = ...
        CalculateDiffusionCoef(330, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
    
    goodTrials = find((SingleSegmentDsq(:,48) < 90));
    
    forDSQCalc.x = traces.(subjectsAll{ii}).xDrift(goodTrials);
    forDSQCalc.y = traces.(subjectsAll{ii}).yDrift(goodTrials);
    
    [~,dataStruct{ii}.Bias,dataStruct{ii}.dCoefDsq, ~, dataStruct{ii}.Dsq, ...
        dataStruct{ii}.SingleSegmentDsq,~,~] = ...
        CalculateDiffusionCoef(330, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
    
    counter = 1;
    for t = 1:length(goodTrials)
        %         figure(100);
        i = goodTrials(t);
        
        x = traces.(subjectsAll{ii}).xDrift{i};
        y = traces.(subjectsAll{ii}).yDrift{i};
        %         U = 1*x;
        %         V = 1*y;
        hold on
        %         plot(x,y,'-')
        %         figure;
        %         angleQ = quiver(x,y,U,V,0);
        angleQ = quiver(x(1:end-1),y(1:end-1),diff(x),diff(y),0);
        r = atan2(angleQ.VData,angleQ.UData);
        %         if circ_var(r') > 0.3
        %             avDirection{ii,t} = NaN;
        %         else
        avDirection1{ii,t} = (circ_mean(r')*180/pi);
        %         end
        
        
        [~, dataStruct{ii}.instSpX{counter},...
            dataStruct{ii}.instSpY{counter},...
            dataStruct{ii}.mn_speed{counter},...
            dataStruct{ii}.driftAngle{counter},...
            dataStruct{ii}.curvature{counter},...
            dataStruct{ii}.varx{counter},...
            dataStruct{ii}.vary{counter},...
            dataStruct{ii}.bcea{counter},...
            dataStruct{ii}.span(counter), ...
            dataStruct{ii}.amplitude(counter), ...
            dataStruct{ii}.prlDistance(counter),...
            dataStruct{ii}.prlDistanceX(counter), ...
            dataStruct{ii}.prlDistanceY(counter)] = ...
            getDriftChar(x, y, 41, 1, 250, 330);
        avDirection{ii,t} = rad2deg(circ_mean(dataStruct{ii}.driftAngle{counter}));
        
        tempT.x = x;
        tempT.y = y;
        [~,dataStruct{ii}.velocity(counter), ...
            dataStruct{ii}.velocityX{counter},...
            dataStruct{ii}.velocityY{counter}] =...
            CalculateDriftVelocity(tempT,[1 55]);
        
        counter = counter + 1;
    end
end
% close figure 100
%% 
% majorAxis = [0 30 60 90 120 150 180 210 240 270 300 330];
majorAxis = 0:15:330;

figure;
subplot(1,3,1)
counter = 1;
values = [];
for ii = find(control)
    allDriftAngle = [];
    polarhistogram(deg2rad([avDirection{ii,:}]));
    allDriftAngle = mod([avDirection{ii,:}],360);
    hold on
    for i = 1:length(majorAxis)
        if majorAxis(i) == 0
            vecVals{ii,i} = (allDriftAngle < 15 & allDriftAngle >= 0) | ...
                allDriftAngle > 345;
        else
            vecVals{ii,i} = (allDriftAngle < majorAxis(i) + 15 &...
                allDriftAngle > majorAxis(i)-15);
        end
        values(counter,i) = length(find(vecVals{ii,i}));
        SEMVal(counter,i) = sem(normalizeVector(allDriftAngle(allDriftAngle < majorAxis(i) + 15 &...
            allDriftAngle > majorAxis(i)-15)));
    end
    counter = counter + 1;
end
rho = normalizeVector(mean(values));% (values(1))]); % change this vector (it is a vector of 8, and then add the first value to the end  to connect the polar plot )
dNum.Controls = values;
dNum.Angles = majorAxis;

subplot(1,3,2)
values = [];
for ii = find(~control)
    %     if ii == 3
    %         continue;
    %     end
    allDriftAngle = [];
    polarhistogram(deg2rad([avDirection{ii,:}]));
    allDriftAngle = mod([avDirection{ii,:}],360);
    hold on
    for i = 1:length(majorAxis)
        if majorAxis(i) == 0
            vecVals{ii,i} = (allDriftAngle < 15 & allDriftAngle >= 0) | ...
                allDriftAngle > 345;
        else
            vecVals{ii,i} = (allDriftAngle < majorAxis(i) + 15 &...
                allDriftAngle > majorAxis(i)-15);
        end
        values(counter,i) = length(find(vecVals{ii,i}));
        SEMVal(counter,i) = sem(normalizeVector(allDriftAngle(allDriftAngle < majorAxis(i) + 15 &...
            allDriftAngle > majorAxis(i)-15)));
    end
    counter = counter + 1;
end
% dNum.PTNoRot = values;
% dNum.Angles = majorAxis;
% for ii = 1:length(find(~control))
%     P2(ii,:) = dNum.PTNoRot(ii,:)/sum(dNum.PTNoRot(ii,:));
% end
values = [];
% figure;
subplot(1,3,3)
counter = 1;
for ii = find(~control)
    allDriftAngleRot = [];
%     rotateAngle = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
%     if ii == 1
%         rotateAngle = 315;
%     elseif ii == 2
%         rotateAngle = 50;
%     elseif ii == 3
%         rotateAngle = 145;
%     elseif ii == 4
%         rotateAngle = 90;
%     elseif ii == 10
%         rotateAngle = 90;
%     elseif ii == 22
%         rotateAngle = 315;
%     elseif ii == 23
%         rotateAngle = 90;
%     end
  lossIdx = find(vfResults.patientInfo.(huxID{ii}).deficit);
    lossAngles = vfResults.patientInfo.(huxID{ii}).rAngles(lossIdx);
    meanAngleRad = circ_mean(lossAngles');
    rotateAngle = mod(rad2deg(meanAngleRad),360);
    %     theta = -rotateAngle;
    theta = rotateAngle;


    avDir360 = mod([avDirection{ii,:}],360);
    allDriftAngleRot = mod(avDir360+theta,360);
    %     allDriftAngleRot(find(allDriftAngleRot > 360)) = ...
    %         allDriftAngleRot(find(allDriftAngleRot > 360)) - 360;
    %     figure;
    %     polarhistogram(deg2rad(allDriftAngleRot));
    %     hold on
    %     driftVelSubject{ii}=allDriftAngleRot;
    for i = 1:length(majorAxis)
        
        %         vecVals{ii,i} = isAngBetween((allDriftAngleRot), ...
        %             (mod(allDriftAngleRot + 15,360)),...
        %             (mod(allDriftAngleRot - 15,360)));
        if majorAxis(i) == 0
            vecVals{ii,i} = (allDriftAngleRot < 15 & allDriftAngleRot >= 0) | ...
                allDriftAngleRot > 345;
        else
            vecVals{ii,i} = (allDriftAngleRot < majorAxis(i) + 15 &...
                allDriftAngleRot > majorAxis(i)-15);
        end
        values(counter,i) = length(find(vecVals{ii,i}));
        SEMVal(counter,i) = sem(normalizeVector(allDriftAngleRot(allDriftAngleRot < majorAxis(i) + 15 &...
            allDriftAngleRot > majorAxis(i)-15)));
    end
    counter = counter + 1;
    
end
rho = normalizeVector(mean(values));% (values(1))]); % change this vector (it is a vector of 8, and then add the first value to the end  to connect the polar plot )
dNum.Patients = values;
dNum.Angles = majorAxis;

for ii = 1:length(find(~control))
    P(ii,:) = dNum.Patients(ii,:)/sum(dNum.Patients(ii,:));
end
%     C = dNum.Controls./max(dNum.Controls')';
for ii = 1:length(find(control))
    C(ii,:) = dNum.Controls(ii,:)/sum(dNum.Controls(ii,:));
end

% for ii = find(~control)
%     figure;
%     heatmpaOutPut{i} = ndhist([dataStruct{ii}.instSpX{:}],...
%         [dataStruct{ii}.instSpY{:}],'bins', 2, 'radial','axis',[-200 200 -200 200],'nr','filt');
%     axis([-60 60 -60 60])
%     load('./MyColormaps.mat')
%     set(gcf, 'Colormap', mycmap)
%     axis square
%     line([-60 60],[0 0])
%     line([0 0],[-60 60])
%     shading interp
% end

%%
[h,p] = ttest([P(:,1)',P(:,2)' P(:,3)' P(:,4)' P(:,5)' P(:,6)' ], ...
    [P(:,7)' P(:,8)' P(:,9)' P(:,10)' P(:,11)' P(:,12)']);
figure;
errorbar([0 1],[mean([P(:,1)',P(:,2)' P(:,3)' P(:,4)' P(:,5)' P(:,6)' ]) ...
    mean([P(:,7)' P(:,8)' P(:,9)' P(:,10)' P(:,11)' P(:,12)'])],...
    [sem(([P(:,1)',P(:,2)' P(:,3)' P(:,4)' P(:,5)' P(:,6)' ])) ...
    sem(([P(:,7)' P(:,8)' P(:,9)' P(:,10)' P(:,11)' P(:,12)']))],'-o',...
    'Color','k','MarkerFaceColor','k','MarkerSize',10);
title(sprintf('p = %.3f',p));
xticks([0 1])
% xtickslabels({'Towards BF
xlim([-.25 1.25])
xticklabels({'Towards BF','Away from BF'});

% [h,p] = ttest([P(:,6)',P(:,7)',P(:,8)'], ...
%     [P(:,18)' P(:,19)' P(:,20)']);
[h,p] = ttest([mean(P(:,6)),mean(P(:,7)),mean(P(:,8))], ...
    [mean(P(:,18)) mean(P(:,19)) mean(P(:,20))]);

% [h,p] = ttest([P(:,3)' P(:,4)' P(:,5)'], [P(:,9)' P(:,10)' P(:,11)']);
figure;
errorbar([0 1],[mean([P(:,6)',P(:,7)',P(:,8)']) ...
    mean([P(:,18)' P(:,19)' P(:,20)'])],...
    [sem([P(:,3)',P(:,4)',P(:,5)']) ...
    sem([P(:,18)' P(:,19)' P(:,20)'])],'-o',...
    'Color','k','MarkerFaceColor','k','MarkerSize',10);
% hold on
% errorbar([2 3],[mean([P(:,6)' P(:,7)' P(:,8)']) mean([P(:,1)' P(:,2)' P(:,12)'])],...
%     [sem([P(:,6)' P(:,7)' P(:,8)']) sem([P(:,1)' P(:,2)' P(:,12)'])],'-o',...
%     'Color','b','MarkerFaceColor','b','MarkerSize',10);
ylabel('Normalized Probability of Drift Direction');
xticks([0 1])
% xtickslabels({'Towards BF
xlim([-.25 1.25])
xticklabels({'Towards BF','Away from BF'});
title(sprintf('p = %.3f',p));
set(gca,'FontSize',14);
% ylim([.3 .6])
% saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/DriftTtest.png');
% saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/DriftTest.epsc');
nanmean([P(:,6)',P(:,7)',P(:,8)'])
nanstd([P(:,6)',P(:,7)',P(:,8)'])

nanmean([P(:,18)',P(:,19)',P(:,20)'])
nanstd([P(:,18)',P(:,19)',P(:,20)'])

%%
figure;
subplot(1,2,1)
polarplot(deg2rad([majorAxis 0]),[mean(P) mean(P(:,1))],'-o'); hold on
polarplot(deg2rad([majorAxis 0]),[mean(P)+sem(P) mean(P(:,1))+sem(P(:,1))],'--');hold on
polarplot(deg2rad([majorAxis 0]),[mean(P)-sem(P) mean(P(:,1))-sem(P(:,1))],'--')
title('Probability of Drift Direction');

% subplot(1,2,2)
% polarplot(deg2rad([majorAxis 0]),[mean(C) mean(C(:,1))],'-o'); hold on
% polarplot(deg2rad([majorAxis 0]),[mean(C)+sem(C) mean(C(:,1))+sem(C(:,1))],'--');hold on
% polarplot(deg2rad([majorAxis 0]),[mean(C)-sem(C) mean(C(:,1))-sem(C(:,1))],'--')
% title('Probability of Drift Direction Controls');

subplot(1,2,2)
upMat = [P(:,5),P(:,6),P(:,7),P(:,8),P(:,9)];
up = [P(:,5)',P(:,6)',P(:,7)',P(:,8)',P(:,9)']/16;
downMat = [P(:,17),P(:,18),P(:,19),P(:,20),P(:,21)];
down = [P(:,17)',P(:,18)',P(:,19)',P(:,20)',P(:,21)']/16;
left = [P(:,10)',P(:,11)',P(:,12)',P(:,13)',P(:,14)',P(:,15)',P(:,16)']/16;
right = [P(:,1)',P(:,2)',P(:,3)',P(:,4)',P(:,22)',P(:,23)']/16;

eachSubjUp = sum(upMat');
eachSubjDo = sum(downMat');
for i = 1:size(upMat)
    plot([0 1],[eachSubjUp(i) eachSubjDo(i)],'--','Color',[.3 .3 .3])
    hold on
end

errorbar([0 1],[sum(up) sum(down)],...
    [sem(sum(upMat')),sem(sum(downMat'))],'-o',...
    'Color','k','MarkerFaceColor','k','MarkerSize',10);
% hold on
% errorbar([2 3],[mean([P(:,6)' P(:,7)' P(:,8)']) mean([P(:,1)' P(:,2)' P(:,12)'])],...
%     [sem([P(:,6)' P(:,7)' P(:,8)']) sem([P(:,1)' P(:,2)' P(:,12)'])],'-o',...
%     'Color','b','MarkerFaceColor','b','MarkerSize',10);
ylabel('Probability of Drift Direction');
xticks([0 1])
% xtickslabels({'Towards BF
xlim([-.25 1.25])
xticklabels({'Towards BF','Away from BF'});
[h,p] = ttest(sum(upMat'),sum(downMat'));
title(sprintf('p = %.3f',p));
set(gca,'FontSize',14);


%% Drift
figure;
temp = nanmean(dNum.PTNoRot./max(dNum.PTNoRot')');
% temp = nanmean(dNum.Patients./max(dNum.Patients')');

polarplot(deg2rad([dNum.Angles dNum.Angles(1)]),...
    [temp temp(1)],...
    'b','MarkerSize', 30);
hold on
tempS = (sem(dNum.PTNoRot./max(dNum.PTNoRot')')/2);
% tempS = (sem(dNum.Patients./max(dNum.Patients')')/2);
polarplot(deg2rad([dNum.Angles dNum.Angles(1)]),...
    [tempS+temp tempS(1)+temp(1)],...
    '--b','MarkerSize', 30);
polarplot(deg2rad([dNum.Angles dNum.Angles(1)]),...
    [temp-tempS temp(1)-tempS(1)],...
    '--b','MarkerSize', 30);
hold on
temp2 =  nanmean(dNum.Controls./max(dNum.Controls')');
polarplot(deg2rad([dNum.Angles dNum.Angles(1)]),[temp2 temp2(1)],...
    'r','MarkerSize', 30);
tempS2 = (sem(dNum.Controls./max(dNum.Controls')')/2);
polarplot(deg2rad([dNum.Angles dNum.Angles(1)]),...
    [tempS2+temp2 tempS2(1)+temp2(1)],...
    '--r','MarkerSize', 30);
polarplot(deg2rad([dNum.Angles dNum.Angles(1)]),...
    [temp2-tempS2 temp2(1)-tempS2(1)],...
    '--r','MarkerSize', 30);

[h,p] = ttest2(P,C);

stats = anovan([mean(dNum.PTNoRot(11:end,:)'),...
    mean(dNum.Controls')],{control});

polarplot(deg2rad(dNum.Angles(find(h))),ones(1,length(find(h))),...
    '*','Color','k');
% saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/DriftHistogramAngleComb.png');
% saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/DriftAnlgeComb.epsc');



%% Clean Task Trials Again
for ii = 1:subNum
    if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
        continue;
    end
    goodTrials = [];
    forDSQCalc = [];
    SingleSegmentDsq = [];
    
    path = subjectThreshUnc(ii).em.allTraces;
    forDSQCalc.x = path.x;
    forDSQCalc.y = path.y;
    
    [~,~,~, ~, ~, ...
        SingleSegmentDsq,~,~] = ...
        CalculateDiffusionCoef(330, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
    
    goodTrials = find((SingleSegmentDsq(:,48) < 90))';
    
    forDSQCalc.x = path.x(goodTrials);
    forDSQCalc.y = path.y(goodTrials);
    
    [~,dataTask{ii}.Bias,dataTask{ii}.dCoefDsq, ~, dataTask{ii}.Dsq, ...
        dataTask{ii}.SingleSegmentDsq,~,~] = ...
        CalculateDiffusionCoef(330, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
    
    counter = 1;
    for t = 1:length(goodTrials)
        i = goodTrials(t);
        x = path.x{i}(1:58);
        y = path.y{i}(1:58);
        [~, dataTask{ii}.instSpX{counter},...
            dataTask{ii}.instSpY{counter},...
            dataTask{ii}.mn_speed{counter},...
            dataTask{ii}.driftAngle{counter},...
            dataTask{ii}.curvature{counter},...
            dataTask{ii}.varx{counter},...
            dataTask{ii}.vary{counter},...
            dataTask{ii}.bcea{counter},...
            dataTask{ii}.span(counter), ...
            dataTask{ii}.amplitude(counter), ...
            dataTask{ii}.prlDistance(counter),...
            dataTask{ii}.prlDistanceX(counter), ...
            dataTask{ii}.prlDistanceY(counter)] = ...
            getDriftChar(x, y, 41, 1, 250,330);
        
        tempT.x = x;
        tempT.y = y;
        [~,dataTask{ii}.velocity(counter),...
            dataTask{ii}.velocityX{counter},...
            dataTask{ii}.velocityY{counter}] = ...
            CalculateDriftVelocity(tempT,[1 55]);
        
        counter = counter + 1;
    end
end

%% Plotting
for ii = 1:subNum
    fix.DC(ii) = dataStruct{ii}.dCoefDsq;
    fix.Curve(ii) = nanmean(cell2mat(dataStruct{ii}.curvature));
    fix.Span(ii) = nanmean((dataStruct{ii}.span));
    for i = 1:length(dataStruct{ii}.mn_speed)
        temp(i) = double(cell2mat(dataStruct{ii}.mn_speed(i)));
    end
    fix.Speed(ii) = nanmean(temp);
    if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
        task.DC(ii) = NaN;
        task.Curve(ii) = NaN;
        task.Span(ii) = NaN;
        task.Speed(ii) = NaN;
        continue;
    end
    task.DC(ii) = dataTask{ii}.dCoefDsq;
    task.Curve(ii) = nanmean(cell2mat(dataTask{ii}.curvature));
    task.Span(ii) = nanmean((dataTask{ii}.span));
    for i = 1:length(dataTask{ii}.mn_speed)
        temp(i) = double(cell2mat(dataTask{ii}.mn_speed(i)));
    end
    task.Speed(ii) = nanmean(temp);
end
plotDriftCharH(fix.DC, task.DC, 'DC', control, subjectsAll, 90)
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/DC.epsc');
plotDriftCharH(fix.Curve, task.Curve, 'Curvature', control, subjectsAll, 19)
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/Curvature.epsc');
plotDriftCharH(fix.Span, task.Span, 'Span', control, subjectsAll, 12)
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/Span.epsc');
plotDriftCharH(fix.Speed, task.Speed, 'Speed', control, subjectsAll, 100)
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/HeatMaps/Speed.epsc');

%% Velocity Maps


