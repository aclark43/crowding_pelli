clear all
close all
clc

% ?numberToAnalyze = [3 5 6 9];
numToAnalyze = [3 5 6 9];
stimSize = 1.5;
widthPixel = 16; %size should be multiple of 2
pixelAngle = stimSize/widthPixel; 
bufferSizeAngle = 2 * 60; % arcmin
bufferSizePixel = bufferSizeAngle / pixelAngle;
sz = 2 * bufferSizePixel;
repN = 10;
sizpad = 1;%size padding
?
for di = 1:length(numberToAnalyze)
    digit = drawDigit(numToAnalyze(di), widthPixel);
    
    if numToAnalyze(di) == 6
        digitSlice = digit(:,size(digit,2)/2+1);
    else
        digitSlice = digit(:,size(digit,2)/2);
    end
    
    stim = [ones(1,sizpad)'*0.5; digitSlice; ones(1,sizpad)'*0.5];
    for ii = 1:repN
        stim = [stim; stim];
    end
    stim = stim-mean(stim);
    k = ((-size(stim,1)/2 ):(size(stim,1)/2)-1) / size(stim,1) / (pixelAngle/60);
    %spectrum
    spect = (fftshift(fft(stim)));
    %phase
    X2=spect;
    threshold = 0.0000000001;%max(abs(spect))/10000; %tolerance threshold
    X2((abs(spect))<threshold) = 0; %maskout values that are below the threshold
    %*180/pi; %phase information multiply by 180/pi to convert to deg
    Phase_spec = (angle(X2)); % from -pi to pi
    
    allSpec(di,:) = spect; 
    allPhase(di,:) = Phase_spec';
?
    
    %% Plotting amplitude and Spatial Frequency
    figure(1);
    plot(k,abs(spect));
    ylabel(sprintf('Power'))
    xlabel('Spatial Frequency (k)')
    %set(gca, 'XTick', [-100:10:100])
    hold on
    title(sprintf('%i, stimulus size %.2farcmin',(numberToAnalyze(di)), stimSize));
    xlim([0 80])
    set(gca, 'XScale', 'log')
    
    %% Plotting phase and Spatial Frequency
    figure(2);
    hold on
    plot(k,Phase_spec, 'o');
    ylabel(sprintf('phase', repN))
    xlabel('Spatial Frequency (k)')
    set(gca, 'XTick', [-100:10:100])
    hold on
    title(sprintf('%i, stimulus size %.2farcmin',(numberToAnalyze(di)), stimSize));
    xlim([0 80])
    
    
end
% ?
D1_P = circ_dist(allPhase(1,:),allPhase(2,:));
D2_P = circ_dist(allPhase(1,:),allPhase(3,:));
D3_P = circ_dist(allPhase(1,:),allPhase(4,:));
D4_P = circ_dist(allPhase(2,:),allPhase(3,:));
D5_P = circ_dist(allPhase(2,:),allPhase(4,:));
D6_P = circ_dist(allPhase(3,:),allPhase(4,:));
D1_A = circ_dist(allSpec(1,:),allSpec(2,:));
D2_A = circ_dist(allSpec(1,:),allSpec(3,:));
D3_A = circ_dist(allSpec(1,:),allSpec(4,:));
D4_A = circ_dist(allSpec(2,:),allSpec(3,:));
D5_A = circ_dist(allSpec(2,:),allSpec(4,:));
D6_A = circ_dist(allSpec(3,:),allSpec(4,:));
% ?
% a = find(abs(D1_P)<0.2);
% b = find(abs(D1_P)>=0.2);
% F_D1 = round(unique(abs(k(b))));
% D1_P(a) = NaN;
% a = find(abs(D2_P)<0.2);
% b = find(abs(D2_P)>=0.2);
% F_D2 = round((abs(k(b))));
% D2_P(a) = NaN;
% a = find(abs(D3_P)<0.2);
% b = find(abs(D3_P)>=0.2);
% F_D3 = round(unique(abs(k(b))));
% D3_P(a) = NaN;
% a = find(abs(D4_P)<0.2);
% b = find(abs(D4_P)>=0.2);
% F_D4 = round(unique(abs(k(b))));
% D4_P(a) = NaN;
% a = find(abs(D5_P)<0.2);
% b = find(abs(D5_P)>=0.2);
% F_D5 = round(unique(abs(k(b))));
% D5_P(a) = NaN;
% a = find(abs(D6_P)<0.2);
% b = find(abs(D6_P)>=0.2);
% F_D6 = round(unique(abs(k(b))));
% D6_P(a) = NaN;
DP_var = circ_var([D1_P;D2_P;D3_P;D4_P;D5_P;D6_P]);
DA_var = var([D1_A;D2_A;D3_A;D4_A;D5_A;D6_A]);
allSpec(allSpec<0)= 0;
M_A = var(allSpec)-mean(allSpec);
M_P = circ_mean(allPhase);
figure;
plot(k,rad2deg(DP_var), 'or', 'LineWidth', 2)
hold on
%semilogx(k,(DA_var/100), 'c', 'LineWidth', 2)
%semilogx(k,M_A/10, 'k', 'LineWidth', 2)
xlim([0.5 60])
title('Phase variance difference')
%ylim([-10 20])
xlabel('K')
% ?
% ?
% figure;
% DP_var2 = var([D6_P]);
% DA_var2 = var([D6_A]);
% semilogx(k,(DP_var2*10)-mean(DP_var2*10), 'g', 'LineWidth', 2)
% ?
% ?
function digit = drawDigit(whichDigit, pixelwidth)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth + 1;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth+ 1;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
end
