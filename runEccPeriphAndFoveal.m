clc
clear all

oldEyeris = [1 1 1 1 0];
subjectsAll = {'Zoe', 'Z091', 'AshleyDDPI','Z138','Z160'};

subject.Zoe = load('MATFiles/Zoe_MidStepVars.mat');
subject.Z091 = load('MATFiles/Z091_MidStepVars.mat');
subject.AshleyDDPI = load('MATFiles/AshleyDDPI_MidStepVars.mat');
subject.Z138 = load('MATFiles/Z138_MidStepVars.mat');


ecc = {'0eccEcc','10ecc','15ecc','25ecc','60ecc','120ecc', '240ecc', '360ecc'};
eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
    'Side60ecc','Side120ecc','Side240ecc','Side360ecc'};
eccVals = [0 10 15 25 60 120 240 360];
stabilization = {'Unstabilized'};
conditions = {'Uncrowded','Crowded'};
params.em = {'Drift'};

c = brewermap(12,'Paired');

%% Absolute Values
figure('units','normalized','outerposition',[0.2 0.05 .4 .6]) %plots all absolute thresholds for each condition
counter = 1;
counterLeg = 1;
for ii = 1:length(subjectsAll)
    clear oneRow
    hold on
    for cIdx = 1:length(conditions)
        for sIdx = 1:length(stabilization)
            clear allValsSubj
            cond = conditions{cIdx};
            stab = stabilization{sIdx};
            for numEcc = 1:length(ecc)
                    oneRow(numEcc) = subject.(subjectsAll{ii}).subjectCond.(cond).(stab).(eccNames{numEcc}).threshInfo.thresh;
                    boots(numEcc) = confInter95(subject.(subjectsAll{ii}).subjectCond.(cond).(stab).(eccNames{numEcc}).threshInfo.threshB)
                    oneRowEcc(numEcc) = eccVals(numEcc);
                    numTrials(ii,numEcc) = sum(subject.(subjectsAll{ii}).subjectCond.(cond).(stab).(eccNames{numEcc}).threshInfo.em.valid);
            end
            hold on
            %             yyaxis left
            if bitget(counter,1) %odd
                leg(counterLeg) = plot(eccVals(~isnan(oneRow))+1,oneRow(~isnan(oneRow)),'-o','Color',c(counter,:),...
                    'MarkerFace',c(counter,:),'MarkerSize', 10, 'LineWidth',3);
                counterLeg = counterLeg + 1;
            else %even
                plot(eccVals(~isnan(oneRow))+1,oneRow(~isnan(oneRow)),'--o','Color',c(counter,:),...
                    'MarkerFace',c(counter,:),'MarkerSize', 10, 'LineWidth',3);
            end
            
            hold on
            %             logMarVals = log10(oneRow/2);
            %             sd = 20*(10.^(logMarVals));
            counter = counter + 1;
        end
        forBouma{cIdx}(ii,:) = oneRow;
        numberTrials{cIdx}(ii,:) = numTrials(ii,:);
        bootsThresh{cIdx}(ii,:) = boots;
    end
end
hold on
ylabel('Threshold (arcmin)');
xlim([.7 500])
x = [30 30];
y = [0 50];
set(gca,'xtick',[1 10 15 25 60 120 240 360],'xticklabel',...
    string([0 10 15 25 60 120 240 360]), 'FontSize',14,'YScale', 'log','XScale', 'log',...
    'ytick',[0 .50 1 1.5 2 2.5 5 10 30 50],'yticklabel',...
    string([0 .50 1 1.5 2 2.5 5 10 30 50]))
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14)
xlabel('Eccentricity (arcmin)')
xtickangle(60)
line(x,y,'Color','k','LineStyle','--')
legend(leg, subjectsAll,...
    'Location','northwest');

temp2 = mean(forBouma{2});
figure;
[~,p,bC,r] = LinRegression(eccVals(5:8), temp2(5:8),...
    0,NaN,1,0);

peripheralData = forBouma;
peripheralData{3} = eccVals;
peripheralData{4} = subjectsAll;
peripheralData{5} = numTrials;
peripheralData{6} = bootsThresh;
save('MATFiles/peripheralCrowdingData','peripheralData');
%% Bouma Law
%%% plot again in terms of critical spacing (* 1.4)
figure;
for ii = 1:length(subjectsAll)
plot([eccVals], forBouma{2}(ii,:)*(1.4),'-o','LineWidth',3) %plotting in terms of critical spacing
hold on
end
%%% plot Boumas law
%%% "critical spacing for identification of small letters is roughly half
%%% the eccentricity"; stim of 1 = cs of 1.4
sigma = eccVals;
w = mean(forBouma{2});
BoumaLaw = (0.5 * sigma);
hold on
plot(sigma, BoumaLaw,'--','Color','r',...
    'MarkerSize',12,'MarkerFaceColor','k','LineWidth',3);
% xlim([0 30])
% ylim([2 5])
ylabel('Critical Spacing (arcmin)')
% title('Critical Spacing x3')
title('Critical Spacing x1')

coefficients = polyfit(eccVals,mean(forBouma{2}), 1);
xFit = linspace(min(eccVals), max(eccVals), 1000);
yFit = polyval(coefficients , xFit);
% m = (yFit(end)-yFit(1))/(xFit(end)-xFit(1)) ; %same as coefficents(1)
hold on
temp = plot(xFit, yFit,'--','Color','k',...
    'MarkerSize',12,'MarkerFaceColor','k','LineWidth',3);
hold on
 plot([0 10 15 25],mean(forLaterOneRow),'o','MarkerSize',12,'MarkerFaceColor','k');

legend([leg2 temp], [subjectsAll {sprintf('Best Fit Line, m=%.2f', m)}],...
    'Location','northwest');
ylim([0 2])

saveas(gcf, '../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/acrossTheFovea/BoumaLawPeriphery.png');
saveas(gcf, '../../../CrowdingTopology/Documents/Overleaf_CrowdingTopology/figures/acrossTheFovea/BoumaLawPeripheryx3.png');
%% Replicate Toet and Levi Figure
figure('units','normalized','outerposition',[0.2 0.05 .4 .6]) %plots all absolute thresholds for each condition
counter = 1;
counterLeg = 1;
for ii = 1:length(subjectsAll)
    if ii == 1
        ecc = {'0eccEcc','10ecc','15ecc','25ecc','60ecc','120ecc', '240ecc', '360ecc'};
        eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
            'Side60ecc','Side120ecc','Side240ecc','Side360ecc'};
        eccVals = [0 10 15 25 60 120 240 360];
        stabilization = {'Unstabilized'};
        conditions = {'Uncrowded','Crowded'};
        params.em = {'Drift'};
    elseif ii == 2
        ecc = {'0eccEcc','10ecc','15ecc','25ecc','60ecc', '240ecc', '360ecc'};
        eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
            'Side60ecc','Side240ecc','Side360ecc'};
        eccVals = [0 10 15 25 60 240 360];
        stabilization = {'Unstabilized'};
        conditions = {'Uncrowded','Crowded'};
        params.em = {'Drift'};
    elseif ii == 3
         ecc = {'0eccEcc','10ecc','15ecc','25ecc','120ecc','240ecc', '360ecc'};
        eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
            'Side120ecc','Side240ecc','Side360ecc'};
        eccVals = [0 10 15 25 120 240 360];
        stabilization = {'Unstabilized'};
        conditions = {'Uncrowded','Crowded'};
        params.em = {'Drift'};
    end
    %     counter = 1;
    clear oneRow
    hold on
    for cIdx = 1:length(conditions)
        for sIdx = 1:length(stabilization)
            clear allValsSubj
            cond = conditions{cIdx};
            stab = stabilization{sIdx};
            for numEcc = 1:length(ecc)
              
                    oneRow(numEcc) = subject.(subjectsAll{ii}).subjectCond.(cond).(stab).(eccNames{numEcc}).threshInfo.thresh;
                    oneRowEcc(numEcc) = eccVals(numEcc);

            end
            hold on
            clear normOneRow
            eccValsPLOT(~isnan(oneRow)) = eccVals(~isnan(oneRow))/60; %convert to deg
            if ~bitget(counter,1)
                oneRow(~isnan(oneRow)) = oneRow(~isnan(oneRow))/60; %convert crowded to deg
            end
            %             normOneRow = oneRow - oneRow(1);
            coefficients = polyfit(eccValsPLOT(~isnan(oneRow)), oneRow(~isnan(oneRow)), 1);
            xFit = linspace(min(eccValsPLOT(~isnan(oneRow))), max(eccValsPLOT(~isnan(oneRow))), 1000);
            % Get the estimated yFit value for each of those 1000 new x locations.
            yFit = polyval(coefficients , xFit);
            if bitget(counter,1) %odd (uncorwded)
                subplot(1,2,1)
                leg(counterLeg) = plot(eccValsPLOT(~isnan(oneRow)),oneRow(~isnan(oneRow)),'o','Color',c(counter,:),...
                    'MarkerFace',c(counter,:),'MarkerSize', 10, 'LineWidth',3);
                counterLeg = counterLeg + 1;
                title('Uncrowded')
                ylabel('Threshold (arcmin)');
            else %even
                subplot(1,2,2)
                plot(eccValsPLOT(~isnan(oneRow)),oneRow(~isnan(oneRow)),'o','Color',c(counter,:),...
                    'MarkerFace',c(counter,:),'MarkerSize', 10, 'LineWidth',3);
                title('Crowded')
                ylabel('Threshold (deg)');
            end  
            xlabel('Eccentricity (deg)');
            axis square
            hold on
            plot(xFit, yFit, '-', 'LineWidth', 2, 'Color', c(counter,:));
            hold on
            counter = counter + 1;
        end
    end
end
hold on
% ylabel('\Delta Threshold (arcmin)');
subplot(1,2,1)
ylim([-1 15])
xlim([-1 13])
subplot(1,2,2)
ylim([-.5 7])
xlim([-1 13])
% xlim([-10 370])
x = [30 30];
y = [0 50];
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14,'YScale', 'log')
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14) %loh(0)  = inf
% xlabel('Eccentricity (arcmin)')
% xtickangle(60)
% line(x,y,'Color','k','LineStyle','--')
legend(leg, {'Zoe','Z091', 'Ashley'},...
    'Location','northwest');

%% Average Across Subjects

name = subject.Zoe.subjectCond.Uncrowded.Unstabilized;
subjectsThresholds.Uncrowded.ecc(1,:) = [name.Center0ecc.threshInfo.thresh ... %0
    name.Side10ecc.threshInfo.thresh ... %10
    name.Side15ecc.threshInfo.thresh ... %15
    name.Side25ecc.threshInfo.thresh ... %25
    NaN,... %60
    name.Side120ecc.threshInfo.thresh ... %120
    name.Side240ecc.threshInfo.thresh ... %240
    name.Side360ecc.threshInfo.thresh]; %360

name = subject.Z091.subjectCond.Uncrowded.Unstabilized;
subjectsThresholds.Uncrowded.ecc(2,:) = [name.Center0ecc.threshInfo.thresh ... %0
    name.Side10ecc.threshInfo.thresh ... %10
    name.Side15ecc.threshInfo.thresh ... %15
    name.Side25ecc.threshInfo.thresh ... %25
    name.Side60ecc.threshInfo.thresh... %60
    NaN,... %120
    name.Side240ecc.threshInfo.thresh ... %240
    name.Side360ecc.threshInfo.thresh]; %360

name = subject.AshleyDDPI.subjectCond.Uncrowded.Unstabilized;
subjectsThresholds.Uncrowded.ecc(3,:) = [name.Center0ecc.threshInfo.thresh ... %0
    name.Side10ecc.threshInfo.thresh ... %10
    name.Side15ecc.threshInfo.thresh ... %15
    name.Side25ecc.threshInfo.thresh ... %25
    NaN,... %60
    NaN,... %120
    name.Side240ecc.threshInfo.thresh ... %240
    name.Side360ecc.threshInfo.thresh]; %360
%%%%%%%%%
name = subject.Zoe.subjectCond.Crowded.Unstabilized;
subjectsThresholds.Crowded.ecc(1,:) = [name.Center0ecc.threshInfo.thresh ... %0
    name.Side10ecc.threshInfo.thresh ... %10
    name.Side15ecc.threshInfo.thresh ... %15
    name.Side25ecc.threshInfo.thresh ... %25
    name.Side60ecc.threshInfo.thresh... %60
    name.Side120ecc.threshInfo.thresh ... %120
    name.Side240ecc.threshInfo.thresh ... %240
    name.Side360ecc.threshInfo.thresh]; %360

name = subject.Z091.subjectCond.Crowded.Unstabilized;
subjectsThresholds.Crowded.ecc(2,:) = [name.Center0ecc.threshInfo.thresh ... %0
    name.Side10ecc.threshInfo.thresh ... %10
    name.Side15ecc.threshInfo.thresh ... %15
    name.Side25ecc.threshInfo.thresh ... %25
    name.Side60ecc.threshInfo.thresh... %60
    NaN,... %120
    name.Side240ecc.threshInfo.thresh ... %240
    name.Side360ecc.threshInfo.thresh]; %360

name = subject.AshleyDDPI.subjectCond.Crowded.Unstabilized;
subjectsThresholds.Crowded.ecc(3,:) = [name.Center0ecc.threshInfo.thresh ... %0
    name.Side10ecc.threshInfo.thresh ... %10
    name.Side15ecc.threshInfo.thresh ... %15
    name.Side25ecc.threshInfo.thresh ... %25
    NaN,... %60
    NaN,... %120
    name.Side240ecc.threshInfo.thresh ... %240
    name.Side360ecc.threshInfo.thresh]; %360

% logMarVals = log10(oneRow/2);
figure;
clear leg
leg(1) = errorbar([0 10 15 25 60 120 240 360]/60, log10(nanmean(subjectsThresholds.Uncrowded.ecc)/2),...
    log10(nanstd(subjectsThresholds.Uncrowded.ecc)/2)/2,'-o',...
    'Color', c(1,:),'MarkerFace',c(1,:),'MarkerSize', 10, 'LineWidth',3);
hold on
leg(2) = errorbar([0 10 15 25 60 120 240 360]/60, log10(nanmean(subjectsThresholds.Crowded.ecc)/2),...
    log10(nanstd(subjectsThresholds.Uncrowded.ecc)/2)/2,'-o',...
    'Color', c(2,:),'MarkerFace',c(2,:),'MarkerSize', 10, 'LineWidth',3);
xlim([-.5 10])
ylim([-.5 9]);
hold on
ylabel('MAR Acuity');
xlabel('Eccentricity (deg)');
% axis square
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14,'YScale', 'log')
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14)

legend(leg, {'Uncrowded', 'Crowded'},...
    'Location','northwest');
%% Norm Vals
figure('units','normalized','outerposition',[0.2 0.05 .4 .6]) %plots all absolute thresholds for each condition
counter = 1;
counterLeg = 1;
for ii = 1:length(subjectsAll)
    if ii == 1
        ecc = {'0eccEcc','10ecc','15ecc','25ecc','60ecc','120ecc', '240ecc', '360ecc'};
        eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
            'Side60ecc','Side120ecc','Side240ecc','Side360ecc'};
        eccVals = [0 10 15 25 60 120 240 360];
        stabilization = {'Unstabilized'};
        conditions = {'Uncrowded','Crowded'};
        params.em = {'Drift'};
    elseif ii == 2
        ecc = {'0eccEcc','10ecc','15ecc','25ecc','60ecc', '240ecc', '360ecc'};
        eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
            'Side60ecc','Side240ecc','Side360ecc'};
        eccVals = [0 10 15 25 60 240 360];
        stabilization = {'Unstabilized'};
        conditions = {'Uncrowded','Crowded'};
        params.em = {'Drift'};
    elseif ii == 3
        ecc = {'0eccEcc','10ecc','15ecc','25ecc','120ecc','240ecc', '360ecc'};
        eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
            'Side120ecc','Side240ecc','Side360ecc'};
        eccVals = [0 10 15 25 120 240 360];
        stabilization = {'Unstabilized'};
        conditions = {'Uncrowded','Crowded'};
        params.em = {'Drift'};
    end
    %     counter = 1;
    clear oneRow
    hold on
    for cIdx = 1:length(conditions)
        for sIdx = 1:length(stabilization)
            clear allValsSubj
            cond = conditions{cIdx};
            stab = stabilization{sIdx};
            for numEcc = 1:length(ecc)
               
                    oneRow(numEcc) = subject.(subjectsAll{ii}).subjectCond.(cond).(stab).(eccNames{numEcc}).threshInfo.thresh;
                    oneRowEcc(numEcc) = eccVals(numEcc);
            end
            hold on
            clear normOneRow
            normOneRow = oneRow - oneRow(1);
            if bitget(counter,1) %odd
                leg(counterLeg) = plot(eccVals(~isnan(oneRow)),normOneRow(~isnan(oneRow)),'-o','Color',c(counter,:),...
                    'MarkerFace',c(counter,:),'MarkerSize', 10, 'LineWidth',3);
                counterLeg = counterLeg + 1;
            else %even
                plot(eccVals(~isnan(oneRow)),normOneRow(~isnan(oneRow)),'--o','Color',c(counter,:),...
                    'MarkerFace',c(counter,:),'MarkerSize', 10, 'LineWidth',3);
            end
            
            hold on
           
            counter = counter + 1;
        end
    end
end
hold on
ylabel('\Delta Threshold (arcmin)');
xlim([-10 370])
x = [30 30];
y = [0 50];
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14,'YScale', 'log')
set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14) %loh(0)  = inf
% xlabel('Eccentricity (arcmin)')
xtickangle(60)
line(x,y,'Color','k','LineStyle','--')
legend(leg, {'Zoe','Z091', 'Ashley'},...
    'Location','northwest');
%% Difference between Uncrowded and Crowded
figure('units','normalized','outerposition',[0.2 0.05 .4 .6]) %plots all absolute thresholds for each condition
counter = 1;
counterLeg = 1;
c = brewermap(12,'Set2');

for ii = 1:length(subjectsAll)
%     if ii == 1
        ecc = {'0eccEcc','10ecc','15ecc','25ecc','60ecc','120ecc', '240ecc', '360ecc'};
        eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
            'Side60ecc','Side120ecc','Side240ecc','Side360ecc'};
        eccVals = [0 10 15 25 60 120 240 360];
        stabilization = {'Unstabilized'};
        conditions = {'Uncrowded','Crowded'};
        params.em = {'Drift'};
% % %     elseif ii == 2
% %         ecc = {'0eccEcc','10ecc','15ecc','25ecc','60ecc', '240ecc', '360ecc'};
% %         eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
% %             'Side60ecc','Side240ecc','Side360ecc'};
% %         eccVals = [0 10 15 25 60 240 360];
% %         stabilization = {'Unstabilized'};
% %         conditions = {'Uncrowded','Crowded'};
% %         params.em = {'Drift'};
% % %     elseif ii == 3
% %         ecc = {'0eccEcc','10ecc','15ecc','25ecc','120ecc','240ecc', '360ecc'};
% %         eccNames = {'Center0ecc','Side10ecc','Side15ecc','Side25ecc',...
% %             'Side120ecc','Side240ecc','Side360ecc'};
% %         eccVals = [0 10 15 25 120 240 360];
% %         stabilization = {'Unstabilized'};
% %         conditions = {'Uncrowded','Crowded'};
% %         params.em = {'Drift'};
% % %     end
    %     counter = 1;
    clear oneRow
    hold on
    for cIdx = 1%:length(conditions)
        for sIdx = 1:length(stabilization)
            clear allValsSubj
            cond = conditions{cIdx};%
            stab = stabilization{sIdx};
            for numEcc = 1:length(ecc)
               
                    oneRow(numEcc) = subject.(subjectsAll{ii}).subjectCond.Crowded.(stab).(eccNames{numEcc}).threshInfo.thresh - ...
                        subject.(subjectsAll{ii}).subjectCond.Uncrowded.(stab).(eccNames{numEcc}).threshInfo.thresh;
                    oneRowEcc(numEcc) = eccVals(numEcc);
            end
            hold on
            clear normOneRow
            normOneRow = oneRow - oneRow(1);
%             if bitget(counter,1) %odd
                leg(counterLeg) = plot(eccVals(~isnan(oneRow)),normOneRow(~isnan(oneRow)),'-o','Color',c(counter,:),...
                    'MarkerFace',c(ii,:),'MarkerSize', 10, 'LineWidth',3);
                counterLeg = counterLeg + 1;
%             else %even
%                 plot(eccVals(~isnan(oneRow)),normOneRow(~isnan(oneRow)),'--o','Color',c(counter,:),...
%                     'MarkerFace',c(counter,:),'MarkerSize', 10, 'LineWidth',3);
%             end
            
            hold on
           
            counter = counter + 1;
        end
    end
end
hold on
ylabel('\Delta Threshold (arcmin)');
xlim([-10 370])
x = [30 30];
y = [0 50];
% set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',14,'YScale', 'log')
set(gca,'xtick',eccVals,'xticklabel',string(eccVals), 'FontSize',10) %loh(0)  = inf
% xlabel('Eccentricity (arcmin)')
xtickangle(60)
line(x,y,'Color','k','LineStyle','--')
legend(leg, {'Zoe','Z091', 'Ashley','Z081'},...
    'Location','northwest');
