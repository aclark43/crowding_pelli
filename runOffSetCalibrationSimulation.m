%%runOffSetCalibrationSimulation

coef = eis_openCalibrationCoefficients('Calibration.eis');%Getting coefficients from Calibration.eis file
        
        % These are the coefficients from the Calibration file
        xCoef1 = coef(1:2:32);
        yCoef1 = coef(2:2:32);
        xCenter1 = coef(33:2:50);
        yCenter1 = coef(34:2:50);