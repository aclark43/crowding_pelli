function [timeOn, timeOff] = timeConvertTracker(params, pptrials, ii, fixation)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
if fixation
    if strcmp('D',params.machine)
        timeOn = ceil(pptrials{ii}.TimeFixationON/(1000/330));
        timeOff = floor(min(pptrials{ii}.TimeFixationOFF/(1000/330))); %, pptrials{ii}.ResponseTime/(1000/330)));
    else
        timeOn = ceil(pptrials{ii}.TimeFixationON);
        timeOff = floor(min(pptrials{ii}.TimeFixationOFF));%, pptrials{ii}.ResponseTime));
    end
else
    if strcmp('D',params.machine)
        timeOn = ceil(pptrials{ii}.TimeTargetON/(1000/330));
        timeOff = floor(min(pptrials{ii}.TimeTargetOFF/(1000/330), pptrials{ii}.ResponseTime/(1000/330)));
    else
        timeOn = ceil(pptrials{ii}.TimeTargetON);
        timeOff = floor(min(pptrials{ii}.TimeTargetOFF, pptrials{ii}.ResponseTime));
    end
end
end

