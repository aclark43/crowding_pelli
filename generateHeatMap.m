function limit = generateHeatMap(axisValue, uSW, result, xValues, ...
    yValues, stimulusSize, title_str, trialChar, params, driftIdx, t, figures, arcminEcc, pptrials)

%%%"HEAT MAPS"
%%% looks at the average position of drift over the course of a SW
%
%RESULT: an nxn matrix which contains the histogrammed x and y values
%   ie: result = histogram2(xValues, yValues, [-limit, limit,n_bins; -limit, limit,n_bins]);
%       result_ = result./max(max(result));

% N_BINS: the number of bins that you want in your data ie: (limit * 2) 1

% LIMIT: is the absolute x/y range 
%   (i.e. for spatial, my limit is 30 because the stimuli 
%   span about -20 to 20 arcmin from 0, so I do 30 to give some wiggle room).
%%Def:
% axisValue = how large you want your axis to be (-X 0 X)
% uSW = set as 1 is you don't care about stimulus size


% figure
load('./MyColormaps.mat');
set(gcf, 'Colormap', mycmap)

limit.xmin = floor(min(xValues));
limit.xmax = ceil(max(xValues));
limit.ymin = floor(min(yValues));
limit.ymax = ceil(max(yValues));

hold on
result(result==0)=NaN;
pcolor(linspace(limit.xmin, limit.xmax, size(result, 1)),...
       linspace(limit.ymin, limit.ymax, size(result, 1)),...
       result');


if figures.FIXATION_ANALYSIS
    %%%%ADD pptrials.FixationSize were
    %%%%(FixationSize*params.pixelAngle)
    if strcmp('D',params.machine)
        plot(0,0,'-ks','MarkerSize',16);
    else
        plot(0,0,'-ks','MarkerSize',16);
    end
    graphTitleLine1 = sprintf('Fixation_Trial_Drift_Heat_Map%s%s', params.timeString, params.ses);
else
    params.timeBin = 500;
%     stimuliSize = unique(round(double(uSW) * unique(params.pixelAngle), 1));
    stimuliSize = stimulusSize/2;
    width = stimuliSize*2;
    height = width * 5;
    centerX = -stimuliSize+arcminEcc;
    centerY = -stimuliSize*5;
%     spacing = 1.4;
%     arcminEcc = uEcc * params.pixelAngle;
    if params.crowded == true
        
        rectangle('Position',[-(width + (width * 1.4)) + arcminEcc, centerY, width, height],'LineWidth',1) %Right
        
        rectangle('Position',[(width * 1.4) + arcminEcc, centerY, width, height], 'LineWidth',1) %Left
        
        rectangle('Position',[centerX, -(height + (height * 1.4)), width, height], 'LineWidth',1) % Bottom
        
        rectangle('Position',[centerX, (height * 1.4), width, height], 'LineWidth',1) %Top
        
    end
    rectangle('Position',[centerX, centerY, width, height],'LineWidth',1)
    if figures.FOLDED
         rectangle('Position',[-centerX+stimuliSize, centerY, width, height],'LineWidth',1,'LineStyle','--')
    end
    format longG
    graphTitleLine1 = sprintf('Drift_Heat_Map_for_Stimuli_Size_%.3f arcmin_SW%i_',(stimuliSize), uSW);
end

graphTitleLine2 = sprintf('%s', (title_str));
title({graphTitleLine1,graphTitleLine2},'Interpreter','none');

plot([-limit.xmin limit.xmax], [0 0], '--k')
plot([0 0], [-limit.ymin limit.ymax], '--k')

set(gca, 'FontSize', 12) 
xlabel('X [arcmin]')
ylabel('Y [arcmin]')

% axisValue = 30;
axis tight
axis square
caxis([0 ceil(max(max(result)))])
shading interp;
colorbar

axis([-axisValue axisValue -axisValue axisValue]);

n = numel(driftIdx);
text(-(axisValue - 10),-(axisValue - 10),(sprintf('N=%d', n)));
text(-(axisValue - 10),-(axisValue - 15),(sprintf('Time=%dms', (t))));

% filepath = ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding');


% if params.DMS == 1
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/%s/%s%s%iTimeBin.epsc', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/JPEG/HeatMaps/DMS/%s%s%i.png', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/FIG/HeatMaps/DMS/%s%s%i.fig', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
% elseif params.D == 1
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/D/%s%s%i.epsc', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/JPEG/HeatMaps/D/%s%s%i.png', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/FIG/HeatMaps/D/%s%s%i.fig', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
% elseif params.MS ==1
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/MS/%s%s%i.epsc', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/JPEG/HeatMaps/MS/%s%s%i.png', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/FIG/HeatMaps/MS/%s%s%i.fig', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2, t));
% end

if params.DMS == 1
    emType = 'DMS';
elseif params.D == 1
    emType = 'D';
elseif params.MS ==1
    emType = 'MS';
end
timeRound = round(t,-2);
% saveas(gcf, sprintf('../../Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/%s/%s%s%s,%s.epsc', ...
%     trialChar.Subject, emType, graphTitleLine1, graphTitleLine2, mat2str(t), mat2str(params.timeBin)));
saveas(gcf, sprintf('../../Data/%s/Graphs/DriftAnalysis/JPEG/HeatMaps/%s/%s%s%s,%s%s.png', ...
    trialChar.Subject, emType, graphTitleLine1, ...
    graphTitleLine2, mat2str(timeRound), mat2str(params.timeBin),params.session));
saveas(gcf, sprintf('../../Data/%s/Graphs/DriftAnalysis/FIG/HeatMaps/%s/%s%s%s,%s%s.fig', ...
    trialChar.Subject, emType, graphTitleLine1, ...
    graphTitleLine2, mat2str(timeRound), mat2str(params.timeBin),params.session));
if figures.FIXATION_ANALYSIS
%     saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/FixationMaps/BySession/%s/%s%s%s,%s%s.png', ...
%         params.ses, emType, graphTitleLine1, ...
%     graphTitleLine2, mat2str(timeRound), mat2str(params.timeBin)));

 saveas(gcf, sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%s/%s%s%s,%s%s.png', ...
        params.ses, emType, graphTitleLine1, ...
    graphTitleLine2, mat2str(timeRound), mat2str(params.timeBin)));
end


end

% clear figure

