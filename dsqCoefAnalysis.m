function [mDSQ, binnedDC] = dsqCoefAnalysis(subjectCondition,subjectThreshCondition, subjectsAll, condition, c, fig, thresholdSWVals, drift, params)
%Looks at dsq calculated in runMultiple Subjects for each individual
%subject at each SW, as well as across Subj for threshold SW.

sw15Size = [3 3 3 3 3 5 4 4 4 4];
%%
if fig.FIGURE_ON
    for ii = 1:length(subjectsAll)
        figure
        for subIdx = 1:length(subjectCondition(ii).SW)
            strokeValue = (subjectCondition(ii).SW(subIdx));
%             strokeValue = sw15Size(ii);
            strokeWidth = sprintf('strokeWidth_%i',strokeValue);
            if strokeValue == 0
                continue
            end
            trialNum = length(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).dCoefDsq);
            %             for curIdx = 1:trialNum
            dsqSubj = (subjectThreshCondition(ii).em.ecc_0.(strokeWidth).dCoefDsq(1,1));
            plotSW = zeros(length(dsqSubj), 1);
            plotSW(:) = strokeValue;
            plotSW = plotSW';
            scatter(dsqSubj,plotSW,200,'filled');
            hold on
        end
        
        title(sprintf('DSQ %s, %s',subjectsAll{ii}, condition{1}))
        xlabel('Diffusion Coeficient')
        ylabel('Threshold Strokewidth')
        
        saveas(gcf, sprintf('../Data/AllSubjTogether/%s_DSQPerSubj_%s.png', ...
            subjectsAll{ii}, condition{1}));
    end
    % saveas(gcf, sprintf('../Data/AllSubjTogether/%s_DSQPerSubj_%s.png', ...
    %         subjectsAll{ii}, condition{1}));
    close all
    clear ii
end
%% Loops through threshold
for ii = 1:length(subjectsAll)
    if strcmp('Uncrowded',condition)
%         sw = sprintf('strokeWidth_%i',sw15Size(ii));
        sw = thresholdSWVals(ii).thresholdSWUncrowded;
    elseif strcmp('Crowded',condition)
        sw = thresholdSWVals(ii).thresholdSWCrowded;
    end
    if params.machine(ii) == 1
        samp = 1000;
    elseif params.machine(ii) == 2
        samp = 341;
    end
    
    mDSQ(ii) = subjectThreshCondition(ii).em.ecc_0.(sw).dCoefDsq;
    
%         xVal = [];
%         yVal = [];
%         xVal175 = [];
%         yVal175 = [];
    for numTrials = 1:length(subjectThreshCondition(ii).em.ecc_0.(sw).position)
%                 xVal = [xVal  subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).x];
%                 yVal = [yVal  subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).y];
%                 xVal175 = [xVal175  subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).x(1:175/params.conversionFac(ii))];
%                 yVal175 =  [yVal175  subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).y(1:175/params.conversionFac(ii))];
        
        x = subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).x;
        y = subjectThreshCondition(ii).em.ecc_0.(sw).position(numTrials).y;
        
        [~,~,individualTrialsDC(ii,numTrials),~,~,~,~,~] = ...
            CalculateDiffusionCoef(samp,struct('x',x, 'y', y));
        
        dataSmallLargeDC.DC(ii,numTrials) = individualTrialsDC(ii,numTrials);
        dataSmallLargeDC.correct(ii,numTrials) = subjectThreshCondition(ii).em.ecc_0.(sw).correct(numTrials);
        temp = (subjectThreshCondition(ii).em.ecc_0.(sw).stimulusSize);
        dataSmallLargeDC.stimSize(ii, numTrials) = temp(1);
        dataSmallLargeDC.tracesx{ii,numTrials} = x;
        dataSmallLargeDC.tracesy{ii,numTrials} = y;
        dataSmallLargeDC.numTrials = numTrials;
    end

    
end
%% Looks at histogram of DC and how performance changes based on small vs large DC within subject
% individualTrialsDC(individualTrialsDC == 0) = NaN;
dataSmallLargeDC.DC(dataSmallLargeDC.DC == 0) = NaN;

% structForBinning = [0 7 2 .1 3 3 .1 1.5 5 0;...
%     2 18 0 2.5 8 20 1 1 3 2.5]/6;

structForBinning = [0 7 3 .1 3 3 2 1 2 0;...
    1 18 1 2.5 8 20 2.5 1 3 2]/7; %%for threshold

numBinsHist = [25 25 25 25 25 100 25 25 25 25];
% individualTrialsDC(individualTrialsDC > 100) = NaN;


forPercent(1,:) = [44,5,36,33,1.25,7,28,34,30,27];
forPercent(2,:) = [52,98,71,65,86,96.9,67,55.25,67,55];

bin = [];
allIndTrialDC = [];
figure;
for ii = 1:length(subjectsAll)
    if params.machine(ii) == 1
        samp = 1000;
    elseif params.machine(ii) == 2
        samp = 341;
    end
    
    temp1 = [];
    temp2 = [];
    idxVals = [];
    idxVals = find(individualTrialsDC(ii,:) > 0);
    
    subplot(5,2,ii);
    histogram(individualTrialsDC(ii,:), numBinsHist(ii), 'FaceColor', c(ii,:));
      bin(ii,1) = prctile(individualTrialsDC(ii,idxVals),forPercent(1,ii),"all");
      bin(ii,2) = prctile(individualTrialsDC(ii,idxVals),forPercent(2,ii),"all");
%     bin(ii,1) = nanmean(individualTrialsDC(ii,:)) - std(individualTrialsDC(ii,:)*structForBinning(1,ii));
%     bin(ii,2) = nanmean(individualTrialsDC(ii,:)) + std(individualTrialsDC(ii,:)*structForBinning(2,ii));%large
    hold on
    temp1 = find(dataSmallLargeDC.DC(ii,:) < bin(ii,1));
    idxBinSmall.dc{ii} = dataSmallLargeDC.DC(ii,temp1);
    idxBinSmall.correct{ii} = dataSmallLargeDC.correct(ii,temp1);
    idxBinSmall.stimSize{ii} = dataSmallLargeDC.stimSize(ii,temp1);
     [~,~,idxBinSmall.recalcDC{ii},~,~,~,~,~] = ...
            CalculateDiffusionCoef(samp,...
            struct('x',dataSmallLargeDC.tracesx(ii,temp1), 'y', dataSmallLargeDC.tracesy(ii,temp1)));
    
    temp2 = find(dataSmallLargeDC.DC(ii,:) > bin(ii,2));
    idxBinLarge.dc{ii} = dataSmallLargeDC.DC(ii,temp2);
    idxBinLarge.correct{ii} = dataSmallLargeDC.correct(ii,temp2);
    idxBinLarge.stimSize{ii} = dataSmallLargeDC.stimSize(ii,temp2);
    
    [~,~,idxBinLarge.recalcDC{ii},~,~,~,~,~] = ...
            CalculateDiffusionCoef(samp,...
            struct('x',dataSmallLargeDC.tracesx(ii,temp2), 'y', dataSmallLargeDC.tracesy(ii,temp2)));
    
    line([bin(ii,1) bin(ii,1)], [0 80],'Color','k','LineStyle','--')
    line([bin(ii,2) bin(ii,2)], [0 80],'Color','r','LineStyle','--')
    title(sprintf('%i,%s',ii,subjectsAll{ii}));
    allIndTrialDC = [allIndTrialDC individualTrialsDC(ii,:)];
    xlim([0 70])
end
binnedDC.large = idxBinLarge;
binnedDC.small = idxBinSmall;
%%
% figure;
% for ii = 1:length(subjectsAll)
%     counterS = 1; counterL = 1;
%     largeIdxTrialAll = []; smallIdxTrialAll = [];
%     path = subjectThreshCondition(ii).em;
%     for i = 1:length(path.x)
%         if path.valid(i)
%             [~,~,recalcDCTrial,~,~,~,~,~] = ...
%                 CalculateDiffusionCoef(samp,...
%                 struct('x',path.x(i), 'y', path.y(i)));
%             
%             if recalcDCTrial < bin(ii, 1)
%                 smallIdxTrialAll(counterS) = i;
%                 counterS = counterS + 1;
%             
%             elseif recalcDCTrial > bin(ii, 2)
%                 largeIdxTrialAll(counterL) = i;
%                 counterL = counterL + 1;
%             end
%             
%         end
%     end
%     [threshSmall(ii)] = psyfitCrowding(path.size(smallIdxTrialAll)*2, ...
%         path.performance(smallIdxTrialAll), 'DistType', ...
%     'Normal','Chance', .25, 'Extra');
%     [threshLarge(ii)] = psyfitCrowding(path.size(largeIdxTrialAll)*2, ...
%         path.performance(largeIdxTrialAll), 'DistType', ...
%     'Normal','Chance', .25, 'Extra');
% end
% 
% figure;
% for ii = 1:length(subjectsAll)
%     plot([1 2 3], [threshSmall(ii) subjectThreshCondition(ii).thresh threshLarge(ii)],...
%         '-o');
%     hold on
%     
% end
% xticks([1 2 3])
%     xticklabels({'Small','All','Large'});
% 
% % figure;
% % for ii = 1:length(subjectsAll)
% %     plot(1, ii, 'o','Color', c(ii,:), 'MarkerFaceColor', c(ii,:));
% %     hold on
% % end
% 
% figure;
% counter = 1;
% for ii = 1:length(subjectsAll)
%     %     figure;
%     subplot(5,4,counter)
%     histogram(idxBinSmall.dc{ii},'FaceColor', c(ii,:));
%     xlabel('Small DC')
%     hold on
%     counter = counter + 1;
%     subplot(5,4,counter)
%     histogram(idxBinLarge.dc{ii},'FaceColor', c(ii,:));
%     xlabel('Large DC');
%     %     suptitle(sprintf('%s',ii));
%     counter = counter + 1;
% end
%%
figure;
% subplot(1,3,3);
for ii = 1:length(subjectsAll)
    if ii == 5 || ii == 6 || ii == 2
        percentCorrect1(ii) = NaN;
        percentCorrect2(ii) = NaN;
        continue
    end
    percentCorrect1(ii) = (sum(double(idxBinSmall.correct{ii}))/length(idxBinSmall.correct{ii}));
    numTrialsTemp.small(ii) = length(idxBinSmall.correct{ii});
    percentCorrect2(ii) = (sum(double(idxBinLarge.correct{ii}))/length(idxBinLarge.correct{ii}));
    numTrialsTemp.large(ii) = length(idxBinLarge.correct{ii});
    
    line([1 2], [percentCorrect1(ii) percentCorrect2(ii)],...
        'Color',[.8 .8 .8],'LineStyle','-')
    hold on
    plot(1,percentCorrect1(ii),'o','Color',[.8 .8 .8],...
        'MarkerFaceColor', c(ii,:), 'MarkerSize', 8);

    hold on

     plot(2,percentCorrect2(ii),'o','Color',[.8 .8 .8],...
        'MarkerFaceColor', c(ii,:), 'MarkerSize', 8);
   hold on

    
end %5, 6
line([1-.1 2-.1], [ nanmean(percentCorrect1)  nanmean(percentCorrect2)],...
    'Color','k','LineStyle','-','LineWidth',3)

% err951 = ci95(percentCorrect1);
% err952 = ci95(percentCorrect2);
% [~, p, ci, stats] = ttest(percentCorrect1, percentCorrect2);


[E1, E2, t, p, hh] = T_Test(percentCorrect1([1, 3:4, 7:10])', percentCorrect2([1, 3:4, 7:10])');
err951 = nanmean(percentCorrect1)-E1;
err952 = nanmean(percentCorrect2)-E2;

errorbar(1-.1, nanmean(percentCorrect1), E1/2, ...
    'o', 'Color','k','MarkerFaceColor' ,'k','LineWidth',4,'MarkerSize', 8);
errorbar(2-.1, nanmean(percentCorrect2), E2/2,...
    'o', 'Color','k','MarkerFaceColor' ,'k','LineWidth',4,'MarkerSize', 8);
xlim([.6 2.25]);
xticks([1 2])
xticklabels({'Small', 'Large'})
xlabel('Diffusion Constant');
ylabel('Proportion Correct');
text(1.2,.8,sprintf('p = %.3f',p),'FontSize',14);
line([1 2],[.78 .78],'Color','k');
% axis square
ylim([.4 .82])
set(gca,'FontSize',12);
yticks([.4:.1:.8])
% ttl = title('B');
% ttl.Units = 'Normalize';
% ttl.Position(1) = 0;
% ttl.HorizontalAlignment = 'left';  
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/DSQdistributions2%s.png', condition{1}));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/DSQdistributions2%s.epsc', condition{1}));



figure;
% subplot(1,3,[1 2])
for ii = [1 3 4 7:10]
    err95S = ci95(idxBinSmall.dc{ii});
    err95L = ci95(idxBinLarge.dc{ii});
 errorbar([mean(idxBinSmall.dc{ii}) mean(idxBinLarge.dc{ii})],...
     [mean(idxBinSmall.correct{ii}) mean(idxBinLarge.correct{ii})],...
     [mean(idxBinSmall.dc{ii})-err95S(1) mean(idxBinLarge.dc{ii})-err95L(1)],...
     [err95S(2)-mean(idxBinSmall.dc{ii}) err95L(2)-mean(idxBinLarge.dc{ii})],...
     'horizontal','-o',...
     'Color','k','MarkerFaceColor', c(ii,:),'MarkerSize', 14)
 hold on
%  plot(mean(individualTrialsDC(ii,:)),...
%      mean(dataSmallLargeDC.correct(ii,1:dataSmallLargeDC.numTrials(ii))), ...
%      'd','Color',[.8 .8 .8],...
%         'MarkerFaceColor', c(ii,:), 'MarkerSize',16);
end
xlabel('Diffusion Constant (arcmin^2/sec)');
% ylabel('Prop Correct');
ylim([.4 .82])
yticks([.4:.1:.8])
set(gca,'FontSize',16);
ylabel('Proportion Correct');
axis square
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/DSQdistributionsUncrowded.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/DSQdistributionsUncrowded.epsc');

% ttl = title('A');
% ttl.Units = 'Normalize';
% ttl.Position(1) = 0;
% ttl.HorizontalAlignment = 'left';  

e
% figure;
% colorCompare =  brewermap(4,'Paired');
% histogram(allIndTrialDC, 400)
% xlim([0 60]);
%
% n = histcounts(allIndTrialDC, 40,'Normalization','probability');
% errorbar_linehistogram( n, 40, colorCompare(4,:));
%
% smallBin
% largeBin

%% Find Trials with Specific H or L DC and performance
for ii = 1:length(subjectsAll)
    if mDSQ(ii) > 14
        idx = find(individualTrialsDC(ii,:) > 8 & individualTrialsDC(ii,:) < 12);
        perfDC10(ii) = mean(dataSmallLargeDC.correct(ii, idx))*100;
        idxNorm = find(individualTrialsDC(ii,:) > 14 & individualTrialsDC(ii,:) > 0);
        perfDC15(ii) = mean(dataSmallLargeDC.correct(ii, idxNorm))*100;
    elseif mDSQ(ii) < 10
        idx = find(individualTrialsDC(ii,:) > 9 & individualTrialsDC(ii,:) < 11);
        perfDC10(ii) = mean(dataSmallLargeDC.correct(ii, idx))*100;
        if ii == 5
            idxNorm = find(individualTrialsDC(ii,:) < 7 & individualTrialsDC(ii,:) > 0);
            idx = find(individualTrialsDC(ii,:) >= 10 & individualTrialsDC(ii,:) < 11);
            perfDC10(ii) = mean(dataSmallLargeDC.correct(ii, idx))*100;
        else
            idxNorm = find(individualTrialsDC(ii,:) < 6 & individualTrialsDC(ii,:) > 0);
            idx = find(individualTrialsDC(ii,:) > 9 & individualTrialsDC(ii,:) < 11);
            perfDC10(ii) = mean(dataSmallLargeDC.correct(ii, idx))*100;
        end
        perfDC7(ii) = mean(dataSmallLargeDC.correct(ii, idxNorm))*100;
    end
end

figure;
for ii = find(perfDC7 > 0)
plot([1 2],...
   [perfDC7(ii) perfDC10(ii)],'-o');
hold on
end
hold on
for ii = find(perfDC15 > 0)
plot([2 3],...
   [perfDC10(ii) perfDC15(ii)],'-o');
end

% plot(ones(1,length(perfDC15))*3,perfDC15,'o');
xlim([0 4])
ylim([20 100])
xticks([1 2 3])
xticklabels({'Small DC Subj (<7)','DC 9-11','Large DC Subj (>14)'})
ylabel('Performance');


for ii = 1:length(subjectsAll)
    normPerformance(ii) =  mean(dataSmallLargeDC.correct(ii, :))*100;
    if ii == 5
        idx = find(individualTrialsDC(ii,:) >= 10 & individualTrialsDC(ii,:) < 11);
    elseif ii == 2
        idx = find(individualTrialsDC(ii,:) >= 5 & individualTrialsDC(ii,:) < 10);
    else
        idx = find(individualTrialsDC(ii,:) > 8 & individualTrialsDC(ii,:) < 10);
    end
    dc10perf(ii) =  mean(dataSmallLargeDC.correct(ii, idx))*100;
end

figure;
for ii = 1:length(subjectsAll)
    if subjectThreshCondition(ii).em.allTraces.dCoefDsq > 14
        plot([3 2],[normPerformance(ii) dc10perf(ii)],'-o');
    else
        plot([1 2],[normPerformance(ii) dc10perf(ii)],'-o');
    end
    hold on
end
xlim([0 4])
ylim([20 100])
xticks([1 2 3])
xticklabels({'Small DC Subj (<14)','DC 9-11','Large DC Subj (>14)'})
ylabel('Performance');

%% Loops through every strokewidth
counterU = 1;
counter = 1;
figure;
for ii = 1:length(subjectsAll)
    sw = fieldnames(subjectThreshCondition(ii).em.ecc_0);
    if ii == 6
        minNumTrials = 25;
    else
        minNumTrials = 35;
    end
    for i = 1:length(sw)
        strokewidth = char(sw(i));
        distTabUnc(counterU).subject = ii;
        distTabUnc(counterU).swSize = (cell2mat(regexp(char(sw(i)),'\d*','Match')));
        size(ii,i) = str2double(cell2mat(regexp(char(sw(i)),'\d*','Match')));
        distTabUnc(counterU).cond = condition;
        distTabUnc(counterU).stimSize = ...
            double(subjectThreshCondition(ii).em.ecc_0.(strokewidth).stimulusSize);
        distTabUnc(counterU).threshold = subjectThreshCondition(ii).thresh;
        distTabUnc(counterU).dc = subjectThreshCondition(ii).em.ecc_0.(strokewidth).dCoefDsq;
   
        swSize = round(subjectThreshCondition(ii).em.ecc_0.(strokewidth).stimulusSize*4);
        if length(subjectThreshCondition(ii).em.ecc_0.(strokewidth).velocity) >= minNumTrials
            dc(ii,swSize) = subjectThreshCondition(ii).em.ecc_0.(strokewidth).dCoefDsq;
           actualSize(ii,swSize) = subjectThreshCondition(ii).em.ecc_0.(strokewidth).stimulusSize;
            numTrials(ii,swSize) = length(subjectThreshCondition(ii).em.ecc_0.(strokewidth).velocity);
        else
            dc(ii,swSize) = 0;
            actualSize(ii,swSize) = 0;
            numTrials(ii,swSize) = 0;
        end
        counterU = counterU + 1;
        dcVariation(i,:) = subjectThreshCondition(ii).em.ecc_0.(strokewidth).Dsq;
        counter = counter + 1;
    end
    subplot(5,2,ii)
    h = histogram(dcVariation);
    xlim([0 60])
    %     xlabel('Individual DC')
    h.FaceColor = c(ii,:);
    title(sprintf('%s, %.3fDC',subjectsAll{ii}, mDSQ(ii)));
end

set(gcf, 'Units', 'Normalized', 'OuterPosition', [1.2, .01, .6, 1.1]);
% suptitle(sprintf('Individual DC - %s',condition{1}));
saveas(gcf,sprintf('../../Data/AllSubjTogether/DSQdistributions%s.png', condition{1}));
saveas(gcf,sprintf('../../Data/AllSubjTogether/DSQdistributions%s.fig', condition{1}));
%% Plots histogram DC
if strcmp('Uncrowded',condition)
    y = [dc];
    y(y == 0) = NaN;

    
    load('bootsDC.mat');
    actualSize(actualSize == 0) = NaN;
%     cTemp = brewermap(9,'YlOrRd');
     [a b] = sort(mDSQ);
     colorDC =  flip(brewermap(length(subjectsAll),'YlGn'));
%      colorDC = brewermap(10,'PRGn')
    figure;
    for ii = 1:length(subjectsAll)
        I = [];
        idx = ~isnan(y(ii,:));
        clear upperY; clear lowerY
        for numPoints = find(idx)
            if ii == 10 && numPoints == 8
                yIdx = 7;
            else
                yIdx = numPoints;
            end
            [upperY(numPoints), lowerY(numPoints)] = confInter95(bootsDC{ii,yIdx});
        end
%         [h,p] = ttest(bootsDC{3, 2}', bootsDC{3, 8}')
        colorIdx = find(b == ii);
        temp = (find(idx));
        legForColors(ii) = plot(rescaleAshley(temp), y(ii,idx), '-o', ...
            'Color', colorDC(colorIdx,:), 'MarkerSize', 6, 'LineWidth',4);
        hold on
        figs = errorbar(rescaleAshley(temp), y(ii,idx), upperY(idx), lowerY(idx), 'o', ...
            'Color', 'k', 'LineWidth',1,'MarkerFaceColor',colorDC(colorIdx,:));
        clear temp; clear tempUpper; clear tempLower;
        temp = y(ii,idx);
        tempUpper = upperY(idx); 
        tempLower = lowerY(idx);
        if temp(1)+tempUpper(1) < temp(end)-tempLower(end)
             taggedSig(ii) = 1; %larger dc lower bound overlaps top of small
        elseif temp(1)+tempLower(1) < temp(end)-tempUpper(end)
             taggedSig(ii) = 2; %smaller dc lower bound overlaps large of small
        else
             taggedSig(ii) = 0;
         end
        hold on
        temp = y(ii,idx);
        minDC(ii)= temp(1); 
        maxDC(ii)= temp(end);

    end
    ylim([0 40])
    ylabel('Diffusion Constant')  
    for ii = 1:length(y)
        if ~isnan(nanmean(y(:,ii)))
            names(ii) = {string(nanmean(round(actualSize(:,ii),2)))};
        else
            names(ii) = {''};
        end
        ylabel('Diffusion Constant')
        xlabel('Stimulus Size')
    end
    %suptitle('Diffusion Constant Distributions (35+ Trials)');
    xticks([0 1])
    xticklabels({'Min Stimulus Size','Maximum Stimulus Size'})
    [h,p] = ttest(minDC,maxDC);
    axis square
    xlim([-0.25 1.25])
    ylim([0 35])

    tempIdx = [1 2 3 4:10]
    meanSmall = mean(minDC(tempIdx));
    meanLarge = mean(maxDC(tempIdx));
    temp = meanLarge - meanSmall;
    max(abs(maxDC(tempIdx) - minDC(tempIdx)))
    tempMean = mean(maxDC(tempIdx) - minDC(tempIdx));
    tempStd = std(maxDC(tempIdx)- minDC(tempIdx));
%     stdLarge = std(maxDC(tempIdx));
    
    percentChange = (meanLarge - meanSmall)/...
        meanLarge * 100;
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/WithinSubjDSQbyStrokewidth%s.png', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/WithinSubjDSQbyStrokewidth%s.epsc', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/WithinSubjDSQbyStrokewidth%s.png', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/WithinSubjDSQbyStrokewidth%s.epsc', condition{1}));


    saveas(gcf,sprintf('../../Data/AllSubjTogether/WithinSubjDSQbyStrokewidth%s.png', condition{1}));
    saveas(gcf,sprintf('../../Data/AllSubjTogether/WithinSubjDSQbyStrokewidth%s.eps', condition{1}));
   %% Normalized DC over all strokewidths
   
     figure;
    for ii = 1:length(subjectsAll)
        I = [];
        idx = ~isnan(y(ii,:));
     
        clear upperY; clear lowerY
        for numPoints = find(idx)
            [upperY(numPoints), lowerY(numPoints)] = confInter95(bootsDC{ii,numPoints});
        end
        colorIdx = find(b == ii);
%         subplot(3,4,ii);
        
         plot(find(idx), y(ii,idx)/max(y(ii,idx)), '-o', ...
            'Color', colorDC(colorIdx,:), 'MarkerSize',2, 'LineWidth',5);        
        hold on
        figs = errorbar(find(idx), y(ii,idx)/max(y(ii,idx)), ...
            upperY(idx)/max(y(ii,idx)), lowerY(idx)/max(y(ii,idx)), 'o', ...
            'Color', colorDC(colorIdx,:), 'LineWidth',1);
        hold on
    end
    ylim([0.25 1.6])
    ylabel('Normalized Diffusion Constant')  
    for ii = 1:length(y)
        if ~isnan(nanmean(y(:,ii)))
            names(ii) = {string(nanmean(round(actualSize(:,ii),2)))};
        else
            names(ii) = {''};
        end
        ylabel('Diffusion Constant')
        xlabel('Stimulus Size')
    end
%     suptitle('Normalized Diffusion Constant Distributions (35+ Trials)');
    xticks([1:18])
    xticklabels(names)

    xlim([1 11])
    
    colormap summer
%     cb = linspace(1,16,16);
%     set(hc, 'YTick',cb, 'YTickLabel',cb)
    
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/NormalizedWithinSubjDSQbyStrokewidth%s.png', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/NormalizedWithinSubjDSQbyStrokewidth%s.epsc', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/NormalizedWithinSubjDSQbyStrokewidth%s.png', condition{1}));

    saveas(gcf,sprintf('../../Data/AllSubjTogether/NormalizedWithinSubjDSQbyStrokewidth%s.png', condition{1}));
    saveas(gcf,sprintf('../../Data/AllSubjTogether/NormalizedWithinSubjDSQbyStrokewidth%s.eps', condition{1}));
    
    %%
    figure
    for ii = 1:length(subjectsAll)
        strokeValue =(subjectThreshCondition(ii).thresh);
        if strcmp('Uncrowded',condition)
            sw = thresholdSWVals(ii).thresholdSWUncrowded;
        elseif strcmp('Crowded',condition)
            sw = thresholdSWVals(ii).thresholdSWCrowded;
        end
        mDSQ(ii) = subjectThreshCondition(ii).em.ecc_0.(sw).dCoefDsq;
        errorBar(mDSQ(ii),strokeValue)
        hold on
        points(ii) = scatter(mDSQ(ii),strokeValue,100,c(ii,:),'filled');
    end
    xlabel('DSQ(Mean)')
    ylabel('Threshold Strokewidth')
    title(sprintf('DSQ,All Subjects,%s',condition{1}))
    legend([points],subjectsAll)
    
    saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjDSQ%s.png', condition{1}));
    
    
    thresholds = [subjectThreshCondition.thresh];
    %%
    figure;
    subplot(1,3,1);
    [sortedDC idxSortDC] = sort(mDSQ);
    
    dcFixation = [12.1695938110352,6.56951570510864,16.9994201660156,12.7618551254272,10.1022663116455,27.2168960571289,NaN,14.7789983749390,11.2414350509644,23.4073143005371];
    upperX = [2.99271335840225,0.817092697381973,2.42462277746201,1.98292314815521,1.84548361086845,10.1013016901016,0,3.90911644363403,3.22456899285316,5.51701744031906];
    lowerX = [2.46694526433945,0.759254653692246,2.27618479394913,1.72916257572174,1.74151771283150,9.17355010890961,0,3.54191333389282,2.70015940070152,5.45869342851639];
    
    bar([sortedDC' dcFixation(idxSortDC)']);
    xlabel('Subject');
    ylabel('Threshold DC');
    title('Acuity Task');
    ylim([0 35])
    xlim([0 11])
    hold on
    for ii = 1:length(subjectsAll)
        [errhigh(ii), errlow(ii)] = confInter95(subjectThreshCondition(ii).dsqBoot);
    end
    er = errorbar(1:(length(subjectsAll)),sortedDC,errlow(idxSortDC),errhigh(idxSortDC));
    er.Color = [0 0 0];
    er.LineStyle = 'none';
    % subplot(1,3,2);
    % dcMSEnhance = [6.42784357070923,6.08805942535400,13.0992002487183,7.97527265548706,9.61051940917969,13.5362033843994];
    % bar(sort(dcMSEnhance));
    % xlabel('Subject');
    % ylabel('DC');
    % title('MS Enhancement Task');
    % ylim([0 25])
    
    subplot(1,3,3);
    bar(sortedDC);
    xlabel('Subject');
    ylabel('Threshold DC');
    title('Task');
    ylim([0 35])
    xlim([0 11])
    hold on
    for ii = 1:length(subjectsAll)
        [errhigh(ii), errlow(ii)] = confInter95(subjectThreshCondition(ii).dsqBoot);
    end
    er = errorbar(1:(length(subjectsAll)),sortedDC,errlow(idxSortDC),errhigh(idxSortDC));
    er.Color = [0 0 0];
    er.LineStyle = 'none';
    
    subplot(1,3,2);
   
    %     dcFaces = [12.1166687011719,15.9552974700928,7.47786474227905,7.85327529907227,7.78105068206787,16.1998004913330,14.3209857940674,7.91298437118530,7.72795963287354,7.30919313430786,10.5822362899780,9.51309299468994];
    %     6.74703407287598,12.9180250167847,10.6092109680176,9.05163288116455,...
    %     7.15500736236572,7.77056026458740,9.61850357055664,8.83896732330322];
    % dcFaces = [10.7401151657105,17.1815872192383,7.38864374160767,6.22957277297974,...
    %     6.74703407287598,12.9180250167847,10.6092109680176,9.05163288116455,...
    %     7.15500736236572,7.77056026458740,9.61850357055664,8.83896732330322];
%     [sortedDCF idxSortDCF] = sort(dcFixation);
    bar(dcFixation(idxSortDC));
    xlabel('Subject');
    ylabel('Threshold DC');
    title('Fixation');
    ylim([0 35])
    hold on
%     for ii = 1:length(subjectsAll)
%         if ii == 7
%             errlow(ii) = 0;
%             errhigh(ii) = 0;
%         else
%             
% %         [errhigh(ii), errlow(ii)] = confInter95(subjectThreshCondition(ii).dsqBoot);
%           [upperX(ii), lowerX(ii)] = confInter95(fixation.First200.bootDSQ{ii});
% %           [errhigh(ii)] = confInter95(fixation.First200.bootDSQ{ii});
%         end
%     end
    er = errorbar(1:(length(subjectsAll)),...
        dcFixation(idxSortDC),...
        lowerX(idxSortDC),...
        upperX(idxSortDC));
    er.Color = [1 0 0];
    er.LineStyle = 'none';
%     
%     bar(sort(dcFixation));
%     xlabel('Subject');
%     ylabel('DC');
%     title('Fixation');
%     ylim([0 25])
%     
%     er = errorbar(1:(length(subjectsAll)),sort(dcFixation),errlow(idxSortDC),errhigh(idxSortDC));

    
    saveas(gcf,sprintf('../../Data/AllSubjTogether/BarDSQ%s.png', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/BarDSQ%s.png', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/BarDSQ%s.epsc', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/BarDSQ%s.eps', condition{1}));
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/BarDSQ%s.png', condition{1}));

    %%
    figure
    [~,p,b,r] = LinRegression(mDSQ,thresholds,0,NaN,1,0);
    for ii = 1:length(mDSQ)
        point(ii) = scatter(mDSQ(ii), thresholds(ii),1,[c(ii,1) c(ii,2) c(ii,3)],'filled');
        hold on
        [upperY, lowerY] = confInter95(subjectThreshCondition(ii).bootsAll);
        errorbar(mDSQ(ii), thresholds(ii), upperY, lowerY, 'Color', c(ii,:));
        hold on
        [upperX, lowerX] = confInter95(subjectThreshCondition(ii).dsqBoot);
        errorbar(mDSQ(ii), thresholds(ii), upperX, upperX, 'horizontal', 'Color', c(ii,:));
    end
else
    thresholds = [subjectThreshCondition.thresh];
    
    figure
%     subplot(1,2,1)
    [~,p,b,r] = LinRegression(mDSQ,thresholds,0,NaN,1,0);
    for ii = 1:length(mDSQ)
        point(ii) = scatter(mDSQ(ii), thresholds(ii),1,[c(ii,1) c(ii,2) c(ii,3)],'filled');
        %     hold on
        %     [upperY, lowerY] = confInter95(subjectThreshCondition(ii).bootsAll);
        %     errorbar(mDSQ(ii), thresholds(ii), upperY, lowerY, 'Color', c(ii,:));
        %     hold on
        %     [upperX, lowerX] = confInter95(subjectThreshCondition(ii).dsqBoot);
        %     errorbar(mDSQ(ii), thresholds(ii), upperX, upperX, 'horizontal', 'Color', c(ii,:));
    end
    
    
end

% for s = 1:length(subjectsAll)
%     tempDC(s) = mean(subjectThreshCondition(s).dsqBoot);
%     tempTh(s) = mean(subjectThreshCondition(s).bootsAll);
% end
%
% figure;
% [~,p,b,r] = LinRegression(tempDC,tempTh ,0,NaN,1,0);
% for ii = 1:length(mDSQ)
% %     point(ii) = scatter(mDSQ(ii), thresholds(ii), 1,[c(ii,1) c(ii,2) c(ii,3)],'filled');
% %     hold on
%     errorbar(mean(subjectThreshCondition(ii).dsqBoot), mean(subjectThreshCondition(ii).bootsAll), confInter95(subjectThreshCondition(ii).bootsAll), 'Color', c(ii,:));
%     hold on
%     errorbar(mean(subjectThreshCondition(ii).dsqBoot), mean(subjectThreshCondition(ii).bootsAll), confInter95(subjectThreshCondition(ii).dsqBoot), 'horizontal', 'Color', c(ii,:));
% end
% legend([point],subjectsAll)
rValue = sprintf('r = %.3f', r);
bValue = sprintf('b = %.3f', b);
pValue = sprintf('p = %.3f', p);
if strcmp('Uncrowded',condition{1})
    axis([0 40 1 2.2]);
    text(25,1.4,pValue)
    text(25,1.3,bValue)
    text(25,1.2,rValue)
elseif strcmp('Crowded',condition{1})
    axis([0 45 1 3.5]);
    text(25,1.5,pValue)
    text(25,1.3,bValue)
    text(25,1.1,rValue)
end

xlabel('Mean Diffusion Constant at Threshold')
ylabel('Strokewidth Threshold')
graphTitle1 = 'Diffusion Constant by Threshold Strokewidth';
graphTitle2 = (sprintf('%s', cell2mat(condition)));
title({graphTitle1,graphTitle2})
xlim([0 40])
% ylim([0 5])
axis square


saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%sUncrowdedSWbyDSQ.png', condition{1}));
saveas(gcf,sprintf('../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/%sUncrowdedSWbyDSQ.eps', condition{1}));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/%sUncrowdedSWbyDSQ.png', condition{1}));
saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/%sUncrowdedSWbyDSQ.eps', condition{1}));

saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjDSQbyStrokewidth%s.png', condition{1}));
saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjDSQbyStrokewidth%s.epsc', condition{1}));


figure;
% subplot(1,2,2)
for ii = 1:length(subjectsAll)
    [biasMeasure(ii), angleRad(ii)] = ...
        calculateBiasSingleValue(subjectThreshCondition(ii).em.ecc_0.(sw).Bias);
    p = polarplot(angleRad(ii),biasMeasure(ii),'o');
    p.Color = c(ii,:);
    p.MarkerSize = 15;
    p.MarkerFaceColor = c(ii,:);
    hold on
end
% suptitle('Bias Threshold Trials');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/ThresholdDriftBias.png');

figure;
for ii = 1:length(subjectsAll)
    subplot(3,4,ii)
    for i = 1:length(subjectThreshCondition(ii).em.ecc_0.(sw).velocity)
        plot(subjectThreshCondition(ii).em.ecc_0.(sw).SingleSegmentDsq(i,:),'-','Color',c(ii,:));
        hold on
    end
    title(subjectsAll{ii});
end
% suptitle('Single Segments at Threshold');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/AllSingleSegments.png');


end

