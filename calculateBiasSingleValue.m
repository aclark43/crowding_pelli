function [ size, angleRad, angleDeg ] = calculateBiasSingleValue( Bias )
%Calculates a bias single value from the DiffusionConsstant function
%   Bias is a 2 column vector of x and y bias values

idx = ~isnan(Bias(:,1)');
xBias = Bias(idx,1)';
yBias = Bias(idx,2)';
size = hypot(xBias(1,length(xBias)), yBias(1,length(yBias)));% Euc Distance

p1 = [0 0];
p2 = [xBias(1,length(xBias)) yBias(1,length(yBias))];
d = p2 - p1;

angleRad = atan2(d(2),d(1));
angleDeg = wrapTo360(rad2deg(atan2(d(2),d(1)))); %angle to degrees

end

