function [distTabUnc, distTabCro] = buildDCStruct...
    (justThreshSW, params, subjectUnc, subjectThreshUnc, subjectCro, subjectThreshCro, thresholdSWVals)

%Builds PRL struct through different SW
counterC = 1;
counterU = 1;


if justThreshSW
    for ii = 1:params.subNum
       
        strokewidth = thresholdSWVals(ii).thresholdSWUncrowded;
        infoLocatin = subjectThreshUnc(ii).em.ecc_0.(strokewidth);
        
        distTabUnc(counterU).swSize = regexp(thresholdSWVals(ii).thresholdSWUncrowded,'\d*','Match');
        distTabUnc(counterU).cond = 'uncrowded';
        distTabUnc(counterU).stimSize = ...
            double(subjectThreshUnc(ii).em.ecc_0.(strokewidth).stimulusSize);
        distTabUnc(counterU).dc = subjectThreshUnc(ii).em.ecc_0.(strokewidth).dCoefDsq;
        distTabUnc(counterU).sizePerform = subjectThreshUnc(ii).em.ecc_0.(strokewidth).performanceAtSize;
        distTabUnc(counterU).threshold = subjectThreshUnc(ii).thresh;
        distTabUnc(counterU).span = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).span);
        distTabUnc(counterU).area = subjectThreshUnc(ii).em.ecc_0.(strokewidth).areaCovered;
       
        xPos = 1;

        for xPosIdx = 1:length(subjectThreshUnc(ii).em.ecc_0.(strokewidth).position)
            
%             distTabUnc(counterU).x{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).x(:))';
%             distTabUnc(counterU).y{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).y(:))';
            
%             if params.machine(ii) == 1
                distTabUnc(counterU).x300{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).shortAnalysis.position(xPosIdx).x);
                distTabUnc(counterU).y300{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).shortAnalysis.position(xPosIdx).y);
%                 
%                 [~, ~, ~, ~, ~, SingleSegs, ~,~] = ...
%                     CalculateDiffusionCoef(1000, struct('x', x, 'y', y));
%                 
%                 if isempty(find(SingleSegs > 600))
%                     
%                     distTabUnc(counterU).x300{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).x(1:params.driftSize));
%                     distTabUnc(counterU).y300{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).y(1:params.driftSize));
%                     
%                     distTabUnc(counterU).x250First{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).x(1:250));
%                     distTabUnc(counterU).y250First{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).y(1:250));
%                     
%                     distTabUnc(counterU).x250Last{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).x(250:length(distTabUnc(counterU).x{xPosIdx})));
%                     distTabUnc(counterU).y250Last{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).y(250:length(distTabUnc(counterU).y{xPosIdx})));
%                     xPos = xPos + 1;
%                 end
%             elseif params.machine(ii) == 2
%                 x = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).positionSHORTER(xPosIdx).x(1:(params.driftSize/(1000/330))));
%                 y = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).positionSHORTER(xPosIdx).y(1:(params.driftSize/(1000/330))));
%                 
%                 [~, ~, ~, ~, ~, SingleSegs, ~,~] = ...
%                     CalculateDiffusionCoef(341, struct('x', x, 'y', y));
%                 
%                 
%                 if isempty(find(SingleSegs > 600))
% %                     plot(SingleSegs); hold on
%                     distTabUnc(counterU).x300{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).x(1:(params.driftSize/(1000/330))));
%                     distTabUnc(counterU).y300{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).y(1:(params.driftSize/(1000/330))));
%                     
%                     distTabUnc(counterU).x250First{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).x(1:250/(1000/330)));
%                     distTabUnc(counterU).y250First{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).y(1:250/(1000/330)));
%                     distTabUnc(counterU).x250Last{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).x(250/(1000/330):length(distTabUnc(counterU).x{xPos})));
%                     distTabUnc(counterU).y250Last{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPosIdx).y(250/(1000/330):length(distTabUnc(counterU).y{xPos})));
%                     xPos = xPos + 1;       
%                 end
%             end
%             clear x
%             clear y
%             clear SingleSegs
        end
        
        counterU = counterU + 1;
        
        distTabCro = [];
%         strokewidthC = thresholdSWVals(ii).thresholdSWCrowded;
%         distTabCro(counterC).swSize = regexp(thresholdSWVals(ii).thresholdSWCrowded,'\d*','Match');
%         distTabCro(counterC).cond = 'crowded';
%         distTabCro(counterC).stimSize = subjectThreshCro(ii).em.ecc_0.(strokewidthC).stimulusSize;
%         distTabCro(counterC).dc = subjectThreshCro(ii).em.ecc_0.(strokewidthC).dCoefDsq;
%         distTabCro(counterC).sizePerform = subjectThreshCro(ii).em.ecc_0.(strokewidthC).performanceAtSize;
%         distTabCro(counterC).threshold = subjectThreshCro(ii).thresh;
%         distTabCro(counterC).span = subjectThreshCro(ii).em.ecc_0.(strokewidthC).meanSpan;
%         distTabCro(counterC).area = subjectThreshCro(ii).em.ecc_0.(strokewidthC).areaCovered;
%         %       
%         for xPos = 1:length(subjectThreshCro(ii).em.ecc_0.(strokewidthC).position)
%             distTabCro(counterC).x{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidthC).position(xPos).x(:))';
%             distTabCro(counterC).y{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidthC).position(xPos).y(:))';
%             
%             if params.machine(ii) == 1
%                 distTabCro(counterC).x300{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidthC).position(xPos).x(1:params.driftSize));
%                 distTabCro(counterC).y300{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidthC).position(xPos).y(1:params.driftSize));
%             elseif params.machine(ii) == 2
%                 distTabCro(counterC).x300{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidthC).position(xPos).x(1:params.driftSize/(1000/330)));
%                 distTabCro(counterC).y300{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidthC).position(xPos).y(1:params.driftSize/(1000/330)));
%             end
%         end
%                 counterC = counterC + 1;
%         
        
    end
else
    for ii = 1:(params.subNum)
        sw = fieldnames(subjectThreshUnc(ii).em.ecc_0);
        for i = 1:length(sw)
            strokewidth = char(sw(i));
            distTabUnc(counterU).subject = ii;
            
            distTabUnc(counterU).swSize = (cell2mat(regexp(char(sw(i)),'\d*','Match')));
            distTabUnc(counterU).cond = 'uncrowded';
            distTabUnc(counterU).stimSize = ...
                double(subjectThreshUnc(ii).em.ecc_0.(strokewidth).stimulusSize);
            distTabUnc(counterU).dCoefDsq = subjectThreshUnc(ii).em.ecc_0.(strokewidth).dCoefDsq;
            
            for xPos = 1:length(subjectThreshUnc(ii).em.ecc_0.(strokewidth).position)
                distTabUnc(counterU).x{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).x(:));
                distTabUnc(counterU).y{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).y(:));
%                 
%                 if params.machine(ii) == 1
%                     distTabUnc(counterU).x300{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).x(1:params.driftSize));
%                     distTabUnc(counterU).y300{xPos} = (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).y(1:params.driftSize));
%                 elseif params.machine(ii) == 2
%                     distTabUnc(counterU).x300{xPos} = ...
%                         (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).x(1:params.driftSize/(1000/330)));
%                     distTabUnc(counterU).y300{xPos} = ...
%                         (subjectThreshUnc(ii).em.ecc_0.(strokewidth).position(xPos).y(1:params.driftSize/(1000/330)));
%                 end
            end
            
            distTabUnc(counterU).correct = subjectThreshUnc(ii).em.ecc_0.(strokewidth).correct;
            distTabUnc(counterU).sizePerform = subjectThreshUnc(ii).em.ecc_0.(strokewidth).performanceAtSize;
            distTabUnc(counterU).threshold = subjectThreshUnc(ii).thresh;
            distTabUnc(counterU).span = mean(subjectThreshUnc(ii).em.ecc_0.(strokewidth).span);
            distTabUnc(counterU).area = subjectThreshUnc(ii).em.ecc_0.(strokewidth).areaCovered;
            
            counterU = counterU + 1;
        end
%         sw = fieldnames(subjectThreshCro(ii).em.ecc_0);
%         
%         for i = 1:length(sw)
%             
%             strokewidth = char(sw(i));
%             
%             distTabCro(counterC).subject = ii;
%             distTabCro(counterC).swSize = (cell2mat(regexp(char(sw(i)),'\d*','Match')));
%             distTabCro(counterC).cond = 'crowded';
%             distTabCro(counterC).stimSize = subjectThreshCro(ii).em.ecc_0.(strokewidth).stimulusSize;
%             distTabCro(counterC).dCoefDsq = subjectThreshCro(ii).em.ecc_0.(strokewidth).dCoefDsq;
%             for xPos = 1:length(subjectThreshCro(ii).em.ecc_0.(strokewidth).position)
%                 distTabCro(counterC).x{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).x(:));
%                 distTabCro(counterC).y{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).y(:));
%                 if params.machine(ii) == 1
%                     distTabCro(counterC).x300{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).x(1:params.driftSize));
%                     distTabCro(counterC).y300{xPos} = (subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).y(1:params.driftSize));
%                 elseif params.machine(ii) == 2
%                     distTabCro(counterC).x300{xPos} = ...
%                         (subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).x(1:params.driftSize/(1000/330)));
%                     distTabCro(counterC).y300{xPos} = ...
%                         (subjectThreshCro(ii).em.ecc_0.(strokewidth).position(xPos).y(1:params.driftSize/(1000/330)));
%                 end
%             end 
%             distTabCro(counterC).correct = subjectThreshCro(ii).em.ecc_0.(strokewidth).correct;
%             distTabCro(counterC).sizePerform = subjectThreshCro(ii).em.ecc_0.(strokewidth).performanceAtSize;
%             distTabCro(counterC).threshold = subjectThreshCro(ii).thresh;
%             distTabCro(counterC).span = subjectThreshCro(ii).em.ecc_0.(strokewidth).meanSpan;
%             distTabCro(counterC).area = subjectThreshCro(ii).em.ecc_0.(strokewidth).areaCovered;
%             counterC = counterC + 1;
%         end
%     end
end

end

