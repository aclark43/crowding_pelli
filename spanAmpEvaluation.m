function p = spanAmpEvaluation(params,spnAmpEvaluate,title_str,trialChar, em)
%%%Makes graph for plotting stimulus size OR flanker distance (based on
%%%condition) by the span of fixation. These are calculated in
%%%driftAnalysis
figure

for ii = 1:length(spnAmpEvaluate.stimulusSize)
    if spnAmpEvaluate.SW(ii) == 0
        spnAmpEvaluate.SW(ii) = NaN;
        spnAmpEvaluate.stimulusSize(ii) = NaN;
        spnAmpEvaluate.meanDriftSpan(ii) = NaN;
        spnAmpEvaluate.stdDriftSpan(ii) = NaN;
    end
end
%     scatter(spnAmpEvaluate.stimulusSize(:),spnAmpEvaluate.meanDriftSpan(:));
scatter(spnAmpEvaluate.stimulusSize(:),spnAmpEvaluate.meanDriftSpan(:));

%axis([0,4,0,10]);
hold on;
err = (spnAmpEvaluate.stdDriftSpan);
errorbar(spnAmpEvaluate.stimulusSize,spnAmpEvaluate.meanDriftSpan, err, 'LineStyle','none');
xlabel('Size of Stimulus (arcmin)')
ylabel('Average Span of Drift per Strokewidth')

%     xforGraph = round(double(unique(spnAmpEvaluate.stimulusSize)),1);
%     set(gca, 'XTick', xforGraph, 'xticklabel', {xforGraph})

graphTitleLine1 = ('Span_Evaluation_for_Uncrowded');
graphTitleLine2 = sprintf('%s', (title_str));
%     [~,p] = ttest(unique(spnAmpEvaluate.stdDriftSpan));
%     temp = [1 1]
%     temp = [1 2 3 4]
%      [h,p] = ttest2(temp,temp);


% lastSTDSpanIdx = length(spnAmpEvaluate.stdDriftSpan);

realSW = find(spnAmpEvaluate.SW>0);
swFirst = sprintf('strokeWidth_%i',realSW(1));
swLast = sprintf('strokeWidth_%i',realSW(length(realSW)));
% nonZero = (spnAmpEvaluate.stdDriftSpan > 0);
% spanNonZero = spnAmpEvaluate.stdDriftSpan(nonZero);

 [~,p] = ttest2(em.ecc_0.(swFirst).span,...
                em.ecc_0.(swLast).span);
           
% [~,p] = ttest2(spnAmpEvaluate.stimulusSize(realSW(1)),...
%                spnAmpEvaluate.stimulusSize(length(realSW)));

text1=(sprintf('N = %i', length(em.ecc_0.(swFirst).span)));
text2=(sprintf('N = %i', length(em.ecc_0.(swLast).span)));

text(spnAmpEvaluate.stimulusSize(...
    realSW(1)) + 0.05, ...
    double(em.ecc_0.(swFirst).meanSpan),...
    text1)
text(spnAmpEvaluate.stimulusSize(...
    realSW(length(realSW)))+0.05, ...
    double(em.ecc_0.(swLast).meanSpan), ...
    text2)

% [~,p] = ttest2([spanNonZero(1) spanNonZero(1+1)],...
%     [spanNonZero(length(spanNonZero)-1) spanNonZero(length(spanNonZero))]);

% [~,p] = ttest2([spnAmpEvaluate.stdDriftSpan(1) spnAmpEvaluate.stdDriftSpan(1+1)],...
%     [spnAmpEvaluate.stdDriftSpan(lastSTDSpanIdx) spnAmpEvaluate.stdDriftSpan(lastSTDSpanIdx-1)]);

%       p = anova1(temp);


% end

if p > 0.05
    graphTitleLine3 = sprintf('p = %.3f and is NOT SIG DIFFFERENT', p);
else
    graphTitleLine3 = sprintf('p = %.3f and IS SIG DIFFERENT', p);
end

title({graphTitleLine1,graphTitleLine2, graphTitleLine3},'Interpreter','none');

filepath = ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/');

saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/%s%s.epsc', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/JPEG/%s%s.png', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/FIG/%s%s.fig', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));

end

