function generateDriftVelocityMap(velX, velY, stimulusSize, params, title_str, trialChar)
%% Plots the heat map of drift velocity (from Natalya)
%   velX = structure with each cell containing the instaneous velocity of X
%   for a trials
%   velY = structure with each cell containing the instaneous velocity of Y
%   for a trials


%%
nVelX = [];
nVelY = [];
for ii = 1:length(velX)
    if velX{ii} < 100000
        if velY{ii} < 100000
   nVelX = [nVelX velX{ii}];
   nVelY = [nVelY velY{ii}];
        end
    end
end

%''0.5''after bins is the factor of how many bins you want to use (IE: .5 = half)
% radial = you want to plot is radialy (vs linearly)
% nr = normalized
% filt = filtered
figure
% if strcmp('strokeWidth_ALL',uSW)
%     lim = abs(mean(nVelX))+300;
%     ndhist(nVelX, nVelY, 'bins', 5, 'radial','axis',[-lim lim -lim lim],'nr','filt');
%     uSW = 0;
% else
    ndhist(nVelX, nVelY, 'bins', 2, 'radial','axis',[-200 200 -200 200],'nr','filt');
% end

% ndhist(cell2mat(cellfun(@(x) single(x), ft.fixation.drift.inst_vel_x, 'UniformOutput',0)),
% cell2mat(cellfun(@(x) single(x), ft.fixation.drift.inst_vel_y, 'UniformOutput',0)), ...

graphTitleLine1 = sprintf('Velocity Directional Map for Size %.3f SW Stimuli', unique(stimulusSize));
graphTitleLine2 = title_str;

title({graphTitleLine1,graphTitleLine2},'Interpreter','none');
% title('Fixation','FontSize',15)
load('./MyColormaps.mat')
set(gcf, 'Colormap', mycmap)
shading interp

filepath = ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/');

if params.DMS == 1
    saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/DMS/%s%s.epsc', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
    saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/JPEG/HeatMaps/DMS/%s%s.png', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/FIG/HeatMaps/DMS/%s%s.fig', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
elseif params.D == 1
    saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/D/%s%s.epsc', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
    saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/JPEG/HeatMaps/D/%s%s.png', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/FIG/HeatMaps/D/%s%s.fig', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
elseif params.MS ==1
    saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/EPSC/HeatMaps/MS/%s%s.epsc', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
    saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/JPEG/HeatMaps/MS/%s%s.png', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
%     saveas(gcf, sprintf('%s/Data/%s/Graphs/DriftAnalysis/FIG/HeatMaps/MS/%s%s.fig', filepath, trialChar.Subject, graphTitleLine1, graphTitleLine2));
end

end