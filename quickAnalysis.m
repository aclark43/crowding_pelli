%quick analysis
clear 
clc
close all

figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial figure
    'FIXATION_ANALYSIS', 0,...
    'CHECK_DRIFTS', 0);

subjectsAll = {'HUX1'};
conditionsAll = {'Uncrowded'};
eccAll = {'0ecc'};
machine = {'D'}; %(D = DDPI, A = DPI)


for ii = 1:length(subjectsAll)
    for jj = 1:length(conditionsAll)
        for kk = 1:length(eccAll)
           
        subject = subjectsAll(ii);%'Z002','Z023','Z013','Z005','Z002','Z063'};%Z063
        condition = conditionsAll(jj); %Crowded or Uncrowded
        ecc = eccAll(kk);
        
        em = {'Drift_MS'}; %Drift,MS,Drift_MS
        stabParam = {'Unstabilized'};
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Based on parameters set above%%
        
        params = struct(...
            'nBoots', 10,...
            'spanMax', 100,... %The Maximum Span
            'spanMin', 0,...  %The Minimum Span
            'crowded', true, ...
            'DMS', false, ...
            'D', false, ...
            'machine', machine, ...
            'MS', false );
        counter = 1;

        for emIdx = 1:length(em)
            currentEm = em{emIdx};
            for condIdx = 1:length(condition)
                currentCond = condition{condIdx};
                for subIdx = 1:length(subject)
                    currentSubj = subject{subIdx};
                    for stabIdx = 1:length(stabParam)
                        currentStab = stabParam{stabIdx};
                        for eccIdx = 1:length(ecc)
                            currentEcc = ecc{eccIdx};
                            
                            switch currentCond
                                case 'Crowded'
                                    if strcmp('Drift_MS',currentEm)
                                        params.DMS = true;
                                    elseif strcmp('Drift',currentEm)
                                        params.D = true;
                                    elseif strcmp('MS',currentEm)
                                        params.MS = true;
                                    end
                                case 'Uncrowded'
                                    params.crowded = false;
                                    if strcmp('Drift_MS',currentEm)
                                        params.DMS = true;
                                    elseif strcmp('Drift',currentEm)
                                        params.D = true;
                                    elseif strcmp('MS',currentEm)
                                        params.MS = true;
                                    end
                            end
%                             C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\HUX1\Uncrowded_Unstabilized_0ecc
                            filepath = sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/%s_%s_%s', ...
                                currentSubj, currentCond, currentStab, currentEcc);
                            
                            title_str = sprintf('%s_%s_%s_%s_%s',currentSubj, currentCond, ...
                                currentStab, currentEm, currentEcc);
                            
                            
                            
                            %%
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            [traces, threshInfo, trialChar] = ...
                                quickAnalysisOneSubject(params, filepath, figures, stabParam, condition, title_str);
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


                        end
                    end
                end
                
            end
            
        end
        end
    end
end

close all