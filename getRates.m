function meanAllRates = getRates(subjectMSCondition, subNum)
%Gets the mean ms rates over all strokewidths for each subject
for ii = 1:subNum
    if isfield(subjectMSCondition(ii).em,'ecc_0')
        swAll = fieldnames(subjectMSCondition(ii).em.ecc_0);
        for i = 1:numel(fieldnames(subjectMSCondition(ii).em.ecc_0))
            sw = char(swAll(i));
            msRate(i) = {subjectMSCondition(ii).em.ecc_0.(sw).microsaccadeRate};
        end
        meanAllRates(ii) = mean(cellfun(@mean,msRate));
    else
        meanAllRates(ii) = NaN;
    end
end