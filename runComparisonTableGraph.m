%%Runs Comparisons Across Subjects
close all
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%% Define Parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

params.fig = struct('FIGURE_ON', 0,...
    'FOLDED',0);
fig = params.fig;
% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013',...
%     'Z024','Z064','Z046','Z084','Z014'};%,...

subjectsAll = {'Z023','Ashley','Z005','Z002','Z013',...
   'Z024','Z064','Z046','Z084','Z014',...
   'Z091','Z138','Z181'};
% subjectsAll = {'Z023','Ashley','Z005'};%,...,'Z002'
machine = [1 1 1 1 1 2 2 2 2 2 2 2 2];
% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,...
%     'Z091','Zoe'}; %Drift Only Subjects
params.driftSize = 175;
% subjectsAll = {'Z024','Z064','Z046','Z084','Z014'};%'AshleyDDPI','Z002DDPI','Z046','Z084'};
% subjectsAll = {'Z023','Ashley','Z005','Z002',...
%     'Z013','Z024','Z064','Z046','Z084','Z014','Z063','Z070'};
params.em = {'Drift'};
% params.em = {'MS'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
em = params.em;
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
% c = brewermap(12,'Set3');
c = flip(brewermap(subNum,'YlGn'));
% c = jet(length(subjectsAll));
params.c = c;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[tempDataStruct, params] = loadVariablesComplete(params);

if strcmp('Drift',params.em)
    load('MATFiles/drift200_Boots_CleanZ024.mat');
    load('MATFiles/FixationBoots3_175.mat');
%     load('MATFiles/Fixation_11302020175.mat');

%     [idx, fixation] =  rebuildFixationData(fix, params);
%     params.idx = idx;
%     params.fixation = fixation;
end
%% Look at how performance is different when choosing "Threshold SW"
% checkSWThresholdPerformance(subjectThreshCro, subjectThreshUnc,c,...
%     thresholdSWVals,subjectsAll,subNum);

% figure;
if strcmp('Drift',params.em)
    for ii = 1:subNum
        tempDataStruct.performU(ii).em = tempDataStruct.subjectThreshUnc(ii).em;
        tempDataStruct.performC(ii).em = tempDataStruct.subjectThreshCro(ii).em;
        tempDataStruct.subjectThreshUnc(ii).bootsAll = tempDataStruct.performU(ii).bootStrapAll;
        tempDataStruct.subjectThreshUnc(ii).dsqBoot = drift.First500.bootDSQ{ii};
        tempDataStruct.subjectThreshCro(ii).bootsAll = tempDataStruct.performC(ii).bootStrapAll;
        allTrialsData.dsq(ii) = tempDataStruct.subjectThreshUnc(ii).em.allTraces.dCoefDsq;
        allTrialsData.span(ii) = mean(tempDataStruct.subjectThreshUnc(ii).em.allTraces.span);
        allTrialsData.curve(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.curvature));
        idx = ~cellfun(@isnan,tempDataStruct.subjectThreshUnc(ii).em.allTraces.mn_speed);
        allTrialsData.speed(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.mn_speed(idx)));
        allTrialsData.area(ii)= tempDataStruct.subjectThreshUnc(ii).em.allTraces.areaCovered;
        allTrialsData.bcea(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.bcea))/60;%in degrees
        allTrialsData.rms(ii) = rms(tempDataStruct.subjectThreshUnc(ii).em.allTraces.prlDistance)/60; %in degrees
        allTrialsData.sdP(ii) = std(tempDataStruct.subjectThreshUnc(ii).em.allTraces.prlDistance)/60;%in degrees
        allTrialsData.plf(ii) = mean(tempDataStruct.subjectThreshUnc(ii).em.allTraces.prlDistance);
        allTrialsData.subjectName{ii} = subjectsAll{ii};
        allTrialsData.shortAnalysis.dsq(ii) = tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.dCoefDsq;
        allTrialsData.shortAnalysis.span(ii)= mean(tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.span);
        allTrialsData.shortAnalysis.curve(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.curvature));
        idx2 = ~cellfun(@isnan,tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.mn_speed);
        allTrialsData.shortAnalysis.speed(ii)= mean(cell2mat(tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.mn_speed(idx2)));
        allTrialsData.shortAnalysis.area(ii)= tempDataStruct.subjectThreshUnc(ii).em.allTraces.shortAnalysis.areaCovered;
        allTrialsData.threshold(ii) = tempDataStruct.subjectThreshUnc(ii).thresh;
        [biasMeasure(ii), angleRad(ii)] = ...
            calculateBiasSingleValue(tempDataStruct.subjectThreshUnc(ii).em.allTraces.Bias);
        
        [biasMeasureT(ii), angleRadT(ii)] = ...
            calculateBiasSingleValue(tempDataStruct.subjectThreshUnc(ii).em.ecc_0.(tempDataStruct.thresholdSWVals(ii).thresholdSWUncrowded).Bias);
        %     test(ii) = std(drift.First500.bootDSQ{ii});
    end
end

%% Changing color to match their overall DC
[SubjectOrderDC,SubjectOrderDCIdx] = sort(allTrialsData.dsq);
figure
 [~,p,b,r] = LinRegression(allTrialsData.dsq,allTrialsData.speed,0,NaN,1,0);
% figure;
for ii = 1:10
    plot(ii,allTrialsData.dsq(ii),'o','Color',c(SubjectOrderDCIdx == ii,:),...
        'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:));
hold on

% hold on
% plot(allTrialsData.dsq(ii),allTrialsData.speed(ii),'o','Color',c(SubjectOrderDCIdx == ii,:),...
%         'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:));
end
xlabel('DC');
ylabel('Speed');
axis square

%% remake inst dsq plots
figure;
for iii = 1:10
    ii = (SubjectOrderDCIdx(iii));
    if machine(ii) == 1
       sampRate = 1;
    else
       sampRate = (1000/330);
    end
    subplot(2,5,iii)
%     if iii == 7
%         for i = 1:length(tempDataStruct.subjectThreshUnc(ii).em.allTraces.x)
% %             smx{i} = sgfilt(tempDataStruct.subjectThreshUnc(ii).em.allTraces.x{i}, 1, 41, 1);
% %             smy{i} = sgfilt(tempDataStruct.subjectThreshUnc(ii).em.allTraces.y{i}, 1, 41, 1);
%             
%             windowSize = 2;
%             b = (1/windowSize)*ones(1,windowSize);
%             a = 1;
%             %Find the moving average of the data and plot it against the original data.
%             
%             smx{i} = filter(b,a,tempDataStruct.subjectThreshUnc(ii).em.allTraces.x{i});
%             smy{i} = filter(b,a,tempDataStruct.subjectThreshUnc(ii).em.allTraces.y{i});
% 
%         end
%         [DCoef_DeTrended, Bias, pathW.dCoefDsq, DsqDetrended, pathW.Dsq, ...
%             pathW.SingleSegmentDsq, pathW.TimeDsq, NFix, RegLineDsq, bint] = ...
%             CalculateDiffusionCoef(341,...
%             struct('x',smx, ...
%             'y', smy));
%         figure;
%         plot(1:length(tempDataStruct.subjectThreshUnc(ii).em.allTraces.x{i}),...
%             tempDataStruct.subjectThreshUnc(ii).em.allTraces.x{i});
%         hold on
%          plot(1:length(tempDataStruct.subjectThreshUnc(ii).em.allTraces.y{i}),...
%             tempDataStruct.subjectThreshUnc(ii).em.allTraces.y{i});
% %         plot(1:length(smx{1}),smx{1});
%         plot(1:length(smx{i}),smx{i},'Color','k');
%         plot(1:length(smy{i}),smy{i},'Color','k');
%         xlabel('Samples');
%         ylabel('arcmin');
%     else
        pathW = tempDataStruct.subjectThreshUnc(ii).em.allTraces;
%     end
    
    plot(pathW.TimeDsq(1:200/sampRate),pathW.Dsq(1:200/sampRate),...
        'o','Color',c(SubjectOrderDCIdx == ii,:),'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:))
    
    xlim([.05 .2])
    hold on
        x = pathW.TimeDsq(1:200/sampRate); % Defines the domain as [-15,25] with a refinement of 0.25
        m  = pathW.RegLineDsq;  % Specify your slope
        x1 = 0; % Specify your starting x
        y1 = pathW.Dsq(100/sampRate);  % Specify your starting y
        y = m*(x - x1); 
        y1 = pathW.Dsq(100/sampRate) - y(100/sampRate);
        y = m*(x - x1) + y1;
    plot(x,y,'Color','k');
    ylabel('Displacement Square')
    xlabel('Time Lag (ms)')
    ylim([0 20])
    axis square
    title(sprintf('DC = %.2f',pathW.dCoefDsq));
    set(gca,'FontSize',10);
end
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DCFitRegLines.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DCFitRegLines.png');

%% Instantaneous Speed
vAll = [];
temp = [];
figure;
tiledlayout(2,5);
for iii = 1:subNum
    ii = find(SubjectOrderDCIdx == iii);
    pathW = tempDataStruct.subjectThreshUnc(ii).em.allTraces; 
     velXAll = [];  velYAll = [];
    for i = 1:length(pathW.x)
        if machine(ii) == 1
            sampRate = 1000;
        else
            sampRate = 341;
        end
        [Speed, Velocity, vel_x, vel_y] = ...
            CalculateDriftVelocity(struct('x',pathW.x{i}, 'y', pathW.y{i}),...
            1);
        
        forDSQCalc.x = {pathW.x{i}};
        forDSQCalc.y = {pathW.y{i}};
        [~,~,dc] = ...
            CalculateDiffusionCoef(sampRate, ...
           struct('x',forDSQCalc.x, 'y', forDSQCalc.y));

        
        velXAll = [velXAll vel_x];
        velYAll = [velYAll vel_y];

        smx = sgfilt(pathW.x{i}, 3, 41, 1);
        smy = sgfilt(pathW.y{i}, 3, 41, 1);
        velocity{i} = (sqrt(smx.^ 2 + smy.^ 2))*1000;
        
        dcAll{iii}(i) = dc;
        amplitude{iii}(i) = sqrt((pathW.x{i}(end) - pathW.x{i}(1))^2 + ...
            (pathW.y{i}(end) - pathW.y{i}(1))^2);
        vely{iii}(i) =  mean(velocity{i});
        
        
        
        if machine(ii) == 1
            vAll{ii}(i,:) = velocity{i}(1:500);
        else
            vAll{ii}(i,:) = velocity{i}(1:167);
        end
        
    end

    nexttile
    hold on
    if ii == 5
       ndhist((velXAll([1:60000, 80000:end])),...
           (velYAll([1:60000, 80000:end])), 'radial','nr','filt',...
           'axis',[-210 210 -210 210]);
    else
        temp{ii} = ([velXAll velYAll]);
       ndhist(velXAll, velYAll,....
           'radial','nr','filt','axis',[-210 210 -210 210]);
    end
    %     axis square
    load('./MyColormaps.mat');
    mycmap(1,:) = [1 1 1];
    set(gcf, 'Colormap', mycmap)
%     axis([-210 210 -210 210])
%     title(ii)
axis off
end
% hp4 = get(subplot(2,2,4),'Position')
% colorbar('Position', [hp4(1)+hp4(3)+0.01  hp4(2)  0.01  hp4(2)+hp4(3)*2.1])
% axis on
cb = colorbar('Ticks',[0,2500],...
    'TickLabels',[0 1],...
    'AxisLocation','in');
cb.Layout.Tile = 'east';
cb.Label.String = {'Normalized Probability'};
set(gca,'FontSize',16)
xlabel('arcmin/sec')
text(-200,200,'200''/sec','Color','red','FontSize',12,'FontWeight', 'Bold')
text(-100,100,'100''/sec','Color','red','FontSize',12,'FontWeight', 'Bold')

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/VelocityMaps.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/VelocityMaps.png');


vComb = [];
for ii = 1:subNum
    if machine(ii) == 1
        vComb(ii,1:500) = mean(vAll{ii});
    else
        temp = mean(vAll{ii});
        counter = 1;
        for i = 1:length(temp)
            spacedVel(counter) = temp(i);
            spacedVel(counter+1) = temp(i);
            spacedVel(counter+2) = temp(i);
            counter = counter + 3;
        end
        vComb(ii,1:500) = spacedVel(1:500);
    end
end
figure;
for  ii = 1:subNum 
    H = plot(1:length(vComb(ii,:)),(vComb(ii,:)),'-','Color',[200 200 200]/255);
    ylim([0 400])
     xlim([20 480])
%     H = errorbar(1:length(vComb(1:2,:)),mean(vComb(1:2,:)),sem(vComb(1:2,:)));
    hold on
    xlabel('Time')
    ylabel('Velocity')
    xlim([0 500])

end
hold on
% for ii = 1:subNum
    H = mseb(1:length(vComb(1:size(vComb),:)),mean(vComb(1:size(vComb),:)),...
        sem(vComb(1:size(vComb),:)),[],.5);
% end
    ylim([0 120])

    hold on
    xlabel('Time (ms)')
    ylabel('Drift Velocity (arcmin/sec)')
    xlim([45 455])
% end
set(gca,'FontSize',14)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/VelocityAv.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/VelocityAv.png');

%%% Speed & Amplitude
figure;
for ii = 1:10
    idxAll = find(dcAll{ii});
    subplot(1,3,1)
     scatter((amplitude{ii}(idxAll)), (vely{ii}(idxAll)),'o',...
        'MarkerFaceColor','m',...
        'MarkerEdgeColor','m',...
        'MarkerFaceAlpha',.05,'MarkerEdgeAlpha',.05);
    hold on
%     alpha(.5)
    hold on
     subplot(1,3,2)
     scatter((amplitude{ii}(idxAll)), (dcAll{ii}(idxAll)),'o',...
        'MarkerFaceColor','r',...
        'MarkerEdgeColor','r',...
        'MarkerFaceAlpha',.05,'MarkerEdgeAlpha',.05);
    hold on
     subplot(1,3,3)
     scatter((vely{ii}(idxAll)), (dcAll{ii}(idxAll)),'o',...
        'MarkerFaceColor','c',...
        'MarkerEdgeColor','c',...
        'MarkerFaceAlpha',.05,'MarkerEdgeAlpha',.05);
    hold on
    meanVel(ii) = mean(vely{ii}(idxAll));
    meanAmp(ii) = mean(amplitude{ii}(idxAll));
    meanDC(ii) = mean(dcAll{ii}(idxAll))
end
subplot(1,3,1)
axis square
ylabel('Speed (arcmin/sec)')
xlabel('Amplituide (arcmin)');
[~,p,b,r] = LinRegression(meanAmp,meanVel,0,NaN,1,0);
xlim([0 20])
ylim([0 200])
subplot(1,3,2)
axis square
ylabel('DC (arcmin^2/sec)')
xlabel('Amplituide (arcmin)');
[~,p,b,r] = LinRegression(meanAmp,meanDC,0,NaN,1,0);
xlim([0 20])
ylim([0 30])
subplot(1,3,3)
axis square
ylabel('DC (arcmin^2/sec)')
xlabel('Speed (arcmin/sec)')
[~,p,b,r] = LinRegression(meanVel,meanDC,0,NaN,1,0);
xlim([20 150])
ylim([0 30])
suptitle('Trial Average Regressions')
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/AmpAndSpeed.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/AmpAndSpeed.png');

figure;
[~,p,b,r] = LinRegression(meanVel(SubjectOrderDCIdx),drift.First500.dsq,0,NaN,1,0);
hold on
for ii = 1:10
     plot( meanVel(ii),allTrialsData.dsq(SubjectOrderDCIdx(ii)),'o',...
        'MarkerFaceColor',c(SubjectOrderDCIdx(ii),:),...
        'MarkerEdgeColor',c(SubjectOrderDCIdx(ii),:));
    hold on
%     temp(ii) = allTrialsData.threshold(SubjectOrderDCIdx == ii);
end
ylim([5 30])
xlim([30 80])
ylabel('Diffusion Constant (arcmin^2/sec)');
xlabel('Mean Velocity (arcmin/sec)');
%% Sanity check of Velocity
fc = 1000;
N = 1000; %(500ms)
% Bias = [90 10 1];

figure;
for ii = 1:100
    [Xe{ii}, Ye{ii}] = EM_Brownian(10,  fc, N, 1,[45, 20, 20]);
    plot(Xe{ii}-Xe{ii}(1),Ye{ii}-Ye{ii}(1))
    hold on
    axis([-10 10 -10 10])
    % condition(1) = {'10 DC, No Bias'};
end
axis square
velXAll = [];
velYAll = [];
for i = 1:100
    tempStruct.x = Xe;
    tempStruct.y = Ye;
    [Speed,Velocity, vel_x, vel_y] = ...
        CalculateDriftVelocity(struct('x',tempStruct.x{i},...
        'y',tempStruct.y{i}),1);
    
    velXAll = [velXAll vel_x];
    velYAll = [velYAll vel_y];
    
%     smx = sgfilt(tempStruct.x{i}, 3, 41, 1);
%     smy = sgfilt(tempStruct.y{i}, 3, 41, 1);
%     
%     velocity{i} = (sqrt(smx.^ 2 + smy.^ 2)* 1000); 
%     vAll{ii}(i,:) = velocity{i}(1:500); 
%     hold on
end
figure;

ndhist((velXAll), (velYAll),....
    'radial','nr','filt');

load('./MyColormaps.mat');
mycmap(1,:) = [1 1 1];
set(gcf, 'Colormap', mycmap)
title('500 Simulated Drift, 10DC 1000ms')
%% Horizontal vs Vertical Drift
% calculate the instaneous drift speed, then per each time you have a
% vector (ie what is the direction of the vector for each sample) what is
% the average "predominant" direction (use tthe circmean to take the
% average), seperate the vertical vs horizontal

% plot individual subjects to e the trends - bin on a single stimulus size
sw15Size = [3 3 3 3 3 5 4 4 4 4];
xallU = [];
yallU = [];
xallR = [];
yallR = [];
xallL = [];
yallL = [];
xallD = [];
yallD = [];
for ii = 1:subNum
    pathW = tempDataStruct.subjectThreshUnc(ii).em.ecc_0.(...
        tempDataStruct.thresholdSWVals(ii).thresholdSWUncrowded);
    Velocity = [];
    yall = [];
    for i = 1:length(pathW.position)
        x = (pathW.position(i).x-pathW.position(i).x(1));
        y = (pathW.position(i).y-pathW.position(i).y(1));
        
        [Speed,Velocity, vel_x, vel_y] = ...
            CalculateDriftVelocity(struct('x',x, 'y', y),...
            1);
        U = 1*x;
        V = 1*y;
%                 figure;
        q(i) = quiver(x,y,U,V,0);
%         hold on
%         plot(x,y)
%         avDirection(i) = rad2deg(atan((x)/y));
        r = atan2(q(i).VData,q(i).UData);

        avDirection(i) = (circ_mean(r')*180/pi);
        
        if circ_var(r') > 0.3
             VorH{ii}(1,i) = NaN;
             VorH{ii}(2,i) = NaN;
            continue;
        end
%         if avDirection(i) < 0
%             avDirection(i) = avDirection(i) + 360;
%         end
% x = vel_x;
% y = vel_y;
        if (avDirection(i) > 45 && avDirection(i) < 135)
            VorH{ii}(1,i) = 2; %up
            VorH{ii}(3,i) = Velocity;
            xallU = [xallU x];
            yallU = [yallU y];
        elseif (avDirection(i) < -135 || avDirection(i) >= 145)
            VorH{ii}(1,i) = 3; %LEFT
            xallL = [xallL x];
            yallL = [yallL y];
        elseif (avDirection(i) >= -135 && avDirection(i) <= -45)
            VorH{ii}(1,i) = 4; %DOWN
            xallD = [xallD x];
            yallD = [yallD y];
        elseif (avDirection(i) <= 45)
            VorH{ii}(1,i) = 1; %RIGHT
            xallR = [xallR x];
            yallR = [yallR y];
        elseif (avDirection(i) > -45)
            VorH{ii}(1,i) = 0; %RIGHT
            xallR = [xallR x];
            yallR = [yallR y];
        end
        VorH{ii}(2,i) = pathW.correct(i);
        VorH{ii}(3,i) = Velocity;
        yall = [yall y/x];
    end
    avDirectionAll{ii} = avDirection; 
    amountVert(ii) = mean(abs(yall));
    avSpeed(ii) = mean(Velocity);
end

figure;
subplot(1,2,1)
ndhist([xallU xallD], ...
    [yallU yallD], 'radial','nr','filt','axis',[-8 8 -8 8]);
title('Vertical');
subplot(1,2,2)
ndhist([xallL xallR],[yallL yallR], 'radial','nr','filt','axis',[-8 8 -8 8]);
title('Horizontal');
load('./MyColormaps.mat');
    mycmap(1,:) = [1 1 1];
    set(gcf, 'Colormap', mycmap)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DirVeloc.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DirVeloc.png');
   
aDirectionU = [];
aDirectionD = [];
aDirectionR = [];
aDirectionL = [];
for ii = 1:subNum
    horTrials = find(VorH{ii}(1,:) == 1 | VorH{ii}(1,:) == 3 | VorH{ii}(1,:) == 0);
    horPerf(ii) = (sum(VorH{ii}(2,horTrials)==1))/(length(horTrials));
    
    uPerf(ii) = (sum(VorH{ii}(2,find(VorH{ii}(1,:) == 2))==1))/(length(find(VorH{ii}(1,:) == 2)));
    dPerf(ii)= (sum(VorH{ii}(2,find(VorH{ii}(1,:) == 4))== 1))/(length(find(VorH{ii}(1,:) == 4)));
    lPerf(ii)= (sum(VorH{ii}(2,find(VorH{ii}(1,:) == 3))==1))/(length(find(VorH{ii}(1,:) == 3)));
    rPerf(ii)= (sum(VorH{ii}(2,find(VorH{ii}(1,:) == 1| VorH{ii}(1,:) == 0))==1))/...
        (length(find(VorH{ii}(1,:) == 1 | VorH{ii}(1,:) == 0)));
    
    
    uSpd(ii) = (mean(VorH{ii}(3,find(VorH{ii}(1,:) == 2))));
    dSpd(ii)= (mean(VorH{ii}(3,find(VorH{ii}(1,:) == 4))));
    lSpd(ii)= (mean(VorH{ii}(3,find(VorH{ii}(1,:) == 3))));
    rSpd(ii)= (mean(VorH{ii}(3,find(VorH{ii}(1,:) == 1| VorH{ii}(1,:) == 0))));
    
    vTrials = find(VorH{ii}(1,:) == 2 | VorH{ii}(1,:) == 4);
    vertPerf(ii) = (sum(VorH{ii}(2,vTrials)== 1))/((length(vTrials)));
    
    numaDirectionU(ii) = length(avDirectionAll{ii}(find(VorH{ii}(1,:) == 2)));
    numaDirectionD(ii) = length(avDirectionAll{ii}(find(VorH{ii}(1,:) == 4)));
    numaDirectionL(ii) = length(avDirectionAll{ii}(find(VorH{ii}(1,:) == 3)));
    numaDirectionR(ii) = length(avDirectionAll{ii}(find(VorH{ii}(1,:) == 1)));
    numaDirectionR2(ii) = length(avDirectionAll{ii}(find(VorH{ii}(1,:) == 0)));
    
    aDirectionU(ii) = nanmean(avDirectionAll{ii}(find(VorH{ii}(1,:) == 2)));
    aDirectionD(ii) = nanmean(avDirectionAll{ii}(find(VorH{ii}(1,:) == 4)));
    aDirectionL(ii) = nanmean(avDirectionAll{ii}(find(VorH{ii}(1,:) == 3)));
    aDirectionR(ii) = nanmean(avDirectionAll{ii}(find(VorH{ii}(1,:) == 1)));
    aDirectionR2(ii) = nanmean(avDirectionAll{ii}(find(VorH{ii}(1,:) == 0)));
    
    avDirectionAllComb(ii) = rad2deg(circ_mean(deg2rad(avDirectionAll{ii}')));
%     if  avDirectionAllComb(ii) < 0
%          avDirectionAllComb(ii) =  avDirectionAllComb(ii) + 360;
%     end
%     if (avDirectionAllComb(ii) > 45 && avDirectionAllComb(ii) < 135)
%         cardDir(ii) = 1; %up
%     elseif (avDirectionAllComb(ii) < -135 ||avDirectionAllComb(ii) >= 135)
%         cardDir(ii) = 2; % %LEFT 
%     elseif (avDirectionAllComb(ii) >= -135 && avDirectionAllComb(ii) <= -45)
%         cardDir(ii) = 3; %DOWN
%     elseif (avDirectionAllComb(ii) <= 45)
%         cardDir(ii) = 4; %RIGHT
%     elseif (avDirectionAllComb(ii) > -45)
%         cardDir(ii) = 4; %RIGHT
%     end
end

figure;
uPerf(uPerf == 0) = NaN;
uPerf(4) = NaN;
dPerf(dPerf == 1) = NaN;

% uPerf = find(uPerf > 0);
% subplot(2,3,[3 6]);
u95 = abs(ci95(uPerf)-nanmean(uPerf));
d95= abs(ci95(dPerf)-nanmean(dPerf));
l95= abs(ci95(lPerf)-mean(lPerf));
r95= abs(ci95(rPerf)-mean(rPerf));

for ii = 1:10
    plot([1.1 2.1 3.1 4.1],[(uPerf(ii)) (dPerf(ii)) (lPerf(ii)) (rPerf(ii))],...
        'o','Color',[.8 .8 .8],'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:));
    hold on
end

errorbar([1 2 3 4],[nanmean(uPerf) nanmean(dPerf) mean(lPerf) mean(rPerf)],...
    [u95(1) d95(1) l95(1) r95(1)],...
     [u95(1) d95(1) l95(1) r95(1)],'-o','Color','k','MarkerFaceColor','k',...
     'MarkerSize',8);
xticks([1 2 3 4])
xticklabels({'North','South','West','East'});
xlim([0.5 4.5])
ylabel('Proportion Correct');
xlabel('Drift Direction');
yticks([.4 .5 .6 .7 .8])
[h,p] = ttest(horPerf, vertPerf);
ylim([.4 .82])
set(gca,'FontSize',12)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/HorVertAv4.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/HorVertAv4.png');

[p,tbl,stats] = anova1([(uPerf)' (dPerf)' (lPerf)' (rPerf)'])
% results = multcompare(stats);
% 

figure;
hold on
for ii = 1:10
    plot([1.1 2.1],[(horPerf(ii)) (vertPerf(ii))],'-o','Color',[.8 .8 .8],...
        'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),'MarkerSize',8);
    hold on
    meanAvDire(ii) = rad2deg(circ_mean(deg2rad(avDirectionAll{ii}')));
end
temp = abs(meanAvDire);
for i = 1:length(temp)
    if temp(i) > 90
        temp(i) = (((temp(i) - 45)-90)*-2) + 45;
    end
end

% temp(find(temp>90)) = (((temp - 45)-90)*-2) + 45;
% figure;


% for i = 1:length(avDirectionAll{ii});
% polarhistogram(deg2rad(abs(avDirectionAll{ii})))
% hold on
% end
% xlim([0 3])
hold on

[E1, E2, t, p, hh] = T_Test(horPerf', vertPerf');


errorbar([1 2],[mean(horPerf) mean(vertPerf)],...
    [E1/2 E2/2],...
    '-o','MarkerFaceColor','k','Color','k','MarkerSize',10,'LineWidth',2);
xticks([1 2])
xticklabels({'Horizontal','Vertical'});
xlim([0.6 2.5])
ylabel('Proportion Correct');
xlabel('Drift Direction');
[h,p] = ttest(horPerf, vertPerf);
ylim([.4 .9])
set(gca,'FontSize',14)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/HorVertAv.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/HorVertAv.png');


figure
[~,p,b,r] = LinRegression(allTrialsData.threshold,amountVert,0,NaN,1,0);
ylim([0 2])
xlim([1 2])
xlabel('Threshold (arcmin)')
ylabel('ABS Y/X Motion Ratio');
set(gca,'FontSize',14)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/AmountVertAv.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/AmountVertAv.png');

%%%% EVERY TRIAL YOU CAN TELL IF IT'S VERTICAL OR HORIZONTAL - WHAT IS THE PROPORTIONAL OF
%%% h VS V = IDENTICAL MEANS THE SAME LEFT/RIGHT
%%% MORE VERTICAL (GREATER THAN 1 IE) THE RATIO CAN BE THE INDEX OF
%%% "VERTICALITY" 

%% Crowded vs Uncrowded
figure;
for ii = 1:10
    plot([1 2],[tempDataStruct.subjectThreshUnc(ii).thresh tempDataStruct.subjectThreshCro(ii).thresh],...
        '-o');
    uc(ii) = tempDataStruct.subjectThreshUnc(ii).thresh;
    cr(ii) = tempDataStruct.subjectThreshCro(ii).thresh;
hold on
end

figure;
x = uc;                      % Create Data
SEM = std(x)/sqrt(length(x));               % Standard Error
ts = tinv([0.025  0.975],length(x)-1);      % T-Score
CIU = mean(x) + ts*SEM;                      % Confidence Intervals

x = cr;                      % Create Data
SEM = std(x)/sqrt(length(x));               % Standard Error
ts = tinv([0.025  0.975],length(x)-1);      % T-Scores
CIC = mean(x) + ts*SEM;                      % Confidence Intervals

plot([1 2], [mean(uc) mean(cr)],'-');
hold on
errorbar(1,mean(uc), CIU(1)-mean(uc), CIU(2)-mean(uc),'o');
hold on
errorbar(2,mean(cr), CIC(1)-mean(cr), CIC(2)-mean(cr),'o');

axis([.75 2.25 0 2.5]);
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/VelocityAv.epsc');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/VelocityAv.png');

%% Other Studies Comparison
load('MATFiles/otherStudyComparisonFEM.mat');

for ii = 1:length(subjectsAll)
    forTable.(subjectsAll{ii}).thresh = tempDataStruct.subjectThreshUnc(ii).thresh;
    forTable.(subjectsAll{ii}).numTrials = length(tempDataStruct.subjectThreshUnc(ii).em.allTraces.xRecentered);
    forTable.(subjectsAll{ii}).dsq = (tempDataStruct.subjectThreshUnc(ii).em.allTraces.dCoefDsq);

end
%% histogram of VA variance
figure;
subplot(1,2,1)
[a,b] = sort([tempDataStruct.subjectThreshUnc(:).thresh]);
newColorGrad = brewermap(subNum,'PuBu');
for ii = 1:length(subjectsAll)
    [upperY(ii), lowerY(ii)] = confInter95(tempDataStruct.subjectThreshUnc(ii).bootsAll);
    bar(ii,a(ii),'FaceColor',newColorGrad(ii,:),'EdgeColor','k');
    hold on
    errorbar(ii,a(ii),upperY(ii), lowerY(ii),'Color','k')
end
ylim([1 2])
xlabel('Subject')
ylabel('Visual Acuity Threshold');


subplot(1,2,2)
[A, B] = sort(allTrialsData.dsq);
load('MATFiles/AllTrialDCBoots.mat')
% figure;
for ii = 1:subNum
    errhigh(ii) = stats500{ii}.ci(2)-allTrialsData.dsq(ii);
    errlow(ii) =  allTrialsData.dsq(ii)-stats500{ii}.ci(1);
end

C = fixation.First200.dsq(B);
h = bar(A);
ylabel('Diffusion Constant')
hold on
er = errorbar(1:(length(subjectsAll)),A,...
    errhigh(B),errlow(B));
er.Color = [0 0 0];
er.LineStyle = 'none';
ylim([0 22])
xlabel('Subject')
xlim([0 4])

saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\COARSES\GrantWriting_Rucci\Images\AcuityandDCVariance.png');
saveas(gcf,'C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\COARSES\GrantWriting_Rucci\Images\AcuityandDCVariance.epsc');

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/AcuityVariance.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/AcuityVariance.epsc');

figure;



%% Make HeatMaps
temp = load('MATFiles/Fixation_9212021500.mat');

for ii = 1:subNum
    axisVal = 15;
    figure;
    generateHeatMapSimple( ...
        tempDataStruct.subjectThreshUnc(ii).em.allTraces.xALL, ...
        tempDataStruct.subjectThreshUnc(ii).em.allTraces.yALL, ...
        'Bins', 30,...
        'StimulusSize', (allTrialsData.threshold(ii)),...
        'AxisValue', axisVal,...
        'Uncrowded', 1,...
        'Borders', 1);
    %         title(params.subject);
    colorbar
    caxis([0.2 1])
    hold on
    colorbar off
    plot([-axisVal axisVal], [0 0], '--k', 'LineWidth',3)
    plot([0 0], [-axisVal axisVal], '--k', 'LineWidth',3)
    set(gca,'FontSize',14)
    axis off
    text(-10, 12, sprintf('S%i', find(SubjectOrderDCIdx == ii)), ...
        'FontSize', 30, 'FontWeight', 'bold');
    rectangle('Position',[-8 -8 16 16])
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/HeatMap%i.png', find(SubjectOrderDCIdx == ii)));
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/HeatMap%i.epsc', find(SubjectOrderDCIdx == ii)));
end

for ii = 1:subNum
    axisVal = 15;
    figure;
    generateHeatMapSimple( ...
        temp.fix.x.(subjectsAll{ii}), ...
        temp.fix.y.(subjectsAll{ii}), ...
        'Bins', 30,...
        'StimulusSize', 10,...
        'AxisValue', axisVal,...
        'Uncrowded', 0,...
        'Borders', 1);
    %         title(params.subject);
    rectangle('Position',[-5, -5, 10, 10],'LineWidth',3)
    colorbar
    caxis([0.2 1])
    hold on
    colorbar 
    plot([-axisVal axisVal], [0 0], '--k', 'LineWidth',3)
    plot([0 0], [-axisVal axisVal], '--k', 'LineWidth',3)
    set(gca,'FontSize',14)
    axis off
    text(-10, 12, sprintf('S%i', find(SubjectOrderDCIdx == ii)), ...
        'FontSize',30, 'FontWeight', 'bold');
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/FixationHeatMap%i.png', find(SubjectOrderDCIdx == ii)));
    saveas(gcf,sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/FixationHeatMap%i.epsc', find(SubjectOrderDCIdx == ii)));
end

%% Performance Differences for Small vs Large DC Subject for 1 arcmin stim
figure;
pathwaySmall = {tempDataStruct.subjectThreshUnc(2).em.ecc_0.strokeWidth_2,...
                tempDataStruct.subjectThreshUnc(2).em.ecc_0.strokeWidth_3,...
                tempDataStruct.subjectThreshUnc(2).em.ecc_0.strokeWidth_4};
pathwayLarge = {tempDataStruct.subjectThreshUnc(9).em.ecc_0.strokeWidth_3,...
                tempDataStruct.subjectThreshUnc(9).em.ecc_0.strokeWidth_4,...
                tempDataStruct.subjectThreshUnc(9).em.ecc_0.strokeWidth_5};
for ii = 2
%     pathwaySmall = tempDataStruct.subjectThreshUnc(5).em.ecc_0.strokeWidth_2;
%     pathwayLarge = tempDataStruct.subjectThreshUnc(9).em.ecc_0.strokeWidth_3;
    stim1.smallDCPerf = pathwaySmall{ii}.performanceAtSize;
    stim1.largeDCPerf = pathwayLarge{ii}.performanceAtSize;
    stim1.smallSEM = sem(pathwaySmall{ii}.correct);
    stim1.largeSEM = sem(pathwayLarge{ii}.correct);
    ax(ii) = errorbar([1,2],[stim1.smallDCPerf stim1.largeDCPerf],...
        [stim1.smallSEM stim1.largeSEM],'-o');
    text(1.05,stim1.smallDCPerf,sprintf('%.1f',pathwaySmall{ii}.stimulusSize));
    text(2.05,stim1.largeDCPerf,sprintf('%.1f',pathwayLarge{ii}.stimulusSize));
    [h,p(ii)] = ttest2(pathwaySmall{ii}.correct, pathwayLarge{ii}.correct);
    pWilc = signrank(pathwaySmall{ii}.correct, pathwayLarge{ii}.correct);
%     [pval] = chi2test([pathwaySmall{ii}.correct; pathwayLarge{ii}.correct]);
  %%%%%%  [h , p] = prop_test([ ], [length(pathwaySmall{ii}.correct), pathwayLarge{ii}.correct);
    
%   if h == 1
%         text(.8,stim1.smallDCPerf,'**','FontSize' ,30);
%     end
%     hold on
end
%%%%%%%


axis([.5 2.5 0 1])
line([0 3],[.25 .25],'LineStyle','--')
line([0 3],[.625 .625],'LineStyle','--','Color','r')
legend(ax,string(p))
title('Performance (SEM)');
set(gca,'xtick',[1 2],'xticklabel', ...
    {'Small DC Subj','Large DC Subj'})

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/PerformanceForSmallVsLargeDCSubj.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/PerformanceForSmallVsLargeDCSubj.epsc');

%% Percent of time spent within a ?x?' Region
areaSize = 8;
for ii = 1:subNum
    x = tempDataStruct.subjectThreshUnc(ii).em.allTraces.xALL;
    y = tempDataStruct.subjectThreshUnc(ii).em.allTraces.yALL;
    temp = (x < areaSize & x > -areaSize & y < areaSize & y > -areaSize);
    allTrialsData.percentWithin5x5(ii) = round(sum(temp)/length(temp)*100,2);
    allTrialsData.TimeWithin5x5(ii) = round(sum(temp)/length(temp),2)*500;
end
std(allTrialsData.TimeWithin5x5)
mean(allTrialsData.TimeWithin5x5)

areaSizeX = (allTrialsData.threshold(ii))*2;
areaSizeY = (allTrialsData.threshold(ii))*5*2;
for ii = 1:subNum
    x = tempDataStruct.subjectThreshUnc(ii).em.allTraces.xALL;
    y = tempDataStruct.subjectThreshUnc(ii).em.allTraces.yALL;
    temp = (x < areaSizeX & x > -areaSizeX & y < areaSizeY & y > -areaSizeY);
    allTrialsData.percentWithinThreshTarg(ii) = round(sum(temp)/length(temp)*100,2);
    allTrialsData.TimeWithinThreshTarg(ii) = round(sum(temp)/length(temp),2)*500;
end
std(allTrialsData.percentWithinThreshTarg)
mean(allTrialsData.TimeWithinThreshTarg)
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/PerformanceForSmallVsLargeDCSubj.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/PerformanceForSmallVsLargeDCSubj.epsc');

%% looking at the difference between 175,500,and 1500 ms fixation
% % % % fix1 = load('MATFiles/Fixation_1500.mat');
% % % % fix2 = load('MATFiles/Fixation_500.mat');
% % % % fix2.fix
% figure;
% for ii = 1:subNum
%     
%     hold on
%     plot(-1,threshAreaUncr.task500.all(ii),'o','Color',c(ii,:),...
%         'MarkerFaceColor',c(ii,:));
% %     hold on
% %     plot(0,threshAreaUncr.task175.all(ii),'o','Color',c(ii,:),...
% %         'MarkerFaceColor',c(ii,:));
% %     hold on
% %     fixation.First1500.area(ii) = fix1.fix.areaDrift.(subjectsAll{ii});
% %     plot(3,fix1.fix.areaDrift.(subjectsAll{ii}),'o','Color',c(ii,:),...
% %         'MarkerFaceColor',c(ii,:));
% %      hold on
% %     if ii == 7
% %         fi.First200.area(ii) = 0;
% %         plot(1,0,'Color',c(ii,:));
% %     else
% %         fi.First200.area(ii) = double(fixation.First200.area{ii});
% %         plot(1, cell2mat(fixation.First200.area(ii)),'o','Color',c(ii,:),...
% %         'MarkerFaceColor',c(ii,:));
% %     end
%     fixation.First500.area(ii) = fix2.fix.areaDrift.(subjectsAll{ii});
%     plot(2,fix2.fix.areaDrift.(subjectsAll{ii}),'o','Color',c(ii,:),...
%         'MarkerFaceColor',c(ii,:));
%     if ii == 7
% %         plot([-1 0 2 3],[threshAreaUncr.task500.all(ii)...
% %         threshAreaUncr.task175.all(ii)...
% %         fix2.fix.areaDrift.(subjectsAll{ii})...
% %         fix1.fix.areaDrift.(subjectsAll{ii})],'-','Color',c(ii,:));
%     else
%      plot([-1 2],[threshAreaUncr.task500.all(ii)...
%         fix2.fix.areaDrift.(subjectsAll{ii})],'-','Color',c(ii,:));
%     
% %      plot([-1:3],[threshAreaUncr.task500.all(ii)...
% %         threshAreaUncr.task175.all(ii)...
% %         cell2mat(fixation.First200.area(ii))...
% %         fix2.fix.areaDrift.(subjectsAll{ii})...
% %         fix1.fix.areaDrift.(subjectsAll{ii})],'-','Color',c(ii,:));
%     end
% end
% 
% params.fixation = fixation
% xticks(-1:3)
% xticklabels({'Task 500ms', 'Task 175ms', 'Fixation 175s', 'Fixation 500ms', 'Fixation 1500ms'})
% xlim([-1.5 3.5]);
% 
% [~,p0] = ttest(threshAreaUncr.task500.all, threshAreaUncr.task175.all);
% [~,p1] = ttest(threshAreaUncr.task500.all, fixation.First1500.area);
% [~,p2] = ttest(threshAreaUncr.task500.all, fixation.First500.area);
% [~,p3] = ttest(threshAreaUncr.task500.all, fi.First200.area);
% [~,p4] = ttest(threshAreaUncr.task175.all, fixation.First1500.area);
% [~,p5] = ttest(threshAreaUncr.task175.all, fixation.First500.area);
% [~,p6] = ttest(threshAreaUncr.task175.all, fi.First200.area);


% test2.thresh = [1.37344752697036,1.17460850695512,1.60533837708974,1.41897412509029,1.25792226788274,1.79207933519614,1.62358811336927,1.52129307586018,1.74729494999884,1.67918227677801];
% test2.dc = [11.1233425140381,4.89311933517456,18.5181446075439,15.6098222732544,7.06990194320679,23.4877014160156,10.7504720687866,18.5418090820313,14.4798927307129,14.2437629699707]

%% Histogram and Power DC
% figure;
critFreq = powerAnalysis_JustCriticalFrequency (allTrialsData.dsq, 0);
critFreq = powerAnalysis_JustCriticalFrequency ([5 20], 2, 1);

% xlim([0.5 80])
% xlim([0.5 100])
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/AllSubjCritFreq.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/PowerSimulations/AllSubjCritFreq.epsc');


[A, B] = sort(allTrialsData.dsq);
load('MATFiles/AllTrialDCBoots.mat')
figure;
for ii = 1:subNum
    errhigh(ii) = stats500{ii}.ci(2)-allTrialsData.dsq(ii);
    errlow(ii) =  allTrialsData.dsq(ii)-stats500{ii}.ci(1);
end

C = fixation.First200.dsq(B);
figure;
yyaxis left
h = bar(A);
ylabel('Diffusion Constant')
hold on
er = errorbar(1:(length(subjectsAll)),A,...
    errhigh(B),errlow(B));
er.Color = [0 0 0];
er.LineStyle = 'none';
hold on
yyaxis right
g = bar(1:10, critFreq(B),'r');
% set ( gca, 'ydir', 'reverse' )
hold on
ylabel('Critical Frequency')
xlabel('Subject')
xlim([0 11])
axis square
 saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/DCAllTrialsCritFreq.png');
 saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/DCAllTrialsCritFreq.epsc');

 figure;
 [~,p,b,r] = LinRegression(allTrialsData.dsq,critFreq,0,NaN,1,0);
 hold on
 for ii = 1:10
     plot(allTrialsData.dsq(ii),critFreq(ii),'o','Color',c(SubjectOrderDCIdx == ii,:),...
         'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),'MarkerSize',12);
     hold on
 end
 xlim([0 30])
 axis square
 xlabel('Diffusion Constant (arcmin^2/sec');
 ylabel('Critical Frequency');
 
%% Get Bootstrapped DC Values
% for ii = 1:subNum
%     x = tempDataStruct.subjectThreshUnc(ii).em.allTraces.x;
%     y = tempDataStruct.subjectThreshUnc(ii).em.allTraces.y;
%     
%     st500 = struct('sampling', params.sampling(ii), ...
%         'x', x, ...
%         'y', y);
% 
%     fprintf('Starting DC Boots Analysis for %s', subjectsAll{ii});
%     capable = @(z)(bootCalculateDiffusionCoef(z));  % Process capability
%     tic
%     [stats500{ii}.ci, stats500{ii}.info] = bootci(100,capable,st500);
%     toc
%     
% %     stats500 = bootstrp(2,@bootCalculateDiffusionCoef,st500);
% 
% end
% save('MATFiles/AllTrialDCBoots.mat','stats500')

%% PLOT DSQ VECTOR AND REGRESSION FIT
for ii = 1:subNum
    if params.machine(ii) == 1
        sampRate = 1;
    else
        sampRate = (1000/330);
    end
%     subplot(3,3,1)
    figure('Position', [2000 10 600 600]);
    pathway = subjectThreshUnc(ii).em.allTraces;
    leg(1) = plot(pathway.TimeDsq(1:200/sampRate),pathway.Dsq(1:200/sampRate),'o','Color','k');
    hold on
    x = pathway.TimeDsq(1:200/sampRate); % Defines the domain as [-15,25] with a refinement of 0.25
    m  = pathway.RegLineDsq;  % Specify your slope
    x1 = pathway.TimeDsq(1); % Specify your starting x
    y1 = pathway.Dsq(1);  % Specify your starting y
    y = m*(x - x1) + y1;
    leg(2) = plot(x,y,'Color','r');
    ylabel('dsqVec')
    legend(leg,{'DSQ Vector','Regression Fit'});
    title(sprintf('%s, DC = %.2f',subjectsAll{ii}, allTrialsData.dsq(ii)));
    saveas(gcf,sprintf...
        ('../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/%s_dsqDetails.png',subjectsAll{ii}));

end

%% Drift Measures Lin Regressions
figure('Position', [2000 10 900 900]);
namesChar = {'dsq','curve','span','speed'};
for cc = 1:length(namesChar)
    subplot(2,3,cc)
    [~,p,b,r] = LinRegression(allTrialsData.(namesChar{cc}),[tempDataStruct.subjectThreshUnc.thresh],0,NaN,1,0);
    hold on
%     for ii = 1:length(subjectsAll)
%         plot(allTrialsData.(namesChar{cc})(ii), tempDataStruct.subjectThreshUnc(ii).thresh, 'o', ...
%             'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),...
%             'MarkerEdgeColor','k',...%c(SubjectOrderDCIdx == ii,:),...
%             'MarkerSize',10);
%     end
    ylim([1 2]);
    ylabel('Acuity Threshold');
    xlabel(namesChar{cc});
    axis square
end
subplot(2,3,5)
for ii = 1:length(subjectsAll)
    p = polarplot(angleRad(ii),biasMeasure(ii),'o');
    p.Color = 'k';%c(SubjectOrderDCIdx == ii,:);
    p.MarkerSize = 15;
    p.MarkerFaceColor = c(SubjectOrderDCIdx == ii,:);
    hold on
end
suptitle('All Trials');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/AllTrialsAgainstThresholdandBias.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/AllTrialsAgainstThresholdandBias.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/AllTrialsAgainstThresholdandBias.epsc');


%%%specifically dc
% figure('Position', [2000 10 900 900]);
namesChar = {'dsq'};
for cc = 1:length(namesChar)
    figure;
    [~,p,b,r] = LinRegression(allTrialsData.(namesChar{cc}),[tempDataStruct.subjectThreshUnc.thresh],0,NaN,1,0);
    hold on
    
    for ii = 1:length(subjectsAll)
       
        [upperY(ii), lowerY(ii)] = confInter95(tempDataStruct.subjectThreshUnc(ii).bootsAll);
    end
    for ii = 1:length(subjectsAll)
        errorbar(allTrialsData.(namesChar{cc})(ii),...
            [tempDataStruct.subjectThreshUnc(ii).thresh],upperY(ii), lowerY(ii),'vertical','o',...
            'Color','k');
        hold on
        errorbar(allTrialsData.(namesChar{cc})(ii),...
            [tempDataStruct.subjectThreshUnc(ii).thresh],errhigh(ii), errlow(ii), 'horizontal','o',...
            'Color','k');
    end
    for ii = 1:length(subjectsAll)
         plot(allTrialsData.(namesChar{cc})(ii), tempDataStruct.subjectThreshUnc(ii).thresh, 'o', ...
            'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),...
            'MarkerEdgeColor','k',...
            'MarkerSize',10);
    end
    ylim([1 2]);
    ylabel('Acuity Threshold');
    xlabel(namesChar{cc});
    axis square
    xlim([0 30])
end
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/AllTrialsAgainstThresholdand.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/AllTrialsAgainstThresholdand.epsc');

%% Fixation Lin Regressions
figure('Position', [2000 10 900 900]);
namesChar = {'dsq','curve','span','speed'};
fixIdx = find(~isnan(fixation.First200.dsq));
for cc = 1:length(namesChar)
    subplot(2,3,cc)
    [~,p,b,r] = LinRegression(fixation.First200.(namesChar{cc})(fixIdx),[tempDataStruct.subjectThreshUnc(fixIdx).thresh],0,NaN,1,0);
    hold on
    for ii = (fixIdx)
        plot(fixation.First200.(namesChar{cc})(ii), tempDataStruct.subjectThreshUnc(ii).thresh, 'o', ...
            'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),...
            'MarkerEdgeColor','k',...
            'MarkerSize',10);
    end
    ylim([1 2]);
    ylabel('Acuity Threshold');
    xlabel(namesChar{cc});
    axis square
end
suptitle('Fixation Trials');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/FixTrialsAgainstThresholdandBias.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/FixTrialsAgainstThresholdandBias.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/FixTrialsAgainstThresholdandBias.epsc');

namesChar = {'dsq'};
for cc = 1:length(namesChar)
    figure;
    [~,p,b,r] = LinRegression(fixation.First200.(namesChar{cc})(fixIdx),[tempDataStruct.subjectThreshUnc(fixIdx).thresh],0,NaN,1,0);
    hold on
    for ii = fixIdx
        [upperY(ii), lowerY(ii)] = confInter95(tempDataStruct.subjectThreshUnc(ii).bootsAll);
        [upperX(ii), lowerX(ii)] = confInter95(fixation.First200.bootDSQ{ii});    
    end
    errorbar(fixation.First200.(namesChar{cc})(fixIdx),...
        [tempDataStruct.subjectThreshUnc(fixIdx).thresh],upperY(fixIdx), lowerY(fixIdx),'vertical','o',...
        'Color','k');
    hold on
    errorbar(fixation.First200.(namesChar{cc})(fixIdx),...
        [tempDataStruct.subjectThreshUnc(fixIdx).thresh],upperX(fixIdx), lowerX(fixIdx), 'horizontal','o',...
        'Color','k');
    for ii = fixIdx
        plot(fixation.First200.(namesChar{cc})(ii), tempDataStruct.subjectThreshUnc(ii).thresh, 'o', ...
            'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),...
            'MarkerEdgeColor','k',...
            'MarkerSize',10);
    end
    ylim([1 2]);
    ylabel('Acuity Threshold');
    xlabel(namesChar{cc});
    axis square
    xlim([0 30])
end
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/FixTrialsAgainstThresholdand.png');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/FixTrialsAgainstThresholdand.epsc');

%% Cruvature and Single ments
figure;
for ii = 1:length(subjectsAll)
    subplot(3,4,ii)
    for i = 1:length(tempDataStruct.subjectThreshUnc(ii).em.allTraces.curvature)
        plot(tempDataStruct.subjectThreshUnc(ii).em.allTraces.SingleSegmentDsq(i,:),'-','Color',c(ii,:));
        hold on
    end
    text(20,20,sprintf('N = %i', length(subjectThreshUnc(ii).em.allTraces.curvature)))
    title(subjectsAll{ii});
end
suptitle('All Trials');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/AllTrialsSingleSegments.png');

[idxN, fixation] =  rebuildFixationData(fix, params);

namesChar = {'dsq','curve','span','speed'};
% namesFChar =  {'dCoefDsq','curve','span','speed'};
% namesFChar = {'dsq','curvature','span','mn_speed','area'};
figure('Position', [2000 10 900 600]);
for numChar = 1:length(namesChar)
    subplot(2,3,numChar)
    for ii = find(idxN)
%         fiX(ii) = fix.(namesFChar{numChar}).(subjectsAll{ii});
        fiX(ii) = fixation.First200.(namesChar{numChar})(ii);
        t1(ii) = allTrialsData.shortAnalysis.(namesChar{numChar})(ii);
        t2(ii) = allTrialsData.(namesChar{numChar})(ii);
        scatter(0, fiX(ii),100, colorSubjects(ii,:), 'filled');
        hold on
        scatter(1, t1(ii) ,100, colorSubjects(ii,:), 'filled');
        scatter(2, t2(ii),100, colorSubjects(ii,:), 'filled');
        
        line([0 1],[fiX(ii) ...
            t1(ii)],'Color',colorSubjects(ii,:));
        line([1 2],[t1(ii) ...
            t2(ii)],'Color',colorSubjects(ii,:));
    end
    names = ({'Fixation','Uncrowded 200', 'Uncrowded 500'});
    set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',15,...
        'FontWeight','bold')
    errorbar(0,mean(fiX), std(fiX),...
        'o','MarkerSize',10,'Color','k','MarkerFaceColor','k')
    errorbar(1,mean(t1), std(t1),...
        'o','MarkerSize',10,'Color','k','MarkerFaceColor','k')
    errorbar(2,mean(t2), std(t2),...
        'o','MarkerSize',10,'Color','k','MarkerFaceColor','k')
    
    names = ({'F175','T175', 'T500'});
    set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',12,...
        'FontWeight','bold')
    ylabel(namesChar{numChar})
    [~,p] = ttest(fiX, t1);
%     [~,p3] = ttest(task.full.dsq(idx), threshDSQUncr(idx));
    
    [~,p2] = ttest(fiX, t2);
    title(sprintf('p = %.3f,p = %.3f,', p, p2));
    %     title(sprintf('p = %.3f', p));
    
    xlim([-0.5 2.5])
end
suptitle('All Trials');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/AllTrialsTTestFicationVsUncAll.png');

%% Eigen Bias!
% Plot the original data and draw the error ellipse (all data points with
% the one ellipse )

figure('Position', [2000 10 900 600]);
for ii = 1:length(subjectsAll)
    subplot(3,4,ii)
    path = tempDataStruct.subjectThreshUnc(ii).em.allTraces;
%     for i = 1:length(path.curvature)
        %         lh =  plot(path.r_ellipse{i}(:,1)+path.original_mu{i}(1),...
        %             path.r_ellipse{i}(:,2)+path.original_mu{i}(2),...
        %             '-','Linewidth',2 );
        lh =  plot(path.r_ellipseRecenteredALL(:,1),...
            path.r_ellipseRecenteredALL(:,2),...
            '-','Linewidth',2 );
        lh.Color = [c(ii,:) 0.5];
        hold on
        
%     end
%     figure(3)
    title(subjectsAll{ii});
end
suptitle('All Trials, One Ellipse Recentered')
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/CircularBiasAnalysisAllTrialsRecentered.png');

figure('Position', [2000 10 900 600]);
for ii = 1:length(subjectsAll)
    subplot(3,4,ii)
    path = tempDataStruct.subjectThreshUnc(ii).em.allTraces;
%     for i = 1:length(path.curvature)
        temp(:,i) = eig(path.VXRecenteredALL);
        ratio(i) = temp(2,i)/temp(1,i);
        hold on
        plot(temp(1,i),temp(2,i),'o','Color',c(ii,:));
        hold on
%     end
    plot(mean(temp(1,:)),mean(temp(2,:)),'d','Color','k');
    axis([0 5 0 50]);
    ratioT(ii) = mean(ratio);
    text(0,40,sprintf('Ratio = %.2f',ratioT(ii)));
    title(subjectsAll{ii});
end
suptitle('Threshold');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/EigenMeansThreshold.png');

%%%%%%

figure('Position', [2000 10 900 600]);
for ii = 1:length(subjectsAll)
    subplot(3,4,ii)
    path = tempDataStruct.subjectThreshUnc(ii).em.ecc_0.(...
        tempDataStruct.thresholdSWVals(ii).thresholdSWUncrowded);
    for i = 1:length(path.curvature)
        %         lh =  plot(path.r_ellipse{i}(:,1)+path.original_mu{i}(1),...
        %             path.r_ellipse{i}(:,2)+path.original_mu{i}(2),...
        %             '-','Linewidth',2 );
        lh =  plot(path.r_ellipse{i}(:,1),...
            path.r_ellipse{i}(:,2),...
            '-','Linewidth',2 );
        lh.Color = [c(ii,:) 0.5];
        hold on
        
    end
%     figure(3)
    title(subjectsAll{ii});
end
suptitle('Threshold')
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/CircularBiasAnalysisThreshold.png');

figure('Position', [2000 10 900 600]);
for ii = 1:length(subjectsAll)
    subplot(3,4,ii)
    path = subjectThreshUnc(ii).em.ecc_0.(thresholdSWVals(ii).thresholdSWUncrowded);
    for i = 1:length(path.curvature)
        temp(:,i) = eig(path.VX{i});
        ratio(i) = temp(2,i)/temp(1,i);
        hold on
        plot(temp(1,i),temp(2,i),'o','Color',c(ii,:));
        hold on
    end
    plot(mean(temp(1,:)),mean(temp(2,:)),'d','Color','k');
    axis([0 5 0 50]);
    ratioT(ii) = mean(ratio);
    text(0,40,sprintf('Ratio = %.2f',ratioT(ii)));
    title(subjectsAll{ii});
end
suptitle('Threshold');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/EigenMeansThreshold.png');

figure('Position', [2000 10 900 600]);
for ii = 1:length(subjectsAll)
    subplot(3,4,ii)
    path = tempDataStruct.subjectThreshUnc(ii).em.allTraces;
    ratio = [];
    for i = 1:length(path.curvature)
        temp(:,i) = eig(path.VX{i});
        ratioALL(ii,i) = temp(2,i)/temp(1,i);
        
%         figure;
%         plot(ratio
        hold on
        plot(temp(1,i),temp(2,i),'o','Color',c(ii,:));
        hold on
    end
    plot(mean(temp(1,:)),mean(temp(2,:)),'d','Color','k');
    axis([0 5 0 50]);
    ratioA(ii) = mean(ratioALL(ii,:));
    text(0,40,sprintf('Ratio = %.2f',ratioA(ii))); 
end
suptitle('All Trials');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/EigenMeansAllTrials.png');

% matrixFill = [1:6 9 10 13 14];
figure('Position', [2000 10 900 600]);
for ii = 1:length(subjectsAll)
    %     subplot(4,4,matrixFill(ii))
    subplot(3,4,ii)
    path = subjectThreshUnc(ii).em.allTraces;
    for i = 1:length(path.curvature)
%        lh =  plot(path.r_ellipse{i}(:,1)+path.original_mu{i}(1),...
%             path.r_ellipse{i}(:,2)+path.original_mu{i}(2),...
%             '-','Linewidth',2 );
        muX(ii,i) = [subjectThreshUnc(ii).em.allTraces.original_mu{i}(1)];
        muY(ii,i) = [subjectThreshUnc(ii).em.allTraces.original_mu{i}(2)];
        lh =  plot(path.r_ellipse{i}(:,1),...
            path.r_ellipse{i}(:,2),...
            '-','Linewidth',2 );
        lh.Color = [c(ii,:) 0.5];
        hold on
        ellipseX(ii,i) = mean([path.r_ellipse{i}(:,1)]);
        ellipseY(ii,i) = mean([path.r_ellipse{i}(:,2)]);
        ellipseMaxX(ii,i) = max([path.r_ellipse{i}(:,1)]);
        ellipseMaxY(ii,i) = max([path.r_ellipse{i}(:,2)]);
        
    end
    meanMU(ii,1) = mean(nonzeros(muX(ii,:)));
    meanMU(ii,2) = mean(nonzeros(muY(ii,:)));
    meanE(ii,1) = mean(nonzeros(ellipseX(ii,:)));
    meanE(ii,2) = mean(nonzeros(ellipseY(ii,:)));
    maxE(ii,1) = max(nonzeros(ellipseX(ii,:)));
    maxE(ii,2) = max(nonzeros(ellipseY(ii,:)));
    title(subjectsAll{ii});
    axis square
    axis([-20 20 -20 20])
end
suptitle('All Trials');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/CircularBiasAllTrials.png');

%%%Histogram of Ratio and Direction of Mu
figure('Position', [2000 10 900 900]);
for ii = 1:length(subjectsAll)
%     figure;
subplot(3,4,ii);
%     plot(ratioALL(ii, ratioALL(ii,:) ~= 0),...
%         atan2d(muX(ii, muX(ii,:) ~= 0), (muY(ii, muY(ii,:) ~= 0))),...
%         'o');
 idx = ratioALL(ii,:) ~= 0 & (ratioALL(ii,:) < 400);
    polarplot(atan2d(muX(ii, idx), (muY(ii, idx))),...
        ratioALL(ii, idx),'o','Color',c(ii,:),'MarkerFaceColo',c(ii,:));
    title(subjectsAll{ii});
end
suptitle('All Trials, Mu Value');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/RatioAndAngleofMu.png');

figure('Position', [2000 10 900 900]);
for ii = 1:length(subjectsAll)
%     figure;
subplot(3,4,ii);
%     plot(ratioALL(ii, ratioALL(ii,:) ~= 0),...
%         atan2d(muX(ii, muX(ii,:) ~= 0), (muY(ii, muY(ii,:) ~= 0))),...
%         'o');
 idx = ratioALL(ii,:) ~= 0 & (ratioALL(ii,:) < 400);
 histogram
    polarplot(atan2d(ellipseMaxX(ii, idx), (ellipseMaxY(ii, idx))),...
        ratioALL(ii, idx),'o','Color',c(ii,:),'MarkerFaceColo',c(ii,:));
    title(subjectsAll{ii});
end
suptitle('All Trials, Max Ellipse Point');
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/RatioAndAngleofMu.png');


% subplot(3,4,[7 8 11 12]);figure
% for ii = 1:length(subjectsAll)
%     plot(meanE(ii,1),meanE(ii,2),...
%         'o','Color',c(ii,:),'MarkerFaceColor',c(ii,:),...
%         'MarkerSize',12,...
%         'MarkerEdgeColor','k');
%     hold on
% end
% axis([-.1 .1 -.1 .1])
% line([5 -5],[0 0],'Color','red','LineStyle','--')
% line([0 0],[-5 5],'Color','red','LineStyle','--')
% axis square
% title('Original Mu Offset');
% xlabel('X');
% ylabel('Y');

%% Old Curvature & Bias Measurement Comparisons

for ii = 1:length(subjectsAll)
    %     subplot(4,4,matrixFill(ii))
    subplot(3,4,ii)
    path = subjectThreshUnc(ii).em.ecc_0.(thresholdSWVals(ii).thresholdSWUncrowded);
    oldCurvature(ii) = mean(cell2mat(path.old_curvature3));
%     oldCurvatureTest(ii) = mean(cell2mat(path.old_curvature2));
    
    path = subjectThreshUnc(ii).em.allTraces;
    oldCurvatureA(ii) = mean((path.old_curvature3));
    %     path = subjectThreshUnc(ii).em.allTraces;
end


%%%Bias Combined Threshold
figure('Position', [2000 10 900 600]);
subplot(2,3,[1 2])
for ii = 1:length(subjectsAll)
    normBiasDSQFunction(ii) = biasMeasureT(ii)/max(biasMeasureT);
    normBiasEigenValue(ii) = ratioT(ii)/max(ratioT);
    normBiasOldCurvature(ii) = oldCurvature(ii)/max(oldCurvature);
    
     plot(0,normBiasDSQFunction(ii),...
        'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
    plot(1,normBiasEigenValue(ii),...
        'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
    plot(2,normBiasOldCurvature(ii),...
        'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
    
    line([0 1],[normBiasDSQFunction(ii) ...
            normBiasEigenValue(ii)],'Color',c(ii,:));
    line([1 2],[normBiasEigenValue(ii) ...
            normBiasOldCurvature(ii)],'Color',c(ii,:));
        hold on
end
names = ({'Bias DSQ Function','Eigen Value Ratio', 'Old Curvature'});
    set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',12,...
        'FontWeight','bold')
axis([-0.5 2.5 -0.2 1.2])
title('Threshold');

subplot(2,3,3); %%%Plots on the unity line
for ii = 1:subNum
    hold on
    plot(normBiasDSQFunction(ii),normBiasEigenValue(ii),'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
end
line([0 1],[0 1]);
xlabel('Bias DSQ Function'); ylabel('Eigen Value Ratio');

figure;
for ii = 1:subNum
    hold on
    plot(normBiasDSQFunction(ii), normBiasOldCurvature(ii),'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
end
ylim([0 0.007])
% line([0 1],[0 1]);
% xlabel('Bias DSQ Function'); ylabel('Old Curvature');
xlabel('Bias DSQ Function'); ylabel('Old Curvature');



%%%Bias Combined All Trials
% figure('Position', [2000 10 900 600]);
subplot(2,3,[4 5])
for ii = 1:length(subjectsAll)
    normBiasDSQFunction(ii) = biasMeasure(ii)/max(biasMeasure);
    normBiasEigenValue(ii) = ratioA(ii)/max(ratioA);
    normBiasOldCurvature(ii) = oldCurvatureA(ii)/max(oldCurvatureA);
    
    plot(0,normBiasDSQFunction(ii),...
        'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
    plot(1,normBiasEigenValue(ii),...
        'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
    plot(2,normBiasOldCurvature(ii),...
        'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
    
    line([0 1],[normBiasDSQFunction(ii) ...
        normBiasEigenValue(ii)],'Color',c(ii,:));
    line([1 2],[normBiasEigenValue(ii) ...
        normBiasOldCurvature(ii)],'Color',c(ii,:));
    hold on
end
names = ({'Bias DSQ Function','Eigen Value Ratio', 'Old Curvature'});
    set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',12,...
        'FontWeight','bold')
axis([-0.5 2.5 -0.2 1.2])
title('All Trials');
suptitle('Bias Comparison');


subplot(2,3,6); %%%Plots on the unity line
for ii = 1:subNum
    hold on
    plot(normBiasDSQFunction(ii),normBiasEigenValue(ii),'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
end
line([0 1],[0 1]);
xlabel('Bias DSQ Function'); ylabel('Eigen Value Ratio');

saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/BiasComparison.png');

% subplot(2,3,6); %%%Plots on the unity line

%% All Trials
%  normBiasDSQFunction(ii) = biasMeasure(ii)/max(biasMeasure);
%     normBiasEigenValue(ii) = ratioA(ii)/max(ratioA);
%     normBiasOldCurvature(ii) = oldCurvatureA(ii)/max(oldCurvatureA);
load('MATFiles/SnellenTask.mat');
reading = load('MATFiles/ReadingTask.mat');
readingData = reading.snellenData;


saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/Bias/BiasComparisonAcrossTasks.png');

%     subplot(2,3,1)
%         plot(biasMeasure(ii),oldCurvatureA(ii),'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
%     subplot(2,3,4)
%         plot(biasMeasure(ii),oldCurvatureA(ii),'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));

%     plot(0,normBiasDSQFunction(ii),...
%         'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
%     plot(1,normBiasEigenValue(ii),...
%         'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
%     plot(2,normBiasOldCurvature(ii),...
%         'o','MarkerSize',10,'Color',c(ii,:),'MarkerFaceColor',c(ii,:));
%     
%     line([0 1],[normBiasDSQFunction(ii) ...
%         normBiasEigenValue(ii)],'Color',c(ii,:));
%     line([1 2],[normBiasEigenValue(ii) ...
%         normBiasOldCurvature(ii)],'Color',c(ii,:));
%     hold on
% end
%% ALL EM THRESHOULD ANALYSIS
[threshDSQUncr, idxBins] = dsqCoefAnalysis(tempDataStruct.subjectUnc, tempDataStruct.subjectThreshUnc, subjectsAll, {'Uncrowded'}, c, fig, tempDataStruct.thresholdSWVals, drift, params);
threshSpanUncr = driftSpanAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, {'Uncrowded'}, c, fig, thresholdSWVals);
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/ThresholdSpan.png');

% threshVelUncr = driftVelocityAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, {'Uncrowded'}, c, fig, thresholdSWVals);
threshAreaUncr = driftAreaAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, {'Uncrowded'}, c, fig, thresholdSWVals, params);

threshCurvUncr = curvatureAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, {'Uncrowded'}, c, fig);
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/ThresholdCurve.png');

threshSpeedUncr = speedAnalysis(subjectUnc, subjectThreshUnc, subjectsAll, {'Uncrowded'}, c, fig);
saveas(gcf,'../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/ComparingMeasures/ThresholdSpeed.png');

graphMultiPsychCurves(subjectsAll, {'Uncrowded'}, {'Unstabilized'}, em, {'0ecc'}, c)

% condition = {'Crowded'};
threshDSQCro = dsqCoefAnalysis(subjectCro, subjectThreshCro, subjectsAll, {'Crowded'}, c, fig, thresholdSWVals, drift, params);
threshSpanCro = driftSpanAnalysis(subjectCro, subjectThreshCro, subjectsAll, {'Crowded'}, c, fig, thresholdSWVals);
% threshVelCro = driftVelocityAnalysis(subjectCro, subjectThreshCro, subjectsAll, {'Crowded'}, c, fig, thresholdSWVals);
threshAreaCro = driftAreaAnalysis(subjectCro, subjectThreshCro, subjectsAll, {'Crowded'}, c, fig, thresholdSWVals, params);
threshCurvCro = curvatureAnalysis(subjectCro, subjectThreshCro, subjectsAll, {'Crowded'}, c, fig);
threshSpeedCro = speedAnalysis(subjectCro, subjectThreshCro, subjectsAll, {'Crowded'}, c, fig);
% graphMultiPsychCurves(subjectsAll, {'Crowded'}, {'Unstabilized'}, em, {'0ecc'}, c)
close all;

%% Reformatting data for a nice file with all data
counter = 1;
for ii = 1:subNum
    dataCrowding.Uncrowded(ii).Threshold = subjectThreshUnc(ii).thresh;
    dataCrowding.Uncrowded(ii).DiffusionConstant = threshDSQUncr(ii);
    dataCrowding.Uncrowded(ii).Span = threshSpanUncr(ii);
    dataCrowding.Uncrowded(ii).Area = threshAreaUncr.task500.all(ii);
    dataCrowding.Uncrowded(ii).Curvature = threshCurvUncr(ii);
    dataCrowding.Uncrowded(ii).Speed = threshSpeedUncr(ii);
    
    allSW = fieldnames(subjectThreshUnc(ii).em.ecc_0);
    
    for swNum = 1:length(allSW)
        currentSW = char(allSW(swNum));
        
        for numTrials = 1:length(subjectThreshUnc(ii).em.ecc_0.(currentSW).mn_speed)
            dataCrowdingTrialLevel(counter).Crowded = 0;
            dataCrowdingTrialLevel(counter).Subject = ii;
            dataCrowdingTrialLevel(counter).Threshold = subjectThreshUnc(ii).thresh;
            dataCrowdingTrialLevel(counter).DiffusionConstant = threshDSQUncr(ii);
            dataCrowdingTrialLevel(counter).Span = threshSpanUncr(ii);
            dataCrowdingTrialLevel(counter).Area = threshAreaUncr.task500.all(ii);
            dataCrowdingTrialLevel(counter).Curvature = threshCurvUncr(ii);
            dataCrowdingTrialLevel(counter).Speed = threshSpeedUncr(ii);
            
            dataCrowdingTrialLevel(counter).Size = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).stimulusSize;
            dataCrowdingTrialLevel(counter).Performance = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).performanceAtSize;
            dataCrowdingTrialLevel(counter).Response = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).response(numTrials);
            dataCrowdingTrialLevel(counter).Answer = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).target(numTrials);
            dataCrowdingTrialLevel(counter).Correct = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).correct(numTrials);
            dataCrowdingTrialLevel(counter).ResponseTime = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).responseTime(numTrials);
            dataCrowdingTrialLevel(counter).Traces = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).position(numTrials);
            dataCrowdingTrialLevel(counter).TrialCurvature = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).curvature{numTrials};
            dataCrowdingTrialLevel(counter).TrialSpeed = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).mn_speed{numTrials};
            dataCrowdingTrialLevel(counter).TrialSpan = ...
                subjectThreshUnc(ii).em.ecc_0.(currentSW).span(numTrials);
            
            [~,~,dataCrowdingTrialLevel(counter).individualDiffusionConstant] = ...
                CalculateDiffusionCoef(params.sampling(ii), ...
                struct('x',subjectThreshUnc(ii).em.ecc_0.(currentSW).position(numTrials).x,...
                'y', subjectThreshUnc(ii).em.ecc_0.(currentSW).position(numTrials).y));
            
            counter = counter + 1;
        end
    end
end

for ii = 1:subNum
    bigii = ii + subNum;
    dataCrowding.Crowded(ii).Threshold = subjectThreshCro(ii).thresh;
    dataCrowding.Crowded(ii).DiffusionConstant = threshDSQCro(ii);
    dataCrowding.Crowded(ii).Span = threshSpanCro(ii);
    dataCrowding.Crowded(ii).Area = threshAreaCro.task500.all(ii);
    dataCrowding.Crowded(ii).Curvature = threshCurvCro(ii);
    dataCrowding.Crowded(ii).Speed = threshSpeedCro(ii);
    allSW = fieldnames(subjectThreshCro(ii).em.ecc_0);

    for swNum = 1:length(allSW)
        currentSW = char(allSW(swNum));
        for numTrials = 1:length(subjectThreshCro(ii).em.ecc_0.(currentSW).mn_speed)
            dataCrowdingTrialLevel(counter).Crowded = 1;
            dataCrowdingTrialLevel(counter).Subject = ii;
            dataCrowdingTrialLevel(counter).Threshold = subjectThreshCro(ii).thresh;
            dataCrowdingTrialLevel(counter).DiffusionConstant = threshDSQCro(ii);
            dataCrowdingTrialLevel(counter).Span = threshSpanCro(ii);
            dataCrowdingTrialLevel(counter).Area = threshAreaCro.task500.all(ii);
            dataCrowdingTrialLevel(counter).Curvature = threshCurvCro(ii);
            dataCrowdingTrialLevel(counter).Speed = threshSpeedCro(ii);
            
            dataCrowdingTrialLevel(counter).Size = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).stimulusSize;
            dataCrowdingTrialLevel(counter).Performance = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).performanceAtSize;
            dataCrowdingTrialLevel(counter).Response = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).response(numTrials);
            dataCrowdingTrialLevel(counter).Answer = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).target(numTrials);
            dataCrowdingTrialLevel(counter).Correct = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).correct(numTrials);
            dataCrowdingTrialLevel(counter).ResponseTime = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).responseTime(numTrials);
            dataCrowdingTrialLevel(counter).Traces = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).position(numTrials);
            dataCrowdingTrialLevel(counter).TrialCurvature = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).curvature{numTrials};
            dataCrowdingTrialLevel(counter).TrialSpeed = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).mn_speed{numTrials};
            dataCrowdingTrialLevel(counter).TrialSpan = ...
                subjectThreshCro(ii).em.ecc_0.(currentSW).span(numTrials);
            
            [~,~,dataCrowdingTrialLevel(counter).individualDiffusionConstant] = ...
                CalculateDiffusionCoef(params.sampling(ii), ...
                struct('x',subjectThreshCro(ii).em.ecc_0.(currentSW).position(numTrials).x,...
                'y', subjectThreshCro(ii).em.ecc_0.(currentSW).position(numTrials).y));
            
            counter = counter + 1;
        end
    end
end

for ii = 1:subNum
    if ii == 7
        continue;
    end
    for numTrials = 1:length(subjectThreshCro(ii).em.ecc_0.(currentSW).mn_speed)
        
        dataCrowding.Fixation(ii).Threshold = NaN;
        dataCrowding.Fixation(ii).DiffusionConstant = fixation.First200.dsq(ii);
        dataCrowding.Fixation(ii).Span = fixation.First200.span(ii);
        dataCrowding.Fixation(ii).Area = fixation.First200.area{ii};
        dataCrowding.Fixation(ii).Curvature = fixation.First200.curve(ii);
        dataCrowding.Fixation(ii).Speed = fixation.First200.speed(ii);
    end
end
% save('data_ClarkCrowding','dataCrowding');
save('data_ClarkCrowding_TrialLevel','dataCrowdingTrialLevel');

% %%% trial level data
% for ii = 1:subNum
%     dataCrowding.Crowded(ii).Threshold = subjectThreshCro(ii).thresh;
%     dataCrowding.Crowded(ii).DiffusionConstant = threshDSQCro(ii);
%     dataCrowding.Crowded(ii).Span = threshSpanCro(ii);
%     dataCrowding.Crowded(ii).Area = threshAreaCro.task500.all(ii);
%     dataCrowding.Crowded(ii).Curvature = threshCurvCro(ii);
%     dataCrowding.Crowded(ii).Speed = threshSpeedCro(ii);
% end

%% Look at the mean PLF for each SW and plot against performance
[distTabUnc, distTabCro] = prldistandperform(params, subjectUnc, subjectThreshUnc,...
    subjectCro, subjectThreshCro, thresholdSWVals, em, c, subjectsAll);close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Looks at the difference in performance for small span and large span
%trials
differentSpanTrialsWithPerformances (subNum,subjectThreshUnc,subjectThreshCro,subjectUnc,subjectCro)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Calculate how far their landing location is from 0 AND their relationship
%to span
% driftLandingLocation(thresholdSWVals, subjectUnc,subjectThreshUnc,...
%     subjectCro, subjectThreshCro,c,subNum,em)


% Calculate Percent of Time Spend on Target
percentOnTarget = calculatingPercentDriftTarget(thresholdSWVals, subNum, subjectUnc, subjectCro, ...
    subjectThreshUnc, subjectThreshCro, subjectsAll, c, fig);

%% Performance, Stimulus Size, and Percent of Time on Target
% analyzingPercentDriftonTarget(percentOnTarget, condition, subNum)
for ii = 1:subnum
    pathway = tempDataStruct.subjectThreshUnc(ii).em;
    idx = pathway.valid;
    stimSizes = pathway.size(idx);
    for i = 1:length(pathway.allTraces.x)
        
    end
end
%%Testing!
% testIfSpanChangesForSample(subjectUnc,subjectThreshUnc,subNum, subjectsAll)

%% Create histogram of Span for the Smallest and Largest SW
spanHistEvaluation(subjectUnc, subjectCro, subNum, 0,...
    subjectThreshUnc, subjectThreshCro); % 0 = measures the DSQ histogram

%% Calculates the ms landing probabilities
calculateProbLandingTable(subjectUnc, subjectMSUnc, subNum, subjectCro, subjectMSCro);

%% Fix Analysis Area
load('MATFiles/FixationTrials500.mat')
for ii = 1:subNum
    threshAreaFix(ii) = fix.area.(subjectsAll{ii});
end

figure
for ii = 1:subNum
    scatter(1,threshAreaUncr.all(ii),100,[c(ii,1) c(ii,2) c(ii,3)],'filled')
    hold on
    scatter(2,threshAreaCro.all(ii),100,[c(ii,1) c(ii,2) c(ii,3)],'filled')
    hold on
    scatter(3,threshAreaFix(ii),100,[c(ii,1) c(ii,2) c(ii,3)],'filled')
end
hold on
e = errorbar(1,threshAreaUncr.mean, threshAreaUncr.std,'-o','MarkerSize',12,...
    'MarkerEdgeColor','k','MarkerFaceColor','k');
e.Color = 'k';
e.CapSize = 15;

hold on
e = errorbar(2,threshAreaCro.mean, threshAreaCro.std,'-o','MarkerSize',12,...
    'MarkerEdgeColor','k','MarkerFaceColor','k');
e.Color = 'k';
e.CapSize = 15;

hold on
e = errorbar(3,mean(threshAreaFix), std(threshAreaFix),'-o','MarkerSize',12,...
    'MarkerEdgeColor','k','MarkerFaceColor','k');
e.Color = 'k';
e.CapSize = 15;

xlim([0 4])
xticks([1 2 3])
xticklabels({'Uncrowded','Crowded','Fixation'})
%
[h,p1] = ttest(threshAreaUncr.all,threshAreaCro.all);
[h,p2] = ttest(threshAreaFix, threshAreaUncr.all);
[h,p3] = ttest(threshAreaFix, threshAreaCro.all);
%% Fixation by Thresholds
subplot(2,3,3)
axis([0 60 0 60])
axis square
hold on
plot(fixation300,dCoefDsq300Thresh(1,:),'o');
plot(fixation300,dCoefDsq300Thresh(2,:),'o');
axis square
ylabel('Condition DC 300')
xlabel('Fixation DC 300')
legend('Uncrowded','Crowded')
line([0,60],[0,60])

% figure;
subplot(2,3,4)
[t,p,b1,r1] = LinRegression(dCoefDsqThresh(1,:),...
    dCoefDsq300Thresh(1,:),0,NaN,1,0);
axis square
axis([0 60 0 60])
xlabel('Uncrowded DC 500')
ylabel('Uncrowded DC 300')

subplot(2,3,5)
[t,p,b1,r1] = LinRegression(dCoefDsqThresh(2,:),...
    dCoefDsq300Thresh(2,:),0,NaN,1,0);
axis square
axis([0 60 0 60])
xlabel('Crowded DC 500')
ylabel('Crowded DC 300')

saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCFixationVSCondition.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCFixationVSCondition.epsc');
%% Fixation vs Task Drift (Time Match)
[ fixation, drift ] = fixationvsuncrowded( params, subjectUnc, subjectThreshUnc, subjectCro, subjectThreshCro, thresholdSWVals );
% [ fixation, drift ] = fixationvsuncrowded( params, subjectUnc, performU, subjectCro, performC, thresholdSWVals );

idx = ~isnan(fixation.First200.dsq);
params.idx = idx;
% 
% filename = 'MATFiles/drift200_Boots_CleanZ024.mat';
% save(filename,'drift')
% filename = 'MATFiles/fixation200_Boots.mat';
% save(filename,'fixation')
% 
% load('MATFiles/drift200_Boots_CleanZ024.mat');
% load('MATFiles/fixation200_Boots.mat');

%% SANITY CHECKS FOR DSQ FUNCTION

dsqSanityChecks( fixation, drift, idx, params )

%% Normalized Histograms

driftParamsNormalizedHist(drift, fixation, params, params.idx)
%%
figure;
plotStyledLinearRegression(drift.First500.dsq, threshDSQUncr,params.c);
axis([0 30 0 30])
ylabel('500ms')
xlabel('200ms')

figure;
plotStyledLinearRegression(drift.First200.dsq, threshDSQUncr,params.c);
axis([0 30 0 30])
ylabel('500ms')
xlabel('200ms')
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/TimeDiffRegression.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/TimeDiffRegression.epsc');

figure;

% plotStyledLinearRegression(drift.First200.dsq, [subjectThreshUnc.thresh],params.c);
[stats.h, stats.p, stats.b, stats.r] = LinRegression(drift.First200.dsq, [subjectThreshUnc.thresh], 0,NaN,1,0);

axis([0 50 1 2.2])
xlabel('DC 200ms')
ylabel('VA')
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/200DCbyThresh.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/200DCbyThresh.epsc');
%% PRL vs DC
figure;
[~,p,~,~] = LinRegression([distTabUnc.meanPRLDistance],...
    [distTabUnc.dc],0,NaN,1,0);
for ii = 1:length([distTabUnc.meanPRLDistance])
    scatter([distTabUnc(ii).meanPRLDistance],[distTabUnc(ii).span],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end
xlabel('PLF Distance');
ylabel('Span');
%  ylim([0 1])
axis([3 11 0 7]);
title('Uncrowded')
text(5,.9,sprintf('p = %.3f',p))
saveas(gcf,'../../Data/AllSubjTogether/UncPRLbySpan.png');

figure;
[~,p,~,~] = LinRegression([distTabCro.meanPRLDistance],...
    [distTabCro.span],0,NaN,1,0);
for ii = 1:length([distTabCro.meanPRLDistance])
    scatter([distTabCro(ii).meanPRLDistance],[distTabCro(ii).span],200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end
xlabel('PLF Distance');
ylabel('Span');
axis([3 11 0 7]);
%  ylim([0 1])
text(5,.9,sprintf('p = %.3f',p))
title('Crowded')

saveas(gcf,'../../Data/AllSubjTogether/CroPRLbySpan.png');
%% DC Same Size Stim Analysis
justThreshSW = 0;
[distAllTabUnc] = ...
    buildDCStruct(justThreshSW, params, tempDataStruct.subjectUnc, tempDataStruct.subjectThreshUnc, tempDataStruct.subjectCro, tempDataStruct.subjectThreshCro, tempDataStruct.thresholdSWVals);

justThreshSW = 1;
[distThreshTabUnc, distThreshTabCro] = ...
    buildDCStruct(justThreshSW, params, tempDataStruct.subjectUnc, tempDataStruct.subjectThreshUnc, tempDataStruct.subjectCro, tempDataStruct.subjectThreshCro, tempDataStruct.thresholdSWVals);
load('bootAndSize.mat');
clear uncr
clear cro
clear corU
clear corC
clear sizeU
clear sizeC

% ootAndSize.dc = bootsDC;
% bootAndSize.size = actualSize;
% save('bootAndSize','bootAndSize');

ii = [];
i = [];
% idxBoot = [6 6 6 6 6 6 6 6 6 6];

for ii = 1:params.subNum
    i = [];
    for i = 1:length(distAllTabUnc)
        sizeTesting = unique(round(distAllTabUnc(i).stimSize,1));
        if distAllTabUnc(i).subject == ii &&...
                ( (sizeTesting) >= 1.2 && (sizeTesting) <= 1.5)
            %                 ( sizeTesting >= .8 && sizeTesting <= 1.2) %LessThan
            %                 ( sizeTesting >= 1.5 && sizeTesting <= 2.0) %GreatThan
            %                 ( sizeTesting >= 1.2 && sizeTesting <= 1.5) %AtThresh
            %
            %
            sizeU(ii) = unique(distAllTabUnc(i).stimSize);
            uncr(ii) = distAllTabUnc(i).dCoefDsq;
            [upperX(ii), lowerX(ii)] = confInter95(bootAndSize.dc{ii,6}');
            corU(ii) = mean(distAllTabUnc(i).correct);
            corUSEM(ii) = sem(distAllTabUnc(i).correct);
        end
    end
    i = [];
%     for i = 1:length(distAllTabCro)
%         sizeTesting = unique(round(distAllTabCro(i).stimSize,1));
%         if isempty(distAllTabCro(i).subject)
%             continue;
%         elseif distAllTabCro(i).subject == ii &&...
%                 ( sizeTesting >= 1.7 && sizeTesting <= 2.0) %AtThresh
%             %                 ( sizeTesting >= 1.2 && sizeTesting <= 1.5) %LessThan
%             %                 ( sizeTesting >= 2.0 && sizeTesting <= 2.3) %GreatThan
%             %                 ( sizeTesting >= 1.7 && sizeTesting <= 2.0) %AtThresh
%             %
%             %
%             sizeC(ii) = unique(distAllTabCro(i).stimSize);
%             cro(ii) = distAllTabCro(i).dCoefDsq;
%             corC(ii) = mean(distAllTabCro(i).correct);
%             corCSEM(ii) = sem(distAllTabCro(i).correct);
%         end
%     end
end
figure;
% load('bootsDC.mat');
% subplot(1,2,1)
[t,p,b1,r1] = LinRegression([uncr],...
    [corU],0,NaN,1,0);
hold on
errorbar([uncr],[corU],corUSEM,'vertical','o','Color','k');
hold on
errorbar([uncr],[corU],errhigh, errlow, 'horizontal','o','Color','k');
for ii = 1:params.subNum
    plot(uncr(ii),...
        corU(ii),'o','Color','k',...
        'MarkerFaceColor',c(SubjectOrderDCIdx == ii,:),...
        'MarkerSize',10);
end
axis([0 25 0.2 1])
axis square
text(5,.4,sprintf('p = %.3f',p))
text(5,.35,sprintf('b = %.3f',b1))
text(5,.3,sprintf('r = %.3f',r1))
title('Uncrowded')
xlabel('DC at 1.5"')
ylabel('Performance');
% xlabel('DC at 1.5-2.0" Larger')
% xlabel('DC at 0.8-1.2" Smaller')
saveas(gcf,...
    '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/DCByPerformanceSmallerTargetSize.png');
saveas(gcf,...
    '../../SummaryDocuments/ManuscriptDocs/Manuscript.VisualAcuity.Clark2020/OverleafDoc/figures/DiffusionConstant/DCByPerformanceSmallerTargetSize.epsc');

subplot(1,2,2);
[t,p,b2,r2] = LinRegression(nonzeros([cro])',...
    nonzeros([corC])',0,NaN,1,0);
for ii = 1:params.subNum
    scatter(nonzeros(cro(ii)),nonzeros(corC(ii)),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end
axis([0 40 .2 1])
axis square
text(5,.4,sprintf('p = %.3f',p))
text(5,.35,sprintf('b = %.3f',b2))
text(5,.3,sprintf('r = %.3f',r2))
title('Crowded')
xlabel('DC at 1.7-2.0"')
% xlabel('DC at 2.0-2.3" Larger')
% xlabel('DC at 1.2-1.5" Smaller')

% ylabel('Performance');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCByPerformanceSameTargetSize.png');
% saveas(gcf,...s
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCByPerformanceSameTargetSize.epsc');

% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCByPerformanceLargerTargetSize.png');
% saveas(gcf,...
%     '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCByPerformanceLargerTargetSize.epsc');

saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCByPerformanceSmallerTargetSize.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCByPerformanceSmallerTargetSize.epsc');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% figure;
% xlabel('PLF Distance');
% ylabel('Performance');
% ylim([0 1])
% title('Uncrowded SW = 4')
% text(5,.9,sprintf('p = %.3f',p))
%
% plotCondition(cro,uncr, params)
% names = {'Uncrowded','Crowded'};
% set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',15,...
%     'FontWeight','bold')
% ylabel('Mean PLF Distance')
% title({'Mean PLF at Same Strokewidth', 'Uncrowded vs Crowded'})
%
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PLFonSameTargetSize.png');
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PLFonSameTargetSize.png');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ii = 1:length(subjectCro)
    swDelta{ii} = subjectThreshCro(ii).thresh - subjectThreshUnc(ii).thresh;
    
    swUnc = thresholdSWVals(ii).thresholdSWUncrowded;
    swCro = thresholdSWVals(ii).thresholdSWCrowded;
    
    thresholdUncrowdedSpan{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;
    thresholdCrowdedSpan{ii} = subjectThreshCro(ii).em.ecc_0.(swCro).meanSpan;
    
    thresholdSWUnc{ii} = subjectThreshUnc(ii).thresh;
    thresholdSWCro{ii} = subjectThreshCro(ii).thresh;
end
figure;
plotDeltaSW = cell2mat(swDelta);
% scatter([(cro-uncr)],plotDeltaSW)
deltaPLF = [(cro-uncr)];
[~,p,~,r2] = LinRegression(deltaPLF,...
    plotDeltaSW,0,NaN,1,0);
hold on
for ii = 1:length(subjectCro)
    scatter(deltaPLF(ii),plotDeltaSW(ii),200,c(ii,:),'filled');
end
xlabel('\Delta DC Same Sizes')
ylabel('Crowding Effect')

saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DeltaDCSameSizeDeltaSW.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DeltaDCSameSizeDeltaSW.epsc');


%%%%%%%%%%%%%%%%%
figure;
sz = 400;
deltaDSQ = threshDSQCro-threshDSQUncr;
[~,p,~,r] = LinRegression(deltaDSQ,plotDeltaSW, 0, NaN,1,0);
for ii = 1:subNum
    scatter(deltaDSQ(ii),plotDeltaSW(ii),sz,[c(ii,1) c(ii,2), c(ii,3)],'filled','d')
end
xlabel('\Delta DC at Threshod')
ylabel('Crowding Effect')
title('Difference of DC by Difference of SW')
format longG
pValue = sprintf('p = %.3f', p);
% text(5, 2, pValue,'Fontsize',25);
axis([-5 25 0 2])
rValue = sprintf('r^2 = %.3f', r);
pValue = sprintf('p = %.3f', p);
text(-0.5, 1.4, pValue,'Fontsize',10);
text(-0.5,1.6,rValue,'Fontsize',10);
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DeltaDCThreshDeltaSW.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DeltaDCThreshDeltaSW.epsc');
%% DC Historgram
x = 1:subNum;
[order, i] = sort(threshDSQUncr);
order(2,:) = fixation300(i)
% y = [threshDSQUncr;fixation300];

figure;
b = bar(order,'FaceColor',[0 .5 .5]);
hold on
bar(sort(fixation300(1,:)),'FaceColor',[0 1 .5]);
% for k = 1:subNum
%     b(k).FaceColor = c(k,:);
% end
% names = {'Uncrowded','Crowded'};
names = {'s1','s2','s3','s4','s5','s6','s7','s8','s9','s10'};
xlim([0.5 10.5])
set(gca,'xtick',[1:10],'xticklabel', names,'FontSize',12)
ylabel('Diffusion Constant')
title('Uncrowded')
axis square
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCHistogramU.png');
saveas(gcf,...
    '../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/DCHistogramU.epsc');
%% DC Thresh Power Analysis

powerAnalysis( params, subjectThreshUnc, subjectThreshCro, threshDSQUncr, fixation )
%% Look at PValues across variables
checkPValues (subjectUnc, subjectCro, subjectsAll, threshDSQUncr, threshDSQCro,...
    threshCurvUncr, threshCurvCro, threshVelUncr, threshVelCro)

%% Rate of MS
findUncrowdedvsCrowdedRates(subjectMSUnc,subjectMSCro, subNum, subjectThreshCro, subjectThreshUnc)

%%
plotPerformanceForSW( subjectsAll, subjectThreshCro, subjectThreshUnc, c, subNum)
plotPerformanceForThreshold( subjectsAll, subjectThreshCro, subjectThreshUnc, c, subNum)
uncrcrowThreshold(c,subjectThreshUnc, subjectThreshCro, subNum)
% %% Graphs THRESHOLD SW by it's Mean Fixation
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% meanFixationbySWThresh(subjectCro, subjectThreshUnc, subjectThreshCro, subNum, em, thresholdSWVals, c)

%
% %% Graphs each SW and it's mean Fixation
%%%NOTNEEDED
%meanFixationbyEachSW(subjectUnc, subjectCro)
%
% %% Creates Uncrowded vs Crowded Graph for Span of Drift(Uses values near threshold)
crowdingCondbyMeanFix(subjectUnc, subjectCro, subjectThreshUnc, subjectThreshCro, subNum)

%% Plots Span by the Difference in SW
plotDeltaSWtoDeltaVar( subjectUnc, subjectCro, ...
    subjectThreshUnc, subjectThreshCro, subNum )





