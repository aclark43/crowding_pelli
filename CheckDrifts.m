function pptrials = checkDrifts(drifts, trialId, pptrials, params, filepath, singleSegs)


%test
figure('position',[200, 100, 1500, 800])
allDriftPlot = subplot(2, 2, 1);
for ii = 1:length(drifts)
    Drifts(ii,:) = drifts{ii};
    plot(drifts{ii})
    hold on
end
% plot(Drifts)
xlim([5, 260])
box off; axis square
xlabel('Time interval [ms]','FontWeight','bold')
ylabel('dsq','FontWeight','bold')
title('All drifts')
allDriftPlot.Position(1) = 0; % repositions the subplot on the x axis

PLOT_DRIFTS = input('Plot individual segments (y/n): ','s');

if PLOT_DRIFTS == 'y'
    yLimit = input('What is the y limit: ');
    for driftIdx = 1:size(Drifts,1)
        indvDriftPlot = subplot(2, 2, 3);
        hold on
        currentTrialId = trialId(driftIdx);
        validDriftSegment = find(~isnan(Drifts(driftIdx,:)) == 1); % finds the last non-nan
        indvCoef = Drifts(driftIdx, validDriftSegment(end));
        plot(Drifts(driftIdx,:)')
        title({sprintf('Trial: %i', currentTrialId); sprintf('Coef: %.2f', indvCoef)})
        xlabel('Time interval [ms]','FontWeight','bold')
        ylabel('dsq','FontWeight','bold')
        axis([5 260 0 yLimit])
        box off
        axis square
        indvDriftPlot.Position(1) = 0;
        
        trialPlot = subplot(2,2,2);
        trialPlot.Position = [0.35, 0.13, 0.6, 0.8];
        hold off % Turn hold off so that the individual traces don't plot on top of one another
        
        % Period of interest start and stop
        if strcmp('D',params.machine)
            poiStart = pptrials{currentTrialId}.TimeTargetON/(1000/330);
            poiEnd = min(pptrials{currentTrialId}.TimeTargetOFF/(1000/330), pptrials{currentTrialId}.ResponseTime/(1000/330));
        else
            poiStart = pptrials{currentTrialId}.TimeTargetON;
            poiEnd = min(pptrials{currentTrialId}.TimeTargetOFF, pptrials{currentTrialId}.ResponseTime);
            
        end
        % Creates a color block (green) which highlights the period of
        % interest
        
%         poi = fill([poiStart, poiStart ...
%             poiEnd, poiEnd], ...
%             [-50, 50, 50, -50], ...
%             'g', 'EdgeColor', 'g', 'LineWidth', 2, 'Clipping', 'On', 'FaceAlpha', 0.25); 
%         xTrace = pptrials{currentTrialId}.x.position + pptrials{currentTrialId}.xoffset * pptrials{currentTrialId}.pxAngle;
%         yTrace = pptrials{currentTrialId}.y.position + pptrials{currentTrialId}.yoffset * pptrials{currentTrialId}.pxAngle;
%         hold on
%         hx = plot(xTrace, 'Color', [0 0 200] / 255, 'HitTest', 'off', 'LineWidth', 2);
%         hy = plot(yTrace, 'Color', [0 180 0] / 255, 'HitTest', 'off', 'LineWidth', 2);
%         axis([poiStart - 400, poiEnd + 400, -35, 35])
%         title('Trial Event Sequence')
%         xlabel('Time','FontWeight','bold')
%         ylabel('arcmin','FontWeight','bold')
%         set(gca, 'FontSize', 12)
%         legend([poi, hx, hy], {'P.O.I','X','Y'},'FontWeight','bold')
%         
        cont = input('Stop (s) or discard the drift (d): ','s');
        if cont == 's'
            save(fullfile(params.pathToData, filepath, 'pptrials.mat'), 'pptrials')
            break
        elseif cont == 'd'
            invalidStartTime = input('What is the bad eye movement start time? ');
            pptrials{currentTrialId}.invalid.start = invalidStartTime;
        else
            continue;
        end
    end
end
close

end
