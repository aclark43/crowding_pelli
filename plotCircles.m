function plotCircles(X, Y, flag)

for j = 1:length(X(2:end))
    if flag == 0
        val = j;
        circle(X(j+1), Y(j+1), 3.5, val);
    else
        val = j;
        circle(X(j+1), Y(j+1), 3.5, val);
    end   
end
end


function circle(x,y,r, i)
%x and y are the coordinates of the center of the circle
%r is the radius of the circle
%0.01 is the angle step, bigger values will draw the circle faster but
%you might notice imperfections (not very smooth)
ang=0:0.01:2*pi; 
xp=r*cos(ang);
yp=r*sin(ang);

if i == 1
    plot(x+xp,y+yp, 'r', 'LineWidth',2.5);
else
    plot(x+xp,y+yp, 'k', 'LineWidth',2.5);
end

hold on
end