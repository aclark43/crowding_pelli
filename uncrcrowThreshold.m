function uncrcrowThreshold(c, subjectThreshUnc, subjectThreshCro, subNum)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


figure;
for ii = 1:length(subjectThreshUnc)
    plot(1,subjectThreshUnc(ii).thresh,'o','Color',c(ii,:), ...
        'MarkerFaceColor', c(ii,:), 'MarkerSize',12)
    hold on
    plot(2,subjectThreshCro(ii).thresh,'o','Color',c(ii,:), ...
        'MarkerFaceColor', c(ii,:), 'MarkerSize',12)
    line([1 2],[subjectThreshUnc(ii).thresh subjectThreshCro(ii).thresh], ...
        'Color',c(ii,:),'LineWidth',2);
end
hold on
errorbar(1 ,mean([subjectThreshUnc.thresh]),sem([subjectThreshUnc.thresh]),...
    'o','Color','k','MarkerFaceColor','k','MarkerSize',15,'Linewidth',2);
errorbar(2 ,mean([subjectThreshCro.thresh]),sem([subjectThreshCro.thresh]),...
    'o','Color','k','MarkerFaceColor','k','MarkerSize',15,'Linewidth',2);

names = {'Uncrowded','Crowded'};
set(gca,'xtick',[1 2],'xticklabel', names,'FontSize',15,...
       'FontWeight','bold')
ylabel('Threshold (arcmin)','FontSize',15,'FontWeight','bold')
axis([0.5 2.5 1 3.5])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 
% for ii = 1:length(subjectThreshCro)
%     thresholdSize2(ii) = subjectThreshCro(ii).thresh;
% end
% 
% hold on;
% x2 = 2+zeros(subNum,1)';
% scatter(x2,thresholdSize2,sz,c,'filled')
% hold on
% scatter(x2(1),mean(thresholdSize2),sz,'k','filled','Linewidth',5);
% 
% xS = [1 2];
% 
% 
% for ii = 1:subNum
%     y1 = [thresholdSize1(1,ii) thresholdSize2(1,ii)];
%     line(xS, y1, 'Color',c(ii,:),'LineWidth',2);
% end
% 
% y1 = [mean(thresholdSize1) mean(thresholdSize2)];
% line(xS, y1, 'Color','k','LineWidth',2);
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% title({'Strokewidth Thresholds','for Crowded and Uncrowded'})%'Crowded')
% 
% 
% axis([.75 2.25 0 3.5])
% axis([.25 2.75 .75 3.5])
% 
% 
% coeffs = polyfit(thresholdSize1, thresholdSize2, 1);
% % Get fitted values
% fittedX = linspace(min(x), max(x2), 200);
% fittedY = polyval(coeffs, fittedX);
% % Plot the fitted line
% hold on;
% % plot(fittedX, fittedY, 'r--', 'LineWidth', 1);
% % [~,p] = ttest2(thresholdSize1, thresholdSize2);
% 
% [~,p,ci] = ttest(thresholdSize1, thresholdSize2);
% pValue = sprintf('p = %.3f', p);
% text(1.25, 3, pValue,'Fontsize',20);
% % mdl = fitlm(thresholdSize1,thresholdSize2);
% % p = mdl.Coefficients.pValue(2);
% 
% %%%%%%%%%%%%%%%%%%
% yMean = [mean(thresholdSize1) mean(thresholdSize2)];
% 
% ySTD = [std(thresholdSize1) std(thresholdSize2)]; %standard deviation
% ySEM = ySTD/(sqrt(subNum)); %Standard Error Of The Mean At Each Value Of �x�
% 
% CI95 = tinv([0.025 0.975], subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
% yCI95 = bsxfun(@times, ySEM, CI95(:)); % Calculate 95% Confidence Intervals Of All Experiments At Each Value Of �x�
% 
% errorbar(1,yMean(1),abs((ci(1)-ci(2))/2),abs((ci(1)-ci(2))/2),...
%     'vertical', '-ok','MarkerSize',4,...
%     'MarkerEdgeColor','black',...
%     'MarkerFaceColor','black',...
%     'CapSize',10,...
%     'LineWidth',2)
% 
% errorbar(2,yMean(2),abs((ci(1)-ci(2))/2),abs((ci(1)-ci(2))/2),...
%     'vertical', '-ok','MarkerSize',4,...
%     'MarkerEdgeColor','black',...
%     'MarkerFaceColor','black',...
%     'CapSize',10,...
%     'LineWidth',2)
% 
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllSubErrorBarj.png');
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllSubErrorBarj.epsc');
% 
% %% %%%%%%%%%%%%%%%%%%%
% figure;
% scatter(zeros(1,length(thresholdSize2)),thresholdSize2 - thresholdSize1,sz,c,'d','filled')
% hold on
% errorbar(0,mean(thresholdSize2 - thresholdSize1),std(thresholdSize2 - thresholdSize1),...
%     'vertical', '-ok','MarkerSize',10,...
%     'MarkerEdgeColor','black',...
%     'MarkerFaceColor','black',...
%     'CapSize',10,...
%     'LineWidth',1)
% 
% hold on;
% scatter(zeros(1,length(thresholdSize2))+2,thresholdSize2 *1.4,sz,c,'d','filled')
% hold on
% errorbar(2,mean(thresholdSize2 * 1.4),std(thresholdSize2 *1.4),...
%     'vertical', '-ok','MarkerSize',10,...
%     'MarkerEdgeColor','black',...
%     'MarkerFaceColor','black',...
%     'CapSize',10,...
%     'LineWidth',1)
% 
% %%
% % figure;
% scatter(ones(1,length(thresholdSize2)),(thresholdSize2 * 1.4) - thresholdSize2,sz,c,'d','filled')
% hold on
% errorbar(1,mean((thresholdSize2 * 1.4) - thresholdSize2),std((thresholdSize2 * 1.4) - thresholdSize2),...
%     'vertical', '-ok','MarkerSize',10,...
%     'MarkerEdgeColor','black',...
%     'MarkerFaceColor','black',...
%     'CapSize',10,...
%     'LineWidth',1)
% 
% names = {'Crowding Effect','E-E Critical Spacing', 'C-C Spacing'};
% set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',15,...
%        'FontWeight','bold')
% % ylabel('Strokewidth Threshold','FontSize',15,'FontWeight','bold')
% ylabel({'Arcmin'})
% xlim([-.5 2.5])
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/ThreCrowdingEffect.png');
% saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/ThreCrowdingEffect.epsc');
% % [h,p,ci,stats] = ttest2(thresholdSize1, thresholdSize2)
% % p = avona1
end

