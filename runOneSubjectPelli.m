function [traces, condition, trialChar, em] = ...
    runOneSubjectPelli(params, filepath, figures, ~, ~, title_str, subject)


if strcmp('D',params.machine)
    [ pptrials ] = reBuildPPtrials (filepath, params);
else
    load(fullfile(filepath, 'pptrials.mat'), 'pptrials');
end

%%%%%%%%% TRIAL PARAMETERS %%%%%%%%%%%%%%%%%%
%Gathering information from each trial from pptrials
for ii = 1:length(pptrials)
    if length(pptrials{ii}.microsaccades.start) > 10
        fprintf('Check trial num %i \n', ii)
    end
end

trialChar = buildTrialCharStruct(pptrials);
%%TODO unique strokewidths using cellfun
% em = buildEmStruct(pptrials);


%% Build pixelangle & nasal into params
for ii = 1:length(pptrials)
    if isfield(pptrials{ii}, 'pixelAngle')
        params.pixelAngle = pptrials{1,ii}.pixelAngle;
        pptrials{1,ii}.pxAngle = pptrials{1,ii}.pixelAngle;
    else
        params.pixelAngle = pptrials{1,ii}.pxAngle;
    end
    %%If Nasal, else if....
    if isfield(pptrials{ii}, 'nasal')
        if pptrials{ii}.nasal == 1
            pptrials{ii}.TargetEccentricity = -(pptrials{ii}.TargetEccentricity);
        end
    end
    
end

flds = fields(trialChar);
for fi = 1:length(flds)
    trialChar.(flds{fi}) = cellfun(@(z) z(:).(flds{fi}), pptrials);
end
for ii = 1:length(pptrials)
    trialChar.TargetSize(ii) = double(pptrials{ii}.TargetStrokewidth)*2*pptrials{ii}.pxAngle;
%      fprintf('Ecc = %i\n\n', pptrials{ii}.TargetEccentricity);
end

%% Checks if there are mixed data in any set
if unique(trialChar.TargetEccentricity) > 1
    disp('WARNING, there is more than one Ecc in this data set');
elseif unique(trialChar.Uncrowded) > 1
    disp('WARNING, there is more than one crowding condition in this data set');
elseif unique(trialChar.Unstab) > 1
    disp('WARNING, there is more than one stabilizing condition in this data set');
end

%% Converts pixel --> arcmin
eccPixelAngle = unique(trialChar.TargetEccentricity);
if length(eccPixelAngle) > 1
    warning('Make sure the ECC is all the same!');
end
getEccArcminNADist = ceil(double(eccPixelAngle)*params.pixelAngle); %picks 1 rounded ecc measure
eccArcMin = getEccArcminNADist(1);
if figures.FIXATION_ANALYSIS
    trialChar.TargetEccentricity(:) = 0;
else
    trialChar.TargetEccentricity(:) = eccArcMin;
end

%have a copy of all the data collected
trialChar.Subject = char(subject);

%% TRIAL COUNTS %%%%%%%%%%%
%sorting all inavlid trials from pptrials
%counting the number of each type of invlaid trial
em = [];
[traces, counter, valid, em] = countingTrials(pptrials, em, params, figures);

if figures.CHECK_TRACES
    if params.MS
        msTRIALS = find(valid.ms > 0 & trialChar.TimeFixationON == 0);
        [singleSegs,x,y] = getDriftSegmentInfo (msTRIALS, pptrials, params);
        pptrials = checkMSTraces(msTRIALS, pptrials, params, filepath, singleSegs);
%         save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    elseif params.D
        driftTRIALS = find(valid.d > 0 & trialChar.TimeFixationON == 0);
        [singleSegs,x,y] = getDriftSegmentInfo (driftTRIALS, pptrials, params);
        pptrials = checkMSTraces(driftTRIALS, pptrials, params, filepath, singleSegs);
%         save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    elseif params.DMS
        driftTRIALS = find(valid.dms > 0 & trialChar.TimeFixationON == 0);
        [singleSegs,x,y] = getDriftSegmentInfo (driftTRIALS, pptrials, params);
        pptrials = checkMSTraces(driftTRIALS, pptrials, params, filepath, singleSegs);
%         pptrials = checkMSTraces(pptrials);
        fprintf('Saving pptrials\n')
%         save(fullfile(filepath, 'pptrials.mat'), 'pptrials')
    end
end

%% HANDY FUNCTIONS
% determineWeirdDSQ(valid,trialChar,driftTrials,pptrials,params)

traces.swIdx = trialChar.TargetStrokewidth(valid.d);
%% look at saccade and microsaccade rates during target presentation%%
fprintf('\n%s: analyzing eye movements\n', datestr(now));


%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
condition = [NaN];
if ~figures.QUICK_ANALYSIS
    [em] = emAnalysis(em, pptrials, valid, trialChar, title_str, params, eccArcMin, filepath, figures);
end
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
%% Graphing Function %%
if ~figures.FIXATION_ANALYSIS
    condition = Psychometric_Graphing(params, eccArcMin, valid, trialChar, params.crowded, title_str);
    fprintf('Threshold SW(Size) for %s  = %.3f\n', trialChar.Subject,condition.thresh);
end

% if FIGURE_ON
%     FIGURE_ON = input('Plot individual segments (y/n): ','s');
% end

if figures.FIGURE_ON
    figure
    if params.DMS
        MarkerPlotting(params, pptrials, valid.dms, trialChar, title_str, figures.VIDEO)
        
    elseif params.MS
        MarkerPlotting(params, pptrials, valid.ms, trialChar, title_str, figures.VIDEO)
    else
        MarkerPlotting(params, pptrials, valid.d, trialChar, title_str, figures.VIDEO)
    end
end

if params.crowded
    crowded.trialChar = trialChar;
    crowded.valid = valid;
    crowded.em = em;
    crowded.counter = counter;
    crowded.params = params;
    save(sprintf('%s_crowded_summary.mat', trialChar.Subject), 'crowded')
else
    uncrowded.trialChar = trialChar;
    uncrowded.valid = valid;
    uncrowded.em = em;
    uncrowded.counter = counter;
    uncrowded.params = params;
    save(sprintf('%s_uncrowded_summary.mat', trialChar.Subject), 'uncrowded')
end
end

