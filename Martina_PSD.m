function crowding_PSD()
close all
%% stimuli
digits = [3, 5, 6, 9];
% vertical or horizontal fft
vertical = 1;
%% power spectra of uncrowded stimuli
% works best when pixelAngle and widthAngle of the digit result in an even
% number of pixels for the width of the digit
pixelAngle = .25; % arcmin
widthAngle = 2; % arcmin (angular width of digit)
widthPixel = widthAngle / pixelAngle; % pixel - even number please!
bufferSizeAngle = 2*60;% arcmin
bufferSizePixel = bufferSizeAngle / pixelAngle;
sz = 2 * bufferSizePixel;
k = ((-sz/2 + 1):(sz/2)) / sz / (pixelAngle/60);
ca = [-40, 20];
%% power spectra from drift (Brownian motion model)
dc = 30; % diffusion constant (arcmin^2/s)
f = linspace(1, 80, 80); % temporal frequencies (Hz)
kdc = k;%logspace(-1, 2, 100); % spatial frequencies (cpd)
psD = Qfunction(f, kdc, dc / 60^2); % assumes isotropic
Dps1 = mean(psD);
for di = 1:length(digits)
    im = drawDigit(digits(di), widthPixel);
    if vertical == 1
        % buffer image and make it square
        if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
            %im_buffered = padarray(im, [bufferSizePixel, bufferSizePixel],0, 'both');
            im_buffered = padarray(im-mean(im(:,size(im,2)/2)), [bufferSizePixel, bufferSizePixel],0, 'both');
        else
            im_buffered = padarray(im-mean(im(:,(size(im,2)/2)+1)), [bufferSizePixel, bufferSizePixel],0, 'both');
        end
    else
        im_buffered = padarray(im-mean(im(round(size(im,1)/2)-1,:)), [bufferSizePixel, bufferSizePixel],0, 'both');
    end
    [m, n] = size(im_buffered);
    df = floor((m - sz)/2);
    im_buffered = im_buffered(df:(df + sz - 1), :);
    df = floor((n - sz)/2);
    im_buffered = im_buffered(:, df:(df + sz - 1));
%     figure
%     subplot(1,2,1)
%     im_low = bandpass(im_buffered-mean(mean(im_buffered)),[0.9 7],60/pixelAngle);
%     imagesc((im_low(400:600,450:500))); colormap gray;
%     subplot(1,2,2)
%     im_high = bandpass(im_buffered-mean(mean(im_buffered)),[8 20],60/pixelAngle);
%     imagesc((im_high(400:600,450:500))); colormap gray;
if vertical == 1
    if digits(di) == 3 || digits(di) == 5 ||  digits(di) == 9
        spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)+1)));
        %[freq,Am,Ph]=ft_spect(im_buffered(:,(length(im_buffered)/2)+1),(pixelAngle/60))
    else
        spec = fftshift(fft(im_buffered(:,(length(im_buffered)/2)+2)));
        %[freq,Am,Ph]=ft_spect(im_buffered(:,(length(im_buffered)/2)+2),(pixelAngle/60))
    end
else
     spec = fftshift(fft(im_buffered((length(im_buffered)/2)-1,:)));
end
    DpsIm = spec.*Dps1;
    % phase
    X2=spec;%store the FFT results in another array
    %detect noise (very small numbers (eps)) and ignore them
    threshold = (abs(X2))/10000; %tolerance threshold
    X2(abs(spec)<threshold) = 0; %maskout values that are below the threshold
    %*180/pi; %phase information multiply by 180/pi to convert to deg
    Phase_spec = (angle(X2));
    allSpec(di,:) = pow2db(abs(spec).^2);
    allPhase(di,:) = Phase_spec;
    plot_k = [2, 10, 30, 50];
    plot_k_index = interp1(k, 1:length(k), plot_k, 'n-mean(mean(im_buffered))earest');
    figure(10);
    semilogx(k(length(Phase_spec)/2:end), ((unwrap(Phase_spec(length(Phase_spec)/2:end)))), '-');
    hold on
    title('Phase of digits');
    xlim([0.5 60])
    xlabel('horizontal spatial frequency (cpd)');
    ylabel('phase deg');
    legend({'3','5','6', '9'}, 'Location', 'Best')
%
%     subplot(1, 2, 2);
%     imagesc(k, k, pow2db(abs(spec).^2));
%     title(sprintf('PSD of %i', digits(di)));
%     axis([-60 60 -60 60]);% image;
%     axis image
%     xlabel('horizontal spatial frequency (cpd)');
%     ylabel('vertical spatial frequency (cpd)');
%     colormap hot; colorbar;
%     caxis(ca);
%     % plot phases
%     figure(1)
%     hold on
%     semilogx(k, mean((Phase_spec),1))
%     xlim([0 60])
%     title('Phase horizontal')
%
%     figure(2)
%     semilogx(k, mean((Phase_spec),2))
%     hold on
%     xlim([0 60])
%     title('Phase vertical')
%     legend({'3','5','6', '9'})
%
%     figure(3)
%     semilogx(k, pow2db(nanmean(abs(spec), 1).^2));
%     hold on
%     title('average horizontal');
%     xlabel('spatial frequency (cpd)');
%     %ylim([-10, 50]);
%     xlim([1 60])
    figure(4)
    semilogx(k,  pow2db((abs(spec)).^2));
    hold on
    %semilogx(k,pow2db(mean(psD).^2),'r')
    a = ((abs(spec)).^2);
    b = (mean(psD).^2);
    %semilogx(k,pow2db(a'.*b),'g')
    title('average vertical');
    xlabel('spatial frequency (cpd)');
    %ylim([-10, 50]);
    xlim([0.5 60])
    legend({'3','5','6', '9'}, 'Location', 'Best')
end
D1_P = circ_dist2(allPhase(1,:),allPhase(2,:));
D2_P = circ_dist2(allPhase(1,:)-allPhase(3,:));
D3_P = circ_dist2(allPhase(1,:)-allPhase(4,:));
D4_P = circ_dist2(allPhase(2,:)-allPhase(3,:));
D5_P = circ_dist2(allPhase(2,:)-allPhase(4,:));
D6_P = circ_dist2(allPhase(3,:)-allPhase(4,:));
D1_A = allSpec(1,:)-allSpec(2,:);
D2_A = allSpec(1,:)-allSpec(3,:);
D3_A = allSpec(1,:)-allSpec(4,:);
D4_A = allSpec(2,:)-allSpec(3,:);
D5_A = allSpec(2,:)-allSpec(4,:);
D6_A = allSpec(3,:)-allSpec(4,:);
DP_var = var([D1_P;D2_P;D3_P;D4_P;D5_P;D6_P]);
DA_var = var([D1_A;D2_A;D3_A;D4_A;D5_A;D6_A]);
allSpec(allSpec<0)= 0;
M_A = var(allSpec)-mean(allSpec);
M_P = circ_mean(allPhase);
figure(4)
semilogx(k,(DP_var*10)-mean(DP_var*10), 'r', 'LineWidth', 2)
hold on
%semilogx(k,(DA_var/100), 'c', 'LineWidth', 2)
%semilogx(k,M_A/10, 'k', 'LineWidth', 2)
xlim([0.5 60])
title('Phase variance difference')
ylim([-10 20])
%
% figure
% semilogx(DA_var)
% xlim([0 60])
% title('Amplitude variance difference')
%
%
% figure
% semilogx(DA_var.*DP_var)
% xlim([0 60])
% title('Amplitude/phase variance difference')
% %
% % % %% compare across digits
% % % cols = lines(length(digits));
% % % figure(3); clf; % plot across digits
% % % figure(4); clf; % plot horizontal and vertical slices
% % % for di = 1:length(digits)
% % %     spec = allSpec{di}.PS;
% % %     k = allSpec{di}.k;
% % %
% % %     figure(3);
% % %     subplot(2, 2, di);
% % %     imagesc(k, k, pow2db(abs(spec).^2));
% % %     title(sprintf('PSD of %i', digits(di)));
% % %     axis image;
% % %     xlabel('horizontal spatial frequency (cpd)');
% % %     ylabel('vertical spatial frequency (cpd)');
% % %     colormap hot; colorbar;
% % %     caxis(ca);
% % %
% % %     figure(4);
% % %     subplot(1, 2, 1); hold on;
% % %     plot(k(k > 0), pow2db(nanmean(abs(spec(:, k>0)), 1).^2), 'b-',...
% % %         'linewidth', 2, 'Color', cols(di, :));
% % %
% % %     subplot(1, 2, 2); hold on;
% % %     plot(k(k > 0), pow2db(nanmean(abs(spec(k>0, :)), 2).^2), 'r-',...
% % %         'linewidth', 2, 'Color', cols(di, :));
% % %
% % % end
% % %
% % % figure(3); hold off;
% % %
% % % figure(4);
% % % subplot(1, 2, 1);
% % % xlabel('horizontal spatial frequency (cpd)');
% % % ylim([-10, 30]);
% % % axis square;
% % % ylabel('power (db)');
% % % set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
% % % xlim([5, k(end)]);
% % % hold off;
% % % title('uncrowded target');
% % %
% % % subplot(1, 2, 2);
% % % xlabel('vertical spatial frequency (cpd)');
% % % ylim([-10, 30]);
% % % legend(cellfun(@num2str, num2cell(digits), 'UniformOutput', false));
% % % axis square;
% % % ylabel('power (db)');
% % % set(gca, 'XScale', 'log', 'XTick', [1, 10, 30, 50]);
% % % xlim([5, k(end)]);
% % % title('uncrowded target');
% % % hold off;
% % %
% % %
% % %
function digit = drawDigit(whichDigit, pixelwidth)
% pixelwidth is ideally an even number
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
switch whichDigit
    case 3
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth + 1;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth + 1):(2*strokewidth)) = 0;
    case 5
        for ii = [1, 5.5 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit((strokewidth*5 + 1):(10*strokewidth), (strokewidth + 1):(2*strokewidth)) = 0;
        digit(strokewidth:(5*strokewidth), 1:strokewidth) = 0;
    case 6
        for ii = [1, 6:10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit(:, 1:strokewidth) = 0;
    case 9
        for ii = [1:5, 10]
            si = (ii - 1)*strokewidth + 1;
            ei = ii * strokewidth;
            digit(si:ei, :) = 0;
        end
        digit(:, (strokewidth+1):(2*strokewidth)) = 0;
end







