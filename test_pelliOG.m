%% Looking at pelli data - first round
%   This is Ashle's code in looking at the percent of time each number is
%   presented to the subject


for ii = 1:length(oo.trialData)
   data.TargetNum(ii) = str2double(oo.trialData(ii).targets);
    
end

data.PercentNum(1) = length(find(data.TargetNum == 3))/length(oo.trialData);
data.PercentNum(2) = length(find(data.TargetNum == 5))/length(oo.trialData);
data.PercentNum(3) = length(find(data.TargetNum == 6))/length(oo.trialData);
data.PercentNum(4) = length(find(data.TargetNum == 9))/length(oo.trialData);

%   Here, I'm plotting the probability of each number - this was out of 100
%   trials.
figure;
plot([1 2 3 4], data.PercentNum,  '-o');
ylim([0 1])
xlabel('Number')
xticks([1 2 3 4])
xticklabels({'3','5','6','9'})
ylabel('Percent of Time Presented')
title('Number Percent Presentation Time');