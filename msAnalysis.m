function [em,xValues,yValues] = msAnalysis(em, pptrials, uEcc, valid, trialChar, msIdx, sIdx, params, ...
    title_str, ii, sw, figures, uSW, saccades, stimulusSize)

if strcmp('D',params.machine)
    samplingRate = 330;
else
    samplingRate = 1000;
end
%Updates the Amplitudes and Angles for microsaccades and saccades
pptrials = updatePPTAmplitudeAngle(pptrials);

% Removes the overshoots for microsaccades
% pptrials = osRemoverTrialDDPIFriendly(pptrials, 'microsaccades', samplingRate);
% pptrials = osRemoverTrialDDPIFriendly(pptrials, 'saccades', samplingRate);


% Run through twice so new overshoots are account for
% Updates the Amplitudes and Angles for microsaccades and saccades
% pptrials = updatePPTAmplitudeAngle(pptrials);

if figures
    strokeWidth = ('strokeWidth_ALL');
    eccentricity = sprintf('ecc_%i', abs(uEcc));
    em.(eccentricity).(strokeWidth).time = 5000/(1000/330);
%     eccentricity = sprintf('ecc_%i', 0);
else
    strokeWidth = sprintf('strokeWidth_%i', sw);
    eccentricity = sprintf('ecc_%i', abs(uEcc));
end

[sRates, msRates] = buildRates(pptrials);

em.(eccentricity).(strokeWidth).strokewidth = sw;
em.(eccentricity).(strokeWidth).saccadeRate = sRates(ii);
em.(eccentricity).(strokeWidth).microsaccadeRate = msRates(msIdx);
em.(eccentricity).(strokeWidth).saccadeRateFixation = sRates(valid.fixation);
em.(eccentricity).(strokeWidth).microsaccadeRateFixation = msRates(valid.fixation);


for ui = 1:length(uEcc) %crowded / uncrowded and eccentricity want to look at the rates at each condition
    eccTrials = valid.dms & uEcc(ui) == trialChar.TargetEccentricity;
    em.(eccentricity).(strokeWidth).saccadeRateByEcc(ui, eccTrials) = sRates(eccTrials);
    em.(eccentricity).(strokeWidth).microsaccadeRateByEcc(ui, eccTrials) = msRates(eccTrials);
end
counter = 1;

if saccades == 1
    emType = ('microsaccades');
    emIdx = msIdx;
elseif saccades == 2
    emType = ('saccades');
    emIdx = sIdx;
end

%%% sanity check - all MS/Sacc trials have microsaccades
% for emTypeIdx = saccades
fixWithMS = [];
for idx = 1:length(emIdx)
    if ~isempty(pptrials{emIdx(idx)}.microsaccades.start)
        fixWithMS(counter) = emIdx(idx);
        counter = counter+1;
    end
end
counter = 1;

xValues = [];
yValues = [];

for ii = 1:length(fixWithMS)
    [timeOn, timeOff] = timeConvertTracker(params, pptrials, emIdx(ii), figures);
    if timeOff == 0
        continue;
    else
        if length(pptrials{fixWithMS(ii)}.x.position) < timeOff
            continue;
        end
        if strcmp(sw,'ALL')
            em.(eccentricity).(strokeWidth).stimSize(counter) = 10;
        else
        em.(eccentricity).(strokeWidth).stimSize(counter) =  (2*double(sw))*params.pixelAngle;
        end
        em.(eccentricity).(strokeWidth).msAmplitude(counter) = {pptrials{fixWithMS(ii)}.microsaccades.amplitude};
        em.(eccentricity).(strokeWidth).msAngle(counter) = {pptrials{fixWithMS(ii)}.microsaccades.angle};
        em.(eccentricity).(strokeWidth).sAmplitude(counter) = {pptrials{fixWithMS(ii)}.saccades.amplitude};
        em.(eccentricity).(strokeWidth).sAngle(counter) = {pptrials{fixWithMS(ii)}.saccades.angle};
        xposition = ...
            pptrials{fixWithMS(ii)}.x.position(timeOn:timeOff-1) + ...
            pptrials{fixWithMS(ii)}.pixelAngle * pptrials{fixWithMS(ii)}.xoffset;
        yposition = ...
            pptrials{fixWithMS(ii)}.y.position(timeOn:timeOff-1) + ...
            pptrials{fixWithMS(ii)}.pixelAngle * pptrials{ii}.yoffset;
        
        
        em.(eccentricity).(strokeWidth).position(counter).x = xposition;
        xValues = [xValues, em.(eccentricity).(strokeWidth).position(counter).x];
        
        em.(eccentricity).(strokeWidth).position(counter).y = yposition;
        yValues = [yValues, em.(eccentricity).(strokeWidth).position(counter).y];
        
        em.(eccentricity).(strokeWidth).id(counter) = fixWithMS(ii);
        
        
        % ms peak velocity ***must account for overshoot FROM ZOE IN
        % calc_ms_peakvelocity and CalculateDriftVelocity
        counterMS = 1;
        for msIdx = 1:length(pptrials{fixWithMS(ii)}.microsaccades.amplitude)
            msStart = ceil((pptrials{fixWithMS(ii)}.microsaccades.start(msIdx))/(1000/samplingRate));
            msEnd = floor((pptrials{fixWithMS(ii)}.microsaccades.duration(msIdx))/(1000/samplingRate) + msStart);
            if isnan(msStart)
                Speed.avg(counterMS) = NaN;
                Speed.SE(counterMS) = NaN;
                velocityMean(counterMS) = NaN;
                velocityMax(counterMS) = NaN;
                velx(counterMS) = NaN;
                vely(counterMS) = NaN;
                counterMS = counterMS + 1;
                continue;
            end
            
            msXPos = pptrials{fixWithMS(ii)}.x.position(msStart: msEnd) + ...
                pptrials{fixWithMS(ii)}.pixelAngle * pptrials{fixWithMS(ii)}.xoffset;
            
            msYPos = pptrials{fixWithMS(ii)}.y.position(msStart: msEnd) + ...
                pptrials{fixWithMS(ii)}.pixelAngle * pptrials{fixWithMS(ii)}.yoffset;
            
            velx = diff(msXPos);
            vely = diff(msYPos);
            velTemp = sqrt(msXPos.^2 + msYPos.^2) * samplingRate;
            velocityMean(counterMS) = nanmean(velTemp);
            velocityMax(counterMS) = max(velTemp);
            
            %             speedTemp = sqrt(xposition.^2 + yposition.^2) * samplingRate;%*(params.Fs);
            Speed.avg(counterMS) = nanmean(velTemp);
            Speed.SE(counterMS) = nanstd(velTemp)/sqrt(length(velTemp));
            
            counterMS = counterMS + 1;
        end
        em.(eccentricity).(strokeWidth).speed(counter) = {Speed};
        em.(eccentricity).(strokeWidth).velocityMean(counter) = {velocityMean};
        em.(eccentricity).(strokeWidth).velocityMax(counter) = {velocityMax};
        em.(eccentricity).(strokeWidth).velocityXY(counter) = {[velx; vely]};
        
        counter = counter + 1;
        em.(eccentricity).(strokeWidth).time(counter) = mean(timeOff-timeOn);
    end
end
% end


t = round(mean(em.(eccentricity).(strokeWidth).time*(1000/samplingRate)));


n_bins = 20;
figure;
result = histogram2(xValues, yValues, n_bins);
result = result.Values;
result = result./(max(max(result)));
em.(eccentricity).(strokeWidth).heatmap = result;

if params.MS
    figure;
    generateHeatMap(30, uSW, result, xValues, yValues, stimulusSize, title_str, ...
        trialChar, params, emIdx, t, figures, uEcc);
end


close all

%% ms characteristics
if ~figures
    pattern = analyzeTask(emIdx, uEcc, stimulusSize, params, pptrials);
else
    pattern = analyzeTask(emIdx, uEcc, 80, params, pptrials);
end

[startingLocation.(emType), endingLocation.(emType)] = ...
    getLandingProbability( pattern, trialChar );

if ~isfield(em,'ms')
    em.ms.landingPattern.startingMSLocation = startingLocation;
    em.ms.landingPattern.endingMSLocation = endingLocation;
else
    em.ms.landingPattern.startingMSLocation = [em.ms.landingPattern.startingMSLocation startingLocation];
    em.ms.landingPattern.endingMSLocation = [em.ms.landingPattern.endingMSLocation endingLocation];
end
close all
em = determineStartAndEndMSLocations(fixWithMS, eccentricity, pptrials,strokeWidth,params,figures, em);


end

