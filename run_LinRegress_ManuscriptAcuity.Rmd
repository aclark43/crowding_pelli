---
title: "LinearRegressionAnalysis_Manuscript"
author: "Ashley Clark"
date: "10/20/2021"
output: html_document
---
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r library, include = F}
#install.packages('lme4')
#install.packages('languageR')
#install.packages('tidyverse')
#install.packages("MuMIn")
#install.packages("ggplot2")
library(lme4)
library(languageR)
library(tidyverse)
library(lmerTest)
library(MuMIn)
library(ggplot2)
library(R.matlab)
library(tidyverse)
library(magrittr)
library(broom)
```

## Load Dataset and get model parameters

```{r data}
df <- read.table("dataForR_LinRegress.csv", 
                 header = TRUE,
                 sep = ",")# get linear mixed model of lexdec, RT~Frequency
#varying intercept
df$If_Fixation_Centerd <- df$If_Fixation - 0.5


model.matrix( ~ 1 + If_Fixation_Centerd * DC_All, data = df)

summary(lm(Acuity_Pelli~ 1 + If_Fixation_Centerd * DC_All, data = df))


#varying slope and intercept
model.matrix( ~ 1 + If_Fixation_Centerd / DC_All, data = df)

summary(lm(Acuity_Pelli ~ 1 + If_Fixation_Centerd / DC_All, data = df))
summary(lme_2) 

ranef(lme_2)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r cars}
summary(cars)
```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
