clear all
clc
close all
sf = 4; %spatial frequency (cycles/deg)
sf2 = 20;
wDeg = 2;
nPix = 200;
orientation = 90;  %deg (counter-clockwise from horizontal)
orientation2 = 180;
[x,y] = meshgrid(linspace(-wDeg/2,wDeg/2,nPix+1));
ramp = sin(orientation*pi/180)*x-cos(orientation*pi/180)*y;
ramp2 = sin(orientation2*pi/180)*x-cos(orientation2*pi/180)*y;
grating = sin(2*pi*sf*ramp);
grating2 = sin(2*pi*sf2*ramp2);
grating = grating+grating2;
sigma = .25;  %width of Gaussian (1/e half-width)
Gaussian = exp(-(x.^2+y.^2)/sigma^2);
%grating = grating.*Gaussian;
sfRange = nPix/(2*wDeg);
v_freq = linspace(-sfRange,sfRange,length(x));
figure, imagesc(grating); colormap gray
% Calculate the discrete Fourier transform of the image
F=fft2(double(grating));
figure, imagesc(v_freq,v_freq,(abs(fftshift(F)))); colormap gray; title('image in freq domain')
%Determine good padding for Fourier transform
%Create a Gaussian lowpass filter
%H = filter('gaussian', size(grating,1), size(grating,2),100);
% high pass
H = hpfilter('gaussian', size(grating,1), size(grating,2),100);
figure, imagesc(v_freq,v_freq, (real(fftshift(H)))); 
    colormap gray; title('Filter')
LPFS = H.*F;
% convert the result to the spacial domain.
LPF=real(ifft2(LPFS));
% Crop the image to undo padding
LPF=LPF(1:size(grating,1), 1:size(grating,2));
%Display the "lowpass" image
figure, imagesc((LPF)); colormap gray; title('Low pass image')
figure, imagesc(v_freq,v_freq,real(fftshift(LPFS))); colormap gray; 
title('filtered image in freq domain')

%% Looking at Pelli Font

% digit = drawDigit(3, pixelwidth);
bufferSizePixel = round(255/2);
pixelwidth = 4;
digit = ones(pixelwidth*5, pixelwidth);
strokewidth = pixelwidth/2;
for ii = [1, 5.5 10]
    si = (ii - 1)*strokewidth + 1;
    ei = ii * strokewidth + 1;
    digit(si:ei, :) = 0;
end
digit(:, (strokewidth + 1):(2*strokewidth)) = 0;

im = digit;
im_buffered = padarray(im-mean(im(:,size(im,2))), [bufferSizePixel, bufferSizePixel], 1, 'both');

figure;subplot(1,2,1);imagesc(im_buffered)
subplot(1,2,2);imagesc(v_freq,v_freq,real(fftshift(im_buffered))); colormap gray; 

H2 = hpfilter('gaussian', size(im_buffered,1), size(im_buffered,2),100);
subplot(1,2,2);imagesc(real(ifft2(H2.*im_buffered))); colormap gray; 


