function CI95 = ci95(A)

A = A(~isnan(A));

% 95% CI, 1.96 = Z score for 95%
%% INSTEAD use the formula from the ttest - TC (value derived from the ttest based on the deg of freedoms)
CI95 = [mean(A) - 1.96*(std(A)./sqrt(numel(A))), mean(A) + 1.96*(std(A)./sqrt(numel(A)))];