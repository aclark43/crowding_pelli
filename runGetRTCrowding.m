subjectsAll = {'Z023','Ashley','Z005','Z002','Z013',...
   'Z024','Z064','Z046','Z084','Z014',...
   'Z091','Z138','Z181'};

for ii = 1:length(subjectsAll)
    rtCrowded{ii} = load(sprintf('RT_Info_%s_1',subjectsAll{ii}));
    rtUncrowded{ii} = load(sprintf('RT_Info_%s_0',subjectsAll{ii}));
    meanCr(ii) = mean(rtCrowded{ii}.info.RT);
    meanUn(ii) = mean(rtUncrowded{ii}.info.RT);
end

[h,p,ci,stats] = ttest(meanCr,meanUn);
std(meanCr)
std(meanUn)

fprintf('\n %.2f',mean(meanCr));
fprintf('\n %.2f',mean(meanUn));
