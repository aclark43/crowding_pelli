function [ Drifts, xValues, yValues ] = buildDriftStruct(...
    sw, driftIdx, Drifts, params, pptrials, ~, strokeWidth, eccentricity, xValues, yValues)
%Creating drift information! Including span, velocity, etc.
[Drifts, xValues, yValues] = buildXYPositions (pptrials, Drifts, xValues, yValues, ...
    driftIdx, params, eccentricity, strokeWidth);

for ii = 1:length(Drifts.(eccentricity).(strokeWidth).position)
    position = Drifts.(eccentricity).(strokeWidth).position(ii);
    %Single Segment
    if ~params.MS
        if strcmp('D',params.machine)
            posX = position.x;
            posY = position.y;
            [~,~,~,~,~, ...
                Drifts.(eccentricity).(strokeWidth).singleSegmentDsq{ii},~,~] = ...
                CalculateDiffusionCoef(struct...
                ('x',posX, ...
                'y',posY,...
                'Fsampling', 341));
        else
            [~,~,~,~,~, ...
                Drifts.(eccentricity).(strokeWidth).singleSegmentDsq{ii},~,~] = ...
                CalculateDiffusionCoef(struct...
                ('x',position.x, ...
                'y', position.y,...
                'Fsampling', 1000));
        end
%         for i = 1:length(Drifts.(eccentricity).(strokeWidth).singleSegmentDsq{ii})
%             plot(Drifts.(eccentricity).(strokeWidth).singleSegmentDsq{ii})
%             hold on
%         end
    end
    %%
    %Mean Amplitude
    Drifts.(eccentricity).(strokeWidth).meanAmplitude(ii) = ...
        mean(Drifts.(eccentricity).(strokeWidth).amplitude(ii));
    
    %STD Amplitude
    Drifts.(eccentricity).(strokeWidth).stdAmplitude(ii) = ...
        std(Drifts.(eccentricity).(strokeWidth).amplitude)/...
        sqrt(length(Drifts.(eccentricity).(strokeWidth).amplitude));
    
    %VELOCITY (& x & y) and SPEED
%     if params.D
    [~, Drifts.(eccentricity).(strokeWidth).velocity(ii), velX{ii}, velY{ii}] = ...
        CalculateDriftVelocity(position, 1);
%     end
    [~, instSpX, instSpY, mn_speed, driftAngle, curvature, varx, vary] = ...
        getDriftChar(position.x, position.y, 41, 1, 180);
    Drifts.(eccentricity).(strokeWidth).curvature{ii} = curvature;
    Drifts.(eccentricity).(strokeWidth).driftAngle{ii} = driftAngle;
    Drifts.(eccentricity).(strokeWidth).mn_speed{ii} = mn_speed;
    Drifts.(eccentricity).(strokeWidth).varX{ii}= varx;
    Drifts.(eccentricity).(strokeWidth).varY{ii} = vary;
    if params.D
        if strcmp('D',params.machine)
            Drifts.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:round(500/(1000/330)));
            Drifts.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:round(500/(1000/330)));
        else
            Drifts.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:500);
            Drifts.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:500);
        end
    elseif params.DMS
        if strcmp('D',params.machine)
            Drifts.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:round(300/(1000/330)));
            Drifts.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:round(300/(1000/330)));
        else
            Drifts.(eccentricity).(strokeWidth).instSpX{ii} = instSpX(1:300);
            Drifts.(eccentricity).(strokeWidth).instSpY{ii} = instSpY(1:300);
        end
    end
end
%Mean Span
Drifts.(eccentricity).(strokeWidth).meanSpan = ...
    (mean(Drifts.(eccentricity).(strokeWidth).span(~isnan(Drifts.(eccentricity).(strokeWidth).span))));%remove NAN values

%STD of Span
Drifts.(eccentricity).(strokeWidth).stdSpan = ...
    std(Drifts.(eccentricity).(strokeWidth).span)/...
    sqrt(length(Drifts.(eccentricity).(strokeWidth).span));
% end
Drifts.(eccentricity).(strokeWidth).ccDistance = (round(2*sw*1.4))*params.pixelAngle;
Drifts.(eccentricity).(strokeWidth).stimulusSize = double((2*sw)*params.pixelAngle);
%%

allPosX = [];
allPosY = [];
for ii = 1:length(Drifts.(eccentricity).(strokeWidth).position)
    nonNanXPosIdx = ~isnan(Drifts.(eccentricity).(strokeWidth).position(ii).x);
    nonNanXPosIdy = ~isnan(Drifts.(eccentricity).(strokeWidth).position(ii).y);
    positionX = Drifts.(eccentricity).(strokeWidth).position(ii).x(nonNanXPosIdx);
    positionY = Drifts.(eccentricity).(strokeWidth).position(ii).y(nonNanXPosIdy);
    allPosX = [allPosX, positionX];
    allPosY = [allPosY, positionY];
end

if ~params.MS
    if strcmp('D',params.machine)
        [~,~,Drifts.(eccentricity).(strokeWidth).dCoefDsq,~, ~, ...
            ~,~,~] = ...
            CalculateDiffusionCoef(struct(...
            'x',allPosX,...
            'y', allPosY),...
            'Fsampling', 341);
    else
        [~,~,Drifts.(eccentricity).(strokeWidth).dCoefDsq,~, ~, ...
            ~,~,~] = ...
            CalculateDiffusionCoef(struct(...
            'x',allPosX,...
            'y', allPosY),...
            'Fsampling', 1000);
    end
end
%%
Drifts.(eccentricity).(strokeWidth).meanAmplitude = ...
    mean(Drifts.(eccentricity).(strokeWidth).amplitude);
Drifts.(eccentricity).(strokeWidth).stdAmplitude = ...
    std(Drifts.(eccentricity).(strokeWidth).amplitude)/...
    sqrt(length(Drifts.(eccentricity).(strokeWidth).amplitude));

Drifts.(eccentricity).(strokeWidth).meanSpan = ...
    mean(Drifts.(eccentricity).(strokeWidth).span);
Drifts.(eccentricity).(strokeWidth).stdSpan = ...
    std(Drifts.(eccentricity).(strokeWidth).span)/...
    sqrt(length(Drifts.(eccentricity).(strokeWidth).span));



end

