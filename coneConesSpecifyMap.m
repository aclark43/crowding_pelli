% Z091path = ...
%     ('X:\Adaptive_Optics\APLab_System\Z190_2022_11_11\processedImages\contrastEnhanced\manuallyTagged\R_5_6_V015_bySJ.txt');
% temp = load(Z091path)
% figure;plot(temp(:,1),temp(:,2),'o');
function numCones = coneConesSpecifyMap(subject,allSubjectStats,ii,e, eccNum, thresh)

if strcmp(subject,'Z091')
    Subpath = ('X:\Adaptive_Optics\psychophysicsData\MiniScotoma\Z190_R_2023_07_25\ConeTagging\Z190_R_2023_07_25_coneTagImg_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
elseif strcmp(subject,'Sanjana') || strcmp('Z160',subject)
    Subpath = ('X:\Adaptive_Optics\APLab_System\Z160_2023_01_10\processedImages\contrastEnhanced\ManuallyTagged\R_5_5_V008_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
elseif strcmp(subject,'Ashley')
    Subpath = ('X:\Adaptive_Optics\psychophysicsData\MiniScotoma\Z055_R_2023_07_19\ConeTagging\Z055_R_2023_07_19_coneTagImg_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
elseif strcmp(subject,'Z126')
    Subpath = ('X:\Adaptive_Optics\APLab_System\Z126_2022_11_03\processedImages\contrastEnhanced\manuallyTagged\R_5_01_V008_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
elseif strcmp(subject,'A188') 
    Subpath = ('X:\Adaptive_Optics\APLab_System\A188_2022_10_17\processedImages\contrastEnhanced\manuallyTagged\R_5_12_V016_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
elseif strcmp('Z151',subject) || strcmp('Soma',subject)
    Subpath = ('X:\Adaptive_Optics\psychophysicsData\MiniScotoma\Z151_R_2023_08_22\ConeTagging\Z151_R_2023_08_22_coneTagImg_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
elseif strcmp(subject,'Janis') || strcmp('Z024',subject)
    Subpath = ('X:\Adaptive_Optics\APLab_System\Z024_2022_07_28\processedImages\contrastEnhanced\manually_tagged\Z024_R_5_4_V008_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
elseif strcmp(subject,'A144')
    Subpath = ('X:\Adaptive_Optics\psychophysicsData\MiniScotoma\A144_R_2023_08_17\ConeTagging\A144_R_2023_08_17_coneTagImg_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
elseif strcmp(subject,'Krish') || strcmp('Z192',subject)
    Subpath = ('X:\Adaptive_Optics\APLab_System\Z192_2023_06_22\ProcessedImages\Z192_R_2023_06_22_fixMove_002_strip_sum_count_img_uncroppedImg_annotated.mat');
    temp = load(Subpath);
    cones(:,2) = temp.conelocs(:,1); %x locs
    cones(:,1) = temp.conelocs(:,2); %y locs
end

prl = temp.euclideanNCones.CDC20_loc;



% if strcmp(subject,'Z091')
%     Subpath = ...
%         ('X:\Adaptive_Optics\APLab_System\Z190_2022_06_03\ProcessedImages\contrastEnhanced\manually_tagged\R_5_07_V010_dropFr_trimmed1_251_NEW_08032022_AMC.txt');
%     cones = load(Subpath);
%     tempPRL = ...
%         load('X:\Adaptive_Optics\APLab_System\Z190_2022_06_03\PRL\R_5_07_V010_dropFr_trimmed1_251_stimData.mat');
%     prl = tempPRL.PRL;
% elseif strcmp(subject,'Sanjana') %Z160
%     Subpath = ...
%         ('X:\Adaptive_Optics\APLab_System\Z160_2022_04_26\ConeDensity\OriginalFileName\R_5_2_V005_AMC_07052022 .txt');
%     cones = load(Subpath);
%     tempPRL = ...
%         load('X:\Adaptive_Optics\APLab_System\Z160_2022_04_26\ConeDensity\Z160_R_2022_04_26_CD_data.mat');
%     prl = [tempPRL.PRL_X tempPRL.PRL_Y];
% elseif strcmp(subject,'Ashley')
%     Subpath = ...
%         ('X:\Adaptive_Optics\APLab_System\Z055_2022_04_26\processedImages\contrastEnhanced\gammaCorrected\manuallyTagged\R_5_2_V010_trimmed100-300_SJ_09092022.txt');
%     cones = load(Subpath);
%     tempPRL = ...
%         load('X:\Adaptive_Optics\APLab_System\Z055_2022_04_26\PRL\R_5_2_V010_trimmed100-300_stab_stimData.mat');
%     prl = tempPRL.PRL;
% elseif strcmp(subject,'Z126')
%      numCones = NaN;
%     return;
% elseif strcmp(subject,'A188')
%     Subpath = ...
%         ('X:\Adaptive_Optics\APLab_System\A188_2022_10_17\processedImages\contrastEnhanced\manuallyTagged\R_5_12_V016_AMC10242022.txt');
%     cones = load(Subpath);
%     tempPRL = ...
%         load('X:\Adaptive_Optics\APLab_System\A188_2022_10_17\ConeDensity\A188_R_2022_10_17_CD_data.mat');
%     prl = [tempPRL.PRL_X tempPRL.PRL_Y];
% elseif strcmp(subject,'Soma')
%      Subpath = ...
%         ('X:\Adaptive_Optics\APLab_System\Z151_2022_07_29\processedImages\contrastEnhanced\manually_tagged\R_5_4_V021_bySam_AMCCheck.txt');
%     cones = load(Subpath);
%     tempPRL = ...
%         load('X:\Adaptive_Optics\APLab_System\Z151_2022_07_29\ConeDensity\Z151_R_2022_07_29_CD_data.mat');
%     prl = [tempPRL.PRL_X tempPRL.PRL_Y];
% elseif strcmp(subject,'Janis')
%     Subpath = ...
%         ('X:\Adaptive_Optics\APLab_System\Z024_2022_07_28\processedImages\contrastEnhanced\manually_tagged\Z024_R_5_4_V008_1_AMC.txt');
%     cones = load(Subpath);
%     tempPRL = ...
%         load('X:\Adaptive_Optics\APLab_System\Z024_2022_07_28\ConeDensity\Z024_R_2022_07_28_CD_data.mat');
%     prl = [tempPRL.PRL_X tempPRL.PRL_Y];
% elseif strcmp(subject,'A144') 
%     Subpath = ...
%         ('X:\Adaptive_Optics\APLab_System\A144_2022_12_14\processedImages\contrastEnhanced\manuallyTagged\R_5_10_V013_AMC_03032023_checkedbySJ.txt');
%     cones = load(Subpath);
%     tempPRL = ...
%         load('X:\Adaptive_Optics\APLab_System\A144_2022_12_14\ConeDensity\A144_R_2022_12_14_CD_data_PJ.mat');
%     prl = [tempPRL.PRL_X tempPRL.PRL_Y];
% else
%     numCones = NaN;
%     return;
% end
% 'Sanjana','Ashley','Z126','A188','Soma','Janis','A144'
figure;
plot((cones(:,2)-prl(1))/8.53,(cones(:,1)-prl(2))/8.53,'.');
hold on
plot(0,0,'d','MarkerFaceColor','k');

P = [(cones(:,2)-prl(1))*.15,(cones(:,1)-prl(2))*.15];
PQ = [allSubjectStats{ii}.xAllSaved{e}' + eccNum...
    allSubjectStats{ii}.yAllSaved{e}' + eccNum];
[k,~] = dsearchn(P,PQ);
figure;plot(P(:,1),P(:,2),'ko')
% hold on
% plot(PQ(:,1),PQ(:,2),'*g')
hold on
plot(P(k,1),P(k,2),'*r')
unique(k)
tempHistCones = histc(k, unique(k));
numCones = length(find(tempHistCones >= thresh));

end



% figure;plot(v,c,'.');
%
% pacing = 0.4;
% numSamples = ceil(sqrt((x2-x1)^2+(y2-y1)^2) / 0.4)
% x = linspace(x1, x2, numSamples)
% y = linspace(y1, y2, numSamples)
% xy = round([x',y'])
% dxy = abs(diff(xy, 1))
% duplicateRows = [0; sum(dxy, 2) == 0]
% % Now for the answer:
% finalxy = xy(~duplicateRows,:)
% finalx = finalxy(:, 1);
% finaly = finalxy(:, 2);
% % Plot the points
% hold on;
% plot(finalx, finaly, 'y*');
%
% solve(curve1==curve2)
%
% intersect([voronoi(temp(:,2),-temp(:,1))],[x,y]);