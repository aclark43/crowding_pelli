function [com, massX, massY, prl, eucDistance, massXSTD, massYSTD] = centerOfMassCalculation(x, y)

massX = nanmean(x);
massY = nanmean(y);
massXSTD = nanstd(x);
massYSTD = nanstd(y);

com = hypot(massX,massY);% C = sqrt(abs(A).^2 + abs(B).^2)

eucDistance = (sqrt(x.^2 + y.^2));
prl = nanmean(eucDistance);

end