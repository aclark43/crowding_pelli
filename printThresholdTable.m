function printThresholdTable(data, params)
flds = {'Norm', 'Wbl'};

% MAR acuity tables
for fi = 1:length(flds)
    tmp = ['psyfits', flds{fi}];
    fprintf('%s\n\n', flds{fi});
    fprintf('& \\multicolumn{2}{|c|}{Uncrowded} & \\multicolumn{2}{|c|}{Crowded}\\\\\n');
    fprintf('Ecc & Thresh & Boot mn$\\pm$sd & Thresh & Boot mn$\\pm$sd\\\\\n');
    fprintf('\\hline\n');
    
    for ii = 1:length(data.uniqueEccentricities)
        if isfield(data.(tmp).crowded(1), 'thresh')
            tempcr = [data.(tmp).crowded(ii).thresh,...
                data.(tmp).crowded(ii).bootMS(1),...
                data.(tmp).crowded(ii).bootMS(2)];
        else
            tempcr = [nan nan nan];
        end
        fprintf('%i & %1.2f & %1.2f$\\pm$%1.2f & %1.2f & %1.2f$\\pm$%1.2f',...
            round(params.pixelAngle*data.uniqueEccentricities(ii)),...
            data.(tmp).uncrowded(ii).thresh,...
            data.(tmp).uncrowded(ii).bootMS(1),...
            data.(tmp).uncrowded(ii).bootMS(2),...
            tempcr(1), tempcr(2), tempcr(3));
        fprintf('\\\\\n');
    end
    fprintf('\\hline\n');
    fprintf('\n\n\n\n');
end

% logmar acuity tables
for fi = 1:length(flds)
    tmp = ['psyfits', flds{fi}];
    fprintf('%s\n\n', flds{fi});
    fprintf('& \\multicolumn{2}{|c|}{Uncrowded} & \\multicolumn{2}{|c|}{Crowded}\\\\\n');
    fprintf('Ecc & Thresh & Boot mn$\\pm$sd & Thresh & Boot mn$\\pm$sd\\\\\n');
    fprintf('\\hline\n');
    if isfield(data.(tmp).crowded(1), 'thresh')
        tempcr = [data.(tmp).crowded(ii).thresh,...
            data.(tmp).crowded(ii).bootMS(1),...
            data.(tmp).crowded(ii).bootMS(2)];
    else
        tempcr = [nan nan nan];
    end
    for ii = 1:length(data.uniqueEccentricities)
        fprintf('%i & %1.2f & %1.2f$\\pm$%1.2f & %1.2f & %1.2f$\\pm$%1.2f',...
            round(params.pixelAngle*data.uniqueEccentricities(ii)),...
            log10(data.(tmp).uncrowded(ii).thresh),...
            data.(tmp).uncrowded(ii).logBootMS(1),...
            data.(tmp).uncrowded(ii).logBootMS(2),...
            log10(tempcr(1)), tempcr(2), tempcr(3));
        fprintf('\\\\\n');
    end
    fprintf('\\hline\n');
    fprintf('\n\n\n\n');
end
end