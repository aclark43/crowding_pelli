function mSpan = driftSpanAnalysis(subjectCondition,subjectThreshCondition, subjectsAll, condition, c, fig, thresholdSWVals)
%Looks at span calculated in runMultiple Subjects for each individual
%subject at each SW, as well as across Subj for threshold SW.

figure
for ii = 1:length(subjectsAll)
    strokeValue =(subjectThreshCondition(ii).thresh);
    if strcmp('Uncrowded',condition)
        sw = thresholdSWVals(ii).thresholdSWUncrowded;
    elseif strcmp('Crowded',condition)
        sw = thresholdSWVals(ii).thresholdSWCrowded;
    end
    mSpan(ii) = mean(subjectThreshCondition(ii).em.ecc_0.(sw).span);
    errorBar(mSpan(ii),strokeValue)
    hold on
    points(ii) = scatter(mSpan(ii),strokeValue,100,c(ii,:),'filled');
end
xlabel('DSQ(Mean)')
ylabel('Threshold Strokewidth')
title(sprintf('DSQ,All Subjects,%s',condition{1}))
legend([points],subjectsAll)

saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjDSQ%s.png', condition{1}));


thresholds = [subjectThreshCondition.thresh];

figure
[~,p,b,r] = LinRegression(mSpan,thresholds,0,NaN,1,0);
for ii = 1:length(mSpan)
    point(ii) = scatter(mSpan(ii), thresholds(ii),200,[c(ii,1) c(ii,2) c(ii,3)],'filled');
end
% legend([point],subjectsAll)
rValue = sprintf('r^2 = %.3f', r);
bValue = sprintf('b = %.3f', b);
pValue = sprintf('p = %.3f', p);
if strcmp('Uncrowded',condition{1})
    axis([1 5 1 2]);
    yticks([1 1.25 1.5 1.75 2])
    text(5,1.4,pValue)
    text(5,1.3,bValue)
    text(5,1.2,rValue)
elseif strcmp('Crowded',condition{1})
axis([1 7 1 3.5])
    text(5,1.5,pValue)
    text(5,1.3,bValue)
    text(5,1.1,rValue)
end

xlabel('Mean Span at Threshold')
ylabel('Strokewidth Threshold')
graphTitle1 = 'Span by Threshold Strokewidth';
graphTitle2 = (sprintf('%s', cell2mat(condition)));
title({graphTitle1,graphTitle2})
% xlim([0 50])
% ylim([0 5])
axis square

saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjSpanbyStrokewidth%s.png', condition{1}));
saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjSpanbyStrokewidth%s.epsc', condition{1}));

end

