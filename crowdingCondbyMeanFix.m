function crowdingCondbyMeanFix(subjectUnc, subjectCro, subjectThreshUnc, subjectThreshCro, subNum)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%%%UNCROWDED!
for ii = 1:subNum
    thresholdSizeSWRound = round(subjectUnc(ii).stimulusSize,1);
    matchingThresholdRound = round(subjectThreshUnc(ii).thresh,1);
    [~,thresholdIdx] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    thresholdUncrowdedSpan{ii} = subjectUnc(ii).meanDriftSpan(thresholdIdx);
    plotSize{ii} = subjectThreshUnc(ii).thresh;
end
figure

stimuliSize = cell2mat(plotSize);
stimuliSpan = cell2mat(thresholdUncrowdedSpan);
sz = 200;
c = jet(subNum);
x = ones(1,subNum);

scatter(x,stimuliSpan,sz,c,'filled')%,'filled')
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[1 2],'xticklabel', names)
ylabel('Mean Span of Drift')
axis([0 3 .5 7])

hold on
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%CROWDED!
for ii = 1:subNum
    thresholdSizeSWRound = round(subjectCro(ii).stimulusSize,1);
    matchingThresholdRound = round(subjectThreshCro(ii).thresh,1);
    [~,thresholdIdx] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    thresholdCrowdedSpan{ii} = subjectCro(ii).meanDriftSpan(thresholdIdx);
    plotSize{ii} = subjectThreshCro(ii).thresh;
end

stimuliSizeC = cell2mat(plotSize);
stimuliSpanC = cell2mat(thresholdCrowdedSpan);
sz = 200;

for spanL = 1:length(stimuliSpanC)
colSub = scatter(2,stimuliSpanC(spanL),sz,...
    [c(spanL,1) c(spanL,2) c(spanL,3)],'filled');
end

xS = [1 2];
for ii = 1:subNum
    y1 = [stimuliSpan(1,ii) stimuliSpanC(1,ii)];
    line(xS, y1, 'Color',c(ii,:));
end


title('Mean span of fixation: Uncrowded vs Crowded')
legend(c,'Subject 1','Subject 2', 'Subject 3')
legend('hide')
end

