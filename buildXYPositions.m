function [Drifts, xValues, yValues] = buildXYPositions (pptrials, Drifts, xValues, yValues, ...
    driftIdx, params, eccentricity, strokeWidth)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

counter = 1;
for ii = driftIdx %for each drift trials in pptrials
    if params.D
        if strcmp('D',params.machine)
            timeOn = round(pptrials{ii}.TimeTargetON/(1000/330));
            timeOff = floor(min(pptrials{ii}.TimeTargetOFF/(1000/330), pptrials{ii}.ResponseTime/(1000/330)));
        else
            timeOn = round(pptrials{ii}.TimeTargetON);
            timeOff = round(min(pptrials{ii}.TimeTargetOFF, pptrials{ii}.ResponseTime));
        end
        Drifts.(eccentricity).(strokeWidth).position(counter).x = pptrials{ii}.x.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset;
        xValues = [xValues, Drifts.(eccentricity).(strokeWidth).position(counter).x];
        
        Drifts.(eccentricity).(strokeWidth).position(counter).y = pptrials{ii}.y.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.yoffset;
        yValues = [yValues, Drifts.(eccentricity).(strokeWidth).position(counter).y];
        
        Drifts.(eccentricity).(strokeWidth).id(counter) = ii;
        Drifts.(eccentricity).(strokeWidth).span(counter) = quantile(...
            sqrt((Drifts.(eccentricity).(strokeWidth).position(counter).x-...
            mean(Drifts.(eccentricity).(strokeWidth).position(counter).x)).^2 + ...
            (Drifts.(eccentricity).(strokeWidth).position(counter).y-...
            mean(Drifts.(eccentricity).(strokeWidth).position(counter).y)).^2), .95);
        
        Drifts.(eccentricity).(strokeWidth).amplitude(counter) = sqrt((pptrials{ii}.x.position(timeOff) - pptrials{ii}.x.position(timeOn))^2 + ...
            (pptrials{ii}.y.position(timeOff) - pptrials{ii}.y.position(timeOn))^2);
        counter = counter + 1;
        
        Drifts.(eccentricity).(strokeWidth).time(counter) = mean(timeOff-timeOn);
        
    elseif params.DMS %%DriftSegments should be ~300ms regardless
        if strcmp('D',params.machine)
            timeOn = round(pptrials{ii}.TimeTargetON/(1000/330));
            timeOff = floor((pptrials{ii}.TimeTargetON+300)/(1000/330));
        else
            timeOn = round(pptrials{ii}.TimeTargetON);
            timeOff = round(min((pptrials{ii}.TimeTargetON+300)));
        end
        
        if length(pptrials{ii}.x.position) < timeOff
            timeOff = length(pptrials{ii}.x.position);
        end
        msDuringBeginning = [];
        %     numberofMS = length(pptrials{ii}.microsaccades.start);
        msInTrial = [];
        if ~isempty(length(pptrials{ii}.microsaccades.start))
            for i = 1:length(pptrials{ii}.microsaccades.start)
                msStartTime = pptrials{ii}.microsaccades.start(i);
                msDurationTime = pptrials{ii}.microsaccades.duration(i);
                if (timeOn <= msStartTime) && ...
                        (timeOff >= msStartTime)
                    msDuringBeginning(i) = 1;
                else
                    msDuringBeginning(i) = 0;
                end
            end
        end
        if isempty(length(pptrials{ii}.microsaccades.start)) || sum(msDuringBeginning) == 0
            Drifts.(eccentricity).(strokeWidth).position(counter).x = ...
                [pptrials{ii}.x.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset];
            xValues = [xValues, Drifts.(eccentricity).(strokeWidth).position(counter).x];
            Drifts.(eccentricity).(strokeWidth).position(counter).y = ...
                [pptrials{ii}.y.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset];
            yValues = [yValues, Drifts.(eccentricity).(strokeWidth).position(counter).y];
        else
            continue;
        end
        
        Drifts.(eccentricity).(strokeWidth).position(counter).x = pptrials{ii}.x.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.xoffset;
        xValues = [xValues, Drifts.(eccentricity).(strokeWidth).position(counter).x];
        
        Drifts.(eccentricity).(strokeWidth).position(counter).y = pptrials{ii}.y.position(timeOn:timeOff) + params.pixelAngle * pptrials{ii}.yoffset;
        yValues = [yValues, Drifts.(eccentricity).(strokeWidth).position(counter).y];
        
        Drifts.(eccentricity).(strokeWidth).id(counter) = ii;
        Drifts.(eccentricity).(strokeWidth).span(counter) = quantile(...
            sqrt((Drifts.(eccentricity).(strokeWidth).position(counter).x-...
            mean(Drifts.(eccentricity).(strokeWidth).position(counter).x)).^2 + ...
            (Drifts.(eccentricity).(strokeWidth).position(counter).y-...
            mean(Drifts.(eccentricity).(strokeWidth).position(counter).y)).^2), .95);
        
        Drifts.(eccentricity).(strokeWidth).amplitude(counter) = sqrt((pptrials{ii}.x.position(timeOff) - pptrials{ii}.x.position(timeOn))^2 + ...
            (pptrials{ii}.y.position(timeOff) - pptrials{ii}.y.position(timeOn))^2);
        counter = counter + 1;
        
        Drifts.(eccentricity).(strokeWidth).time(counter) = mean(timeOff-timeOn);
    end
end
end

