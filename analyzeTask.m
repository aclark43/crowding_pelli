function pattern = analyzeTask(vt, uEcc, sw, params, pptrials)

% vt = pptrials(msIdx);

Coordinates = SetCoordinates(uEcc, sw);
%ReadParams;


 [dt.Discarded_Trials.Blinks, dt.Discarded_Trials.No_Tracks, dt.TotTrialsBeforeSel,...
     dt.Discarded_Trials.EmptyTrial, dt.Discarded_Trials.Offset, dt.Discarded_Trials.Min_Trial_Dur]...
     = deal(0);

pattern = struct(...
               'trial','',...
               'msStartTime','',...
               'startMS','',...
               'endMS','',...
               'startS','',...
               'endS','',...
               'responseTime','');
           
cont = 0;           
for jj = 1:length(vt)
        cont = cont +1;
        [MicroSaccStartTime, microSaccadeEndTime, MicroSaccStartLoc, MicroSaccEndLoc, SaccStartLoc, SaccEndLoc] = ...
            recordingTrialLandingLocations(pptrials{vt(jj)}, Coordinates, params, vt(jj));
        pattern(cont).trial = vt(jj);
        pattern(cont).msStartTime = MicroSaccStartTime;
        pattern(cont).msEndTime = microSaccadeEndTime;
        pattern(cont).startMS = MicroSaccStartLoc;
        pattern(cont).endMS = MicroSaccEndLoc;
        pattern(cont).startS = SaccStartLoc;
        pattern(cont).endS = SaccEndLoc;
        pattern(cont).responseTime = pptrials{vt(jj)}.ResponseTime;                 
end
end