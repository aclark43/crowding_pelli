clear all
close all
clc

figures = struct(...
    'FIGURE_ON', 0,... %Show individual trial figure with traces
    'VIDEO', 0,... %Wills save video of FIGURE_ON figures
    'FIXATION_ANALYSIS',1,... %Plots psychometric curves
    'QUICK_ANALYSIS',0,... %Only plots psychometric curves
    'CHECK_TRACES',0,... %will plot traces
    'HUX_RUN',0,... %will discard trials differently
    'COMPARE_SPANS',0); %1 = small spans, 2 = large spans

%{'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};

machine = {'D'}; %(D = DDPI, A = DPI)
subjectsAll = {'Z024','Z064','Z046','Z084','Z014'};%Z070
% subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z063'};
trialLength = 500;
conditionsAll = {'Uncrowded','Crowded'};
eccAll = {'0ecc'};


session = {'ALL'}; %%ALL

for ii = 1:length(subjectsAll)
    for jj = 1:length(conditionsAll)
        for kk = 1:length(eccAll)
            
            subject = subjectsAll(ii);%'Z002','Z023','Z013','Z005','Z002','Z063'};%Z063
            condition = conditionsAll(jj); %Crowded or Uncrowded
            ecc = eccAll(kk);
            
            em = {'Drift'}; %Drift,MS,Drift_MS
            stabParam = {'Unstabilized'};
            
            params = buildParams(machine, session, figures.COMPARE_SPANS, subject, condition);
            
            for emIdx = 1:length(em)
                currentEm = em{emIdx};
                for condIdx = 1:length(condition)
                    currentCond = condition{condIdx};
                    for subIdx = 1:length(subject)
                        currentSubj = subject{subIdx};
                        for stabIdx = 1:length(stabParam)
                            currentStab = stabParam{stabIdx};
                            for eccIdx = 1:length(ecc)
                                currentEcc = ecc{eccIdx};
                                
                                params = determineParams(params, currentEm, currentCond);
                                params.ecc = currentEcc;
                                
                                filepath = sprintf...
                                    ('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Crowding/Data/%s/%s_%s_%s', ...
                                    currentSubj, currentCond, currentStab, currentEcc);
                                
                                title_str = sprintf('%s_%s_%s_%s_%s',currentSubj, currentCond, ...
                                    currentStab, currentEm, currentEcc);
                                
                                %                                 [traces, threshInfo, trialChar, em] = ...
                                %                                     oneSubjectPelli(params, filepath, figures, stabParam, condition, title_str, currentSubj);
                                
                                if strcmp('D',params.machine)
                                    [ pptrials ] = reBuildPPtrials (filepath, params);
                                else
                                    load(fullfile(filepath, 'pptrials.mat'), 'pptrials');
                                end
                                
                                for xx = 1:length(pptrials)
                                    if isfield(pptrials{xx}, 'pixelAngle')
                                        params.pixelAngle = pptrials{1,xx}.pixelAngle;
                                        pptrials{1,xx}.pxAngle = pptrials{1,xx}.pixelAngle;
                                    else
                                        params.pixelAngle = pptrials{1,xx}.pxAngle;
                                    end
                                end
                                params.fixation = 1;
                                pptrialsUnc = [];
                                pptrialsCro = [];
                                allTrials{ii,jj} = pptrials;
                                clear pptrials;
                            end
                        end
                    end
                end
            end
        end
    end
end

% load(fullfile('MATFiles', sprintf('%s_KeepFixationTrials%s.mat',...
%     string(machine),string(trialLength))));


for i = 1:length(allTrials)
    counter = 1;
    subj = (subjectsAll{i});
    for ii = 1:length(allTrials{i,1}) %%Uncrowded
        pptrials = allTrials{i,1};
        if ceil(pptrials{ii}.TimeTargetON) > 0
            fixationOnlyTrialsIdx.(subj)(1,counter) = ii;
            counter = counter + 1;
        end
    end
    
    counter = 1;
    for ii = 1:length(allTrials{i,2}) %%Crowded
        pptrials = allTrials{i,2};
        if ceil(pptrials{ii}.TimeTargetON) > 0
            fixationOnlyTrialsIdx.(subj)(2,counter) = ii;
            counter = counter + 1;
        end
    end
end
%% MS Analysis
% % for ii = 1:length(subjectsAll)
% %     subject = subjectsAll{ii};
% %     %     allTrialsBothCond = [allTrials{ii,1} allTrials{ii,2}];
% %     if strcmp('D',params.machine)
% %         samplingRate = 330;
% %     else
% %         samplingRate = 1000;
% %     end
% %     numTrials1 = numel(find(fixationOnlyTrialsIdx.(subject)(1,:) > 0));
% %     numTrials2 = numel(find(fixationOnlyTrialsIdx.(subject)(2,:) > 0));
% %
% %     pptrials1 = allTrials{ii,1};
% %     pptrials2 = allTrials{ii,2};
% %     %Updates the Amplitudes and Angles for microsaccades and saccades
% %     pptrials1 = updatePPTAmplitudeAngle(pptrials1);
% %     pptrials2 = updatePPTAmplitudeAngle(pptrials2);
% %     % Removes the overshoots for microsaccades
% % %     pptrials1 = osRemoverTrialDDPIFriendly(pptrials1, 'microsaccades', samplingRate);
% % %     pptrials2 = osRemoverTrialDDPIFriendly(pptrials2, 'microsaccades', samplingRate);
% %
% %     %     alltrialsBothCondFixIdx = [fixationOnlyTrialsIdx.(subject)(1,1:numTrials1) fixationOnlyTrialsIdx.(subject)(2,1:numTrials1)];
% %     [sRates1, msRates1] = buildRates(allTrials{ii,1}(1,fixationOnlyTrialsIdx.(subject)(1,1:numTrials1)));
% %     [sRates2, msRates2] = buildRates(allTrials{ii,2}(1,fixationOnlyTrialsIdx.(subject)(2,1:numTrials2)));
% %     counter = 1;
% %     for i = 1:length(pptrials1)
% %         msAmps(ii,counter) ={pptrials1{i}.microsaccades.amplitude};
% %         msAngles(ii,counter) ={pptrials1{i}.microsaccades.angle};
% %         counter = counter + 1;
% %     end
% %     for i = 1:length(pptrials2)
% %         msAmps(ii,counter) ={pptrials2{i}.microsaccades.amplitude};
% %         msAngles(ii,counter) ={pptrials2{i}.microsaccades.angle};
% %         counter = counter + 1;
% %     end
% %
% %     sRates(ii) = mean([sRates1 sRates2]);
% %     msRates(ii) = mean([msRates1 msRates2]);
% % end
%
%
% for ii = 1:length(subjectsAll)
%     amps = [];
%     angles = [];
%     counter = 1;
%     for msAmpIdx = 1:length(msAmps)
%         for i = 1:length(msAmps{ii,msAmpIdx})
%             amps(counter) = msAmps{ii,msAmpIdx}(i);
%             angles(counter) = msAngles{ii,msAmpIdx}(i);
%             counter = counter + 1;
%         end
%     end
%     correctAmpsIdx = find(amps < 100);
%     amps = amps(correctAmpsIdx);
%     angles = angles(correctAmpsIdx);
%     msAmpsMeans(ii) = nanmean(amps);
%     msAnglesMeans(ii) = nanmean(angles);
%     anglesAll{ii} = angles;
%     figure;
%     c = linspace(0, 2*pi, 30);
%     n1 = hist(angles, c); % histogram of saccade directions
%     n1 = n1/ sum(n1); % normalize for empirical probability distribution
%     % subplot(1,3,2);
%     h1p = polar([c, c(1)], [n1, n1(1)]);  % repeat first point so that it goes all the way around
%     % title({'MS Direction Uncrowded','(# of MS per Direction)'})
%     % set(gcf, 'Position',  [600, 300, 1200, 700])
%     % subplot(1,2,2);
%     title(sprintf('Fixation Subj %i, %i Number of MS',ii,length(angles)));
% end
% counter = 1;
% for ii = 1:length(subjectsAll)
% %     amps = [];
% %     angles = [];
% %     counter = 1;
%     for msAmpIdx = 1:length(msAmps)
%         for i = 1:length(msAmps{ii,msAmpIdx})
%             amps(counter) = msAmps{ii,msAmpIdx}(i);
%             angles(counter) = msAngles{ii,msAmpIdx}(i);
%             counter = counter + 1;
%         end
%     end
%     correctAmpsIdx = find(amps < 100);
% %     amps = amps(correctAmpsIdx);
% %     angles = angles(correctAmpsIdx);
% %     msAmpsMeans(ii) = nanmean(amps);
% %     msAnglesMeans(ii) = nanmean(angles);
% %     anglesAll{ii} = angles;
% %     figure;
% %     c = linspace(0, 2*pi, 30);
% %     n1 = hist(angles, c); % histogram of saccade directions
% %     n1 = n1/ sum(n1); % normalize for empirical probability distribution
% %     h1p = polar([c, c(1)], [n1, n1(1)]);  % repeat first point so that it goes all the way around
% %     title(sprintf('Fixation Subj %i, %i Number of MS',ii,length(angles)));
% end


%% Drift Analysis
% fixationTrials = pptrials{fixationOnlyTrialsIdx};
% traces = [];


for i = 1:length(fieldnames(fixationOnlyTrialsIdx))
    subj = (subjectsAll{i});
    counter = 1;
    for cond = 1 %2
        if cond == 1
            condition = ('Uncrowded');
            row = 1;
        else
            condition = ('Crowded');
            row = 2;
        end
        for ii = fixationOnlyTrialsIdx.(subj)(row,:) %%Condition
            pptrials = allTrials{i,row};
            %%
            if ~ii == 0
                if strcmp('D',params.machine)
                    conversionFactor = 1000/330;
                    pxAngle = pptrials{ii}.pixelAngle;
                else
                    conversionFactor = 1;
                    pxAngle = pptrials{ii}.pxAngle;
                end
                timeOn = ceil(pptrials{ii}.TimeTargetON/conversionFactor);
                timeOff = timeOn + trialLength/conversionFactor;
                x = pptrials{ii}.x.position((ceil(timeOn/conversionFactor)):timeOff) + pxAngle * pptrials{ii}.xoffset;
                y = pptrials{ii}.y.position((ceil(timeOn/conversionFactor)):timeOff) + pxAngle * pptrials{ii}.yoffset;
                
                %%
                span = quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95);
                if mean(sqrt(x.^2 + y.^2)) > 30 %%%Gaze Off Center/Span too large
                    continue;
                end
                %Checks for LARGE OFFSET
                if sum(abs(x) > 30) > 0 || sum(abs(y) > 30) > 0
                    continue;
                end
                % Checks for BLINKS
                if find(pptrials{ii}.blinks.start > timeOn & pptrials{ii}.blinks.start < timeOff)%%%BLINKS
                    continue;
                end
                % Checks for NO TRACKS
                if find(pptrials{ii}.notracks.start>timeOn & pptrials{ii}.notracks.start<timeOff)
                    continue;
                end
                if strcmp('D',params.machine)
                    if  ~isempty(find(diff(pptrials{ii}.x.position(ceil(timeOn/(1000/330)):floor(timeOff/(1000/330)))) == 0, 1))
                        continue;
                    end
                end
                % Checks for NO RESPONSE
                if pptrials{ii}.Correct == 3 && pptrials{ii}.TimeFixationON == 0 %%%NO RESPONSE
                    continue;
                end
                % Checks for MANUAL DISCARD
                if  ~isempty(pptrials{ii}.invalid.start)
                    continue;
                end
                % Makes sure there are no fixation trials
                if pptrials{ii}.TimeFixationOFF > 0
                    continue;
                end
                % Checks for Span Limits
                if span < -30 || span > 30 %%%IF Span is too large/too small
                    continue;
                end
                % Saccade Check
                msInTrial = 0;
                sInTrial = 0;
                sacInTrial = 0;
                msPartiallyInTrial = 0;
                if ~isempty(length(pptrials{ii}.saccades.start))
                    for s = 1:length(pptrials{ii}.saccades.start)
                        sStartTime = pptrials{ii}.saccades.start(s);
                        sDurationTime = pptrials{ii}.saccades.duration(s);
                        if (timeOn <= sStartTime) && ...
                                (timeOff > sStartTime + sDurationTime)
                            sInTrial = 1;
                        elseif (timeOn >= sStartTime) && ...
                                (timeOn <  sStartTime + sDurationTime) || ...
                                (timeOff > sStartTime) && ...
                                (timeOff <= sStartTime + sDurationTime)
                            sInTrial = 1;
                        end
                    end
                    if sInTrial
                        continue;
                    end
                end
                % Microsaccade Check
                if ~isempty(length(pptrials{ii}.microsaccades.start))
                    for m = 1:length(pptrials{ii}.microsaccades.start)
                        msStartTime = pptrials{ii}.microsaccades.start(m);
                        msDurationTime = pptrials{ii}.microsaccades.duration(m);
                        if (timeOn <= msStartTime) && ...
                                (timeOff > msStartTime + msDurationTime)
                            msInTrial = 1;
                        elseif (timeOn >= msStartTime) && ...
                                (timeOn <  msStartTime + msDurationTime) || ...
                                (timeOff > msStartTime) && ...
                                (timeOff <= msStartTime + msDurationTime)
                            msPartiallyInTrial = 1;
                        end
                    end
                    if msPartiallyInTrial
                        continue;
                    elseif msInTrial
                        continue;
                    end
                end
                if isempty(pptrials{ii}.drifts)
                    continue;
                end
                %%
                
                traces.(subj).x{counter} = x;
                traces.(subj).y{counter} = y;
                
                counter = counter + 1;
            end
        end
    end
end



%% Plotting Heat Maps
for i = 1:length(fieldnames(fixationOnlyTrialsIdx))
    subj = (subjectsAll{i});
    xValues = [];
    yValues = [];
    xDrift = [];
    yDrift = [];
    counter = 1;
    if isfield(traces.(subj), 'xDrift')
        for ii = 1:length(traces.(subj).xDrift) %%Drift
            x = traces.(subj).xDrift{ii};
            y = traces.(subj).yDrift{ii};
            
            spanTemp= quantile(sqrt((x-mean(x)).^2 + (y-mean(y)).^2), .95);
            if ~isnan(spanTemp)
                span(counter) = spanTemp;
                counter = counter + 1;
            end
            xDrift = [xDrift x];
            yDrift = [yDrift y];
        end
    end
    for ii = 1:length(traces.(subj).x) %%Drift & MS
        xTemp = traces.(subj).x{ii};
        yTemp = traces.(subj).y{ii};
        
        xValues = [xValues xTemp];
        yValues = [yValues yTemp];
    end
    [ xValues, yValues] = checkXYArt(xValues, yValues, min(round(length(xValues)/1000)),31); %Checks strange artifacts
    [ xValues, yValues] = checkXYBound(xValues, yValues, 30); %Checks boundries
    
    %     if isfield(traces.(subj), 'xDrift')
    forDSQCalc.x = traces.(subj).x;
    forDSQCalc.y = traces.(subj).y;
    
    if strcmp('D',params.machine)
        sampling = 341;
    else
        sampling = 1000;
    end
    [~,~,dCoefDsq.(subj), ~, ~, ...
        SingleSegmentDsq.(subj),~,~] = ...
        CalculateDiffusionCoef(sampling,struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
    %     end
    %     meanSpan(i) = mean(span);
    
    figure;
    for s = 1:size(SingleSegmentDsq.(subj))
        if max(SingleSegmentDsq.(subj)(s,:)) > mean(nanmean(SingleSegmentDsq.(subj))) ...
                + 8 * std(nanmean(SingleSegmentDsq.(subj)))
            valid(s) = 0;
        else
            valid(s) = 1;
        end
        plot(SingleSegmentDsq.(subj)(s,:))
        hold on
    end
    
    c = 1;
    for v = 1:size(traces.(subj).x)
        if valid(v)
            forDSQCalcV.x(c) = traces.(subj).x(v);
            forDSQCalcV.y(c) = traces.(subj).y(v);
            c = c + 1;
        end
    end
    [~,~,dCoefDsqV.(subj), ~, ~, ...
        SingleSegmentDsqCorV.(subj),~,~] = ...
        CalculateDiffusionCoef(sampling,struct('x',forDSQCalcV.x, 'y', forDSQCalcV.y));
    figure;
     for s = 1:size(SingleSegmentDsq.(subj))
        plot(SingleSegmentDsqCorV.(subj)(s,:))
        hold on
    end
end
% filename = sprintf('MATFiles/%s_KeepFixationTrials%s.mat',string(machine),string(trialLength));
% save(filename,'saveIdx')


%% CombineSubjects
a300Subj_Fixation = {'Z023','Ashley','Z005','Z002','Z013','Z063'};
a300_Fixation = [22.004753112792970, 9.677511215209961, ...
    16.351053237915040, 11.936565399169922, 6.074677467346191, 12.663557052612305];

d300Subj_Fixaqtion = {'Z024','Z064','Z046','Z084','Z014'};%Z070
d300_Fixation = [26.657310485839844, 6.494433879852295, ...
    16.940389633178710, 14.227915763854980, 28.966794967651367];
