function huxMSAnalysis(subjectsAll, subjectThreshUnc, vfResults, control, huxID)

subNum = length(subjectsAll);
prl.x = [-3.6235213,-1.9011810,1.6922642,9.1925192,0.31330070,-3.4936483,1.2013147,-1.5845748,7.2855325,-2.6934733,-1.3505460,4.2888293,0.63825506,4.4881263,-6.6085296,2.1053123,1.7794517,0.053696334,-0.16159466,6.2817893,-0.12630363,-4.0137539,-12.460599,-2.5793967,-3.1131990,-3.8910515];
prl.y = [3.4364948,2.8098791,2.8107243,-0.28045452,6.5732045,7.8145442,9.4996395,4.3779044,-1.4699855,1.1341612,9.6747885,-2.4013331,-6.1710248,4.3945508,-5.3797216,0.89892393,3.0521350,2.3207297,-4.5593548,8.0076065,-3.8920093,5.6634879,-0.077674948,0.98524725,-1.5957737,4.2907729];
%% Microsaccade Data Task
for ii = 1:subNum
    if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
        msTask{ii}.msAmplitude = NaN;
        msTask{ii}.msAngle = NaN;
        msTask{ii}.msRate = NaN;
        msTask{ii}.msPattern = NaN;
        msTask{ii}.msPosition = NaN;
        continue;
    end
    msAmplitude = [];
    msAngle = [];
    msPosition = [];
    msRate = [];
    msPattern = [];
    msStartX = []; msEndX = []; msStartY = []; msEndY = [];
    path = [];
    names = [];
    counter = 1; trialCounter = 1;
    names = fieldnames(subjectThreshUnc(ii).em.msAnalysis);
    for n = 1:length(names)
        path = subjectThreshUnc(ii).em.msAnalysis.(names{n});
        if isfield(path, 'position')
            for i = 1:length(path.position)
                for j = 1:length(path.MicroSaccStartLoc(i).x)
                    if path.msStartTime{i}(j) < 400 ||...
                        path.msStartTime{i}(j) > 900 
                        continue
                    end
                    msStartX = [msStartX...
                        path.MicroSaccStartLoc(i).x(j)];
                    msStartY = [msStartY...
                        path.MicroSaccStartLoc(i).y(j)];
                    msEndX = [msEndX...
                        path.MicroSaccEndLoc(i).x(j)];
                    msEndY = [msEndY...
                        path.MicroSaccEndLoc(i).y(j)];
                    msRate = [msRate...
                        path.microsaccadeRate(i)];
                    msAmplitude = [msAmplitude...
                        eucDist(...
                        path.MicroSaccStartLoc(i).x(j)-path.MicroSaccEndLoc(i).x(j), ...
                        path.MicroSaccStartLoc(i).y(j)-path.MicroSaccEndLoc(i).y(j))];
                    msAngle = [msAngle...
                        atan2d(...
                        path.MicroSaccStartLoc(i).y(j)-path.MicroSaccEndLoc(i).y(j), ...
                        path.MicroSaccStartLoc(i).x(j)-path.MicroSaccEndLoc(i).x(j))];
                    counter = counter + 1;
                end
%                 if counter > 1
                
%                 msAngle = [msAngle...
%                     path.msAngle];
                msPosition{1,trialCounter} = path.position(i).x{1};
                msPosition{2,trialCounter} = path.position(i).y{1};
               
                trialCounter = trialCounter + 1;
%                 end
            end
         end
       
    end
%     msIdx = find(~isnan(msStartX) & ~isnan(msStartY) &...
%         ~isnan(msEndX) & ~isnan(msEndY))
    msTask{ii}.msAmplitude = msAmplitude;
    msTask{ii}.msAngle = msAngle;
    msTask{ii}.msRate = mean(msRate(msRate < 5));
    msTask{ii}.msPattern = msPattern;
    msTask{ii}.msPosition = msPosition;
    msTask{ii}.msStart(1,:) =  msStartX;
    msTask{ii}.msStart(2,:) =  msStartY;
    msTask{ii}.msEnd(1,:) = msEndX; 
    msTask{ii}.msEnd(2,:) = msEndY;
    
%     for i = 1:length(fixationInfo{ii}.tracesX)
%         findMSandSPostProcess(pptrials,i)
%     end
end



%% MS Data Combine Fix and Task
load('MATFiles/MSFixationResultsHuxlin2.mat') %msFix from runHuxFixAnalysis

msAllAnalysis.task = msTask;
msAllAnalysis.ID = subjectsAll;
msAllAnalysis.PT = (~control);

%% orangize ms based on first, second, etc in trial Fixation
for ii = 1:subNum
    for i = 1:length(msFix{ii}.MicroSaccStartLoc)
        for m = 1:length(msFix{ii}.msAngle{i})
            msFix{ii}.msAngleStruct(i,m) = atan2d(...
                        msFix{ii}.MicroSaccStartLoc(i).y(m)-msFix{ii}.MicroSaccEndLoc(i).y(m), ...
                        msFix{ii}.MicroSaccStartLoc(i).x(m)-msFix{ii}.MicroSaccEndLoc(i).x(m));
            msFix{ii}.msAmpStruc(i,m)= msFix{ii}.msAmplitude{i}(m);
        end
    end
    msFix{ii}.msAngleStruct((msFix{ii}.msAngleStruct==0)) = NaN;
    msFix{ii}.msAmpStruc((msFix{ii}.msAmpStruc==0)) = NaN;  
end


for ii = find(~control)
%     rotateAngle = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
%     if ii == 1
%         rotateAngle = 315;
%     elseif ii == 2
%         rotateAngle = 50;
%     elseif ii == 3
%         rotateAngle = 145;
%     elseif ii == 4
%         rotateAngle = 90;
%     elseif ii == 10
%         rotateAngle = 90;
%     elseif ii == 22
%         rotateAngle = 315;
%     elseif ii == 23
%         rotateAngle = 90;
%     end
     lossIdx = find(vfResults.patientInfo.(huxID{ii}).deficit);
    lossAngles = vfResults.patientInfo.(huxID{ii}).rAngles(lossIdx);
    meanAngleRad = circ_mean(lossAngles');
    rotateAngle = mod(rad2deg(meanAngleRad),360);
    %     theta = -rotateAngle;
%     theta = rotateAngle;
    theta = rotateAngle;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    
%     v = [msAllAnalysis.fixation{ii}.msAllXCorrected;... %%%MS Fixation
%         msAllAnalysis.fixation{ii}.msAllYCorrected]';
    msFix{ii}.msAngleStructRotate = mod(msFix{ii}.msAngleStruct + theta,360);
%     msFix{ii}.msAngleStructRotate = v*R;
end

msAllAnalysis.fixation = msFix;
save('msAllAnalysis.mat','msAllAnalysis')


figure;
angles = [0 90 180 270];
polarplot(deg2rad(angles),[1 1 1 1],'*');

figure;
counter = 0;
for ii = find(~control)
%     figure;
counter = counter + 1;
subplot(4,4,counter)
    forlegend = [];
%     for i = 1:height(msFix{ii}.msAngleStructRotate)
        for t = 1:length(msFix{ii}.msAngleStructRotate(1,:))
            idx = msFix{ii}.msAmpStruc(:,t) < 60 &...
                msFix{ii}.msAmpStruc(:,t) > 5;
            if sum(idx) == 0
            continue
            end
            if t == 1
                forlegend(t) = polarplot(deg2rad(msFix{ii}.msAngleStructRotate(idx,t)),...
                    msFix{ii}.msAmpStruc(idx,t),'o','MarkerFaceColor',[0 0.4470 0.7410]);
            elseif t == 2
               
                forlegend(t) = polarplot(deg2rad(msFix{ii}.msAngleStructRotate(idx,t)),...
                    msFix{ii}.msAmpStruc(idx,t),'o','MarkerFaceColor',[0.8500 0.3250 0.0980]);
            else
                continue;
                 forlegend(t) = polarplot(deg2rad(msFix{ii}.msAngleStructRotate(idx,t)),...
                    msFix{ii}.msAmpStruc(idx,t),'o');
           
            end
            hold on
%             end
        end
%         legend(string([1:length(forlegend)]))
%         suptitle(sprintf('S%i',ii))
%     end
end


values = [];
msThreshold = 5;
rotateExtra = 0;
majorAxis = 0:15:330;

ii = 1;
for t = (find(~control))
    theta_deg = []; 
    theta_radians = [];
    theta_deg = reshape(msFix{t}.msAngleStructRotate,1,[]);
    theta_degNonRot = reshape(msFix{t}.msAngleStruct,1,[]);

    theta_radians = deg2rad(theta_deg);
    for i = 1:length(majorAxis)
         if majorAxis(i) == 0
            vecVals2{ii,i} = ((theta_deg < 15 & theta_deg >= 0) | ...
                theta_deg > 345);
        else
            vecVals2{ii,i} = (theta_deg < majorAxis(i) + 15 &...
                theta_deg > majorAxis(i)-15);
         end
        values2(ii,i) = length(find(vecVals2{ii,i}));
        
        vecVals3{ii,i} = (theta_degNonRot < majorAxis(i) + 15 &...
            theta_degNonRot > majorAxis(i)-15);
        values3(ii,i) = length(find(vecVals3{ii,i}));
        SEMVal(ii,i) = sem((theta_deg(theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15)));
    end
    ii = 1+ii;
end
hold on
rho2 = normalizeVector(mean(values2));
msNum.Patients = values2;
msNum.notRotatedPatients = values3;

ii = 1;
for t = (find(control))
    theta_deg = []; 
    theta_radians = [];
    theta_deg = reshape(msFix{t}.msAngleStruct,1,[]);
%     theta_degNonRot = reshape(msFix{ii}.msAngleStruct,1,[]);

    theta_radians = deg2rad(theta_deg);
    for i = 1:length(majorAxis)
         if majorAxis(i) == 0
            vecVals2{ii,i} = ((theta_deg < 15 & theta_deg >= 0) | ...
                theta_deg > 345);
        else
            vecVals2{ii,i} = (theta_deg < majorAxis(i) + 15 &...
                theta_deg > majorAxis(i)-15);
         end
        values2(ii,i) = length(find(vecVals2{ii,i}));
        
%         vecVals3{ii,i} = (theta_degNonRot < majorAxis(i) + 15 &...
%             theta_degNonRot > majorAxis(i)-15);
%         values3(ii,i) = length(find(vecVals3{ii,i}));
        SEMVal(ii,i) = sem((theta_deg(theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15)));
    end
    ii = 1+ii;
end
msNum.Controls = values;
msNum.Angles = majorAxis;

P = msNum.Patients./max(msNum.Patients')';
C = msNum.Controls./max(msNum.Controls')';
for ii = 1:length(find(~control))
    P(ii,:) = msNum.Patients(ii,:)/sum(msNum.Patients(ii,:));
end
%     C = dNum.Controls./max(dNum.Controls')';
for ii = 1:length(find(control))
    C(ii,:) = msNum.Controls(ii,:)/sum(msNum.Controls(ii,:));
end

towardsBF = nanmean([P(:,6) P(:,7) P(:,8)]);
awayBF = nanmean([P(:,18) P(:,19) P(:,20)]);
[h,p,~,stats] = ttest([P(:,6)' P(:,7)' P(:,8)'], [P(:,18)' P(:,19)' P(:,20)']);

figure;
% [h,p] = ttest([P(:,1)',P(:,2)' P(:,3)' P(:,4)' P(:,5)' P(:,6)' ], ...
%    [P(:,7)' P(:,8)' P(:,9)' P(:,10)' P(:,11)' P(:,12)']);
% errorbar([0 1],[mean([P(:,1)',P(:,2)' P(:,3)' P(:,4)' P(:,5)' P(:,6)' ]) ...
%     mean([P(:,7)' P(:,8)' P(:,9)' P(:,10)' P(:,11)' P(:,12)'])],...
%     [sem(([P(:,1)',P(:,2)' P(:,3)' P(:,4)' P(:,5)' P(:,6)' ])) ...
%     sem(([P(:,7)' P(:,8)' P(:,9)' P(:,10)' P(:,11)' P(:,12)']))],'-o',...
%     'Color','k','MarkerFaceColor','k','MarkerSize',10);
errorbar([0 1],[mean([P(:,6)' P(:,7)' P(:,8)']) mean([P(:,18)' P(:,19)' P(:,20)'])],...
    [sem([P(:,6)' P(:,7)' P(:,8)']) sem([P(:,8)' P(:,19)' P(:,20)'])],'-o');
ylabel('Normalized Probability of MS Direction');
xticks([0 1])
xlim([-.25 1.25])
xticklabels({'Towards BF','Away from BF'});
title('p = ',round(p,3))
set(gca,'FontSize',14);
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSTtest.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSTest.epsc');

figure;
polarplot(deg2rad([majorAxis 0]),[mean(P) mean(P(:,1))],'-o'); hold on
polarplot(deg2rad([majorAxis 0]),[mean(P)+sem(P) mean(P(:,1))+sem(P(:,1))],'--');hold on
polarplot(deg2rad([majorAxis 0]),[mean(P)-sem(P) mean(P(:,1))-sem(P(:,1))],'--')
title('Probability of MS Direction');
%% Clean Up MS Based on Angle and Amp.
%%%fixation
for ii = 1:length(subjectsAll) %getALLMS in one vector
    tempX = []; tempY = []; temp1 = []; temp2 = []; temp3 = []; temp4 = [];
    for i = 1:length(msFix{ii}.MicroSaccEndLoc)
        tempX = [tempX msFix{ii}.MicroSaccEndLoc(i).x - msFix{ii}.MicroSaccStartLoc(i).x];
        tempY = [tempY msFix{ii}.MicroSaccEndLoc(i).y - msFix{ii}.MicroSaccStartLoc(i).y];
        temp1 = [temp1 msFix{ii}.MicroSaccStartLoc(i).x];
        temp2 = [temp2 msFix{ii}.MicroSaccStartLoc(i).y];
        temp3 = [temp3 msFix{ii}.MicroSaccEndLoc(i).x];
        temp4 = [temp4 msFix{ii}.MicroSaccEndLoc(i).y];
    end
    idx = find(abs(eucDist(tempX,tempY)) < 30);
    msAllAnalysis.fixation{ii}.msAllXCorrected = tempX(idx);
    msAllAnalysis.fixation{ii}.msAllYCorrected = tempY(idx);
    msAllAnalysis.fixation{ii}.msAllCorrectedIdx = idx;
    msAllAnalysis.fixation{ii}.msAllCleanS(1,:) = temp1(idx);
    msAllAnalysis.fixation{ii}.msAllCleanS(2,:) = temp2(idx);
    msAllAnalysis.fixation{ii}.msAllCleanE(1,:) = temp3(idx);
    msAllAnalysis.fixation{ii}.msAllCleanE(2,:) = temp4(idx);
    
    x2 = tempX(idx);
    y2 = tempY(idx);
    msAllAnalysis.fixation{ii}.msAngleCorrected = ...
        atan2d(y2,x2);
     
    msAllAnalysis.fixation{ii}.msAmpCorrected = ...
        hypot(y2,x2);
    
    idx = find(abs(eucDist(tempX,tempY)) >= 30 & eucDist(tempX,tempY) < 500);
    sAllAnalysis.fixation{ii}.sAllXCorrected = tempX(idx);
    sAllAnalysis.fixation{ii}.sAllYCorrected = tempY(idx);
    sAllAnalysis.fixation{ii}.sAllCorrectedIdx = idx;
    x2 = tempX(idx);
    y2 = tempY(idx);
    sAllAnalysis.fixation{ii}.sAngleCorrected = ...
        atan2d(y2,x2);
     sAllAnalysis.fixation{ii}.sAmpCorrected = ...
        hypot(y2,x2);
end
%%%task
for ii = 1:length(subjectsAll) %getALLMS in one vector
    if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
        msAllAnalysis.task{ii}.msAllXCorrected = NaN;
        msAllAnalysis.task{ii}.msAllYCorrected = NaN;
        msAllAnalysis.task{ii}.msAllCorrectedIdx = NaN;
        msAllAnalysis.task{ii}.msAmpCorrected = NaN;
        msAllAnalysis.task{ii}.msAngleCorrected = NaN;
        continue;
    end
    tempX = []; tempY = []; temp1 = []; temp2 = []; temp3 = []; temp4 = [];
    for i = 1:length(msFix{ii}.MicroSaccEndLoc)
        tempX = [tempX msTask{ii}.msEnd(1,:) - msTask{ii}.msStart(1,:)];
        tempY = [tempY msTask{ii}.msEnd(2,:) - msTask{ii}.msStart(2,:)];
        temp1 = [temp1 msTask{ii}.msStart(1,:)];
        temp2 = [temp2 msTask{ii}.msStart(2,:)];
        temp3 = [temp3 msTask{ii}.msEnd(1,:)];
        temp4 = [temp4 msTask{ii}.msEnd(2,:)];
    end
    idx = find(eucDist(tempX,tempY) < 30);
    msAllAnalysis.task{ii}.msAllXCorrected = tempX(idx);
    msAllAnalysis.task{ii}.msAllYCorrected = tempY(idx);
    msAllAnalysis.task{ii}.msAllCorrectedIdx = idx;
    msAllAnalysis.task{ii}.msAllCleanS(1,:) = temp1(idx);
    msAllAnalysis.task{ii}.msAllCleanS(2,:) = temp2(idx);
    msAllAnalysis.task{ii}.msAllCleanE(1,:) = temp3(idx);
    msAllAnalysis.task{ii}.msAllCleanE(2,:) = temp4(idx);
    
    x2 = tempX(idx);
    y2 = tempY(idx);
    msAllAnalysis.task{ii}.msAngleCorrected = ...
        atan2d(y2,x2);
     msAllAnalysis.task{ii}.msAmpCorrected = ...
        hypot(y2,x2);

    idx = find(eucDist(tempX,tempY) > 30 & eucDist(tempX,tempY) < 500);
    sAllAnalysis.task{ii}.sAllXCorrected = tempX(idx);
    sAllAnalysis.task{ii}.sAllYCorrected = tempY(idx);
    sAllAnalysis.task{ii}.sAllCorrectedIdx = idx;
    x2 = tempX(idx);
    y2 = tempY(idx);
    sAllAnalysis.task{ii}.sAngleCorrected = ...
        atan2d(y2,x2);
    sAllAnalysis.task{ii}.sAmpCorrected = ...
        hypot(y2,x2);
end

%%% roated task and fix
for ii = find(~control)
%     rotateAngle = vfResults.patientInfo.(huxID{ii}).middleAngleLoss;
%     if ii == 1
%         rotateAngle = 315;
%     elseif ii == 2
%         rotateAngle = 50;
%     elseif ii == 3
%         rotateAngle = 145;
%     elseif ii == 4
%         rotateAngle = 90;
%     elseif ii == 10
%         rotateAngle = 90;
%     elseif ii == 22
%         rotateAngle = 315;
%     elseif ii == 23
%         rotateAngle = 90;
%     end
%     theta = -rotateAngle;
    lossIdx = find(vfResults.patientInfo.(huxID{ii}).deficit);
    lossAngles = vfResults.patientInfo.(huxID{ii}).rAngles(lossIdx);
    meanAngleRad = circ_mean(lossAngles');
    rotateAngle = mod(rad2deg(meanAngleRad),360);
    theta = rotateAngle;
    
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    
    v = [msAllAnalysis.fixation{ii}.msAllXCorrected;... %%%MS Fixation
        msAllAnalysis.fixation{ii}.msAllYCorrected]';
    rotatedAligned.fix{ii} = v*R;
    rotatedAligned.msAngleCorrectedFix{ii} = ...
        atan2d(rotatedAligned.fix{ii}(:,2),rotatedAligned.fix{ii}(:,1));
  
    
    v = [msAllAnalysis.fixation{ii}.msAllCleanS(1,:);... %ms start fix
        msAllAnalysis.fixation{ii}.msAllCleanS(2,:)]';
    rotatedAligned.msSfixation{ii} = v*R;
        
    v = [msAllAnalysis.fixation{ii}.msAllCleanE(1,:);...%MS end fix
        msAllAnalysis.fixation{ii}.msAllCleanE(2,:)]';
    rotatedAligned.msEfixation{ii} = v*R;
    
    v = [sAllAnalysis.fixation{ii}.sAllXCorrected;... %%%S Fixation
        sAllAnalysis.fixation{ii}.sAllYCorrected]';
    rotatedAligned.sfix{ii} = v*R;
    rotatedAligned.sAngleCorrectedFix{ii} = ...
        atan2d(rotatedAligned.sfix{ii}(:,2),rotatedAligned.sfix{ii}(:,1));
    
    
    if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
        rotatedAligned.task{ii} = NaN;
        rotatedAligned.msAngleCorrectedtask{ii} = NaN;
        rotatedAligned.msAmpCorrectedtask{ii} = NaN;
        continue;
    end
    v2 = [msAllAnalysis.task{ii}.msEnd(1,:) - msAllAnalysis.task{ii}.msStart(1,:)  ;...
       msAllAnalysis.task{ii}.msEnd(2,:) - msAllAnalysis.task{ii}.msStart(2,:)]';
    rotatedAligned.task{ii} = v2*R;
    rotatedAligned.msAngleCorrectedtask{ii} = ...
        atan2d(rotatedAligned.task{ii}(:,2),rotatedAligned.task{ii}(:,1));
    rotatedAligned.msAmpCorrectedtask{ii} = ...
        hypot(rotatedAligned.task{ii}(:,2),rotatedAligned.task{ii}(:,1));
    
    v2 = [msAllAnalysis.task{ii}.msAllCleanS(1,:);...%start task
        msAllAnalysis.task{ii}.msAllCleanS(2,:)]';
    rotatedAligned.msStask{ii} = v2*R;
        
    v2 = [msAllAnalysis.task{ii}.msAllCleanE(1,:);...%end task
        msAllAnalysis.task{ii}.msAllCleanE(2,:)]';
    rotatedAligned.msEtask{ii} = v2*R;
    
    v2 = [sAllAnalysis.task{ii}.sAllXCorrected;... %saccade
        sAllAnalysis.task{ii}.sAllYCorrected]';
    rotatedAligned.stask{ii} = v2*R;
    rotatedAligned.sAngleCorrectedtask{ii} = ...
        atan2d(rotatedAligned.stask{ii}(:,2),rotatedAligned.stask{ii}(:,1));
    
    
end

for ii = 1:length(subjectsAll)
    msAll.msRateT(ii) = msTask{ii}.msRate;
    msAll.msRateF(ii) = mean(msFix{ii}.msRates(msFix{ii}.msRates > 0));
end

%%
[h,p] = ttest2(msAll.msRateF(control == 1),msAll.msRateF(control == 0))

%% Plot MS from Start to end

% for ii = find(~control)
%     for t = 1:length(msAllAnalysis.fixation{ii}.msAngle)
%         for m = 1:length(msAllAnalysis.fixation{ii}.msAngle(t))
%             ms(t,m) = msAllAnalysis.fixation{ii}.msAngle(m);
%         end
%     end
% end



%% Angle of Fixation (PT Rotated and CTL)
values = [];
msThreshold = 5;
rotateExtra = 0;
% close all
% majorAxis = [0 30 60 90 120 150 180 210 240 270 300 330];
majorAxis = 0:15:330;

ii = 1;
for t = (find(msAllAnalysis.PT == 0))
    theta_deg = []; 
    theta_radians = [];
    theta_deg = mod(msAllAnalysis.fixation{t}.msAngleCorrected(...
        msAllAnalysis.fixation{t}.msAmpCorrected > msThreshold),360);
    theta_radians = deg2rad(theta_deg);
    for i = 1:length(majorAxis)
        if majorAxis(i) == 0
            vecVals{ii,i} = (theta_deg < 15 & theta_deg >= 0) | ...
                theta_deg > 345;
        else
            vecVals{ii,i} = (theta_deg < majorAxis(i) + 15 &...
                theta_deg > majorAxis(i)-15);
        end
        values(ii,i) = length(find(vecVals{ii,i}));
        SEMVal(ii,i) = sem(normalizeVector(theta_deg(theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15)));
    end
    ii = 1+ii;
end
rho = normalizeVector(mean(values));% (values(1))]); % change this vector (it is a vector of 8, and then add the first value to the end  to connect the polar plot )

% [p,tbl,stats] = anova1(msNum.Patients);
% xticklabels(majorAxis);
% results = multcompare(stats);
% yticklabels(majorAxis);

msNum.Controls = values;
msNum.Angles = majorAxis;
% save('HuxMsNums.mat','msNum');
ii = 1;
for t = (find(~control))
    theta_deg = []; 
    theta_radians = [];
    theta_deg = mod(rotatedAligned.msAngleCorrectedFix{t}(...
        msAllAnalysis.fixation{t}.msAmpCorrected > msThreshold),360)'+rotateExtra;
    if rotateExtra ~= 0
        idx = (find(theta_deg>360)); theta_deg(idx) = theta_deg(idx) - 360;
    end
       theta_degNonRot = mod(msAllAnalysis.fixation{t}.msAngleCorrected(...
        msAllAnalysis.fixation{t}.msAmpCorrected > msThreshold),360)';
    
    theta_radians = deg2rad(theta_deg);
    for i = 1:length(majorAxis)
         if majorAxis(i) == 0
            vecVals2{ii,i} = ((theta_deg < 15 & theta_deg >= 0) | ...
                theta_deg > 345);
        else
            vecVals2{ii,i} = (theta_deg < majorAxis(i) + 15 &...
                theta_deg > majorAxis(i)-15);
        end
%         vecVals2{ii,i} = (theta_deg < majorAxis(i) + 15 &...
%             theta_deg > majorAxis(i)-15);
        values2(ii,i) = length(find(vecVals2{ii,i}));
        
        vecVals3{ii,i} = (theta_degNonRot < majorAxis(i) + 15 &...
            theta_degNonRot > majorAxis(i)-15);
        values3(ii,i) = length(find(vecVals3{ii,i}));
        SEMVal(ii,i) = sem((theta_deg(theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15)));
    end
    ii = 1+ii;
end
hold on
rho2 = normalizeVector(mean(values2));
msNum.Patients = values2;
msNum.notRotatedPatients = values3;

P2 = msNum.notRotatedPatients./max(msNum.notRotatedPatients')';
P = msNum.Patients./max(msNum.Patients')';
C = msNum.Controls./max(msNum.Controls')';


towardsBF = nanmean([P(:,6) P(:,7) P(:,8)]);
awayBF = nanmean([P(:,18) P(:,19) P(:,20)]);
[h,p,~,stats] = ttest([P(:,6)' P(:,7)' P(:,8)'], [P(:,18)' P(:,19)' P(:,20)']);
figure;
errorbar([0 1],[mean([P(:,6)' P(:,7)' P(:,8)']) mean([P(:,18)' P(:,19)' P(:,20)'])],...
    [sem([P(:,6)' P(:,7)' P(:,8)']) sem([P(:,8)' P(:,19)' P(:,20)'])],'-o');
ylabel('Normalized Probability of MS Direction');
xticks([0 1])
xlim([-.25 1.25])
xticklabels({'Towards BF','Away from BF'});
title('p = ',round(p,3))
set(gca,'FontSize',14);
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSTtest.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSTest.epsc');

mean([P(:,6)' P(:,7)' P(:,8)'])
std([P(:,6)' P(:,7)' P(:,8)'])
mean(([P(:,18)' P(:,19)' P(:,20)']))
std(([P(:,18)' P(:,19)' P(:,20)']))


figure;
temp = nanmean(msNum.notRotatedPatients./max(msNum.notRotatedPatients')');
% temp = nanmean(msNum.Patients./max(msNum.Patients')');

polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [temp temp(1)],...
    'b','MarkerSize', 30);
hold on
tempS = (sem(msNum.notRotatedPatients./max(msNum.notRotatedPatients')')/2);
% tempS = (sem(msNum.Patients./max(msNum.Patients')')/2);
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [tempS+temp tempS(1)+temp(1)],...
    '--b','MarkerSize', 30);
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [temp-tempS temp(1)-tempS(1)],...
    '--b','MarkerSize', 30);
hold on
temp2 = mean(msNum.Controls./max(msNum.Controls')');
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),[temp2 temp2(1)],...
    'r','MarkerSize', 30);
tempS2 = (sem(msNum.Controls./max(msNum.Controls')')/2);
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [tempS2+temp2 tempS2(1)+temp2(1)],...
    '--r','MarkerSize', 30);
polarplot(deg2rad([msNum.Angles msNum.Angles(1)]),...
    [temp2-tempS2 temp2(1)-tempS2(1)],...
    '--r','MarkerSize', 30);

[h,p] = ttest2(msNum.notRotatedPatients,msNum.Controls);
% mean(p)
[~,~,stats] = anovan(mean([msNum.notRotatedPatients',...
       msNum.Controls']),{control});
multcompare(stats)
% multcompare(stats,"Dimension",[1 2])

[h,p] = ttest2(P,C);
polarplot(deg2rad(msNum.Angles(find(h))),ones(1,length(find(h))),...
    '*','Color','k');

saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSHistogramAngleComb.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAnlgeComb.epsc');


[~,~,stats] = anovan([mean([msNum.notRotatedPatients]),...
       mean([msNum.Controls])],{[ones(1,23),ones(1,23)*2]});
multcompare(stats)


for ii = 1:length(find(~control))
    P(ii,:) = msNum.Patients(ii,:)/sum(msNum.Patients(ii,:));
end
%     C = dNum.Controls./max(dNum.Controls')';
for ii = 1:length(find(control))
    C(ii,:) = msNum.Controls(ii,:)/sum(msNum.Controls(ii,:));
end
% for ii = 1:length(find(~control))
%     P2(ii,:) = dNum.Patients(ii,:)/sum(dNum.Patients(ii,:));
% end

%%
figure;
subplot(1,2,1)
polarplot(deg2rad([majorAxis 0]),[mean(P) mean(P(:,1))],'-o'); hold on
polarplot(deg2rad([majorAxis 0]),[mean(P)+sem(P) mean(P(:,1))+sem(P(:,1))],'--');hold on
polarplot(deg2rad([majorAxis 0]),[mean(P)-sem(P) mean(P(:,1))-sem(P(:,1))],'--')
title('Probability of MS Direction');

% figure;
% subplot(1,2,1)
% polarplot(deg2rad([majorAxis 0]),[mean(P2) mean(P2(:,1))],'-o'); hold on
% polarplot(deg2rad([majorAxis 0]),[mean(P2)+sem(P2) mean(P2(:,1))+sem(P2(:,1))],'--');hold on
% polarplot(deg2rad([majorAxis 0]),[mean(P2)-sem(P2) mean(P2(:,1))-sem(P2(:,1))],'--')
% title('Probability of MS Direction');
% 
% subplot(1,2,2)
% polarplot(deg2rad([majorAxis 0]),[mean(C) mean(C(:,1))],'-o'); hold on
% polarplot(deg2rad([majorAxis 0]),[mean(C)+sem(C) mean(C(:,1))+sem(C(:,1))],'--');hold on
% polarplot(deg2rad([majorAxis 0]),[mean(C)-sem(C) mean(C(:,1))-sem(C(:,1))],'--')
% title('Probability of MS Direction Controls');


subplot(1,2,2)
upMat = [P(:,5),P(:,6),P(:,7),P(:,8),P(:,9)];
up = [P(:,5)',P(:,6)',P(:,7)',P(:,8)',P(:,9)']/16;
downMat = [P(:,17),P(:,18),P(:,19),P(:,20),P(:,21)];
down = [P(:,17)',P(:,18)',P(:,19)',P(:,20)',P(:,21)']/16;
left = [P(:,10)',P(:,11)',P(:,12)',P(:,13)',P(:,14)',P(:,15)',P(:,16)']/16;
right = [P(:,1)',P(:,2)',P(:,3)',P(:,4)',P(:,22)',P(:,23)']/16;

eachSubjUp = sum(upMat');
eachSubjDo = sum(downMat');
for i = 1:size(upMat)
    plot([0 1],[eachSubjUp(i) eachSubjDo(i)],'--','Color',[.3 .3 .3])
    hold on
end

errorbar([0 1],[sum(up) sum(down)],...
    [sem(sum(upMat')),sem(sum(downMat'))],'-o',...
    'Color','k','MarkerFaceColor','k','MarkerSize',10);
hold on
errorbar([2 3],[mean([P(:,6)' P(:,7)' P(:,8)']) mean([P(:,1)' P(:,2)' P(:,12)'])],...
    [sem([P(:,6)' P(:,7)' P(:,8)']) sem([P(:,1)' P(:,2)' P(:,12)'])],'-o',...
    'Color','b','MarkerFaceColor','b','MarkerSize',10);
ylabel('Probability of MS Direction');
xticks([0 1])
% xtickslabels({'Towards BF
xlim([-.25 1.25])
xticklabels({'Towards BF','Away from BF'});
[h,p] = ttest(sum(upMat'),sum(downMat'));
title(sprintf('p = %.3f',p));
set(gca,'FontSize',14);




%% MS Rates
% % % figure;
% % %      plot(control,msAll.msRateT,'o')
% % %      hold on
% % %      plot(control+2,msAll.msRateF,'o')
% % % %      set(gca, 'YScale', 'log')
% % %      ax = gca; % axes handle
% % %      xlim([-.5 3.5])
% % %      xlabel('Condition');
% % % %      ylim([0 1])
% % %      ylabel('Microsaccade Rate (ms/s)');
% % % %      yticks([0.01 0.03 0.1 0.3 1 3 10])
% % %      xticks([0 1 2 3]);
% % %      xticklabels({'Patient Task','Control Task', 'Patient Fix','Control Fix'});
% % %     hold on
% % %     errorbar([0.2 1.2 2.2 3.2],...
% % %         [nanmean(msAll.msRateT(find(~control))),...
% % %         nanmean(msAll.msRateT(find(control))),...
% % %         nanmean(msAll.msRateF(find(~control))),...
% % %         nanmean(msAll.msRateF(find(control)))],...
% % %         [nanstd(msAll.msRateT(find(~control))),...
% % %         nanstd(msAll.msRateT(find(control))),...
% % %         nanstd(msAll.msRateF(find(~control))),...
% % %         nanstd(msAll.msRateF(find(control)))],...
% % %         'Color','k')
% % %     saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSRate.png');
% % % saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSRate.epsc');
%% MS Directions
% % % figure;
% % % subplot(4,3,1)
% % %     sAngleC = [];
% % %     for ii = find(control)
% % %         sAngleC = [sAngleC sAllAnalysis.task{ii}.sAngleCorrected];
% % %     end
% % %      polarhistogram(deg2rad(sAngleC))
% % %     title('Control,Task S')
% % % 
% % % subplot(4,3,2)
% % %     sAngleP = [];
% % %     for ii = find(~control)
% % %         if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
% % %             continue;
% % %         end
% % %         sAngleP = [sAngleP sAllAnalysis.task{ii}.sAngleCorrected];
% % %     end
% % %     polarhistogram(deg2rad(sAngleP))
% % %     title('Patient,Task S')
% % %     
% % % subplot(4,3,3)
% % %     sAnglePRot = [];
% % %     for ii = find(~control)
% % %       if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
% % %             continue;
% % %         end
% % %         sAnglePRot = [sAnglePRot rotatedAligned.sAngleCorrectedtask{ii}'];
% % %     end
% % %     polarhistogram(deg2rad(sAnglePRot))
% % %     title('Patient, Task S Rotated')
% % % 
% % % subplot(4,3,4)
% % %     sAngleC = [];
% % %     for ii = find(control)
% % %         sAngleC = [sAngleC (sAllAnalysis.fixation{ii}.sAngleCorrected)];
% % %     end
% % %      polarhistogram(deg2rad(sAngleC))
% % %     
% % %     title('Control, Fixation S')
% % %     % ylabel('Fixation');
% % % 
% % % subplot(4,3,5)
% % %     sAngleP = [];
% % %     for ii = find(~control)
% % %        sAngleP = [sAngleP sAllAnalysis.fixation{ii}.sAngleCorrected];
% % %     end
% % %     polarhistogram(deg2rad(sAngleP))
% % %     title('Patient,Fix S')
% % % 
% % % 
% % % subplot(4,3,6)
% % %     sAnglePRot = [];
% % %     for ii = find(~control)
% % %         sAnglePRot = [sAnglePRot rotatedAligned.sAngleCorrectedFix{ii}'];
% % %     end
% % %     polarhistogram(deg2rad(sAnglePRot(sAnglePRot ~= 0)))
% % %     title('Patient, Fix S Rotated')    
% % %     
% % %     
% % % %%% Microsaccades    
% % % subplot(4,3,7)
% % %     msAngleC = [];
% % %     allIdx = [];
% % %     for ii = find(control)
% % %         msAngleC = [msAngleC msAllAnalysis.task{ii}.msAngle];
% % %     end
% % %      polarhistogram(deg2rad(msAngleC))
% % %     title('Control,Task MS')
% % %     % ylabel('Task');    
% % % 
% % % %  subplot(4,3,8)
% % % 
% % % figure;
% % % subplot(1,2,1)
% % % msAngleP = [];msAngleAmplP = [];
% % % for ii = find(~control)
% % %     if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
% % %         continue;
% % %     end
% % %     msAngleP = [msAngleP msAllAnalysis.task{ii}.msAngleCorrected];
% % %     msAngleAmplP = [msAngleAmplP msAllAnalysis.task{ii}.msAmpCorrected];
% % %     
% % % end
% % % polarhistogram(deg2rad(msAngleP(find(msAngleAmplP > 5))))
% % % title('Patient,Task MS')
% % % 
% % % subplot(1,2,2)
% % % msAngleP = [];msAngleAmplP = [];
% % % for ii = find(control)
% % %     if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
% % %         continue;
% % %     end
% % %     msAngleP = [msAngleP msAllAnalysis.task{ii}.msAngleCorrected];
% % %     msAngleAmplP = [msAngleAmplP msAllAnalysis.task{ii}.msAmpCorrected];
% % %     
% % % end
% % % polarhistogram(deg2rad(msAngleP(find(msAngleAmplP > 5))))
% % % title('Control, Task')
% % % 
% % % %%%%%%%fixation%%%%
% % % figure;
% % % 
% % % % subplot(4,3,10)
% % %     msAngleC = []; msAngleAmplC = [];
% % %     for ii = find(control)
% % %         msAngleC = [msAngleC (msAllAnalysis.fixation{ii}.msAngleCorrected)];
% % %         msAngleAmplC = [msAngleAmplC msAllAnalysis.fixation{ii}.msAmpCorrected];
% % %     end
% % %      polarhistogram(deg2rad(msAngleC(find(msAngleAmplC > 5))))
% % %     
% % %     title('Control, Fixation MS')
% % %     % ylabel('Fixation');
% % % 
% % % % subplot(4,3,11)
% % % %     msAngleP = [];
% % % %     for ii = find(~control)
% % % %        msAngleP = [msAngleP msAllAnalysis.fixation{ii}.msAngleCorrected];
% % % %     end
% % % %     polarhistogram(deg2rad(msAngleP))
% % % %     title('Patient,Fix MS')
% % % 
% % % 
% % % subplot(4,3,12)
% % % figure;
% % %     msAnglePRot = []; msAngleAmpl = [];
% % %     for ii = find(~control)
% % %         msAnglePRot = [msAnglePRot rotatedAligned.msAngleCorrectedFix{ii}'];
% % %         msAngleAmpl = [msAngleAmpl msAllAnalysis.fixation{ii}.msAmpCorrected];
% % %     end
% % %      polarhistogram(deg2rad(msAnglePRot(find(msAngleAmpl > 5))))
% % %     title('Patient, Fix MS Rotated')
% % %     
% % %  
% % % % suptitle('Microsaccade Analysis')
% % % saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAngle.png');
% % % saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAnlge.epsc');

%% Angle of Task (note not really enough task trials)

msThreshold = 5;

close all
majorAxis = [0 30 60 90 120 150 180 210 240 270 300 330];
ii = 1;
for t = (find(control))
    theta_deg = []; theta_radians = [];
    theta_deg = mod(msAllAnalysis.task{t}.msAngleCorrected(...
        msAllAnalysis.task{t}.msAmpCorrected > msThreshold),360);
    theta_radians = deg2rad(theta_deg);
    for i = 1:length(majorAxis)
        vecVals{ii,i} = (theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15);
        values(ii,i) = length(find(vecVals{ii,i}));
        SEMVal(ii,i) = sem(normalizeVector(theta_deg(theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15)));
    end
    ii = 1+ii;
end
rho = normalizeVector(mean(values));% (values(1))]); % change this vector (it is a vector of 8, and then add the first value to the end  to connect the polar plot )

msNumT.Controls = values;
msNumT.Angles = majorAxis;
% save('HuxMsNums.mat','msNum');
ii = 1;
for t = (find(~control))
    theta_deg = []; theta_radians = [];
    theta_deg = mod(rotatedAligned.msAngleCorrectedtask{t}(...
        rotatedAligned.msAmpCorrectedtask{t}> msThreshold),360)';
    
    theta_degNonRot = mod(msAllAnalysis.task{t}.msAngleCorrected(...
        msAllAnalysis.task{t}.msAmpCorrected > msThreshold),360)';
    
    theta_radians = deg2rad(theta_deg);
    for i = 1:length(majorAxis)
        vecVals2{ii,i} = (theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15);
        values2(ii,i) = length(find(vecVals2{ii,i}));
        
        vecVals3{ii,i} = (theta_degNonRot < majorAxis(i) + 15 &...
            theta_degNonRot > majorAxis(i)-15);
        values3(ii,i) = length(find(vecVals3{ii,i}));
        SEMVal(ii,i) = sem((theta_deg(theta_deg < majorAxis(i) + 15 &...
            theta_deg > majorAxis(i)-15)));
    end
    ii = 1+ii;
end
hold on
rho2 = normalizeVector(mean(values2));
msNumT.Patients = values2;
msNumT.notRotatedPatients = values3;
% figure;
% errorbar(msNum.Angles,...
%     mean(msNum.Patients./max(msNum.Patients')'),...
%     sem(msNum.Patients./max(msNum.Patients')'))
% hold on
% errorbar(msNum.Angles,...
%     mean(msNum.Controls./max(msNum.Controls')'),...
%     sem(msNum.Controls./max(msNum.Controls')'))
% P = msNum.notRotatedPatients./max(msNum.notRotatedPatients')';
P = msNumT.Patients./max(msNumT.Patients')';
C = msNumT.Controls./max(msNumT.Controls')';
% [h,p] = ttest2(P,C)
% plot(msNum.Angles(find(h)),ones(1,length(find(h))),'*','Color','k');

% % % % % % figure;
% % % % % % % temp = mean(msNum.notRotatedPatients./max(msNum.notRotatedPatients')');
% % % % % % temp = nanmean(msNumT.Patients./max(msNumT.Patients')');
% % % % % % 
% % % % % % polarplot(deg2rad([msNumT.Angles msNumT.Angles(1)]),...
% % % % % %     [temp temp(1)],...
% % % % % %     'b','MarkerSize', 30);
% % % % % % hold on
% % % % % % % tempS = (sem(msNum.notRotatedPatients./max(msNum.notRotatedPatients')')/2);
% % % % % % tempS = (sem(msNumT.Patients./max(msNumT.Patients')')/2);
% % % % % % polarplot(deg2rad([msNumT.Angles msNumT.Angles(1)]),...
% % % % % %     [tempS+temp tempS(1)+temp(1)],...
% % % % % %     '--b','MarkerSize', 30);
% % % % % % polarplot(deg2rad([msNumT.Angles msNumT.Angles(1)]),...
% % % % % %     [temp-tempS temp(1)-tempS(1)],...
% % % % % %     '--b','MarkerSize', 30);
% % % % % % hold on
% % % % % % temp2 = nanmean(msNumT.Controls./max(msNumT.Controls')');
% % % % % % polarplot(deg2rad([msNumT.Angles msNumT.Angles(1)]),[temp2 temp2(1)],...
% % % % % %     'r','MarkerSize', 30);
% % % % % % tempS2 = (sem(msNumT.Controls./max(msNumT.Controls')')/2);
% % % % % % polarplot(deg2rad([msNumT.Angles msNumT.Angles(1)]),...
% % % % % %     [tempS2+temp2 tempS2(1)+temp2(1)],...
% % % % % %     '--r','MarkerSize', 30);
% % % % % % polarplot(deg2rad([msNumT.Angles msNumT.Angles(1)]),...
% % % % % %     [temp2-tempS2 temp2(1)-tempS2(1)],...
% % % % % %     '--r','MarkerSize', 30);
% % % % % % 
% % % % % % [h,p] = ttest2(P,C);
% % % % % % polarplot(deg2rad(msNumT.Angles(find(h))),ones(1,length(find(h))),...
% % % % % %     '*','Color','k');
%% Micrisaccade Landing Absolute
%%%tasks
% % % 
% % % figure;
% % % subplot(2,3,1)
% % %     temp1S = [];
% % %     temp11S = [];
% % %     temp1E = [];
% % %     temp11E = [];
% % %     for ii = find(control)
% % %         temp1S = [temp1S msAllAnalysis.task{ii}.msAllCleanS(1,:)];
% % %         temp11S = [temp11S msAllAnalysis.task{ii}.msAllCleanS(2,:)];
% % %         temp1E = [temp1E msAllAnalysis.task{ii}.msAllCleanE(1,:)];
% % %         temp11E = [temp11E msAllAnalysis.task{ii}.msAllCleanE(2,:)];
% % %     end
% % %     leg(1) = plot(temp1S,temp11S,'*','Color','r');
% % %     hold on
% % %     leg(2) = plot(temp1E,temp11E,'*','Color','b');
% % %     legend(leg,{'Start','End'});
% % %     % end
% % %     axis([-60 60 -60 60])
% % %     axis square
% % %     title('Task, Control')
% % % 
% % %  subplot(2,3,2)
% % %     temp1S = [];
% % %     temp11S = [];
% % %     temp1E = [];
% % %     temp11E = [];
% % %     for ii = find(~control)
% % %         if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
% % %             continue;
% % %         end
% % %         temp1S = [temp1S msAllAnalysis.task{ii}.msAllCleanS(1,:)];
% % %         temp11S = [temp11S msAllAnalysis.task{ii}.msAllCleanS(2,:)];
% % %         temp1E = [temp1E msAllAnalysis.task{ii}.msAllCleanE(1,:)];
% % %         temp11E = [temp11E msAllAnalysis.task{ii}.msAllCleanE(2,:)];
% % %     end
% % %      leg(1) = plot(temp1S,temp11S,'*','Color','r');
% % %     hold on
% % %     leg(2) = plot(temp1E,temp11E,'*','Color','b');
% % %     legend(leg,{'Start','End'});
% % %     % end
% % %     axis([-60 60 -60 60])
% % %     axis square
% % %     title('Task, PT')
% % %     
% % % subplot(2,3,3)
% % %     temp1S = [];
% % %     temp11S = [];
% % %     temp1E = [];
% % %     temp11E = [];
% % %     for ii = find(~control)
% % %         if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
% % %             continue;
% % %         end
% % %         temp1S = [temp1S rotatedAligned.msStask{ii}(:,1)];
% % %         temp11S = [temp11S rotatedAligned.msStask{ii}(:,2)];
% % %         temp1E = [temp1E rotatedAligned.msEtask{ii}(:,1)];
% % %         temp11E = [temp11E rotatedAligned.msEtask{ii}(:,2)];
% % %     end
% % %      leg(1) = plot(temp1S,temp11S,'*','Color','r');
% % %     hold on
% % %     leg(2) = plot(temp1E,temp11E,'*','Color','b');
% % %     legend(leg,{'Start','End'});
% % %     % end
% % %     axis([-60 60 -60 60])
% % %     axis square
% % %     title('Task Rotated, PT')
% % % 
% % % %%% fixation
% % % subplot(2,3,4)
% % %     temp1S = [];
% % %     temp11S = [];
% % %     temp1E = [];
% % %     temp11E = [];
% % %     for ii = find(control)
% % %         temp1S = [temp1S msAllAnalysis.fixation{ii}.msAllCleanS(1,:)];
% % %         temp11S = [temp11S msAllAnalysis.fixation{ii}.msAllCleanS(2,:)];
% % %         temp1E = [temp1E msAllAnalysis.fixation{ii}.msAllCleanE(1,:)];
% % %         temp11E = [temp11E msAllAnalysis.fixation{ii}.msAllCleanE(2,:)];
% % %     end
% % %     leg(1) = plot(temp1S,temp11S,'*','Color','r');
% % %     hold on
% % %     leg(2) = plot(temp1E,temp11E,'*','Color','b');
% % %     legend(leg,{'Start','End'});
% % %     % end
% % %     axis([-60 60 -60 60])
% % %     axis square
% % %     title('Task, Control')
% % % 
% % %  subplot(2,3,5)
% % %     temp1S = [];
% % %     temp11S = [];
% % %     temp1E = [];
% % %     temp11E = [];
% % %     for ii = find(~control)
% % %         temp1S = [temp1S msAllAnalysis.fixation{ii}.msAllCleanS(1,:)];
% % %         temp11S = [temp11S msAllAnalysis.fixation{ii}.msAllCleanS(2,:)];
% % %         temp1E = [temp1E msAllAnalysis.fixation{ii}.msAllCleanE(1,:)];
% % %         temp11E = [temp11E msAllAnalysis.fixation{ii}.msAllCleanE(2,:)];
% % %     end
% % %      leg(1) = plot(temp1S,temp11S,'*','Color','r');
% % %     hold on
% % %     leg(2) = plot(temp1E,temp11E,'*','Color','b');
% % %     legend(leg,{'Start','End'});
% % %     % end
% % %     axis([-60 60 -60 60])
% % %     axis square
% % %     title('Task, PT')
% % %     
% % % subplot(2,3,6)
% % %     temp1S = [];
% % %     temp11S = [];
% % %     temp1E = [];
% % %     temp11E = [];
% % %     for ii = find(~control)
% % %        temp1S = [temp1S rotatedAligned.msSfixation{ii}(:,1)];
% % %         temp11S = [temp11S rotatedAligned.msSfixation{ii}(:,2)];
% % %         temp1E = [temp1E rotatedAligned.msEfixation{ii}(:,1)];
% % %         temp11E = [temp11E rotatedAligned.msEfixation{ii}(:,2)];
% % %     end
% % %      leg(1) = plot(temp1S,temp11S,'*','Color','r');
% % %     hold on
% % %     leg(2) = plot(temp1E,temp11E,'*','Color','b');
% % %     legend(leg,{'Start','End'});
% % %     % end
% % %     axis([-60 60 -60 60])
% % %     axis square
% % %     title('Task Rotated, PT')
% % % 
% % % 
% % % saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSLandingAbsScatterTask.png');
% % saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSLandingAbsScatterTask.epsc');
%% INDIVIDUAL Micrisaccade Landing Absolute
% % task
% % % for ii = 1:length(subjectsAll)
% % %     if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
% % %         continue;
% % %     end
% % %     idx = msAllAnalysis.task{ii}.msAmplitude > 1;
% % %     figure;
% % %     subplot(1,2,1)
% % %     plot(msAllAnalysis.task{ii}.msAllCleanS(1,idx),...
% % %         msAllAnalysis.task{ii}.msAllCleanS(2,idx),'*','Color','r');
% % %     hold on
% % %     axis([-60 60 -60 60])
% % %     axis square
% % %     title('Start')
% % %     line([-60 60],[0 0])
% % %     line([0 0],[-60 60])
% % %     subplot(1,2,2)
% % %     plot(msAllAnalysis.task{ii}.msAllCleanE(1,idx),...
% % %         msAllAnalysis.task{ii}.msAllCleanE(2,idx),'*','Color','b');
% % %     
% % %     axis([-60 60 -60 60])
% % %     title('End')
% % %     axis square
% % %     line([-60 60],[0 0])
% % %     line([0 0],[-60 60])
% % %     suptitle(sprintf('%s, %i',subjectsAll{ii}, control(ii)))
% % %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sMSSandEAbsScatterTask.png', subjectsAll{ii}));
% % %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sMSSandEAbsScatterTask.epsc', subjectsAll{ii}));
% % %     
% % %     if control(ii) == 0
% % %         figure;
% % %         subplot(1,2,1)
% % %         plot(rotatedAligned.msStask{ii}(idx,1),...
% % %             rotatedAligned.msStask{ii}(idx,2),'*','Color','m');
% % %         hold on
% % %         axis([-60 60 -60 60])
% % %         axis square
% % %         title('Start')
% % %         line([-60 60],[0 0])
% % %         line([0 0],[-60 60])
% % %         subplot(1,2,2)
% % %         plot(rotatedAligned.msEtask{ii}(idx,1),...
% % %             rotatedAligned.msEtask{ii}(idx,2),'*','Color','c');
% % %         axis([-60 60 -60 60])
% % %         title('End')
% % %         axis square
% % %         line([-60 60],[0 0])
% % %         line([0 0],[-60 60])
% % %         suptitle(sprintf('%s, %i',subjectsAll{ii}, control(ii)))
% % %         saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sMSSandEAbsScatterTaskRotated.png', subjectsAll{ii}));
% % %         saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sMSSandEAbsScatterTaskRotated.epsc', subjectsAll{ii}));
% % %         
% % %     end
% % % end
% % % 
% % % % % fixation
% % % for ii = 1:length(subjectsAll)
% % %     figure;
% % %    idx2 = msAllAnalysis.fixation{ii}.msAmpCorrected > 3;
% % % 
% % %     subplot(1,2,1)
% % %     plot(msAllAnalysis.fixation{ii}.msAllCleanS(1,idx2),...
% % %         msAllAnalysis.fixation{ii}.msAllCleanS(2,idx2),'*','Color','r');
% % %     hold on
% % %     axis([-60 60 -60 60])
% % %     axis square
% % %     title('Start')
% % %     line([-60 60],[0 0])
% % %     line([0 0],[-60 60])
% % %     subplot(1,2,2)
% % %     plot(msAllAnalysis.fixation{ii}.msAllCleanE(1,idx2),...
% % %         msAllAnalysis.fixation{ii}.msAllCleanE(2,idx2),'*','Color','b');
% % %     
% % %     axis([-60 60 -60 60])
% % %     title('End')
% % %     axis square
% % %     line([-60 60],[0 0])
% % %     line([0 0],[-60 60])
% % %     suptitle(sprintf('%s, %i',subjectsAll{ii}, control(ii)))
% % %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sMSSandEAbsScatterFixation.png', subjectsAll{ii}));
% % %     saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sMSSandEAbsScatterFixation.epsc', subjectsAll{ii}));
% % %     
% % %     if control(ii) == 0
% % %         figure;
% % %         subplot(1,2,1)
% % %         plot(rotatedAligned.msSfixation{ii}(idx2,1),...
% % %             rotatedAligned.msSfixation{ii}(idx2,2),'*','Color','m');
% % %         hold on
% % %         axis([-60 60 -60 60])
% % %         axis square
% % %         title('Start')
% % %         line([-60 60],[0 0])
% % %         line([0 0],[-60 60])
% % %         subplot(1,2,2)
% % %         plot(rotatedAligned.msEfixation{ii}(idx2,1),...
% % %             rotatedAligned.msEfixation{ii}(idx2,2),'*','Color','c');
% % %         axis([-60 60 -60 60])
% % %         title('End')
% % %         axis square
% % %         line([-60 60],[0 0])
% % %         line([0 0],[-60 60])
% % %         suptitle(sprintf('%s, %i',subjectsAll{ii}, control(ii)))
% % %         saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sMSSandEAbsScatterFixationRotated.png', subjectsAll{ii}));
% % %         saveas(gcf,sprintf('../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/%sMSSandEAbsScatterFixationRotated.epsc', subjectsAll{ii}));
% % %         
% % %     end
% % % end
%% Individuals Towards or Away from Blindfield ("Productive MS")
% % fixation

%%%Polar Plot, Fixation
return;

figure;
subplot(1,2,2)
idx2 = find(msAngleAmpl > 10);
th1 = msAnglePRot(idx2);
th = deg2rad(th1);
r = msAngleAmpl(idx2);
polarscatter(th,r,'b','filled','MarkerFaceAlpha',.2)
title('Patients, Rotated')
hold on
majorAxis = [0 30 60 90 120 150 180 -150 -120 -90 -60 -30];
for i = 1:length(majorAxis)
    m = majorAxis(i);
    meanDirect1(i) = nanmean(r(find(th1 < m+15 & th1 > m-15)));
    stdDirect1(i) = nanstd(r(find(th1 < m+15 & th1 > m-15)));
    valsDirection1{i} = r(find(th1 < m+15 & th1 > m-15));
end
hold on
polarscatter(deg2rad(majorAxis),meanDirect1,200,'r','filled','Marker','square')

subplot(1,2,1)
idx2 = find(msAngleAmplC > 10);
th2 = msAngleC(idx2);
th = deg2rad(th2);
r = [msAngleAmplC(idx2)];
for i = 1:length(majorAxis)
    m = majorAxis(i);
    meanDirect(i) = nanmean(r(find(th2 < m+15 & th2 > m-15)));
    stdDirect(i) = nanstd(r(find(th2 < m+15 & th2 > m-15)));
    valsDirection{i} = r(find(th2 < m+15 & th2 > m-15));
end
polarscatter(th,r,'b','filled','MarkerFaceAlpha',.2)
hold on
polarscatter(deg2rad(majorAxis),meanDirect,200,'r','filled','Marker','square')
title('Controls')

saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAmplitudeandAngle.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAmplitudeandAngle.epsc');


for i = 1:length(majorAxis)
[h,p(i)] = ttest2(valsDirection1{i},valsDirection{i});
end

figure;
polarwitherrorbar(deg2rad([majorAxis majorAxis(1)]),[meanDirect1 meanDirect1(1)],...
    [stdDirect1 stdDirect1(1)]);
title('patient')
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAmplitudeandAnglePatient.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAmplitudeandAnglePatient.epsc');

figure;
polarwitherrorbar(deg2rad([majorAxis majorAxis(1)]),[meanDirect meanDirect(1)],...
    [stdDirect stdDirect(1)]);
title('control')
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAmplitudeandAngleControl.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSAmplitudeandAngleControl.epsc');

%,...
 %   'Color','b')
% hold on
% polarplot(deg2rad([majorAxis majorAxis(1)]),[meanDirect meanDirect(1)],...
%     'Color','r')

% polarplot(theta,rho)
% polarscatter(deg2rad(majorAxis),meanDirect1,...
%     200,'g','filled')
% hold on
% polarscatter(deg2rad(majorAxis),meanDirect,...
%     200,'b','filled')

%%% Histograms
for ii = 1:length(subjectsAll) 
    idx2 = msAllAnalysis.fixation{ii}.msAmpCorrected >= 3;
    prlInd = [prl.x(ii), prl.y(ii)]; 
    if control(ii) == 0     
        startMS = [rotatedAligned.msSfixation{ii}(idx2,1),...
            rotatedAligned.msSfixation{ii}(idx2,2)];
        endMS = [rotatedAligned.msEfixation{ii}(idx2,1),...
            rotatedAligned.msEfixation{ii}(idx2,2)];
        
        for i = 1:length(startMS)
            prlDist(ii).start(i) = pdist([startMS(i,:);prlInd],...
                'euclidean');
            prlDist(ii).end(i) = pdist([endMS(i,:);prlInd], 'euclidean');
            centerDist(ii).start(i) = pdist([startMS(i,:); 0,0],'euclidean');
            centerDist(ii).end(i) = pdist([endMS(i,:); 0,0],'euclidean');
        end
    else
        startMS = [msAllAnalysis.fixation{ii}.msAllCleanS(1,idx2)',...
        msAllAnalysis.fixation{ii}.msAllCleanS(2,idx2)'];
        endMS = [msAllAnalysis.fixation{ii}.msAllCleanE(1,idx2)',...
        msAllAnalysis.fixation{ii}.msAllCleanE(2,idx2)'];
        for i = 1:length(startMS)
            prlDist(ii).start(i) = pdist([startMS(i,:);prlInd],...
                'euclidean');
            prlDist(ii).end(i) = pdist([endMS(i,:);prlInd], 'euclidean');
            centerDist(ii).start(i) = pdist([startMS(i,:); 0,0],'euclidean');
            centerDist(ii).end(i) = pdist([endMS(i,:); 0,0],'euclidean');
        end
    end
end
allPLFHistC = []; allCenterHistC = [];
allPLFHistP = []; allCenterHistP = [];
for ii = 1:length(subjectsAll)
    if control(ii)
        allPLFHistC = [allPLFHistC prlDist(ii).start-prlDist(ii).end];
        allCenterHistC = [allCenterHistC centerDist(ii).start-centerDist(ii).end]; 
%         forhistogram.plf{ii} = allPLFHistC;
    else
        allPLFHistP = [allPLFHistP prlDist(ii).start-prlDist(ii).end];
        allCenterHistP = [allCenterHistP centerDist(ii).start-centerDist(ii).end];
%         forhistogram.plf{ii} = allPLFHistP;
    end
    deltaPLF(ii) = median(prlDist(ii).start-prlDist(ii).end);
    deltaCenter(ii) = median(centerDist(ii).start-centerDist(ii).end);
    forhistogram.plf{ii} = (prlDist(ii).start-prlDist(ii).end);
    forhistogram.center{ii} = (centerDist(ii).start-centerDist(ii).end);
end

figure;
plot(control,deltaPLF,'o');
hold on
plot(control+2,deltaCenter,'o');
ylim([-1 1])
xlim([-.25 3.25])
xticks([0 1 2 3])
xticklabels({'PT, PLF','CT, PLF','PT, 0,0','CT, 0,0'});
[h,p] = ttest2(deltaPLF(find(control)),deltaPLF(find(~control)));
ylabel('Median Distance of Start-End');

figure;
subplot(1,2,1)
[counts,bins] = hist(allPLFHistC); %# get counts and bin locations
barh(bins,counts)
hold on
[counts,bins] = hist(allPLFHistP); %# get counts and bin locations
barh(bins,counts)
subplot(1,2,2)
[counts,bins] = hist(allCenterHistC); %# get counts and bin locations
barh(bins,counts)
hold on
[counts,bins] = hist(allCenterHistP); %# get counts and bin locations
barh(bins,counts)
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSStart-LandPLFCenter.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSStart-LandPLFCenter.epsc');

[h,p] = ttest(allPLFHistP, allCenterHistP);
[h,p] = ttest(allPLFHistC, allCenterHistC);

[h,p] = ttest2(allPLFHistP > 0,allPLFHistC > 0);
[h,p] = ttest2(allCenterHistP < 0,allCenterHistC < 0);


figure;
[pY_FullFixation1, meanValsCL1, meanValsPT1, meansValsIndY1] = histogramNormalizedForRotation(subjectsAll, control, ...
    forhistogram.plf, forhistogram.plf, 2, 0); %x = 1 or y = 2
xlabel('Start-End Dist from PLF')

[pY_FullFixation2, meanValsCL2, meanValsPT2, meansValsIndY2] = histogramNormalizedForRotation(subjectsAll, control, ...
    forhistogram.center, forhistogram.center, 2, 0); %x = 1 or y = 2
xlabel('Start-End Dist from Center')

[h,p] = ttest(meansValsIndY1(find(~control)), meansValsIndY2(find(~control)));

%% Individuals Amplitude MS Histograms
% % task
temp = []; temp2 = [];
for ii = 1:length(subjectsAll)
    if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
        continue;
    end
    if control(ii) 
        temp = [temp msAllAnalysis.task{ii}.msAmplitude];
    else
        temp2 = [temp2 msAllAnalysis.task{ii}.msAmplitude];
    end
end
figure;
subplot(1,2,1)
leg(1) = histogram(temp2, 60);
hold on
leg(2) = histogram(temp, 60);
xlim([-2 30])
legend(leg,{'Patient','Control'})
title('Task')
xlabel('Amplitude (arcmin)')
  % % fixation
temp = [];temp2 = [];
for ii = 1:length(subjectsAll)
    if control(ii)
        temp = [temp msAllAnalysis.fixation{ii}.msAmpCorrected];
    else
        temp2 = [temp2 msAllAnalysis.fixation{ii}.msAmpCorrected];
    end
end
% figure;
subplot(1,2,2)
leg(1) = histogram(temp2, 60);
hold on
leg(2) = histogram(temp, 60);
xlim([-2 30])
legend(leg,{'Patient','Control'})
title('Fixation')
xlabel('Amplitude (arcmin)')

%% %% Individuals Amplitude Saccade Histograms
  % % fixation
temp = [];temp2 = [];
for ii = 1:length(subjectsAll)
    if control(ii)
        temp = [temp sAllAnalysis.fixation{ii}.sAmpCorrected];
    else
        temp2 = [temp2 sAllAnalysis.fixation{ii}.sAmpCorrected];
    end
end
figure;
% subplot(1,2,2)
leg(1) = histogram(temp2/60, 60);
hold on
leg(2) = histogram(temp/60, 60);
% xlim([-2 30])
legend(leg,{'Patient','Control'})
title('Fixation')
xlabel('Amplitude (deg)')

%% % Microsaccade Landing Position With Starts at 0
figure;
% subplot(2,2,1)
temp1 = []; temp11 = [];
for ii = find(control)
    temp1 = [temp1 msAllAnalysis.task{ii}.msAllXCorrected];
    temp11 = [temp11 msAllAnalysis.task{ii}.msAllYCorrected];
end
ctl = ones(1,length(temp1));

temp2 = []; temp22 = [];
for ii = find(~control)
    if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
        continue;
    end
    temp2 = [temp2 msAllAnalysis.task{ii}.msAllXCorrected];
    temp22 = [temp22 msAllAnalysis.task{ii}.msAllYCorrected];
end
pt = zeros(1,length(temp2));

temp3 = []; temp33 = [];
for ii = find(~control)
    if strcmp(subjectsAll{ii}, 'HUX5') || strcmp(subjectsAll{ii}, 'HUX27')
        continue;
    end
    temp3 = [temp3 rotatedAligned.task{ii}(:,1)'];
    temp33 = [temp33 rotatedAligned.task{ii}(:,2)'];
end
pt2 = ones(1,length(temp3))*2;

idxControl = [ctl pt pt2];
scatterhist([temp1 temp2 temp3], ...
         [temp11 temp22 temp33],...
         'Group',idxControl,'Kernel','on','Location','SouthWest',...
            'Direction','out');
axis square
title('PT, CT, PTRot');
suptitle('Task');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSLandingLocScatterTask.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSLandingLocScatterTask.epsc');

%%% fixation
figure;
% subplot(2,2,1)
temp1 = []; temp11 = [];
for ii = find(control)
    temp1 = [temp1 msAllAnalysis.fixation{ii}.msAllXCorrected];
    temp11 = [temp11 msAllAnalysis.fixation{ii}.msAllYCorrected];
end
ctl = ones(1,length(temp1));

temp2 = []; temp22 = [];
for ii = find(~control)
    temp2 = [temp2 msAllAnalysis.fixation{ii}.msAllXCorrected];
    temp22 = [temp22 msAllAnalysis.fixation{ii}.msAllYCorrected];
end
pt = zeros(1,length(temp2));

temp3 = []; temp33 = [];
for ii = find(~control)
    temp3 = [temp3 rotatedAligned.fix{ii}(:,1)'];
    temp33 = [temp33 rotatedAligned.fix{ii}(:,2)'];
end
pt2 = ones(1,length(temp3))*2;

idxControl = [ctl pt pt2];
scatterhist([temp1 temp2 temp3], ...
         [temp11 temp22 temp33],...
         'Group',idxControl,'Kernel','on','Location','SouthWest',...
            'Direction','out');
axis square
title('PT, CT, PTRot');
suptitle('Fixation');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSLandingLocScatterFix.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/MSLandingLocScatterFix.epsc');

%%% Saccade Fix
figure;
% subplot(2,2,1)
temp1 = []; temp11 = [];
for ii = find(control)
    temp1 = [temp1 sAllAnalysis.fixation{ii}.sAllXCorrected];
    temp11 = [temp11 sAllAnalysis.fixation{ii}.sAllYCorrected];
end
ctl = ones(1,length(temp1));

temp2 = []; temp22 = [];
for ii = find(~control)
    temp2 = [temp2 sAllAnalysis.fixation{ii}.sAllXCorrected];
    temp22 = [temp22 sAllAnalysis.fixation{ii}.sAllYCorrected];
end
pt = zeros(1,length(temp2));

temp3 = []; temp33 = [];
for ii = find(~control)
    temp3 = [temp3 rotatedAligned.sfix{ii}(:,1)'];
    temp33 = [temp33 rotatedAligned.sfix{ii}(:,2)'];
end
pt2 = ones(1,length(temp3))*2;

idxControl = [ctl pt pt2];
scatterhist([temp1 temp2 temp3], ...
         [temp11 temp22 temp33],...
         'Group',idxControl,'Kernel','on','Location','SouthWest',...
            'Direction','out');
axis square
title('PT, CT, PTRot');
axis([-500 500 -500 500])
suptitle('Fixation');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/SLandingLocScatterFix.png');
saveas(gcf,'../../SummaryDocuments/huxpatientsummaries/LaTex/Overleaf_StrokeStudy/figures/SLandingLocScatterFix.epsc');


     %% MS HeatMaps
% % %     figure;
% % % subplot(2,2,1)
% % %     tempx = [];
% % %     tempy = [];
% % %     for ii = find(control)
% % %         tempx = [tempx msTask{ii}.msEnd(1,:) - msTask{ii}.msStart(1,:)];%, ...
% % %         tempy = [tempy  msTask{ii}.msEnd(2,:) - msTask{ii}.msStart(2,:)];
% % %     end
% % %      generateHeatMapSimple( ...
% % %         tempx(find(tempx < 31 & tempy < 30)), ...
% % %         tempy(find(tempx < 31 & tempy < 30)), ...
% % %         'Bins', 20,...
% % %         'StimulusSize', 10,...
% % %         'AxisValue', 20,...
% % %         'Uncrowded', 0,...
% % %         'Borders', 1);
% % %     ylabel('Task');
% % % 
% % %  subplot(2,2,2)
% % %     tempx = [];
% % %     tempy = [];
% % %     for ii = find(~control)
% % %          if ii == 3 || ii == 23
% % %             continue;
% % %         end
% % %         tempx = [tempx msTask{ii}.msEnd(1,:) - msTask{ii}.msStart(1,:)];%, ...
% % %         tempy = [tempy  msTask{ii}.msEnd(2,:) - msTask{ii}.msStart(2,:)];
% % %     end
% % %      generateHeatMapSimple( ...
% % %         tempx(find(tempx < 31 & tempy < 30)), ...
% % %         tempy(find(tempx < 31 & tempy < 30)), ...
% % %         'Bins', 20,...
% % %         'StimulusSize', 10,...
% % %         'AxisValue', 20,...
% % %         'Uncrowded', 0,...
% % %         'Borders', 1);
% % % 
% % % subplot(2,2,3)
% % %     tempx = [];
% % %     tempy = [];
% % %     for ii = find(control)
% % %         for i = 1:length(msFix{ii}.MicroSaccEndLoc)
% % %             tempx = [tempx msFix{ii}.MicroSaccEndLoc(i).x - msFix{ii}.MicroSaccStartLoc(i).x];%, ...
% % %             tempy = [tempy msFix{ii}.MicroSaccEndLoc(i).y - msFix{ii}.MicroSaccStartLoc(i).y];
% % %         end
% % %     end
% % %      generateHeatMapSimple( ...
% % %         tempx, ...
% % %         tempy, ...
% % %         'Bins', 20,...
% % %         'StimulusSize', 10,...
% % %         'AxisValue', 20,...
% % %         'Uncrowded', 0,...
% % %         'Borders', 1);
% % % 
% % %     
% % % subplot(2,2,4)
% % %     for ii = find(~control)
% % %         if ii == 3 || ii == 23
% % %             continue;
% % %         end
% % %         for i = 1:length(msFix{ii}.MicroSaccEndLoc)
% % %             plot(msFix{ii}.MicroSaccEndLoc(i).x - msFix{ii}.MicroSaccStartLoc(i).x, ...
% % %                 msFix{ii}.MicroSaccEndLoc(i).y - msFix{ii}.MicroSaccStartLoc(i).y,...
% % %                 'o','Color','b');
% % %             hold on
% % %         end
% % %     end
% % %     axis([-60 60 -60 60]); axis square;
% % %     