function distance = getPRLFromTargetEdge(stimuliSize, x, y)

% figure;

x = mean(x);
y = mean(y);

width = stimuliSize/2;
height = (5 * stimuliSize)/2;

% hold on;
% leg = plot(x,y,'o','LineWidth',10);
% color = leg.Color;
% 
% centerTarget = (rectangle('Position',[-stimuliSize/2 ...
%     (-stimuliSize*5)/2 ...
%     (stimuliSize) ...
%     (5*stimuliSize)],'EdgeColor',color,...
%     'LineWidth',3));
% axis([-5 5 -5 5])


posX = abs(x);
posY = abs(y);

leg = plot(posX,posY,'o','LineWidth',10);
color = leg.Color;

hold on;
rectangle('Position',[0 ...
    0 ...
    width ...
    height], ...
    'EdgeColor',color,...
    'LineWidth',3)
axis([0 6 0 6])

if x <= width && y <= height
    distance = 0;
else
    distance = (sqrt((width-x)^2+(height-y)^2));
end


% norm(y-x,2)
%if distance is the same as x or y, then PRL is on target
% if x == distance
%     distance = 0;
% end
% if y == distance
%     distance = 0;
% end

end