function [targetEccentricity,correct,dataUncrowded,targetStrokewidth,subjectData,pixelAngle,nBoots,ip,validTrials] = getCleanVariables(data, fixationTrials, params, valid)

%% Creating processed data %% 
%Removing all invalid, no response, and fixation trials

% valid = buildValidStruct(pptrials);

validTrials = ones(1, length(data.Correct));

fixationSpanCondition = (params.spanMin < data.em.span & data.em.span < params.spanMax);

defaultBadTrialsIdx = unique([find(valid.notrack == 1), find(valid.blink == 1),  find(valid.tooshort == 1), ...
    find(valid.s == 1), find(valid.noresponse == 1), find(~fixationSpanCondition), fixationTrials]);
%%%%%% DRIFT & MICROSACCADE %%%%%%
if (params.DMS == true)
    badTrials = defaultBadTrialsIdx;       
end
%%%%%% MICROSACCADE %%%%%%
if (params.MS == true)
    badTrials = unique([defaultBadTrialsIdx, find(valid.ms == 0)]);        
end
%%%%%% DRIFT %%%%%%
if (params.D == true)
    badTrials = unique([defaultBadTrialsIdx, find(valid.ms == 1)]); 
end

validTrials(:, badTrials) = 0;
targetEccentricity = data.TargetEccentricity;
targetEccentricity(:,badTrials) = NaN;
correct = data.Correct;
correct(:,badTrials) = NaN;
dataUncrowded = data.Uncrowded;
dataUncrowded(:,badTrials) = NaN;
targetStrokewidth = data.TargetStrokewidth;
targetStrokewidth(:,badTrials) = NaN;
subjectData = data.Subject;
pixelAngle = params.pixelAngle;
nBoots = params.nBoots;
ip = params.imagePath;

end

