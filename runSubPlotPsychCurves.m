%runSubPlotPsychCurves
%% create subplot of psych curves

subjectAll = {'Z023','Ashley','Z005','Z002','Z013',...
    'Z024','Z064','Z046','Z084','Z014',...
    'Z091','Z138','Z181'};%,...
% subjectAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,...
% subjectAll = {'Z023','Ashley','Z005'}; %%Z002
%weirdos Z048, Z070, Z090

% subjectAll = {'Z090'};

condition = {'Uncrowded','Crowded'}; %Figure 1 and Figure 2
stabilization = {'Unstabilized'};
em = {'Drift'};
ecc = {'0ecc'};
counter = 1;
% c = brewermap(12,'Dark2');
% c = brewermap(length(subjectAll),'Paired');
% ctemp = flip(brewermap(length(subjectAll),'RdYlBu'));
ctemp = gray(length(subjectAll)+20);

% figure;
for ii = 1:length(subjectAll)
    titleFigs{ii} = sprintf('%s_%s_%s_%s_%s', subjectAll{ii}, condition{1}, stabilization{1}, em{1}, ecc{1});
%     titleFigs{ii} = sprintf('NoToss%s_%s_%s_%s_%s', subjectAll{ii}, condition, stabilization{1}, em{1}, ecc{1});
    
    pathToData = sprintf('../../Data/%s/Graphs/FIG', subjectAll{ii}); %File Location
%     pathToData = ('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\AllSubjTogether'); %File Location
    color = ctemp(ii,:);

    fH{ii} = createGraphTogetherMultipleSubj...
        (titleFigs{ii},pathToData,color);
    xlim([0 5.5]);

    ax = gca;
    figs{ii} = get(ax,'children'); %get handle to all the children in the figure
%     if length(condition) > 1
%         hold on
%         titleFigs{ii} = sprintf('%s_%s_%s_%s_%s', subjectAll{ii}, condition{2}, stabilization{1}, em{1}, ecc{1});
%         %     titleFigs{ii} = sprintf('NoToss%s_%s_%s_%s_%s', subjectAll{ii}, condition, stabilization{1}, em{1}, ecc{1});
%         
%         pathToData = sprintf('../../Data/%s/Graphs/FIG', subjectAll{ii}); %File Location
%         %     pathToData = ('C:\Users\Ruccilab\OneDrive - University of Rochester\Documents\Crowding\Data\AllSubjTogether'); %File Location
%         color = [1 0 0];
%         
%         fH{ii} = createGraphTogetherMultipleSubj...
%             (titleFigs{ii},pathToData,color);
%     end

end

figure('Position',[100 10 1000 1200]);
for ii = 1:length(subjectAll)
    s{ii} = subplot(4,4,ii);
%     fig1 = get(ax{ii},'children'); %get handle to all the children in the figure
    copyobj(figs{ii},s{ii});
    xlim([0 5.5]);
    ylim([0 1]);
    if ii == 1
        xlabel('Strokewidth (arcmin)');
        ylabel('Proportion Correct');
    end
end


 saveas(gcf, ...
        sprintf('../../SummaryDocuments/ManuscriptDocs/Manuscript.FovealCrowding.Clark2020/OverleafDocs_Crowding1/figures/%sPsychCurves.png',...
        condition{1}));









