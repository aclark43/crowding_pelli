function [sRates, msRates] = buildRates(pptrials)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

sRates = cellfun(@(z) rateCalculation(z, 's'), pptrials);
msRates = cellfun(@(z) rateCalculation(z, 'ms'), pptrials);


end

