% function [percentOnTarg, infoOnTarg, numTrials, swThreshold] = distanceFromTarget(subjectCond, ...
%     subjectThreshCondition, subjectsAll, ~, ~, ~)
function [percentOnTarg, infoOnTarg, numTrials] = distanceFromTarget(subjectCond, ...
    subjectThreshCondition, subjectsAll, condition, ~, ~, thresholdSWVals)
%Looks at each point in drift to see how far away it is from the center of
%the target. Used drift only trials.
%   percentage of the time spent on target (larger than target)
for ii = 1:length(subjectsAll)
    if strcmp('Ashley',subjectsAll(ii)) || ...
            strcmp('Z013',subjectsAll(ii)) ||...
            strcmp('Z023',subjectsAll(ii)) ||...
            strcmp('Z002',subjectsAll(ii)) ||...
            strcmp('Z005',subjectsAll(ii))
        subjectThreshCondition(ii).machine = 1;
    else
        subjectThreshCondition(ii).machine = 2;
    end
end

lessCount = 1;
moreCount = 1;
finalTable = [];
for ii = 1:length(subjectsAll)
    for swIdx = 1:length(subjectCond(ii).SW)
        strokeValue = subjectCond(ii).SW(swIdx);
        strokeWidth = sprintf('strokeWidth_%i',strokeValue);
        if strokeValue == 0
            continue
        end
        uEcc = 0;
        threshName = sprintf('thresholdSW%s',cell2mat(condition));
        stimSize = subjectCond(ii).stimulusSize(swIdx);
        centerTarget = (rectangle('Position',[(-stimSize)+uEcc ...
            (-stimSize*5) ...
            (2*stimSize) ...
            (10*stimSize)]));
        if strcmp('Crowded',condition)
            width = 2 * stimuliSize;
            height = width * 5;
            centerX = (-stimuliSize+arcminEcc);
            centerY = (-stimuliSize*5);
            
            rightF = rectangle('Position',[-(width + (width * 1.4)) + arcminEcc, centerY, width, height],'LineWidth',1);
            pointR = bbox2points(rightF.Position);
            
            leftF = rectangle('Position',[(width * 1.4) + arcminEcc, centerY, width, height], 'LineWidth',1); %Left
            pointL= bbox2points(leftF.Position);
            
            topF =  rectangle('Position',[centerX, (height * 1.4), width, height], 'LineWidth',1); %Top
            pointT= bbox2points(topF.Position);
            
            bottomF = rectangle('Position',[centerX, -(height + (height * 1.4)), width, height], 'LineWidth',1); % Bottom
            pointB= bbox2points(bottomF.Position);
        end
        axis([-20 20 -20 20])
        figure;
        pointsCenterTarget = bbox2points(centerTarget.Position);
        close
        allPositions = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).position;
        numTrials(ii) = length(allPositions);
%         smallerSW = sprintf('strokeWidth_%i',...
%             sscanf(thresholdSWVals(ii).(threshName),'strokeWidth_%d')-1);
%         smallerSW = sprintf('strokeWidth_%i',...
%             sscanf(thresholdSWVals(ii).(threshName),'strokeWidth_%d')+1);
        smallerSW = sprintf('strokeWidth_%i',...
            sscanf(thresholdSWVals(ii).(threshName),'strokeWidth_%d'));
        
        if strcmp(strokeWidth, smallerSW)
                fprintf('%s \n', smallerSW);
        end
        
            for xyIdx = 1:numTrials(ii)
                if subjectThreshCondition(ii).machine == 2%(allPositions(xyIdx).x) < 500
                    lengthPos = 500/(1000/330);
                else
                    lengthPos = 500;
                end
                allX(xyIdx,:) = allPositions(xyIdx).x(1:lengthPos);
                allY(xyIdx,:) = allPositions(xyIdx).y(1:lengthPos);
                
                if strcmp(strokeWidth, smallerSW)
                    flankersAll = num2str(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).flankers(xyIdx));
                    flankL = str2double(flankersAll(1));
                    flankR = str2double(flankersAll(2));
                    flankB = str2double(flankersAll(3));
                    flankT = str2double(flankersAll(4));
                    %                 targetNum = double(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).target(xyIdx));
                    resp =  double(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).response(xyIdx));
                    
                    onTargetTrial = determinePointsOnBox(allX,xyIdx,pointsCenterTarget);   
                    percentOnTarg.byTrial.onTarget{ii,xyIdx} = num2str(round(numel(onTargetTrial)/lengthPos, 4)*100, '%.2f');
                    percentOnTarg.byTrial.correctTarg{ii,xyIdx} = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).correct(xyIdx);
                    
                    if strcmp('Crowded',condition)
                        rightFlanker = determinePointsOnBox(allX,xyIdx,pointR);
                        percentOnTarg.byTrial.percentR{ii,xyIdx} = num2str(round(numel(rightFlanker)/numel(allX(xyIdx,:)), 4)*100, '%.2f');
                        if resp == flankR
                            percentOnTarg.byTrial.correctR{ii,xyIdx} = 1;
                        else
                            percentOnTarg.byTrial.correctR{ii,xyIdx} = 0;
                        end
                        leftFlanker = determinePointsOnBox(allX,xyIdx,pointL);
                        percentOnTarg.byTrial.percentL{ii,xyIdx} = num2str(round(numel(leftFlanker)/numel(allX(xyIdx,:)), 4)*100, '%.2f');
                        if resp == flankL
                            percentOnTarg.byTrial.correctL{ii,xyIdx} = 1;
                        else
                            percentOnTarg.byTrial.correctL{ii,xyIdx} = 0;
                        end
                        
                        topFlanker = determinePointsOnBox(allX,xyIdx,pointT);
                        percentOnTarg.byTrial.percentT{ii,xyIdx} = num2str(round(numel(topFlanker)/numel(allX(xyIdx,:)), 4)*100, '%.2f');
                        if resp == flankT
                            percentOnTarg.byTrial.correctT{ii,xyIdx} = 1;
                        else
                            percentOnTarg.byTrial.correctT{ii,xyIdx} = 0;
                        end
                        
                        bottomFlanker = determinePointsOnBox(allX,xyIdx,pointB);
                        percentOnTarg.byTrial.percentB{ii,xyIdx} = num2str(round(numel(bottomFlanker)/numel(allX(xyIdx,:)), 4)*100, '%.2f');
                        if resp == flankB
                            percentOnTarg.byTrial.correctB{ii,xyIdx} = 1;
                        else
                            percentOnTarg.byTrial.correctB{ii,xyIdx} = 0;
                        end
                    end
                end
            end

        onTarget = find(allX > pointsCenterTarget(1,1) & ...
            allX < pointsCenterTarget(2,1) & ...
            allY > pointsCenterTarget(1,2) & ...
            allY < pointsCenterTarget(3,2));
        if strcmp('Crowded',condition)
            rightFlanker = find(allX > pointR(1,1) & ...
                allX < pointR(2,1) & ...
                allY > pointR(1,2) & ...
                allY < pointR(3,2));
            percentOnTarg.percentR(ii).(strokeWidth) = num2str(round(numel(rightFlanker)/numel(allX), 4)*100, '%.2f');
            
            leftFlanker = find(allX > pointL(1,1) & ...
                allX < pointL(2,1) & ...
                allY > pointL(1,2) & ...
                allY < pointL(3,2));
            percentOnTarg.percentL(ii).(strokeWidth) = num2str(round(numel(leftFlanker)/numel(allX), 4)*100, '%.2f');
            
            topFlanker = find(allX > pointT(1,1) & ...
                allX < pointT(2,1) & ...
                allY > pointT(1,2) & ...
                allY < pointT(3,2));
            percentOnTarg.percentT(ii).(strokeWidth) = num2str(round(numel(topFlanker)/numel(allX), 4)*100, '%.2f');
            
            bottomFlanker = find(allX > pointB(1,1) & ...
                allX < pointB(2,1) & ...
                allY > pointB(1,2) & ...
                allY < pointB(3,2));
            percentOnTarg.percentB(ii).(strokeWidth) = num2str(round(numel(bottomFlanker)/numel(allX), 4)*100, '%.2f');
            
        end        
        roundMat = num2str(round(numel(onTarget)/numel(allX), 4)*100, '%.2f');
        
        percentOnTarg.percent(ii).(strokeWidth) = roundMat;
        percentOnTarg.perform(ii).(strokeWidth) = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).performanceAtSize;
        percentOnTarg.sizeStim(ii).(strokeWidth) = stimSize;
        percentOnTarg.span(ii).(strokeWidth) = mean(double(subjectThreshCondition(ii).em.ecc_0.(strokeWidth).span));
        
        for trialIdx = 1:numTrials(ii)
            onTargetForTrial = find(allX(trialIdx,:) > pointsCenterTarget(1,1) & ...
                allX(trialIdx,:) < pointsCenterTarget(2,1) & ...
                allY(trialIdx,:) > pointsCenterTarget(1,2) & ...
                allY(trialIdx,:) < pointsCenterTarget(3,2));
            
            percTrialTarg = length(onTargetForTrial)/lengthPos;
            corTrialTarg = subjectThreshCondition(ii).em.ecc_0.(strokeWidth).correct(:)';
            
            
            if strcmp('Crowded',condition)
                if strcmp('Z023',subjectsAll{ii})
                    propLess = .25;
                    propMore = .6;
                elseif strcmp('Ashley',subjectsAll{ii})
                    propLess = .3;
                    propMore = .8;
                elseif strcmp('Z005',subjectsAll{ii})
                    propLess = .2;
                    propMore = .5;
                elseif strcmp('Z002',subjectsAll{ii})
                    propLess = .01;
                    propMore = .5;
                elseif strcmp('Z013',subjectsAll{ii})
                    propLess = .3;
                    propMore = .4;
                elseif strcmp('Z024',subjectsAll{ii})
                    propLess = .01;
                    propMore = .5;
                elseif strcmp('Z064',subjectsAll{ii})
                    propLess = .02;
                    propMore = .4;
                elseif strcmp('Z046',subjectsAll{ii})
                    propLess = .2;
                    propMore = .3;
                elseif strcmp('Z084',subjectsAll{ii})
                    propLess = .01;
                    propMore = .8;
                elseif strcmp('Z014',subjectsAll{ii})
                    propLess = .1;
                    propMore = .2;
                end
            elseif strcmp('Uncrowded',condition)
                if strcmp('Z023',subjectsAll{ii})
                    propLess = .005;
                    propMore = .5;
                elseif strcmp('Ashley',subjectsAll{ii})
                    propLess = .3;
                    propMore = .4;
                elseif strcmp('Z005',subjectsAll{ii})
                    propLess = .05;
                    propMore = .4;
                elseif strcmp('Z002',subjectsAll{ii})
                    propLess = .01;
                    propMore = .2;
                elseif strcmp('Z013',subjectsAll{ii})
                    propLess = .2;
                    propMore = .3;
                elseif strcmp('Z024',subjectsAll{ii})
                    propLess = .01;
                    propMore = .5;
                elseif strcmp('Z064',subjectsAll{ii})
                    propLess = .02;
                    propMore = .3;
                elseif strcmp('Z046',subjectsAll{ii})
                    propLess = .2;
                    propMore = .3;
                elseif strcmp('Z084',subjectsAll{ii})
                    propLess = .01;
                    propMore = .5;
                elseif strcmp('Z014',subjectsAll{ii})
                    propLess = .1;
                    propMore = .2;
                end
            end
            if strcmp(thresholdSWVals(ii).(threshName),strokeWidth)
                if percTrialTarg < propLess
                    vectorLessFif(lessCount) = percTrialTarg;
                    vectorLessFifCor(lessCount) = corTrialTarg(trialIdx);
                    lessCount = lessCount + 1;
                elseif percTrialTarg > propMore
                    vectorMoreFif(moreCount) = percTrialTarg;
                    vectorMoreFifCor(moreCount) = corTrialTarg(trialIdx);
                    moreCount = moreCount + 1;
                end
            end
        end
        
    end
    infoOnTarg(ii).numTrialsLess = lessCount;
    infoOnTarg(ii).numTrialsMore = moreCount;
    percCorLess(ii) = mean(vectorLessFifCor);%sum(vectorLessFifCor)/(length(vectorLessFifCor));
    percCorMore(ii) = mean(vectorMoreFifCor);%sum(vectorMoreFifCor)/(length(vectorMoreFifCor));
    
    infoOnTarg(ii).perCorLess = percCorLess(ii);
    infoOnTarg(ii).perCorMore = percCorMore(ii);
    
    vectorMoreFifCor = [];
    vectorLessFifCor = [];
    lessCount = 1;
    moreCount = 1;
    
    currentTable = {ii};
    finalTable = [finalTable, currentTable];
    clear allX
    clear allY
    infoOnTarg(ii).mean = mean(percentOnTarg.percent(ii).(strokeWidth(:)));
    infoOnTarg(ii).std = std(percentOnTarg.percent(ii).(strokeWidth(:)));
    
    
end
end

