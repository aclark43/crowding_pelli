function [dataStruct, params] = loadVariablesComplete(params)

%% Which color is which subject
figure;
for ii = 1:params.subNum
    plot(ii,ii,'o','Color','k','MarkerFaceColor',params.c(ii,:),'MarkerSize',10);
    hold on
end
xticklabels(params.subjectsAll);

%% Load Variables
condition = {'Uncrowded','Crowded'};

[dataStruct.subjectUnc, dataStruct.subjectThreshUnc, dataStruct.subjectMSUnc, dataStruct.performU] = ...
    loadVariables(params.subjectsAll, {condition{1}}, params, 0, '0ecc');

[dataStruct.subjectCro, dataStruct.subjectThreshCro, dataStruct.subjectMSCro, dataStruct.performC ] = ...
    loadVariables(params.subjectsAll, {condition{2}}, params, 0, '0ecc');

%% Setting Closest Value to Threshold
dataStruct.thresholdSWVals = determineThresholdSW(dataStruct.subjectUnc,dataStruct.subjectThreshUnc,...
    dataStruct.subjectThreshCro, dataStruct.subjectCro, params.subNum, params.subjectsAll);
% thresholdSWVals = determineThresholdSW(subjectUnc,performU,...
%     performC, subjectCro, params.subNum,subjectsAll);
subjectsAll = params.subjectsAll;

for ii = 1:params.subNum
    if strcmp('Z023',subjectsAll{ii}) || strcmp('Ashley',subjectsAll{ii}) ||...
            strcmp('Z005',subjectsAll{ii}) || strcmp('Z002',subjectsAll{ii}) ||...
            strcmp('Z013',subjectsAll{ii})
        params.machine(ii) = 1; %DPI
        params.conversionFac(ii) = 1;
        params.sampling(ii) = 1000;
%     elseif strcmp('Z024',subjectsAll{ii}) || strcmp('Z064',subjectsAll{ii}) ||...
%             strcmp('Z046',subjectsAll{ii}) || strcmp('Z084',subjectsAll{ii}) ||...
%             strcmp('Z014',subjectsAll{ii})
    else
        params.machine(ii) = 2; %dDPI
        params.conversionFac(ii) = 1000/330;
        params.sampling(ii) = 341;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end