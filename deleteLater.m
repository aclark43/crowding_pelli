clear all
clc

params.fig = struct('FIGURE_ON', 0);
fig = params.fig;
subjectsAll = {'Z023','Ashley','Z005','Z002','Z013','Z024','Z064','Z046','Z084','Z014'};%,...
%     'AshleyDDPI','Z002DDPI','Z046DDPI','Z084DDPI'};%,'Z084','Z014'}; %Drift Only Subjects,'Z084'
% subjectsAll = {'AshleyDDPI','Z002DDPI','Z046','Z084'};
% subjectsAll = {'Z024'};
%     'Z013','Z024','Z064','Z046','Z084','Z014','Z063','Z070'};

params.em = {'Drift'};
em = params.em;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Starting Analysis %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
params.subjectsAll = subjectsAll;
params.subNum = length(subjectsAll);
subNum = params.subNum;
c = jet(12);
% c = jet(length(subjectsAll));
params.c = c;


condition = {'Uncrowded','Crowded'};

[subjectUnc, subjectThreshUnc, subjectMSUnc, performU] = loadVariables(subjectsAll, {condition{1}}, params.em, 0, '0eccEcc');
[subjectCro, subjectThreshCro, subjectMSCro, performC ] = loadVariables(subjectsAll, {condition{2}}, params.em, 0, '0eccEcc');


thresholdSWVals = determineThresholdSW(subjectUnc,subjectThreshUnc,...
    subjectThreshCro, subjectCro, params.subNum,subjectsAll);

justThreshSW = 0;
[distAllTabUnc, distAllTabCro] = ...
    buildPRLStruct(justThreshSW, params, subjectUnc, subjectThreshUnc, subjectCro, subjectThreshCro, thresholdSWVals);

justThreshSW = 1;
[distThreshTabUnc, distThreshTabCro] = ...
    buildPRLStruct(justThreshSW, params, subjectUnc, subjectThreshUnc, subjectCro, subjectThreshCro, thresholdSWVals);

%% PRL GRAPHS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
% subplot(1,2,2)
params.subNum = 10;
params.cond = {'Crowded'};
[smallCro, largeCro] = getPLFSmallLargeCondition(distAllTabCro, distThreshTabCro, params, subjectsAll);
params.cond = {'Uncrowded'};
[smallUnc, largeUnc] = getPLFSmallLargeCondition(distAllTabUnc, distThreshTabUnc, params, subjectsAll);

% small

%% Combining Crowded and Uncrowded
for ii = 1:params.subNum
    far(1,ii) = largeUnc(ii).meanPerformance;
    farPRL (1,ii) = largeUnc(ii).meanPRL;
    numTrialsF(1,ii) = length(largeUnc(ii).prlLarge);
    closer(1,ii) = smallUnc(ii).meanPerformance;
    closerPRL (1,ii) = smallUnc(ii).meanPRL;
    numTrialsC(1,ii) = length(smallUnc(ii).prlSmall);
end
for ii = 1:params.subNum
    far(2,ii) = largeCro(ii).meanPerformance;
    farPRL(2,ii) = largeCro(ii).meanPRL;
    numTrialsF(2,ii) = length(largeCro(ii).prlLarge);
    closer(2,ii) = smallCro(ii).meanPerformance;
    closerPRL(2,ii) = smallCro(ii).meanPRL;
    numTrialsC(2,ii) = length(smallCro(ii).prlSmall);
end

vecOne = ones(1,20);
vecZero = zeros(1,20);

counter = 1;
for ii = 1:params.subNum
    totalNumTrialsF(ii) = sum([numTrialsF(1,ii),numTrialsF(2,ii)]);
    totalNumTrialsC(ii) = sum([numTrialsC(1,ii),numTrialsC(2,ii)]);
    if totalNumTrialsF(ii) > 30 && totalNumTrialsC(ii) > 30
        keepIdx(ii) = 1;
        meanCloser(counter) = mean([closer(1,ii) closer(2,ii)]);
        stdCloserPerf(counter) = std([closer(1,ii) closer(2,ii)]);
        meanPRLClose(counter)= mean([closerPRL(1,ii) closerPRL(2,ii)]);
        
        meanFar(counter) = mean([far(1,ii) far(2,ii)]);
        stdFarPerf(counter) = std([far(1,ii) far(2,ii)]);
        meanPRLFar(counter) = mean([farPRL(1,ii) farPRL(2,ii)]);
        counter = counter + 1;
    else
        keepIdx(ii) = 0;
    end
end

allPRLCloseFar = ([meanPRLClose meanPRLFar]);
colorDistance = parula(length(allPRLCloseFar));
% [b,i] = sort(allPRLCloseFar);
% maxVal = max(allPRLCloseFar);
% minVal = min(allPRLCloseFar);
% figure;
% for ii = 1:length(meanCloser)
%   scatter(0, meanCloser(i(ii)),200,[colorDistance(i(ii),:)],'filled'); 
%   hold on
% end
% 
% for nii = 1:length(meanFar)
%   leg(nii) = scatter(1, meanFar(i(nii)),200,[colorDistance(i(nii+ii),:)],'filled'); 
%   hold on
% end

diff = meanPRLFar - meanPRLClose;
colorDistance = parula(length(diff));
[b,i] = sort(diff);
%% Unity line
figure;
hold on
for ii = 1:length(meanCloser)
  leg(ii) = scatter(meanCloser(ii), meanFar(ii),200,[colorDistance(i(ii),:)],'filled'); 
  hold on
end
e = errorbar(mean(meanCloser),mean(meanFar),std(meanFar),std(meanFar),std(meanCloser),std(meanCloser),'o','MarkerSize',10,...
    'MarkerEdgeColor','k','MarkerFaceColor','k')
e.Color = 'k';
e.CapSize = 15;
% errorbar(meanCloser,meanFar,stdFarPerf,stdFarPerf,stdCloserPerf,stdCloserPerf,'o')
line([0 1],[ 0 1],'Color','k')
axis([0 1 0 1])
axis square
% cb = colorbar;
% set(cb, 'Ticks', [0 .5 1], 'TickLabels', {min(diff),mean(diff), max(diff)})
xlabel('Small Gaze Position Peformance');
ylabel('Large Gaze Position Peformance');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/UnitySmallLargePLFsFewTrials.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/UnitySmallLargePLFsFewTrials.epsc');
%% 2 Scatters
figure;
for ii = 1:length(meanCloser)
  scatter(0, meanCloser(ii),200,[0 0 0],'filled'); 
  hold on
end

for nii = 1:length(meanFar)
  leg(nii) = scatter(1, meanFar(nii),200,[colorDistance(i(nii),:)],'filled'); 
  hold on
end
cb = colorbar;
set(cb, 'Ticks', [0 1], 'TickLabels', {min(diff), max(diff)})

xS = [1 0];
for ii = 1:params.subNum
    if keepIdx(ii)
    y1 = [mean([far(1,ii),far(2,ii)]) mean([closer(1,ii) closer(2,ii)])];
    points = line(xS, y1, 'Color',colorDistance(i(ii),:),'LineWidth',2);
    points.LineStyle = '--';
    end
end
idx = find(keepIdx);
hold on
% errorbar(1, mean([far(1,idx) far(2,idx)]),std([far(1,idx) far(2,idx)]), 100,'k','filled')
errorbar(1, mean([far(1,idx) far(2,idx)]),std([far(1,idx) far(2,idx)]),...
    'vertical', '-or','MarkerSize',15,...
    'MarkerEdgeColor','red',...
    'MarkerFaceColor','red',...
    'CapSize',10,...
    'LineWidth',2);

hold on
% scatter(0, mean([closer(1,idx) closer(2,idx)]), 100,'k','filled')
errorbar(0, mean([closer(1,idx) closer(2,idx)]),std([closer(1,idx) closer(2,idx)]),...
    'vertical', '-or','MarkerSize',15,...
    'MarkerEdgeColor','red',...
    'MarkerFaceColor','red',...
    'CapSize',10,...
    'LineWidth',2);
xS = [1 0];
y1 = [mean([far(1,idx) far(2,idx)]), mean([closer(1,idx) closer(2,idx)])];
line(xS, y1, 'Color','k','LineWidth',2);
xticks([0 1])
xticklabels({'Small PLFs','Large PLFs'})
ylabel('Performance')
title('Crowded and Uncrowded Combined PLFs by Performance')
xlim([-.5 1.5])
ylim([0.25 .8])
[h,p] = ttest([far(1,idx) far(2,idx)],[closer(1,idx) closer(2,idx)]);
% legend(leg(keepIdx),subjectsAll(keepIdx));
text(0,.35,sprintf('p = %.3f',p))
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SmallLargePLFsFewTrials.png');

%% Seperate Crowded and Uncrowded
clear far
clear closer
clear farPRL
clear closerPRL
for ii = 1:length(largeCro)
    far(1,ii) = largeUnc(ii).meanPerformance;
    farPRL (1,ii) = largeUnc(ii).meanPRL;
    numTrialsF(1,ii) = length(largeUnc(ii).prlLarge);
    closer(1,ii) = smallUnc(ii).meanPerformance;
    closerPRL (1,ii) = smallUnc(ii).meanPRL;
    numTrialsC(1,ii) = length(smallUnc(ii).prlSmall);
end
for ii = 1:length(largeCro)
    far(2,ii) = largeCro(ii).meanPerformance;
    farPRL(2,ii) = largeCro(ii).meanPRL;
    numTrialsF(2,ii) = length(largeCro(ii).prlLarge);
    closer(2,ii) = smallCro(ii).meanPerformance;
    closerPRL(2,ii) = smallCro(ii).meanPRL;
    numTrialsC(2,ii) = length(smallCro(ii).prlSmall);
end

vecOne = ones(1,20);
vecZero = zeros(1,20);

clear meanCloser
clear meanFar
% Uncrowded%%%%%%%%%%
counter = 1;
for ii = 1:params.subNum
    totalNumTrialsF = sum([numTrialsF(1,ii)]);
    totalNumTrialsC = sum([numTrialsC(1,ii)]);
    if totalNumTrialsF > 10 && totalNumTrialsC > 10
        keepIdx(ii) = 1;
        meanCloser(counter) = mean([closer(1,ii)]);
        meanPRLClose(counter)= mean([closerPRL(1,ii)]);
        meanFar(counter) = mean([far(1,ii)]);
        meanPRLFar(counter) = mean([farPRL(1,ii)]);
        counter = counter + 1;
    else
        keepIdx(ii) = 0;
    end
end

figure;
scatter(zeros(1,length(meanCloser)),meanCloser)
hold on
scatter(ones(1,length(meanCloser)),meanFar)
xlim([-1 2])

xS = [1 0];
for ii = 1:params.subNum
    if keepIdx(ii)
    y1 = [mean([far(1,ii)]) mean([closer(1,ii)])];
    if ii == 2 ||  ii ==4 ||  ii == 8 ||  ii == 9
        points = line(xS, y1, 'Color','g','LineWidth',.5);
    else
        points = line(xS, y1, 'Color','k','LineWidth',.5);
    end
    points.LineStyle = '--';
    end
end
title('Uncrowded')
%Crowded%%%%%%%%%
% vecOne = ones(1,20);
% vecZero = zeros(1,20);
% 
clear meanCloser
clear meanFar
counter = 1;
for ii = 1:params.subNum
    totalNumTrialsF(ii) = sum([numTrialsF(2,ii)]);
    totalNumTrialsC(ii) = sum([numTrialsC(2,ii)]);
    if totalNumTrialsF(ii) > 15 && totalNumTrialsC(ii) > 15
        keepIdx(ii) = 1;
        meanCloser(counter) = mean([closer(2,ii)]);
        meanPRLClose(counter)= mean([closerPRL(2,ii)]);
        meanFar(counter) = mean([far(2,ii)]);
        meanPRLFar(counter) = mean([farPRL(2,ii)]);
        counter = counter + 1;
    else
        keepIdx(ii) = 0;
    end
end
figure;
xS = [1 0];
for ii = 1:params.subNum
    if keepIdx(ii)
        y1 = [mean([far(2,ii)]) mean([closer(2,ii)])];
        if ii == 1 ||  ii ==4 ||  ii == 8 ||  ii == 9 
            points = line(xS, y1, 'Color','g','LineWidth',.5);
        else
            points = line(xS, y1, 'Color','k','LineWidth',.5);
        end
        points.LineStyle = '--';
    end
end

title('Crowded')
% xS = [1 0];
% for ii = 1:params.subNum
%     if keepIdx(ii)
%         y1 = [mean([far(1,ii),far(2,ii)]) mean([closer(1,ii) closer(2,ii)])];
%         if ii == 2 ||  ii ==4 ||  ii == 8 ||  ii == 9
%             points = line(xS, y1, 'Color','g','LineWidth',.7);
%         else
%             points = line(xS, y1, 'Color','k','LineWidth',.5);
%         end
%         points.LineStyle = '--';
%     end
% end

saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/SmallvsLargePLFonSameTargetSizeUncrandCroDifBins.epsc');

clear ii
