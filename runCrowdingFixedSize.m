% data.Krish = load('X:\Ashley\Crowding\Krish\Crowded_Stabilized_0ecc_sw2.mat');
% data.Z055 = load('X:\Ashley\Crowding\Z055\crowded_stabilized_0ecc.mat');
% data.Z114 = load('X:\Ashley\Crowding\Z114\stabilized_2arcminSW.mat');
clear all
clc

% Recollect for him
data.Stabilized.Z091 = load(...
    'X:\Ashley\Crowding\Z091\Crowded_Stabilized_42.mat');
data.Unstabilized.Z091 = load(...
    'X:\Ashley\Crowding\Z091\Crowded_Unstabilized_27.mat');

data.Stabilized.Krish = load(...
    'X:\Ashley\Crowding\Krish\Crowded_Stabilized_0ecc_37.mat');
data.Unstabilized.Krish = load(...
    'X:\Ashley\Crowding\Krish\Crowded_Unstabilized_0ecc_30.mat');

data.Stabilized.Z160 = load(...
    'X:\Ashley\Crowding\Z160\Crowded_Stabilized_0ecc_sw40.mat');
data.Unstabilized.Z160 = load(...
    'X:\Ashley\Crowding\Z160\Crowded_Unstabilized_0ecc_sw35.mat');

data.Stabilized.A146 = load(...
    'X:\Ashley\Crowding\A146\Crowded_Stabilized_50sw.mat');
data.Unstabilized.A146 = load(...
    'X:\Ashley\Crowding\A146\Crowded_Unstabilized_46sw2.mat');

data.Stabilized.SM = load(...
    'X:\Ashley\Crowding\SM\Crowded_Stabilized_NoPest_36sw.mat');
data.Unstabilized.SM = load(...
    'X:\Ashley\Crowding\SM\Crowded_Unstabilized_NoPest_32sw.mat');

data.Stabilized.Ben = load(...
    'X:\Ashley\Crowding\Ben\Crowded_Stabilized_46.mat');
data.Unstabilized.Ben = load(...
    'X:\Ashley\Crowding\Ben\Crowded_Unstabilized_40.mat');

data.Stabilized.Z055 = load(...
    'X:\Ashley\Crowding\Z055\Crowded_Stabilized_24.mat');
data.Unstabilized.Z055 = load(...
    'X:\Ashley\Crowding\Z055\Crowded_Unstabilized_22.mat');

% data.Stabilized.A144 = load(...
%     'X:\Ashley\Crowding\A144\Crowded_Stabilized_28.mat');
% data.Unstabilized.A144 = load(...
%     'X:\Ashley\Crowding\A144\Crowded_Unstabilized_26.mat');

% % % dataP.Stabilized.A144 = load(...
% % %     'X:\Ashley\Crowding\A144\Uncrowded_Stabilized_PEST.mat');

dataP.Unstabilized.A146 = load(...
    'X:\Ashley\Crowding\A146\Uncrowded_Unstabilized_PEST.mat');
dataP.Stabilized.A146 = load(...
    'X:\Ashley\Crowding\A146\Uncrowded_Stabilized_PEST.mat');

dataP.Stabilized.SM = load(...
    'X:\Ashley\Crowding\SM\Uncrowded_Stabilized_PEST.mat');
dataP.Unstabilized.SM = load(...
    'X:\Ashley\Crowding\SM\Uncrowded_Unstabilized_PEST.mat');

dataP.Stabilized.Z055 = load(...
    'X:\Ashley\Crowding\Z055\Uncrowded_Stabilized_PEST.mat');
dataP.Unstabilized.Z055 = load(...
    'X:\Ashley\Crowding\Z055\Uncrowded_Unstabilized_PEST.mat');

dataP.Stabilized.Z160 = load(...
    'X:\Ashley\Crowding\Z160\Uncrowded_Stabilized_0ecc_PEST.mat');
dataP.Unstabilized.Z160 = load(...
    'X:\Ashley\Crowding\Z160\Uncrowded_Unstabilized_0ecc_PEST.mat');
% % % dataP.Stabilized.Z091 = load(...
% % %     'X:\Ashley\Crowding\Z091\Uncrowded_Stabilized_PEST.mat');
% % % dataP.Unstabilized.Z091 = load(...
% % %     'X:\Ashley\Crowding\Z091\Uncrowded_Unstabilized_PEST.mat');
%--------------------------------------------------------------

subjectsAll = fieldnames(data.Unstabilized);
conditionsS = {'Stabilized','Unstabilized'};

reprocess = 1;

% [averageError] = determineStabError(data.Stabilized); %takes long time to run

%--------------------------------------------------------------
if reprocess
    counterStruct = 1;
    for e = 1:length(subjectsAll)
        for s = 1:2
            temp = data.(conditionsS{s}).(subjectsAll{e});
            dateCollect = datetime(str2double(temp.eis_data.filename(5:8)),...
                str2double(temp.eis_data.filename(9:10)),...
                str2double(temp.eis_data.filename(11:12)));
            
            temppptrials = convertDataToPPtrials(temp.eis_data,dateCollect);
            processedPPTrials = newEyerisEISRead(temppptrials);
            
            counter = 1;
            for ii = 1:length(processedPPTrials)
                
                dataAll{s,e}.sw(counter) = double(processedPPTrials{ii}.TargetStrokewidth)/5;
                dataAll{s,e}.response(ii) = processedPPTrials{ii}.Response;
                dataAll{s,e}.correct(ii) = processedPPTrials{ii}.Correct;
                dataAll{s,e}.spaceFlank(counter) = double(processedPPTrials{ii}.FlankerDist);
                if double(processedPPTrials{ii}.FlankerDist) == 200000
                    dataAll{s,e}.spaceFlank(counter) = 2.5;
                end
                dataAll{s,e}.spaceFlanker_CenterArcmin(counter) = ...
                    (dataAll{s,e}.spaceFlank(counter)*dataAll{s,e}.sw(counter));
                
                
%                 ++++----++++, 
                
                dataAll{s,e}.spaceFlanker_EdgeArcmin(counter) = ...
                    (dataAll{s,e}.spaceFlank(counter)*(dataAll{s,e}.sw(counter))) ...
                    -(dataAll{s,e}.sw(counter));

                dataAll{s,e}.snellenAcuity(counter) = (dataAll{s,e}.sw(counter))/2;
                dataAll{s,e}.logMar(counter) = dataAll{s,e}.snellenAcuity(counter)*20;
                dataAll{s,e}.x{counter} = processedPPTrials{ii}.x;
                dataAll{s,e}.y{counter} = processedPPTrials{ii}.y;
                counter = counter + 1;
                
                tableData(counterStruct).subject = e;
                tableData(counterStruct).stabilized = s;
                tableData(counterStruct).sw = double(processedPPTrials{ii}.TargetStrokewidth)/5;
                tableData(counterStruct).response = processedPPTrials{ii}.Response;
                tableData(counterStruct).correct = processedPPTrials{ii}.Correct;
                tableData(counterStruct).spaceFlank = double(processedPPTrials{ii}.FlankerDist);
                if double(processedPPTrials{ii}.FlankerDist) == 200000
                    tableData(counterStruct).spaceFlank = 2.5;
                end
                tableData(counterStruct).logMar = (dataAll{s,e}.sw(counter-1))/2;
                tableData(counterStruct).snellenAcuity = dataAll{s,e}.snellenAcuity(counter-1)*20;
                tableData(counterStruct).x = processedPPTrials{ii}.x;
                tableData(counterStruct).y = processedPPTrials{ii}.y;
                counterStruct = counterStruct + 1;
            end
            %     close all
            figure;
            idxTrials = find(dataAll{s,e}.correct < 3);
            dataAll{s,e}.idxTrials = idxTrials;
            acrossSubStats.numTrials(e,s) = length(idxTrials);
%             
            [thresh{s,e}, par{s,e}, threshB{s,e}, xi{s,e}, yi{s,e}, chiSq{e}] = ...
                psyfitCrowding(dataAll{s,e}.spaceFlank(idxTrials), ...
                dataAll{s,e}.correct(idxTrials), 'DistType', ...
                'Normal','Chance', .25, 'Extra');
%              [thresh{s,e}, par{s,e}, threshB{s,e}, xi{s,e}, yi{s,e}, chiSq{e}] = ...
%                 psyfitCrowding(dataAll{s,e}.sw(idxTrials), ...
%                 dataAll{s,e}.correct(idxTrials), 'DistType', ...
%                 'Normal','Chance', .25, 'Extra');
            
            xlabel('Center to Center Spacing (Multiplier)');
            ylabel('Performance');
            xticks([0.5:.5:2.5])
            xticklabels({'0.5','1','1.5','2','Inf'});
            acrossSubStats.thresh(e,s) = thresh{e};
            title(sprintf('%s, %s',subjectsAll{e},conditionsS{s}))
            saveas(gcf,sprintf('CrowdingFits/PsychFit%s%s.png',subjectsAll{e},conditionsS{s}));
            
            
%            
            [threshCenterArcmin{s,e}] = ...
                psyfitCrowding(dataAll{s,e}.spaceFlanker_CenterArcmin(idxTrials), ...
                dataAll{s,e}.correct(idxTrials), 'DistType', ...
                'Normal','Chance', .25, 'Extra');     
            
            [threshEdgeArcmin{s,e}] = ...
                psyfitCrowding(dataAll{s,e}.spaceFlanker_EdgeArcmin(idxTrials), ...
                dataAll{s,e}.correct(idxTrials), 'DistType', ...
                'Normal','Chance', .25, 'Extra');       
     
        end
    end
    save('crowdingFixedSizeData');
else
    load('crowdingFixedSizeData.mat');
end


%% Looking at PEST Thresholds
PESTtable = load('PESTdata.mat');
idx1 = find([PESTtable.tableData.subject] == 1);
idx2 = find([PESTtable.tableData.subject] == 2);
idx3 = find([PESTtable.tableData.subject] == 3);
idx4 = find([PESTtable.tableData.subject] == 4);
for i = idx1
    PESTtable.tableData(i).subject = 4;
end
for i = idx2
    PESTtable.tableData(i).subject = 5;
end
for i = idx3
    PESTtable.tableData(i).subject = 7;
end
for i = idx4
    PESTtable.tableData(i).subject = 3;
end
new_table = [tableData PESTtable.tableData];

clear threshN
for ii = 1:length(subjectsAll)
    idx1 = find([new_table.subject] == ii & ...
        [new_table.stabilized] == 1 &...
        [new_table.correct] < 3 & ...
        [new_table.spaceFlank] == 2.5);
    
%     if length(unique([new_table(idx1).sw])) == 1
%         threshN(ii,1) = {unique([new_table(idx1).sw])};
        perfs(ii,1) = mean([new_table(idx1).correct]);
        swTest(ii,1) = mean([new_table(idx1).sw]);
%         noMease(ii) = ii;
%         continue;
%     end
%     
%     [threshN{ii,1}] = ...
%         psyfitCrowding([new_table(idx1).sw], ...
%         [new_table(idx1).correct], 'DistType', ...
%         'Normal','Chance', .25, 'Extra');
    
    
end
for ii = 1:length(subjectsAll)

    idx2 = find([new_table.subject] == ii & ...
        [new_table.stabilized] == 2 &...
        [new_table.correct] < 3 & ...
        [new_table.spaceFlank] == 2.5);
    
%     if length(unique([new_table(idx2).sw])) == 1
%         threshN(ii,2) = {unique([new_table(idx2).sw])};
        perfs(ii,2) = mean([new_table(idx2).correct]);
        swTest(ii,2) = mean([new_table(idx1).sw]);

%          noMease(2,ii) = ii;
%         continue;
%     end
%     
%     [threshN{ii,2}] = ...
%         psyfitCrowding([new_table(idx2).sw], ...
%         [new_table(idx2).correct], 'DistType', ...
%         'Normal','Chance', .25, 'Extra');
% 
%    perfs(ii,2) = mean([new_table(idx2).correct]);
end

figure;
for ii = 1:length(subjectsAll)
        plot([1 2],[perfs(ii,1) perfs(ii,2)],'-o')
%     meanThresh(ii,:) = [threshN{ii,1} threshN{ii,2}]
    hold on
end
ylim([.25 1])
% errorbar([1 2]
ylabel('Performance Correct')
xticks([1 2])
xticklabels(conditionsS)
xlim([0 3])
errorbar([1 2],mean(perfs),std(perfs),'-o',...
    'MarkerFaceColor','k','Color','k');


figure;
for ii = 1:length(subjectsAll)
     idx1 = find([tableData.subject] == ii & ...
        [tableData.stabilized] == 1 &...
        [tableData.correct] < 3 & ...
        [tableData.spaceFlank] == 2.5);
     idx2 = find([tableData.subject] == ii & ...
        [tableData.stabilized] == 2 &...
        [tableData.correct] < 3 & ...
        [tableData.spaceFlank] == 2.5);

    forlegs(1) = plot([1 2],[mean([tableData(idx1).sw])...
       mean([tableData(idx2).sw])] ,'-','Color',[.27 .27 .27]);
   forsavs(ii,:) = [mean([tableData(idx1).sw])...
       mean([tableData(idx2).sw])];
    hold on
%     forlegs(2) = plot(2,mean([tableData(idx2).sw]),'o','Color','b');
end
% xlabel('Subject')
xticks([1 2])
xticklabels({'Stabilized','Unstabilized'})
ylabel('Stimulus Size Width (in arcmin)');
% legend(forlegs,{'Stabilized','Unstabilized'});
xlim([0.5 2.5])
errorbar ([1 2],mean(forsavs),sem(forsavs),'-o','Color','k','MarkerFaceColor','k');
[h,p] = ttest(forsavs(:,1),forsavs(:,2))
line([1 2],[2.3 2.3],'LineWidth',3)
text(1.5,2.4,'*','FontSize',14)
ylim([.5 2.5])

figure;
for ii = 1:length(subjectsAll)
%     if ii == 1 || ii == 2 || ii == 6
%         plot([1 2],[threshN{ii,1} threshN{ii,2}],'-*')
%     else
        plot([1 2],[threshN{ii,1} threshN{ii,2}],'-o')
%     end
    meanThresh(ii,:) = [threshN{ii,1} threshN{ii,2}]
    hold on
end
% errorbar([1 2]
ylabel('Acuity Threshold (arcmin)')
xticks([1 2])
xticklabels(conditionsS)
xlim([0 3])
errorbar([1 2],mean(meanThresh),sem(meanThresh),'-o',...
    'MarkerFaceColor','k','Color','k');

[h,p] = ttest2(meanThresh(:,1),meanThresh(:,2));

    plot([1 2],[mean([new_table(idx1).correct]),...
        mean([new_table(idx2).correct])],...
        '-o');
    hold on
%     plot([1 2],[thresh{1,ii},thresh{2,ii}],'-o');
    hold on


%%


% valFIX = [5 3 3 3];
% figure;
% for ii = 1:length(subjectsAll)
%     valCommon = unique([tableData([tableData.subject] == ii).sw]);
%     idx1 = find([tableData.subject] == ii & ...
%         [tableData.stabilized] == 1 &...
%         [tableData.correct] < 3 & ...
%         [tableData.sw] == valCommon(valFIX(ii)))
%     
%    idx2 = find([tableData.subject] == ii & ...
%         [tableData.stabilized] == 2 &...
%         [tableData.correct] < 3 & ...
%         [tableData.sw] == valCommon(valFIX(ii)))
%    nums(ii,1) = length(idx1);
%    nums(ii,2) = length(idx2);
% 
%    perfs(ii,:) = [mean([tableData(idx1).correct]),...
%         mean([tableData(idx2).correct])];
%     
%     plot([1 2],[mean([tableData(idx1).correct]),...
%         mean([tableData(idx2).correct])],...
%         '-o');
%     hold on
% %     plot([1 2],[thresh{1,ii},thresh{2,ii}],'-o');
%     hold on
% end
% ylabel('Performance')
% xticks([1 2])
% xticklabels({'Stabilized','Unstabilized'})
% title('Fixed Stim Size')
% xlim([0 3])

figure;
for ii = 1:length(subjectsAll)
    forLegs(ii) = plot([1.1 2.2],[thresh{1,ii},...
        thresh{2,ii}],'-','Color',[.7 .7 .7]);
    hold on
end
threshMat = cell2mat(thresh)

errorbar([1 2],[mean(threshMat(1,:)) mean(threshMat(2,:))],...
    [sem(threshMat(1,:)) sem(threshMat(2,:))],'-o',...
    'MarkerFaceColor','k','Color','k');
xticks([1 2])
xlim([0.5 2.5])
xticklabels({'Stabilized','Unstabilized'});
ylabel('Center-Center Nominal Spacing')
[h,p] = ttest(threshMat(1,:),threshMat(2,:))
title(sprintf('p = %.2f',p))
% legend(forLegs,subjectsAll)
% legend off
ylim([0.8 2.2]);

figure;
% subplot(1,2,1)
for ii = 1:length(subjectsAll)
    idx1 = find([tableData.subject] == ii & ...
        [tableData.stabilized] == 1 &...
        [tableData.correct] < 3 & ...
        [tableData.spaceFlank] == 2.5);
    idx2 = find([tableData.subject] == ii & ...
        [tableData.stabilized] == 2 &...
        [tableData.correct] < 3 & ...
        [tableData.spaceFlank] == 2.5);
    
    perfs(ii,:) = [mean([tableData(idx1).correct]),...
        mean([tableData(idx2).correct])]
    
    plot([1 2],[mean([tableData(idx1).correct]),...
        mean([tableData(idx2).correct])],...
        '-o');
    hold on
%     dataCorrect
%     [threshCenterArcmin{s,e}] = ...
%         psyfitCrowding(tabledata., ...
%         dataAll{s,e}.correct(idxTrials), 'DistType', ...
%         'Normal','Chance', .25, 'Extra');
end
[h,p] = ttest2(perfs(:,1),perfs(:,2))
xticks([1 2])
xticklabels({'Stabilized','Unstabilized'})


for ii = 1:length(threshEdgeArcmin)
    if threshEdgeArcmin{1,ii} < 0
        threshEdgeArcmin{1,ii} = 0;
    end
end


%%
threshEdgeArcmin{1,1} = 0;
threshEdgeArcmin{1,2} = 0;
figure;
subplot(1,2,1)
for ii = 1:length(subjectsAll)
    forLegs(ii) = plot([1.1 2.2],[threshEdgeArcmin{1,ii},...
        threshEdgeArcmin{2,ii}],'-*');
    hold on
end
errorbar([1 2],[mean([threshEdgeArcmin{1,:}]) mean([threshEdgeArcmin{2,:}])],...
    [sem([threshEdgeArcmin{1,:}]) sem([threshEdgeArcmin{2,:}])],'-o',...
    'MarkerFaceColor','k','Color','k');
xticks([1 2])
xlim([0 3])
xticklabels({'Stabilized','Unstabilized'});
ylabel('Edge-Edge Critical Spacing (Arcmin)')
[h,p,ci,stats] = ttest([threshEdgeArcmin{1,:}],[threshEdgeArcmin{2,:}])
title(sprintf('p = %.2f',p))
ylim([-.25 2.5])

for ii = 1:length(threshCenterArcmin)
    if threshCenterArcmin{1,ii} < 0
        threshCenterArcmin{1,ii} = 0;
    end
end

subplot(1,2,2)
for ii = 1:length(subjectsAll)
    forLegs(ii) = plot([1.1 2.2],[threshCenterArcmin{1,ii},...
        threshCenterArcmin{2,ii}],'-*');
    hold on
end
errorbar([1 2],[mean([threshCenterArcmin{1,:}]) mean([threshCenterArcmin{2,:}])],...
    [sem([threshCenterArcmin{1,:}]) sem([threshCenterArcmin{2,:}])],'-o',...
    'MarkerFaceColor','k','Color','k');
xticks([1 2])
xlim([0 3])
xticklabels({'Stabilized','Unstabilized'});
ylabel('Center-Center Critical Spacing (Arcmin)')
[h,p,ci,stats] = ttest([threshCenterArcmin{1,:}],[threshCenterArcmin{2,:}])
title(sprintf('p = %.2f',p))
%%
afsdasd 

% figure;
% for ii = 1:length(subjectsAll)
%     idxArcS = find([tableData(:).subject] == ii & ...
%         [tableData(:).stabilized] == 1);
%     idxArcU = find([tableData(:).subject] == ii & ...
%         [tableData(:).stabilized] == 2);    
%     
%     newVals1(ii) = acrossSubStats.thresh(ii,1)*(tableData(idxArcS(1)).sw/2);
%     newVals2(ii) = acrossSubStats.thresh(ii,2)*(tableData(idxArcU(1)).sw/2);
%     
%     forLegs(ii) = plot([1.1 2.2],[newVals1(ii),...
%         newVals2(ii)],'-*');
%     hold on
% end
% errorbar([1 2],[mean(newVals1) mean(newVals2)],...
%     [sem(newVals1) sem(newVals2)],'-o',...
%     'MarkerFaceColor','k','Color','k');
% xticks([1 2])
% xlim([0 3])
% xticklabels({'Stabilized','Unstabilized'});
% ylabel('Edge-Edge Critical Spacing (arcmin)')
% [h,p] = ttest(newVals1,newVals2)
% title(sprintf('p = %.2f',p))
% % legend(forLegs,subjectsAll)
% % legend off
% % ylim([1 4.5]);

