%%runAcuityAnalysis
clear all
close all
clc
% A132 had too few trials at various eccentricites
% A169 had hard time with calibration and unreliable data
%%% Krishn Collected
% Z187
% Z127 - full set
% Z149
% Krish,'Krish'

% Fixed Order: 'Z091','Sanjana','Ashley','Z126','A188','Soma','Janis','A144'
subjectsAll = {'Z091','Sanjana','Ashley','Z126','A188','Soma',...
    'Janis','A144','Z187','Z127','Z149','Z197'}; %Z185
subjectsAll = {'Z197','Z118'};%Z187','Z127','Z149','Z197'};
% manCheckDone = [1 0 0 0 0 0 0 0 0 0 0 0];
manCheckDone = [0 0 0 0];
% newDate = [ 0 0 0 0 0 0 0 0 1 1 1 1];
newDate = [1 1 1 1];
rerunProcess = 1;
cm = jet(length(subjectsAll));


if rerunProcess
    for ss = 1:length(subjectsAll)       
        fprintf('Loading %s Data \n',subjectsAll{ss});
        [data{ss}] = loadAcuityAOTaskData(subjectsAll{ss}, manCheckDone(ss));
        idxName = fieldnames(data{ss});
        %% EM Analysis
        DC = [];
        validForAnalyis = [];
        strs = idxName;
        output = zeros(length(strs),1);
        fprintf('Starting EM Analysis \n');
        for iter=1:length(strs)
            num_strs = regexp(strs{iter},'\d*','Match');
            output(iter) = str2double(num_strs{1});
        end
        
        for i = 1:length(unique(output))%:4
            currentIdxName = idxName{i};
            
            if manCheckDone(ss) == 0
                dataAll{i}.pptrials = newEyerisEISRead(data{ss}.(currentIdxName));
%                 if ~exist(sprintf('SavedPPTrials_CleanAOAcuityData/%s', sprintf(subjectsAll{ss})), 'dir')
%                     mkdir('SavedPPTrials_CleanAOAcuityData', sprintf(subjectsAll{ss}));
%                     
%                     dataAll{i}.pptrials = cleaningTrialsVisually(dataAll{i}.pptrials,...
%                         'NewFileName',sprintf('pptrials_%s',currentIdxName),...
%                         'Folder',sprintf('SavedPPTrials_CleanAOAcuityData/%s', subjectsAll{ss}),...
%                         'Conversion',round(1000/dataAll{i}.pptrials{1}.sRate));
%                 end
            else
                dataAll{i}.pptrials = newEyerisEISRead(data{ss}.(currentIdxName).pptrials);
            end

        
%         loadParms;
%         fprintf('Loading %s Data \n',subjectsAll{ss});
%         [data{ss}] = loadAcuityAOTaskData(subjectsAll{ss}, manCheckDone(ss));
%         idxName = fieldnames(data{ss});
%         %% EM Analysis
%         DC = [];
%         validForAnalyis = [];
%         strs = idxName;
%         output = zeros(length(strs),1);
%         fprintf('Starting EM Analysis \n');
%         for iter=1:length(strs)
%             num_strs = regexp(strs{iter},'\d*','Match');
%             output(iter) = str2double(num_strs{1});
%         end
%         
%         for i = 1:length(unique(output))%:4
%             currentIdxName = idxName{i};
            
%             if manCheckDone(ss) == 0
%                 
%                 if ~isfolder(sprintf('SavedPPTrials_CleanAOAcuityData/%s', sprintf(subjectsAll{ss})))
%                     dataAll{i}.pptrials = newEyerisEISRead(data{ss}.(currentIdxName));
%                     mkdir('SavedPPTrials_CleanAOAcuityData', sprintf(subjectsAll{ss}));
%                     temppptrials = dataAll{i}.pptrials;
%                     save(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc%i.mat',subjectsAll{ss},output(i))...
%                         ,'temppptrials')
%                     
%                      dataAll{i}.pptrials = cleaningTrialsVisually(dataAll{i}.pptrials,...
%                         'NewFileName',sprintf('pptrials_%s',currentIdxName),...
%                         'Folder',sprintf('SavedPPTrials_CleanAOAcuityData/%s', subjectsAll{ss}),...
%                         'Conversion',round(1000/dataAll{i}.pptrials{1}.sRate));
%                 else
%                     temp = load(sprintf('SavedPPTrials_CleanAOAcuityData/%s/pptrials_ecc0.mat',subjectsAll{ss}));
%                     dataAll{i}.pptrials = temp.pptrials;
%                     dataAll{i}.pptrials = cleaningTrialsVisually(dataAll{i}.pptrials,...
%                         'NewFileName',sprintf('pptrials_%s',currentIdxName),...
%                         'Folder',sprintf('SavedPPTrials_CleanAOAcuityData/%s', subjectsAll{ss}),...
%                         'Conversion',round(1000/dataAll{i}.pptrials{1}.sRate));
% %                 end
%             else
% % %                 dataAll{i}.pptrials = newEyerisEISRead(data{ss}.(currentIdxName).pptrials);
% % 
%                 dataAll{i}.pptrials = newEyerisEISRead(data{ss}.(currentIdxName).pptrials);
%             end
            
            xAll = [];
            yAll = [];
            xAllFixation = [];
            yAllFixation = [];
            counter = 1;
            counter2 = 1;
            for ii = 1:length(dataAll{i}.pptrials)
                if ~isfield(dataAll{i}.pptrials{ii},'drifts')
                    validForAnalyis{i}(ii) = 0;
                    continue;
                end
                if dataAll{i}.pptrials{ii}.fixationTrial
                    keepIdx = find(dataAll{i}.pptrials{ii}.x > -60 & ...
                        dataAll{i}.pptrials{ii}.x < 60 & ...
                        dataAll{i}.pptrials{ii}.y > -60 & ...
                        dataAll{i}.pptrials{ii}.y < 60);
                    x = dataAll{i}.pptrials{ii}.x(keepIdx);
                    y = dataAll{i}.pptrials{ii}.y(keepIdx);
                    if length(x) >= 400
                        xAllFixation = [xAllFixation x(1:400) - mean(x(1:400))];
                        yAllFixation = [yAllFixation y(1:400) - mean(y(1:400))];
                    end
                    %                 bceaFix{i}(counter) = get_bcea(...
                    %                         dataAll{i}.pptrials{ii}.x(keepIdx)/60,...
                    %                         dataAll{i}.pptrials{ii}.y(keepIdx)/60);
                end
                
                %%%getting correct trials for performance
                valid = countingTrialsAOAcuity(dataAll{i}.pptrials(ii));
                tossstats(ii).saveValid = valid;
                
                pptrials = dataAll{1,1}.pptrials
                gazeshift_x_min = 5;% Define gaze position limits for filtering trials
                gazeshift_x_max = -5;
                gazeshift_y_min = -20;
                gazeshift_y_max = 20;
                trial_duration = 500;
%                 for ii = 1:length(pptrials)
%                     if sum(~(pptrials{ii}.x < gazeshift_x_min)) > 0 ||...
%                             sum(~(pptrials{ii}.x > gazeshift_x_max)) > 0 ||...
%                             sum((pptrials{ii}.y < gazeshift_y_min)) > 0 ||... %exclude
%                             sum((pptrials{ii}.y > gazeshift_y_max)) > 0 ||...
%                             all(pptrials{ii}.drifts.duration <= trial_duration)
%                         exclude(ii) = 1;
%                         exclude_count_1(ecc_idx) = exclude_count_1(ecc_idx) + 1; % Increment count for exclude = 1
%                         exclude_1_indices = [exclude_1_indices, ii]; % Store index of exclude = 1
%                         %                     continue;
%                     end
%                 end
%                 
                
                %                 if valid.d || valid.dms
                %                     validForAnalyis{i}(ii) = 1;
                %                 else
                %                     validForAnalyis{i}(ii) = 0;
                %                 end
                for d = 1:length(dataAll{i}.pptrials{ii}.drifts.start)
                    driftStart = dataAll{i}.pptrials{ii}.drifts.start(d);
                    driftEnd = dataAll{i}.pptrials{ii}.drifts.duration(d) + ...
                        driftStart;
                    if driftStart >= length(dataAll{i}.pptrials{ii}.x)
                        continue;
                    elseif driftEnd >=length(dataAll{i}.pptrials{ii}.x)
                        driftEnd = length(dataAll{i}.pptrials{ii}.x);
                    end
                    if dataAll{i}.pptrials{ii}.fixationTrial
                        
                    else
                        bceaTask{i}(counter) = get_bcea(...
                            dataAll{i}.pptrials{ii}.x(driftStart:driftEnd)/60,...
                            dataAll{i}.pptrials{ii}.y(driftStart:driftEnd)/60);
                        
                        
                        xAll = [xAll dataAll{i}.pptrials{ii}.x(driftStart:driftEnd)];
                        yAll = [yAll dataAll{i}.pptrials{ii}.y(driftStart:driftEnd)];
                        dataAll{i}.position(counter).x = dataAll{i}.pptrials{ii}.x(driftStart:driftEnd);
                        dataAll{i}.position(counter).y = dataAll{i}.pptrials{ii}.y(driftStart:driftEnd);
                        if length(dataAll{i}.pptrials{ii}.x(driftStart:driftEnd)) >= 256
                            dataAll{i}.positionDC(counter2).x = ...
                                dataAll{i}.pptrials{ii}.x(driftStart:driftStart+255);
                            dataAll{i}.positionDC(counter2).y = ...
                                dataAll{i}.pptrials{ii}.y(driftStart:driftStart+255);
                            counter2 = counter2 + 1;
                        end
                        counter = counter + 1;
                    end
                end
            end
            tempDX = {dataAll{i}.positionDC.x};
            tempDY = {dataAll{i}.positionDC.y};
            forDSQCalc.x = tempDX;
            forDSQCalc.y = tempDY;
            xAllSaved{i} = xAll;
            yAllSaved{i} = yAll;
            xAllSavedF{i} = xAllFixation;
            yAllSavedF{i} = yAllFixation;
            [~,~,~, ~, ~, ...
                tempSingleSegmentDsq,~,~] = ...
                CalculateDiffusionCoef(1000, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
            idx2 = [];
            for s = 4:size(tempSingleSegmentDsq)
                %         if isempty(find(tempSingleSegmentDsq(s,:) > 65 | tempSingleSegmentDsq(s,100) > 40)) %Janis
                if strcmp(subjectsAll{ss},'A188')
                    if isempty(find(tempSingleSegmentDsq(s,:) > 120 | ...
                            tempSingleSegmentDsq(s,40) > 15)) %Sanjana
                        idx2(s) = 1;
                    else
                        idx2(s) = 0;
                    end
                elseif  strcmp(subjectsAll{ss},'Sanjana')
                    if isempty(find(tempSingleSegmentDsq(s,:) > 120 | tempSingleSegmentDsq(s,50) > 20)) %Sanjana
                        idx2(s) = 1;
                    else
                        idx2(s) = 0;
                    end
                elseif  strcmp(subjectsAll{ss},'Z091')
                    if isempty(find(tempSingleSegmentDsq(s,:) > 80 | ...
                            tempSingleSegmentDsq(s,50) > 10))
                        idx2(s) = 1;
                    else
                        idx2(s) = 0;
                    end
                else
                    if isempty(find(isnan(tempSingleSegmentDsq(s,:))))
                        idx2(s) = 1;
                        if isempty(find(tempSingleSegmentDsq(s,:) > 120))
                            idx2(s) = 1;
                        else
                            idx2(s) = 0;
                        end
                    else
                        idx2(s) = 0;
                    end
                end
            end
            forDSQCalc.x = tempDX(find(idx2));
            forDSQCalc.y = tempDY(find(idx2));
            if strcmp (subjectsAll{ss},'Z091')
                [~,dataAll{i}.Bias,dataAll{i}.dCoefDsq, ~, dataAll{i}.Dsq, ...
                    dataAll{i}.SingleSegmentDsq,~,~] = ...
                    CalculateDiffusionCoef(341, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
            else
                [~,dataAll{i}.Bias,dataAll{i}.dCoefDsq, ~, dataAll{i}.Dsq, ...
                    dataAll{i}.SingleSegmentDsq,~,~] = ...
                    CalculateDiffusionCoef(1000, struct('x',forDSQCalc.x, 'y', forDSQCalc.y));
            end
            DC = [DC dataAll{i}.dCoefDsq];
            allStats{i} = tossstats;
        end
        %% Performance Analysis
        fprintf('Calculating Performance Data \n');
            [acrossSubStats,dataTemp] = ...
                calculatePerformanceAnalysisAcuity(data{ss}, idxName, validForAnalyis, manCheckDone(ss),newDate(ss));
        figure;
        plot(acrossSubStats.ecc, acrossSubStats.snellAcuity,'-o')
        xlabel('Eccentricity (arcmin)')
        ylabel('Acuity 20/X');
        text(acrossSubStats.ecc,acrossSubStats.snellAcuity,string(acrossSubStats.numTrials));
        % AO = [12359.85958	10813.84851	10091.52408	9277.956081	8737.329778];
        % %Ashley
        % AO = [12684.61208	10159.54458	9646.510861	8013.680678	7277.125707]; %Janis
        [AO] = loadinAOData(subjectsAll{ss});
        aoFreq = [];
        for i = 1:length(idxName)
            D = AO.(idxName{i}).AO.PRL;
            %%% We can compute the sampling limit a... using the following formulas:
            %%% https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6660219/
            aoFreq(i) = (1/2)*(sqrt((2/sqrt(3)) * D)); %%from Rossi Rooda
        end
        % figure;
        clear s
        [~,p,b,r] = LinRegression(acrossSubStats.ecc,acrossSubStats.freqThresh,0,NaN,1,0);
        hold on
        s(1) = plot(acrossSubStats.ecc,acrossSubStats.freqThresh,'o','MarkerFaceColor','g');
        [~,p,b,r] = LinRegression(acrossSubStats.ecc,aoFreq,0,NaN,1,0);
        s(2) = plot(acrossSubStats.ecc,aoFreq,'o','MarkerFaceColor','m');
        ylim([10 65])
        xlim([-3 27])
        line([0 27],[acrossSubStats.freqThresh(1) acrossSubStats.freqThresh(1)],...
            'Color','k','LineStyle','--')
        xticks([0 10 15 25])
        yticks([10 20 30 40 50 60])
        % legend(s,{'Acuity Frequency','Nyquist Frequency (Nasal)'});
        title(subjectsAll{1});
        axis square
        axis square
        xlabel('Eccentricity (arcmin)');
        ylabel('Frequency (cycl/deg)');
        
        
        
        % DCSave = [5.62084627151489,7.25673580169678,9.23340225219727,7.83249378204346];
        figure(100)
        acrossSubStats.SFfromDC = powerAnalysis_JustCriticalFrequency (DC, 0);
        close figure 100
        hold on
        % yyaxis left
        s(3) = plot(acrossSubStats.ecc, acrossSubStats.SFfromDC,'-o');
        %     legend(s,{'Acuity Frequency','Nyquist Frequency (Nasal)',...
        %         'DC Frequency'});
        title(subjectsAll{ss});
        
        allSubjectStats{ss} = acrossSubStats;
        allSubjectStats{ss}.dc = DC;
        allSubjectStats{ss}.bceaTask = bceaTask;
        %     allSubjectStats{ss}.bceaFix = bceaFix;
        allSubjectStats{ss}.xFix = xAllSavedF;
        allSubjectStats{ss}.yFix = yAllSavedF;
        allSubjectStats{ss}.AOFreq = aoFreq;
        allSubjectStats{ss}.xAllSaved = xAllSaved;
        allSubjectStats{ss}.yAllSaved = yAllSaved;
    end
%     save('allSubjectStatsAOAcuityForVSS.mat','allSubjectStats')
    save('allSubjectStatsAOAcuity11292023.mat','allSubjectStats')

else
     load('allSubjectStatsAOAcuityForVSS.mat');
end

figure;
for ii = 1:length(allSubjectStats)
    hold on
    forlegs(ii) = plot(...
        allSubjectStats{ii}.ecc,allSubjectStats{ii}.snellAcuity,'o-');
    for i = 1:length(allSubjectStats{ii}.ecc)
    text(allSubjectStats{ii}.ecc(i),allSubjectStats{ii}.snellAcuity(i),...
        num2str(allSubjectStats{ii}.numTrials(i)))
    end
end
% legend(forlegs(1:12),subjectsAll(1:12))
xlabel('Eccentricity')
ylabel('Snellen Acuity (20/X)');





% AOMaps = [40 40 40 40 40 40 40 40 40];
AOMaps = [60 60 60 60 60 60 60 60 60];

ecc = [0 10 15 25];
for ii = 1:length(subjectsAll)
     [AOAll{ii}] = loadinAOData(subjectsAll{ii});
    for e = 1:length(ecc)
        eccName = sprintf('ecc%i',ecc(e));
        aoDens(ii,e) = AOAll{ii}.(eccName).AO.PRL;
        numCones(ii,e) = 0;
        if AOMaps(ii) == 0
            continue
        end
        %             if flatDC(ii,e)
        numCones(ii,e) = coneConesSpecifyMap(...
            subjectsAll{ii},allSubjectStats,ii,e,ecc(e),AOMaps(ii));
        %             else
        %                 numCones(ii,e) =  NaN;
        %             end
    end
end

counter = 1;
for ii = [1:length(allSubjectStats)]
    dc(counter,:) = allSubjectStats{ii}.dc(1:4);
    acuity(counter,:) = allSubjectStats{ii}.snellAcuity(1:4);
    counter = counter + 1; 
end

forlegs = [];
for ii = 1:length(subjectsAll)
    figure('Position',[100 -10 400 1000]);
    for e = 1:4
        %     subplot(2,4,ii)
        %     figure(100)
        subplot(4,1,e)
        dcFreq = powerAnalysis_JustCriticalFrequency(dc(ii,e), 0);
        %     close Figure 100
        aoFreq  = (1/2)*(sqrt((2/sqrt(3)) * aoDens(ii,e)));
        acuityFreq  = (60./((acuity(ii,e)./20)*5))*2.5;
        
        freqInfo.dc(ii,e) = dcFreq;
        freqInfo.ao(ii,e) = aoFreq;
        freqInfo.acuity(ii,e) = acuityFreq;
        hold on
        line([aoFreq aoFreq],[-45 0],'Color','red','LineStyle','--');
        line([acuityFreq acuityFreq],[-45 0],'Color','blue','LineStyle',':');
        line([dcFreq dcFreq],[-45 0],'Color','black','LineStyle','-.');
        t = text(aoFreq,-30,'Retina Limit');t.Rotation = 90;
        t2 = text(acuityFreq,-30,'Acuity');t2.Rotation = 90;
        t3 = text(dcFreq,-30,'DC');t3.Rotation = 90;

        t4 = text(acuityFreq,-35,'\leftarrow Optics \rightarrow','FontSize',(aoFreq-acuityFreq)/4);
        %     forlegs(1) = plot(ecc,dcFreq,'-s');
        %     hold on
        %     forlegs(2) = plot(ecc,aoFreq,'-o');
        %     hold on
        %     forlegs(3) = plot(ecc,acuityFreq,'-d');
        %     end
        xlim([10 100])
        title(sprintf('Ecc %i',ecc(e)));
%         axis square
   

    end
     saveas(gcf,sprintf('C:/Users/Ruccilab/OneDrive - University of Rochester/Documents/Adaptive Optics/Images/%i.png',ii));
end


for ii = 1:length(allSubjectStats)
    indBCEA(ii) = get_bcea(allSubjectStats{ii}.xFix{1}/60, ...
        allSubjectStats{ii}.yFix{1}/60);
end


%% figure;
% [~,p,b,r] = LinRegression(temp3,temp4,0,NaN,1,0);
% figure;
% for ii = 1:length(allSubjectStats)
%     numTrials = length(allSubjectStats{ii}.xFix{1})/400;
%     counter = 1;
%     for n = 1:numTrials
%         matrixTrialsX{ii}(n,:) = allSubjectStats{ii}.xFix{1}(counter:counter+399);
%         matrixTrialsY{ii}(n,:) = allSubjectStats{ii}.yFix{1}(counter:counter+399);
%         counter = counter + 499;
%     end
%     y = randi([1 numTrials],1,3);
%     m(:,ii) = bootstrp(100, @get_bcea, ...
%         [matrixTrialsX{ii}(y(1),:) matrixTrialsX{ii}(y(2),:) matrixTrialsX{ii}(y(3),:)]'/60,...
%         [matrixTrialsY{ii}(y(1),:) matrixTrialsY{ii}(y(2),:) matrixTrialsY{ii}(y(3),:)]'/60);
% end
% boxplot(m)
% set(gca, 'YScale', 'log')
% set(gca, 'YTick', [0.005 0.01 0.05 0.1 0.2])
% ylim([0.001 0.2])
% xticklabels(subjectsAll)
% title('100 Bootstrap Recentered BCEA')
% ylabel('BCEA deg^2')
%% 


mean((acuity(:,4) - acuity(:,1))./acuity(:,1))
std((acuity(:,4) - acuity(:,1))./acuity(:,1))


figure;
subplot(1,3,1)
boxplot(indBCEA);
xticklabels({'BCEA'}); 
% ax = gca;
% ax.YAxis.Scale ="log";
% ylim([-0.1 .15])

subplot(1,3,2)
boxplot(acuity(:,1));
xticklabels({'Acuity (Snellen 20/X)'}); 
subplot(1,3,3)
boxplot(dc(:,1));
xticklabels({'D (arcmin^2/sec)'}); 
% acuity(9,2) = 24;

% flatDC = [1 1 0 0;...
%         1 0 1 0;...
%         1 1 0 1;...
%         1 0 0 1;
%         0 0 0 0;...
%         1 1 1 1;...
%         0 0 0 0;...
%         0 0 1 1];

    
figure;
subplot(1,2,1)
[~,p,b,r] = LinRegression(dc(:,1)',acuity(:,1)',0,NaN,1,0);
xlabel('DC');
ylabel('Snellen Acuity at 0ecc');
% suptitle('p = 0.01');
% figure;
subplot(1,2,2)

[~,p,b,r] = LinRegression(dc(:)',acuity(:)',0,NaN,1,0);
xlabel('DC');
ylabel('Snellen Acuity');

 
%% EM HeatMaps

for ii = 1:length(allSubjectStats)
    figure;
    counter = 1;
    for i = 1:length(allSubjectStats{ii}.numTrials)
%         if length(allSubjectStats{ii}.xAllSaved{1, 1}  ) < 50 || ...
%                 isempty(find(allSubjectStats{ii}.xAllSaved{1, 1}   == 10))
%             continue;
%         end
        subplot(1,4,i)
        output = generateHeatMapSimple( ...
            allSubjectStats{ii}.xAllSaved{i}, ...
            allSubjectStats{ii}.yAllSaved{i}, ...
            'Bins', 25,...
            'StimulusSize', 20,...
            'AxisValue', 20,...
            'Uncrowded', 0,...
            'Borders', 1);
        title(allSubjectStats{ii}.ecc(counter))
        line([0 0],[-10 10]);
        line([-10 10],[0 0]);
        axis square;
        counter = counter + 1;
    end
    suptitle(subjectsAll{ii});
    
end

%% Estimate Stimulation Strength by Acuity


close all
numCones(find((numCones == 0))) = NaN;
aoDens(find((aoDens == 0))) = NaN;
idx = find(~isnan(reshape(numCones,1,[])));
rightCones = reshape(numCones,1,[]);
rightAcuity = reshape(acuity,1,[]);
rightCD = reshape(aoDens,1,[]);
rightDC = reshape(dc,1,[]);

cm = jet(length(subjectsAll));

figure;
% fmt = {'*','o','^','.'};
for ii = 1:length(subjectsAll)
    subplot(3,4,ii)
%         for e = 1:4
    plot(aoDens(ii,:),...
        dc(ii,:),'-o')
    hold on
    legs(1) = plot(aoDens(ii,1),...
        dc(ii,1),'*','Color','r');
    hold on
%         end
end
legend(legs,{'CDC'});
xlabel('Cone Density');
ylabel('DC');

figure;
mdl1 = fitlm(numCones(:,1),...
    acuity(:,1));
subplot(2,2,1);plot(mdl1)
xlabel('Number Stimulated Cones');
ylabel('Acuity at 0ecc');
% fmt = {'*','o','^','.','+','x','s','p','h'};
for ii = 1:length(subjectsAll)
    hold on
    forleg(1) = plot(numCones(ii,1),acuity(ii,1),'o',...
        'MarkerFaceColor',cm(ii,[1:3]),'MarkerSize',5);
    hold on
end
legend off
subtitle(sprintf('p = %.2f',mdl1.Coefficients.pValue(2)));


mdl2 = fitlm(numCones(:,1),...
    dc(:,1));
subplot(2,2,2);plot(mdl2)
xlabel('Number Stimulated Cones');
ylabel('DC');

mdl3= fitlm(rightCD,...
    rightDC);
subplot(2,2,[3 4]);plot(mdl3)
xlabel('Cone Density at Ecc');
ylabel('DC');
% hold on
% cm = jet(4);
% for ii = 1:4;%length(subjectsAll)
%     hold on
%     plot(aoDens(:,ii),...
%     dc(:,ii),'o','MarkerFaceColor',cm(ii,[1:3]));
% end
fmt = {'*','o','^','.','+','x','s','p','h'};
for ii = 1:length(subjectsAll)
    for e = 1:4
        hold on
        if e == 1
            forleg(1) = plot(aoDens(ii,1),dc(ii,1),fmt{ii},'Color','r','MarkerSize',12);
        end
        if e == 2
            forleg(2) = plot(aoDens(ii,2),dc(ii,2),fmt{ii},'Color','g','MarkerSize',12);
        end
        if e == 3
            forleg(3) = plot(aoDens(ii,3),dc(ii,3),fmt{ii},'Color','b','MarkerSize',12);
        end
        if e == 4
            forleg(4) = plot(aoDens(ii,4),dc(ii,4),fmt{ii},'Color','c','MarkerSize',12);
        end
        hold on
    end
    hold on
end
legend off
legend(forleg,{'0ecc','10ecc','15ecc','25ecc'},'Location','northeast')


%%
figure;
valsKepp = ~isnan(aoDens(:)');
[~,p,b,r] = LinRegression((aoDens(valsKepp)),(acuity(valsKepp)),0,NaN,1,0);
xlabel('AO Density');
ylabel('Snellen Acuity');
ylim([10 30])

ylim([10 35])
xlim([4000 16000])
clrsString = {'r','g','y','b'};
fmt = {'-*','-o','-+','-x','-s','-p','-h','-^','-.',};
for ii = 1:length(subjectsAll)-1
%     for i = 1:4
        hold on
        forlegs(i) = plot(aoDens(ii,:),acuity(ii,:),fmt{ii},...
            'Color',cm(ii,[1:3]),'MarkerFaceColor',cm(ii,[1:3]),...
            'MarkerSize',10);
%     end
end


figure;
valsKepp = ~isnan(aoDens(:)');
[~,p,b,r] = LinRegression((aoDens(valsKepp)),(dc(valsKepp)),0,NaN,1,0);
xlabel('AO Density');
ylabel('D');

ylim([0 30])
xlim([4000 16000])
clrsString = {'r','g','y','b'};
fmt = {'-*','-o','-+','-x','-s','-p','-h','-^','-.',};
for ii = 1:length(subjectsAll)
%     for i = 1:4
        hold on
        forlegs(i) = plot(aoDens(ii,:),dc(ii,:),fmt{ii},...
            'Color',cm(ii,[1:3]),'MarkerFaceColor',cm(ii,[1:3]),...
            'MarkerSize',10);
%     end
end

%%

figure;
    subplot(1,3,1)
    for ii = 1:length(subjectsAll)
        plot([0 10 15 25],dc(ii,:),'-o');
        hold on
    end
    errorbar([0 10 15 25],mean(dc),std(dc),'o','Color','k','MarkerFaceColor','k');
    ylabel('D (arcmin^2/sec)');
    xlabel('Eccentricity (arcmin)');
    axis square
    subplot(1,3,2)
%     
% figure;
    for ii = 1:length(subjectsAll)
        plot([0 10 15 25],acuity(ii,:),'-o');
        hold on
    end
    errorbar([0 10 15 25],mean(acuity),std(acuity),'o','Color','k','MarkerFaceColor','k');
    ylabel('Acuity (20/X Snellen Eye Chart)');
    xlabel('Eccentricity (arcmin)');
    xlim([-1 26])
    ylim([10 35])
    axis square
    [h,p1] = ttest(acuity(:,1),acuity(:,4))
    [h,p2] = ttest(acuity(:,2),acuity(:,4))
    [h,p3] = ttest(acuity(:,3),acuity(:,4))
    [h,p4] = ttest(acuity(:,1),acuity(:,3))


%     line([0 25],[32 32])
groups = {[0 25],[10 25],[15 25]};
    H=sigstar(groups,[p1 p2 p3]);

    
    
    subplot(1,3,3)
    figure;
    for ii = 1:length(subjectsAll)
        plot([0 10 15 25],aoDens(ii,:),'-o');
        hold on
    end
    errorbar([0 10 15 25],nanmean(aoDens),nanstd(aoDens),'o','Color','k','MarkerFaceColor','k');
    ylabel('AO Cone Density (deg^2)');
    xlabel('Eccentricity (arcmin)');
    axis square

    mdl = fitlm(aoDens(:,1),...
    acuity(:,1));
    figure;
    plot(mdl)
    xlabel('Cone Density');
    ylabel('Acuity');
        xlim([-1 26])

    title('At the PRL');
    
    
%%
mdl = fitlm(rightCones(idx),...
    rightAcuity(idx));
figure;plot(mdl)
xlabel({'Stimulation Strength','(Cones from Drift)'});
ylabel('Snellen Acuity (20/X)');
fmt = {'*','o','^','.','+','x','s','p','h'};
for ii = 1:length(subjectsAll)
    hold on
    idxDC = find(flatDC(ii,:));
    if ismember(1,idxDC)
        forleg(1) = plot(numCones(ii,1)',acuity(ii,1)',fmt{ii},'Color','r','MarkerSize',12);
    end
    if ismember(2,idxDC)
        forleg(2) = plot(numCones(ii,2)',acuity(ii,2)',fmt{ii},'Color','g','MarkerSize',12);
    end
    if ismember(3,idxDC)
        forleg(3) = plot(numCones(ii,3)',acuity(ii,3)',fmt{ii},'Color','b','MarkerSize',12);
    end
    if ismember(4,idxDC)
        forleg(4) = plot(numCones(ii,4)',acuity(ii,4)',fmt{ii},'Color','c','MarkerSize',12);
    end
    idxDC = [];
end
legend off
legend(forleg,{'0ecc','10ecc','15ecc','25ecc'},'Location','northeast')

%%
% samplingLimit = (1/2)*sqrt((2/(sqrt(3)))*angularDensity);
% coneSpacing = SamplingLimit^-1*60*(1/(sqrt(3)));
coneSizeAll = [0.5; 0.556; 0.625; 0.833]; %rough estimates from reinger
allAreas = [-30 30];
ecc = [0 10 15 25];
for ii = 1:length(subjectsAll)
    for e = 1:length(ecc)
        %         pairs = []; idx = [];
        samplingLimit = allSubjectStats{ii}.AOFreq(e); %(1/2)*sqrt((2/(sqrt(3)))*angularDensity);
        if samplingLimit == 0
            coneSpacing =coneSizeAll(e);
        else
            coneSpacing = samplingLimit^-1*60*(1/(sqrt(3)));
        end
        stimSize = (allSubjectStats{ii}.thresh(e))*5;%;2;%(arcmin)
        stimWidthRound =abs(round(stimSize) + round( (stimSize-round(stimSize))/0.5) * 0.5);
%         stimSizeInCones = -(stimSize / coneSpacing)/2:0.5:(stimSize / coneSpacing)/2;
        % figure;plot(x-x(1),y-y(1)); axis([-5 5 -5 5])
        stimSizeInCones = (stimSize / coneSpacing);
        
        area = allAreas(1,:); %in arcminutes
        idx = find(allSubjectStats{ii}.xAllSaved{e} < area(2) &...
            allSubjectStats{ii}.xAllSaved{e} > area(1) & ...
            allSubjectStats{ii}.yAllSaved{e} < area(2) &...
            allSubjectStats{ii}.yAllSaved{e} > area(1));
        if isempty(idx)
            numConesFromTrace(ii,e) = NaN;
            continue;
        end
        
       
        pairs = [allSubjectStats{ii}.xAllSaved{e}(idx)',...
            allSubjectStats{ii}.yAllSaved{e}(idx)'];

        xyPair = pairs;
%         counter = length(pairs);
%         for i = 1:length(pairs)
%             for j = ((-stimSizeInCones/2):coneSpacing:(stimSizeInCones/2))
%                  counter = counter + 1;
%                 xyPair(counter,:) = [pairs(i,1)+j pairs(i,2)+j]; 
%             end
%         end
        
        pairs2cones{ii,e} = floor(xyPair) + ceil( (xyPair-floor(xyPair))/coneSpacing) * coneSpacing;
        [B, ~, ib] = unique(pairs2cones{ii,e}, 'rows');
        numoccurences = accumarray(ib, 1);
        numConesFromTrace(ii,e) = length(B);
        % indices = accumarray(ib, find(ib), [], @(rows){rows});  %the find(ib) simply generates (1:size(a,1))'
    end
end

numConesFromTrace(numConesFromTrace==400)=nan;
for ii = 1:length(subjectsAll)
    figure;
    for e = 1:length(ecc)
        stimSameCones(ii,e) = length(pairs2cones{ii,e});
        subplot(2,2,e)
        plot(pairs2cones{ii,e}(:,1)+ecc(e),pairs2cones{ii,e}(:,2)','o')
        axis([-15+ecc(e) 15+ecc(e) -15 15]);
    end
end
% firgure;
% [~,p,b,r] = LinRegression(numConesFromTrace(:),acuity(:),0,NaN,1,0);
% numConesNot0 = numConesFromTrace(:,[2 3 4]);
% numAcuityNot0 = acuity(:,[2 3 4]);
% numConesNot0 = numConesFromTrace(:,[1]);
% numAcuityNot0 = acuity(:,[1]);
% mdl = fitlm(numConesNot0(:),numAcuityNot0(:))

mdl = fitlm(numConesFromTrace(:),acuity(:))
figure;plot(mdl)
xlabel({'Stimulation Strength','(Cones from Drift)'});
ylabel('Snellen Acuity (20/X)');
hold on
legend off
fmt = {'*','o','^','.','+','x','s','p','h'};
for ii = 1:length(subjectsAll)
    forleg(1) = plot(numConesFromTrace(ii,1)',acuity(ii,1)',fmt{ii},'Color','r','MarkerSize',12);
    forleg(2) = plot(numConesFromTrace(ii,2)',acuity(ii,2)',fmt{ii},'Color','g','MarkerSize',12);
    forleg(3) = plot(numConesFromTrace(ii,3)',acuity(ii,3)',fmt{ii},'Color','b','MarkerSize',12);
    forleg(4) = plot(numConesFromTrace(ii,4)',acuity(ii,4)',fmt{ii},'Color','c','MarkerSize',12);
end
legend(forleg,{'0ecc','10ecc','15ecc','25ecc'},'Location','northwest')
% set(gsb,'MarkerSize',2);

md2 = fitlm(stimSameCones(:),acuity(:))
figure;plot(md2)
ylabel('Acuity Threshold')
xlabel('Cones Stimulated (1 cone can be twice)')
title(sprintf('p = %.2f',mdl.Coefficients.pValue(2)))


%% Nyquist Freuquency
figure;
for ii = 1:length(subjectsAll)
    subplot(2,4,ii)
    figure(100)
    dcFreq = powerAnalysis_JustCriticalFrequency(dc(ii,:), 0);
    close Figure 100
    aoFreq  = (1/2)*(sqrt((2/sqrt(3)) * aoDens(ii,:)));
    acuityFreq  = (60./((acuity(ii,:)./20)*5))*2.5;
    
    forlegs(1) = plot(ecc,dcFreq,'-s');
    hold on
    forlegs(2) = plot(ecc,aoFreq,'-o');
    hold on
    forlegs(3) = plot(ecc,acuityFreq,'-d');
ylim([5 80])
end
subplot(2,4,1)
xlabel('Ecc');
ylabel('Nyquist Freq')
legend(forlegs,{'D','AO','Acuity'});

%% Martina Discussion - what about 0 and 10?
% figure;

figure;
for ii = 1:length(subjectsAll)
    plot([1 2], [freqInfo.acuity(ii,1)-freqInfo.acuity(ii,4),...
        freqInfo.ao(ii,1)-freqInfo.ao(ii,4)],'o');
    hold on
end
errorbar([1 2], [mean(freqInfo.acuity(:,1)-freqInfo.acuity(:,4)),...
        mean(freqInfo.ao(:,1)-freqInfo.ao(:,4))],...
        [sem(freqInfo.acuity(:,1)-freqInfo.acuity(:,4)),...
        sem(freqInfo.ao(:,1)-freqInfo.ao(:,4))],...
        '-o','Color','k','MarkerFaceColor','k');
ylabel('\Delta CPD 0 to 25')
xticks([1 2])
xticklabels({'Acuity','AO'});
xlim([0 3])
[h,p] = ttest([(freqInfo.acuity(:,1)-freqInfo.acuity(:,4))],...
        [(freqInfo.ao(:,1)-freqInfo.ao(:,4))])

figure;
for ii = 1:length(subjectsAll)
    subplot(1,2,1)
    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 10],[freqInfo.ao(ii,1) freqInfo.ao(ii,2)],'-o');
    hold on
end
errorbar([0 10], [mean(freqInfo.ao(:,1)),...
        mean(freqInfo.ao(:,2))],...
        [sem(freqInfo.ao(:,1)),...
        sem(freqInfo.ao(:,2))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 15])
ylim([50 90])
xticks([0 10])
ylabel('Nyquist Limit Based on Cone Sampling');
xlabel('Eccentricity (arcmin)')
[h,p] = ttest(freqInfo.ao(:,1),freqInfo.ao(:,2))

for ii = 1:length(subjectsAll)
    subplot(1,2,2)
    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 10],[freqInfo.acuity(ii,1) freqInfo.acuity(ii,2)],'-o');
    hold on
end
errorbar([0 10], [mean(freqInfo.acuity(:,1)),...
        mean(freqInfo.acuity(:,2))],...
        [sem(freqInfo.acuity(:,1)),...
        sem(freqInfo.acuity(:,2))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 15])
ylim([20 60])
xticks([0 10])
ylabel('Nyquist Limit Based on Visual Acuity Thresholds');
xlabel('Eccentricity (arcmin)')

%%
figure;
for ii = 1:length(subjectsAll)
    subplot(1,3,1)
    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 10 15 25],[freqInfo.ao(ii,1) freqInfo.ao(ii,2) freqInfo.ao(ii,3) freqInfo.ao(ii,4)],'-o');
    hold on
end
errorbar([0 10 15 25], [mean(freqInfo.ao(:,1)),...
        mean(freqInfo.ao(:,2)) mean(freqInfo.ao(:,3)) mean(freqInfo.ao(:,4))],...
        [sem(freqInfo.ao(:,1)),...
        sem(freqInfo.ao(:,2)) sem(freqInfo.ao(:,3)) sem(freqInfo.ao(:,4))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 30])
ylim([40 90])
% xticks([0 10])
ylabel('Nyquist Limit (Retina)');
xlabel('Eccentricity (arcmin)')


for ii = 1:length(subjectsAll)
    subplot(1,3,2)
    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 10 15 25],[freqInfo.acuity(ii,1) freqInfo.acuity(ii,2) freqInfo.acuity(ii,3) freqInfo.acuity(ii,4)],'-o');
    hold on
end
errorbar([0 10 15 25], [mean(freqInfo.acuity(:,1)),...
        mean(freqInfo.acuity(:,2)) mean(freqInfo.acuity(:,3)) mean(freqInfo.acuity(:,4))],...
        [sem(freqInfo.acuity(:,1)),...
        sem(freqInfo.acuity(:,2)) sem(freqInfo.acuity(:,3)) sem(freqInfo.acuity(:,4))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 30])
ylim([10 60])
% xticks([0 10])
ylabel('Acuity Frequency');
xlabel('Eccentricity (arcmin)')

for ii = 1:length(subjectsAll)
    subplot(1,3,3)
    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 10 15 25],[freqInfo.dc(ii,1) freqInfo.dc(ii,2) freqInfo.dc(ii,3) freqInfo.dc(ii,4)],'-o');
    hold on
end
errorbar([0 10 15 25], [mean(freqInfo.dc(:,1)),...
        mean(freqInfo.dc(:,2)) mean(freqInfo.dc(:,3)) mean(freqInfo.dc(:,4))],...
        [sem(freqInfo.dc(:,1)),...
        sem(freqInfo.dc(:,2)) sem(freqInfo.dc(:,3)) sem(freqInfo.dc(:,4))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 30])
ylim([10 60])
% xticks([0 10])
ylabel('Drift Frequency (D)');
xlabel('Eccentricity (arcmin)')
[h,p] = ttest(freqInfo.dc(:,1),freqInfo.dc(:,2))


%%
isVals = [1 2 3 4 ...
        1 2 3 4 ...
        1 2 3 4 ...
        1 2 3 4 ...
        1 2 3 4 ...
        1 2 3 4 ...
        1 2 3 4 ...
        1 2 3 4];
[~,~,stats] = anovan(reshape([aoDens],1,[]),{isVals});
[h,p] = ttest(freqInfo.ao(:,1),freqInfo.ao(:,2))
[results,~,~,gnames] = multcompare(stats);

%% For VSS

fg = figure;%('Position',[100 0 400 1200]);
subplot(3,4,[1 2 5 6])
figure;
    for ii = 1:length(subjectsAll)
        plot([0 10 15 25],acuity(ii,:),'-o');
        hold on
    end
    errorbar([0 10 15 25],mean(acuity),sem(acuity),'o','Color','k','MarkerFaceColor','k');
    ylabel('Acuity (20/X Snellen Eye Chart)');
    xlabel('Eccentricity (arcmin)');
    xlim([-1 26])
    ylim([10 35])
    axis square
    [h,p1] = ttest(acuity(:,1),acuity(:,4))
    [h,p2] = ttest(acuity(:,2),acuity(:,4))
    [h,p3] = ttest(acuity(:,3),acuity(:,4))
    [h,p4] = ttest(acuity(:,1),acuity(:,3))


%     line([0 25],[32 32])
groups = {[0 25],[10 25],[15 25]};
    H=sigstar(groups,[p1 p2 p3]);
    
    
subplot(3,4,[3 4 7 8])
    for ii = 1:length(subjectsAll)
        plot([0 10 15 25],aoDens(ii,:),'-o');
        hold on
    end
    errorbar([0 10 15 25],mean(aoDens),sem(aoDens),'o','Color','k','MarkerFaceColor','k');
    ylabel('Cone Density (cones/deg^2)');
    xlabel('Eccentricity (arcmin)');
    xlim([-1 26])
%     ylim([10 35])
    axis square
    [h,p1] = ttest(aoDens(:,1),aoDens(:,4))
    [h,p2] = ttest(aoDens(:,2),aoDens(:,4))
    [h,p3] = ttest(aoDens(:,3),aoDens(:,4))
    [h,p4] = ttest(aoDens(:,1),aoDens(:,3))   
    [h,p5] = ttest(aoDens(:,1),aoDens(:,2))    

 groups = {[0 25],[10 25],[15 25],[0,15],[0 10]};
    H=sigstar(groups,[p1 p2 p3 p4 p5]);   
    
    
    
% figure;
for ii = 1:length(subjectsAll)
    sp(9) =subplot(3,4,9)
    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 10],[freqInfo.ao(ii,1) freqInfo.ao(ii,2)],'-o');
    hold on
end
errorbar([0 10], [mean(freqInfo.ao(:,1)),...
        mean(freqInfo.ao(:,2))],...
        [sem(freqInfo.ao(:,1)),...
        sem(freqInfo.ao(:,2))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 15])
ylim([50 90])
xticks([0 10])
ylabel('Nyquist Limit Based on Cone Sampling');
xlabel('Eccentricity (arcmin)')
[h,p] = ttest(freqInfo.ao(:,1),freqInfo.ao(:,2))
groups = {[0 10]};
H=sigstar(groups,[p]);


for ii = 1:length(subjectsAll)
    sp(10) = subplot(3,4,10)
    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 10],[freqInfo.acuity(ii,1) freqInfo.acuity(ii,2)],'-o');
    hold on
end
errorbar([0 10], [mean(freqInfo.acuity(:,1)),...
        mean(freqInfo.acuity(:,2))],...
        [sem(freqInfo.acuity(:,1)),...
        sem(freqInfo.acuity(:,2))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 15])
ylim([20 60])
xticks([0 10])
ylabel('Nyquist Limit Based on VA Thresholds');
xlabel('Eccentricity (arcmin)')
% [h,p] = ttest(freqInfo.acuity(:,1),freqInfo.acuity(:,2))
% groups = {[0 10]};
% H=sigstar(groups,[p]);



for ii = 1:length(subjectsAll)
    sp(11) = subplot(3,4,11);

    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 25],[freqInfo.ao(ii,1) freqInfo.ao(ii,4)],'-o');
    hold on
end
errorbar([0 25], [mean(freqInfo.ao(:,1)),...
        mean(freqInfo.ao(:,4))],...
        [sem(freqInfo.ao(:,1)),...
        sem(freqInfo.ao(:,4))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 30])
ylim([35 80])
xticks([0 25])
ylabel('Nyquist Limit Based on Cone Sampling');
xlabel('Eccentricity (arcmin)')
[h,p] = ttest(freqInfo.ao(:,1),freqInfo.ao(:,4))
groups = {[0 25]};
H=sigstar(groups,[p]);

for ii = 1:length(subjectsAll)
    sp(12) = subplot(3,4,12)
    %     optics(ii) = (aoFreq(ii,1)-aoFreq(ii,2));
    plot([0 25],[freqInfo.acuity(ii,1) freqInfo.acuity(ii,4)],'-o');
    hold on
end
errorbar([0 25], [mean(freqInfo.acuity(:,1)),...
        mean(freqInfo.acuity(:,4))],...
        [sem(freqInfo.acuity(:,1)),...
        sem(freqInfo.acuity(:,4))],...
        '-o','Color','k','MarkerFaceColor','k');
xlim([-5 30])
ylim([10 55])
xticks([0 25])
ylabel('Nyquist Limit Based on VA Thresholds');
xlabel('Eccentricity (arcmin)')
[h,p] = ttest(freqInfo.acuity(:,1),freqInfo.acuity(:,4))

groups = {[0 25]};
H=sigstar(groups,[p]);

AddLetters2Plots(fg, 'HShift', 0, 'VShift', 0, 'Direction', 'LeftRight')

% spPos = cat(1,sp([9 12]).Position);
% titleSettings = {'HorizontalAlignment','center','EdgeColor','none','FontSize',10};
% annotation('textbox','Position',[spPos(1,1:2) 0.3 0.3],'String','10 ecc',titleSettings{:})
% annotation('textbox','Position',[spPos(1,3:4) 0.3 0.3],'String','25 ecc',titleSettings{:})


