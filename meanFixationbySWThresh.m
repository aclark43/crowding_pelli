function meanFixationbySWThresh(subjectCro, subjectThreshUnc, subjectThreshCro, subNum, em, thresholdSWVals, c)

figure
for ii = 1:subNum
    swUnc = thresholdSWVals(ii).thresholdSWUncrowded;
    thresholdUncrowdedSpan{ii} = subjectThreshUnc(ii).em.ecc_0.(swUnc).meanSpan;
    thresholdSW{ii} = (subjectThreshUnc(ii).thresh);
end
thresholdSize1 = cell2mat(thresholdSW);
thresholdSpan1 = cell2mat(thresholdUncrowdedSpan);

sz = 200;
[~,p,~,r] = LinRegression(thresholdSpan1,thresholdSize1,0,NaN,1,0);

for x = 1:subNum
    leg(x) = scatter(thresholdSpan1(x),thresholdSize1(x),sz,[c(x,1) c(x,2) c(x,3)],'filled');%,'Linewidth',5)
end
xValues = unique(round(sort(thresholdSpan1),1));
% set(gca, 'XTick', xValues, 'xticklabel', {xValues})
xlabel('Mean Drift Span','FontSize',24)

yValues = unique(round(sort(thresholdSize1),1));
% set(gca, 'YTick', yValues, 'yticklabel', {yValues})
ylabel('Strokewidth Threshold','FontSize',24)

axis([1 8 0.5 4])

text(2,3,sprintf('p = %.3f',p),'FontSize',18)
text(2,2.5,sprintf('r = %.3f',r),'FontSize',18)

dx = 0.1; dy = 0.1; % displacement so the text does not overlay the data points




%% Quick check on OCT ANalysis

% for ii = 1:subNum
%     text(thresholdSpan1(1,ii)+dx, thresholdSize1(1,ii), subjectsAll{ii});
% end


% title('Uncrowded')
% axis([1.5 7 .5 5])
% legend(leg,subjectsAll)
coeffs = polyfit(thresholdSpan1, thresholdSize1, 1);
% Get fitted values
fittedX = linspace(min(thresholdSpan1), max(thresholdSpan1), 200);
fittedY = polyval(coeffs, fittedX);
hold on;


% mdl = fitlm(thresholdSpan1,thresholdSize1);
% %[h1,p1,ci1,stats1] = ttest(thresholdSpan,thresholdSize);
% p1 = mdl.Coefficients.pValue(2);

% thresholdSpan1 = thresholdSpan;
condition = {'Uncrowded'};
if strcmp('Drift',em)
    title({'Span by Threshold Strokewidth','Uncrowded'})
    saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjSpanbyStrokewidth%s.png', condition{1}));
elseif strcmp('Drift_MS',em)
    title({'Span by Threshold Strokewidth first 300','Uncrowded'})
    saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjSpanbyStrokewidthFirst300%s.png', condition{1}));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% subplot (1,2,2)
% clear mdl
clear thresholdSpan
clear thresholdSize
% hold on
figure

for ii = 1:subNum
    thresholdSizeSWRound = round(subjectCro(ii).stimulusSize,1);
    matchingThresholdRound = round(subjectThreshCro(ii).thresh,1);
    [~,thresholdIdx] = min(abs(matchingThresholdRound - thresholdSizeSWRound));
    swCro = sprintf('strokeWidth_%i', subjectCro(ii).SW(thresholdIdx));
%     if strcmp('Drift',em)
        thresholdCrowdedSpan{ii} = subjectThreshCro(ii).em.ecc_0.(swCro).meanSpan;%subjectUnc(ii).meanDriftSpan(thresholdIdx);
%     elseif strcmp('Drift_MS',em)
%         thresholdCrowdedSpan{ii} = subjectThreshCro(ii).em.First300.ecc_0.(swCro).meanSpan;%subjectUnc(ii).meanDriftSpan(thresholdIdx);
%     end
    thresholdCroSW{ii} = subjectThreshCro(ii).thresh;
end
thresholdSize2 = cell2mat(thresholdCroSW);
thresholdSpan2 = cell2mat(thresholdCrowdedSpan);

sz = 300;
% c = jet(subNum);%linspace(1,20,length(thresholdSize2));

hold on
% [h1,p1,ci1,stats1] = ttest(thresholdSpan1,thresholdSize1);
[~,p,~,r] = LinRegression(thresholdSpan2,thresholdSize2,0,NaN,1,0);

% scatter(thresholdSpan2,thresholdSize2,sz,c,'filled','d','Linewidth',5)
for ii = 1:subNum
leg(ii) = scatter(thresholdSpan2(ii),thresholdSize2(ii),sz,[c(ii,1) c(ii,2) c(ii,3)],'filled','Linewidth',5);
end

% legend(leg, subjectsAll)
xValues = unique(round(sort(thresholdSpan2),1));
% set(gca, 'XTick', xValues, 'xticklabel', {xValues})
xlabel('Mean Drift Span','FontSize',24)

yValues = unique(round(sort(thresholdSize2),1));
% set(gca, 'YTick', yValues, 'yticklabel', {yValues})
ylabel('Strokewidth Threshold','FontSize',24)

axis([1 7 1 3.5])

text(5,1,sprintf('p = %.3f',p),'FontSize',18)
text(5,.5,sprintf('r = %.3f',r),'FontSize',18)

dx = 0.1; dy = 0.1; % displacement so the text does not overlay the data points
condition = {'Crowded'};
if strcmp('Drift',em)
    title({'Span by Threshold Strokewidth','Crowded'},'FontSize',20)
    saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjSpanbyStrokewidth%s.png', condition{1}));
elseif strcmp('Drift_MS',em)
    title({'Span by Threshold Strokewidth First 300','Crowded'})
    saveas(gcf,sprintf('../../Data/AllSubjTogether/AllSubjSpanbyStrokewidthFirst300%s.png', condition{1}));
end
% axis([1.5 7 .5 5])

% coeffs2 = polyfit(thresholdSpan2, thresholdSize2, 1);
% % Get fitted values
% fittedX2 = linspace(min(thresholdSpan2), max(thresholdSpan2), 200);
% fittedY2 = polyval(coeffs2, fittedX2);
%
% % hold on;
%
%
% axis([0.5 7 0 4.5])
% [h2,p2,ci2,stats2] = ttest(thresholdSpan2,thresholdSize2);
% mdl = fitlm(thresholdSpan1,thresholdSize1);
% md2 = fitlm(thresholdSpan2,thresholdSize2);
% % p2 = md2.Coefficients.pValue(2);
%
% legend('Uncrowded','Crowded','Location','northwest')
%
% format longG
% rSquare1 = round(mdl.Rsquared.Ordinary,3);
% rSquare2 = round(md2.Rsquared.Ordinary,3);
%
% pValue1 = sprintf('Uncrowded p = %.3f', p1);
% pValue2 = sprintf('Crowded p = %3f', p2);
% rValue1 = sprintf('Uncrowded r^2 = %.3f', rSquare1);
% rValue2 = sprintf('Crowded r^2 = %.3f', rSquare2);
% text(4.75,1.5,rValue1,'Fontsize',15);
% text(4.75,1.5,rValue2,'Fontsize',15);
% text(4.75,1.5,pValue1,'Fontsize',15);
% text(4.75,1.5,pValue2,'Fontsize',15);
%
% plot(fittedX2, fittedY2, '--', 'LineWidth', 1);
% plot(fittedX, fittedY, 'r--', 'LineWidth', 1);
%
% legend('Uncrowded','Crowded','Location','northwest')
%
% Span_mean_Uncrowded = thresholdSpan1';
% Span_mean_Crowded = thresholdSpan2';
% spanTable = table(Name,Span_mean_Uncrowded, Span_mean_Crowded);

end

