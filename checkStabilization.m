% script to compare position with stabilized trace
load('\\casfsb\APLAB\JanisData\FovealCrowding\Data\Stabilized-NoGlasses\pptrials.mat','data');
for i = 1:length(data.x)
    figure(1); clf;
    subplot(2, 1, 1); hold on;
    plot(data.stream00{i}.ts, data.stream00{i}.data*.24);
    plot(data.x{i});
    title('Horizontal');
    xlabel('time (ms)');
    ylabel('arcmin');
    
    subplot(2, 1, 2); hold on;
    plot(data.stream01{i}.ts, data.stream01{i}.data*.24);
    plot(data.y{i});
    title('Vertical');
    xlabel('time (ms)');
    ylabel('arcmin');
    
    print(sprintf('checkStabilization_%i.png', i), '-dpng');
    
end