function findUncrowdedvsCrowdedRates(subjectMSUnc,subjectMSCro, subNum, subjectThreshCro, subjectThreshUnc)
%find the difference in MSrates from uncrowded and crowded conditions

msRatesU = getRates(subjectMSUnc, subNum);
msRatesC = getRates(subjectMSCro, subNum);

msRatesUSaved = [1.66433351656795,0,1.42670395817410,1.84279123591895,...
    1.31476550073307,1.46157458631352,2.39370155831958,2.13017123169948,...
    0,1.96074894796252];

msRatesFix = [0.983448330160877,0.942283403285698,1.49054405657832,1.30932978860189,1.91649426098946,1.40807075824448...
    1.41242177314011,1.48166069697109,2.29914130937470,0.787980685188240,1.75556515833540];

for ii = 1:subNum
    if isnan(msRatesU(ii))
        msRatesU(ii) = 0;
    end
end
%[0.917885108 0.326175024	1.490544057	1.309329789	1.916494261...		
%1.259944423	1.481660697	2.299141309	0.773113125	1.755565158	NaN	1.251618452];

% for ii = 1:subNum
% msRatesU(2,ii) = subjectThreshUnc(ii).thresh;
% msRatesC(2,ii) = subjectThreshCro(ii).thresh;
% end
% %%Purposely use msRatesU in both conditions because uncrowded condition 
% %%has more SW without enough ms to analyze
% nanValsIdx = ~isnan(msRatesU(1,:));
% figure;
% [~,p,~,r] = LinRegression(msRatesU(1,nanValsIdx),msRatesU(2,nanValsIdx),0,NaN,1,0);
% 
% nanValsIdx = ~isnan(msRatesC(1,:));
% figure;
% [~,p,~,r] = LinRegression(msRatesC(1,nanValsIdx),msRatesC(2,nanValsIdx),0,NaN,1,0);
% for ii = 1:length(msRatesU)
%     if isnan(msRatesU(ii))
%         msRatesU(ii) = 0;
%     end
%     if isnan(msRatesC(ii))
%         msRatesC(ii) = 0;
%     end
% end

figure;
for i = 1:length(msRatesU)
    scatter(0,msRatesU(i),100,'k');
    hold on
end
for i = 1:length(msRatesC)
    scatter(1,msRatesC(i),100,'k');
    hold on
end
for i = 1:length(msRatesFix)
    scatter(2,msRatesFix(i),100,'k');
    hold on
end
errorbar(0, nanmean(msRatesU), nanstd(msRatesU), 'o','Color','k', ...
'MarkerSize',10,'MarkerEdgeColor','black','MarkerFaceColor','black', 'Capsize',15)
hold on
errorbar(1, nanmean(msRatesC), nanstd(msRatesC), 'o','Color','k', ...
'MarkerSize',10,'MarkerEdgeColor','black','MarkerFaceColor','black', 'Capsize',15)
% scatter(1,nanmean(msRatesC),100,'k','filled');
hold on
errorbar(2, nanmean(msRatesFix), nanstd(msRatesFix), 'o','Color','k', ...
'MarkerSize',10,'MarkerEdgeColor','black','MarkerFaceColor','black', 'Capsize',15)
xlim([-0.5 2.5])
% scatter(2,nanmean(msRatesFix),100,'k','filled');

cleanMSRatesC  = msRatesC(~isnan(msRatesU) & ~isnan(msRatesFix));
cleanMSRatesU = msRatesU(~isnan(msRatesU) & ~isnan(msRatesFix));  
cleanMSRatesFix = msRatesFix(~isnan(msRatesU) & ~isnan(msRatesFix)); 
% 
% cleanMSRatesC  = msRatesC(~isnan(msRatesC));
% cleanMSRatesU = msRatesU(~isnan(msRatesU));  
% cleanMSRatesFix = msRatesFix(~isnan(msRatesFix));  

% 
% % cond = categorical ({'Uncrowded' 'Crowded'});
% figure
% scatter(zeros(1,length(cleanMSRatesU)),cleanMSRatesU,100,'k');
% hold on
% scatter(ones(1,length(cleanMSRatesC)),cleanMSRatesC,100,'k');
% hold on
% scatter([2 2 2 2 2 2 2 2 2],cleanFixRates, 100, 'k');
% hold on
% scatter(0,mean(cleanMSRatesU),150,'k','filled');
% hold on
% scatter(1,mean(cleanMSRatesC),150,'k','filled');
% hold on
% scatter(2,mean(cleanFixRates), 150,'k','filled');
% 
% meanAll = [mean(cleanMSRatesU) mean(cleanMSRatesC) mean(cleanFixRates)];
% stdAll = [std(cleanMSRatesU) std(cleanMSRatesC) std(cleanFixRates)];
% hold on
% er = errorbar([0 1 2]...
%     ,meanAll, stdAll);
% er.Color = [0 0 0];                            
% er.LineStyle = 'none';  
[~,p1,ci] = ttest([cleanMSRatesU], cleanMSRatesC);
[~,p2,ci] = ttest(cleanMSRatesC, cleanMSRatesFix);
[~,p3,ci] = ttest([cleanMSRatesU], cleanMSRatesFix);


text(0,2.6,sprintf('U vs C p=%.3f',p1))
text(1,2.6,sprintf('C vs F p=%.3f',p2))
text(2,2.6,sprintf('U vs F p=%.3f',p3))

% er = errorbar((categorical([{'Landing on Target or Flanker'} {'Landing on Background'}]))...
%     ,meanAll, ci);
hold off
axis([-0.5 2.5 0 2.8])
xlabel('Condition')
ylabel('Microsaccade Rate')
names = {'Uncrowded',...
   'Crowded','Fixation'};
set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',12,...
    'FontWeight','bold')
title('Rate of Microsaccades by Condition')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSRatesConditionFixation.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSRatesConditionFixation.eps');

% 
% 
% 
% bar(categorical({'Uncrowded'}), mean(cleanMSRatesU), 'FaceColor', [0, 0.5, 0]);
% hold on
% bar(categorical({'Crowded'}), mean(cleanMSRatesC), 'FaceColor', [0, .5, .5]);

meanAll = [mean(cleanMSRatesU) mean(cleanMSRatesC) ];
stdAll = [std(cleanMSRatesU) std(cleanMSRatesC) ];
hold on
er = errorbar([0 1]...
    ,meanAll, stdAll);
er.Color = [0 0 0];                            
er.LineStyle = 'none';  
[~,p,ci] = ttest(cleanMSRatesU, cleanMSRatesC);
text(0,2,sprintf('p =  %.3f',p))
% er = errorbar((categorical([{'Landing on Target or Flanker'} {'Landing on Background'}]))...
%     ,meanAll, ci);
hold off
xlim([-0.5 1.5])
xlabel('Condition')
ylabel('Microsaccade Rate')
names = {'Uncrowded',...
   'Crowded'};
set(gca,'xtick',[0 1],'xticklabel', names,'FontSize',12,...
    'FontWeight','bold')
title('Rate of Microsaccades by Condition')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSRatesCondition.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSRatesCondition.eps');
%% Plot Threshold by MS Rate
figure;
counter = 1;
for ii = 1:subNum
    if ~isnan(msRatesU(ii))
        linX(counter) = msRatesU(ii);
        linY(counter) = subjectThreshUnc(ii).thresh;
        scatter(msRatesU(ii),subjectThreshUnc(ii).thresh)
        hold on
        counter = counter+1;
    end
end

[~,p,~,r] = LinRegression(linX,linY,0,NaN,1,0);
axis([1 2 -1 5])
xlabel('MS Rate')
ylabel('Threshold')

figure;
counter = 1;
for ii = 1:subNum
    if ~isnan(msRatesC(ii))
        linX(counter) = msRatesC(ii);
        linY(counter) = subjectThreshCro(ii).thresh;
        scatter(msRatesC(ii),subjectThreshCro(ii).thresh)
        hold on
        counter = counter+1;
    end
end

[~,p,~,r] = LinRegression(linX,linY,0,NaN,1,0);
axis([1 2 -1 5])
xlabel('MS Rate')
ylabel('Threshold')

%% MS Amplitude
[msAmpsU allAmpsU] = getAmplitudes(subjectMSUnc, subNum); %Amp, Angle, Velocity
[msAmpsC allAmpsC] = getAmplitudes(subjectMSCro, subNum);

fixAmps = ...
    [12.88012069	11.55884493	14.26265615	12.09154363	13.39020466...		
15.82658634	14.96841661	12.69900554	13.47422799	18.17838613	NaN	9.890955389];

fixAngles = ...
    [1.865266658	2.626422913	2.14600231	2.380220064	3.681200222	...	
3.201828483	3.220087407	3.483600565	4.290476283	3.176751693	NaN	2.4013379];%dDPI

% [velocityU allVelU] = getVelocities(subjectMSUnc, subNum);
% [velocityC allVelC] = getVelocities(subjectMSCro, subNum);

cleanMSAmpsC  = msAmpsC(~isnan(msAmpsU) & ~isnan(msAmpsC) & (~isnan(fixAmps)));
cleanMSAmpsU = msAmpsU(~isnan(msAmpsU) & ~isnan(msAmpsC) & (~isnan(fixAmps)));  
cleanMSAmpsF = fixAmps(~isnan(msAmpsU) & ~isnan(msAmpsC) & (~isnan(fixAmps)));  

% cond = categorical ({'Uncrowded' 'Crowded'});
figure
for ii = 1:length(cleanMSAmpsC)
     scatter(0,cleanMSAmpsU(ii),100,'k');
    hold on
    scatter(1,cleanMSAmpsC(ii),100,'k');
    hold on
    scatter(2,cleanMSAmpsF(ii),100,'k');
end
hold on
scatter(0,mean(cleanMSAmpsU),150,'k','filled');
hold on
scatter(1,mean(cleanMSAmpsC),150,'k','filled');
hold on
scatter(2,mean(cleanMSAmpsF),150,'k','filled');
meanAll = [nanmean(cleanMSAmpsU) nanmean(cleanMSAmpsC) nanmean(cleanMSAmpsF)];
stdAll = [nanstd(cleanMSAmpsU) nanstd(cleanMSAmpsC) nanstd(cleanMSAmpsF)];
% for ii = 1:length(fixAmps)
%     scatter(0,msAmpsC(ii),100,c(ii,:),'filled');
%     hold on
%     scatter(1,msAmpsU(ii),100,c(ii,:),'filled');
%     hold on
%     scatter(2,fixAmps(ii),100,c(ii,:),'filled');
% end
% hold on
% scatter(0,nanmean(msAmpsC),150,'k','filled');
% hold on
% scatter(1,nanmean(msAmpsU),150,'k','filled');
% hold on
% scatter(2,nanmean(fixAmps),150,'k','filled');
% 
% 
% meanAll = [nanmean(cleanMSAmpsU) nanmean(cleanMSAmpsC) nanmean(cleanMSAmpsF)];
% stdAll = [nanstd(cleanMSAmpsU) nanstd(cleanMSAmpsC) nanstd(cleanMSAmpsF)];
hold on
er = errorbar([0 1 2]...
    ,meanAll, stdAll);
er.Color = [0 0 0];                            
er.LineStyle = 'none';  
[~,p,ci] = ttest(cleanMSAmpsU, cleanMSAmpsC);
text(0.25,10,sprintf('p =  %.3f',p))
% er = errorbar((categorical([{'Landing on Target or Flanker'} {'Landing on Background'}]))...
%     ,meanAll, ci);
hold off
xlim([-0.5 2.5])
xlabel('Condition')
ylabel('Microsaccade Rate')
names = {'Uncrowded',...
   'Crowded','Fixation'};
set(gca,'xtick',[0 1 2],'xticklabel', names,'FontSize',12,...
    'FontWeight','bold')
title('Amplitude of Microsaccades by Condition')
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSAmplitudesCondition.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSAmplitudesCondition.epsc');




%% MS Direction
counter = 1;
for ii = 1:length(allAmpsC)
%     counter = 1;
    for i = 1:length(allAmpsU{1,ii})
%         allAmplitudesU(counter) = allAmpsU{1,ii}(i);
        allAnglesU(counter) = allAmpsU{2,ii}(i);
        counter = counter + 1;
    end
% subplot(3,4,ii);
% % subplot(1,2,1);
% c = linspace(0, 2*pi, 30);
% n1 = hist(allAnglesU, c); % histogram of saccade directions
% n1 = n1/ sum(n1); % normalize for empirical probability distribution
% % subplot(1,3,2);
% h1p = polar([c, c(1)], [n1, n1(1)]);  % repeat first point so that it goes all the way around
% % title({'MS Direction Uncrowded','(# of MS per Direction)'})
% % set(gcf, 'Position',  [600, 300, 1200, 700])
% % subplot(1,2,2);
% title(ii)
end
% suptitle('Uncrowded')
% figure;
counter = 1;
for ii = 1:length(allAmpsC)
%     counter = 1;
    for i = 1:length(allAmpsC{1,ii})
%         allAmplitudesC(counter) = allAmpsC{1,ii}(i);
        allAnglesC(counter) = allAmpsC{2,ii}(i);
        counter = counter + 1;
    end
%  subplot(3,4,ii);
%     microsaccadeDirections = [allAnglesC];
% c = linspace(0, 2*pi, 30);
% n1 = hist(microsaccadeDirections, c); % histogram of saccade directions
% n1 = n1/ sum(n1); % normalize for empirical probability distribution
% % subplot(1,3,2);
% h2p = polar([c, c(1)], [n1, n1(1)], 'k');  % repeat first point so that it goes all the way around
% % legend('Uncrowded','Crowded');
% title(ii);
end
% suptitle('Crowded')
[~,p,ci] = ttest(allAnglesU, cleanMSAmpsC);

figure;
c = linspace(0, 2*pi, 30);
n1 = hist( [allAnglesU], c); % histogram of saccade directions
n1 = n1/ sum(n1); % normalize for empirical probability distribution
h1p = polar([c, c(1)], [n1, n1(1)]);  % repeat first point so that it goes all the way around

hold on
c = linspace(0, 2*pi, 30);
n1 = hist([allAnglesC], c); % histogram of saccade directions
n1 = n1/ sum(n1); % normalize for empirical probability distribution
% subplot(1,3,2);
h2p = polar([c, c(1)], [n1, n1(1)]);  % repeat first point so that it goes all the way around
hold on

c = linspace(0, 2*pi, 30);
n1 = hist([fixAnglesAll], c); % histogram of saccade directions
n1 = n1/ sum(n1); % normalize for empirical probability distribution
% subplot(1,3,2);
h2p = polar([c, c(1)], [n1, n1(1)]);  % repeat first point so that it goes all the way around
hold on

legend('Uncrowded','Crowded','Fixation');
title('MS Angles');
% title({'MS Direction Crowded','(# of MS per Direction)'})
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSAnglesConditionUnc.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSAnglesCondition.epsc');

%% Linearity of Amplitudes
% counter = 1;
for ii = 1:length(allAmpsC)
    counter = 1;
    for i = 1:length(allAmpsU{1,ii})
        if allAmpsU{1,ii}(i) == 0
            allAmplitudesU(ii,counter) = [];
            continue;
        end
        allAmplitudesU(ii,counter) = allAmpsU{1,ii}(i);
%         allAnglesU(counter) = allAmpsU{2,ii}(i);
        counter = counter + 1;
    end
end

temp = find(allAmplitudesU == 0);
% allAmplitudesU(temp)

for ii = 1:length(allAmpsC)
    counter = 1;
    for i = 1:length(allAmpsC{1,ii})
%         if allAmpsC{1,ii}(i) == 0
%             allAmplitudesC(ii,counter) = [];
%             continue;
%         end
        allAmplitudesC(ii,counter) = allAmpsC{1,ii}(i);
%         allAnglesC(counter) = allAmpsC{2,ii}(i);
        counter = counter + 1;
    end
end

figure;
cParula = parula(length(allAmplitudesU));
for ii = 1:length(allAmplitudesU)
scatter(allAmplitudesC(:,ii), allAmplitudesU(:,ii),20,cParula(ii,:),'filled')
hold on
end
cb = colorbar;
axis square
axis([0 60 0 60])
xlabel('Uncrowded')
ylabel('Crowded')
title('MS Amplitudes');
set(cb, 'Ticks', [0 1], 'TickLabels', {'Small SW', 'Large SW'})
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSAmpsLinearCondition.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSAmpsLinearCondition.epsc');
%% Peak Velocity by Amplitude
for ii = 1:length(allAmpsU)
    counter = 1;
    for i = 1:length(allAmpsU{1,ii})
        if allAmpsU{1,ii}(i) == 0
            allAmplitudesU(ii,counter) = [];
            allVelU(ii,counter) = [];
            continue;
        end
        allAmplitudesU(ii,counter) = allAmpsU{1,ii}(i);
        allVelU(ii,counter) = double(allAmpsU{3,ii}(i));
%         allAnglesU(counter) = allAmpsU{2,ii}(i);
        counter = counter + 1;
    end
end
figure;
subplot(1,3,1);
cParula = parula(length(allAmplitudesU));
for ii = 1:length(allAmplitudesU)
scatter((allVelU(:,ii)/60), allAmplitudesU(:,ii)/60,20,cParula(ii,:),'filled')
% semilogx((allVelU(:,ii)/60), allAmplitudesU(:,ii),'o')
hold on
end
% cb = colorbar;
% axis square
axis([0 2000 0 1])
xlabel('Peak Velocity(arcmin/sec)')
ylabel('Amplitude (arcmin)')
title('Uncrowded')

for ii = 1:length(allAmpsC)
    counter = 1;
    for i = 1:length(allAmpsC{1,ii})
        if allAmpsC{1,ii}(i) == 0
            allAmplitudesC(ii,counter) = NaN;
            allVelC(ii,counter) = NaN;
            continue;
        end
        allAmplitudesC(ii,counter) = allAmpsC{1,ii}(i);
        allVelC(ii,counter) = allAmpsC{3,ii}(i);
%         allAnglesU(counter) = allAmpsU{2,ii}(i);
        counter = counter + 1;
    end
end

subplot(1,3,2);
cParula = parula(length(allAmplitudesC));
for ii = 1:length(allAmplitudesC)
scatter((allVelC(:,ii)/60), allAmplitudesC(:,ii)/60,20,cParula(ii,:),'filled')
hold on
end

% axis square
axis([0 20 0 1])
xlabel('Peak Velocity(arcmin/sec)')
ylabel('Amplitude (arcmin)')
title('Crowded')

subplot(1,3,3)
cb = colorbar;
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSAmpsVelCondition.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/MSAmpsVelCondition.epsc');
end



