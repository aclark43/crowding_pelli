function emRate = rateCalculation(pptrial, emType)

if pptrial.TimeFixationOFF > 0 %FIXATION TRIAL
    endTrial = pptrial.TimeFixationOFF;
    startTrial = pptrial.TimeFixationON;
% elseif pptrial.ResponseTime == 0
%     endTrial = pptrial.TimeTargetOFF;
%     startTrial
else
    endTrial = pptrial.TimeTargetOFF;
    startTrial = pptrial.TimeTargetON;
end

switch emType
    case 's'
        numEM = sum(pptrial.saccades.start < endTrial &...
            pptrial.saccades.start > startTrial);
    case 'ms'
        numEM = sum(pptrial.microsaccades.start < endTrial &...
            pptrial.microsaccades.start > startTrial);
end

emRate = (numEM/(endTrial-startTrial))*1000;

end