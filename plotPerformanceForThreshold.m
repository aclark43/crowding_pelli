function plotPerformanceForThreshold( subjectsAll, subjectThreshCro, subjectThreshUnc, c, subNum)
%Plots the difference in Performnace for threshold in the Uncrowded
%condition - ie Look at changes in performance when Uncrowded is at threshold, and crowded is at the matching size.

%% Uncrowded at Threshold 
for ii = 1:length(subjectsAll)
    swUncr(ii) = 1;
    swCro(ii) = 2;
    n = 0.625;
    [~,idxThresh] = min(abs(subjectThreshUnc(ii).performanceAtSizesY-n));
    thresholdUnc(ii) = subjectThreshUnc(ii).performanceAtSizesY(idxThresh)*100;
    sizeOfThreshUnc(ii) = subjectThreshUnc(ii).sizesTestedX(idxThresh);
    thresholdMatchCro(ii) = subjectThreshCro(ii).performanceAtSizesY(idxThresh)*100;
    sizeOfThreshMatchCro(ii) = subjectThreshCro(ii).sizesTestedX(idxThresh);
    cNew(ii,:) = [c(ii,1) c(ii,2) c(ii,3)];
%     crowdedThresh(ii) = (subjectThreshCro(ii).threshSW4 *100);
%     uncrowdedThresh(ii) = (subjectThreshUnc(ii).threshSW4 * 100);
end

%% Crowded at Threshold
% for ii = 1:length(subjectsAll)
%     swUncr(ii) = 1;
%     swCro(ii) = 2;
%     n = 0.625;
%     [~,idxThresh] = min(abs(subjectThreshCro(ii).performanceAtSizesY-n));
%     thresholdUnc(ii) = subjectThreshUnc(ii).performanceAtSizesY(idxThresh)*100;
%     sizeOfThreshUnc(ii) = subjectThreshUnc(ii).sizesTestedX(idxThresh);
%     thresholdMatchCro(ii) = subjectThreshCro(ii).performanceAtSizesY(idxThresh)*100;
%     sizeOfThreshMatchCro(ii) = subjectThreshCro(ii).sizesTestedX(idxThresh);
% %     crowdedThresh(ii) = (subjectThreshCro(ii).threshSW4 *100);
% %     uncrowdedThresh(ii) = (subjectThreshUnc(ii).threshSW4 * 100);
%  end
%%
sz = 200;
figure;
scatter(swUncr,thresholdUnc,sz,cNew,'filled');
hold on
scatter(swCro,thresholdMatchCro,sz,cNew,'filled');
names = {'Uncrowded','Crowded'};
set(gca,'xtick',[1 2],'xticklabel', names,'FontSize',15,...
       'FontWeight','bold')
ylabel('Performance','FontSize',15,'FontWeight','bold')
axis([0.5 2.5 0 100])

hold on;
x2 = 2+zeros(subNum,1)';
% scatter(x2,thresholdSize2,sz,c,'filled')
% hold on
% scatter(x2(1),mean(thresholdSize2),sz,'k','Linewidth',5);

xS = [1 2];


for ii = 1:length(subjectsAll)
    yMean = [thresholdUnc(1,ii) thresholdMatchCro(1,ii)];
    line(xS, yMean, 'Color',c(ii,:),'LineWidth',2);
end

yMean = [mean(thresholdUnc) mean(thresholdMatchCro)];
scatter(xS(1),yMean(1),sz,'k','filled')
hold on
scatter(xS(2),yMean(2),sz,'k','filled')
[~,p,ci] = ttest(thresholdUnc,thresholdMatchCro);

t = text(1.25,20,sprintf('p = %.3f', p));
t(1).FontSize = 14;
line(xS, yMean, 'Color','k','LineWidth',2);


ySTD = [std(thresholdUnc) std(thresholdMatchCro)]; %standard deviation
ySEM = ySTD/(sqrt(subNum)); %Standard Error Of The Mean At Each Value Of �x�

CI95 = tinv([0.025 0.975], subNum-1);  % Calculate 95% Probability Intervals Of t-Distribution
yCI95 = bsxfun(@times, ySEM, CI95(:)); % Calculate 95% Confidence Intervals Of All Experiments At Each Value Of �x�

errorbar(2,yMean(2),yCI95(4),yCI95(4),...
    'vertical', '-ok','MarkerSize',4,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)
errorbar(1,yMean(1),yCI95(1),yCI95(1),...
    'vertical', '-ok','MarkerSize',4,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',2)

title({'Performance for Uncrowded vs Crowded', 'Strokewidth Size at Uncrowded Threshold'})

saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllSubjSameUSW.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/AllSubjSameUSW.epsc');
%% Crowding Effect Graph
figure;
scatter(zeros(1,length(thresholdMatchCro)),thresholdUnc - thresholdMatchCro,sz,c,'d','filled')
set(gca,'xtick',[],'xticklabel', [],'FontSize',15,...
       'FontWeight','bold')
   hold on
errorbar(0,mean(thresholdUnc - thresholdMatchCro),std(thresholdUnc - thresholdMatchCro),...
    'vertical', '-ok','MarkerSize',10,...
    'MarkerEdgeColor','black',...
    'MarkerFaceColor','black',...
    'CapSize',10,...
    'LineWidth',1)
ylabel({'\Delta Performance','Crowding Effect'})
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PerfCrowdingEffect.png');
saveas(gcf,'../../SummaryDocuments/GraphsforSummaryDocs/ManuScriptV1/PerfCrowdingEffect.epsc');
end

